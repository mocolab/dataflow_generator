/*! \file
    \brief Declaration of functions that are defined in master.c 

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#ifndef MASTER_HELPER_H
#define MASTER_HELPER_H

#include "globalMasterInclude.h"
#include "errors.h" 
#include "message.h" 

// Process handler functions
void installProcess(int virtualLocation, int processType, int appID, int processID, int taskparallelism, int dataparallelism, DALProcess *p);
void startProcess(int virtualLocation, int appID, int processID, DALProcess *p);
void stopProcess(int virtualLocation, int appID, int processID, DALProcess *p);
void pauseProcess(int virtualLocation, int appID, int processID, DALProcess *p);
void resumeProcess(int virtualLocation, int appID, int processID, DALProcess *p);

// Channel handler functions
void linkChannel(int virtualLocation, int appID, int processID, int port, int channelID, int portType, int tokenrate, DALProcess *p);
void installChannel(int virtualSrclocation, int virtualDestlocation, int appID, int channelID, int capacity, int tokensize, int initialtokens, int channelType, DALProcess *p);
void killChannel(int virtualSrclocation, int virtualDestlocation, int appID, int channelID, DALProcess *p);

// Ack functions
void sendCollectiveAck(int *ackVirtualLocations, int numberOfVirtualLocations, DALProcess *p);

// Internal functions
int getSlavePort(int location, DALProcess *p);
void installLocalFifo(int slaveId, int appID, int channelID, int capacity, int tokensize, int initialtokens, int channelType, DALProcess *p);
void installInFifo(int slaveId, int appID, int channelID, int capacity, int peerLoc, int peerRank, int tokensize, int initialtokens, int channelType, DALProcess *p);
void installOutFifo(int slaveId, int appID, int channelID, int capacity, int peerLoc, int peerRank, int tokensize, int initialtokens, int channelType, DALProcess *p);
void killFifo(int location, int appID, int channelID, DALProcess *p);
void sendAck(int location, DALProcess * p);

// Receive functions
void receiveEvent(int *location, int *appID, int *processID, int *message, DALProcess *p);
void receiveAck(int *location, int *appID, int *processID, DALProcess * p);

// Reconfiguration functions
void reconfigureAfterFault(int location, DALProcess *p);
int reconfigureAfterRecovery(int location, DALProcess *p);
void reconfigureAfterRecoverySwitch(int location, int virtualProcessorToRecover, DALProcess *p);

#endif
