#ifndef MASTER_H
#define MASTER_H

#include "globalMasterInclude.h"

#define FAULTY_CORE_EVENT_TYPE -1
#define RECOVERED_CORE_EVENT_TYPE -2

void master_init(DALProcess *);
int master_fire(DALProcess *);
void master_finish(DALProcess *);

#endif
