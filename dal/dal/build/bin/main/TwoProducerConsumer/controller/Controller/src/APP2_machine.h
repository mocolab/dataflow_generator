#ifndef APP2_MACHINE_H
#define APP2_MACHINE_H

#include <stdio.h> 
#include <stdlib.h>

#include "masterhelper.h"
#include "globalMasterInclude.h"
#include "errors.h"
#include "message.h"

int APP2_start(AppState *appState, StateT state, DALProcess* p); 
int APP2_stop(AppState *appState, StateT state, DALProcess* p); 
int APP2_resume(AppState *appState, StateT state, DALProcess* p); 
int APP2_pause(AppState *appState, StateT state, DALProcess* p); 
int APP2_startPaused(AppState *appState, StateT state, DALProcess* p); 
int APP2_restartRequired(StateT state, int location, DALProcess* p); 
int APP2_restartRequiredVirtual(StateT state, int location, DALProcess* p); 

#endif
