#ifndef APP1_MACHINE_H
#define APP1_MACHINE_H

#include <stdio.h> 
#include <stdlib.h>

#include "masterhelper.h"
#include "globalMasterInclude.h"
#include "errors.h"
#include "message.h"

int APP1_start(AppState *appState, StateT state, DALProcess* p); 
int APP1_stop(AppState *appState, StateT state, DALProcess* p); 
int APP1_resume(AppState *appState, StateT state, DALProcess* p); 
int APP1_pause(AppState *appState, StateT state, DALProcess* p); 
int APP1_startPaused(AppState *appState, StateT state, DALProcess* p); 
int APP1_restartRequired(StateT state, int location, DALProcess* p); 
int APP1_restartRequiredVirtual(StateT state, int location, DALProcess* p); 

#endif
