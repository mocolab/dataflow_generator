#ifndef GLOBAL_MASTER_H
#define GLOBAL_MASTER_H

#include "dal.h"

#define NUMBER_PHYSICAL_PROCESSORS 4
#define NUMBER_STATES 3
#define NUMBER_SLAVES 1
#define MAX_PROCESSORS_PER_SLAVE 4

typedef enum StateT {State_1, State_2, END_STATE} StateT;

#define PORTTYPE_IN 0
#define PORTTYPE_OUT 1


typedef enum AppState {APP_STOPPING, APP_RUNNING, APP_PAUSING} AppState;

typedef struct _template_content {
    int processType;
    int location;
} Template_Content;


typedef struct _local_states {
    int location;
    int **templateMap;
    Template_Content ***templates;
    unsigned int *locationToPortMap;
    unsigned int *locationToRankMap;
    char **faultyProcessors;
    char **aliveProcessors;
    char **substituteProcessors;
    char **wrongMappedVirtualProcessors;
    int *virtualToPhysicalProcessor;
    enum AppState appStates[2];
    enum StateT state;
} Master_State;

#endif
