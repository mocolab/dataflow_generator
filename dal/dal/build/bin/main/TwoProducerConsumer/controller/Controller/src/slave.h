/*! \file
    \brief Declaration of functions that are defined in slave.c 

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#ifndef SLAVE_H
#define SLAVE_H

#include "dal.h"

// Port to send a command to the master
#define PORT_EVENT 0
// Port to send an acknowledgment to the master
#define PORT_ACK 1
// Port used to receive a message from the master
#define PORT_IN 2

typedef struct _local_states {
    int location;
} Slave_State;

void slave_init(DALProcess *);
int slave_fire(DALProcess *);
void slave_finish(DALProcess *);

#endif
