#include "master.h"

#include "masterhelper.h"
#include "errors.h"
#include "message.h"

#include "APP1_machine.h"
#include "APP2_machine.h"
#include <stdio.h> 
#include <stdlib.h>
#include <stdint.h>

// Initialize the finite state machine to its
// default state.
void master_init(DALProcess *p) { 
    p->local->appStates[0] = APP_STOPPING;
    p->local->appStates[1] = APP_STOPPING;

    // Which state uses which template
    p->local->templateMap = (int **) malloc(sizeof(int *) * 2);
    p->local->templateMap[0] =(int *) malloc(sizeof(int) * NUMBER_STATES);
    for (int i = 0; i < NUMBER_STATES; i++) p->local->templateMap[0][i] = -1;
    p->local->templateMap[0][State_1] = 0;
    p->local->templateMap[1] =(int *) malloc(sizeof(int) * NUMBER_STATES);
    for (int i = 0; i < NUMBER_STATES; i++) p->local->templateMap[1][i] = -1;
    p->local->templateMap[1][State_2] = 0;

    // The actual templates
    p->local->templates = (Template_Content ***) malloc(sizeof(Template_Content **) * 2); 
    p->local->templates[0] = (Template_Content **) malloc(sizeof(Template_Content *) * 1); 
    static Template_Content tcTable0[3] = {
        {PROCESS_POSIX_T, 2}, //generator
        {PROCESS_POSIX_T, 2}, //consumer
        {PROCESS_POSIX_T, 2}, //square
    };
    p->local->templates[0][0] = tcTable0;
    p->local->templates[1] = (Template_Content **) malloc(sizeof(Template_Content *) * 1); 
    static Template_Content tcTable1[3] = {
        {PROCESS_POSIX_T, 1}, //generator
        {PROCESS_POSIX_T, 1}, //consumer
        {PROCESS_POSIX_T, 1}, //square
    };
    p->local->templates[1][0] = tcTable1;

    // Location to slave map
    p->local->locationToPortMap = (unsigned int *) malloc(sizeof( unsigned int ) * NUMBER_PHYSICAL_PROCESSORS); 
    p->local->locationToPortMap[0] = 1;
    p->local->locationToPortMap[1] = 1;
    p->local->locationToPortMap[2] = 1;
    p->local->locationToPortMap[3] = 1;

    // Location to rank map
    p->local->locationToRankMap = (unsigned int *) malloc(sizeof( unsigned int ) * NUMBER_PHYSICAL_PROCESSORS); 
    p->local->locationToRankMap[0] = 0;
    p->local->locationToRankMap[1] = 0;
    p->local->locationToRankMap[2] = 0;
    p->local->locationToRankMap[3] = 0;

    // List with all faulty processors (empty at the beginning)
    p->local->faultyProcessors = (char **) malloc(sizeof(char *) * (1 + NUMBER_SLAVES));
    p->local->faultyProcessors[1] = (char *) malloc(sizeof(char) * MAX_PROCESSORS_PER_SLAVE);
    for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) p->local->faultyProcessors[1][i] = 0;

    // List with all substitute processors
    p->local->substituteProcessors = (char **) malloc(sizeof(char *) * (1 + NUMBER_SLAVES));
    p->local->substituteProcessors[1] = (char *) malloc(sizeof(char) * MAX_PROCESSORS_PER_SLAVE);
    for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) p->local->substituteProcessors[1][i] = 0;
    p->local->substituteProcessors[1][3] = 1;

    // Array with virtual processors to physical processors
    p->local->virtualToPhysicalProcessor = (int *) malloc(sizeof( int ) * NUMBER_PHYSICAL_PROCESSORS); 
    p->local->virtualToPhysicalProcessor[0] = 0; 
    p->local->virtualToPhysicalProcessor[1] = 1; 
    p->local->virtualToPhysicalProcessor[2] = 2; 
    p->local->virtualToPhysicalProcessor[3] = -1; // Substitute location

    // List with all wrong mapped virtual processors (empty at the beginning)
    p->local->wrongMappedVirtualProcessors = (char **) malloc(sizeof(char *) * (1 + NUMBER_SLAVES));
    p->local->wrongMappedVirtualProcessors[1] = (char *) malloc(sizeof(char) * MAX_PROCESSORS_PER_SLAVE);
    for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) p->local->wrongMappedVirtualProcessors[1][i] = 0;

    // List with all alive processors
    p->local->aliveProcessors = (char **) malloc(sizeof(char *) * (1 + NUMBER_SLAVES));
    p->local->aliveProcessors[1] = (char *) malloc(sizeof(char) * MAX_PROCESSORS_PER_SLAVE);
    for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) p->local->aliveProcessors[1][i] = 0;
    p->local->aliveProcessors[1][1] = 1;
    p->local->aliveProcessors[1][2] = 1;
    p->local->aliveProcessors[1][3] = 1;

    p->local->state = State_1;
    APP1_start(&(p->local->appStates[0]), p->local->state, p);

#ifdef TESTING_MODE
    printf("State_1\n"); fflush(stdout);
#endif
#ifdef DEMO
    printf("Controller: new State is State_1\n"); fflush(stdout);
#endif
    return;
}

// Master finite state machine: is regularly executed until
// the end state is reached.
int master_fire(DALProcess * p) { 
    int location;
    int appID;
    int processID;
    int event;

    receiveEvent(&location, &appID, &processID, &event, p);
    switch(p->local->state){
        case State_1:
            if (event == 0) {
                APP1_stop(&(p->local->appStates[0]), p->local->state, p);
                p->local->state = State_2;
                APP2_start(&(p->local->appStates[1]), p->local->state, p);

#ifdef TESTING_MODE
                printf("State_2\n"); fflush(stdout);
#endif
#ifdef DEMO
                printf("Controller: new State is State_2\n"); fflush(stdout);
#endif
            }
            else if (event == FAULTY_CORE_EVENT_TYPE) {
                bool APP1_restart = APP1_restartRequired(p->local->state, location, p);
                if (APP1_restart) APP1_stop(&(p->local->appStates[0]), p->local->state, p);
                reconfigureAfterFault(location, p);
                if (APP1_restart)APP1_start(&(p->local->appStates[0]), p->local->state, p);
            }
            else if (event == RECOVERED_CORE_EVENT_TYPE) {
                int affectedVirtualProcessor = reconfigureAfterRecovery(location, p);
                if (affectedVirtualProcessor >= 0) {
                    bool APP1_restart = APP1_restartRequiredVirtual(p->local->state, affectedVirtualProcessor, p);
                    if (APP1_restart) APP1_stop(&(p->local->appStates[0]), p->local->state, p);
                    reconfigureAfterRecoverySwitch(location, affectedVirtualProcessor, p);
                    if (APP1_restart)APP1_start(&(p->local->appStates[0]), p->local->state, p);
                }
            }
            break;
        case State_2:
            if (event == 1) {
                APP2_stop(&(p->local->appStates[1]), p->local->state, p);
                p->local->state = END_STATE;

#ifdef TESTING_MODE
                printf("END_STATE\n"); fflush(stdout);
#endif
#ifdef DEMO
                printf("Controller: new State is END_STATE\n"); fflush(stdout);
#endif
                if (p->local->appStates[0] != APP_STOPPING) {
                    error_abort("Not all applications are correctly stopped.");
                }
                if (p->local->appStates[1] != APP_STOPPING) {
                    error_abort("Not all applications are correctly stopped.");
                }
                
                for (int i=1; i<=1; i++) {
                    sendMessage((void *)(intptr_t)i, MSGCNTL, STOPSLAVE, i, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
                }

                return(1);
            }
            else if (event == FAULTY_CORE_EVENT_TYPE) {
                bool APP2_restart = APP2_restartRequired(p->local->state, location, p);
                if (APP2_restart) APP2_stop(&(p->local->appStates[1]), p->local->state, p);
                reconfigureAfterFault(location, p);
                if (APP2_restart)APP2_start(&(p->local->appStates[1]), p->local->state, p);
            }
            else if (event == RECOVERED_CORE_EVENT_TYPE) {
                int affectedVirtualProcessor = reconfigureAfterRecovery(location, p);
                if (affectedVirtualProcessor >= 0) {
                    bool APP2_restart = APP2_restartRequiredVirtual(p->local->state, affectedVirtualProcessor, p);
                    if (APP2_restart) APP2_stop(&(p->local->appStates[1]), p->local->state, p);
                    reconfigureAfterRecoverySwitch(location, affectedVirtualProcessor, p);
                    if (APP2_restart)APP2_start(&(p->local->appStates[1]), p->local->state, p);
                }
            }
            break;
        case END_STATE:
            errno_abort ("End state should not be reached.");
            return(1);
        default:
            errno_abort ("Undefined state.");
            return(1);
    }
    return(0);
}

// Initialize the finite state machine to its
// default state.
void master_finish(DALProcess *p) { 

    // List with all alive processors
    free(p->local->aliveProcessors[1]);
    free(p->local->aliveProcessors);

    // List with all wrong mapped virtual processors
    free(p->local->wrongMappedVirtualProcessors[1]);
    free(p->local->wrongMappedVirtualProcessors);

    // Array with virtual processors to physical processors
    free(p->local->virtualToPhysicalProcessor);

    // List with all substitute processors
    free(p->local->substituteProcessors[1]);
    free(p->local->substituteProcessors);

    // List with all faulty processors
    free(p->local->faultyProcessors[1]);
    free(p->local->faultyProcessors);

    // Location to slave map
    free(p->local->locationToPortMap); 

    // Location to rank map
    free(p->local->locationToRankMap); 

    // The actual templates
    free(p->local->templates[0]); 
    free(p->local->templates[1]); 
    free(p->local->templates); 
    // Which state uses which template
    free(p->local->templateMap[0]);
    free(p->local->templateMap[1]);
    free(p->local->templateMap);
    return;
}
