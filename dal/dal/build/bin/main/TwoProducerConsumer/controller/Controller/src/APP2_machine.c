#include "APP2_machine.h"

typedef struct 
{
	int Id;
	int WorkGroups;
	int WorkItems;
	params Parameters;
} ProcessInfo;

typedef struct 
{
	int Id;
	int FromId;
	int ToId;
	size_t Size;
	int TokenSize;
	int InitialTokens;
	int ChannelType;
} ChannelInfo;

typedef struct 
{
	int ProcessId;
	int PortId;
	int ChannelId;
	int PortType;
	int TokenRate;
} PortInfo;


static const int AppId = 1;

static ProcessInfo Processes[] = 
{
    {0, 1, 1, {0, 0, "generator"} },
    {1, 1, 1, {0, 0, "consumer"} },
    {2, 1, 1, {0, 0, "square"} },
};
static const size_t ProcessCount = sizeof(Processes)/sizeof(ProcessInfo);

static ChannelInfo Channels[] = 
{
    {0, 0, 2, 10, 1, 0, 0},
    {1, 2, 1, 10, 1, 0, 0},
};
static const size_t ChannelCount = sizeof(Channels)/sizeof(ChannelInfo);

static PortInfo PortLinks[] = 
{
    {0, 0, 0, PORTTYPE_OUT, 1 },
    {1, 0, 1, PORTTYPE_IN, 1 },
    {2, 0, 0, PORTTYPE_IN, 1 },
    {2, 1, 1, PORTTYPE_OUT, 1 },
};
static const size_t PortLinkCount = sizeof(PortLinks)/sizeof(PortInfo);


// Starts an application
int APP2_start(AppState *appState, StateT state, DALProcess* p) { 
    if (*appState != APP_STOPPING) {
        error_abort("Cannot start application APP2 as it is not stopped.");
    }
    int myTemplate = p->local->templateMap[AppId][state];

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        memcpy(&p->parameters, &ppi->Parameters, sizeof(p->parameters));
        installProcess(p->local->templates[AppId][myTemplate][ppi->Id].location, p->local->templates[AppId][myTemplate][ppi->Id].processType, AppId, ppi->Id, ppi->WorkGroups, ppi->WorkItems, p);
    }

    for(ChannelInfo *pci = Channels; pci < Channels + ChannelCount; ++pci)
    {
        installChannel(p->local->templates[AppId][myTemplate][pci->FromId].location, p->local->templates[AppId][myTemplate][pci->ToId].location, AppId, pci->Id, pci->Size, pci->TokenSize, pci->InitialTokens, pci->ChannelType, p);
    }

    for(PortInfo *ppi = PortLinks; ppi < PortLinks + PortLinkCount; ++ppi)
    {
        linkChannel(p->local->templates[AppId][myTemplate][ppi->ProcessId].location, AppId, ppi->ProcessId, ppi->PortId, ppi->ChannelId, ppi->PortType, ppi->TokenRate, p);
    }

    int ackLocations[ProcessCount];
    for(size_t i = 0; i < ProcessCount; ++i)
    {
        ackLocations[i] = p->local->templates[AppId][myTemplate][Processes[i].Id].location;
    }
    sendCollectiveAck(ackLocations, ProcessCount, p);

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        startProcess(p->local->templates[AppId][myTemplate][ppi->Id].location, AppId, ppi->Id, p);
    }

    *appState = APP_RUNNING;
    return(1);
}

// Stops an application
int APP2_stop(AppState *appState, StateT state, DALProcess* p) { 
    if (!(*appState == APP_RUNNING || *appState == APP_PAUSING)) {
        error_abort("Cannot stop application APP2 as it is not running.");
    }
    int myTemplate = p->local->templateMap[AppId][state];

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        stopProcess(p->local->templates[AppId][myTemplate][ppi->Id].location, AppId, ppi->Id, p);
    }

    int ackLocations[ProcessCount];
    for(size_t i = 0; i < ProcessCount; ++i)
    {
        ackLocations[i] = p->local->templates[AppId][myTemplate][Processes[i].Id].location;
    }
    sendCollectiveAck(ackLocations, ProcessCount, p);

    for(ChannelInfo *pci = Channels; pci < Channels + ChannelCount; ++pci)
    {
        killChannel(p->local->templates[AppId][myTemplate][pci->FromId].location, p->local->templates[AppId][myTemplate][pci->ToId].location, AppId, pci->Id, p);
    }

    *appState = APP_STOPPING;
    return(1);
}

// Resumes an application
int APP2_resume(AppState *appState, StateT state, DALProcess* p) { 
    if (*appState != APP_PAUSING) {
        error_abort("Cannot resume application APP2 as it is not pausing.");
    }
    int myTemplate = p->local->templateMap[AppId][state];

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        resumeProcess(p->local->templates[AppId][myTemplate][ppi->Id].location, AppId, ppi->Id, p);
    }

    *appState = APP_RUNNING;
    return(1);
}

// Pauses an application
int APP2_pause(AppState *appState, StateT state, DALProcess* p) { 
    if (*appState != APP_RUNNING) {
        error_abort("Cannot pause application APP2 as it is not running.");
    }
    int myTemplate = p->local->templateMap[AppId][state];

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        pauseProcess(p->local->templates[AppId][myTemplate][ppi->Id].location, AppId, ppi->Id, p);
    }

    *appState = APP_PAUSING;
    return(1);
}

// Installs an application
int APP2_startPaused(AppState *appState, StateT state, DALProcess* p) { 
    if (*appState != APP_STOPPING) {
        error_abort("Cannot start application APP2 as it is not stopped.");
    }
    int myTemplate = p->local->templateMap[AppId][state];

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        installProcess(p->local->templates[AppId][myTemplate][ppi->Id].location, p->local->templates[AppId][myTemplate][ppi->Id].processType, AppId, ppi->Id, ppi->WorkGroups, ppi->WorkItems, p);
    }

    for(ChannelInfo *pci = Channels; pci < Channels + ChannelCount; ++pci)
    {
        installChannel(p->local->templates[AppId][myTemplate][pci->FromId].location, p->local->templates[AppId][myTemplate][pci->ToId].location, AppId, pci->Id, pci->Size, pci->TokenSize, pci->InitialTokens, pci->ChannelType, p);
    }

    for(PortInfo *ppi = PortLinks; ppi < PortLinks + PortLinkCount; ++ppi)
    {
        linkChannel(p->local->templates[AppId][myTemplate][ppi->ProcessId].location, AppId, ppi->ProcessId, ppi->PortId, ppi->ChannelId, ppi->PortType, ppi->TokenRate, p);
    }

    int ackLocations[ProcessCount];
    for(size_t i = 0; i < ProcessCount; ++i)
    {
        ackLocations[i] = p->local->templates[AppId][myTemplate][Processes[i].Id].location;
    }
    sendCollectiveAck(ackLocations, ProcessCount, p);

    *appState = APP_PAUSING;
    return(1);
}

// Is a restart required for a certain physical location
int APP2_restartRequired(StateT state, int location, DALProcess* p) { 
    int myTemplate = p->local->templateMap[AppId][state];
    int vLocation;

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        vLocation = p->local->templates[AppId][myTemplate][ppi->Id].location;
        if (p->local->virtualToPhysicalProcessor[vLocation] == location) return true;
    }

    return false;
}

// Is a restart required for a certain virtual location
int APP2_restartRequiredVirtual(StateT state, int location, DALProcess* p) { 
    int myTemplate = p->local->templateMap[AppId][state];
    int vLocation;

    for(ProcessInfo *ppi = Processes; ppi < Processes + ProcessCount; ++ppi)
    {
        vLocation = p->local->templates[AppId][myTemplate][ppi->Id].location;
        if (p->local->virtualToPhysicalProcessor[vLocation] == location) return true;
    }

    return false;
}

