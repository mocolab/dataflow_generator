/*! \file
    \brief Sending and receiving messages via channels

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/
#include "message.h"

#include "errors.h"
#include "dal.h"

#include <stdio.h> 

//! Sends a message to a channel.
/*!
    The function blocks if there is no space on the channel to send
    the message.
    \param queue points to the channel onto which we send the message
    \param msgType is the type of message we send (currently one of 
         MSGCNTL, MSGINSTALLP, MSGLINKC (see also message.h )
    \param message denotes the contents of the message for type MSGCNTL
    \param location denotes the sending or recieving location
    \param processID denotes the ID of the sending or receiving process
    \param name[] is a string that contains the name of the process to be
        installed in message type MSGINSTALLP
    \param basename[] is a string that contains the basename of the process to be
        installed in message type MSGINSTALLP
    \param port denotes the port which is linked to a channel in
        message type MSGLINKC
    \param channel points to the channel that is linked to port
        in message type MSGLINKC
*/
void sendMessage(void *port, int msgType, int message, int location, int rank, int appID, int processID, int channelPort, int channelID, int capacity, int processType, int taskparallelism, int dataparallelism, int tokensize, int initialtokens, int portType, int tokenrate, int channelType, DALProcess *p){
    msgInstallP InstallP;
    msgLinkC LinkC;
    msgCntl Cntl;
    msgCntlC CntlC;
		
    switch(msgType) {
        case MSGCNTL:
            Cntl.type = MSGCNTL;
            Cntl.appID = appID;
            Cntl.processID = processID;
            Cntl.location = location;
            Cntl.message = message;
            DAL_write(port, &Cntl, sizeof(msgCntl), p);
#ifdef DEBUG
            printf("msg sent: (type=MSGCNTL, location=%d, appID=%d, processID=%d, message=%d)\n", location, appID, processID, message);
#endif
            break;
        case MSGINSTALLP:
            InstallP.type = MSGINSTALLP;
            InstallP.location = location;
            InstallP.appID = appID;
            InstallP.processID = processID;
            InstallP.processType = processType;
            InstallP.taskparallelism = taskparallelism;
            InstallP.dataparallelism = dataparallelism;
            InstallP.parameters = (params)(p->parameters);
            DAL_write(port, &InstallP, sizeof(msgInstallP), p);
#ifdef DEBUG
            printf("msg sent: (type=MSGINSTALLP, location=%d, appID=%d, processID=%d)\n", location, appID, processID);
#endif
            break;
        case MSGLINKC:
            LinkC.type = MSGLINKC;
            LinkC.appID = appID;
            LinkC.processID = processID;
            LinkC.port = channelPort;
            LinkC.channelID = channelID;
            LinkC.tokenrate = tokenrate;
            LinkC.portType = portType;
            DAL_write(port, &LinkC, sizeof(msgLinkC), p);
#ifdef DEBUG
            printf("msg sent: (type=MSGLINKC, location=%d, appID=%d, processID=%d, port=%d)\n", location, appID, processID, port);
#endif
            break;
        case MSGCNTLC:
        	CntlC.type = MSGCNTLC;
        	CntlC.appID = appID;
        	CntlC.channelID = channelID;
        	CntlC.message = message;
        	CntlC.capacity = capacity;
        	CntlC.tokensize = tokensize;
        	CntlC.initialtokens = initialtokens;
        	CntlC.peerLocation = location;
        	CntlC.peerRank = rank;
        	CntlC.channelType = channelType;
            DAL_write(port, &CntlC, sizeof(msgCntlC), p);
			break; 
        default:
            error_abort("attempt to send with wrong message type");
    }  
}

//! Receives a message from a channel.
/*!
    The function blocks if there is no message on the channel.
    \param queue points to the channel from which we receive the message
    \param msgType is the type of message we received (currently one of 
         MSGCNTL, MSGINSTALLP, MSGLINKC (see also message.h )
    \param Cntl is a pointer to structure of type msgCntl which contains
        the message if msgType = MSGCNTL
    \param InstallP is a pointer to structure of type msgInstallP which contains
        the message if msgType = MSGINSTALLP
    \param LinkC is a pointer to structure of type msgLinkC which contains
        the message if msgType = MSGLINKC
*/
void receiveMessage(void *port,  int *msgType, \
    msgCntl *Cntl, msgInstallP *InstallP, msgLinkC *LinkC, msgCntlC *CntlC, DALProcess *p){
    
    DAL_read(port, msgType, sizeof(int), p);
			
    switch(*msgType) {
        case MSGCNTL:
        	DAL_read(port, ((char *) Cntl) + sizeof(int), sizeof(msgCntl) - sizeof(int), p);
        	Cntl->type = *msgType;
#ifdef DEBUG
            printf("msg recv: (type=MSGCNTL, location=%x, appID=%x, processID=%d, message=%d)\n", Cntl->location, Cntl->appID, Cntl->processID, Cntl->message);
#endif
            break;
        case MSGINSTALLP:
        	DAL_read(port, ((char *) InstallP) + sizeof(int), sizeof(msgInstallP) - sizeof(int), p);
        	InstallP->type = *msgType;
#ifdef DEBUG
        	printf("msg recv: (type=MSGINSTALLP, location=%x, appID=%x, processID=%d)\n", InstallP->location, InstallP->appID, InstallP->processID); fflush(stdout);
#endif
            break;
        case MSGLINKC:
        	DAL_read(port, ((char *) LinkC) + sizeof(int), sizeof(msgLinkC) - sizeof(int), p);
        	LinkC->type = *msgType;
#ifdef DEBUG
        	printf("msg recv: (type=MSGLINKQ, appID=%d, processID=%d, port=%d)\n", LinkC->appID, LinkC->processID, LinkC->port);
#endif
            break;
        case MSGCNTLC:
        	DAL_read(port, ((char *) CntlC) + sizeof(int), sizeof(msgCntlC) - sizeof(int), p);
        	CntlC->type = *msgType;
        	break;
        default:
            error_abort("attempt to read with wrong message type");
    }  
}
