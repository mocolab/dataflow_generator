/*! \file
    \brief Declares data structure and functions of message.c .

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include "dal.h"

#define MSGCNTL 1
#define MSGINSTALLP 2
#define MSGLINKC 3
#define MSGCNTLC 4

#define STARTP -1
#define PAUSEP -2
#define STOPP -3
#define RESUMEP -4
#define ACK -5
#define STOPSLAVE -6

#define INSTALLLOCC -7
#define INSTALLOUTC -8
#define INSTALLINC -9
#define KILLC -10

//! defines the datastructure of control messages 
typedef struct msgCntl {
        int             type; //!< type of the message (MSGCNTL)
        int             appID; //!< sender or receiver appID of the message (>=0)
        int             processID; //!< sender or receiver processID of the message (>=0)
        int             location; //!< sender or receiver location of the message
        long            message; //!< contents of the message (STARTP ... ACK or arbitrary message>=0)
} msgCntl;

//! defines the datastructure of install process messages
typedef struct msgInstallP {
        int             type; //!< type of the message (MSGINSTALLP)
        int             location; //!< sender or receiver location of the message
        int             appID; //!< sender or receiver appID of the message (>=0)
        int             processID; //!< sender or receiver processID of the message (>=0)
        int				processType; //!< type of the process (PROCESS_POSIX_T or PROCESS_OPENCL_T)
        int				taskparallelism; //!< amount of processes that can run in parallel
        int				dataparallelism;
        params      parameters; //!< parameters to be passed to the init of a process;
} msgInstallP;

//! defines the datastructure of install channel messages
typedef struct msgCntlC {
        int             type; //!< type of the message (MSGINSTALLP)
        int             appID; //!< sender or receiver appID of the message (>=0)
        int             channelID; //!< channelID of the message (>=0)
        int             capacity; //!< capacity of the channel (>=0)
        int				tokensize; //!< tokensize of the channel
        int				initialtokens; //!< initial tokens of the channel
        int             peerLocation; //!< location of peer node (>=0)
        int             peerRank; //!< rank of peer node (>=0)
        int				channelType; //!< type fo the channel (0 fifo, 1 wfifo)
        long            message; //!< contents of the message (STARTP ... ACK or arbitrary message>=0)
} msgCntlC;

//! defines the datastructure of link channel messages
typedef struct msgLinkC {
        int             type; //!< type of the message (MSGLINKC)
        int             appID; //!< sender or receiver appID of the message (>=0)
        int             processID; //!< sender or receiver processID of the message (>=0)
        int             port; //!< ID of the port that is linked to channel fifo
        int             channelID; //!< pointer to channel that will be linked to port
        int				portType; //!< type of the port
        int				tokenrate; //!< tokenrate of the port
} msgLinkC;

void sendMessage(void *port, int msgType, int message, int location, int rank, int appID, int processID, int channelPort, int channelID, int capacity, int processType, int taskparallelism, int dataparallelism, int tokensize, int initialtokens, int portType, int tokenrate, int channelType, DALProcess *p);
void receiveMessage(void *port, int *msgType, msgCntl *Cntl, msgInstallP *InstallP, msgLinkC *LinkC, msgCntlC *CntlC, DALProcess *p);

#endif
