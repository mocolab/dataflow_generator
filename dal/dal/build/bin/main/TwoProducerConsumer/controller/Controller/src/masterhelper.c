/*! \file master.c
    \brief Implementation of the master controller
    
    The file contains the functionality of the master controller.
    The function master() is called by the main program as
    a thread that runs continuously. By definition, the master controller
    is running at location 0. Locations of the
    slave controller are in 1 <= location <= NUMBER_SLAVES. The master
    controller receives messages via the channel MSChannel[0] from the
    slave controllers and the running processes (via call to the 
    function DAL_send_event() channel.c. It sends messages to the
    slave controllers in locations 1 <= location <= NUMBER_SLAVES via
    channels MSChannel[location]. The main controller functionality
    is contained in file machine.c which defines function machine()
    which may be generated based on the functionality of the set
    of applications.

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#include "masterhelper.h"

#include <stdio.h> 
#include <stdlib.h>
#include <stdint.h>

//!
/*!
 * Get the port of the slave that is responsible for a specific location
 */
int getSlavePort(int location, DALProcess *p) {
	return p->local->locationToPortMap[location];
}

//!
/*!
 * Get the rank of the location
 */
int getRankForLocation(int location, DALProcess *p) {
	return p->local->locationToRankMap[location];
}

//! Installs a fifo by allocating a fifo.
/*!
    The function installs a fifo by allocating the necessary
    memory and initiating the data structure.
    \param fifoID is the ID of the fifo that is installed, i.e. the index
        where the channel is found in Channel[].
    \param Channel[] is an array which contains the pointer to the
        channel fifos and Channel[fifoID] points to the newly generated
        fifo after execution of the function.
    \param capacity is the capacity of the channel-fifo in Bytes.
*/
void installLocalFifo(int slaveId, int appID, int channelID, int capacity, int tokensize, int initialtokens, int channelType, DALProcess *p){
	sendMessage((void *)(intptr_t)(slaveId), MSGCNTLC, INSTALLLOCC, 0, 0, appID, 0, 0, channelID, capacity, 0, 0, 0, tokensize, initialtokens, 0, 0, channelType, p);
}

//! Installs a fifo by allocating a fifo.
/*!
    The function installs a fifo by allocating the necessary
    memory and initiating the data structure.
    \param fifoID is the ID of the fifo that is installed, i.e. the index
        where the channel is found in Channel[].
    \param Channel[] is an array which contains the pointer to the
        channel fifos and Channel[fifoID] points to the newly generated
        fifo after execution of the function.
    \param capacity is the capacity of the channel-fifo in Bytes.
*/
void installInFifo(int slaveId, int appID, int channelID, int capacity, int peerLoc, int peerRank, int tokensize, int initialtokens, int channelType, DALProcess *p){
	sendMessage((void *)(intptr_t)(slaveId), MSGCNTLC, INSTALLINC, peerLoc, peerRank, appID, 0, 0, channelID, capacity, 0, 0, 0, tokensize, initialtokens, 0, 0, channelType, p);
}

//! Installs a fifo by allocating a fifo.
/*!
    The function installs a fifo by allocating the necessary
    memory and initiating the data structure.
    \param fifoID is the ID of the fifo that is installed, i.e. the index
        where the channel is found in Channel[].
    \param Channel[] is an array which contains the pointer to the
        channel fifos and Channel[fifoID] points to the newly generated
        fifo after execution of the function.
    \param capacity is the capacity of the channel-fifo in Bytes.
*/
void installOutFifo(int slaveId, int appID, int channelID, int capacity, int peerLoc, int peerRank, int tokensize, int initialtokens, int channelType, DALProcess *p){
	sendMessage((void *)(intptr_t)(slaveId), MSGCNTLC, INSTALLOUTC, peerLoc, peerRank, appID, 0, 0, channelID, capacity, 0, 0, 0, tokensize, initialtokens, 0, 0, channelType, p);
}

//! Kill a fifo and reclaim the allocated memory.
/*!
    The function kills a fifo and reclaims the memory.
    \param fifoID is the ID of the fifo that is killed, i.e. the index
        where the channel is found in Channel[].
    \param Channel[] is an array which contains the pointer to the
        channel fifos and Channel[fifoID] points to the channel-fifo
        that is removed.
*/
void killFifo(int slaveId, int appID, int channelID, DALProcess *p){
	sendMessage((void *)(intptr_t)(slaveId), MSGCNTLC, KILLC, 0, 0, appID, 0, 0, channelID, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
}

//! Install a process, to be used by machine()
/*!
    The function installs a process. It needs to be called
    before the start command to the process (which runs init()
    of the process and then fire() repetitively).
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
    \param name is the name of the process and should have less
        then MAX_STRINGLENGTH characters.
    \param basename is the basename of the process and should have less
        then MAX_STRINGLENTGH characters.
*/
void installProcess(int virtualLocation, int processType, int appID, int processID, int taskparallelism, int dataparallelism, DALProcess *p){

    int location = p->local->virtualToPhysicalProcessor[virtualLocation]; 
    
    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGINSTALLP, 0, location, 0, appID, processID, 0, 0, 0, processType, taskparallelism, dataparallelism, 0, 0, 0, 0, 0, p);
}

//! Start a process, to be used by machine(), installProcess needs to be called beforehand
/*!
    The function starts a process. It runs init()
    of the process once and then fire() repetitively, until 
    fire() returns 1 or the process is paused or stopped.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
*/
void startProcess(int virtualLocation, int appID, int processID, DALProcess *p){

    int location = p->local->virtualToPhysicalProcessor[virtualLocation]; 

    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGCNTL, STARTP, 0, 0, appID, processID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
}

//! Stop a process, to be used by machine(), removes and detaches process
/*!
    The function stops a process. It stops the repetitive execution of 
    fire(), runs finish() once and detaches the process. If fire() or 
    finish() does not return, then the slave blocks.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
*/
void stopProcess(int virtualLocation, int appID, int processID, DALProcess *p){

    int location = p->local->virtualToPhysicalProcessor[virtualLocation]; 

    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGCNTL, STOPP, 0, 0, appID, processID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
}

//! Pause a process, to be used by machine()
/*!
    The function pauses a process. It stops the repetitive execution of 
    fire(). If fire() does not return, then the slave blocks.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
*/
void pauseProcess(int virtualLocation, int appID, int processID, DALProcess *p){

    int location = p->local->virtualToPhysicalProcessor[virtualLocation]; 

    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGCNTL, PAUSEP, 0, 0, appID, processID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
}

//! Resume a process, to be used by machine()
/*!
    The function resumes a process, i.e. it starts the repetitive
    execution of fire(). If the process is still running (e.g. 
    fire() did not finish), then the slave blocks.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
*/
void resumeProcess(int virtualLocation, int appID, int processID, DALProcess *p){

    int location = p->local->virtualToPhysicalProcessor[virtualLocation]; 

    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGCNTL, RESUMEP, 0, 0, appID, processID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
}

/*!
    The function resumes a process, i.e. it starts the repetitive
    execution of fire(). If the process is still running (e.g.
    fire() did not finish), then the slave blocks.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
*/
void sendCollectiveAck(int *ackVirtualLocations, int numberOfVirtualLocations, DALProcess *p){
	// init a list where we can check if we already sent an ack or not
	int *portSendList = (int *) malloc(sizeof(int) * NUMBER_PHYSICAL_PROCESSORS); 
	for (int i = 0; i < NUMBER_PHYSICAL_PROCESSORS; i++) {
		portSendList[i] = -1; 
	}

	int totalNumberOfAcksSent = 0; 

	// send the acknowledgement requests
	for (int i = 0; i < numberOfVirtualLocations; i++) {
		int location = p->local->virtualToPhysicalProcessor[ackVirtualLocations[i]];
		int slavePort = getSlavePort(location, p); 
		
		// Only send an ack if no ack has been send so far
		if (portSendList[slavePort] == -1) {
			totalNumberOfAcksSent++; 
			portSendList[slavePort] = 0; 
			sendMessage((void *)(intptr_t)(slavePort), MSGCNTL, ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
		}
	}

	int location;
    int appID;
    int processID;
	for (int i = 0; i < totalNumberOfAcksSent; i++) {
		receiveAck(&location, &appID, &processID, p);
	}

	free(portSendList); 
}

//! Send an Ack message to a slave controller, to be used by machine()
/*!
    The function sends an Ack message to a slave controller. The slave
    controller sends it back. As the slave controller executes all
    the commands sequentially, this mechanism can be used to make sure
    that all previously sent commands have been finished.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
*/
void sendAck(int location, DALProcess *p){
    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGCNTL, ACK, location, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
}

//! Link a channel to a process port, to be used by machine()
/*!
    The function links a channel with channelID to a port of
    a process.
    \param location is the location of the slave which hosts
        the process where 1 <= location <= NUMBER_SLAVES.
    \param processID is the ID of the process; it is local
        to the slave and 0 <= processID < MAX_NUMBER_PROCESSES.
    \param port is the ID of the port to which the channel
        will be attached with 0 <= port < MAX_NUMBER_PORTS.
    \param channelID is the ID of the channel that will be
        attached with 0 <= channelID < MAX_NUMBER_PROCESSCHANNELS.
        The channelID is global for all locations (and slave
        controllers). The channel needs to be installed before
        calling linkChannel.
*/
void linkChannel(int virtualLocation, int appID, int processID, int port, int channelID, int portType, int tokenrate, DALProcess *p){

    int location = p->local->virtualToPhysicalProcessor[virtualLocation]; 

    sendMessage((void *)(intptr_t)(getSlavePort(location, p)), MSGLINKC, 0, 0, 0, appID, processID, port, channelID, 0, 0, 0, 0, 0, 0, portType, tokenrate, 0, p);
}

//! Installs a process channel, to be used by machine()
/*!
    The function installs a channel with channelID.
    \param channelID is the ID of the channel that will be
        attached with 0 <= channelID < MAX_NUMBER_PROCESSCHANNELS.
        The channelID is global for all locations (and slave
        controllers).
    \param capacity is the capacity of the channel in Bytes.
*/
void installChannel(int vritualSrclocation, int virtualDestlocation, int appID, int channelID, int capacity, int tokensize, int initialtokens, int channelType, DALProcess *p){

    int srclocation = p->local->virtualToPhysicalProcessor[vritualSrclocation]; 
    int destlocation = p->local->virtualToPhysicalProcessor[virtualDestlocation]; 

    // Is source and destination on the same slave?
    if (getSlavePort(srclocation, p) == getSlavePort(destlocation, p)) {
    	installLocalFifo(getSlavePort(srclocation, p), appID, channelID, capacity, tokensize, initialtokens, channelType, p);
    } else {
    	installInFifo(getSlavePort(destlocation, p), appID, channelID, capacity, srclocation, getRankForLocation(srclocation, p), tokensize, initialtokens, channelType, p);
    	installOutFifo(getSlavePort(srclocation, p), appID, channelID, capacity, destlocation, getRankForLocation(destlocation, p), tokensize, initialtokens, channelType, p);
    }
}

//! Kills a process channel and reclaims the memory, to be used by machine()
/*!
    The function kills a channel with channelID.
    \param channelID is the ID of the channel that will be
        attached with 0 <= channelID < MAX_NUMBER_PROCESSCHANNELS.
        The channelID is global for all locations (and slave
        controllers). The channel needs to be installed beforehand.
*/
void killChannel(int vritualSrclocation, int virtualDestlocation, int appID, int channelID, DALProcess *p){

    int srclocation = p->local->virtualToPhysicalProcessor[vritualSrclocation]; 
    int destlocation = p->local->virtualToPhysicalProcessor[virtualDestlocation]; 

    // Is source and destination on the same slave?
    if (getSlavePort(srclocation, p) == getSlavePort(destlocation, p)) {
    	killFifo((getSlavePort(srclocation, p)), appID, channelID, p);
    } else {
    	killFifo((getSlavePort(srclocation, p)), appID, channelID, p);
    	killFifo((getSlavePort(destlocation, p)), appID, channelID, p);
    }
}

//! Reads in event from the master controller input queue, to be used by machine()
/*!
    The function reads an event from the input queue. It blocks
    until there is an event present in the input queue.
    \param location points to an integer. After executing
        the function, it contains the location of the sender of the event.
    \param processID points to an integer. After executing
        the function, it contains the processID of the sender of the event.
    \param message points to an integer. After executing
        the function, it contains the message of the event. It is either
        one of the values defined in message.h (e.g. ACK) or some
        message number provided by the sender of the event.
*/
void receiveEvent(int *location, int *appID, int *processID, int *message, DALProcess *p){
    int msgType;
    msgCntl Cntl;

    receiveMessage((void *)(0), &msgType, &Cntl, 0, 0, 0, p);
    
    if (msgType ==  MSGCNTL) {
		*location = Cntl.location;
		*appID = Cntl.appID;
		*processID = Cntl.processID;
		*message = Cntl.message;
    } else {
    	error_abort("wrong message type received at master");
    }
}

//! Reads in ack from the master controller input ack queue, to be used by machine()
/*!
    The function reads an ack from the input queue. It blocks
    until there is an ack present in the input queue.
    \param location points to an integer. After executing
        the function, it contains the location of the sender of the event.
    \param processID points to an integer. After executing
        the function, it contains the processID of the sender of the event.
*/
void receiveAck(int *location, int *appID, int *processID, DALProcess *p){
    int msgType;
    msgCntl Cntl;

    // TODO change the number automatically
    receiveMessage((void *)(2), &msgType, &Cntl, 0, 0, 0, p);

    if (msgType ==  MSGCNTL) {
		*location = Cntl.location;
		*appID = Cntl.appID;
		*processID = Cntl.processID;

        if (Cntl.message != ACK) {
        	error_abort("wrong message type received at ack master");
        }

	} else {
		error_abort("wrong message type received at master");
	}
}

void reconfigureAfterFault(int location, DALProcess *p) {

    // The corresponding slave
    int slave = p->local->locationToPortMap[location]; 

    // Consistency check: check if it was not yet faulty
    char *faultyList = p->local->faultyProcessors[slave];

    if (faultyList[location] == 1) {
    	return;
    }

    // Helper variables
    int *virtualToPhysicalProcessorList = p->local->virtualToPhysicalProcessor;

    // Add the process to the faulty processor list
    p->local->faultyProcessors[slave][location] = 1;

    // Remove it from the alive processor list
    p->local->aliveProcessors[slave][location] = 0;

    // Search for a substitute process
    char *substituteList = p->local->substituteProcessors[slave];

    int substituteLocation = -1;
    for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) {
    	if (substituteList[i] == 1) {
    		substituteLocation = i;
    		substituteList[i] = 0;
    		break;
    	}
    }

    // No free processor!
    if (substituteLocation == -1) {
        printf("WARNING: Mapping to a non-free processor\n"); fflush(stdout);

        // Which virtual processors does this affect
        for (int i = 0; i < NUMBER_PHYSICAL_PROCESSORS; i++) {
            if (virtualToPhysicalProcessorList[i] == location) {
                p->local->wrongMappedVirtualProcessors[slave][i] = 1;
            }
        }

        for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) {
        	if (p->local->aliveProcessors[slave][i] == 1) {
        		substituteLocation = i;
        		break;
        	}
        }
    }

    // Update the virtual to physical processor list
    for (int i = 0; i < NUMBER_PHYSICAL_PROCESSORS; i++) {
        if (virtualToPhysicalProcessorList[i] == location) {
#ifdef DEMO
	printf("Controller: failure on processor %d that runs virtual processor %d\n", location, i); fflush(stdout);
#endif

            virtualToPhysicalProcessorList[i] = substituteLocation; 

#ifdef DEMO
	printf("Controller: virtual processor %d now mapped onto processor %d\n", i, substituteLocation); fflush(stdout);
#endif
        }
    }

#ifdef DEMO
    printf("Controller: new virtual processor to physical processor mapping is\n                "); fflush(stdout);
    for (int i = 0; i < NUMBER_PHYSICAL_PROCESSORS; i++) {
    	if (virtualToPhysicalProcessorList[i] >= 0) {
    		if (i != 0) {
    			printf(", "); fflush(stdout);
    		}
    		printf("%d -> %d", i, virtualToPhysicalProcessorList[i]); fflush(stdout);
    	}
    }
    printf("\n"); fflush(stdout);
#endif

#ifdef TESTING_MODE
    printf("Reconfiguration: %d to %d\n", location, substituteLocation); fflush(stdout);
#endif
}

int reconfigureAfterRecovery(int location, DALProcess *p) {
    // The corresponding slave
    int slave = p->local->locationToPortMap[location];

    // Consistency check: Do only recover a location if is faulty
    char *faultyList = p->local->faultyProcessors[slave];
    if (faultyList[location] == 0) return -1;


    // Do we have wrong mapped virtual processors to recover?
    int virtualProcessorToRecover = -1;
    for (int i = 0; i < MAX_PROCESSORS_PER_SLAVE; i++) {
    	if (p->local->wrongMappedVirtualProcessors[slave][i] == 1) {
    		virtualProcessorToRecover = i;
    		p->local->wrongMappedVirtualProcessors[slave][i] = 0;
    		break;
    	}
    }

    if (virtualProcessorToRecover == -1) {
        p->local->aliveProcessors[slave][location] = 1;
        p->local->substituteProcessors[slave][location] = 1;
        p->local->faultyProcessors[slave][location] = 0;
    }

#ifdef DEMO
	printf("Controller: processor %d is again available\n", location); fflush(stdout);
#endif

#ifdef TESTING_MODE
    printf("Reconfiguration: %d alive\n", location); fflush(stdout);
#endif

    return virtualProcessorToRecover;
}

void reconfigureAfterRecoverySwitch(int location, int virtualProcessorToRecover, DALProcess *p) {

    int slave = p->local->locationToPortMap[location];

    // Add the new location to the virtual processor
    p->local->virtualToPhysicalProcessor[virtualProcessorToRecover] = location;

    // Add it back to the alive list
    p->local->aliveProcessors[slave][location] = 1;

    // Remove it from the fault list
    p->local->faultyProcessors[slave][location] = 0;
}
