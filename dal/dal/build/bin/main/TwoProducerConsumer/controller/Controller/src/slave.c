/*! \file
    \brief Implementation of the slave controller
    
    The file contains the functionality of the slave controller.
    The function slave_fire receives messages from the master
    controller which is at location=0. In the current implementation,
    the slave controller just interprets the messages from the master 
    and executes the corresponding actions (startDALProcess, 
    stopDALProcess, resumeDALProcess, pauseDALProcess, 
    installDALProcess, linkChannel).

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#include "slave.h"

#include "controllerHelper.h"
#include "message.h"

#include <stdio.h>

/**
 * Initial function of the slave
 * @param p DAL process
 */
void slave_init(DALProcess *p) {
}

/**
 * Processes one message from the master controller. If the master sends an ACK message,
    the slave sends an ACK back to the master controller. This is used to inform
    the master controller that the actions that correspond to messages
    that have been send before the ACK message have been completed. This way,
    the master controller can make sure that actions are executed in the
    correct sequence.
 * @param p DAL process
 * @return 0 if successful, 1 if an error occurred
 */
int slave_fire(DALProcess *p) {

    int msgType;
    msgInstallP InstallP;
    msgLinkC LinkC;
    msgCntl Cntl;
    msgCntlC CntlC;
	
    // Receives a message from the master controller (blocking)
	receiveMessage((void *)PORT_IN, &msgType, &Cntl, &InstallP, &LinkC, &CntlC, p);
	
	switch(msgType) {
		// Commands to start, stop, pause, resume a process
		case MSGCNTL:
			switch(Cntl.message) {
				case STARTP:
					startDALProcess(Cntl.appID, Cntl.processID, p);
					break;
				case PAUSEP:
					pauseDALProcess(Cntl.appID, Cntl.processID, p);
					break;
				case STOPP:
					stopDALProcess(Cntl.appID, Cntl.processID, p);
					break;
				case RESUMEP:
					resumeDALProcess(Cntl.appID, Cntl.processID, p);
					break;
				case ACK:
					sendMessage((void *)PORT_ACK, MSGCNTL, ACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p);
					break;
				case STOPSLAVE:
					killScheduler(p);
					return 1;
				default:
					break;
			}
			break;

		// Commands to install a process
		case MSGINSTALLP:
			//copy parameters
			p->parameters = InstallP.parameters;
			installDALProcess(InstallP.appID, InstallP.processID, InstallP.location, InstallP.processType, InstallP.taskparallelism, InstallP.dataparallelism, p);
			break;

		// Commands to link a process with a channel
		case MSGLINKC:
			linkDALChannel(LinkC.appID, LinkC.processID, LinkC.port, LinkC.portType, LinkC.tokenrate, LinkC.channelID, p);
			break;

		// Commands to install and kill a channel
		case MSGCNTLC:
			switch(CntlC.message) {
				case INSTALLLOCC:
					installDALLocalChannel(CntlC.appID, CntlC.channelID, CntlC.capacity, CntlC.tokensize, CntlC.initialtokens, CntlC.channelType, p);
					break;
				case INSTALLINC:
					installDALInChannel(CntlC.appID, CntlC.channelID, CntlC.capacity, CntlC.tokensize, CntlC.peerLocation, CntlC.peerRank, CntlC.channelType, p);
					break;
				case INSTALLOUTC:
					installDALOutChannel(CntlC.appID, CntlC.channelID, CntlC.capacity, CntlC.tokensize, CntlC.peerLocation, CntlC.peerRank, CntlC.channelType, p);
					break;
				case KILLC:
					killDALChannel(CntlC.appID, CntlC.channelID, p);
					break;
				default:
					break;
			}
			break; 

		default:
			printf("wrong message type received\n");
			return 1;
	}
	return 0;
}

/**
 * Cleanup function of the slave controller
 * @param p DAL process
 */
void slave_finish(DALProcess *p) {
}
