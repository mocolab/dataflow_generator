public class PlatformGenerator {

  public PlatformGenerator() {
    //nothing to be done here
  }

  public static void main(String[] args) {
int processorIdCounter = 0;
    java.util.Hashtable<String, Integer> table = new java.util.Hashtable<String, Integer>();
    System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    System.out.println("<architecture xmlns=\"http://www.tik.ee.ethz.ch/~euretile/schema/ARCHITECTURE\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n  xsi:schemaLocation=\"http://www.tik.ee.ethz.ch/~euretile/schema/ARCHITECTURE                       http://www.tik.ee.ethz.ch/~euretile/schema/architecture.xsd\" name=\"Quad-core platform (4 cores with one bus)\">\n ");
    System.out.println("<processor name=\"core_0\" type=\"RISC\" basename=\"core_0\" id=\"" + processorIdCounter+++ "\">");
    System.out.println("  <port name=\"port1\"/>");
    System.out.println("</processor>");
    System.out.println("<processor name=\"core_1\" type=\"RISC\" basename=\"core_1\" id=\"" + processorIdCounter+++ "\">");
    System.out.println("  <port name=\"port1\"/>");
    System.out.println("</processor>");
    System.out.println("<processor name=\"core_2\" type=\"RISC\" basename=\"core_2\" id=\"" + processorIdCounter+++ "\">");
    System.out.println("  <port name=\"port1\"/>");
    System.out.println("</processor>");
    System.out.println("<processor name=\"core_3\" type=\"RISC\" basename=\"core_3\" substitute=\"1\" id=\"" + processorIdCounter+++ "\">");
    System.out.println("  <port name=\"port1\"/>");
    System.out.println("</processor>");
    System.out.println("<shared name=\"localhost\">");
    System.out.println("  <port name=\"port1\"/>");
    System.out.println("  <port name=\"port2\"/>");
    System.out.println("  <port name=\"port3\"/>");
    System.out.println("  <port name=\"port4\"/>");
    System.out.println("</shared>");
    System.out.println("<link name=\"link_1\">");
    System.out.println("  <end_point_1 name=\"core_0\">");
    System.out.println("    <port name=\"port1\"/>");
    System.out.println("  </end_point_1>");
    System.out.println("  <end_point_2 name=\"localhost\">");
    System.out.println("    <port name=\"port1\"/>");
    System.out.println("  </end_point_2>");
    System.out.println("</link>");
    System.out.println("<link name=\"link_2\">");
    System.out.println("  <end_point_1 name=\"core_1\">");
    System.out.println("    <port name=\"port1\"/>");
    System.out.println("  </end_point_1>");
    System.out.println("  <end_point_2 name=\"localhost\">");
    System.out.println("    <port name=\"port2\"/>");
    System.out.println("  </end_point_2>");
    System.out.println("</link>");
    System.out.println("<link name=\"link_3\">");
    System.out.println("  <end_point_1 name=\"core_2\">");
    System.out.println("    <port name=\"port1\"/>");
    System.out.println("  </end_point_1>");
    System.out.println("  <end_point_2 name=\"localhost\">");
    System.out.println("    <port name=\"port3\"/>");
    System.out.println("  </end_point_2>");
    System.out.println("</link>");
    System.out.println("<link name=\"link_4\">");
    System.out.println("  <end_point_1 name=\"core_3\">");
    System.out.println("    <port name=\"port1\"/>");
    System.out.println("  </end_point_1>");
    System.out.println("  <end_point_2 name=\"localhost\">");
    System.out.println("    <port name=\"port4\"/>");
    System.out.println("  </end_point_2>");
    System.out.println("</link>");
    System.out.println("</architecture>");
  }

}