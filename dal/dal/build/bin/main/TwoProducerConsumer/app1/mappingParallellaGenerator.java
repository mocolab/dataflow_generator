public class mappingParallellaGenerator {

  public mappingParallellaGenerator() {
    //nothing to be done here
  }

  public static void main(String[] args) {
int processorIdCounter = 0;
    java.util.Hashtable<String, Integer> table = new java.util.Hashtable<String, Integer>();
    System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    System.out.println("<mapping xmlns=\"http://www.tik.ee.ethz.ch/~euretile/schema/MAPPING\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n  xsi:schemaLocation=\"http://www.tik.ee.ethz.ch/~euretile/schema/MAPPING     http://www.tik.ee.ethz.ch/~euretile/schema/mapping.xsd\" name=\"mapping1\" processnetwork=\"APP1\">\n ");
    System.out.println("<binding name=\"generator\">");
    System.out.println("  <process name=\"generator\"/>");
    System.out.println("  <processor name=\"core_1\"/>");
    System.out.println("</binding>");
    System.out.println("<binding name=\"consumer\">");
    System.out.println("  <process name=\"consumer\"/>");
    System.out.println("  <processor name=\"core_1\"/>");
    System.out.println("</binding>");
    System.out.println("<binding name=\"square\">");
    System.out.println("  <process name=\"square\"/>");
    System.out.println("  <processor name=\"e_core_0_0\"/>");
    System.out.println("</binding>");
    System.out.println("</mapping>");
  }

}