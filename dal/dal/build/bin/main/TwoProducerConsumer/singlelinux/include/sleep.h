/*! \file sleep.h
    \brief Defines methods to sleep

    \authors Lars Schor, Devendra Rai
*/


#ifndef SLEEP_H
#define SLEEP_H

#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "wrap.h"

struct timespec * millisecondsFromNow (struct timespec * time, int millisecs);
void sleepMilliseconds(int millisecs, wrapDALProcess *DALprocess);

#endif /* SLEEP_H */
