/*
 * WindowedFifo.h
 */

#ifndef WINDOWEDFIFO_H_
#define WINDOWEDFIFO_H_

#include "InnerProcessorFifo.h"
#include "errors.h"

class WindowedFifo: public InnerProcessorFifo {

private:
	int isOnBlockEnqueue;
	int isOnBlockDequeue;

public:

	WindowedFifo(int capacity, int appId, int channelId);
	virtual ~WindowedFifo();

	int uncond_enqueue(void *data, int len, int srcRank, int *stopCondition);
	int uncond_dequeue(void *data, int len, int *stopCondition);

	int enqueue_start(void **data, int len, int *stopCondition);
	int enqueue_end(void *data, int srcRank);
	int dequeue_start(void **data, int len, int *stopCondition);
	int dequeue_end();
};

#endif /* WINDOWEDFIFO_H_ */
