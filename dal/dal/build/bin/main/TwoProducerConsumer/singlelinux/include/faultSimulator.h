#ifndef FAULTSIMULATOR_H
#define FAULTSIMULATOR_H

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "sleep.h"
#include "errors.h"
#include "wrap.h"

#ifndef MSGCNTL
#define MSGCNTL 1

typedef struct msgCntl {
        int             type; //!< type of the message (MSGCNTL)
        int             appID; //!< sender or receiver appID of the message (>=0)
        int             processID; //!< sender or receiver processID of the message (>=0)
        int             location; //!< sender or receiver location of the message
        long            message; //!< contents of the message (STARTP ... ACK or arbitrary message>=0)
} msgCntl;
#endif

#define FAULTY_CORE_MESSAGE_TYPE -1
#define RECOVERED_CORE_MESSAGE_TYPE -2

void *faultSimulator(wrapDALProcess *slave);

#endif
