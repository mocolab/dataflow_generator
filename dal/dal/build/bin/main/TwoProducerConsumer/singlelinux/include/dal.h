/*! \file dal.h
 \brief Define the DAL process handler scheme.

 - Local variables are defined in structure LocalState. Local
 variables may vary from different processes.
 - The ProcessInit function pointer points to a function which
 initializes a process.
 - The ProcessFire function pointer points to a function which
 performs the actual computation. The communication between
 processes is inside the ProcessFire function.
 - The WPTR is a placeholder for callback. One can just
 leave it blank.

 \authors Lothar Thiele, Lars Schor, Devendra Rai
 */

/************************************************************************
 * do not add code to this header
 ************************************************************************/

#ifndef DAL_H
#define DAL_H

#define PROCESS_POSIX_T		0
#define PROCESS_OPENCL_T	1

#include "dalSupport.h"

//structure for local memory of process
typedef struct _local_states *LocalState;

//process handler
struct _process;

//additional behavioral functions could be declared here
typedef void (*ProcessInit)(struct _process*);
typedef void (*ProcessFinish)(struct _process*);
typedef int (*ProcessFire)(struct _process*);
typedef void *WPTR;

typedef struct param_str {
	int parameter_1;
	int parameter_2;
	char name[32];
} params;

typedef struct _process {
	LocalState local;
	ProcessInit init;
	ProcessFire fire;
	ProcessFinish finish;
	params parameters;
	WPTR wptr; //placeholder for wrapper instance
} DALProcess;

//process interfaces
extern int DAL_skip(void *port, int len, DALProcess *p);
extern int increment_FIFO_Size(void *port, unsigned inc, DALProcess* p);
extern int DAL_send_event(void *message, DALProcess *p);
extern int *createPort(int *port, int base, int number_of_indices,
		int index_range_pairs, ...);
extern int DAL_getIndex(int dimension, DALProcess *p);
extern int DAL_printf(const char *in, ...);

// Video displaying interface
extern void *DAL_fb_new(int width, int height, int mode); /* Generates a new video frame */
extern void DAL_fb_update(void *buffer); /* Updates the current video frame */
extern void DAL_fb_free(void *buffer); /* Updates the current video frame */

// checkpointing interface
extern void DAL_create_checkpoint(void *buf, int len, char* name,
		DALProcess *p);
extern int DAL_read_checkpoint(void *buf, int len, char* name, DALProcess *p);
extern void DAL_destroy_checkpoint(char* name, DALProcess *p);

#define CREATEPORTVAR(name) \
    int name

#define CREATEPORT(port, base, number_of_indices, index_range_pairs...) \
    createPort(&port, base, number_of_indices, index_range_pairs)

#define GETINDEX(dimension) \
		DAL_getIndex(dimension, p)
#endif
