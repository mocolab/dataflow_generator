/*
 * InnerProcessorFifo.h
 *
 *  Created on: May 4, 2012
 *      Author: lschor
 */

#ifndef INNERPROCESSORFIFO_H_
#define INNERPROCESSORFIFO_H_

#include "Fifo.h"

class InnerProcessorFifo: public Fifo {
protected:
	void enqueueDataInSafeFifo(void *data, int len);
	void broadcastDequeue(int amount);
public:
	InnerProcessorFifo(int capacity, int appId, int channelId);
	virtual ~InnerProcessorFifo();
};

#endif /* INNERPROCESSORFIFO_H_ */
