/*! \file
 \brief Declares data structure and functions of fifo.c .

 \authors Lothar Thiele, Lars Schor, Devendra Rai
 */

#ifndef FIFO_H
#define FIFO_H

#define CHANNEL_TYPE_FIFO 0
#define CHANNEL_TYPE_WFIFO 1

#include <pthread.h>

//! Basic data structure of a fifo.
class Fifo {
protected:
	unsigned char *buffer; //! pointer to buffer which contains fifo data
	int capacity; //! capacity of fifo in Bytes (unsigned char)
	int size; //! current number of stored Bytes (unsigned char)
	int in; //! buffer location of first unused Byte (for writing)
	int out; //! buffer location of first used Byte (for reading)

	unsigned char *readBuf;
	unsigned char *writeBuf;
	int writeBufSize;
	int readBufSize;
	int numWrites;

	int appId; //! application id the fifo belongs to
	int channelId; //! channel id of the fifo

	pthread_mutex_t mutex; //! mutex for fifo access

	virtual void enqueueDataInSafeFifo(void *data, int len) = 0;
	virtual void broadcastDequeue(int amount) = 0;

public:

	pthread_cond_t cond_full; //! signal if fifo is not full anymore
	pthread_cond_t cond_empty; //! signal if fifo is not empty anymore

	Fifo() {};
	Fifo(int capacity, int appId, int channelId);
	virtual ~Fifo();
	typedef struct valueaddedbuffer_str {
		char caller[64]; //maximum size of caller name
		void* data;
	} valueaddedbuffer;

	virtual int uncond_enqueue(void *data, int len, int srcRank, int *stopCondition);
	virtual int uncond_enqueue_NB(void *data, int len, int srcRank); //non-blocking form
	virtual int uncond_dequeue(void *data, int len, int *stopCondition);
    virtual int uncond_dequeue_NB(void *data, int len); //non-blocking form
	virtual int uncond_dequeue_withoutdata(int len);

	virtual int getSlackAndStartBufferCopy();
	virtual char *getBufferStartAddress();
	virtual void completeBufferCopy(int len);
	virtual void AdjustFIFO(unsigned NewFIFOCapacity, unsigned char* newFIFOptr);

	virtual int enqueue_start(void **data, int len, int *stopCondition);
	virtual int enqueue_end(void *data, int srcRank);
	virtual int dequeue_start(void **data, int len, int *stopCondition);
	virtual int dequeue_end();

	int getSize();
	int getCapacity();
	void setSize(int newSize);
	void addReadAmount(int amount);

	unsigned char *getReadBuffer();
	unsigned char *getWriteBuffer();

	void setWriteBufSize(int size);
	int getWriteBufSize();

	void setReadBufSize(int size);
	int getReadBufSize();

	void setNumWrites(int num);
	int getNumWrites();

	int getAppId();
	int getChannelId();
};

#endif
