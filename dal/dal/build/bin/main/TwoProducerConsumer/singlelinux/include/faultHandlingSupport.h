/*! \file faultHandlingSupport.h
 \brief Define functions for fault tolerance

 \authors Devendra Rai, Lothar Thiele, Lars Schor
 */

#ifndef FAULT_HANDLING_SUPPORT_H
#define FAULT_HANDLING_SUPPORT_H

#include "dal.h"

int increment_FIFO_Size(void *port, unsigned inc, DALProcess* p);

#endif
