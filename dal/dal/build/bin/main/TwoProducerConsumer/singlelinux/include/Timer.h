#ifndef _TIMER_H_
#define _TIMER_H_

#include <iostream>
#include <sys/time.h>

class Timer {
	public:
	Timer();
	~Timer();
	void start();
	void stop();
	void print(int appId, int processId);
	void reset();

	private:
	timeval mUserStart;
	timeval mUserEnd;
	timeval mSysStart;
	timeval mSysEnd;

	bool _hasStarted;
};

#endif
