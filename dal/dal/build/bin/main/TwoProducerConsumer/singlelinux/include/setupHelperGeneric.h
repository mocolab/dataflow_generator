#ifndef SETUPHELPERGENERIC_H
#define SETUPHELPERGENERIC_H

#include "wrap.h"
#include <string>

Fifo *installLocalMasterFifo(int capacity, int appId, int channelId);

wrapDALProcess* createAndStartSlave(int appId, int processId, int location, int core, Fifo*** PChannel, char* pathToLib);
wrapDALProcess* createAndStartMaster(int appId, int processId, int core, char* pathToLib);
void killSlave(wrapDALProcess* p);
void killMaster(wrapDALProcess* p);

bool hasEnding (std::string const &fullString, std::string const &ending);
void deleteTemporaryCheckpointingFiles();

#endif
