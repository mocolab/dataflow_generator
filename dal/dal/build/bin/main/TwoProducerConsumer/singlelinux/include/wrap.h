/*! \file
\brief Wrapper data structure of a process 

wrapDALProcess contains all information about
a process. The slave controller maintains an array
that contains pointers to data structures of all
its processes.

\authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#ifndef WRAP_H
#define WRAP_H

#include <pthread.h>
#include <map>

#include "dal.h"
#include "backend.h"
#include "global_definitions.h"
#include "Fifo.h"

#ifdef PERFORMANCE_ANALYSIS
#include "Timer.h"
#endif

//! Wrapper data structure of a process
typedef struct wrapDALProcess {
    int*            index; //!< the indeces of the process
    int             appID; //!< the app ID of process's application (>= 0)
    int             processID; //!< the process ID of the process (>= 0)
    int             location; //!< the location of the process (>= 1)
    void            *portToChannel[MAX_NUMBER_PORTS + 1]; //!< pointer to channels for each port
    int				portToChannelType[MAX_NUMBER_PORTS]; //!< port type (in or out)
    pthread_t       thread; //!< pointer to thread of process
    DALProcess      *process; //!< pointer to DALProcess data structure
    void            *lib; //!< pointer to library of process
    void*           (*start)(DALProcess *); //!< pointer to start function wrap_start
    void*           (*resume)(DALProcess *); //!< pointer to resume function wrap_resume
    DALProcess*     (*install)(void*); //!< pointer to install function wrap_install
    void*           (*kill)(DALProcess *); //!< pointer to kill function wrap_kill
    void*			subWrap; 
    int             pause; //!< pause=!=0 causes process to pause
    int             stop; //!< stop=!=0 causes process to stop
    pthread_mutex_t mutex_pause; //! mutex for pause access
    pthread_cond_t  cond_pause; //! signal if pause is not any more set
    int				core; //! core on which the thread should run
    int             rank; //!< rank of the process
    int             priority; //!< 1 if the process should have a high priority
    int 			processType; //!< PROCESS_POSIX_T or PROCESS_OPENCL_T
#ifdef PERFORMANCE_ANALYSIS
    Timer           *timer; //!< timer to measure the execution time of the process
#endif
} wrapDALProcess;

//! Wrapper data structure of a process
typedef struct wrapProcess {
    char            name[MAX_STRINGLENGTH]; //!< the name of the process
    char            basename[MAX_STRINGLENGTH]; //!< the basename of the process
    char			appname[MAX_STRINGLENGTH]; //!< the name of the application the process belongs to
    OS_info			osInfo;
} wrapProcess;

#include "wrapController.h"

extern "C" DALProcess *wrap_install(void* ProcessInstallParameters);
extern "C" void *wrap_kill(DALProcess *process);
extern "C" void *wrap_start(DALProcess *process);
extern int DAL_send_ack(DALProcess *p);

//process macros
int getIndex(const char* string, char* tokens, int indexNumber);

#endif
