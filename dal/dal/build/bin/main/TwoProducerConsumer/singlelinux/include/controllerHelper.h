#ifndef CONTROLLERHELPER_H
#define CONTROLLERHELPER_H

#include "dal.h"

void startDALProcess(int appID, int processID, DALProcess *p);
void stopDALProcess(int appID, int processID, DALProcess *p);
void resumeDALProcess(int appID, int processID, DALProcess *p);
void pauseDALProcess(int appID, int processID, DALProcess *p);
void installDALProcess(int appID, int processID, int location, int processType, int taskparallelism, int dataparallelism, DALProcess *p);
void linkDALChannel(int appID, int processID, int port, int porttype, int size, int channelID, DALProcess *p);
void installDALLocalChannel(int appID, int channelID, int capacity, int tokensize, int initialTokens, int channelType, DALProcess *p);
void installDALInChannel(int appID, int channelID, int capacity, int tokensize, int peerLocation, int peerRank, int channelType, DALProcess *p);
void installDALOutChannel(int appID, int channelID, int capacity, int tokensize, int peerLocation, int peerRank, int channelType, DALProcess *p);
void killDALChannel(int appID, int channelID, DALProcess *p);
void killScheduler(DALProcess *p);

#endif
