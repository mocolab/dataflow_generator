/*! \file
 \brief additional functions for DAL

 \authors Lothar Thiele, Lars Schor, Devendra Rai
 */

#include <stdio.h>
#include <stdarg.h>

#include "dal.h"
#include "wrap.h"
#include "errors.h"

/**
 * Gets an index of a string, where the index must be separated by
 * a character specified in tokens.
 * Returns -1, when an error occurs.
 *
 * Example:
 * getIndex("name_1_2", "_", 0) will return 1.
 * getIndex("name_1_2", "_", 1) will return 2.
 *
 * \param string string to parse
 * \param tokens delimiter of indices
 * \param indexNumber position of index (starting at 0)
 */
int getIndex(const char* string, char* tokens, int indexNumber) {
	char* string_copy;
	char* token_pointer;
	int index = 0;

	string_copy = (char*) malloc(sizeof(char) * (strlen(string) + 1));
	if (!string_copy) {
		error_abort("getIndex(): could not allocate memory.");
		return -1;
	}

	strcpy(string_copy, string);

	token_pointer = strtok(string_copy, tokens);
	do {
		token_pointer = strtok(NULL, tokens);
		index++;
	} while (index <= indexNumber && token_pointer != 0);

	if (token_pointer) {
		index = atoi(token_pointer);
		free(string_copy);
		return index;
	}

	free(string_copy);
	return -1;
}

/**
 * Create the port name of an iterated port based on its basename and the
 * given indices.
 *
 * \param port buffer where the result is stored (created using
 *             CREATEPORTVAR)
 * \param base basename of the port
 * \param number_of_indices number of dimensions of the port
 * \param index_range_pairs index and range values for each dimension
 */
int *createPort(int *port, int base, int number_of_indices,
		int index_range_pairs, ...) {
	int index[4];
	int range[4];
	int i;
	int value;

	va_list marker;
	va_start(marker, index_range_pairs);

	value = index_range_pairs;
	for (i = 0; i < number_of_indices; i++) {
		index[i] = value;
		value = va_arg(marker, int);
		range[i] = value;
		if (i < number_of_indices - 1) {
			value = va_arg(marker, int);
		}
	}

	*port = base;
	for (i = 0; i < number_of_indices; i++) {
		int j;
		int weight = 1;
		for (j = 1; j < number_of_indices - i; j++) {
			weight *= range[j];
		}
		*port += index[i] * weight;
	}

	return port;
}

/**
 * Get the index of the DAL process when using iterators
 *
 * \param dimension index dimension
 * \param p DAL process
 * \return the index
 */
int DAL_getIndex(int dimension, DALProcess *p) {
	return ((wrapDALProcess*) (p->wptr))->index[dimension];
}
