/*! \file
\brief Contains methods for fault simulation

\authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#include "faultSimulator.h"

//! Creates a simulator for faulty processors
/*!
A file is created that can be used to simulate any errors in the system.
*/
void *faultSimulator(wrapDALProcess *slave) {

	char fileName[30];
	char numberBuf[10];
	sprintf(numberBuf,"%d",slave->processID);

	strcpy(fileName, "faults_slave_");
	strcat(fileName, numberBuf);
	strcat(fileName, ".txt");

    // Create a file that can be used by the user to simulate faults
    FILE * faultyFile;
    faultyFile = fopen (fileName,"w+");

    if (faultyFile == NULL) {
        err_abort(0,"file for fault handling could not be created.");
    }

    fclose(faultyFile);

    //Only stop if the slave is stopped
    while (slave->stop == 0) {

        // obtain file size
    	faultyFile = fopen (fileName,"r");
        rewind (faultyFile);
        fseek (faultyFile , 0 , SEEK_END);
        long lSize = ftell (faultyFile);
        rewind (faultyFile);

        // If the file is not any more empty, we have a new number
        if (lSize > 0) {

            // read the core number
            char *buffer = (char*) malloc (sizeof(char)*lSize);
            size_t result;
            result = fread (buffer, 1, lSize, faultyFile);
            if (result != (unsigned long) lSize) {
            	fputs ("Reading error in the fault simulator", stderr);
            	exit (3);
            }

            int core = atoi(buffer);
            free(buffer);

            // reopen the file to clear it
            faultyFile = freopen(fileName, "w+", faultyFile);
            if (faultyFile == NULL) {
            	err_abort(0,"file for fault handling is blocked.");
			}

            // Send the information to the master
            msgCntl Cntl;
            Fifo *channel  = (Fifo *) slave->portToChannel[((wrapController *) slave->subWrap)->eventPortID];

            // A core is recovered
            if (core >= 0) {
                Cntl.message = RECOVERED_CORE_MESSAGE_TYPE;

#ifdef DEMO
                printf("HW-message: core %d is again available.\n", core); fflush(stdout);
#endif

            }
            // A core is faulty
            else {
                core *= -1;
                Cntl.message = FAULTY_CORE_MESSAGE_TYPE;

#ifdef DEMO
                printf("HW-message: core %d became faulty.\n", core); fflush(stdout);
#endif
            }

            Cntl.location = core;
            Cntl.type = MSGCNTL;
            Cntl.appID = slave->appID;
            Cntl.processID = slave->processID;

            int write = 0;
            int stopSignal = 0;
           	while (write < (int) sizeof(msgCntl)) {
           		write = channel->uncond_enqueue(&Cntl, sizeof(msgCntl), slave->rank, &stopSignal);
           	}
            //channel->enqueue(&Cntl, sizeof(msgCntl));
        }

        // Sleep a few milliseconds to not fully utilize the processor
        fclose(faultyFile);

        sleepMilliseconds(30, slave);
    }

    return NULL;
}
