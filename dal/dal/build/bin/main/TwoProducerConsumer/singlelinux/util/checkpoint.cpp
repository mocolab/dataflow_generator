#include "checkpoint.h"

void DAL_create_checkpoint(void *buf, int len, char* name, DALProcess *p) {

    wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

    char fileName[200];
    sprintf(fileName,"%d_%s.cpt", DALprocess->appID, name);

    std::ofstream myFile (fileName, std::ios::out | std::ios::binary);
    myFile.write ((char *) buf, len);

    myFile.close();
}

int DAL_read_checkpoint(void *buf, int len, char* name, DALProcess *p) {

    wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

    char fileName[200];
    sprintf(fileName,"%d_%s.cpt", DALprocess->appID, name);

    std::ifstream myFile (fileName, std::ios::in | std::ios::binary | std::ios::ate);

    // Check if the file exists
    if (myFile) {
        long size = myFile.tellg(); 

        if (len == size) {
            myFile.seekg (0, std::ios::beg);
            myFile.read ((char*) buf, size);
            myFile.close();
            return len; 
        } else {
        	myFile.close();
            return 0; 
        }

    } else {
        return 0; 
    }
}

void DAL_destroy_checkpoint(char* name, DALProcess *p) {
    wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

    char fileName[200];
    sprintf(fileName,"%d_%s.cpt", DALprocess->appID, name);

    remove(fileName); 
}
