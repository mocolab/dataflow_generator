#include <stdio.h>
#include <string.h>

#include <pthread.h>
#include <dlfcn.h>

#include "errors.h"
#include "controllerHelper.h"
#include "wrap.h"
#include "Fifo.h"
#include "InnerProcessorFifo.h"
#include "WindowedFifo.h"

//! Link a channel to a port of a process.
/*!
 The function links the channel by storing the pointer to the
 channel into the portToChannel[] array which is part of the
 wrapper data structure (see wrap.h ). The function returns
 without blocking.
 \param appID is the ID (0 <= ID < NUMBER_OF_APPS)
 of the process' application whose port should be linked.
 \param processID is the ID (0 <= ID < MAX_NUMBER_PROCESSES)
 of the process whose port should be linked.
 \param myProcesses[] contains the wrapper data structures
 of all processes at this location.
 \param port contains the port number which is linked where
 (0 <= port < MAX_NUMBER_PORTS).
 \param channel is the pointer to the channel that is linked
 (it is actually the pointer to the fifo which implements the channel).
 */
void linkDALChannel(int appID, int processID, int port, int porttype,
		int numtokens, int channelID, DALProcess *slaveP) {

	wrapController* slave =
			(wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;

	wrapDALProcess* myProcess = slave->myProcesses[appID][processID];
#ifdef DEBUG
	if (myProcess == NULL) error_abort("attempt to link a channel to an uninitialized process");
#endif
	myProcess->portToChannel[port] = slave->PChannel[appID][channelID];

	myProcess->portToChannelType[port] = porttype;
}

//! Installs a fifo by allocating a fifo.
/*!
 The function installs a fifo by allocating the necessary
 memory and initiating the data structure.
 \param fifoID is the ID of the fifo that is installed, i.e. the index
 where the channel is found in Channel[].
 \param Channel[] is an array which contains the pointer to the
 channel fifos and Channel[fifoID] points to the newly generated
 fifo after execution of the function.
 \param capacity is the capacity of the channel-fifo in Bytes.
 */
void installDALLocalChannel(int appID, int fifoID, int capacity,
		int tokensize, int initialTokens, int channelType, DALProcess *slaveP) {


	wrapController* slave =
			(wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;

	pthread_mutex_lock(slave->mutex_install);

	if (slave->PChannel[appID][fifoID] == NULL) {

		Fifo *newFifo;
		if (channelType == CHANNEL_TYPE_FIFO) {
			newFifo = new InnerProcessorFifo(capacity * tokensize, appID, fifoID);
		} else if (channelType == CHANNEL_TYPE_WFIFO) {
			newFifo = new WindowedFifo(capacity, appID, fifoID);
		} else {
			errno_abort("channel type is not supported.");
		}

		if (newFifo == NULL) {
			errno_abort ("fifo could not be allocated.");
		}

		slave->PChannel[appID][fifoID] = newFifo;
	}

	pthread_mutex_unlock(slave->mutex_install);
}

//! Start a process by executing wrap_start() in wrap_process.c as a thread.
/*!
 The thread in myProcesses[processID] must not run if startDALProcess
 is called. Otherwise, there will be an undefined behavior. The code
 attempts to stop a running thread by setting pause to 1. But if
 the thread still runs, the function will block forever on a pthread_join.
 The function executes  wrap_start() in wrap_process.c
 as a thread and returns immediately. The thread executes the init() function of the
 corresponding process once and then repetitively the fire() function.
 The function returns directly after having started the thread.
 \param processID is the ID (0 <= ID < MAX_NUMBER_PROCESSES)
 of the process which is started.
 \param myProcesses[] contains the wrapper data structures
 of all processes at this location.
 */
void startDALProcess(int appID, int processID, DALProcess *slaveP) {

#ifdef DEBUG
	wrapController* slave = (wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;
	wrapDALProcess* myProcess = slave->myProcesses[appID][processID];

	if (slave->myProcesses[appID][processID] == NULL) error_abort("attempt to start an uninitialized process");
#endif

	resumeDALProcess(appID, processID, slaveP);
}

//! Kill a process.
/*!
 The function at first sets the pause field in the wrapper data structure
 of the process to 1. Therefore, the fire function will no longer
 be executed and the running thread will exit. The function blocks
 until the thread has exited (and as a consequence, it will block
 infinitely if the fire function does not finish for some reason).
 Then, the function wrap_kill from wrap_process.c is executed. wrap_kill
 executes the finish() function of the process and then frees process resources.
 Finally, resources related to the wrapper data structure of the
 process are freed and the function returns. In summary, the function
 returns after the process has been successfully killed.
 \param processID is the ID (0 <= ID < MAX_NUMBER_PROCESSES)
 of the process which is started.
 \param myProcesses[] contains the wrapper data structures
 of all processes at this location.
 */
void stopDALProcess(int appID, int processID, DALProcess *slaveP) {
	// Get the slave process and the actual process
	wrapController* slave =
			(wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;
	wrapDALProcess* myProcess = slave->myProcesses[appID][processID];

#ifdef DEBUG
	if (myProcess == NULL) error_abort("attempt to stop an uninitialized process");
#endif

	// Set the stop flag
	myProcess->stop = 1;

	// If the process is currently paused, we first have to resume it!
	if (myProcess->pause == 1) {
		myProcess->pause = 0;
		pthread_cond_broadcast(&(myProcess->cond_pause));
	}

	// Unlock all FIFOs
	for (int port = 0; port < MAX_NUMBER_PORTS; port++) {
		if (myProcess->portToChannel[port] != NULL) {
			if (myProcess->portToChannelType[port] == 0) {
				// in FIFO
				void * tmp = myProcess->portToChannel[port];
				pthread_cond_broadcast(&(((Fifo *)tmp)->cond_empty));
			} else if (myProcess->portToChannelType[port] == 1) {
				// out FIFO
				void * tmp = myProcess->portToChannel[port];
				pthread_cond_broadcast(&(((Fifo *)tmp)->cond_full));
			} else {
				errno_abort("Channel is of neither in nor out type");
			}
		}
	}

	// Wait until the process is stopped
	pthread_join(myProcess->thread, NULL);

	// Cleanup
	(*myProcess->kill)(myProcess->process);

	// Kill the mutex and signal for pausing
	pthread_mutex_destroy(&(myProcess->mutex_pause));
	pthread_cond_destroy(&(myProcess->cond_pause));

	// Cleanup the memory
	free(myProcess->index);
	dlclose(myProcess->lib);
	free(myProcess->subWrap);
	free(myProcess);
	slave->myProcesses[appID][processID] = NULL;
}

//! Pause a process.
/*!
 The function at first sets the pause field in the wrapper data structure
 of the process to 1. Therefore, the fire function will no longer
 be executed and the running thread will exit. The function blocks
 until the thread has exited (and as a consequence, it will block
 infinitely if the fire function does not finish for some reason).
 The function returns after the process has been successfully paused.
 \param processID is the ID (0 <= ID < MAX_NUMBER_PROCESSES)
 of the process which is started.
 \param myProcesses[] contains the wrapper data structures
 of all processes at this location.
 */
void pauseDALProcess(int appID, int processID, DALProcess *slaveP) {

	wrapController* slave =
			(wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;
	wrapDALProcess* myProcess = slave->myProcesses[appID][processID];

#ifdef DEBUG
	if (myProcess == NULL) error_abort("attempt to pause an uninitialized process");
#endif

	pthread_mutex_lock(&(myProcess->mutex_pause));
	myProcess->pause = 1;
	pthread_mutex_unlock(&(myProcess->mutex_pause));
}

//! Resume a process.
/*!
 The process should not run (fire()) if the function is called.
 As a safeguard, the function at first sets the pause field in
 the wrapper data structure
 of the process to 1. Therefore, the fire function will no longer
 be executed and the running thread will exit. The function blocks
 until the thread has exited (and as a consequence, it will block
 infinitely if the fire function does not finish for some reason).
 Then the function creates a thread that calls wrap_resume in
 wrap_process.c which repetitively calls the fire() method of the
 process.
 \param processID is the ID (0 <= ID < MAX_NUMBER_PROCESSES)
 of the process which is started.
 \param myProcesses[] contains the wrapper data structures
 of all processes at this location.
 */
void resumeDALProcess(int appID, int processID, DALProcess *slaveP) {

	wrapController* slave =
			(wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;
	wrapDALProcess* myProcess = slave->myProcesses[appID][processID];

#ifdef DEBUG
	if (myProcess == NULL) error_abort("attempt to resume an uninitialized process");
#endif

	pthread_mutex_lock(&(myProcess->mutex_pause));
	myProcess->pause = 0;
	pthread_mutex_unlock(&(myProcess->mutex_pause));

	pthread_cond_broadcast(&(myProcess->cond_pause));
}

void installDALProcess(int appID, int processID, int location, int type,
		int taskparallelism, int dataparallelism, DALProcess *slaveP) {

	wrapDALProcess* slave = (wrapDALProcess*) slaveP->wptr;
	wrapController* slaveControl = (wrapController*) slave->subWrap;
	static params ProcessinstallParameters;
	int i;
	void *lib;
	int err;
	char str[MAX_STRINGLENGTH];
	wrapDALProcess *DALprocess;
	wrapProcess * DALprocessSub;

	// allocate memory for the wrapper data structure
	DALprocess = (wrapDALProcess *) malloc(sizeof(wrapDALProcess));
	if (DALprocess == NULL) {
		errno_abort ("wrapDALProcess could not be allocated.");
	}

	DALprocessSub = (wrapProcess *) malloc(sizeof(wrapProcess));
	DALprocess->subWrap = DALprocessSub;
	slaveControl->myProcesses[appID][processID] = DALprocess;

	// initialize the wrapper data structure
	strcpy(DALprocessSub->name, slaveControl->processname[appID][processID]);
	strcpy(DALprocessSub->basename,
			slaveControl->processbasename[appID][processID]);
	strcpy(DALprocessSub->appname, slaveControl->appname[appID]);

	//initialize the index array
	DALprocess->index = (int *) malloc(4 * sizeof(int));
	for (i = 0; i < 4; i++) {
		DALprocess->index[i] = getIndex(DALprocessSub->name, (char *) "_", i);
	}

	DALprocess->location = location;
	DALprocess->core = slaveControl->locationToCoreMap->find(location)->second;
	DALprocess->processID = processID;
	DALprocess->appID = appID;
	DALprocess->rank = slave->rank;
	DALprocess->priority = 0;

	for (i = 0; i < MAX_NUMBER_PORTS; i++) {
		DALprocess->portToChannel[i] = NULL;
	}
	DALprocess->pause = 1;
	DALprocess->stop = 0;

	//initialize the pausing variables
	pthread_mutex_init(&(DALprocess->mutex_pause), NULL);
	pthread_cond_init(&(DALprocess->cond_pause), NULL);

	// link to the dynamic library
	strcpy(str, "lib/");
	strcat(str, DALprocessSub->appname);
	strcat(str, "/lib");
	strcat(str, DALprocessSub->basename);
	strcat(str, ".so");

	lib = dlopen(str, RTLD_LAZY);
	if (!lib) {
		fprintf(stderr, "Error opening dynamic library \"%s\": %s\n", str, dlerror());
		abort();
	}
	DALprocess->lib = lib;

	// get function pointer to wrap_install
	strcpy(str, "wrap_install");
	DALprocess->install = (DALProcess* (*)(void*)) dlsym(lib, str);
	char *lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// get function pointer to wrap_kill
	strcpy(str, "wrap_kill");
	DALprocess->kill = (void* (*)(DALProcess *)) dlsym(lib, str);
	lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// get function pointer to wrap_start
	strcpy(str, "wrap_start");
	DALprocess->start = (void* (*)(DALProcess *)) dlsym(lib, str);
	lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// init the thread variable to NULL
	DALprocess->thread = (pthread_t) NULL;

	// call the method wrap_install of the process defined in wrap_process.c;
	// it returns the DALProcess structure of the process
	ProcessinstallParameters = slave->process->parameters;
	
	DALprocess->process = (DALprocess->install) ((void*) (&ProcessinstallParameters));

	// put a pointer to the wrapper data structure in the DALProcess
	// data structure such that the wrapper (who only knows DALProcess)
	// has access to the pause variable (to pause a process)
	(DALprocess->process)->wptr = DALprocess;

	// Add the event channel
	DALprocess->portToChannel[MAX_NUMBER_PORTS]
			= slave->portToChannel[slaveControl->eventPortID];

	// Create the thread and start it (but process is paused!)
	err = pthread_create(&(DALprocess->thread), NULL,
			(void* (*)(void*)) DALprocess->start, (void*) DALprocess->process);
	if (err)
		err_abort(err,"thread could not be created");

}
