
#include "setupHelperGeneric.h"

#include "InnerProcessorFifo.h"
#include "global_definitions.h"
#include "errors.h"
#include "wrap.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <dirent.h>
#include <string>
#include <dlfcn.h>


//! Installs a fifo by allocating a fifo.
/*!
 The function installs a fifo by allocating the necessary
 memory and initiating the data structure.
 \param fifoID is the ID of the fifo that is installed, i.e. the index
 where the channel is found in Channel[].
 \param Channel[] is an array which contains the pointer to the
 channel fifos and Channel[fifoID] points to the newly generated
 fifo after execution of the function.
 \param capacity is the capacity of the channel-fifo in Bytes.
 */
Fifo *installLocalMasterFifo(int capacity, int appId, int channelId) {
	Fifo *newFifo = new InnerProcessorFifo(capacity, appId, channelId);
	if (newFifo == NULL) {
		errno_abort("fifo could not be allocated.");
	}

	return newFifo;
}

wrapDALProcess* createAndStartSlave(int appId, int processId, int location, int core, Fifo*** PChannel,
		char* pathToLib) {

	int i, j;
	char str[MAX_STRINGLENGTH];

	wrapDALProcess* slave = (wrapDALProcess *) malloc(sizeof(wrapDALProcess));
	wrapController* controllWrap = (wrapController *) malloc(
			sizeof(wrapController));
	slave->appID = appId;
	slave->processID = processId;
	slave->location = location;
	slave->core = core;
	slave->priority = 1;
	slave->subWrap = (void *) controllWrap;

	wrapDALProcess ***myProcesses = (wrapDALProcess ***) malloc(
			sizeof(wrapDALProcess **) * NUMBER_OF_APPLICATIONS);
	for (j = 0; j < NUMBER_OF_APPLICATIONS; j++) {
		myProcesses[j] = (wrapDALProcess **) malloc(
				sizeof(wrapDALProcess *) * MAX_NUMBER_PROCESSES);
		for (i = 0; i < MAX_NUMBER_PROCESSES; i++) {
			myProcesses[j][i] = NULL;
		}
	}
	controllWrap->myProcesses = myProcesses;
	controllWrap->PChannel = PChannel;
	controllWrap->locationToCoreMap = new std::map <int, int>();

	slave->pause = 0;
	slave->stop = 0;

	//initialize the pausing variables
	pthread_mutex_init(&(slave->mutex_pause), NULL);
	pthread_cond_init(&(slave->cond_pause), NULL);

	void *lib;
	// link to the dynamic library
	lib = dlopen(pathToLib, RTLD_LAZY);
	if (!lib) {
		errno_abort("open dynamic library");
	}
	slave->lib = lib;

	// get function pointer to wrap_install
	strcpy(str, "wrap_install");
	slave->install = (DALProcess* (*)(void*)) dlsym(lib, str);
	char *lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// get function pointer to wrap_kill
	strcpy(str, "wrap_kill");
	slave->kill = (void* (*)(DALProcess *)) dlsym(lib, str);
	lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// get function pointer to wrap_start
	strcpy(str, "wrap_start");
	slave->start = (void* (*)(DALProcess *)) dlsym(lib, str);
	lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// init the thread variable to NULL
	slave->thread = (pthread_t) NULL;

	// call the method wrap_install of the process defined in wrap_process.c;
	// it returns the DALProcess structure of the process
	slave->process = (slave->install)(NULL);

	// put a pointer to the wrapper data structure in the DALProcess
	// data structure such that the wrapper (who only knows DALProcess)
	// has access to the pause variable (to pause a process)
	(slave->process)->wptr = slave;

	return slave;
}

wrapDALProcess* createAndStartMaster(int appId, int processId, int core, char* pathToLib) {

	char str[MAX_STRINGLENGTH];

	wrapDALProcess* master = (wrapDALProcess *) malloc(sizeof(wrapDALProcess));
	master->appID = appId;
	master->processID = processId;
	master->location = 0;
	master->core = core;
	master->priority = 1;
	master->subWrap = (void *) 0;

	master->pause = 0;
	master->stop = 0;

	//initialize the pausing variables
	pthread_mutex_init(&(master->mutex_pause), NULL);
	pthread_cond_init(&(master->cond_pause), NULL);

	void *lib;
	// link to the dynamic library
	lib = dlopen(pathToLib, RTLD_LAZY);
	if (!lib) {
		fprintf(stderr, "Error opening dynamic library \"%s\": %s\n", pathToLib, dlerror());
		exit(1);
	}
	master->lib = lib;

	// get function pointer to wrap_install
	strcpy(str, "wrap_install");
	master->install = (DALProcess* (*)(void*)) dlsym(lib, str);
	char *lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// get function pointer to wrap_kill
	strcpy(str, "wrap_kill");
	master->kill = (void* (*)(DALProcess *)) dlsym(lib, str);
	lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// get function pointer to wrap_start
	strcpy(str, "wrap_start");
	master->start = (void* (*)(DALProcess *)) dlsym(lib, str);
	lError = dlerror();
	if ((lError) != NULL && (strcmp(lError, "No error") != 0)) {
		errno_abort("reference to dynamic funtion");
	}

	// init the thread variable to NULL
	master->thread = (pthread_t) NULL;

	// call the method wrap_install of the process defined in wrap_process.c;
	// it returns the DALProcess structure of the process
	master->process = (master->install)(NULL);

	// put a pointer to the wrapper data structure in the DALProcess
	// data structure such that the wrapper (who only knows DALProcess)
	// has access to the pause variable (to pause a process)
	(master->process)->wptr = master;

	return master;
}

/**
 *
 * @param p
 */
void killMaster(wrapDALProcess* p) {

	// Set the stop flag (should not be required)
	p->stop = 1;

	// Cleanup function
	int err = pthread_create(&(p->thread), NULL,
			(void* (*)(void*)) p->kill, (void*) p->process);
	if (err)
		err_abort(err, "thread could not be created");
	pthread_join(p->thread, NULL);

    // Kill the mutex and signal for pausing
	pthread_mutex_destroy(&(p->mutex_pause));
	pthread_cond_destroy(&(p->cond_pause));

	// Close the library
	dlclose(p->lib);
	free(p);
}

/**
 *
 * @param p
 */
void killSlave(wrapDALProcess* p) {

	// Get the required variables
	wrapController* controllWrap = (wrapController *) p->subWrap;

	// Set the stop flag (should not be required)
	p->stop = 1;

	// Cleanup function
	int err = pthread_create(&(p->thread), NULL,
			(void* (*)(void*)) p->kill, (void*) p->process);
	if (err)
		err_abort(err, "thread could not be created");
	pthread_join(p->thread, NULL);

    // Kill the mutex and signal for pausing
	pthread_mutex_destroy(&(p->mutex_pause));
	pthread_cond_destroy(&(p->cond_pause));

	// Kill the contains for the processes
	for (int j = 0; j < NUMBER_OF_APPLICATIONS; j++) {
		for (int i = 0; i < MAX_NUMBER_PROCESSES; i++) {
			controllWrap->myProcesses[j][i] = NULL;
		}
		free(controllWrap->myProcesses[j]);
	}
	free(controllWrap->myProcesses);

	// Cleanup other variables
	controllWrap->locationToCoreMap->clear();
	delete controllWrap->locationToCoreMap;

	// Close the library
	dlclose(p->lib);
	free(p->subWrap);
	free(p);
}

bool hasEnding (std::string const &fullString, std::string const &ending) {
	if (fullString.length() >= ending.length()) {
		return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
	} else {
		return false;
	}
}

void deleteTemporaryCheckpointingFiles() {
	DIR *dirp = opendir(".");
	struct dirent *dp;
	while ((dp = readdir(dirp)) != NULL) {
		if (hasEnding(dp->d_name, ".cpt")) {
			remove(dp->d_name);
		}
	}
	(void) closedir(dirp);
}
