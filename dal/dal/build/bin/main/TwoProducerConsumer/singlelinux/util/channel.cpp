/*! \file
 \brief Interfaces to channels as used by processes

 \authors Lothar Thiele, Lars Schor, Devendra Rai, Andreas Tretter
 */

#include <stdio.h> 
#include <stdarg.h>
#include <inttypes.h>

#include "wrap.h"
#include "Fifo.h"
#include "errors.h"
#include "faultHandlingSupport.h"

#if __cplusplus > 201100 //only works with C++11
#include <atomic>
std::atomic_ulong msgcounter(0);

#define CPP11(x...) x
#else
#define CPP11(x...)
#endif //C++11


#ifndef MSGCNTL
#define MSGCNTL 1

typedef struct msgCntl {
	int type; //!< type of the message (MSGCNTL)
	int appID; //!< sender or receiver appID of the message (>=0)
	int processID; //!< sender or receiver processID of the message (>=0)
	int location; //!< sender or receiver location of the message
	long message; //!< contents of the message (STARTP ... ACK or arbitrary message>=0)
} msgCntl;
#endif

//! Writes data to a port of a process.
/*!
 The DAL_write function blocks if there is not enough space in the
 channel to send len Bytes of data. Usually, the channel should
 be allocated, i.e. the pointer to the channel should not be NULL.
 Otherwise there will be an error message in DEBUG mode. An alternative
 would be to do a busy wait until the channel is allocated (see the
 comments in the code below).
 \param port denotes the port of a process (0 <= port < MAX_NUMBER_PORTS);
 \param buf is a pointer to a contiguous piece of memory from which
 data are read
 \param len is the number of Bytes that are transmitted from buf to port
 \param p is a pointer to the process structure (DALProcess)
 */
int channel_write(void *port, void *buf, int len, DALProcess *p) {
	long portId = (long) port;
	void * tmp = ((wrapDALProcess *) p->wptr)->portToChannel[portId];
	int write = 0;

	wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

#ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write is NULL");
    CPP11(ulong msgid = msgcounter++;)
	printf (CPP11("%8lu ") "write %2d %4d %4ld %6d %6d %6d\n", CPP11(msgid,) DALprocess->appID, DALprocess->processID, portId, len, ((Fifo *) tmp)->getSize(), ((Fifo *) tmp)->getCapacity());
    fflush(stdout);
#endif

#ifdef FAULTHANDLING_DUPLICATION
	//decide whether to perform a blocking or a non-blocking call, depending on the caller.
	/* If the call comes from Splittermanager, then the read must be non-blocking, returning -1 on failure*/
	if ((strcmp((char*) (p->parameters.name), "splittermanager-FT") == 0)) {
		/*Attempt to shove in data */
		//printf ("\n Splittermanager writing data into FIFO: %p. Called from Rank: %d\n", tmp, DALprocess->rank);
		write = ((Fifo *) tmp)->uncond_enqueue_NB(buf, len, DALprocess->rank);

		/* requires number of bytes are not available. */
		if (write == WRITE_INCOMPLETE) {
			//printf ("\nDAL_write from splittermanager: FIFO full\n");
			return (WRITE_INCOMPLETE);
		} else {
			//printf ("\nDAL_write from splittermanager: FIFO available\n");
			return (write);
		}
	}
#endif

	while ((DALprocess->stop == 0) && write < len) {
		write = ((Fifo *) tmp)->uncond_enqueue(buf, len, DALprocess->rank, &(DALprocess->stop));

		pthread_mutex_lock(&(DALprocess->mutex_pause));
		while (DALprocess->pause == 1) {
			pthread_cond_wait(&(DALprocess->cond_pause),
					&(DALprocess->mutex_pause));
		}
		pthread_mutex_unlock(&(DALprocess->mutex_pause));
	}

#ifdef DEBUG
    CPP11(printf("%8lu finished\n", msgid); fflush(stdout));
#endif

	if (DALprocess->stop != 0) {
		pthread_exit (NULL);
	}

	return (0);
}

//! Reads data from a port of a process.
/*!
 The DAL_read function blocks if there are less then len Bytes
 in the channel. Usually, the channel should
 be allocated, i.e. the pointer to the channel should not be NULL.
 Otherwise there will be an error message in DEBUG mode. An alternative
 would be to do a busy wait until the channel is allocated (see the
 comments in the code below).
 \param port denotes the port of a process (0 <= port < MAX_NUMBER_PORTS);
 \param buf is a pointer to a contiguous piece of memory to which
 data are written
 \param len is the number of Bytes that are read from port to buf
 \param p is a pointer to the process structure (DALProcess)
 */
int channel_read(void *port, void *buf, int len, DALProcess *p) {
	long portId = (long) port;
	void * tmp = ((wrapDALProcess *) p->wptr)->portToChannel[portId];
	int read = 0;

	wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

#ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write is NULL");
    CPP11(ulong msgid = msgcounter++;)
	printf (CPP11("%8lu ") "read  %2d %4d %4ld %6d %6d %6d\n", CPP11(msgid,) DALprocess->appID, DALprocess->processID, portId, len, ((Fifo *) tmp)->getSize(), ((Fifo *) tmp)->getCapacity());
    fflush(stdout);
#endif

#ifdef FAULTHANDLING_DUPLICATION
	//decide whether to perform a blocking or a non-blocking call, depending on the caller.
	/* If the call comes from join process, then the read must be non-blocking, returning -1 on failure*/
	/* Read on port 0 must be blocking */
	if ((strcmp((char*) (p->parameters.name), "join-FT") == 0)) {
		/*Attempt to shove in data */
		//printf ("\n Splittermanager writing data into FIFO: %p", tmp);
		read = ((Fifo *) tmp)->uncond_dequeue_NB(buf, len);

		/* requires number of bytes are not available. */
		if (read == READ_INCOMPLETE) {
			//printf ("\nDAL_write from splittermanager: FIFO full\n");
			return (READ_INCOMPLETE);
		} else {
			//printf ("\nDAL_write from splittermanager: FIFO available\n");
			return (read);
		}
	}
#endif

	while ((DALprocess->stop == 0) && read < len) {
		read = ((Fifo *) tmp)->uncond_dequeue(buf, len, &(DALprocess->stop));

		pthread_mutex_lock(&(DALprocess->mutex_pause));
		while (DALprocess->pause == 1) {
			pthread_cond_wait(&(DALprocess->cond_pause),
					&(DALprocess->mutex_pause));
		}
		pthread_mutex_unlock(&(DALprocess->mutex_pause));
	}
    
#ifdef DEBUG
    CPP11(printf("%8lu finished\n", msgid); fflush(stdout));
#endif

	if (DALprocess->stop != 0) {
		pthread_exit (NULL);
	}

	return (0);
}

void *channel_read_begin(int port, int tokensize, int tokenrate, DALProcess *p) {

	wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

	void *tmp = ((wrapDALProcess *)p->wptr)->portToChannel[port];
	int len = tokenrate * tokensize;
	void *buf;
    
#ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write_begin is NULL");
    CPP11(ulong msgid = msgcounter++;)
	printf (CPP11("%8lu ") "bread  %2d %4d %4ld %6d %6d %6d\n", CPP11(msgid,) DALprocess->appID, DALprocess->processID, port, 
        tokenrate*tokensize, ((Fifo *) tmp)->getSize(), ((Fifo *) tmp)->getCapacity());
    fflush(stdout);
#endif

	int read = 0;
	while ((DALprocess->stop == 0) && read < len) {

		read = ((Fifo *) tmp)->dequeue_start(&buf, len, &(DALprocess->stop));

		pthread_mutex_lock(&(DALprocess->mutex_pause));
		while (DALprocess->pause == 1) {
			pthread_cond_wait(&(DALprocess->cond_pause),
					&(DALprocess->mutex_pause));
		}
		pthread_mutex_unlock(&(DALprocess->mutex_pause));
	}
    
#ifdef DEBUG
    CPP11(printf("%8lu finished\n", msgid); fflush(stdout));
#endif
	if (DALprocess->stop != 0) {
		pthread_exit (NULL);
	}

	return buf;
}

void channel_read_end(int port, void *buf, DALProcess *p)
{
	void *tmp = ((wrapDALProcess *)p->wptr)->portToChannel[port];

    #ifdef DEBUG
	wrapDALProcess *DALprocess = (wrapDALProcess *) p->wptr;
	if (tmp == NULL) error_abort("Queue pointer in DAL_write_begin is NULL");
	printf (CPP11("%8lu ") "eread  %2d %4d %4ld %6d %6d %6d\n", CPP11(0L,) DALprocess->appID, DALprocess->processID, port, 
        0, ((Fifo *) tmp)->getSize(), ((Fifo *) tmp)->getCapacity());
    fflush(stdout);
    #endif

	((Fifo *) tmp)->dequeue_end();
}

void *channel_write_begin(int port, int tokensize, int tokenrate, int blocksize, int blk_ptr, DALProcess *p) {

	wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

	void *tmp = ((wrapDALProcess *)p->wptr)->portToChannel[port];
	int len = tokenrate * tokensize;
	uint8_t *buf;
    
#ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write_begin is NULL");
    CPP11(ulong msgid = msgcounter++;)
	printf (CPP11("%8lu ") "bwrite %2d %4d %4ld %6d %6d %6d\n", CPP11(msgid,) DALprocess->appID, DALprocess->processID, port, 
        tokenrate*tokensize, ((Fifo *) tmp)->getSize(), ((Fifo *) tmp)->getCapacity());
    fflush(stdout);
#endif

	int numWrites = ((Fifo *)tmp)->getNumWrites();
	if (numWrites == 0) {
		numWrites = tokenrate / blocksize;
		((Fifo *)tmp)->setNumWrites(tokenrate / blocksize);
	}

	int write = 0;
	while ((DALprocess->stop == 0) && write < len) {
		write = ((Fifo *)tmp)->enqueue_start((void**) &buf, len, &(DALprocess->stop));

		pthread_mutex_lock(&(DALprocess->mutex_pause));
		while (DALprocess->pause == 1) {
			pthread_cond_wait(&(DALprocess->cond_pause),
					&(DALprocess->mutex_pause));
		}
		pthread_mutex_unlock(&(DALprocess->mutex_pause));
	}

	((Fifo *)tmp)->setNumWrites(numWrites - 1);
    
#ifdef DEBUG
    CPP11(printf("%8lu finished\n", msgid); fflush(stdout));
#endif

	return buf + blk_ptr * tokensize;
}

void channel_write_end(int port, void *buf, DALProcess *p) {

	wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

	void *tmp = ((wrapDALProcess *)p->wptr)->portToChannel[port];
    
    #ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write_begin is NULL");
	printf (CPP11("%8lu ") "ewrite %2d %4d %4ld %6d %6d %6d\n", CPP11(0L,) DALprocess->appID, DALprocess->processID, port, 
        0, ((Fifo *) tmp)->getSize(), ((Fifo *) tmp)->getCapacity());
    fflush(stdout);
    #endif


	if (((Fifo *)tmp)->getNumWrites() == 0) {
		((Fifo *) tmp)->enqueue_end(buf, DALprocess->rank);

		pthread_mutex_lock(&(DALprocess->mutex_pause));
		while (DALprocess->pause == 1) {
			pthread_cond_wait(&(DALprocess->cond_pause),
					&(DALprocess->mutex_pause));
		}
		pthread_mutex_unlock(&(DALprocess->mutex_pause));
	}

	if (DALprocess->stop != 0) {
		pthread_exit (NULL);
	}
}

//! Skips data from a port of a process.
/*!
 The DAL_skip function blocks if there are less then len Bytes
 in the channel. Usually, the channel should
 be allocated, i.e. the pointer to the channel should not be NULL.
 Otherwise there will be an error message in DEBUG mode. An alternative
 would be to do a busy wait until the channel is allocated (see the
 comments in the code below).
 \param port denotes the port of a process (0 <= port < MAX_NUMBER_PORTS);
 \param len is the number of Bytes that are skipped from port to buf
 \param p is a pointer to the process structure (DALProcess)
 */
int DAL_skip(void *port, int len, DALProcess *p) {
	long portId = (long) port;
	void *tmp = ((wrapDALProcess *) p->wptr)->portToChannel[portId];
	int read = 0;

	wrapDALProcess *DALprocess;
	DALprocess = (wrapDALProcess *) p->wptr;

#ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write is NULL");
#endif

	while ((DALprocess->stop == 0) && read < len) {
		read = ((Fifo *) tmp)->uncond_dequeue_withoutdata(len);

		pthread_mutex_lock(&(DALprocess->mutex_pause));
		while (DALprocess->pause == 1) {
			pthread_cond_wait(&(DALprocess->cond_pause),
					&(DALprocess->mutex_pause));
		}
		pthread_mutex_unlock(&(DALprocess->mutex_pause));
	}

	if (DALprocess->stop != 0) {
		pthread_exit (NULL);
	}

	return (0);
}

//! Sends an event to the master controller.
/*!
 The DAL_send_event function blocks if there is not enough space
 on the channel to the master controller. Usually, the channel should
 be allocated, i.e. the pointer to the channel should not be NULL.
 Otherwise there will be an error message in DEBUG mode. A message
 of type MSGCNTL is sent (see also the definitions in message.h ). It
 contains as fields the location and processID of the sending process
 and the message itself.
 \param message denotes the message that is send, i.e. the event
 \param p is a pointer to the process structure (DALProcess)
 */
int DAL_send_event(void *message, DALProcess *p) {
	long messageId = (long) message;
	msgCntl Cntl;
	wrapDALProcess *dal = (wrapDALProcess *) p->wptr;
	Fifo *channel = (Fifo *) dal->portToChannel[MAX_NUMBER_PORTS];

#ifdef DEBUG
	if (channel == NULL) error_abort("Queue pointer in DAL_writer is NULL");
#endif

	Cntl.type = MSGCNTL;
	Cntl.location = dal->location;
	Cntl.appID = dal->appID;
	Cntl.processID = dal->processID;
	Cntl.message = messageId;

	int stopSignal = 0;
	int write = 0;
	while (write < (int) sizeof(msgCntl)) {
		write = channel->uncond_enqueue(&Cntl, sizeof(msgCntl), dal->rank, &stopSignal);
	}
	return (0);
}
