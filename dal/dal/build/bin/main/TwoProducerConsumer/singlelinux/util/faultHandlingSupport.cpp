/*! \file
 \brief support for fault tolerance

 \authors Devendra Rai, Lothar Thiele, Lars Schor
 */

#include <stdio.h>
#include <stdarg.h>

#include "errors.h"
#include "Fifo.h"

#include "faultHandlingSupport.h"

#ifdef FAULTHANDLING_DUPLICATION
#define READ_INCOMPLETE (-1)
#define WRITE_INCOMPLETE (-1)

int increment_FIFO_Size(void *port, unsigned inc, DALProcess* p) {
	/*Get which FIFO*/
	long portId = (long) port;
	void * tmp = ((wrapDALProcess *) p->wptr)->portToChannel[portId];
	unsigned char* newFIFO;
	unsigned FIFOCapacity;

#ifdef DEBUG
	if (tmp == NULL) error_abort("Queue pointer in DAL_write is NULL");
#endif

#ifdef DEBUG
	printf ("\n [DAL increment FIFO]: caller: %s:%p\t to port %d (FIFO: %p), Increment: %u\n", (char*)(p->local), p, portId, tmp, inc);
#endif

	/* Get current FIFO capacity */
	FIFOCapacity = ((Fifo *) tmp)->getCapacity();

	/* Increment the FIFO size*/
	newFIFO = new unsigned char[inc + FIFOCapacity];
	if (NULL == newFIFO) {
		/* cannot increment the FIFO size*/
		return (-1);
	} else {
		/*We have a new larger FIFO: Adjust parameters: Old FIFO is automatically destroyed */
		((Fifo *) tmp)->AdjustFIFO((inc + FIFOCapacity), newFIFO);

	}

	return (0);
}
#endif
