/*
 * WindowedFifo.cpp
 */

#include "WindowedFifo.h"

#include <string.h>

WindowedFifo::WindowedFifo(int capacity, int appId, int channelId) :
	InnerProcessorFifo(capacity, appId, channelId) {
}

WindowedFifo::~WindowedFifo() {
}

int WindowedFifo::enqueue_start(void **data, int len, int *stopCondition) {

	pthread_mutex_lock(&(this->mutex));
	while (this->size + len > this->capacity) {
		pthread_cond_wait(&(this->cond_full), &(this->mutex));
		if ((*stopCondition) != 0) {
			// Stop was set
			pthread_mutex_unlock(&(this->mutex));
			return 0;
		}
	}
	int amount = len;

	this->setWriteBufSize(len);

	int slack = this->capacity - this->in;

	if (slack >= len) {
		this->isOnBlockEnqueue = 1;
		*(data) = (this->buffer + this->in);
	} else {
		this->isOnBlockEnqueue = 0;
		*(data) = this->getWriteBuffer();
	}

	pthread_mutex_unlock(&(this->mutex));
	return amount;
}

int WindowedFifo::enqueue_end(void *data, int srcRank) {
	pthread_mutex_lock(&(this->mutex));
	int len = this->getWriteBufSize();

	if (this->isOnBlockEnqueue == 1) {
		this->size += len;
		this->in += len;
		this->in %= this->capacity;
		pthread_cond_broadcast(&(this->cond_empty));
	} else {
		enqueueDataInSafeFifo(data, len);
	}

	pthread_mutex_unlock(&(this->mutex));

	return len;
}

int WindowedFifo::dequeue_start(void **data, int len, int *stopCondition) {

	int amount = 0;

	pthread_mutex_lock(&(this->mutex));

	while (this->size < len) {
		pthread_cond_wait(&(this->cond_empty), &(this->mutex));
		if ((*stopCondition) != 0) {
			// Stop condition was set
			pthread_mutex_unlock(&(this->mutex));
			return 0;
		}
	}

	amount = len;
	int slack = this->capacity - this->out;

	if (slack >= len) {
		this->setReadBufSize(len);
		this->isOnBlockDequeue = 1;
		*(data) = (this->buffer + this->out);
	} else {
		this->isOnBlockDequeue = 0;
		(*data) = this->getReadBuffer();

		memcpy(*(data), this->buffer + this->out, slack);
		memcpy((void *) ((char *)(*data) + slack), this->buffer, len - slack);

		this->size -= len;
		this->out += len;
		this->out %= this->capacity;

		broadcastDequeue(amount);
	}

	pthread_mutex_unlock(&(this->mutex));
	return amount;
}

int WindowedFifo::dequeue_end() {

	pthread_mutex_lock(&(this->mutex));

	if (this->isOnBlockDequeue == 1) {
		int len = this->getReadBufSize();
		this->size -= len;
		this->out += len;
		this->out %= this->capacity;
		broadcastDequeue(len);
	}

	pthread_mutex_unlock(&(this->mutex));
	return 0;
}

int WindowedFifo::uncond_dequeue(void *data, int len, int *stopCondition) {
	printf("Windowed Fifo: dequeue at %d / %d\n", this->appId, this->channelId);
	error_abort("Windowed FIFOs do not support the standard interface. Change the process network specification.");
	return 0;
}

int WindowedFifo::uncond_enqueue(void *data, int len, int srcRank, int *stopCondition) {
	printf("Windowed Fifo: enqueue at %d / %d\n", this->appId, this->channelId);
	error_abort("Windowed FIFOs do not support the standard interface. Change the process network specification.");
	return 0;
}
