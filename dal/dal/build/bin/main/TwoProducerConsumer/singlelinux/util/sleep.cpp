/*! \file
\brief Methods to sleep while executing a thread

\authors Lars Schor, Devendra Rai
*/

#include "sleep.h"

//! Calculates the time plus a certain milliseconds from now on
/*!
\param time pointer to the time struct with the resulting time
\param millisecs milliseconds to add

\return pointer to the resulting time struct
*/
struct timespec * millisecondsFromNow (struct timespec * time, int millisecs) {
    struct timeval currSysTime;
    unsigned long nanosecs, secs;
    const unsigned long NANOSEC_PER_SEC = 1000000000;

    /* get current system time and add millisecs */
    gettimeofday(&currSysTime,NULL);

    nanosecs = ((unsigned long) (millisecs * 1000L + ((unsigned long) currSysTime.tv_usec))) * 1000L;
    if (nanosecs >= NANOSEC_PER_SEC)
    {
        secs = currSysTime.tv_sec + (nanosecs / NANOSEC_PER_SEC);
        nanosecs %= NANOSEC_PER_SEC;
    }
    else
    {
        secs = currSysTime.tv_sec;
    }

    time->tv_nsec = (long) nanosecs;
    time->tv_sec = (long) secs;

    return time;
}

void sleepMilliseconds(int millisecs, wrapDALProcess *DALprocess) {
    pthread_mutex_t fakeMutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t fakeCond = PTHREAD_COND_INITIALIZER;

    pthread_mutex_lock(&(fakeMutex));

    struct timespec to;
    millisecondsFromNow(&to, millisecs);

    pthread_cond_timedwait(&(fakeCond), &(fakeMutex), &to); 

    pthread_mutex_unlock(&(fakeMutex));
}
