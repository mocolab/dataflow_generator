/*! \file
 \brief Generic interface to display an interface that should both work for ETH and TIMA

 \authors Clement Deschamps <clement.deschamps@imag.fr>, Lars Schor
 */

#ifdef VIEWER
#include "CImg.h"
using namespace cimg_library;

struct s_framebuffer {
	int width, height;
	int mode;
	CImgDisplay *main_disp;
	void *buffer;
};

#else
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cstring>

struct s_framebuffer {
	int width, height;
	int mode;
	void *buffer;
};
#endif

int fb_count = 0;
#define MAX_FB 10

struct s_framebuffer fb_array[MAX_FB];

static s_framebuffer *fb_find(void *buffer) {
	int i;
	for(i=0; i<fb_count; i++) {
		if(fb_array[i].buffer == buffer) {
			return &fb_array[i]; // corresponding framebuffer found
		}
	}
	return 0;
}

void *DAL_fb_new(int width, int height, int mode) {

#ifdef VIEWER
	if(fb_count == MAX_FB) {
		printf("fb_new: error, no framebuffer available\n");
		return 0;
	}

	if(mode == 0) { // Grayscale (8 bits)
		void *buffer = malloc(width * height);
		if(buffer == NULL) {
			printf("fb_new: memory allocation failed !\n");
			return NULL;
		}
		fb_array[fb_count].width = width;
		fb_array[fb_count].height = height;
		fb_array[fb_count].mode = mode;
		fb_array[fb_count].buffer = buffer;
		fb_array[fb_count].main_disp = new CImgDisplay(width, height, "DAL MJPEG");

		fb_count++;

		return buffer;
	}

	printf("fb_new: unknown mode %d\n", mode);
	return 0;
#else
	if(fb_count == MAX_FB) {
		printf("fb_new: error, no framebuffer available\n");
		return 0;
	}

	if(mode == 0) { // Grayscale (8 bits)
		void *buffer = malloc(width * height);
		if(buffer == NULL) {
			printf("fb_new: memory allocation failed !\n");
			return NULL;
		}
		fb_array[fb_count].width = width;
		fb_array[fb_count].height = height;
		fb_array[fb_count].mode = mode;
		fb_array[fb_count].buffer = buffer;

		fb_count++;

		return buffer;
	}

	printf("fb_new: unknown mode %d\n", mode);
	return 0;
#endif

}

void DAL_fb_update(void *buffer) {

#ifdef VIEWER
	struct s_framebuffer *fb = fb_find(buffer);
	if(fb) {
		CImg<unsigned char> img((unsigned char *)fb->buffer,
				fb->width, fb->height),
		visu(fb->width, fb->height, 1, 1, 0);
		fb->main_disp->display(img);
		fb->main_disp->paint();
	}
#endif

}

void DAL_fb_free(void *buffer) {

#ifdef VIEWER
	struct s_framebuffer *fb = fb_find(buffer);
	if(fb) {
		delete fb->main_disp;
		free(fb->buffer);
		fb->buffer = 0;
	}
#else
	struct s_framebuffer *fb = fb_find(buffer);
	if(fb) {
		free(fb->buffer);
		fb->buffer = 0;
	}
#endif
}
