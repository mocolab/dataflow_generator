/*
 * InnerProcessorFifo.cpp
 *
 *  Created on: May 4, 2012
 *      Author: lschor
 */

#include "InnerProcessorFifo.h"

#include <string.h>

InnerProcessorFifo::InnerProcessorFifo(int capacity, int appId, int channelId) :
	Fifo(capacity, appId, channelId) {
}

InnerProcessorFifo::~InnerProcessorFifo() {
}

void InnerProcessorFifo::enqueueDataInSafeFifo(void *data, int len) {
	int slack;
	slack = this->capacity - this->in;

	if (slack >= len) {
		memcpy(this->buffer + this->in, data, len);
	} else {
		memcpy(this->buffer + this->in, data, slack);
		memcpy(this->buffer, (void *) ((char *) data + slack), len - slack);
	}

	this->size += len;
	this->in += len;
	this->in %= this->capacity;

	pthread_cond_broadcast(&(this->cond_empty));
}

void InnerProcessorFifo::broadcastDequeue(int amount) {
	pthread_cond_broadcast(&(this->cond_full));
}
