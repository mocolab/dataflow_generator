/*! \file
 \brief Implementation of a thread-safe fifo queue

 \authors Lothar Thiele, Lars Schor, Devendra Rai
 */

#include "Fifo.h"

#include "sleep.h"
#include "errors.h"

#include <stdio.h> 
#include <string.h> 
#include <time.h>
#include <sys/time.h>

Fifo::Fifo(int capacity, int appId, int channelId) {
	this->capacity = capacity;

	this->buffer = new unsigned char[this->capacity];
	if (this->buffer == NULL) {
		errno_abort("fifo buffer could not be allocated.");
	}

	this->size = 0;
	this->in = 0;
	this->out = 0;

	this->readBuf = new unsigned char[this->capacity];
	if (this->readBuf == NULL) {
		errno_abort("fifo read buffer could not be allocated.");
	}

	this->writeBuf = new unsigned char[this->capacity];
	if (this->writeBuf == NULL) {
		errno_abort("fifo write buffer could not be allocated.");
	}

	this->numWrites = 0;

	this->appId = appId;
	this->channelId = channelId;

	pthread_mutex_init(&this->mutex, NULL);
	pthread_cond_init(&this->cond_full, NULL);
	pthread_cond_init(&this->cond_empty, NULL);
}

Fifo::~Fifo() {
	pthread_mutex_destroy(&(this->mutex));

	pthread_cond_destroy(&(this->cond_full));
	pthread_cond_destroy(&(this->cond_empty));

	delete[] this->buffer;
	delete[] this->readBuf;
	delete[] this->writeBuf;
}

//! Returns the number of Bytes currently stored in the fifo.
/*!
 \return number of Bytes in the fifo
 */
int Fifo::getSize() {
	pthread_mutex_lock(&(this->mutex));
	int size = this->size;
	pthread_mutex_unlock(&(this->mutex));
	return size;
}

//! Sets the number of Bytes currently stored in the fifo.
/*!
 \param newSize new size of the fifo
 */
void Fifo::setSize(int newSize) {
	pthread_mutex_lock(&(this->mutex));
	this->size = newSize;
	pthread_cond_broadcast(&(this->cond_full));
	pthread_mutex_unlock(&(this->mutex));
}

void Fifo::addReadAmount(int amount) {
	pthread_mutex_lock(&(this->mutex));
	this->size -= amount;
	pthread_cond_broadcast(&(this->cond_full));
	pthread_mutex_unlock(&(this->mutex));
}

int Fifo::getCapacity() {
	return this->capacity;
}

unsigned char *Fifo::getReadBuffer() {
	return this->readBuf;
}

unsigned char *Fifo::getWriteBuffer() {
	return this->writeBuf;
}

void Fifo::setWriteBufSize(int size) {
	this->writeBufSize = size;
}

int Fifo::getWriteBufSize() {
	return this->writeBufSize;
}

void Fifo::setReadBufSize(int size) {
	this->readBufSize = size;
}

int Fifo::getReadBufSize() {
	return this->readBufSize;
}

void Fifo::setNumWrites(int num) {
	this->numWrites = num;
}

int Fifo::getNumWrites() {
	return this->numWrites;
}

int Fifo::getAppId() {
	return this->appId;
}

int Fifo::getChannelId() {
	return this->channelId;
}

//! Enqueues unconditionally data in a fifo buffer.
/*!
 The enqueue operation returns len if there is enough space
 in the fifo to enqueue len Bytes, otherwise it returns 0.
 \param fifo points to the data structure of the fifo
 \param data is a pointer to the first data item to be enqueued
 \param len is the number of Bytes to be enqueued, starting at data
 \return len if there is enough space in the fifo to enqueue len Bytes,
 otherwise it returns 0.
 */
int Fifo::uncond_enqueue(void *data, int len, int srcRank, int *stopCondition) {

	pthread_mutex_lock(&(this->mutex));

#ifdef DEBUG
	if (len > this->capacity) {
		char buffer [100];
		sprintf (buffer, "App %d, Channel %d: tries to enqueue %d, but has a capacity of %d\n", this->appId, this->channelId, len, this->capacity);
		pthread_mutex_unlock(&(this->mutex));
		error_abort(buffer);
	}
#endif

	while (this->size + len > this->capacity) {
		pthread_cond_wait(&(this->cond_full), &(this->mutex));
		if ((*stopCondition) != 0) {
			// Stop was set
			pthread_mutex_unlock(&(this->mutex));
			return 0;
		}
	}
	int amount = len;

#ifdef TESTING_MODE_FIFO
	printf("FIFO %d/%d: enqueue %d bytes\n", this->appId, this->channelId, len); fflush(stdout);
#endif

	enqueueDataInSafeFifo(data, len);

	pthread_mutex_unlock(&(this->mutex));
	return amount;
}


//! Non-blocking write: if the FIFO does not have space, returns with -1.
/*!
 The enqueue operation returns len if there is enough space
 in the fifo to enqueue len Bytes, otherwise it returns 0.
 \param fifo points to the data structure of the fifo
 \param data is a pointer to the first data item to be enqueued
 \param len is the number of Bytes to be enqueued, starting at data
 \return len if there is enough space in the fifo to enqueue len Bytes,
 otherwise it returns 0.
 */
int Fifo::uncond_enqueue_NB(void *data, int len, int srcRank) {
#ifdef DEBUG
	if(srcRank == 0)
	{
		printf ("\n From FIFO: %p, size: %d, len: %d, capacity: %d\n", this, this->size, len, this->capacity);
	}
#endif
	pthread_mutex_lock(&(this->mutex));

	if (this->size + len > this->capacity) {
		//printf ("\n From FIFO: size: %d, len: %d, capacity: %d\n", this->size, len, this->capacity);

		pthread_mutex_unlock(&(this->mutex));
		return (-1);
	}
	int amount = len;
	enqueueDataInSafeFifo(data, len);

	pthread_mutex_unlock(&(this->mutex));
	return amount;
}

//! Dequeues unconditionally data in a fifo buffer.
/*!
 The dequeue operation returns len if there are at least
 len Bytes in the fifo, otherwise it returns 0.
 \param fifo points to the data structure of the fifo
 \param data is a pointer to the first target data item
 \param len is the number of Bytes to be dequeued
 \return len if there is enough space in the fifo to dequeue len Bytes,
 otherwise it returns 0.
 */
int Fifo::uncond_dequeue(void *data, int len, int *stopCondition) {
	int slack;
	int amount = 0;

#ifdef DEBUG
	if (len > this->capacity) {
		char buffer [100];
		sprintf (buffer, "App %d, Channel %d: tries to dequeue %d, but has a capacity of %d\n", this->appId, this->channelId, len, this->capacity);
		pthread_mutex_unlock(&(this->mutex));
		error_abort(buffer);
	}
#endif

	pthread_mutex_lock(&(this->mutex));

	while (this->size < len) {
		pthread_cond_wait(&(this->cond_empty), &(this->mutex));
		if ((*stopCondition) != 0) {
			// Stop condition was set
			pthread_mutex_unlock(&(this->mutex));
			return 0;
		}
	}

#ifdef TESTING_MODE_FIFO
	printf("FIFO %d/%d: dequeue %d bytes\n", this->appId, this->channelId, len); fflush(stdout);
#endif

	if (this->size >= len) {
		slack = this->capacity - this->out;
		if (slack >= len) {
			memcpy(data, this->buffer + this->out, len);
		} else {
			memcpy(data, this->buffer + this->out, slack);
			memcpy((void *) ((char *) data + slack), this->buffer, len - slack);
		}
		this->size -= len;
		this->out += len;
		this->out %= this->capacity;
		amount = len;

		broadcastDequeue(amount);
	}

	pthread_mutex_unlock(&(this->mutex));

	return amount;
}

int Fifo::uncond_dequeue_NB(void *data, int len) {
      int slack;
      int amount = 0;

#ifdef DEBUG
      if (len > this->capacity) {
            char buffer [100];
            sprintf (buffer, "App %d, Channel %d: tries to dequeue %d, but has a capacity of %d\n", this->appId, this->channelId, len, this->capacity);
            pthread_mutex_unlock(&(this->mutex));
            error_abort(buffer);
      }
#endif

      pthread_mutex_lock(&(this->mutex));

      if (this->size < len) {
            // token not available

                  // TIMEDOUT
                  pthread_mutex_unlock(&(this->mutex));
                  return (-1);

      }

      if (this->size >= len) {
            slack = this->capacity - this->out;
            if (slack >= len) {
                  memcpy(data, this->buffer + this->out, len);
            } else {
                  memcpy(data, this->buffer + this->out, slack);
                  memcpy((void *) ((char *) data + slack), this->buffer, len - slack);
            }
            this->size -= len;
            this->out += len;
            this->out %= this->capacity;
            amount = len;

            broadcastDequeue(amount);
      }

      pthread_mutex_unlock(&(this->mutex));

      return amount;
}


int Fifo::uncond_dequeue_withoutdata(int len) {
	int amount = 0;

	struct timespec to;

	pthread_mutex_lock(&(this->mutex));

	while (this->size < len) {
		millisecondsFromNow(&to, 10);
		if (pthread_cond_timedwait(&(this->cond_empty), &(this->mutex), &to)
				!= 0) {
			// TIMEDOUT
			pthread_mutex_unlock(&(this->mutex));
			return 0;
		}
	}

	if (this->size >= len) {
		this->size -= len;
		this->out += len;
		this->out %= this->capacity;
		amount = len;

		broadcastDequeue(amount);
	}

	pthread_mutex_unlock(&(this->mutex));

	return amount;
}

int Fifo::getSlackAndStartBufferCopy() {
	pthread_mutex_lock(&(this->mutex));
	return this->capacity - this->in;
}

char *Fifo::getBufferStartAddress() {
	return (char *) (this->buffer + this->in);
}

void Fifo::completeBufferCopy(int len) {
	if (len > 0) {
		this->size += len;
		this->in += len;
		this->in %= this->capacity;

		pthread_cond_broadcast(&(this->cond_empty));

	}
	pthread_mutex_unlock(&(this->mutex));
}

void Fifo::AdjustFIFO(unsigned NewFIFOCapacity, unsigned char* newFIFOptr) {
	unsigned slack;
	unsigned len;
	unsigned FIFOSize;
	unsigned char* oldFIFOptr;
	void* data = (void*)newFIFOptr;
	/* Acquire mutex*/
	pthread_mutex_lock(&(this->mutex));

	FIFOSize = this->size;
	oldFIFOptr = this->buffer;

	/* 1. Move the contents from old FIFO to new one*/
	len = this->size;
	slack = NewFIFOCapacity;
	if (slack >= len) {
		memcpy(data, this->buffer + this->out, len);
	} else {
		memcpy(data, this->buffer + this->out, slack);
		memcpy((void *) ((char *) data + slack), this->buffer, len - slack);
	}

	/* The current number of elements in the new FIFO is also the same as in the old one*/
	this->size = FIFOSize;

	/* Write head points to the end of currently used segment*/
	this->in = FIFOSize;

	/* Read head points to beginning of new FIFO	 */
	this->out = 0;

	/* Capacity of the FIFO is increased */
	this->capacity = NewFIFOCapacity;

	this->in %= this->capacity;

	/* The new buffer pointer*/
	this->buffer = newFIFOptr;

	delete oldFIFOptr;

	/* unlock the mutex*/
	pthread_mutex_unlock(&(this->mutex));
}

int Fifo::enqueue_start(void **data, int len, int *stopCondition) {

	pthread_mutex_lock(&(this->mutex));
	while (this->size + len > this->capacity) {
		pthread_cond_wait(&(this->cond_full), &(this->mutex));
		if ((*stopCondition) != 0) {
			// Stop was set
			pthread_mutex_unlock(&(this->mutex));
			return 0;
		}
	}
	int amount = len;

	this->setWriteBufSize(len);
	*(data) = this->getWriteBuffer();

	pthread_mutex_unlock(&(this->mutex));
	return amount;
}

int Fifo::enqueue_end(void *data, int srcRank) {

	int len = this->getWriteBufSize();

	pthread_mutex_lock(&(this->mutex));
	enqueueDataInSafeFifo(this->getWriteBuffer(), len);
	pthread_mutex_unlock(&(this->mutex));

	return len;
}

int Fifo::dequeue_start(void **data, int len, int *stopCondition) {
	*(data) = this->getReadBuffer();
	int read = this->uncond_dequeue(*(data), len, stopCondition);
	return read;
}

int Fifo::dequeue_end() {
	return 0;
}
