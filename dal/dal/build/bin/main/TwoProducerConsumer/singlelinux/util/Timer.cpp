#include <stdio.h>

#ifdef LINUX
#include <sys/resource.h>
#endif

#include "global_definitions.h"

#include "Timer.h"

Timer::Timer() {
	_hasStarted = false;
}

Timer::~Timer() {

}

void Timer::start() {

	if (!_hasStarted) {
#ifdef LINUX
		rusage ru;
		getrusage(RUSAGE_THREAD, &ru);
		mUserStart = ru.ru_utime;
		mSysStart  = ru.ru_stime;
#endif
		
		_hasStarted = true;
	} else {
		std::cout << "Error: called startTimer() after calling startTimer()."
				<< std::endl;
	}

}

void Timer::stop() {

	if (_hasStarted) {
#ifdef LINUX
		rusage ru;
		getrusage(RUSAGE_THREAD, &ru);
		mUserEnd = ru.ru_utime;
		mSysEnd  = ru.ru_stime;
#endif

		_hasStarted = false;
	} else {
		std::cout << "Error: called stopTimer() before startTimer()."
				<< std::endl;
	}

}

void Timer::print(int appId, int processId) {

	if (!_hasStarted) {
		unsigned long usrdiff, sysdiff;
		usrdiff = (mUserEnd.tv_sec-mUserStart.tv_sec)*1000000 + 
			mUserEnd.tv_usec-mUserStart.tv_usec;
		sysdiff = (mSysEnd.tv_sec-mSysStart.tv_sec)*1000000 + 
			mSysEnd.tv_usec-mSysStart.tv_usec;

		unsigned long usrmsec = (usrdiff+500)/1000;
		unsigned long sysmsec = (sysdiff+500)/1000;

		char buffer[100];
		if (appId == CONTROLLER_ID) {
#ifdef PERFORMANCE_ANALYSIS_CONTROLLER
			if (processId == 0) {
				sprintf(buffer, "\nMaster controller: elapsed time is %ld ms.",
						usrmsec);
				puts(buffer);
				fflush(stdout);
			} else {
				sprintf(buffer, "Slave %d controller: elapsed time is %ld ms.",
						processId, usrmsec);
				puts(buffer);
				fflush(stdout);
			}
#endif
		} else {
			sprintf(buffer, "App %d, process %d: user time is %ld ms, sys time is %ld ms.",
					appId, processId, usrmsec, sysmsec);
			puts(buffer);
			fflush(stdout);
		}
	} else {
		std::cout << "Error: Can't print timer, timer still in progress."
				<< std::endl;
	}

}

void Timer::reset() {

	if (!_hasStarted) {
		/*_startTime->tv_sec = 0;
		_startTime->tv_usec = 0;
		_stopTime->tv_sec = 0;
		_stopTime->tv_usec = 0;
		_time = 0;*/
	} else {
		std::cout << "Error: Can't reset timer, timer still in progress."
				<< std::endl;
	}

}
