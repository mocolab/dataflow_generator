/*! \file
\brief Wrapper data structure of a controller

\authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#ifndef WRAPCONTROLLER_H
#define WRAPCONTROLLER_H

//! Wrapper data structure of a process
typedef struct wrapController {
    Fifo            ***PChannel;
    wrapDALProcess  ***myProcesses;
    const char**          appname;
    const char***         processbasename;
    const char***         processname;
    int             eventPortID; 
    std::map<int, int> *locationToCoreMap;
    pthread_mutex_t *mutex_install; 
    pthread_t       faultFileThread; 
} wrapController;

#endif
