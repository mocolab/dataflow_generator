#include <stdio.h>
#include <string.h>

#include "errors.h"
#include "controllerHelper.h"
#include "wrap.h"
#include "Fifo.h"

void installDALInChannel(int appID, int fifoID, int capacity, int tokensize, int peerLocation, int peerRank, int channelType,
		DALProcess *slaveP) {
	installDALLocalChannel(appID, fifoID, capacity, tokensize, 0, channelType, slaveP);
}

void installDALOutChannel(int appID, int fifoID, int capacity,
		int tokensize, int peerLocation, int peerRank, int channelType, DALProcess *slaveP) {
	installDALLocalChannel(appID, fifoID, capacity, tokensize, 0, channelType, slaveP);
}

//! Kill a fifo and reclaim the allocated memory.
/*!
 The function kills a fifo and reclaims the memory.
 \param fifoID is the ID of the fifo that is killed, i.e. the index
 where the channel is found in Channel[].
 \param Channel[] is an array which contains the pointer to the
 channel fifos and Channel[fifoID] points to the channel-fifo
 that is removed.
 */
void killDALChannel(int appID, int fifoID, DALProcess *slaveP) {
	wrapController* slave =
			(wrapController*) ((wrapDALProcess*) slaveP->wptr)->subWrap;

	pthread_mutex_lock(slave->mutex_install);
	// Just kill it if it is not yet NULL
	if (slave->PChannel[appID][fifoID] != NULL) {
		Fifo *oldFifo = slave->PChannel[appID][fifoID];
		slave->PChannel[appID][fifoID] = NULL;
		delete oldFifo;
	}

	pthread_mutex_unlock(slave->mutex_install);
}

void killScheduler(DALProcess *slaveP) {
}
