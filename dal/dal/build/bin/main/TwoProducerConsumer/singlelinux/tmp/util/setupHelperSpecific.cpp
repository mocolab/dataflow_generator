//! Installs a fifo by allocating a fifo.
/*!
 The function installs a fifo by allocating the necessary
 memory and initiating the data structure.
 \param fifoID is the ID of the fifo that is installed, i.e. the index
 where the channel is found in Channel[].
 \param Channel[] is an array which contains the pointer to the
 channel fifos and Channel[fifoID] points to the newly generated
 fifo after execution of the function.
 \param capacity is the capacity of the channel-fifo in Bytes.
 */

#include "setupHelperSpecific.h"

//! Kill a fifo and reclaim the allocated memory.
/*!
 The function kills a fifo and reclaims the memory.
 \param fifoID is the ID of the fifo that is killed, i.e. the index
 where the channel is found in Channel[].
 \param Channel[] is an array which contains the pointer to the
 channel fifos and Channel[fifoID] points to the channel-fifo
 that is removed.
 */
void killMasterFifo(Fifo *channel) {
	if (channel != NULL) {
		delete channel;
	}
}
