/*! \file
    \brief Links process wrapper to process 

    The link between the process wrapper wrap_process.c
    and the process should be generated automatically. In
    each subdirectory corresponding to a process there should
    be a generate.h file and a copy of the wrap_process.c
    source. PROCESSNAME needs to be replaced by the process
    name which needs to be the name of the directory, the
    source file and the header file containing the local
    process data.

    \authors Lothar Thiele, Lars Schor, Devendra Rai
*/

#ifndef GENERATE_H 
#define GENERATE_H

#include <stdio.h>
#include <stdarg.h>

#include "wrap.h"

#include "@PROCESSNAME@.h"

#define process_init(args...) @PROCESSNAME@_init(args)
#define process_fire(args...) @PROCESSNAME@_fire(args)
#define process_finish(args...) @PROCESSNAME@_finish(args)

#ifdef TESTING_MODE
#define LOG_FILE_NAME "log_@APP_NAME@_@PROCESSNAME@.txt"
#endif

#endif
