# -------------------------------------
# Configuration file for DAL Linux compilation
# Copyright: ETH Zurich

# -------------------------------------
# User defined compile and linking arguments
# 
# Often used compiler arguments
# -DFAULTHANDLING_DUPLICATION --> activates process duplication support
# -DPERFORMANCE_ANALYSIS --> activates timing measurements
# -DPERFORMANCE_ANALYSIS_CONTROLLER --> activates timing measurements for controller
# -DTESTING_MODE --> used for automatic testing (different output format)
# -DTESTING_MODE_FIFO --> used for testing of the fifos
# -DDEMO --> activates demo mode to see the interactions of the controllers
#
# Video Demos
# COMP_ARG = -DVIEWER
# LINK_ARG = -lX11
# 
COMP_ARG = 
LINK_ARG = 

# -------------------------------------
# Activate debugging mode by switching this variable to 1
DEBUGGING = 0

# -------------------------------------
# General constants
OS:=$(shell uname -s)

# -------------------------------------
# Testing mode
ifeq ($(DAL_TESTING),1)
DAL_TESTING_ARG = -fno-builtin-printf -DTESTING_MODE
else
DAL_TESTING_ARG = 
endif

# -------------------------------------
# OPTIMIZE / Debugging Mode
ifeq ($(DEBUGGING),1)
OPTFLAGS = -g
COMP_ARG += -DDEBUG
else
OPTFLAGS = -O2 -s -fpermissive
endif

# -------------------------------------
# File tools
CXX = g++
RM = rm -f
RMDIR = rm -rf
RMREC = rm -f -R
CP = cp
MV = mv

# -------------------------------------
# OS Support
ifeq ($(OS),Linux) #Linux
CXX_OS_FLAGS = -fPIC -DLINUX
CXX_OS_LINK_FLAGS = -pthread
SO_FLAG = -soname
endif
ifeq ($(OS),Darwin) # Mac Os X
CXX_OS_FLAGS = -x c++ -fPIC -Qunused-arguments
CXX_OS_LINK_FLAGS = -pthread -Qunused-arguments
SO_FLAG = -install_name
endif
ifneq (,$(findstring CYGWIN,$(OS))) # Cygwin
CXX_OS_FLAGS = -DWINDOWS
CXX_OS_LINK_FLAGS = -lpthread
SO_FLAG = -soname
endif

# -------------------------------------
# C++ compile and link arguments
CXX_FLAGS = $(COMP_ARG) $(DAL_TESTING_ARG) $(CXX_OS_FLAGS)
CXX_COMPILE = $(CXX) -c $(OPTFLAGS) $(CXX_FLAGS) -Wall
CXX_LINK = $(CXX) $(CXX_OS_LINK_FLAGS)
