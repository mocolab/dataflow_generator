#ifndef CONSUMER_H
#define CONSUMER_H

#include <dal.h>
#include "global.h"

#define PORT_OUTPUT_COEFFICIENTS "output_coefficients"
#define EVENT_1 "activate_matrix"

typedef struct _local_states
{
  int index;
  ComplexNumber coeffs[NUMBER_OF_FFT_POINTS];
} Consumer_State;

void consumer_init(DALProcess *);
int consumer_fire(DALProcess *);
void consumer_finish(DALProcess *);

#endif
