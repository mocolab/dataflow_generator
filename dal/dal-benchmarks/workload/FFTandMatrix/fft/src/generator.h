#ifndef PRODUCER_H
#define PRODUCER_H

#include <dal.h>
#include "global.h"
#include "stdlib.h"

#define PORT_INPUT_COEFFICIENTS "input_coefficients"

typedef struct _local_states
{
  int index;
  ComplexNumber coeffs[NUMBER_OF_FFT_POINTS];
} Generator_State;

void generator_init(DALProcess *);
int generator_fire(DALProcess *);
void generator_finish(DALProcess *);

#endif
