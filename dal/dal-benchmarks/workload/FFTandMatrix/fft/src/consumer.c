#include <stdio.h>

#include "consumer.h"

void consumer_init(DALProcess *p)
{
  ; //nothing to be done here
}

int consumer_fire(DALProcess *p)
{
  CREATEPORTVAR(input_port);

  for (p->local->index = 0; p->local->index < NUMBER_OF_FFT_POINTS;
       p->local->index++) {
    CREATEPORT(input_port, PORT_OUTPUT_COEFFICIENTS, 1,
            p->local->index, NUMBER_OF_FFT_POINTS);
    DAL_read((void*)input_port, &(p->local->coeffs[p->local->index]),
        sizeof(ComplexNumber), p);
    printf("%15s: coeff[%d]: %9f + j * %9f\n",
           "fft_consumer", p->local->index,
           p->local->coeffs[p->local->index].real,
           p->local->coeffs[p->local->index].imag);
    fflush(stdout);
  }

  DAL_send_event((void *)EVENT_1, p);
  return(1);
}

void consumer_finish(DALProcess *p) {
	fflush(stdout);
}
