#include <stdio.h>

#include "generator.h"

/**
 * Returns a random integer in the range between lower_bound and
 * upper_bound, where the bounding values are included in the interval.
 */
int getRandomNumber(int lower_bound, int upper_bound)
{
  return (rand() % (upper_bound - lower_bound + 1)) + lower_bound;
}


void generator_init(DALProcess *p)
{
  ; //nothing to be done here
}


int generator_fire(DALProcess *p)
{
  CREATEPORTVAR(output_port);

  srand(0); //initialize random number generator

  //generate input coefficients and write them to output ports
  for (p->local->index = 0;
       p->local->index < NUMBER_OF_FFT_POINTS;
       p->local->index++) {
    p->local->coeffs[p->local->index].real = (float)getRandomNumber(-9, 9);
    p->local->coeffs[p->local->index].imag = (float)getRandomNumber(-9, 9);

    CREATEPORT(output_port, PORT_INPUT_COEFFICIENTS, 1,
            p->local->index, NUMBER_OF_FFT_POINTS);
    printf("%15s: Write to input_coefficients_%d: %9f + j * %9f\n",
           "fft_generator", p->local->index,
           p->local->coeffs[p->local->index].real,
           p->local->coeffs[p->local->index].imag);
    fflush(stdout);
    DAL_write((void*)output_port, &(p->local->coeffs[p->local->index]),
              sizeof(ComplexNumber), p);
  }

  return(1);
}

void generator_finish(DALProcess *p) {
	fflush(stdout);
}
