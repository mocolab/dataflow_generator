#ifndef CONSUMER_H
#define CONSUMER_H

#include "constants.h"
#include <dal.h>

#define PORT_MATRIXC "matrixC"
#define EVENT_1 "stop_fsm"

typedef struct _local_states
{
  unsigned row, col;
  float matrixC_value;
} Output_consumer_State;

void output_consumer_init(DALProcess *);
int output_consumer_fire(DALProcess *);
void output_consumer_finish(DALProcess *);

#endif
