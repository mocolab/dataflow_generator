#ifndef MERGESTREAM_H
#define MERGESTREAM_H

#include <dal.h>
#include "jpeg.h"

//LM: Event to declare the end of the system needed by TIMA's snake
#define EVENT_DONE "end_state"

#define PORT_IN1   "in1"
#define PORT_IN2   "in2"

//local variables
typedef struct _local_states {
    unsigned int x_size;
    unsigned int y_size;
    int port_count;
    unsigned int ii;
    int num_iter; // nummber of fire
    unsigned char *display_buffer;
} Mergestream_State;

void mergestream_init(DALProcess *);
int mergestream_fire(DALProcess *);
void mergestream_finish(DALProcess *);

#endif
