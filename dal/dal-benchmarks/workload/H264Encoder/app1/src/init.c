#include "init.h"

/***********************
  FUNCTION DECLARATIONS
***********************/

static inline void plane_copy( uint8_t *dst, int i_dst,
        uint8_t *src, int i_src, int w, int h)
{
    for( ; h > 0; h-- )
    {
        memcpy( dst, src, w );
        dst += i_dst;
        src += i_src;
    }
}

static int read_frame_yuv( YUVFrameData *pFrameData, int iFrame, FILE *handle)
{
    uint8_t buffer[176*144];

    if (ftell(handle) != iFrame * 176 * 144 * 3 / 2)
        fseek(handle, iFrame * 176 * 144 * 3 / 2, SEEK_SET);

    if (fread(buffer, 1, 176 * 144, handle ) <= 0)
        return -1;
    else
    {
        plane_copy( pFrameData->bufYData, 176+64,
                buffer, 176, 176, 144 );	
    }

    if (fread( buffer, 1, 176 * 144/4, handle ) <= 0)
        return -1;
    else
    {
        plane_copy( pFrameData->bufUData, (176+64)/2,
                buffer, 176/2, 176/2, 144/2 );	
    }

    if (fread( buffer, 1, 176 * 144/4, handle ) <= 0)
        return -1;
    else
    {
        plane_copy( pFrameData->bufVData, (176+64)/2,
                buffer, 176/2, 176/2, 144/2 );	
    }

    return 0;
}
/*********************
  DAL FUNCTIONS
**********************/

void init_init(DALProcess *p) {
	p->local->fpYUV_1872 = NULL;
	p->local->g_iFrame_1873 = -1;
	p->local->i_last_idr_1874 = -MAX_GOP_SIZE;
	p->local->i_idr_pic_id = 0;

	p->local->o_intFrameNum_1838 = 0;
	p->local->o_intFrameType_1839 = 0;
	p->local->fpYUV_1872 = fopen(FILE_NAME, "rb");

	if (NULL == p->local->fpYUV_1872)
	{
		printf("%s open error...\n", FILE_NAME);
		exit(0);
	}
	p->local->o_intLambda_1843 = 0;
	p->local->output_1854 = 0;
	{int i; for(i=0;i<99;i++) p->local->output_1857[i] = 0;}

	p->local->o_MB_index = 0 ;

	p->local->phase_99 = 0;
	p->local->q_index = 0;

/////////////////////////////////////
    p->local->iteration = 0;
    p->local->counter=0;
    {int i; for(i=0;i<25;i++)p->local->myutime[i] = 0;}
    {int i; for(i=0;i<25;i++)p->local->mystime[i] = 0;}
    gettimeofday(&p->local->total_start, NULL);
/////////////////////////////////////
}

int init_fire(DALProcess *p) {
	InitMEPacket t;

    int terminate = 0;

    // phase 0
    if ( p->local->phase_99 == 0 )
    {
        {  /* initI113.XAVIReaderI0 (class CGCXAVIReader) */
            p->local->g_iFrame_1873++;
            if (read_frame_yuv(&(p->local->o_msgFrameData_1837), p->local->g_iFrame_1873, p->local->fpYUV_1872) <0) {

                //signal VLC process to send the event to terminate the application

                if (++p->local->iteration >= 1){
                    terminate = 1;
                    DAL_write((void*) PORT_OUT2, &terminate, sizeof(int), p);
                    return 1;
                }
                else {
                    fclose(p->local->fpYUV_1872);
                    p->local->fpYUV_1872 = NULL;
                    p->local->g_iFrame_1873 = -1;
                    p->local->i_last_idr_1874 = -MAX_GOP_SIZE;
                    p->local->i_idr_pic_id = 0;

                    p->local->o_intFrameNum_1838 = 0;
                    p->local->o_intFrameType_1839 = 0;
                    p->local->fpYUV_1872 = fopen(FILE_NAME, "rb");

                    if (NULL == p->local->fpYUV_1872)
                    {
                        printf("%s open error...\n", FILE_NAME);
                        exit(0);
                    }
                    p->local->o_intLambda_1843 = 0;
                    p->local->output_1854 = 0;
                    {int i; for(i=0;i<99;i++) p->local->output_1857[i] = 0;}

                    p->local->o_MB_index = 0 ;

                    p->local->phase_99 = 0;
                    p->local->q_index = 0;
                }
            }

            p->local->o_intFrameNum_1838 = p->local->g_iFrame_1873;

            if (p->local->g_iFrame_1873 - p->local->i_last_idr_1874 >= MAX_GOP_SIZE)
            {
                p->local->o_intFrameType_1839 = FRAME_TYPE_IDR;

                p->local->i_s_info.inter_do_it = FRAME_TYPE_IDR;
                p->local->i_last_idr_1874 = p->local->g_iFrame_1873;
                p->local->o_msgIPoc_1840.m_nCurrRef = 0;
                //o_msgIPoc_1840.m_nPrevRef = -1;
            }
            else
            {
                p->local->o_intFrameType_1839 = FRAME_TYPE_P;

                p->local->i_s_info.inter_do_it = FRAME_TYPE_P;
                p->local->o_msgIPoc_1840.m_nCurrRef = 2 * (p->local->g_iFrame_1873 - p->local->i_last_idr_1874);
            }

            p->local->o_msgIPoc_1840.m_nCurrFrame = 2 * (p->local->g_iFrame_1873 - p->local->i_last_idr_1874);
        }

        {  /* initI113.RepeatI38 (class CGCRepeat) */
            int i;
            for (i = 0; i < 99; i++) {
                p->local->output_1857[(98-(i))] = p->local->i_s_info.inter_do_it;
            }
        }

        {  /* initI113.XPreProcessI5 (class CGCXPreProcess) */
            p->local->o_msgSliceHeader_1841.i_type = p->local->o_intFrameType_1839;
            p->local->o_msgSliceHeader_1841.i_first_mb = 0;
            p->local->o_msgSliceHeader_1841.i_last_mb = SPS_MB_WIDTH * SPS_MB_HEIGHT;
            p->local->o_msgSliceHeader_1841.i_frame_num = p->local->o_intFrameNum_1838;
            p->local->o_msgSliceHeader_1841.i_poc_lsb = 0;
            p->local->o_msgSliceHeader_1841.i_disable_deblocking_filter_idc = 0;

            if (FRAME_TYPE_IDR == p->local->o_intFrameType_1839)
            {
                p->local->o_msgSliceHeader_1841.i_idr_pic_id = p->local->i_idr_pic_id;
                p->local->o_msgSliceHeader_1841.b_num_ref_idx_override = 0;
                p->local->o_msgSliceHeader_1841.i_qp = QP_CONSTANT_I;
                p->local->i_idr_pic_id = (p->local->i_idr_pic_id + 1) % 65536;
            }
            else
            {
                p->local->o_msgSliceHeader_1841.i_idr_pic_id = -1;
                p->local->o_msgSliceHeader_1841.b_num_ref_idx_override = 1;
                p->local->o_msgSliceHeader_1841.i_qp = QP_CONSTANT;
            }

            p->local->o_msgSliceHeader_1841.i_qp_delta = p->local->o_msgSliceHeader_1841.i_qp - QP_CONSTANT;
            p->local->o_msgSliceHeader_1841.i_poc_lsb = p->local->o_msgIPoc_1840.m_nCurrRef & ((1<<(SPS_MAX_POC_LSB))-1);
        }

        memcpy(&t.output, &(p->local->output_1857), sizeof(int)*99);
        memcpy(&t.o_msgSliceHeader, &(p->local->o_msgSliceHeader_1841), sizeof(SliceHeader));
        t.o_intFrameType = p->local->o_intFrameType_1839;
    } //phase 0 end

    { 
        {

            {  /*initI113.XMBInitI10 (class CGCXMBInit) */
                int i, j;
                int mb_phase = p->local->phase_99;

                int i_mb_x = mb_phase % SPS_MB_WIDTH;
                int i_mb_y = mb_phase / SPS_MB_WIDTH;
                uint8_t *p_plane;
                uint8_t *p_buf;

                p->local->o_MB_index= mb_phase;

                p->local->o_intLambda_1843 = i_qp0_cost_table[(FRAME_TYPE_IDR == p->local->o_intFrameType_1839) ? QP_CONSTANT_I : QP_CONSTANT];

                for (i = 0; i < 3; i++)
                {
                    const int w = (i == 0 ? 16 : 8);
                    const int i_stride = !i ? (176+64) : (176+64)/2;
                    p_plane = !i ? p->local->o_msgFrameData_1837.bufYData : ((i == 1) ? p->local->o_msgFrameData_1837.bufUData : p->local->o_msgFrameData_1837.bufVData);
                    p_buf = !i ? p->local->i_s_info.src_block_Y.data : ((i == 1) ? p->local->i_s_info.src_block_U.data : p->local->i_s_info.src_block_V.data);

                    for (j = 0; j < w; j++)
                        memcpy(&p_buf[j*w],  &p_plane[ w * ( i_mb_x + i_mb_y * i_stride ) + j * i_stride], w);
                }
            }

            {  /* initI113.XInverterI31 (class CGCXInverter) */
                p->local->i_s_info.intra_do_it = !p->local->output_1857[p->local->q_index];
            }

            memcpy(&t.i_s_info, &(p->local->i_s_info), sizeof(init_info));

            //signal VLC
            DAL_write((void*) PORT_OUT2, &terminate, sizeof(int), p);
            DAL_write( (void*)PORT_OUT1, &t, sizeof(InitMEPacket), p); // to ME


        }
    } /* end repeat, depth 2*/

    p->local->phase_99++;
    p->local->q_index++;

    if(p->local->phase_99 == 99)
    {
        p->local->phase_99 = 0;
        p->local->q_index = 0;
    }

    p->local->counter++;

	return 0; //successful execution
}

void init_finish(DALProcess *p) {
    fclose(p->local->fpYUV_1872);
}
