#ifndef INIT_H
#define INIT_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <dal.h>

#include "structs.h"

#define PORT_OUT1 1
#define PORT_OUT2 2


/*********************
  VARIOUS DEFINITIONS
**********************/

#define REFERENCE_FRAME_COUNT 1
#define MAX_GOP_SIZE		250
#define MIN_GOP_SIZE		25
#define SCENE_CUT			40 // How aggressively to insert extra I-frames
#define MIN_QP_VALUE		10
#define MAX_QP_VALUE		51
#define BITRATE_VALUE		1000
#define MAX_QP_STEP			4
#define MAX_ME_RANGE		16
#define SUBPIXEL_ME_P		5
#define FPS_NUM				30
#define FPS_DEN				2
#define RATE_TOLERANCE		1.0
#define IP_FACTOR			1.4

#define LEVEL_IDC			51
#define LEVEL_FRAME_SIZE	36864 	// max frame size (macroblocks)
#define LEVEL_DPB			70778880 // max decoded picture buffer (bytes)
#define LEVEL_MBPS			983040   // max macroblock processing rate (macroblocks/sec)
#define LEVEL_MV_RANGE		512

#define FILE_NAME "../app1/src/FOREMAN.QCIF"
#define FRAME_TYPE_IDR		0
#define FRAME_TYPE_P		1

#define SPS_CROP_RIGHT		((-176)&5)
#define SPS_CROP_BOTTOM		((-144)&15)
#define SPS_B_CROP			(SPS_CROP_RIGHT||SPS_CROP_BOTTOM)
#define SPS_MAX_FRAME_NUM	8
#define SPS_MAX_POC_LSB		(SPS_MAX_FRAME_NUM+1)

#define SPS_MB_WIDTH ((176+15)/16)    
#define SPS_MB_HEIGHT ((144+15)/16)
#define QP_CONSTANT 26
#define QP_CONSTANT_I 23


/***********************
  CONSTANT DECLARATIONS
***********************/

static const int i_qp0_cost_table[52] = 
{
    1, 1, 1, 1, 1, 1, 1, 1,  /*  0-7 */
    1, 1, 1, 1,              /*  8-11 */
    1, 1, 1, 1, 2, 2, 2, 2,  /* 12-19 */
    3, 3, 3, 4, 4, 4, 5, 6,  /* 20-27 */
    6, 7, 8, 9,10,11,13,14,  /* 28-35 */
    16,18,20,23,25,29,32,36,  /* 36-43 */
    40,45,51,57,64,72,81,91   /* 44-51 */
};


/*********************
  STRUCT DECLARATIONS
**********************/
typedef struct
{
    uint8_t bufYData[(176+64) * 144];
    uint8_t bufUData[(176/2+32) * 144/2];
    uint8_t bufVData[(176/2+32) * 144/2];
} YUVFrameData;

typedef struct { int m_nCurrFrame; int m_nCurrRef; int m_nPrevRef; } IPoc;

typedef struct
{
    int i_type;
    int i_first_mb;
    int i_last_mb;		
    int i_frame_num;		
    int i_idr_pic_id;
    int i_poc_lsb;		
    int b_num_ref_idx_override;		
    int i_qp;
    int i_qp_delta;		
    int i_disable_deblocking_filter_idc;		
} SliceHeader;

typedef struct{
    int output[99];
    int o_intFrameType;
    SliceHeader o_msgSliceHeader;
    init_info i_s_info;
} InitMEPacket;


/****************************
  LOCAL STATE OF THE PROCESS
*****************************/

typedef struct _local_states {

	FILE *fpYUV_1872;
	int g_iFrame_1873;
	int i_last_idr_1874;
	int i_idr_pic_id;


	YUVFrameData o_msgFrameData_1837;
	int o_intFrameNum_1838;
	int o_intFrameType_1839;
	IPoc o_msgIPoc_1840;
	SliceHeader o_msgSliceHeader_1841;
	int o_intLambda_1843;
	struct Uint8_tArr256 o_msgYBlock_1844;
	struct Uint8_tArr64 o_msgUBlock_1845;
	struct Uint8_tArr64 o_msgVBlock_1846;
	int output_1854;
	int output_1857[99];
	int o_MB_index;
	init_info i_s_info;

	//variables for FIRE only
	int phase_99;
	int q_index;

//////////////////
    int iteration;
	int counter;
    struct rusage start, end;
    long long myutime[25];
    long long mystime[25];
    struct timeval total_start, total_end;
/////////////////

} init_State;

void init_init(DALProcess *);
int init_fire(DALProcess *);
void init_finish(DALProcess *);

#endif
