#ifndef VLC_H
#define VLC_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <dal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "x264.h"
#include "structs.h"

#define PORT_IN1 1
#define PORT_IN2 2
#define EVENT_DONE "stop_state1"

/****************************
  DEFINITIONS AND CONSTANTS
*****************************/

#define REFERENCE_FRAME_COUNT 1
#define MAX_GOP_SIZE		250
#define MIN_GOP_SIZE		25
#define SCENE_CUT			40 // How aggressively to insert extra I-frames
#define MIN_QP_VALUE		10
#define MAX_QP_VALUE		51
#define BITRATE_VALUE		1000
#define MAX_QP_STEP			4
#define MAX_ME_RANGE		16
#define SUBPIXEL_ME_P		5
#define FPS_NUM				30
#define FPS_DEN				2
#define RATE_TOLERANCE		1.0
#define IP_FACTOR			1.4

#define LEVEL_IDC			51
#define LEVEL_FRAME_SIZE	36864 	// max frame size (macroblocks)
#define LEVEL_DPB			70778880 // max decoded picture buffer (bytes)
#define LEVEL_MBPS			983040   // max macroblock processing rate (macroblocks/sec)
#define LEVEL_MV_RANGE		512

#define FRAME_TYPE_IDR		0
#define FRAME_TYPE_P		1

#define SPS_MB_WIDTH ((176+15)/16) 
#define SPS_MB_HEIGHT ((144+15)/16)

#define SPS_CROP_RIGHT		((-176)&5)
#define SPS_CROP_BOTTOM		((-144)&15)
#define SPS_B_CROP			(SPS_CROP_RIGHT||SPS_CROP_BOTTOM)
#define SPS_MAX_FRAME_NUM	8
#define SPS_MAX_POC_LSB		(SPS_MAX_FRAME_NUM+1)
#define X264_MIN(a,b) ( (a)<(b) ? (a) : (b) )
#define X264_MAX(a,b) ( (a)>(b) ? (a) : (b) )

#define X264_ANALYSE_I4x4       0x0001  /* Analyse i4x4 */
#define X264_ANALYSE_PSUB16x16  0x0010  /* Analyse p16x8, p8x16 and p8x8 */
#define X264_ANALYSE_PSUB8x8    0x0020  /* Analyse p8x4, p4x8, p4x4 */
#define X264_ANALYSE_INTRA 		(X264_ANALYSE_I4x4)
#define X264_ANALYSE_INTER 		(X264_ANALYSE_I4x4 | X264_ANALYSE_PSUB16x16 | X264_ANALYSE_PSUB8x8)
#define DATA_MAX 3000000
#define QP_CONSTANT 26
#define QP_CONSTANT_I 23

#define BLOCK_INDEX_CHROMA_DC   (-1)
#define BLOCK_INDEX_LUMA_DC     (-2)

static const int x264_mb_pred_mode4x4_fix[13] =
{
    -1,
    I_PRED_4x4_V,   I_PRED_4x4_H,   I_PRED_4x4_DC,
    I_PRED_4x4_DDL, I_PRED_4x4_DDR, I_PRED_4x4_VR,
    I_PRED_4x4_HD,  I_PRED_4x4_VL,  I_PRED_4x4_HU,
    I_PRED_4x4_DC,  I_PRED_4x4_DC,  I_PRED_4x4_DC
};

#define x264_mb_pred_mode4x4_fix(t) x264_mb_pred_mode4x4_fix[(t)+1]	

typedef struct
{
    int i_bits;
    int i_size;
} vlc_t;

/* XXX: don't forget to change it if you change vlc_t */
#define MKVLC( a, b ) { a, b }
static const vlc_t x264_coeff_token[5][17*4] =
{
    /* table 0 */
    {
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x5, 6 ), /* str=000101 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x7, 8 ), /* str=00000111 */
        MKVLC( 0x4, 6 ), /* str=000100 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x7, 9 ), /* str=000000111 */
        MKVLC( 0x6, 8 ), /* str=00000110 */
        MKVLC( 0x5, 7 ), /* str=0000101 */
        MKVLC( 0x3, 5 ), /* str=00011 */

        MKVLC( 0x7, 10 ), /* str=0000000111 */
        MKVLC( 0x6, 9 ), /* str=000000110 */
        MKVLC( 0x5, 8 ), /* str=00000101 */
        MKVLC( 0x3, 6 ), /* str=000011 */

        MKVLC( 0x7, 11 ), /* str=00000000111 */
        MKVLC( 0x6, 10 ), /* str=0000000110 */
        MKVLC( 0x5, 9 ), /* str=000000101 */
        MKVLC( 0x4, 7 ), /* str=0000100 */

        MKVLC( 0xf, 13 ), /* str=0000000001111 */
        MKVLC( 0x6, 11 ), /* str=00000000110 */
        MKVLC( 0x5, 10 ), /* str=0000000101 */
        MKVLC( 0x4, 8 ), /* str=00000100 */

        MKVLC( 0xb, 13 ), /* str=0000000001011 */
        MKVLC( 0xe, 13 ), /* str=0000000001110 */
        MKVLC( 0x5, 11 ), /* str=00000000101 */
        MKVLC( 0x4, 9 ), /* str=000000100 */

        MKVLC( 0x8, 13 ), /* str=0000000001000 */
        MKVLC( 0xa, 13 ), /* str=0000000001010 */
        MKVLC( 0xd, 13 ), /* str=0000000001101 */
        MKVLC( 0x4, 10 ), /* str=0000000100 */

        MKVLC( 0xf, 14 ), /* str=00000000001111 */
        MKVLC( 0xe, 14 ), /* str=00000000001110 */
        MKVLC( 0x9, 13 ), /* str=0000000001001 */
        MKVLC( 0x4, 11 ), /* str=00000000100 */

        MKVLC( 0xb, 14 ), /* str=00000000001011 */
        MKVLC( 0xa, 14 ), /* str=00000000001010 */
        MKVLC( 0xd, 14 ), /* str=00000000001101 */
        MKVLC( 0xc, 13 ), /* str=0000000001100 */

        MKVLC( 0xf, 15 ), /* str=000000000001111 */
        MKVLC( 0xe, 15 ), /* str=000000000001110 */
        MKVLC( 0x9, 14 ), /* str=00000000001001 */
        MKVLC( 0xc, 14 ), /* str=00000000001100 */

        MKVLC( 0xb, 15 ), /* str=000000000001011 */
        MKVLC( 0xa, 15 ), /* str=000000000001010 */
        MKVLC( 0xd, 15 ), /* str=000000000001101 */
        MKVLC( 0x8, 14 ), /* str=00000000001000 */

        MKVLC( 0xf, 16 ), /* str=0000000000001111 */
        MKVLC( 0x1, 15 ), /* str=000000000000001 */
        MKVLC( 0x9, 15 ), /* str=000000000001001 */
        MKVLC( 0xc, 15 ), /* str=000000000001100 */

        MKVLC( 0xb, 16 ), /* str=0000000000001011 */
        MKVLC( 0xe, 16 ), /* str=0000000000001110 */
        MKVLC( 0xd, 16 ), /* str=0000000000001101 */
        MKVLC( 0x8, 15 ), /* str=000000000001000 */

        MKVLC( 0x7, 16 ), /* str=0000000000000111 */
        MKVLC( 0xa, 16 ), /* str=0000000000001010 */
        MKVLC( 0x9, 16 ), /* str=0000000000001001 */
        MKVLC( 0xc, 16 ), /* str=0000000000001100 */

        MKVLC( 0x4, 16 ), /* str=0000000000000100 */
        MKVLC( 0x6, 16 ), /* str=0000000000000110 */
        MKVLC( 0x5, 16 ), /* str=0000000000000101 */
        MKVLC( 0x8, 16 ), /* str=0000000000001000 */
    },

    /* table 1 */
    {
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0xb, 6 ), /* str=001011 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x7, 6 ), /* str=000111 */
        MKVLC( 0x7, 5 ), /* str=00111 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x7, 7 ), /* str=0000111 */
        MKVLC( 0xa, 6 ), /* str=001010 */
        MKVLC( 0x9, 6 ), /* str=001001 */
        MKVLC( 0x5, 4 ), /* str=0101 */

        MKVLC( 0x7, 8 ), /* str=00000111 */
        MKVLC( 0x6, 6 ), /* str=000110 */
        MKVLC( 0x5, 6 ), /* str=000101 */
        MKVLC( 0x4, 4 ), /* str=0100 */

        MKVLC( 0x4, 8 ), /* str=00000100 */
        MKVLC( 0x6, 7 ), /* str=0000110 */
        MKVLC( 0x5, 7 ), /* str=0000101 */
        MKVLC( 0x6, 5 ), /* str=00110 */

        MKVLC( 0x7, 9 ), /* str=000000111 */
        MKVLC( 0x6, 8 ), /* str=00000110 */
        MKVLC( 0x5, 8 ), /* str=00000101 */
        MKVLC( 0x8, 6 ), /* str=001000 */

        MKVLC( 0xf, 11 ), /* str=00000001111 */
        MKVLC( 0x6, 9 ), /* str=000000110 */
        MKVLC( 0x5, 9 ), /* str=000000101 */
        MKVLC( 0x4, 6 ), /* str=000100 */

        MKVLC( 0xb, 11 ), /* str=00000001011 */
        MKVLC( 0xe, 11 ), /* str=00000001110 */
        MKVLC( 0xd, 11 ), /* str=00000001101 */
        MKVLC( 0x4, 7 ), /* str=0000100 */

        MKVLC( 0xf, 12 ), /* str=000000001111 */
        MKVLC( 0xa, 11 ), /* str=00000001010 */
        MKVLC( 0x9, 11 ), /* str=00000001001 */
        MKVLC( 0x4, 9 ), /* str=000000100 */

        MKVLC( 0xb, 12 ), /* str=000000001011 */
        MKVLC( 0xe, 12 ), /* str=000000001110 */
        MKVLC( 0xd, 12 ), /* str=000000001101 */
        MKVLC( 0xc, 11 ), /* str=00000001100 */

        MKVLC( 0x8, 12 ), /* str=000000001000 */
        MKVLC( 0xa, 12 ), /* str=000000001010 */
        MKVLC( 0x9, 12 ), /* str=000000001001 */
        MKVLC( 0x8, 11 ), /* str=00000001000 */

        MKVLC( 0xf, 13 ), /* str=0000000001111 */
        MKVLC( 0xe, 13 ), /* str=0000000001110 */
        MKVLC( 0xd, 13 ), /* str=0000000001101 */
        MKVLC( 0xc, 12 ), /* str=000000001100 */

        MKVLC( 0xb, 13 ), /* str=0000000001011 */
        MKVLC( 0xa, 13 ), /* str=0000000001010 */
        MKVLC( 0x9, 13 ), /* str=0000000001001 */
        MKVLC( 0xc, 13 ), /* str=0000000001100 */

        MKVLC( 0x7, 13 ), /* str=0000000000111 */
        MKVLC( 0xb, 14 ), /* str=00000000001011 */
        MKVLC( 0x6, 13 ), /* str=0000000000110 */
        MKVLC( 0x8, 13 ), /* str=0000000001000 */

        MKVLC( 0x9, 14 ), /* str=00000000001001 */
        MKVLC( 0x8, 14 ), /* str=00000000001000 */
        MKVLC( 0xa, 14 ), /* str=00000000001010 */
        MKVLC( 0x1, 13 ), /* str=0000000000001 */

        MKVLC( 0x7, 14 ), /* str=00000000000111 */
        MKVLC( 0x6, 14 ), /* str=00000000000110 */
        MKVLC( 0x5, 14 ), /* str=00000000000101 */
        MKVLC( 0x4, 14 ), /* str=00000000000100 */
    },
    /* table 2 */
    {
        MKVLC( 0xf, 4 ), /* str=1111 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0xf, 6 ), /* str=001111 */
        MKVLC( 0xe, 4 ), /* str=1110 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0xb, 6 ), /* str=001011 */
        MKVLC( 0xf, 5 ), /* str=01111 */
        MKVLC( 0xd, 4 ), /* str=1101 */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x8, 6 ), /* str=001000 */
        MKVLC( 0xc, 5 ), /* str=01100 */
        MKVLC( 0xe, 5 ), /* str=01110 */
        MKVLC( 0xc, 4 ), /* str=1100 */

        MKVLC( 0xf, 7 ), /* str=0001111 */
        MKVLC( 0xa, 5 ), /* str=01010 */
        MKVLC( 0xb, 5 ), /* str=01011 */
        MKVLC( 0xb, 4 ), /* str=1011 */

        MKVLC( 0xb, 7 ), /* str=0001011 */
        MKVLC( 0x8, 5 ), /* str=01000 */
        MKVLC( 0x9, 5 ), /* str=01001 */
        MKVLC( 0xa, 4 ), /* str=1010 */

        MKVLC( 0x9, 7 ), /* str=0001001 */
        MKVLC( 0xe, 6 ), /* str=001110 */
        MKVLC( 0xd, 6 ), /* str=001101 */
        MKVLC( 0x9, 4 ), /* str=1001 */

        MKVLC( 0x8, 7 ), /* str=0001000 */
        MKVLC( 0xa, 6 ), /* str=001010 */
        MKVLC( 0x9, 6 ), /* str=001001 */
        MKVLC( 0x8, 4 ), /* str=1000 */

        MKVLC( 0xf, 8 ), /* str=00001111 */
        MKVLC( 0xe, 7 ), /* str=0001110 */
        MKVLC( 0xd, 7 ), /* str=0001101 */
        MKVLC( 0xd, 5 ), /* str=01101 */

        MKVLC( 0xb, 8 ), /* str=00001011 */
        MKVLC( 0xe, 8 ), /* str=00001110 */
        MKVLC( 0xa, 7 ), /* str=0001010 */
        MKVLC( 0xc, 6 ), /* str=001100 */

        MKVLC( 0xf, 9 ), /* str=000001111 */
        MKVLC( 0xa, 8 ), /* str=00001010 */
        MKVLC( 0xd, 8 ), /* str=00001101 */
        MKVLC( 0xc, 7 ), /* str=0001100 */

        MKVLC( 0xb, 9 ), /* str=000001011 */
        MKVLC( 0xe, 9 ), /* str=000001110 */
        MKVLC( 0x9, 8 ), /* str=00001001 */
        MKVLC( 0xc, 8 ), /* str=00001100 */

        MKVLC( 0x8, 9 ), /* str=000001000 */
        MKVLC( 0xa, 9 ), /* str=000001010 */
        MKVLC( 0xd, 9 ), /* str=000001101 */
        MKVLC( 0x8, 8 ), /* str=00001000 */

        MKVLC( 0xd, 10 ), /* str=0000001101 */
        MKVLC( 0x7, 9 ), /* str=000000111 */
        MKVLC( 0x9, 9 ), /* str=000001001 */
        MKVLC( 0xc, 9 ), /* str=000001100 */

        MKVLC( 0x9, 10 ), /* str=0000001001 */
        MKVLC( 0xc, 10 ), /* str=0000001100 */
        MKVLC( 0xb, 10 ), /* str=0000001011 */
        MKVLC( 0xa, 10 ), /* str=0000001010 */

        MKVLC( 0x5, 10 ), /* str=0000000101 */
        MKVLC( 0x8, 10 ), /* str=0000001000 */
        MKVLC( 0x7, 10 ), /* str=0000000111 */
        MKVLC( 0x6, 10 ), /* str=0000000110 */

        MKVLC( 0x1, 10 ), /* str=0000000001 */
        MKVLC( 0x4, 10 ), /* str=0000000100 */
        MKVLC( 0x3, 10 ), /* str=0000000011 */
        MKVLC( 0x2, 10 ), /* str=0000000010 */
    },

    /* table 3 */
    {
        MKVLC( 0x3, 6 ), /* str=000011 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x4, 6 ), /* str=000100 */
        MKVLC( 0x5, 6 ), /* str=000101 */
        MKVLC( 0x6, 6 ), /* str=000110 */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x8, 6 ), /* str=001000 */
        MKVLC( 0x9, 6 ), /* str=001001 */
        MKVLC( 0xa, 6 ), /* str=001010 */
        MKVLC( 0xb, 6 ), /* str=001011 */

        MKVLC( 0xc, 6 ), /* str=001100 */
        MKVLC( 0xd, 6 ), /* str=001101 */
        MKVLC( 0xe, 6 ), /* str=001110 */
        MKVLC( 0xf, 6 ), /* str=001111 */

        MKVLC( 0x10, 6 ), /* str=010000 */
        MKVLC( 0x11, 6 ), /* str=010001 */
        MKVLC( 0x12, 6 ), /* str=010010 */
        MKVLC( 0x13, 6 ), /* str=010011 */

        MKVLC( 0x14, 6 ), /* str=010100 */
        MKVLC( 0x15, 6 ), /* str=010101 */
        MKVLC( 0x16, 6 ), /* str=010110 */
        MKVLC( 0x17, 6 ), /* str=010111 */

        MKVLC( 0x18, 6 ), /* str=011000 */
        MKVLC( 0x19, 6 ), /* str=011001 */
        MKVLC( 0x1a, 6 ), /* str=011010 */
        MKVLC( 0x1b, 6 ), /* str=011011 */

        MKVLC( 0x1c, 6 ), /* str=011100 */
        MKVLC( 0x1d, 6 ), /* str=011101 */
        MKVLC( 0x1e, 6 ), /* str=011110 */
        MKVLC( 0x1f, 6 ), /* str=011111 */

        MKVLC( 0x20, 6 ), /* str=100000 */
        MKVLC( 0x21, 6 ), /* str=100001 */
        MKVLC( 0x22, 6 ), /* str=100010 */
        MKVLC( 0x23, 6 ), /* str=100011 */

        MKVLC( 0x24, 6 ), /* str=100100 */
        MKVLC( 0x25, 6 ), /* str=100101 */
        MKVLC( 0x26, 6 ), /* str=100110 */
        MKVLC( 0x27, 6 ), /* str=100111 */

        MKVLC( 0x28, 6 ), /* str=101000 */
        MKVLC( 0x29, 6 ), /* str=101001 */
        MKVLC( 0x2a, 6 ), /* str=101010 */
        MKVLC( 0x2b, 6 ), /* str=101011 */

        MKVLC( 0x2c, 6 ), /* str=101100 */
        MKVLC( 0x2d, 6 ), /* str=101101 */
        MKVLC( 0x2e, 6 ), /* str=101110 */
        MKVLC( 0x2f, 6 ), /* str=101111 */

        MKVLC( 0x30, 6 ), /* str=110000 */
        MKVLC( 0x31, 6 ), /* str=110001 */
        MKVLC( 0x32, 6 ), /* str=110010 */
        MKVLC( 0x33, 6 ), /* str=110011 */

        MKVLC( 0x34, 6 ), /* str=110100 */
        MKVLC( 0x35, 6 ), /* str=110101 */
        MKVLC( 0x36, 6 ), /* str=110110 */
        MKVLC( 0x37, 6 ), /* str=110111 */

        MKVLC( 0x38, 6 ), /* str=111000 */
        MKVLC( 0x39, 6 ), /* str=111001 */
        MKVLC( 0x3a, 6 ), /* str=111010 */
        MKVLC( 0x3b, 6 ), /* str=111011 */

        MKVLC( 0x3c, 6 ), /* str=111100 */
        MKVLC( 0x3d, 6 ), /* str=111101 */
        MKVLC( 0x3e, 6 ), /* str=111110 */
        MKVLC( 0x3f, 6 ), /* str=111111 */
    },

    /* table 4 */
    {
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x7, 6 ), /* str=000111 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x4, 6 ), /* str=000100 */
        MKVLC( 0x6, 6 ), /* str=000110 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x3, 6 ), /* str=000011 */
        MKVLC( 0x3, 7 ), /* str=0000011 */
        MKVLC( 0x2, 7 ), /* str=0000010 */
        MKVLC( 0x5, 6 ), /* str=000101 */

        MKVLC( 0x2, 6 ), /* str=000010 */
        MKVLC( 0x3, 8 ), /* str=00000011 */
        MKVLC( 0x2, 8 ), /* str=00000010 */
        MKVLC( 0x0, 7 ), /* str=0000000 */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */

        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    }
};

static const vlc_t x264_level_prefix[16] =
{
    MKVLC( 0x01,  1 ),
    MKVLC( 0x01,  2 ),
    MKVLC( 0x01,  3 ),
    MKVLC( 0x01,  4 ),
    MKVLC( 0x01,  5 ),
    MKVLC( 0x01,  6 ),
    MKVLC( 0x01,  7 ),
    MKVLC( 0x01,  8 ),
    MKVLC( 0x01,  9 ),
    MKVLC( 0x01, 10 ),
    MKVLC( 0x01, 11 ),
    MKVLC( 0x01, 12 ),
    MKVLC( 0x01, 13 ),
    MKVLC( 0x01, 14 ),
    MKVLC( 0x01, 15 ),
    MKVLC( 0x01, 16 )
};

/* [i_total_coeff-1][i_total_zeros] */
static const vlc_t x264_total_zeros[15][16] =
{
    { /* i_total 1 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x3, 4 ), /* str=0011 */
        MKVLC( 0x2, 4 ), /* str=0010 */
        MKVLC( 0x3, 5 ), /* str=00011 */
        MKVLC( 0x2, 5 ), /* str=00010 */
        MKVLC( 0x3, 6 ), /* str=000011 */
        MKVLC( 0x2, 6 ), /* str=000010 */
        MKVLC( 0x3, 7 ), /* str=0000011 */
        MKVLC( 0x2, 7 ), /* str=0000010 */
        MKVLC( 0x3, 8 ), /* str=00000011 */
        MKVLC( 0x2, 8 ), /* str=00000010 */
        MKVLC( 0x3, 9 ), /* str=000000011 */
        MKVLC( 0x2, 9 ), /* str=000000010 */
        MKVLC( 0x1, 9 ), /* str=000000001 */
    },
    { /* i_total 2 */
        MKVLC( 0x7, 3 ), /* str=111 */
        MKVLC( 0x6, 3 ), /* str=110 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x5, 4 ), /* str=0101 */
        MKVLC( 0x4, 4 ), /* str=0100 */
        MKVLC( 0x3, 4 ), /* str=0011 */
        MKVLC( 0x2, 4 ), /* str=0010 */
        MKVLC( 0x3, 5 ), /* str=00011 */
        MKVLC( 0x2, 5 ), /* str=00010 */
        MKVLC( 0x3, 6 ), /* str=000011 */
        MKVLC( 0x2, 6 ), /* str=000010 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 3 */
        MKVLC( 0x5, 4 ), /* str=0101 */
        MKVLC( 0x7, 3 ), /* str=111 */
        MKVLC( 0x6, 3 ), /* str=110 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 4 ), /* str=0100 */
        MKVLC( 0x3, 4 ), /* str=0011 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 4 ), /* str=0010 */
        MKVLC( 0x3, 5 ), /* str=00011 */
        MKVLC( 0x2, 5 ), /* str=00010 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 4 */
        MKVLC( 0x3, 5 ), /* str=00011 */
        MKVLC( 0x7, 3 ), /* str=111 */
        MKVLC( 0x5, 4 ), /* str=0101 */
        MKVLC( 0x4, 4 ), /* str=0100 */
        MKVLC( 0x6, 3 ), /* str=110 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 4 ), /* str=0011 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 4 ), /* str=0010 */
        MKVLC( 0x2, 5 ), /* str=00010 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x0, 5 ), /* str=00000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 5 */
        MKVLC( 0x5, 4 ), /* str=0101 */
        MKVLC( 0x4, 4 ), /* str=0100 */
        MKVLC( 0x3, 4 ), /* str=0011 */
        MKVLC( 0x7, 3 ), /* str=111 */
        MKVLC( 0x6, 3 ), /* str=110 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 4 ), /* str=0010 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x0, 5 ), /* str=00000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 6 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x7, 3 ), /* str=111 */
        MKVLC( 0x6, 3 ), /* str=110 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 7 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 8 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 9 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x0, 6 ), /* str=000000 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 10 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x0, 5 ), /* str=00000 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 11 */
        MKVLC( 0x0, 4 ), /* str=0000 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 12 */
        MKVLC( 0x0, 4 ), /* str=0000 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 13 */
        MKVLC( 0x0, 3 ), /* str=000 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 14 */
        MKVLC( 0x0, 2 ), /* str=00 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_total 15 */
        MKVLC( 0x0, 1 ), /* str=0 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
};

/* [i_total_coeff-1][i_total_zeros] */
static const vlc_t x264_total_zeros_dc[3][4] =
{
    {
        MKVLC( 0x01, 1 ), /* 1  */
        MKVLC( 0x01, 2 ), /* 01 */
        MKVLC( 0x01, 3 ), /* 001*/
        MKVLC( 0x00, 3 )  /* 000*/
    },
    {
        MKVLC( 0x01, 1 ), /* 1  */
        MKVLC( 0x01, 2 ), /* 01 */
        MKVLC( 0x00, 2 ), /* 00 */
        MKVLC( 0x00, 0 )  /*    */
    },
    {
        MKVLC( 0x01, 1 ), /* 1  */
        MKVLC( 0x00, 1 ), /* 0  */
        MKVLC( 0x00, 0 ), /*    */
        MKVLC( 0x00, 0 )  /*    */
    }
};

/* x264_run_before[__MIN( i_zero_left -1, 6 )][run_before] */
static const vlc_t x264_run_before[7][15] =
{
    { /* i_zero_left 1 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x0, 1 ), /* str=0 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_zero_left 2 */
        MKVLC( 0x1, 1 ), /* str=1 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x0, 2 ), /* str=00 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_zero_left 3 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x0, 2 ), /* str=00 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_zero_left 4 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x1, 2 ), /* str=01 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 3 ), /* str=000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_zero_left 5 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x2, 2 ), /* str=10 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x0, 3 ), /* str=000 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_zero_left 6 */
        MKVLC( 0x3, 2 ), /* str=11 */
        MKVLC( 0x0, 3 ), /* str=000 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
        MKVLC( 0x0, 0 ), /* str= */
    },
    { /* i_zero_left 7 */
        MKVLC( 0x7, 3 ), /* str=111 */
        MKVLC( 0x6, 3 ), /* str=110 */
        MKVLC( 0x5, 3 ), /* str=101 */
        MKVLC( 0x4, 3 ), /* str=100 */
        MKVLC( 0x3, 3 ), /* str=011 */
        MKVLC( 0x2, 3 ), /* str=010 */
        MKVLC( 0x1, 3 ), /* str=001 */
        MKVLC( 0x1, 4 ), /* str=0001 */
        MKVLC( 0x1, 5 ), /* str=00001 */
        MKVLC( 0x1, 6 ), /* str=000001 */
        MKVLC( 0x1, 7 ), /* str=0000001 */
        MKVLC( 0x1, 8 ), /* str=00000001 */
        MKVLC( 0x1, 9 ), /* str=000000001 */
        MKVLC( 0x1, 10 ), /* str=0000000001 */
        MKVLC( 0x1, 11 ), /* str=00000000001 */
    },
};

static const int x264_mb_partition_listX_table[2][17] =
{
    {
        1, 1, 1, 1, /* D_L0_* */
        0, 0, 0, 0, /* D_L1_* */
        1, 1, 1, 1, /* D_BI_* */
        0,          /* D_DIRECT_8x8 */
        0, 0, 0, 0  /* 8x8 .. 16x16 */
    },
    {
        0, 0, 0, 0, /* D_L0_* */
        1, 1, 1, 1, /* D_L1_* */
        1, 1, 1, 1, /* D_BI_* */
        0,          /* D_DIRECT_8x8 */
        0, 0, 0, 0  /* 8x8 .. 16x16 */
    }
};

static const uint8_t intra4x4_cbp_to_golomb[48]=
{
    3, 29, 30, 17, 31, 18, 37,  8, 32, 38, 19,  9, 20, 10, 11,  2,
    16, 33, 34, 21, 35, 22, 39,  4, 36, 40, 23,  5, 24,  6,  7,  1,
    41, 42, 43, 25, 44, 26, 46, 12, 45, 47, 27, 13, 28, 14, 15,  0
};

static const uint8_t inter_cbp_to_golomb[48]=
{
    0,  2,  3,  7,  4,  8, 17, 13,  5, 18,  9, 14, 10, 15, 16, 11,
    1, 32, 33, 36, 34, 37, 44, 40, 35, 45, 38, 41, 39, 42, 43, 19,
    6, 24, 25, 20, 26, 21, 46, 28, 27, 47, 22, 29, 23, 30, 31, 12
};

static const uint8_t mb_type_b_to_golomb[3][9]=
{
    { 4,  8, 12, 10,  6, 14, 16, 18, 20 }, /* D_16x8 */
    { 5,  9, 13, 11,  7, 15, 17, 19, 21 }, /* D_8x16 */
    { 1, -1, -1, -1,  2, -1, -1, -1,  3 }  /* D_16x16 */
};

static const uint8_t sub_mb_type_p_to_golomb[4]=
{
    3, 1, 2, 0
};

static const uint8_t sub_mb_type_b_to_golomb[13]=
{
    10,  4,  5,  1, 11,  6,  7,  2, 12,  8,  9,  3,  0
};

/*********************
  STRUCT DECLARATIONS
**********************/

typedef struct
{
    int i_type;
    int i_first_mb;
    int i_last_mb;		
    int i_frame_num;		
    int i_idr_pic_id;
    int i_poc_lsb;		
    int b_num_ref_idx_override;		
    int i_qp;
    int i_qp_delta;		
    int i_disable_deblocking_filter_idc;		
} SliceHeader;

typedef struct{
    int i_cbp_luma;
    int i_cbp_chroma;
    struct IntBlock16x16 luma4x4_out;
    struct IntArr16x15 luma_residual_ac;
    struct IntArr48 non_zero_count;
    struct IntArr8x15 chroma_residual_ac;
    struct IntArr2x4 chroma_dc;
    struct IntArr16 luma4x4;
    SliceHeader msgSliceHeader;
    int intFrameType;
    encoder_send_info e_s_info;
} EncVLCPacket;

typedef struct
{
    int size;
    uint8_t data[176*144];
} bs_data;

typedef struct
{
    int i_ref_idc;  /* nal_priority_e */
    int i_type;     /* nal_unit_type_e */

    /* This data are raw payload */
    int     i_payload;
    uint8_t *p_payload;
} x264_nal_t;

typedef struct bs_s
{
    uint8_t *p_start;
    uint8_t *p;
    uint8_t *p_end;

    int     i_left;    /* i_count number of available bits */
    int     i_bits_encoded; /* RD only */
} bs_t;

/****************************
  LOCAL STATE OF THE PROCESS
*****************************/

typedef struct _local_states {
	FILE *fp264_1986;
	int i_intFrameType_1967_phase;
	int i_intMBType_1968;
	int i_intIntra16x16_pred_mode_1969;
	int i_msgIntra4x4_pred_mode_1970;
	int i_msgRef_1971;
	int i_msgMV_1972;
	int i_msgNon_zero_count_1973;
	int i_intCbp_luma_1974;
	int i_intCbp_chroma_1975;
	int i_intChroma_pred_mode_1976;
	int i_intPartition_1977;
	int i_msgSub_partition_1978;
	int i_msgLuma16x16_dc_1979;
	int i_msgLuma4x4_1980;
	int i_msgChroma_dc_1981;
	int i_msgResidual_luma_ac_1982;
	int i_msgResidual_chroma_ac_1983;
	int i_msgSliceHeader_1984_phase;
	bs_data o_msgBS_data_1947;
	int o_msgBS_data_1985_phase;
	int output_1948;
	int output_1987_phase;
	int output_1949[99];
	int output_1989;
	int output_1950[99];
	int output_1991;
	struct IntArr48 output_1951[99];
	int output_1993;
	struct ref_mb output_1952[99];
	int output_1995;
	struct mv_mb output_1953[99];
	int output_1997;
	struct IntArr48 output_1954[99];
	int output_1999;
	int output_1955[99];
	int output_2001;
	int output_1956[99];
	int output_2003;
	int output_1957[99];
	int output_2005;
	int output_1958[99];
	int output_2007;
	struct IntArr4 output_1959[99];
	int output_2009;
	struct IntArr16 output_1960[99];
	int output_2011;
	struct IntBlock16x16 output_1961[99];
	int output_2013;
	struct IntArr2x4 output_1962[99];
	int output_2015;
	struct IntArr16x15 output_1963[99];
	int output_2017;
	struct IntArr8x15 output_1964[99];
	int output_2019;
	SliceHeader output_1965;
	int output_2021_phase;
	int output_1966;
	encoder_send_info vlc_r_info;
	int sdfLoopCounter_31;
	bs_t bitstream;
    int i_skip;

//////////////////
    int counter;
    struct rusage start, end;
    long long myutime[25];
    long long mystime[25];
/////////////////
} VLC_State;

void VLC_init(DALProcess *);
int VLC_fire(DALProcess *);
void VLC_finish(DALProcess *);

#endif
