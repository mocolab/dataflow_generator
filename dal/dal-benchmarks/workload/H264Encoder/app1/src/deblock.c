#include "deblock.h"

static int get_MB_Available(int x, int y, int* MB_Available) {
	if (x < 0 || x >= SPS_MB_WIDTH)
		return 1;
	if (y < 0 || y >= SPS_MB_HEIGHT)
		return 1;
	if (MB_Available[x + y * SPS_MB_WIDTH] == 1)
		return 1;
	else
		return 0;
}

void MB_finder(int mb_index, int Y_stride, int * MB_Available, DALProcess * p) {
	int mb_x = mb_index % Y_stride;
	int mb_y = mb_index / Y_stride;

	p->local->R_Available = p->local->LD_Available = 0;

	if (get_MB_Available(mb_x + 2, mb_y - 1, p->local->MB_Available) == 1) {
		if (mb_x + 1 < SPS_MB_WIDTH)
			p->local->R_Available = 1;
	}
	if (get_MB_Available(mb_x - 2, mb_y + 1, p->local->MB_Available) == 1) {
		if (mb_x - 1 >= 0 && mb_y + 1 < SPS_MB_HEIGHT)
			p->local->LD_Available = 1;
	}
}

static void construct_luma_frm(int mb_xy, int mb_width, int stride,
		uint8_t recon_block[256], DALProcess * p) {
	int i;
	int x_pos = mb_xy % mb_width;
	int y_pos = mb_xy / mb_width;

	for (i = 0; i < 16; i++)
		memcpy(
				p->local->p_Y_frm_buf_1663 + (16 * y_pos + i) * stride
						+ (16 * x_pos), &recon_block[i * 16],
				sizeof(uint8_t) * 16);
}

static void construct_chroma_frm(int mb_xy, int mb_width, int stride,
		uint8_t recon_block[256], uint8_t *dst) {
	int i;
	int x_pos = mb_xy % mb_width;
	int y_pos = mb_xy / mb_width;

	for (i = 0; i < 8; i++)
		memcpy(dst + (8 * y_pos + i) * stride + (8 * x_pos),
				&recon_block[i * 8], sizeof(uint8_t) * 8);
}

static void save_i4x4_nzc(int i_mb_type, int i_mb_xy, int i_pred4x4[4][4],
		int non_zero_count[48],
		int nzc_frm[(176 + 15) / 16 * (144 + 15) / 16][16 + 4 + 4],
		DALProcess * p) {
	int i;

	if (i_mb_type == I_4x4) {
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][0] =
				i_pred4x4[block_idx_x[10]][block_idx_y[10]];
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][1] =
				i_pred4x4[block_idx_x[11]][block_idx_y[11]];
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][2] =
				i_pred4x4[block_idx_x[14]][block_idx_y[14]];
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][3] =
				i_pred4x4[block_idx_x[15]][block_idx_y[15]];
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][4] =
				i_pred4x4[block_idx_x[5]][block_idx_y[5]];
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][5] =
				i_pred4x4[block_idx_x[7]][block_idx_y[7]];
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][6] =
				i_pred4x4[block_idx_x[13]][block_idx_y[13]];
	} else {
		p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][0] =
				p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][1] =
						p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][2] =
								p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][3] =
										p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][4] =
												p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][5] =
														p->local->intra4x4_pred_mode_frm_1662[i_mb_xy][6] =
																I_PRED_4x4_DC;
	}

	if (i_mb_type != I_PCM) {
		/* save non zero count */
		for (i = 0; i < 16 + 2 * 4; i++) {
			nzc_frm[i_mb_xy][i] = non_zero_count[x264_scan8[i]];
		}
	}
}

static void save_ref_mv(int i_mb_type, int i_mb_xy, int8_t ref_mb[2][48],
		int16_t mv_mb[2][48][2],
		int8_t ref_frm[(176 + 15) / 16 * (144 + 15) / 16 * 4],
		int16_t mv_frm[(176 + 15) / 16 * (144 + 15) / 16 * 2 * 16][2]) {
	int i_mb_x = i_mb_xy % SPS_MB_WIDTH;
	int i_mb_y = i_mb_xy / SPS_MB_WIDTH;

	int s4x4 = SPS_MB_WIDTH * 4;
	int s8x8 = SPS_MB_WIDTH * 2;

	int i_mb_4x4 = 4 * (i_mb_y * s4x4 + i_mb_x);
	int i_mb_8x8 = 2 * (i_mb_y * s8x8 + i_mb_x);

	if (!IS_INTRA( i_mb_type )) {
		int y, x;

		ref_frm[i_mb_8x8 + 0 + 0 * s8x8] = ref_mb[0][x264_scan8[0]];
		ref_frm[i_mb_8x8 + 1 + 0 * s8x8] = ref_mb[0][x264_scan8[4]];
		ref_frm[i_mb_8x8 + 0 + 1 * s8x8] = ref_mb[0][x264_scan8[8]];
		ref_frm[i_mb_8x8 + 1 + 1 * s8x8] = ref_mb[0][x264_scan8[12]];

		for (y = 0; y < 4; y++) {
			for (x = 0; x < 4; x++) {
				mv_frm[i_mb_4x4 + x + y * s4x4][0] = mv_mb[0][x264_scan8[0] + x
						+ 8 * y][0];
				mv_frm[i_mb_4x4 + x + y * s4x4][1] = mv_mb[0][x264_scan8[0] + x
						+ 8 * y][1];
			}
		}
	} else {
		int y, x;

		ref_frm[i_mb_8x8 + 0 + 0 * s8x8] = ref_frm[i_mb_8x8 + 1 + 0 * s8x8] =
				ref_frm[i_mb_8x8 + 0 + 1 * s8x8] = ref_frm[i_mb_8x8 + 1
						+ 1 * s8x8] = -1;
		for (y = 0; y < 4; y++) {
			for (x = 0; x < 4; x++) {
				mv_frm[i_mb_4x4 + x + y * s4x4][0] = 0;
				mv_frm[i_mb_4x4 + x + y * s4x4][1] = 0;
			}
		}
	}
}

static void load_i4x4_nzc(int i_mb_type, int i_mb_xy, int *i_neighbour,
		int *b_fast_intra, int i_pred4x4[48], int non_zero_count[48],
		int nzc_frm[(176 + 15) / 16 * (144 + 15) / 16][16 + 4 + 4],
		int8_t mb_frm[((176 + 15) / 16) * ((144 + 15) / 16)], DALProcess * p) {
	int i_mb_x = i_mb_xy % SPS_MB_WIDTH;
	int i_mb_y = i_mb_xy / SPS_MB_WIDTH;

	int i_top_xy = i_mb_xy - SPS_MB_WIDTH;
	int i_left_xy = -1;
	int i_top_type = -1;
	int i_left_type = -1;

	int i_mb_type_top = -1;
	int i_mb_type_left = -1;
	int i_mb_type_topleft = -1;
	int i_mb_type_topright = -1;

	*i_neighbour = 0;
	*b_fast_intra = 0;

	if (i_mb_xy >= SPS_MB_WIDTH) // top available
	{
		i_mb_type_top = i_top_type = mb_frm[i_top_xy];

		*i_neighbour |= MB_TOP;

		/* load intra4x4 */

		i_pred4x4[x264_scan8[0] - 8] =
				p->local->intra4x4_pred_mode_frm_1662[i_top_xy][0];
		i_pred4x4[x264_scan8[1] - 8] =
				p->local->intra4x4_pred_mode_frm_1662[i_top_xy][1];
		i_pred4x4[x264_scan8[4] - 8] =
				p->local->intra4x4_pred_mode_frm_1662[i_top_xy][2];
		i_pred4x4[x264_scan8[5] - 8] =
				p->local->intra4x4_pred_mode_frm_1662[i_top_xy][3];

		/* load non_zero_count */

		non_zero_count[x264_scan8[0] - 8] = nzc_frm[i_top_xy][10];
		non_zero_count[x264_scan8[1] - 8] = nzc_frm[i_top_xy][11];
		non_zero_count[x264_scan8[4] - 8] = nzc_frm[i_top_xy][14];
		non_zero_count[x264_scan8[5] - 8] = nzc_frm[i_top_xy][15];

		non_zero_count[x264_scan8[16 + 0] - 8] = nzc_frm[i_top_xy][16 + 2];
		non_zero_count[x264_scan8[16 + 1] - 8] = nzc_frm[i_top_xy][16 + 3];

		non_zero_count[x264_scan8[16 + 4 + 0] - 8] = nzc_frm[i_top_xy][16 + 4
				+ 2];
		non_zero_count[x264_scan8[16 + 4 + 1] - 8] = nzc_frm[i_top_xy][16 + 4
				+ 3];

	} else {
		i_mb_type_top = -1;

		/* load intra4x4 */
		i_pred4x4[x264_scan8[0] - 8] = i_pred4x4[x264_scan8[1] - 8] =
				i_pred4x4[x264_scan8[4] - 8] = i_pred4x4[x264_scan8[5] - 8] =
						-1;

		/* load non_zero_count */
		non_zero_count[x264_scan8[0] - 8] = non_zero_count[x264_scan8[1] - 8] =
				non_zero_count[x264_scan8[4] - 8] = non_zero_count[x264_scan8[5]
						- 8] = non_zero_count[x264_scan8[16 + 0] - 8] =
						non_zero_count[x264_scan8[16 + 1] - 8] =
								non_zero_count[x264_scan8[16 + 4 + 0] - 8] =
										non_zero_count[x264_scan8[16 + 4 + 1]
												- 8] = 0x80;

	}

	if (i_mb_x > 0 && i_mb_xy > 0) // left available
			{
		i_left_xy = i_mb_xy - 1;
		i_mb_type_left = i_left_type = mb_frm[i_left_xy];

		*i_neighbour |= MB_LEFT;

		/* load intra4x4 */
		i_pred4x4[x264_scan8[0] - 1] =
				p->local->intra4x4_pred_mode_frm_1662[i_left_xy][4];
		i_pred4x4[x264_scan8[2] - 1] =
				p->local->intra4x4_pred_mode_frm_1662[i_left_xy][5];
		i_pred4x4[x264_scan8[8] - 1] =
				p->local->intra4x4_pred_mode_frm_1662[i_left_xy][6];
		i_pred4x4[x264_scan8[10] - 1] =
				p->local->intra4x4_pred_mode_frm_1662[i_left_xy][3];

		/* load non_zero_count */
		non_zero_count[x264_scan8[0] - 1] = nzc_frm[i_left_xy][5];
		non_zero_count[x264_scan8[2] - 1] = nzc_frm[i_left_xy][7];
		non_zero_count[x264_scan8[8] - 1] = nzc_frm[i_left_xy][13];
		non_zero_count[x264_scan8[10] - 1] = nzc_frm[i_left_xy][15];

		non_zero_count[x264_scan8[16 + 0] - 1] = nzc_frm[i_left_xy][16 + 1];
		non_zero_count[x264_scan8[16 + 2] - 1] = nzc_frm[i_left_xy][16 + 3];

		non_zero_count[x264_scan8[16 + 4 + 0] - 1] = nzc_frm[i_left_xy][16 + 4
				+ 1];
		non_zero_count[x264_scan8[16 + 4 + 2] - 1] = nzc_frm[i_left_xy][16 + 4
				+ 3];
	} else {
		i_mb_type_left = -1;

		i_pred4x4[x264_scan8[0] - 1] = i_pred4x4[x264_scan8[2] - 1] =
				i_pred4x4[x264_scan8[8] - 1] = i_pred4x4[x264_scan8[10] - 1] =
						-1;

		/* load non_zero_count */
		non_zero_count[x264_scan8[0] - 1] =
				non_zero_count[x264_scan8[2] - 1] = non_zero_count[x264_scan8[8]
						- 1] = non_zero_count[x264_scan8[10] - 1] =
						non_zero_count[x264_scan8[16 + 0] - 1] =
								non_zero_count[x264_scan8[16 + 2] - 1] =
										non_zero_count[x264_scan8[16 + 4 + 0]
												- 1] =
												non_zero_count[x264_scan8[16 + 4
														+ 2] - 1] = 0x80;
	}

	if (i_mb_x < SPS_MB_WIDTH - 1 && i_top_xy + 1 >= 0) {
		*i_neighbour |= MB_TOPRIGHT;
		i_mb_type_topright = mb_frm[i_top_xy + 1];
	} else
		i_mb_type_topright = -1;

	if (i_mb_x > 0 && i_top_xy - 1 >= 0) {
		*i_neighbour |= MB_TOPLEFT;
		i_mb_type_topleft = mb_frm[i_top_xy - 1];
	} else
		i_mb_type_topleft = -1;

	if (i_mb_xy > 4) {
		if (IS_INTRA(i_mb_type_left ) || IS_INTRA(i_mb_type_top )
				|| IS_INTRA(i_mb_type_topleft )
				|| IS_INTRA(i_mb_type_topright )) { /* intra is likely */
		} else {
			*b_fast_intra = 1;
		}
	}
}

static void load_ref_mv(int frame_type, int mb_type, int i_neighbour,
		int i_mb_xy, int8_t ref_mb[2][48], int16_t mv_mb[2][48][2],
		int8_t ref_frm[(176 + 15) / 16 * (144 + 15) / 16 * 4],
		int16_t mv_frm[(176 + 15) / 16 * (144 + 15) / 16 * 2 * 16][2]) {
	if (frame_type) {
		int i;

		int i_mb_x = i_mb_xy % SPS_MB_WIDTH;
		int i_mb_y = i_mb_xy / SPS_MB_WIDTH;

		const int s4x4 = SPS_MB_WIDTH * 4;
		const int s8x8 = SPS_MB_WIDTH * 2;

		int i_mb_4x4 = 4 * (i_mb_y * s4x4 + i_mb_x);
		int i_mb_8x8 = 2 * (i_mb_y * s8x8 + i_mb_x);

		{
			if (i_neighbour & MB_TOPLEFT) {
				const int i8 = x264_scan8[0] - 1 - 1 * 8;
				const int ir = i_mb_8x8 - s8x8 - 1;
				const int iv = i_mb_4x4 - s4x4 - 1;

				ref_mb[0][i8] = ref_frm[ir];
				mv_mb[0][i8][0] = mv_frm[iv][0];
				mv_mb[0][i8][1] = mv_frm[iv][1];
			} else {
				const int i8 = x264_scan8[0] - 1 - 1 * 8;

				ref_mb[0][i8] = -2;
				mv_mb[0][i8][0] = 0;
				mv_mb[0][i8][1] = 0;
			}

			if (i_neighbour & MB_TOP) {
				const int i8 = x264_scan8[0] - 8;
				const int ir = i_mb_8x8 - s8x8;
				const int iv = i_mb_4x4 - s4x4;

				ref_mb[0][i8 + 0] = ref_mb[0][i8 + 1] = ref_frm[ir + 0];
				ref_mb[0][i8 + 2] = ref_mb[0][i8 + 3] = ref_frm[ir + 1];
				for (i = 0; i < 4; i++) {
					mv_mb[0][i8 + i][0] = mv_frm[iv + i][0];
					mv_mb[0][i8 + i][1] = mv_frm[iv + i][1];
				}
			} else {
				const int i8 = x264_scan8[0] - 8;
				for (i = 0; i < 4; i++) {
					ref_mb[0][i8 + i] = -2;
					mv_mb[0][i8 + i][0] = mv_mb[0][i8 + i][1] = 0;
				}
			}

			if (i_neighbour & MB_TOPRIGHT) {
				const int i8 = x264_scan8[0] + 4 - 1 * 8;
				const int ir = i_mb_8x8 - s8x8 + 2;
				const int iv = i_mb_4x4 - s4x4 + 4;

				ref_mb[0][i8] = ref_frm[ir];
				mv_mb[0][i8][0] = mv_frm[iv][0];
				mv_mb[0][i8][1] = mv_frm[iv][1];
			} else {
				const int i8 = x264_scan8[0] + 4 - 1 * 8;

				ref_mb[0][i8] = -2;
				mv_mb[0][i8][0] = 0;
				mv_mb[0][i8][1] = 0;
			}

			if (i_neighbour & MB_LEFT) {
				const int i8 = x264_scan8[0] - 1;
				const int ir = i_mb_8x8 - 1;
				const int iv = i_mb_4x4 - 1;

				ref_mb[0][i8 + 0 * 8] = ref_mb[0][i8 + 1 * 8] = ref_frm[ir
						+ 0 * s8x8];
				ref_mb[0][i8 + 2 * 8] = ref_mb[0][i8 + 3 * 8] = ref_frm[ir
						+ 1 * s8x8];

				for (i = 0; i < 4; i++) {
					mv_mb[0][i8 + i * 8][0] = mv_frm[iv + i * s4x4][0];
					mv_mb[0][i8 + i * 8][1] = mv_frm[iv + i * s4x4][1];
				}
			} else {
				const int i8 = x264_scan8[0] - 1;
				for (i = 0; i < 4; i++) {
					ref_mb[0][i8 + i * 8] = -2;
					mv_mb[0][i8 + i * 8][0] = mv_mb[0][i8 + i * 8][1] = 0;
				}
			}
		}
	}
}

static void load_preddata(int i_mb_xy, int i_neighbour, int stride_luma,
		int stride_chroma, uint8_t preddata16[2][21], uint8_t preddata8_U[2][9],
		uint8_t preddata8_V[2][9], DALProcess * p) {
	int i_mb_x = i_mb_xy % SPS_MB_WIDTH;
	int i_mb_y = i_mb_xy / SPS_MB_WIDTH;

	uint8_t *p_Y = p->local->p_Y_frm_buf_1663 + (i_mb_y * 16 * stride_luma)
			+ (i_mb_x * 16);
	uint8_t *p_U = p->local->p_U_frm_buf_1664 + (i_mb_y * 8 * stride_chroma)
			+ (i_mb_x * 8);
	uint8_t *p_V = p->local->p_V_frm_buf_1665 + (i_mb_y * 8 * stride_chroma)
			+ (i_mb_x * 8);

	if (i_neighbour & MB_TOP) {
		int i;

		for (i = 0; i < 20; i++)
			preddata16[0][i + 1] = p_Y[i - stride_luma];
		for (i = 0; i < 8; i++) {
			preddata8_U[0][i + 1] = p_U[i - stride_chroma];
			preddata8_V[0][i + 1] = p_V[i - stride_chroma];
		}
	}
	if (i_neighbour & MB_LEFT) {
		int i;

		for (i = 1; i < 17; i++)
			preddata16[1][i] = p_Y[stride_luma * (i - 1) - 1];
		for (i = 1; i < 9; i++) {
			preddata8_U[1][i] = p_U[stride_chroma * (i - 1) - 1];
			preddata8_V[1][i] = p_V[stride_chroma * (i - 1) - 1];
		}
	}
	if (i_neighbour & MB_TOPLEFT) {
		preddata16[0][0] = preddata16[1][0] = p_Y[-stride_luma - 1];
		preddata8_U[0][0] = preddata8_U[1][0] = p_U[-stride_chroma - 1];
		preddata8_V[0][0] = preddata8_V[1][0] = p_V[-stride_chroma - 1];
	}
}

static inline int x264_clip3(int v, int i_min, int i_max) {
	return ((v < i_min) ? i_min : (v > i_max) ? i_max : v);
}
static inline int clip_uint8(int a) {
	if (a & (~255))
		return (-a) >> 31;
	else
		return a;
}

static void deblock_luma_c(uint8_t *pix, int xstride, int ystride, int alpha,
		int beta, int8_t *tc0) {
	int i, d;
	for (i = 0; i < 4; i++) {
		if (tc0[i] < 0) {
			pix += 4 * ystride;
			continue;
		}
		for (d = 0; d < 4; d++) {
			const int p2 = pix[-3 * xstride];
			const int p1 = pix[-2 * xstride];
			const int p0 = pix[-1 * xstride];
			const int q0 = pix[0 * xstride];
			const int q1 = pix[1 * xstride];
			const int q2 = pix[2 * xstride];

			if (abs(p0 - q0) < alpha && abs(p1 - p0) < beta
					&& abs(q1 - q0) < beta) {

				int tc = tc0[i];
				int delta;

				if (abs(p2 - p0) < beta) {
					pix[-2 * xstride] = p1
							+ x264_clip3(
									((p2 + ((p0 + q0 + 1) >> 1)) >> 1) - p1,
									-tc0[i], tc0[i]);
					tc++;
				}
				if (abs(q2 - q0) < beta) {
					pix[1 * xstride] = q1
							+ x264_clip3(
									((q2 + ((p0 + q0 + 1) >> 1)) >> 1) - q1,
									-tc0[i], tc0[i]);
					tc++;
				}

				delta = x264_clip3((((q0 - p0) << 2) + (p1 - q1) + 4) >> 3, -tc,
						tc);
				pix[-1 * xstride] = clip_uint8(p0 + delta); /* p0' */
				pix[0 * xstride] = clip_uint8(q0 - delta); /* q0' */
			}
			pix += ystride;
		}
	}
}

static void deblock_luma_intra_c(uint8_t *pix, int xstride, int ystride,
		int alpha, int beta) {
	int d;
	for (d = 0; d < 16; d++) {
		const int p2 = pix[-3 * xstride];
		const int p1 = pix[-2 * xstride];
		const int p0 = pix[-1 * xstride];
		const int q0 = pix[0 * xstride];
		const int q1 = pix[1 * xstride];
		const int q2 = pix[2 * xstride];

		if (abs(p0 - q0) < alpha && abs(p1 - p0) < beta
				&& abs(q1 - q0) < beta) {

			if (abs(p0 - q0) < ((alpha >> 2) + 2)) {
				if (abs(p2 - p0) < beta) {
					const int p3 = pix[-4 * xstride];
					/* p0', p1', p2' */
					pix[-1 * xstride] = (p2 + 2 * p1 + 2 * p0 + 2 * q0 + q1 + 4)
							>> 3;
					pix[-2 * xstride] = (p2 + p1 + p0 + q0 + 2) >> 2;
					pix[-3 * xstride] = (2 * p3 + 3 * p2 + p1 + p0 + q0 + 4)
							>> 3;
				} else {
					/* p0' */
					pix[-1 * xstride] = (2 * p1 + p0 + q1 + 2) >> 2;
				}
				if (abs(q2 - q0) < beta) {
					const int q3 = pix[3 * xstride];
					/* q0', q1', q2' */
					pix[0 * xstride] = (p1 + 2 * p0 + 2 * q0 + 2 * q1 + q2 + 4)
							>> 3;
					pix[1 * xstride] = (p0 + q0 + q1 + q2 + 2) >> 2;
					pix[2 * xstride] = (2 * q3 + 3 * q2 + q1 + q0 + p0 + 4)
							>> 3;
				} else {
					/* q0' */
					pix[0 * xstride] = (2 * q1 + q0 + p1 + 2) >> 2;
				}
			} else {
				/* p0', q0' */
				pix[-1 * xstride] = (2 * p1 + p0 + q1 + 2) >> 2;
				pix[0 * xstride] = (2 * q1 + q0 + p1 + 2) >> 2;
			}
		}
		pix += ystride;
	}
}

static void deblock_chroma_c(uint8_t *pix, int xstride, int ystride, int alpha,
		int beta, int8_t *tc0) {
	int i, d;
	for (i = 0; i < 4; i++) {
		const int tc = tc0[i];
		if (tc <= 0) {
			pix += 2 * ystride;
			continue;
		}
		for (d = 0; d < 2; d++) {
			const int p1 = pix[-2 * xstride];
			const int p0 = pix[-1 * xstride];
			const int q0 = pix[0 * xstride];
			const int q1 = pix[1 * xstride];

			if (abs(p0 - q0) < alpha && abs(p1 - p0) < beta
					&& abs(q1 - q0) < beta) {

				int delta = x264_clip3((((q0 - p0) << 2) + (p1 - q1) + 4) >> 3,
						-tc, tc);
				pix[-1 * xstride] = clip_uint8(p0 + delta); /* p0' */
				pix[0 * xstride] = clip_uint8(q0 - delta); /* q0' */
			}
			pix += ystride;
		}
	}
}

static void deblock_chroma_intra_c(uint8_t *pix, int xstride, int ystride,
		int alpha, int beta) {
	int d;
	for (d = 0; d < 8; d++) {
		const int p1 = pix[-2 * xstride];
		const int p0 = pix[-1 * xstride];
		const int q0 = pix[0 * xstride];
		const int q1 = pix[1 * xstride];

		if (abs(p0 - q0) < alpha && abs(p1 - p0) < beta
				&& abs(q1 - q0) < beta) {

			pix[-1 * xstride] = (2 * p1 + p0 + q1 + 2) >> 2; /* p0' */
			pix[0 * xstride] = (2 * q1 + q0 + p1 + 2) >> 2; /* q0' */
		}

		pix += ystride;
	}
}

static void deblock_h_luma_c(uint8_t *pix, int stride, int alpha, int beta,
		int8_t *tc0) {
	deblock_luma_c(pix, 1, stride, alpha, beta, tc0);
}

static void deblock_h_luma_intra_c(uint8_t *pix, int stride, int alpha,
		int beta) {
	deblock_luma_intra_c(pix, 1, stride, alpha, beta);
}

static void deblock_h_chroma_c(uint8_t *pix, int stride, int alpha, int beta,
		int8_t *tc0) {
	deblock_chroma_c(pix, 1, stride, alpha, beta, tc0);
}

static void deblock_h_chroma_intra_c(uint8_t *pix, int stride, int alpha,
		int beta) {
	deblock_chroma_intra_c(pix, 1, stride, alpha, beta);
}

static void deblock_v_luma_c(uint8_t *pix, int stride, int alpha, int beta,
		int8_t *tc0) {
	deblock_luma_c(pix, stride, 1, alpha, beta, tc0);
}

static void deblock_v_luma_intra_c(uint8_t *pix, int stride, int alpha,
		int beta) {
	deblock_luma_intra_c(pix, stride, 1, alpha, beta);
}

static void deblock_v_chroma_c(uint8_t *pix, int stride, int alpha, int beta,
		int8_t *tc0) {
	deblock_chroma_c(pix, stride, 1, alpha, beta, tc0);
}

static void deblock_v_chroma_intra_c(uint8_t *pix, int stride, int alpha,
		int beta) {
	deblock_chroma_intra_c(pix, stride, 1, alpha, beta);
}

static void deblock_edge(uint8_t *pix, int i_stride, int bS[4], int i_qp,
		int b_chroma, x264_deblock_inter_t pf_inter,
		x264_deblock_intra_t pf_intra) {
	int i;
	const int index_a = x264_clip3(i_qp, 0, 51);
	const int alpha = i_alpha_table[index_a];
	const int beta = i_beta_table[x264_clip3(i_qp, 0, 51)];

	if (bS[0] < 4) {
		int8_t tc[4];
		for (i = 0; i < 4; i++)
			tc[i] = (bS[i] ? i_tc0_table[index_a][bS[i] - 1] : -1) + b_chroma;
		pf_inter(pix, i_stride, alpha, beta, tc);
	} else {
		pf_intra(pix, i_stride, alpha, beta);
	}
}

static void plane_expand_border(uint8_t *pix, int i_stride, int i_height,
		int i_pad) {
#define PPIXEL(x, y) ( pix + (x) + (y)*i_stride )
	const int i_width = i_stride - 2 * i_pad;
	int y;

	for (y = 0; y < i_height; y++) {
		/* left band */
		memset(PPIXEL(-i_pad, y), PPIXEL(0, y)[0], i_pad);
		/* right band */
		memset(PPIXEL(i_width, y), PPIXEL(i_width-1, y)[0], i_pad);
	}
	/* upper band */
	for (y = 0; y < i_pad; y++)
		memcpy(PPIXEL(-i_pad, -y-1), PPIXEL(-i_pad, 0), i_stride);
	/* lower band */
	for (y = 0; y < i_pad; y++)
		memcpy(PPIXEL(-i_pad, i_height+y), PPIXEL(-i_pad, i_height-1),
				i_stride);
#undef PPIXEL
}

static void x264_frame_deblocking_filter(int frame_type,
		int8_t mb_type[(176 + 15) / 16 * (144 + 15) / 16],
		int non_zero_count[(176 + 15) / 16 * (144 + 15) / 16][16 + 4 + 4],
		int8_t ref_frm[(176 + 15) / 16 * (144 + 15) / 16 * 4],
		int16_t mv_frm[(176 + 15) / 16 * (144 + 15) / 16 * 2 * 16][2],
		DALProcess* p) {
	const int s8x8 = 2 * SPS_MB_WIDTH;
	const int s4x4 = 4 * SPS_MB_HEIGHT;

	int mb_y, mb_x;

	for (mb_y = 0, mb_x = 0; mb_y < SPS_MB_HEIGHT;) {
		const int mb_xy = mb_y * SPS_MB_WIDTH + mb_x;
		const int mb_8x8 = 2 * s8x8 * mb_y + 2 * mb_x;
		const int mb_4x4 = 4 * s4x4 * mb_y + 4 * mb_x;
		int i_edge;
		int i_dir;

		/* i_dir == 0 -> vertical edge
		 * i_dir == 1 -> horizontal edge */
		for (i_dir = 0; i_dir < 2; i_dir++) {
			int i_start;
			int i_qp, i_qpn;

			i_start =
					((i_dir == 0 && mb_x != 0) || (i_dir == 1 && mb_y != 0)) ?
							0 : 1;

			for (i_edge = i_start; i_edge < 4; i_edge++) {
				int mbn_xy =
						i_edge > 0 ?
								mb_xy :
								(i_dir == 0 ? mb_xy - 1 : mb_xy - SPS_MB_WIDTH);
				int mbn_8x8 =
						i_edge > 0 ?
								mb_8x8 :
								(i_dir == 0 ? mb_8x8 - 2 : mb_8x8 - 2 * s8x8);
				int mbn_4x4 =
						i_edge > 0 ?
								mb_4x4 :
								(i_dir == 0 ? mb_4x4 - 4 : mb_4x4 - 4 * s4x4);

				int bS[4]; /* filtering strength */

				/* *** Get bS for each 4px for the current edge *** */
				if (IS_INTRA( mb_type[mb_xy] ) || IS_INTRA( mb_type[mbn_xy] )) {
					bS[0] = bS[1] = bS[2] = bS[3] = (i_edge == 0 ? 4 : 3);
				} else {
					int i;
					for (i = 0; i < 4; i++) {
						int x = i_dir == 0 ? i_edge : i;
						int y = i_dir == 0 ? i : i_edge;
						int xn = (x - (i_dir == 0 ? 1 : 0)) & 0x03;
						int yn = (y - (i_dir == 0 ? 0 : 1)) & 0x03;

						if (non_zero_count[mb_xy][block_idx_xy[x][y]] != 0
								|| non_zero_count[mbn_xy][block_idx_xy[xn][yn]]
										!= 0) {
							bS[i] = 2;
						} else {
							/* FIXME: A given frame may occupy more than one position in
							 * the reference list. So we should compare the frame numbers,
							 * not the indices in the ref list.
							 * No harm yet, as we don't generate that case.*/

							int i8p = mb_8x8 + (x / 2) + (y / 2) * s8x8;
							int i8q = mbn_8x8 + (xn / 2) + (yn / 2) * s8x8;
							int i4p = mb_4x4 + x + y * s4x4;
							int i4q = mbn_4x4 + xn + yn * s4x4;
							int l;

							bS[i] = 0;

							if (ref_frm[i8p] != ref_frm[i8q]
									|| abs(mv_frm[i4p][0] - mv_frm[i4q][0]) >= 4
									|| abs(mv_frm[i4p][1] - mv_frm[i4q][1])
											>= 4) {
								bS[i] = 1;
							}
						}
					}
				}

				/* *** filter *** */
				/* Y plane */
				i_qp = frame_type == 0 ? 23 : 26;
				i_qpn = frame_type == 0 ? 23 : 26;

				if (i_dir == 0) {
					/* vertical edge */
					deblock_edge(
							&(p->local->plane0_1666[16 * mb_y * (176 + 64)
									+ 16 * mb_x + 4 * i_edge]), 176 + 64, bS,
							(i_qp + i_qpn + 1) >> 1, 0, deblock_h_luma_c,
							deblock_h_luma_intra_c);
					if (!(i_edge & 1)) {
						/* U/V planes */
						int i_qpc = (i_chroma_qp_table[x264_clip3(i_qp, 0, 51)]
								+ i_chroma_qp_table[x264_clip3(i_qpn, 0, 51)]
								+ 1) >> 1;
						deblock_edge(
								&(p->local->plane1_1667[8
										* (mb_y * (176 + 64) / 2 + mb_x)
										+ 2 * i_edge]), (176 + 64) / 2, bS,
								i_qpc, 1, deblock_h_chroma_c,
								deblock_h_chroma_intra_c);
						deblock_edge(
								&(p->local->plane2_1668[8
										* (mb_y * (176 + 64) / 2 + mb_x)
										+ 2 * i_edge]), (176 + 64) / 2, bS,
								i_qpc, 1, deblock_h_chroma_c,
								deblock_h_chroma_intra_c);
					}
				} else {
					/* horizontal edge */
					deblock_edge(
							&(p->local->plane0_1666[(16 * mb_y + 4 * i_edge)
									* (176 + 64) + 16 * mb_x]), 176 + 64, bS,
							(i_qp + i_qpn + 1) >> 1, 0, deblock_v_luma_c,
							deblock_v_luma_intra_c);
					/* U/V planes */
					if (!(i_edge & 1)) {
						int i_qpc = (i_chroma_qp_table[x264_clip3(i_qp, 0, 51)]
								+ i_chroma_qp_table[x264_clip3(i_qpn, 0, 51)]
								+ 1) >> 1;
						deblock_edge(
								&(p->local->plane1_1667[8
										* (mb_y * (176 + 64) / 2 + mb_x)
										+ 2 * i_edge * (176 + 64) / 2]),
								(176 + 64) / 2, bS, i_qpc, 1,
								deblock_v_chroma_c, deblock_v_chroma_intra_c);
						deblock_edge(
								&(p->local->plane2_1668[8
										* (mb_y * (176 + 64) / 2 + mb_x)
										+ 2 * i_edge * (176 + 64) / 2]),
								(176 + 64) / 2, bS, i_qpc, 1,
								deblock_v_chroma_c, deblock_v_chroma_intra_c);
					}
				}
			}
		}

		/* newt mb */
		mb_x++;
		if (mb_x >= SPS_MB_WIDTH) {
			mb_x = 0;
			mb_y++;
		}
	}
}

static void x264_frame_expand_border(DALProcess* p) {
	plane_expand_border(p->local->plane0_1666, 176 + 64, 144, 32);
	plane_expand_border(p->local->plane1_1667, (176 + 64) / 2, 144 / 2, 16);
	plane_expand_border(p->local->plane2_1668, (176 + 64) / 2, 144 / 2, 16);
}

static inline int x264_tapfilter(uint8_t *pix, int i_pix_next) {
	return pix[-2 * i_pix_next] - 5 * pix[-1 * i_pix_next]
			+ 20 * (pix[0] + pix[1 * i_pix_next]) - 5 * pix[2 * i_pix_next]
			+ pix[3 * i_pix_next];
}

static inline int x264_tapfilter1(uint8_t *pix) {
	return pix[-2] - 5 * pix[-1] + 20 * (pix[0] + pix[1]) - 5 * pix[2] + pix[3];
}

static inline uint8_t x264_mc_clip1(int x) {
	return x264_mc_clip1_table[x + 80];
}

static inline void mc_hh(uint8_t *src, int i_src_stride, uint8_t *dst,
		int i_dst_stride, int i_width, int i_height) {
	int x, y;

	for (y = 0; y < i_height; y++) {
		for (x = 0; x < i_width; x++) {
			dst[x] = x264_mc_clip1((x264_tapfilter1(&src[x]) + 16) >> 5);
		}
		src += i_src_stride;
		dst += i_dst_stride;
	}
}

static inline void mc_hv(uint8_t *src, int i_src_stride, uint8_t *dst,
		int i_dst_stride, int i_width, int i_height) {
	int x, y;

	for (y = 0; y < i_height; y++) {
		for (x = 0; x < i_width; x++) {
			dst[x] = x264_mc_clip1(
					(x264_tapfilter(&src[x], i_src_stride) + 16) >> 5);
		}
		src += i_src_stride;
		dst += i_dst_stride;
	}
}

static inline void mc_hc(uint8_t *src, int i_src_stride, uint8_t *dst,
		int i_dst_stride, int i_width, int i_height) {
	uint8_t *out;
	uint8_t *pix;
	int x, y;

	for (x = 0; x < i_width; x++) {
		int tap[6];

		pix = &src[x];
		out = &dst[x];

		tap[0] = x264_tapfilter1(&pix[-2 * i_src_stride]);
		tap[1] = x264_tapfilter1(&pix[-1 * i_src_stride]);
		tap[2] = x264_tapfilter1(&pix[0 * i_src_stride]);
		tap[3] = x264_tapfilter1(&pix[1 * i_src_stride]);
		tap[4] = x264_tapfilter1(&pix[2 * i_src_stride]);

		for (y = 0; y < i_height; y++) {
			tap[5] = x264_tapfilter1(&pix[3 * i_src_stride]);

			*out = x264_mc_clip1(
					(tap[0] - 5 * tap[1] + 20 * tap[2] + 20 * tap[3]
							- 5 * tap[4] + tap[5] + 512) >> 10);

			/* Next line */
			pix += i_src_stride;
			out += i_dst_stride;
			tap[0] = tap[1];
			tap[1] = tap[2];
			tap[2] = tap[3];
			tap[3] = tap[4];
			tap[4] = tap[5];
		}
	}
}

static void x264_frame_filter(DALProcess * p) {
	const int x_inc = 16, y_inc = 16;
	const int stride = 176 + 64;
	int x, y;

	{
		for (y = -8; y < 144 + 8; y += y_inc) {
			uint8_t *p_in = p->local->plane0_1669 + y * stride - 8;
			uint8_t *p_h = p->local->filtered1_1670 + y * stride - 8;
			uint8_t *p_v = p->local->filtered2_1671 + y * stride - 8;
			uint8_t *p_hv = p->local->filtered3_1672 + y * stride - 8;
			for (x = -8; x < stride - 64 + 8; x += x_inc) {
				mc_hh(p_in, stride, p_h, stride, x_inc, y_inc);
				mc_hv(p_in, stride, p_v, stride, x_inc, y_inc);
				mc_hc(p_in, stride, p_hv, stride, x_inc, y_inc);

				p_h += x_inc;
				p_v += x_inc;
				p_hv += x_inc;
				p_in += x_inc;
			}
		}
	}
}

static void x264_frame_expand_border_filtered(DALProcess * p) {
	/* during filtering, 8 extra pixels were filtered on each edge.
	 we want to expand border from the last filtered pixel */

	plane_expand_border(p->local->filtered1_1670 - 8 * (176 + 64) - 8, 176 + 64,
			144 + 2 * 8, 24);
	plane_expand_border(p->local->filtered2_1671 - 8 * (176 + 64) - 8, 176 + 64,
			144 + 2 * 8, 24);
	plane_expand_border(p->local->filtered3_1672 - 8 * (176 + 64) - 8, 176 + 64,
			144 + 2 * 8, 24);
}

/*********************
 DAL FUNCTIONS
 **********************/
void deblock_init(DALProcess *p) {
	memset(p->local->recon_frame_Y_1599.data, 0,
			sizeof(uint8_t) * (176 + 64) * (144 + 64));
	memset(p->local->recon_frame_U_1600.data, 0,
			sizeof(uint8_t) * (176 / 2 + 32) * (144 / 2 + 32));
	memset(p->local->recon_frame_V_1601.data, 0,
			sizeof(uint8_t) * (176 / 2 + 32) * (144 / 2 + 32));
	memset(p->local->ref_frm_1611.data, -2,
			sizeof(int8_t) * (176 + 15) / 16 * (144 + 15) / 16 * 4);
	p->local->p_Y_frm_buf_1663 = p->local->recon_frame_Y_1599.data
			+ (176 + 64) * 32 + 32;
	p->local->p_U_frm_buf_1664 = p->local->recon_frame_U_1600.data
			+ (176 / 2 + 32) * 16 + 16;
	p->local->p_V_frm_buf_1665 = p->local->recon_frame_V_1601.data
			+ (176 / 2 + 32) * 16 + 16;
	memset(p->local->ref_mb_1604.data, -2, sizeof(p->local->ref_mb_1604.data));
	p->local->plane0_1666 = p->local->recon_frame_Y_1613.data + (176 + 64) * 32
			+ 32;
	p->local->plane1_1667 = p->local->recon_frame_U_1614.data
			+ (176 / 2 + 32) * 16 + 16;
	p->local->plane2_1668 = p->local->recon_frame_V_1615.data
			+ (176 / 2 + 32) * 16 + 16;
	p->local->plane0_1669 = p->local->recon_frame_Y_1613.data + (176 + 64) * 32
			+ 32;
	p->local->filtered1_1670 = p->local->Filtered_frame_1_1619.data
			+ (176 + 64) * 32 + 32;
	p->local->filtered2_1671 = p->local->Filtered_frame_2_1620.data
			+ (176 + 64) * 32 + 32;
	p->local->filtered3_1672 = p->local->Filtered_frame_3_1621.data
			+ (176 + 64) * 32 + 32;

	p->local->output_1642 = 0;
	p->local->output_1630 = 0;
	p->local->output_1708_phase = 0;

	p->local->mb_index_Debl = 0;
	{
		int i;
		for (i = 0; i < 99; i++)
			p->local->MB_Available[i] = 0;
	}

	p->local->expand_1673 = 0;
	p->local->quiet_1674 = 1;

////////////////////////////////////
	p->local->counter = 0;
	{
		int i;
		for (i = 0; i < 25; i++)
			p->local->myutime[i] = 0;
	}
	{
		int i;
		for (i = 0; i < 25; i++)
			p->local->mystime[i] = 0;
	}
////////////////////////////////////

}

int deblock_fire(DALProcess * p) {

	EncDbkPacket d;

	{
		DAL_read((void*) PORT_IN, &d, sizeof(EncDbkPacket), p);

		memcpy(&(p->local->output_1634), &d.recon_block_1313, 256);
		memcpy(&(p->local->output_1635), &d.recon_block_1382, 256);
		memcpy(&(p->local->output_1636), &d.recon_block_U, 64);
		memcpy(&(p->local->output_1637), &d.recon_block_V, 64);
		memcpy(&(p->local->output_1639), &d.non_zero_count, 192);
		memcpy(&(p->local->d_r_info), &d.d_s_info, sizeof(deblock_send_info));
		p->local->mb_index_Debl = d.MB_index_Enc;

		if (p->local->mb_index_Debl == 0) {
			{ /* DeblockI82.XFrameFilterI44 (class CGCXFrameFilter) */
				x264_frame_filter(p);
				x264_frame_expand_border_filtered(p); //1666,1667,1668
			}
		}

		// 99 loop start
		{
			// MB_Available matrix
			p->local->MB_Available[p->local->mb_index_Debl] = 1;
			MB_finder(p->local->mb_index_Debl, SPS_MB_WIDTH,
					p->local->MB_Available, p);

			if (p->local->mb_index_Debl == 0)
				p->local->output_1630 = d.intFrameType;

			{ /*DeblockI82.XMBPostI0 (class CGCXMBPost) */
				if (p->local->d_r_info.mb_type == I_4x4)
					construct_luma_frm(p->local->mb_index_Debl, SPS_MB_WIDTH,
							176 + 64, p->local->d_r_info.recon_block_4x4.data,
							p);
				else if (p->local->d_r_info.mb_type == I_16x16)
					construct_luma_frm(p->local->mb_index_Debl, SPS_MB_WIDTH,
							176 + 64, p->local->output_1634.data, p);
				else
					construct_luma_frm(p->local->mb_index_Debl, SPS_MB_WIDTH,
							176 + 64, p->local->output_1635.data, p);

				construct_chroma_frm(p->local->mb_index_Debl, SPS_MB_WIDTH,
						176 / 2 + 32, p->local->output_1636.data,
						p->local->p_U_frm_buf_1664);
				construct_chroma_frm(p->local->mb_index_Debl, SPS_MB_WIDTH,
						176 / 2 + 32, p->local->output_1637.data,
						p->local->p_V_frm_buf_1665);

				p->local->mb_frm_1609.data[p->local->mb_index_Debl] =
						p->local->d_r_info.mb_type;
				save_i4x4_nzc(p->local->d_r_info.mb_type,
						p->local->mb_index_Debl,
						p->local->d_r_info.i_pred4x4.data,
						p->local->output_1639.data, p->local->nzc_frm_1610.data,
						p);
				save_ref_mv(p->local->d_r_info.mb_type, p->local->mb_index_Debl,
						p->local->d_r_info.ref_mb.data,
						p->local->d_r_info.mv_mb.data,
						p->local->ref_frm_1611.data,
						p->local->mv_frm_1612.data);

				if (p->local->mb_index_Debl
						!= SPS_MB_WIDTH * SPS_MB_HEIGHT - 1) {
					if (p->local->R_Available) {
						load_i4x4_nzc(p->local->d_r_info.mb_type,
								p->local->mb_index_Debl + 1,
								&(p->local->info_R.i_neighbour),
								&(p->local->info_R.b_fast_intra),
								p->local->info_R.i_pred4x4.data,
								p->local->info_R.non_zero_count.data,
								p->local->nzc_frm_1610.data,
								p->local->mb_frm_1609.data, p);
						load_ref_mv(p->local->output_1630,
								p->local->d_r_info.mb_type,
								p->local->info_R.i_neighbour,
								p->local->mb_index_Debl + 1,
								p->local->info_R.ref_mb.data,
								p->local->info_R.mv_mb.data,
								p->local->ref_frm_1611.data,
								p->local->mv_frm_1612.data);
						load_preddata(p->local->mb_index_Debl + 1,
								p->local->info_R.i_neighbour, 176 + 64,
								176 / 2 + 32,
								p->local->info_R.preddata16_Y.data,
								p->local->info_R.preddata8_U.data,
								p->local->info_R.preddata8_V.data, p);
					}
					if (p->local->LD_Available) {
						load_i4x4_nzc(p->local->d_r_info.mb_type,
								p->local->mb_index_Debl + SPS_MB_WIDTH - 1,
								&(p->local->info_LD.i_neighbour),
								&(p->local->info_LD.b_fast_intra),
								p->local->info_LD.i_pred4x4.data,
								p->local->info_LD.non_zero_count.data,
								p->local->nzc_frm_1610.data,
								p->local->mb_frm_1609.data, p);
						load_ref_mv(p->local->output_1630,
								p->local->d_r_info.mb_type,
								p->local->info_LD.i_neighbour,
								p->local->mb_index_Debl + SPS_MB_WIDTH - 1,
								p->local->info_LD.ref_mb.data,
								p->local->info_LD.mv_mb.data,
								p->local->ref_frm_1611.data,
								p->local->mv_frm_1612.data);
						load_preddata(
								p->local->mb_index_Debl + SPS_MB_WIDTH - 1,
								p->local->info_LD.i_neighbour, 176 + 64,
								176 / 2 + 32,
								p->local->info_LD.preddata16_Y.data,
								p->local->info_LD.preddata8_U.data,
								p->local->info_LD.preddata8_V.data, p);
					}
				} else { // last macro block // is this right?
					load_i4x4_nzc(p->local->d_r_info.mb_type, 0,
							&(p->local->info_R.i_neighbour),
							&(p->local->info_R.b_fast_intra),
							p->local->info_R.i_pred4x4.data,
							p->local->info_R.non_zero_count.data,
							p->local->nzc_frm_1610.data,
							p->local->mb_frm_1609.data, p);
					load_ref_mv(p->local->output_1630,
							p->local->d_r_info.mb_type,
							p->local->info_R.i_neighbour, 0,
							p->local->info_R.ref_mb.data,
							p->local->info_R.mv_mb.data,
							p->local->ref_frm_1611.data,
							p->local->mv_frm_1612.data);
					load_preddata(0, p->local->info_R.i_neighbour, 176 + 64,
							176 / 2 + 32, p->local->info_R.preddata16_Y.data,
							p->local->info_R.preddata8_U.data,
							p->local->info_R.preddata8_V.data, p);

					DAL_write((void*) PORT_OUT, &(p->local->info_R),
							sizeof(neighbour_info), p);
				}

				// phase & queue - jwlee
			}

			if (p->local->R_Available) { /* (class CGCXMErefgen) */
				/*
				int i;
				int i_mb_x = p->local->mb_index_Debl + 1 % SPS_MB_WIDTH;
				int i_mb_y = p->local->mb_index_Debl + 1 / SPS_MB_WIDTH;

				int offset = i_mb_y * 16 * (176 + 64) + i_mb_x * 16;
				int offset_c = i_mb_y * 8 * (176 / 2 + 32) + i_mb_x * 8;
				int stride = 176 + 64;
				int stride_c = 176 / 2 + 32;

				uint8_t *p_0 = p->local->plane0_1666 + offset;
				uint8_t *p_1 = p->local->filtered1_1670 + offset;
				uint8_t *p_2 = p->local->filtered2_1671 + offset;
				uint8_t *p_3 = p->local->filtered3_1672 + offset;
				uint8_t *p_4 = p->local->plane1_1667 + offset_c;
				uint8_t *p_5 = p->local->plane2_1668 + offset_c;

				 for (i = -16 ; i < 32 ; i++) {
				 memcpy(&info_R.ME_ref.data[0][(i+16)*48], &p_0[i * stride - 16], sizeof(uint8_t)*48);
				 memcpy(&info_R.ME_ref.data[1][(i+16)*48], &p_1[i * stride - 16], sizeof(uint8_t)*48);
				 memcpy(&info_R.ME_ref.data[2][(i+16)*48], &p_2[i * stride - 16], sizeof(uint8_t)*48);
				 memcpy(&info_R.ME_ref.data[3][(i+16)*48], &p_3[i * stride - 16], sizeof(uint8_t)*48);
				 }

				 for (i = -8 ; i < 16 ; i++) {
				 memcpy(&info_R.ME_ref_chroma.data[0][(i+8)*24], &p_4[i * stride_c - 8], sizeof(uint8_t)*24);
				 memcpy(&info_R.ME_ref_chroma.data[1][(i+8)*24], &p_5[i * stride_c - 8], sizeof(uint8_t)*24);
				 }
				 */

				DAL_write((void*) PORT_OUT, &(p->local->info_R),
						sizeof(neighbour_info), p);
			}

			//if ( LD_Available )
			if (!(p->local->R_Available) && !(p->local->LD_Available)
					&& (p->local->mb_index_Debl
							!= SPS_MB_WIDTH * SPS_MB_HEIGHT - 1)) { /* (class CGCXMErefgen) */
				int i;
				int i_mb_x = (p->local->mb_index_Debl + SPS_MB_WIDTH - 1)
						% SPS_MB_WIDTH;
				int i_mb_y = (p->local->mb_index_Debl + SPS_MB_WIDTH - 1)
						/ SPS_MB_WIDTH;

				int offset = i_mb_y * 16 * (176 + 64) + i_mb_x * 16;
				int offset_c = i_mb_y * 8 * (176 / 2 + 32) + i_mb_x * 8;
				int stride = 176 + 64;
				int stride_c = 176 / 2 + 32;

				uint8_t *p_0 = p->local->plane0_1666 + offset;
				uint8_t *p_1 = p->local->filtered1_1670 + offset;
				uint8_t *p_2 = p->local->filtered2_1671 + offset;
				uint8_t *p_3 = p->local->filtered3_1672 + offset;
				uint8_t *p_4 = p->local->plane1_1667 + offset_c;
				uint8_t *p_5 = p->local->plane2_1668 + offset_c;
				/*
				 for (i = -16 ; i < 32 ; i++) {
				 memcpy(&info_LD.ME_ref.data[0][(i+16)*48], &p_0[i * stride - 16], sizeof(uint8_t)*48);
				 memcpy(&info_LD.ME_ref.data[1][(i+16)*48], &p_1[i * stride - 16], sizeof(uint8_t)*48);
				 memcpy(&info_LD.ME_ref.data[2][(i+16)*48], &p_2[i * stride - 16], sizeof(uint8_t)*48);
				 memcpy(&info_LD.ME_ref.data[3][(i+16)*48], &p_3[i * stride - 16], sizeof(uint8_t)*48);
				 }

				 for (i = -8 ; i < 16 ; i++) {
				 memcpy(&info_LD.ME_ref_chroma.data[0][(i+8)*24], &p_4[i * stride_c - 8], sizeof(uint8_t)*24);
				 memcpy(&info_LD.ME_ref_chroma.data[1][(i+8)*24], &p_5[i * stride_c - 8], sizeof(uint8_t)*24);
				 }
				 */

				DAL_write((void*) PORT_OUT, &p->local->info_LD,
						sizeof(neighbour_info), p);
				//if(count < 199)  printf("%d) LD: %d\n",count, mb_index_Debl+1);
			}
		}
		//        } // 99 loop end
	} /* end repeat, depth 2*/
	// Frame encoding end
	if (p->local->mb_index_Debl == 98) // last macroblock
			{
		{
			int i;
			for (i = 0; i < SPS_MB_WIDTH * SPS_MB_HEIGHT; i++) {
				p->local->MB_Available[i] = 0;
			}
		}

		p->local->output_1642 = d.intFrameType;

		{ /* XDeblockFilterI29 (class CGCXDeblockFilter) */
			memcpy(p->local->recon_frame_Y_1613.data,
					p->local->recon_frame_Y_1599.data,
					(176 + 64) * (144 + 64) * sizeof(uint8_t));
			memcpy(p->local->recon_frame_U_1614.data,
					p->local->recon_frame_U_1600.data,
					(176 / 2 + 32) * (144 / 2 + 32) * sizeof(uint8_t));
			memcpy(p->local->recon_frame_V_1615.data,
					p->local->recon_frame_V_1601.data,
					(176 / 2 + 32) * (144 / 2 + 32) * sizeof(uint8_t));
			x264_frame_deblocking_filter(p->local->output_1642,
					p->local->mb_frm_1609.data, p->local->nzc_frm_1610.data,
					p->local->ref_frm_1611.data, p->local->mv_frm_1612.data, p);
			x264_frame_expand_border(p);
		}
	}

	p->local->counter++;

	return 0;
}

void deblock_finish(DALProcess *p) {
}
