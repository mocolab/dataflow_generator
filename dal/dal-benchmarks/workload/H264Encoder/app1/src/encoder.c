#include "encoder.h"

/************************
  DEFINITIONS AND MACROS
*************************/

#define QP_CONSTANT 26
#define QP_CONSTANT_I 23
#define QUANT_ONE( coef, mf ) \
{ \
    if( (coef) > 0 ) \
    (coef) = ( f + (coef) * (mf) ) >> i_qbits; \
    else \
    (coef) = - ( ( f - (coef) * (mf) ) >> i_qbits ); \
}
#define DEQUANT_SHL( x ) \
    dct[y][x] = ( dct[y][x] * dequant4_mf[0][i_mf][y][x] ) << i_qbits

#define DEQUANT_SHR( x ) \
    dct[y][x] = ( dct[y][x] * dequant4_mf[0][i_mf][y][x] + f ) >> (-i_qbits)
#define ZIG(i,y,x) level[i] = dct[y][x];
#define IS_INTRA(type) ( (type) == I_4x4 || (type) == I_8x8 || (type) == I_16x16)


/***********************
  FUNCTION DECLARATIONS
***********************/

static inline int clip_uint8( int a )
{
    if (a&(~255))
        return (-a)>>31;
    else
        return a;
}

static void predict_16x16_v_new( uint8_t *src, uint8_t preddata[2][21] );
static void predict_16x16_h_new( uint8_t *src, uint8_t preddata[2][21]);
static void predict_16x16_dc_new( uint8_t *src, uint8_t preddata[2][21] );
static void predict_16x16_p_new( uint8_t *src, uint8_t preddata[2][21] );
static void predict_16x16_dc_left_new( uint8_t *src, uint8_t preddata[2][21] );
static void predict_16x16_dc_top_new( uint8_t *src, uint8_t preddata[2][21] );
static void predict_16x16_dc_128_new( uint8_t *src, uint8_t preddata[2][21] );

static void predict_16x16_get_best(int i_pred16x16, uint8_t preddata16[2][21], uint8_t dst_block[16*16]) {

    switch(i_pred16x16)
    {
        case I_PRED_16x16_V :
            predict_16x16_v_new( dst_block, preddata16 );
            break;
        case I_PRED_16x16_H :
            predict_16x16_h_new( dst_block, preddata16 );
            break;
        case I_PRED_16x16_DC :
            predict_16x16_dc_new( dst_block, preddata16 );
            break;
        case I_PRED_16x16_P :
            predict_16x16_p_new( dst_block, preddata16 );
            break;
        case I_PRED_16x16_DC_LEFT :
            predict_16x16_dc_left_new( dst_block, preddata16  );
            break;
        case I_PRED_16x16_DC_TOP :
            predict_16x16_dc_top_new( dst_block, preddata16  );
            break;
        case I_PRED_16x16_DC_128 :
            predict_16x16_dc_128_new( dst_block, preddata16  );
            break;
    }
}

static void predict_16x16_v_new( uint8_t *src, uint8_t preddata[2][21] )
{
    int i;

    for( i = 0; i < 16; i++ )
        memcpy(&src[i*16], &preddata[0][1], 16*sizeof(uint8_t));
}

static void predict_16x16_h_new( uint8_t *src, uint8_t preddata[2][21])
{
    int i;

    for( i = 0; i < 16; i++ )
        memset(&src[i*16], preddata[1][i+1], 16);
}

static void predict_16x16_dc_new( uint8_t *src, uint8_t preddata[2][21] )
{

    int dc = 0;
    int i;

    /* calculate DC value */
    for( i = 0; i < 16; i++ )
    {
        dc += preddata[0][i+1];
        dc += preddata[1][i+1];
    }
    dc = ( dc + 16 ) >> 5;

    for( i = 0; i < 16; i++ )
        memset(&src[i*16], dc, 16);
}

static void predict_16x16_p_new( uint8_t *src, uint8_t preddata[2][21] )
{
    int x, y, i;
    int a, b, c;
    int H = 0;
    int V = 0;
    int i00;

    /* calcule H and V */
    for( i = 0; i <= 7; i++ )
    {
        H += ( i + 1 ) * ( preddata[0][9 + i] - preddata[0][7 -i] );
        V += ( i + 1 ) * ( preddata[1][9 + i] - preddata[1][7 -i] );
    }

    a = 16 * ( preddata[1][16] + preddata[0][16] );
    b = ( 5 * H + 32 ) >> 6;
    c = ( 5 * V + 32 ) >> 6;

    i00 = a - b * 7 - c * 7 + 16;

    for( y = 0; y < 16; y++ )
    {
        for( x = 0; x < 16; x++ )
        {
            int pix;

            pix = (i00+b*x)>>5;

            src[y* 16 + x] = clip_uint8( pix );
        }
        i00 += c;
    }
}

static void predict_16x16_dc_left_new( uint8_t *src, uint8_t preddata[2][21] )
{
    int dc = 0;
    int i;

    for( i = 0; i < 16; i++ )
    {
        dc += preddata[1][i+1];
    }
    dc = ( dc + 8 ) >> 4;

    for( i = 0; i < 16; i++ )
        memset(&src[i*16], dc, 16);
}

static void predict_16x16_dc_top_new( uint8_t *src, uint8_t preddata[2][21] )
{           
    int dc = 0;
    int i;

    for( i = 0; i < 16; i++ )
    {
        dc += preddata[0][i + 1];
    }
    dc = ( dc + 8 ) >> 4;

    for( i = 0; i < 16; i++ )
        memset(&src[i*16], dc, 16);
}

static void predict_16x16_dc_128_new( uint8_t *src, uint8_t preddata[2][21] )
{           
    int i;

    for( i = 0; i < 16; i++ )
        memset((void *)&src[i*16], 128, 16);
}

static void x264_pre_16x16_dct(int i, uint8_t src_block[16*16], uint8_t best_block[16*16], uint8_t src_block_4x4[4*4], uint8_t best_block_4x4[4*4])
{
    int x = block_idx_x[i];
    int y = block_idx_y[i];
    int iter;

    for (iter = 0 ; iter < 4 ; iter++) {
        memcpy(&src_block_4x4[iter*4], &src_block[(y*4 + iter) * 16 + (x*4)], sizeof(uint8_t)*4);
        memcpy(&best_block_4x4[iter*4], &best_block[(y*4 + iter) * 16 + (x*4)], sizeof(uint8_t)*4);
    }
}

static void pixel_sub_wxh( int16_t *diff, int i_size,
        uint8_t *pix1, int i_pix1, uint8_t *pix2, int i_pix2 )
{
    int y, x;
    for( y = 0; y < i_size; y++ )
    {
        for( x = 0; x < i_size; x++ )
        {
            diff[x + y*i_size] = pix1[x] - pix2[x];
        }
        pix1 += i_pix1;
        pix2 += i_pix2;
    }
}

static void sub4x4_dct( int16_t dct[4][4], uint8_t *pix1, int i_pix1, uint8_t *pix2, int i_pix2 )
{
    int16_t d[4][4];
    int16_t tmp[4][4];
    int i;

    pixel_sub_wxh( (int16_t*)d, 4, pix1, i_pix1, pix2, i_pix2 );
    //pixel_sub_wxh( *d, 4, pix1, i_pix1, pix2, i_pix2 );

    for( i = 0; i < 4; i++ )
    {
        const int s03 = d[i][0] + d[i][3];
        const int s12 = d[i][1] + d[i][2];
        const int d03 = d[i][0] - d[i][3];
        const int d12 = d[i][1] - d[i][2];

        tmp[0][i] =   s03 +   s12;
        tmp[1][i] = 2*d03 +   d12;
        tmp[2][i] =   s03 -   s12;
        tmp[3][i] =   d03 - 2*d12;
    }

    for( i = 0; i < 4; i++ )
    {
        const int s03 = tmp[i][0] + tmp[i][3];
        const int s12 = tmp[i][1] + tmp[i][2];
        const int d03 = tmp[i][0] - tmp[i][3];
        const int d12 = tmp[i][1] - tmp[i][2];

        dct[0][i] =   s03 +   s12;
        dct[1][i] = 2*d03 +   d12;
        dct[2][i] =   s03 -   s12;
        dct[3][i] =   d03 - 2*d12;
    }
}

static void x264_16x16_dc_gen(int i, int16_t src_block_4x4[4][4], int16_t dc_block_4x4[4][4])
{
    int x = block_idx_x[i];
    int y = block_idx_y[i];
    int iter;

    dc_block_4x4[x][y] = src_block_4x4[0][0];
}

static void quant_4x4_core( int16_t dct[4][4], int i_mf, int i_qbits, int f )
{
    int i;
    for( i = 0; i < 16; i++ )
        QUANT_ONE( dct[0][i], quant4_mf[0][i_mf][0][i] );
}

static void quant_4x4(int16_t dct[4][4], int i_qscale, int b_intra )
{
    const int i_qbits = 15 + i_qscale / 6;
    const int i_mf = i_qscale % 6;
    const int f = ( 1 << i_qbits ) / ( b_intra ? 3 : 6 );
    quant_4x4_core( dct, i_mf, i_qbits, f );
}

static void quant_4x4_dc_core( int16_t dct[4][4], int i_mf, int i_qbits, int f )
{
    int i;
    for( i = 0; i < 16; i++ )
        QUANT_ONE( dct[0][i], quant4_mf[0][i_mf][0][0] );
}

static void quant_4x4_dc(int16_t dct[4][4], int i_qscale)
{
    const int i_qbits = 16 + i_qscale / 6;
    const int i_mf = i_qscale % 6;
    const int f = ( 1 << i_qbits ) / 3;
    quant_4x4_dc_core( dct, i_mf, i_qbits, f );
}

static void idct4x4dc( int16_t d[4][4] )
{
    int16_t tmp[4][4];
    int s01, s23;
    int d01, d23;
    int i;

    for( i = 0; i < 4; i++ )
    {
        s01 = d[0][i] + d[1][i];
        d01 = d[0][i] - d[1][i];
        s23 = d[2][i] + d[3][i];
        d23 = d[2][i] - d[3][i];

        tmp[0][i] = s01 + s23;
        tmp[1][i] = s01 - s23;
        tmp[2][i] = d01 - d23;
        tmp[3][i] = d01 + d23;
    }

    for( i = 0; i < 4; i++ )
    {
        s01 = tmp[i][0] + tmp[i][1];
        d01 = tmp[i][0] - tmp[i][1];
        s23 = tmp[i][2] + tmp[i][3];
        d23 = tmp[i][2] - tmp[i][3];

        d[i][0] = s01 + s23;
        d[i][1] = s01 - s23;
        d[i][2] = d01 - d23;
        d[i][3] = d01 + d23;
    }
}

static void dequant_4x4_dc(int16_t dct[4][4], int i_qp)
{
    const int i_qbits = i_qp/6 - 6;
    int y;

    if( i_qbits >= 0 )
    {
        const int i_dmf = dequant4_mf[0][i_qp%6][0][0] << i_qbits;

        for( y = 0; y < 4; y++ )
        {
            dct[y][0] *= i_dmf;
            dct[y][1] *= i_dmf;
            dct[y][2] *= i_dmf;
            dct[y][3] *= i_dmf;
        }
    }
    else
    {
        const int i_dmf = dequant4_mf[0][i_qp%6][0][0];
        const int f = 1 << (-i_qbits-1);

        for( y = 0; y < 4; y++ )
        {
            dct[y][0] = ( dct[y][0] * i_dmf + f ) >> (-i_qbits);
            dct[y][1] = ( dct[y][1] * i_dmf + f ) >> (-i_qbits);
            dct[y][2] = ( dct[y][2] * i_dmf + f ) >> (-i_qbits);
            dct[y][3] = ( dct[y][3] * i_dmf + f ) >> (-i_qbits);
        }
    }
}

static void dequant_4x4( int16_t dct[4][4],  int i_qp )
{
    const int i_mf = i_qp%6;
    const int i_qbits = i_qp/6 - 4;
    int y;

    if( i_qbits >= 0 )
    {
        for( y = 0; y < 4; y++ )
        {
            DEQUANT_SHL( 0 );
            DEQUANT_SHL( 1 );
            DEQUANT_SHL( 2 );
            DEQUANT_SHL( 3 );
        }
    }
    else
    {
        const int f = 1 << (-i_qbits-1);
        for( y = 0; y < 4; y++ )
        {
            DEQUANT_SHR( 0 );
            DEQUANT_SHR( 1 );
            DEQUANT_SHR( 2 );
            DEQUANT_SHR( 3 );
        }
    }
}

static void x264_16x16_dc_merge(int16_t src_block_4x4[4][4], int16_t dc_block_4x4[4][4], int16_t dst_block[4][4], int idx)
{
    int x = block_idx_x[idx];
    int y = block_idx_y[idx];

    memcpy(&dst_block[0][0], &src_block_4x4[0][0], sizeof(int16_t)*16);
    dst_block[0][0] = dc_block_4x4[x][y];	
}

static void add4x4_idct( uint8_t *p_dst, int i_dst, int16_t dct[4][4] )
{
    int16_t d[4][4];
    int16_t tmp[4][4];
    int x, y;
    int i;

    for( i = 0; i < 4; i++ )
    {
        const int s02 =  dct[i][0]     +  dct[i][2];
        const int d02 =  dct[i][0]     -  dct[i][2];
        const int s13 =  dct[i][1]     + (dct[i][3]>>1);
        const int d13 = (dct[i][1]>>1) -  dct[i][3];

        tmp[i][0] = s02 + s13;
        tmp[i][1] = d02 + d13;
        tmp[i][2] = d02 - d13;
        tmp[i][3] = s02 - s13;
        //if(c2 == 0) printf("%d %d %d %d\n", dct[i][0], dct[i][1], dct[i][2], dct[i][3]);
    }

    for( i = 0; i < 4; i++ )
    {
        const int s02 =  tmp[0][i]     +  tmp[2][i];
        const int d02 =  tmp[0][i]     -  tmp[2][i];
        const int s13 =  tmp[1][i]     + (tmp[3][i]>>1);
        const int d13 = (tmp[1][i]>>1) -   tmp[3][i];

        d[0][i] = ( s02 + s13 + 32 ) >> 6;
        d[1][i] = ( d02 + d13 + 32 ) >> 6;
        d[2][i] = ( d02 - d13 + 32 ) >> 6;
        d[3][i] = ( s02 - s13 + 32 ) >> 6;
    }

    for( y = 0; y < 4; y++ )
    {
        for( x = 0; x < 4; x++ )
        {
            p_dst[x] = clip_uint8( p_dst[x] + d[y][x] );
        }
        p_dst += i_dst;
    }
}

static void x264_post_16x16_dct(int idx, uint8_t src_block_4x4[16], uint8_t recon_block[16*16])
{
    int x = block_idx_x[idx];
    int y = block_idx_y[idx];
    int i, j;

    for (i = 0 ; i < 4 ; i++)
        for (j = 0 ; j < 4 ; j++)
            recon_block[(y*4 + i) * 16 + (x*4) + j] = src_block_4x4[i*4 + j];
}

static void scan_zigzag_4x4full( int level[16], int16_t dct[4][4] )
{
    ZIG( 0,0,0) ZIG( 1,0,1) ZIG( 2,1,0) ZIG( 3,2,0)
        ZIG( 4,1,1) ZIG( 5,0,2) ZIG( 6,0,3) ZIG( 7,1,2)
        ZIG( 8,2,1) ZIG( 9,3,0) ZIG(10,3,1) ZIG(11,2,2)
        ZIG(12,1,3) ZIG(13,2,3) ZIG(14,3,2) ZIG(15,3,3)
}

static void scan_zigzag_4x4( int level[15], int16_t dct[4][4] )
{
    ZIG( 0,0,1) ZIG( 1,1,0) ZIG( 2,2,0)
        ZIG( 3,1,1) ZIG( 4,0,2) ZIG( 5,0,3) ZIG( 6,1,2)
        ZIG( 7,2,1) ZIG( 8,3,0) ZIG( 9,3,1) ZIG(10,2,2)
        ZIG(11,1,3) ZIG(12,2,3) ZIG(13,3,2) ZIG(14,3,3)
}

static void dct4x4dc( int16_t d_in[4][4], int16_t d_out[4][4] )
{
    int16_t tmp[4][4];
    int s01, s23;
    int d01, d23;
    int i;

    for( i = 0; i < 4; i++ )
    {
        s01 = d_in[i][0] + d_in[i][1];
        d01 = d_in[i][0] - d_in[i][1];
        s23 = d_in[i][2] + d_in[i][3];
        d23 = d_in[i][2] - d_in[i][3];

        tmp[0][i] = s01 + s23;
        tmp[1][i] = s01 - s23;
        tmp[2][i] = d01 - d23;
        tmp[3][i] = d01 + d23;
    }

    for( i = 0; i < 4; i++ )
    {
        s01 = tmp[i][0] + tmp[i][1];
        d01 = tmp[i][0] - tmp[i][1];
        s23 = tmp[i][2] + tmp[i][3];
        d23 = tmp[i][2] - tmp[i][3];

        d_out[0][i] = ( s01 + s23 + 1 ) >> 1;
        d_out[1][i] = ( s01 - s23 + 1 ) >> 1;
        d_out[2][i] = ( d01 - d23 + 1 ) >> 1;
        d_out[3][i] = ( d01 + d23 + 1 ) >> 1;
    }
}

static inline void pixel_avg( uint8_t *dst,  int i_dst_stride,
        uint8_t *src1, int i_src1_stride,
        uint8_t *src2, int i_src2_stride,
        int i_width, int i_height )
{
    int x, y;
    for( y = 0; y < i_height; y++ )
    {
        for( x = 0; x < i_width; x++ )
        {
            dst[x] = ( src1[x] + src2[x] + 1 ) >> 1;
        }
        dst  += i_dst_stride;
        src1 += i_src1_stride;
        src2 += i_src2_stride;
    }
}

static void mc_copy(uint8_t *src, int i_src_stride, uint8_t *dst, int i_dst_stride, int i_width, int i_height )
{
    int y;

    for( y = 0; y < i_height; y++ )
    {
        memcpy( dst, src, i_width );

        src += i_src_stride;
        dst += i_dst_stride;
    }
}

static void mc_luma(uint8_t src [4][48*48], int i_src_stride,
        uint8_t *dst,    int i_dst_stride,
        int mvx,int mvy,
        int i_width, int i_height )
{
    uint8_t *src1, *src2;

    int correction = (mvx&1) && (mvy&1) && ((mvx&2) ^ (mvy&2));
    int hpel1x = mvx>>1;
    int hpel1y = (mvy+1-correction)>>1;
    int shpel1x = hpel1x >> 1;
    int shpel1y = hpel1y >> 1;
    int filter1 = (hpel1x & 1) + ( (hpel1y & 1) << 1 );

    src1 = src[filter1] + (shpel1y + 16) * i_src_stride + (shpel1x + 16);

    if ( (mvx|mvy) & 1 ) /* qpel interpolation needed */
    {
        int hpel2x = (mvx+1)>>1;
        int hpel2y = (mvy+correction)>>1;
        int shpel2x = hpel2x >> 1;
        int shpel2y = hpel2y >> 1;
        int filter2 = (hpel2x & 1) + ( (hpel2y & 1) <<1 );

        src2 = src[filter2] + (shpel2y+16) * i_src_stride + (shpel2x +16);

        pixel_avg(dst, i_dst_stride, src1, i_src_stride, src2, i_src_stride, i_width, i_height );
    }
    else
    {
        mc_copy(src1, i_src_stride, dst, i_dst_stride, i_width, i_height );
    }
}

static void motion_compensation_chroma_new( uint8_t *src, int i_src_stride,
        uint8_t *dst, int i_dst_stride,
        int mvx, int mvy,
        int i_width, int i_height )
{
    uint8_t *srcp;
    int x, y;

    const int d8x = mvx&0x07;
    const int d8y = mvy&0x07;

    const int cA = (8-d8x)*(8-d8y);
    const int cB = d8x    *(8-d8y);
    const int cC = (8-d8x)*d8y;
    const int cD = d8x    *d8y;

    src  += ((mvy >> 3)+8) * i_src_stride + ((mvx >> 3)+8);
    srcp = &src[i_src_stride];

    for( y = 0; y < i_height; y++ )
    {
        for( x = 0; x < i_width; x++ )
        {
            dst[x] = ( cA*src[x]  + cB*src[x+1] +
                    cC*srcp[x] + cD*srcp[x+1] + 32 ) >> 6;
        }
        dst  += i_dst_stride;

        src   = srcp;
        srcp += i_src_stride;
    }
}

static void x264_mb_mc_0xywh_new(int x, int y, int width, int height, uint8_t ME_ref[4][48*48], uint8_t ME_ref_chroma[2][24*24], 
        int16_t mv_mb[2][48][2], int8_t ref_mb[2][48], uint8_t *dst, uint8_t *dstU, uint8_t *dstV)
{
    const int i8 = x264_scan8[0]+x+8*y;
    const int i_ref = ref_mb[0][i8];
    int mvx = mv_mb[0][i8][0];
    const int mvy = mv_mb[0][i8][1];

    mc_luma( ME_ref, 48, &dst[4*y*16 +4*x], 16,
            mvx + 4*4*x, mvy + 4*4*y, 4*width, 4*height );

    motion_compensation_chroma_new( &ME_ref_chroma[0][2*y*24 + 2*x], 24, &dstU[2*y*8 +2*x], 8,
            mvx, mvy, 2*width, 2*height );

    motion_compensation_chroma_new( &ME_ref_chroma[1][2*y*24 + 2*x], 24, &dstV[2*y*8 +2*x], 8,
            mvx, mvy, 2*width, 2*height );
}

static void x264_mb_mc(int i_type, int i_partition, int i_sub_partition[4], int16_t mv_mb[2][48][2], int8_t ref_mb[2][48], 
        uint8_t ME_ref[4][48*48], uint8_t ME_ref_chroma[2][24*24],
        uint8_t recon_block[256], uint8_t recon_block_U[64], uint8_t recon_block_V[64])
{
    if( i_type == P_L0 )
    {
        if( i_partition == D_16x16 )
        {
            x264_mb_mc_0xywh_new( 0, 0, 4, 4, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V );
        }
        else if( i_partition == D_16x8 )
        {
            x264_mb_mc_0xywh_new( 0, 0, 4, 2, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
            x264_mb_mc_0xywh_new( 0, 2, 4, 2, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
        }
        else if( i_partition == D_8x16 )
        {
            x264_mb_mc_0xywh_new( 0, 0, 2, 4, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
            x264_mb_mc_0xywh_new( 2, 0, 2, 4, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
        }
    }
    else if( i_type == P_8x8)
    {
        int i;
        for( i = 0; i < 4; i++ )
        {
            const int x = 2*(i%2);
            const int y = 2*(i/2);
            switch( i_sub_partition[i] )
            {
                case D_L0_8x8:
                    x264_mb_mc_0xywh_new( x, y, 2, 2, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    break;
                case D_L0_8x4:
                    x264_mb_mc_0xywh_new( x, y+0, 2, 1, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    x264_mb_mc_0xywh_new( x, y+1, 2, 1, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    break;
                case D_L0_4x8:
                    x264_mb_mc_0xywh_new( x+0, y, 1, 2, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    x264_mb_mc_0xywh_new( x+1, y, 1, 2, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    break;
                case D_L0_4x4:
                    x264_mb_mc_0xywh_new( x+0, y+0, 1, 1, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    x264_mb_mc_0xywh_new( x+1, y+0, 1, 1, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    x264_mb_mc_0xywh_new( x+0, y+1, 1, 1, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    x264_mb_mc_0xywh_new( x+1, y+1, 1, 1, ME_ref, ME_ref_chroma, mv_mb, ref_mb, recon_block, recon_block_U, recon_block_V  );
                    break;
                    /*
                    // do not support direct mode yet
                    case D_DIRECT_8x8:
                    x264_mb_mc_direct8x8( x, y );
                    break;
                     */
            }
        }
    }    
}

static int x264_mb_decimate_score( int *dct, int i_max )
{
    static const int i_ds_table4[16] = {
        3,2,2,1,1,1,0,0,0,0,0,0,0,0,0,0 };
    static const int i_ds_table8[64] = {
        3,3,3,3,2,2,2,2,2,2,2,2,1,1,1,1,
        1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

    const int *ds_table = (i_max == 64) ? i_ds_table8 : i_ds_table4;
    int i_score = 0;
    int idx = i_max - 1;

    while( idx >= 0 && dct[idx] == 0 )
        idx--;

    while( idx >= 0 )
    {
        int i_run;

        if( abs( dct[idx--] ) > 1 )
            return 9;

        i_run = 0;
        while( idx >= 0 && dct[idx] == 0 )
        {
            idx--;
            i_run++;
        }
        i_score += ds_table[i_run];
    }

    return i_score;
}

static void x264_set_nnz(int i_decimate[16], struct IntArr16 luma4x4[16], struct IntArr16 luma4x4_out[16], int do_it_out[16])
{
    int i_decimate_8x8[4];
    int i_decimate_total;

    i_decimate_8x8[0] = i_decimate[0]+i_decimate[1]+i_decimate[2]+i_decimate[3];
    i_decimate_8x8[1] = i_decimate[4]+i_decimate[5]+i_decimate[6]+i_decimate[7];
    i_decimate_8x8[2] = i_decimate[8]+i_decimate[9]+i_decimate[10]+i_decimate[11];
    i_decimate_8x8[3] = i_decimate[12]+i_decimate[13]+i_decimate[14]+i_decimate[15];

    i_decimate_total = i_decimate_8x8[0] + i_decimate_8x8[1] + i_decimate_8x8[2] + i_decimate_8x8[3];

    if (i_decimate_total < 6) {
        int i;
        for (i = 0 ; i < 16 ; i++) {
            do_it_out[i] = 0;
            memset(luma4x4_out[i].data, 0, 16*sizeof(int));
        }
    }	
    else {
        int i;

        for (i = 0 ; i < 4 ; i++) {
            if (i_decimate_8x8[i] < 4) {
                int j; 
                for (j = 0 ; j < 4 ; j++) do_it_out[i*4+j] = 0;
                memset(luma4x4_out[i*4].data, 0, 16*sizeof(int));
                memset(luma4x4_out[i*4+1].data, 0, 16*sizeof(int));
                memset(luma4x4_out[i*4+2].data, 0, 16*sizeof(int));
                memset(luma4x4_out[i*4+3].data, 0, 16*sizeof(int));
            }
            else {
                int j;
                for (j = 0 ; j < 4 ; j++) {
                    do_it_out[i*4+j] = 1;
                    memcpy(luma4x4_out[i*4+j].data, luma4x4[i*4+j].data, 16*sizeof(int));
                }
            }
        }
    }		
}

static void predict_8x8c_dc( uint8_t *src, uint8_t preddata8[2][9] )
{		
    int x,y;
    int s0 = 0, s1 = 0, s2 = 0, s3 = 0;
    int dc0, dc1, dc2, dc3;
    int i;

    /* First do :
       s0 s1
       s2
       s3
     */
    for( i = 0; i < 4; i++ )
    {
        s0 += preddata8[0][i + 1];
        s1 += preddata8[0][i + 5];
        s2 += preddata8[1][i + 1];
        s3 += preddata8[1][i + 5];
    }
    /* now calculate
       dc0 dc1
       dc2 dc3
     */
    dc0 = ( s0 + s2 + 4 ) >> 3;
    dc1 = ( s1 + 2 ) >> 2;
    dc2 = ( s3 + 2 ) >> 2;
    dc3 = ( s1 + s3 + 4 ) >> 3;

    for( y = 0; y < 4; y++ )
    {
        for( x = 0; x < 4; x++ )
        {
            src[             x    ] = dc0;
            src[             x + 4] = dc1;
            src[4*8 + x    ] = dc2;
            src[4*8 + x + 4] = dc3;
        }
        src += 8;
    }
}

static void predict_8x8c_h( uint8_t *src, uint8_t preddata8[2][9] )
{	
    int i,j;

    for( i = 0; i < 8; i++ )
    {
        uint8_t v;

        v = preddata8[1][i+1];

        for( j = 0; j < 8; j++ )
        {
            src[j] = v;
        }
        src += 8;
    }
}

static void predict_8x8c_v( uint8_t *src, uint8_t preddata8[2][9] )
{			
    int i,j;

    for( i = 0; i < 8; i++ )
    {
        for( j = 0; j < 8; j++ )
        {
            src[i * 8 +j] = preddata8[0][j + 1];
        }
    }
}

static void predict_8x8c_p( uint8_t *src, uint8_t preddata8[2][9] )
{
    int i;
    int x,y;
    int a, b, c;
    int H = 0;
    int V = 0;
    int i00;

    for( i = 0; i < 4; i++ )
    {
        H += ( i + 1 ) * ( preddata8[0][5+i] - preddata8[0][3-i]);
        V += ( i + 1 ) * ( preddata8[1][5+i] - preddata8[1][3-i]);
    }

    a = 16 * ( preddata8[0][8] + preddata8[1][8] );
    b = ( 17 * H + 16 ) >> 5;
    c = ( 17 * V + 16 ) >> 5;
    i00 = a -3*b -3*c + 16;

    for( y = 0; y < 8; y++ )
    {
        for( x = 0; x < 8; x++ )
        {
            int pix;

            pix = (i00 +b*x) >> 5;
            src[x] = clip_uint8( pix );
        }
        src += 8;
        i00 += c;
    }
}

static void predict_8x8c_dc_left( uint8_t *src, uint8_t preddata8[2][9] )
{		
    int x,y;
    int dc0 = 0, dc1 = 0;

    for( y = 0; y < 4; y++ )
    {
        dc0 += preddata8[1][y+1];
        dc1 += preddata8[1][y+5];
    }
    dc0 = ( dc0 + 2 ) >> 2;
    dc1 = ( dc1 + 2 ) >> 2;

    for( y = 0; y < 4; y++ )
    {
        for( x = 0; x < 8; x++ )
        {
            src[           x] = dc0;
            src[4*8+x] = dc1;
        }
        src += 8;
    }
}

static void predict_8x8c_dc_top( uint8_t *src, uint8_t preddata8[2][9] )
{		
    int x,y;
    int dc0 = 0, dc1 = 0;

    for( x = 0; x < 4; x++ )
    {
        dc0 += preddata8[0][x+1];
        dc1 += preddata8[0][x+5];
    }
    dc0 = ( dc0 + 2 ) >> 2;
    dc1 = ( dc1 + 2 ) >> 2;

    for( y = 0; y < 8; y++ )
    {
        for( x = 0; x < 4; x++ )
        {
            src[x    ] = dc0;
            src[x + 4] = dc1;
        }
        src += 8;
    }	
}

static void predict_8x8c_dc_128( uint8_t *src)
{		
    int x,y;

    for( y = 0; y < 8; y++ )
    {
        for( x = 0; x < 8; x++ )
        {
            src[x] = 128;
        }
        src += 8;
    }
}

static void quant_2x2_dc_core( int16_t dct[2][2], int i_quant_mf, int i_qbits, int f )
{
    QUANT_ONE( dct[0][0], i_quant_mf );
    QUANT_ONE( dct[0][1], i_quant_mf );
    QUANT_ONE( dct[1][0], i_quant_mf );
    QUANT_ONE( dct[1][1], i_quant_mf );
}

static void quant_2x2_dc(int16_t dct[2][2], int quant_mf[6][4][4], int i_qscale, int b_intra )
{
    const int i_qbits = 16 + i_qscale / 6;
    const int i_mf = i_qscale % 6;
    const int f = ( 1 << i_qbits ) / ( b_intra ? 3 : 6 );
    quant_2x2_dc_core( dct, quant_mf[i_mf][0][0], i_qbits, f );
}

static void sub8x8_dct( int16_t dct[4][4][4], uint8_t *pix1, int i_pix1, uint8_t *pix2, int i_pix2 )
{
    sub4x4_dct( dct[0], &pix1[0], i_pix1, &pix2[0], i_pix2 );
    sub4x4_dct( dct[1], &pix1[4], i_pix1, &pix2[4], i_pix2 );
    sub4x4_dct( dct[2], &pix1[4*i_pix1+0], i_pix1, &pix2[4*i_pix2+0], i_pix2 );
    sub4x4_dct( dct[3], &pix1[4*i_pix1+4], i_pix1, &pix2[4*i_pix2+4], i_pix2 );
}

static void dct2x2dc( int16_t d[2][2] )
{
    int tmp[2][2];

    tmp[0][0] = d[0][0] + d[0][1];
    tmp[1][0] = d[0][0] - d[0][1];
    tmp[0][1] = d[1][0] + d[1][1];
    tmp[1][1] = d[1][0] - d[1][1];

    d[0][0] = tmp[0][0] + tmp[0][1];
    d[0][1] = tmp[1][0] + tmp[1][1];
    d[1][0] = tmp[0][0] - tmp[0][1];
    d[1][1] = tmp[1][0] - tmp[1][1];
}

static inline void scan_zigzag_2x2_dc( int level[4], int16_t dct[2][2] )
{
    ZIG(0,0,0)
    ZIG(1,0,1)
    ZIG(2,1,0)
    ZIG(3,1,1)
}

static void x264_mb_dequant_2x2_dc( int16_t dct[2][2], int dequant_mf[6][4][4], int i_qp )
{
    const int i_qbits = i_qp/6 - 5;

    if( i_qbits >= 0 )
    {
        const int i_dmf = dequant_mf[i_qp%6][0][0] << i_qbits;
        dct[0][0] *= i_dmf;
        dct[0][1] *= i_dmf;
        dct[1][0] *= i_dmf;
        dct[1][1] *= i_dmf;
    }
    else
    {
        const int i_dmf = dequant_mf[i_qp%6][0][0];
        // chroma DC is truncated, not rounded
        dct[0][0] = ( dct[0][0] * i_dmf ) >> (-i_qbits);
        dct[0][1] = ( dct[0][1] * i_dmf ) >> (-i_qbits);
        dct[1][0] = ( dct[1][0] * i_dmf ) >> (-i_qbits);
        dct[1][1] = ( dct[1][1] * i_dmf ) >> (-i_qbits);
    }
}

static void add8x8_idct( uint8_t *p_dst, int i_dst, int16_t dct[4][4][4] )
{
    add4x4_idct( p_dst, i_dst,             dct[0] );
    add4x4_idct( &p_dst[4], i_dst,         dct[1] );
    add4x4_idct( &p_dst[4*i_dst+0], i_dst, dct[2] );
    add4x4_idct( &p_dst[4*i_dst+4], i_dst, dct[3] );
}

static void x264_mb_encode_8x8_chroma(uint8_t src_block_U[64], uint8_t src_block_V[64], uint8_t recon_block_U[64],  uint8_t recon_block_V[64],
        int b_inter, int i_qscale, int residual_ac[8][15], int chroma_dc[2][4])
{
    int i, ch;

    for( ch = 0; ch < 2; ch++ )
    {
        uint8_t  *p_src = !ch ? src_block_U : src_block_V;
        uint8_t  *p_dst = !ch ? recon_block_U : recon_block_V;
        int i_decimate_score = 0;
        static int16_t dct2x2[2][2];
        int16_t dct4x4[4][4][4];

        sub8x8_dct( dct4x4, p_src, 8, p_dst, 8);

        /* calculate dct coeffs */
        for( i = 0; i < 4; i++ )
        {
            /* copy dc coeff */
            dct2x2[block_idx_y[i]][block_idx_x[i]] = dct4x4[i][0][0];

            /* no trellis; it doesn't seem to help chroma noticeably */
            quant_4x4(dct4x4[i], i_qscale, !b_inter );
            scan_zigzag_4x4( residual_ac[i+ch*4], dct4x4[i] );

            if( b_inter )
            {
                i_decimate_score += x264_mb_decimate_score( residual_ac[i+ch*4], 15 );
            }
        }

        dct2x2dc( dct2x2 );
        quant_2x2_dc( dct2x2, quant4_mf[CQM_4IC + b_inter], i_qscale, !b_inter );
        scan_zigzag_2x2_dc(chroma_dc[ch], dct2x2 );

        /* output samples to fdec */
        dct2x2dc( dct2x2 );
        x264_mb_dequant_2x2_dc( dct2x2, dequant4_mf[CQM_4IC + b_inter], i_qscale );  /* XXX not inversed */

        if( b_inter && i_decimate_score < 7 )
        {
            /* Near null chroma 8x8 block so make it null (bits saving) */
            memset( dct4x4, 0, sizeof( dct4x4 ) );
            memset( &residual_ac[ch*4][0], 0, 4 * 15 * sizeof(int) );
        }
        else
        {
            for( i = 0; i < 4; i++ )
                dequant_4x4( dct4x4[i], i_qscale );
        }
        /* calculate dct coeffs */
        for( i = 0; i < 4; i++ )
        {
            /* copy dc coeff */
            dct4x4[i][0][0] = dct2x2[0][i];
        }
        add8x8_idct( p_dst, 8, dct4x4 );
    }
}

static inline int array_non_zero( int *v, int i_count )
{
    int i;
    for( i = 0; i < i_count; i++ )
        if( v[i] ) return 1;
    return 0;
}

static inline int array_non_zero_count( int *v, int i_count )
{
    int i;
    int i_nz;

    for( i = 0, i_nz = 0; i < i_count; i++ )
        if( v[i] )
            i_nz++;

    return i_nz;
}

static void calc_luma_patern_nzc(struct IntArr16 luma4x4[16], int non_zero_count[48], int luma4x4_out[16][16], int *i_cbp_luma) {
    int i;
    for (i = 0 ; i < 16 ; i++) {
        const int nz = array_non_zero_count(luma4x4[i].data, 16);
        non_zero_count[x264_scan8[i]] = nz;
        if (nz > 0)
            *i_cbp_luma |= 1 << (i/4);
        memcpy(&luma4x4_out[i][0], luma4x4[i].data, sizeof(int)*16);
    }
}

static void calc_luma_patern_nzc_16(struct IntArr15 residual_ac[16], int non_zero_count[48], int luma_residual_ac[16][15], int *i_cbp_luma) {
    int i;
    for (i = 0 ; i < 16 ; i++) {
        const int nz = array_non_zero_count(residual_ac[i].data, 15);
        non_zero_count[x264_scan8[i]] = nz;
        if (nz > 0)
            *i_cbp_luma = 0x0f;
        memcpy(&luma_residual_ac[i][0], residual_ac[i].data, sizeof(int)*15);
    }
}

static void calc_chroma_patern_nzc(int chroma_residual_ac[8][15], int chroma_dc[2][4], int non_zero_count[48], int *i_cbp_chroma) {
    int i;
    for (i = 0 ; i < 8 ; i++) {
        const int nz = array_non_zero_count(chroma_residual_ac[i], 15);
        non_zero_count[x264_scan8[16+i]] = nz;
        if (nz > 0)
            *i_cbp_chroma = 0x02;
    }

    if (*i_cbp_chroma == 0x00 && array_non_zero(chroma_dc[0], 8) )
        *i_cbp_chroma = 0x01;
}



/*********************
  DAL FUNCTIONS
**********************/

void encoder_init(DALProcess *p)
{
	p->local->do_it_1438 = 0;
    p->local->do_it_1439 = 0;
    p->local->best_block_4x4_1440 = 15;
    p->local->do_it_1441 = 0;
    p->local->src_block_1442 = 0;
    p->local->best_block_1443 = 0;
    p->local->dct4x4_1444 = 0;
    p->local->do_it_1445 = 0;
    p->local->do_it_1446 = 0;
    p->local->frame_type_1447_phase = 0;
    p->local->frame_type_1447 = 0;
    p->local->dct4x4_in_1448 = 0;
    p->local->dct4x4_out_1449 = 0;
    p->local->do_it_1450 = 0;
    p->local->frame_type_1451_phase = 0;
    p->local->dct4x4_in_1452 = 0;
    p->local->do_it_1453 = 0;
    p->local->do_it_1454 = 0;
    p->local->frame_type_1455_phase = 0;
    p->local->do_it_1456 = 0;
    p->local->frame_type_1457_phase = 0;
    p->local->frame_type_1457 = 0;
    p->local->dct4x4_in_1458 = 0;
    p->local->dct4x4_out_1459 = 0;
    p->local->do_it_1460 = 0;
    p->local->src_block_4x4_1461 = 0;
    p->local->dc_block_4x4_1462_phase = 0;
    p->local->do_it_1463 = 0;
    p->local->best_block_1464 = 0;
    p->local->dst_block_1465 = 0;
    p->local->do_it_1466 = 0;
    p->local->do_it_1467 = 0;
    p->local->do_it_1468 = 0;
    p->local->dct4x4_1469 = 0;
    p->local->residual_ac_1470 = 0;
    p->local->output_1320 = 0;
    p->local->input_1471 = 0;
    {int i; for(i=0;i<1584;i++) p->local->output_1326[i] = 0;}
    p->local->output_1472 = 15;
    {int i; for(i=0;i<16;i++) p->local->output_1346[i] = 0;}
    p->local->do_it_1473 = 0;
    p->local->dst_block_1474 = 0;
    p->local->recon_block_U_1475 = 0;
    p->local->recon_block_V_1476 = 0;
    p->local->do_it_1477 = 0;
    p->local->src_block_1478 = 0;
    p->local->best_block_1479 = 0;
    {int i; for(i=0;i<16;i++) p->local->output_1358[i] = 0;}
    p->local->do_it_1480 = 0;
    p->local->frame_type_1481_phase = 0;
    p->local->frame_type_1481 = 0;
    p->local->dct4x4_out_1482 = 0;
    p->local->do_it_1483 = 0;
    p->local->dct4x4_1484 = 0;
    p->local->luma4x4_1485 = 0;
    {int i; for(i=0;i<16;i++) p->local->output_1369[i] = 0;}
    p->local->do_it_1486 = 0;
    p->local->luma4x4_1487 = 0;
    {int i; for(i=0;i<16;i++) p->local->i_decimate_1374[i] = 0;}
    p->local->i_decimate_1488 = 0;
    p->local->luma4x4_out_1489 = 15;
    {int i; for(i=0;i<16;i++) p->local->do_it_out_1376[i] = 0;}
    p->local->do_it_1490 = 0;
    p->local->frame_type_1491_phase = 0;
    p->local->frame_type_1491 = 0;
    p->local->dct4x4_in_1492 = 0;
    p->local->do_it_1493 = 0;
    p->local->best_block_1494 = 0;
    p->local->do_it_1495 = 0;
    p->local->src_block_predct_1496 = 0;
    p->local->src_block_1497 = 0;
    p->local->output_1383 = 0;
    p->local->output_1384 = 0;
    p->local->do_it_1498 = 0;
    p->local->i_mb_type_1499 = 0;
    p->local->i_frame_type_1500_phase = 0;
    p->local->chroma_pred_mode_1501 = 0;
    p->local->src_block_U_1502 = 0;
    p->local->src_block_V_1503 = 0;
    p->local->recon_block_U_in_1504 = 0;
    p->local->recon_block_V_in_1505 = 0;
    p->local->preddata8_U_1506 = 0;
    p->local->preddata8_V_1507 = 0;
    p->local->i_frame_type_1508_phase = 0;
    p->local->i_mb_type_1509 = 0;
    p->local->luma4x4_4I_1510 = 15;
    p->local->luma4x4_4P_1511 = 15;
    p->local->residual_ac_16_1512 = 15;
    p->local->luma4x4_Inter_1513 = 15;
    p->local->non_zero_count_in_1514 = 0;
    p->local->i_cbp_luma_1391 = 0;
    p->local->i_cbp_chroma_1392 = 0;
    {int i; for(i=0;i<99;i++) p->local->output_1404[i] = 0;}
    p->local->output_1515 = 0;
    p->local->output_1410 = 0;
    p->local->output_1519_phase = 0;
    p->local->output_1411 = 0;
    p->local->output_1521_phase = 0;
    p->local->output_1412 = 0;
    p->local->output_1413 = 0;
    p->local->output_1421 = 0;
    p->local->output_1423 = 0;
    p->local->output_1424 = 0;
    p->local->output_1425 = 0;
    {int i; for(i=0;i<99;i++) p->local->output_1426[i] = 0;}
    p->local->output_1539 = 0;
    p->local->output_1427 = 0;
    p->local->output_1541_phase = 0;
    {int i; for(i=0;i<99;i++) p->local->output_1428[i] = 0;}
    p->local->output_1543 = 0;
    p->local->output_1545 = 0;
    p->local->output_1547 = 0;
    p->local->output_1549 = 0;
    p->local->output_1551 = 0;
    p->local->output_1433 = 0;
    p->local->output_1555_phase = 0;
    {int i; for(i=0;i<99;i++ ) p->local->output_1434[i] = 0;}
    p->local->output_1557 = 0;
    p->local->output_1559 = 15;
    p->local->output_1561 = 15;
    p->local->output_1563 = 0;
    {int i; for(i=0;i<64;i++) p->local->recon_block_U_1389.data[i] = 0;}
    {int i; for(i=0;i<64;i++) p->local->recon_block_V_1390.data[i] = 0;}

    p->local->sdfLoopCounter_16=0;

////////////////////////////////////
    p->local->counter=0;
    {int i; for(i=0;i<25;i++) p->local->myutime[i] = 0;}
    {int i; for(i=0;i<25;i++) p->local->mystime[i] = 0;}
////////////////////////////////////
}


int encoder_fire(DALProcess *p)
{
    {
        {

            DAL_read((void*)PORT_IN, &p->local->m, sizeof(MEEncPacket), p);

            memcpy(&(p->local->e_r_info), &(p->local->m.e_s_info), sizeof(encoder_send_info));
            memcpy(&(p->local->i_to_e_info), &(p->local->m.i_info), sizeof(init_info));

            if(p->local->sdfLoopCounter_16 == 0){
                p->local->output_1424 = p->local->m.o_intFrameType;
                p->local->output_1412 = p->local->m.o_intFrameType;
                p->local->m_o_intFrameType = p->local->m.o_intFrameType;
                memcpy(&(p->local->m_output), &(p->local->m.output), sizeof(int)*99);
                memcpy(&(p->local->m_o_msgSliceHeader), &(p->local->m.o_msgSliceHeader), sizeof(SliceHeader));

                {  // encoder2I39.x264_v1_EncodeInterI9.RepeatI83 (class CGCRepeat)
                    {
                        int i;
                        for (i = 0; i < 16; i++) {
                            p->local->output_1369[(15-(i))] = p->local->output_1424;
                        }
                    }
                }

                {  // encoder2I39.x264_v1_Encode16x16I0.RepeatI150 (class CGCRepeat)
                    {
                        int i;
                        for (i = 0; i < 16; i++) {
                            p->local->output_1346[(15-(i))] = p->local->output_1412;
                        }
                    }
                }
            }

            p->local->d.MB_index_Enc = p->local->sdfLoopCounter_16;
            p->local->output_1423 = p->local->m_output[p->local->sdfLoopCounter_16];
            
            {  /* encoder2I39.x264_v1_EncodeInterI9.RepeatI56 (class CGCRepeat) */
                {
                    int i;
                    for (i = 0; i < 16; i++) {
                        p->local->output_1358[(15-(i))] = p->local->output_1423;
                    }
                }
            }

            {  /* encoder2I39.ConstIntI97 (class CGCConstInt) */
                p->local->output_1404[p->local->output_1515] = 1.0;
                p->local->output_1515 += 1;
            }

            {  /* encoder2I39.x264_v1_Encode16x16I0.RepeatI114 (class CGCRepeat) */
                {
                    int i;
                    for (i = 0; i < 16; i++) {
                        p->local->output_1326[(p->local->output_1472-(i))] = p->local->output_1404[p->local->input_1471];
                    }
                }
                p->local->input_1471 += 1;
                p->local->output_1472 += 16;
                if (p->local->output_1472 >= 1584)
                    p->local->output_1472 -= 1584;
            }

            memcpy((unsigned char *)&(p->local->output_1426[p->local->output_1539]),&(p->local->e_r_info.mb_type), 4); // from ME MB type
            memcpy((unsigned char *)&(p->local->output_1428[p->local->output_1543]),&(p->local->e_r_info.chroma_pred_mode), 4); //  from ME chroma_pred_mode
            memcpy((unsigned char *)&(p->local->output_1431[p->local->output_1549]),&(p->local->e_r_info.preddata8_U),18); // from ME preddata8_U
            memcpy((unsigned char *)&(p->local->output_1432[p->local->output_1551]),&(p->local->e_r_info.preddata8_V) ,18); // from ME preddata8_V
            memcpy((unsigned char *)&(p->local->output_1434[p->local->output_1557]),&(p->local->e_r_info.mb_type) ,4); // from ME mb_type 
            memcpy((unsigned char *)&(p->local->output_1437[p->local->output_1563]),&(p->local->e_r_info.non_zero_count) ,192);  // from ME non_zero_count
   
            p->local->output_1413 = p->local->m_output[p->local->sdfLoopCounter_16];

            {  /* encoder2I39.x264_v1_EncodeInterI9.XmbmcI20 (class CGCXmbmc) */
                if (p->local->output_1413) 
                    x264_mb_mc(p->local->e_r_info.i_type, p->local->e_r_info.i_partition, p->local->e_r_info.i_sub_partition.data, 
                            p->local->e_r_info.mv_mb.data, p->local->e_r_info.ref_mb.data, p->local->e_r_info.ME_ref.data, p->local->e_r_info.ME_ref_chroma.data, 
                            p->local->recon_block_1350.data, p->local->recon_block_U_1351[p->local->recon_block_U_1475].data, p->local->recon_block_V_1352[p->local->recon_block_V_1476].data);
                p->local->recon_block_U_1475 += 1;
                p->local->recon_block_V_1476 += 1;
            }

            p->local->output_1421 = p->local->m_output[p->local->sdfLoopCounter_16];
            {  /* encoder2I39.x264_v1_EncodeInterI9.XPre16x16DctI32 (class CGCXPre16x16Dct) */
                if (p->local->output_1421) {
                    int i;

                    for (i = 0 ; i < 16 ; i++)			
                        x264_pre_16x16_dct(i, p->local->i_to_e_info.src_block_Y.data, p->local->recon_block_1350.data, p->local->src_block_4x4_1353[(15-((15-i)))].data, p->local->best_block_4x4_1354[(15-((15-i)))].data);
                }
            }

            {  /*encoder2I39.x264_v1_Encode16x16I0.Xpred16x16GetBestI0 (class CGCXpred16x16GetBest) */
                if (p->local->output_1404[p->local->do_it_1438])
                    predict_16x16_get_best(p->local->e_r_info.i_pred16x16, p->local->e_r_info.preddata16_Y.data, p->local->best_block_1299.data);
                p->local->do_it_1438 += 1;
            }

            {  /* encoder2I39.x264_v1_Encode16x16I0.XPre16x16DctI5 (class CGCXPre16x16Dct) */
                if (p->local->output_1404[p->local->do_it_1439]) {
                    int i;

                    for (i = 0 ; i < 16 ; i++)			
                        x264_pre_16x16_dct(i, p->local->i_to_e_info.src_block_Y.data, p->local->best_block_1299.data, p->local->src_block_4x4_1300[(15-((15-i)))].data, p->local->best_block_4x4_1301[(p->local->best_block_4x4_1440-((15-i)))].data);
                }
                p->local->do_it_1439 += 1;
                p->local->best_block_4x4_1440 += 16;
                if (p->local->best_block_4x4_1440 >= 1584)
                    p->local->best_block_4x4_1440 -= 1584;
            }

            memcpy ((unsigned char *)&(p->local->output_1430[p->local->output_1547]),p->local->i_to_e_info.src_block_V.data, 64); // from init now from ME V!

            p->local->output_1547 += 1;
            memcpy ((unsigned char *)&(p->local->output_1429[p->local->output_1545]),p->local->i_to_e_info.src_block_U.data, 64); // from init now from ME U!

            p->local->output_1545 += 1;
            p->local->output_1539 += 1;
            p->local->output_1543 += 1;
            if (p->local->output_1541_phase==0)
                p->local->output_1427 = p->local->m_o_intFrameType;
            p->local->output_1541_phase = (p->local->output_1541_phase+1)%99;

            // preddata8_U,V fixed by jwlee 080518
            p->local->output_1549 += 1;
            p->local->output_1551 += 1;

            {  /* encoder2I39.XEncodeChromaI26 (class CGCXEncodeChroma) */
                if (p->local->output_1404[p->local->do_it_1498]) {
                    int i_qscale = p->local->output_1427 ? QP_CONSTANT : QP_CONSTANT_I;

                    if(IS_INTRA(p->local->output_1426[p->local->i_mb_type_1499]) ) {
                        switch (p->local->output_1428[p->local->chroma_pred_mode_1501])
                        {
                            case I_PRED_CHROMA_DC :
                                predict_8x8c_dc( p->local->recon_block_U_1389.data, p->local->output_1431[p->local->preddata8_U_1506].data);
                                predict_8x8c_dc( p->local->recon_block_V_1390.data, p->local->output_1432[p->local->preddata8_V_1507].data );
                                break;
                            case I_PRED_CHROMA_H :
                                predict_8x8c_h( p->local->recon_block_U_1389.data, p->local->output_1431[p->local->preddata8_U_1506].data );
                                predict_8x8c_h( p->local->recon_block_V_1390.data, p->local->output_1432[p->local->preddata8_V_1507].data );
                                break;
                            case I_PRED_CHROMA_V :
                                predict_8x8c_v( p->local->recon_block_U_1389.data, p->local->output_1431[p->local->preddata8_U_1506].data );
                                predict_8x8c_v( p->local->recon_block_V_1390.data, p->local->output_1432[p->local->preddata8_V_1507].data );
                                break;
                            case I_PRED_CHROMA_P :
                                predict_8x8c_p( p->local->recon_block_U_1389.data, p->local->output_1431[p->local->preddata8_U_1506].data );
                                predict_8x8c_p( p->local->recon_block_V_1390.data, p->local->output_1432[p->local->preddata8_V_1507].data );
                                break;
                            case I_PRED_CHROMA_DC_LEFT :
                                predict_8x8c_dc_left( p->local->recon_block_U_1389.data, p->local->output_1431[p->local->preddata8_U_1506].data );
                                predict_8x8c_dc_left( p->local->recon_block_V_1390.data, p->local->output_1432[p->local->preddata8_V_1507].data );
                                break;
                            case I_PRED_CHROMA_DC_TOP :
                                predict_8x8c_dc_top( p->local->recon_block_U_1389.data, p->local->output_1431[p->local->preddata8_U_1506].data );
                                predict_8x8c_dc_top( p->local->recon_block_V_1390.data, p->local->output_1432[p->local->preddata8_V_1507].data );
                                break;
                            case I_PRED_CHROMA_DC_128 :
                                predict_8x8c_dc_128( p->local->recon_block_U_1389.data);
                                predict_8x8c_dc_128( p->local->recon_block_V_1390.data);
                                break;
                        }
                    }
                    else {
                        memcpy(p->local->recon_block_U_1389.data, p->local->recon_block_U_1351[p->local->recon_block_U_in_1504].data, sizeof(uint8_t)*64);
                        memcpy(p->local->recon_block_V_1390.data, p->local->recon_block_V_1352[p->local->recon_block_V_in_1505].data, sizeof(uint8_t)*64);
                    }

                    x264_mb_encode_8x8_chroma(p->local->output_1429[p->local->src_block_U_1502].data, p->local->output_1430[p->local->src_block_V_1503].data, p->local->recon_block_U_1389.data,  p->local->recon_block_V_1390.data, 
                            !IS_INTRA(p->local->output_1426[p->local->i_mb_type_1499]), i_qscale, p->local->chroma_residual_ac_1387.data, p->local->chroma_dc_1388.data);

                }
                p->local->i_frame_type_1500_phase = (p->local->i_frame_type_1500_phase+1)%99;
                p->local->do_it_1498 += 1;
                p->local->i_mb_type_1499 += 1;
                p->local->chroma_pred_mode_1501 += 1;
                p->local->src_block_U_1502 += 1;
                p->local->src_block_V_1503 += 1;
                p->local->recon_block_U_in_1504 += 1;
                p->local->recon_block_V_in_1505 += 1;
                p->local->preddata8_U_1506 += 1;
                p->local->preddata8_V_1507 += 1;
            }
 
            memcpy(&(p->local->d.recon_block_U), &(p->local->recon_block_U_1389.data), sizeof(uint8_t)*64);
            memcpy(&(p->local->d.recon_block_V), &(p->local->recon_block_V_1390.data), sizeof(uint8_t)*64);
 
            memcpy(&(p->local->e.chroma_dc), &(p->local->chroma_dc_1388), 32);
            memcpy(&(p->local->e.chroma_residual_ac), &(p->local->chroma_residual_ac_1387), 480);

            { 
                int sdfLoopCounter_18;
                for (sdfLoopCounter_18 = 0; sdfLoopCounter_18 < 16; sdfLoopCounter_18++) 
                {

                    {  /* star CIC_TM.Complete_galaxy_TMI0.Complete_galaxy_TMI0_Encoder_1.Complete_galaxy_TMI0_arm926ej_s_1.CIC_TM.Complete_galaxy_TMI0.Complete_galaxy_TM_GI0.encoder2I39.x264_v1_Encode16x16I0.Xsub4x4_dctI11 (class CGCXsub4x4_dct) */
                        if (p->local->output_1326[p->local->do_it_1441]) 
                            sub4x4_dct(p->local->dct4x4_1302[p->local->dct4x4_1444].data, p->local->src_block_4x4_1300[p->local->src_block_1442].data, 4, p->local->best_block_4x4_1301[p->local->best_block_1443].data, 4);
                        p->local->do_it_1441 += 1;
                        p->local->src_block_1442 += 1;
                        if (p->local->src_block_1442 >= 16)
                            p->local->src_block_1442 -= 16;
                        p->local->best_block_1443 += 1;
                        p->local->dct4x4_1444 += 1;
                        if (p->local->dct4x4_1444 >= 16)
                            p->local->dct4x4_1444 -= 16;
                    }

                    {  /*encoder2I39.x264_v1_Encode16x16I0.ConstIntI95 (class CGCConstInt) */
                        p->local->output_1320 = 1.0;
                    }

                    {  /*encoder2I39.x264_v1_Encode16x16I0.Xquant_4x4I24 (class CGCXquant_4x4) */
                        if (p->local->output_1326[p->local->do_it_1446]) {
                            memcpy(p->local->dct4x4_out_1306[p->local->dct4x4_out_1449].data, p->local->dct4x4_1302[p->local->dct4x4_in_1448].data, sizeof(int16_t)*16);
                            quant_4x4(p->local->dct4x4_out_1306[p->local->dct4x4_out_1449].data, p->local->output_1346[p->local->frame_type_1447] ? QP_CONSTANT : QP_CONSTANT_I, p->local->output_1320);
                        }
                        p->local->frame_type_1447_phase = (p->local->frame_type_1447_phase+1)%99;
                        p->local->do_it_1446 += 1;
                        if (p->local->frame_type_1447_phase == 0)
                            p->local->frame_type_1447 += 1;
                        if (p->local->frame_type_1447 >= 16)
                            p->local->frame_type_1447 -= 16;
                        p->local->dct4x4_in_1448 += 1;
                        if (p->local->dct4x4_in_1448 >= 16)
                            p->local->dct4x4_in_1448 -= 16;
                        p->local->dct4x4_out_1449 += 1;
                        if (p->local->dct4x4_out_1449 >= 16)
                            p->local->dct4x4_out_1449 -= 16;
                    }

                    {  /* encoder2I39.x264_v1_Encode16x16I0.Xscan_zigzag_4x4I81 (class CGCXscan_zigzag_4x4) */
                        if (p->local->output_1326[p->local->do_it_1468]) {
                            scan_zigzag_4x4(p->local->residual_ac_1319[p->local->residual_ac_1470].data, p->local->dct4x4_out_1306[p->local->dct4x4_1469].data);
                        }
                        p->local->do_it_1468 += 1;
                        p->local->dct4x4_1469 += 1;
                        if (p->local->dct4x4_1469 >= 16)
                            p->local->dct4x4_1469 -= 16;
                        p->local->residual_ac_1470 += 1;
                    }

                }
            } /* end repeat, depth 3*/

            {  /* encoder2I39.x264_v1_Encode16x16I0.X16x16DcGenI16 (class CGCX16x16DcGen) */
                if (p->local->output_1404[p->local->do_it_1445]) {
                    int i;

                    for (i = 0 ; i < 16 ; i++)			
                        x264_16x16_dc_gen(i, p->local->dct4x4_1302[(15-((15-i)))].data, p->local->dc_block_4x4_1303.data);
                }
                p->local->do_it_1445 += 1;
            }

            {  /* encoder2I39.x264_v1_Encode16x16I0.Xdct4x4dcI157 (class CGCXdct4x4dc) */
                if (p->local->output_1404[p->local->do_it_1473]) 
                    dct4x4dc(p->local->dc_block_4x4_1303.data, p->local->dst_block_1349[p->local->dst_block_1474].data);
                p->local->do_it_1473 += 1;
                p->local->dst_block_1474 += 1;
            }

            if (p->local->output_1519_phase==0)
                p->local->output_1410 = p->local->m_o_intFrameType;
            p->local->output_1519_phase = (p->local->output_1519_phase+1)%99;
            {  /* encoder2I39.x264_v1_Encode16x16I0.Xquant4x4dcI30 (class CGCXquant4x4dc) */
                if (p->local->output_1404[p->local->do_it_1450]) {
                    memcpy(p->local->dct4x4_out_1307.data, p->local->dst_block_1349[p->local->dct4x4_in_1452].data, sizeof(int16_t)*16);
                    quant_4x4_dc(p->local->dct4x4_out_1307.data, p->local->output_1410 ? QP_CONSTANT : QP_CONSTANT_I);
                }
                p->local->frame_type_1451_phase = (p->local->frame_type_1451_phase+1)%99;
                p->local->do_it_1450 += 1;
                p->local->dct4x4_in_1452 += 1;
            }

            {  /* x264_v1_Encode16x16I0.Xidct4x4dcI35 (class CGCXidct4x4dc) */
                if (p->local->output_1404[p->local->do_it_1453]) {
                    memcpy(p->local->dst_block_1308.data, p->local->dct4x4_out_1307.data, sizeof(int16_t)*4*4);
                    idct4x4dc(p->local->dst_block_1308.data);
                }
                p->local->do_it_1453 += 1;
            }

            if (p->local->output_1521_phase==0)
                p->local->output_1411 = p->local->m_o_intFrameType;
            p->local->output_1521_phase = (p->local->output_1521_phase+1)%99;
            {  /* .encoder2I39.x264_v1_Encode16x16I0.Xdequant4x4dcI39 (class CGCXdequant4x4dc) */
                if (p->local->output_1404[p->local->do_it_1454]) {
                    memcpy(p->local->dct4x4_out_1309.data, p->local->dst_block_1308.data, sizeof(int16_t)*16);
                    dequant_4x4_dc(p->local->dct4x4_out_1309.data, p->local->output_1411 ? QP_CONSTANT : QP_CONSTANT_I);
                }
                p->local->frame_type_1455_phase = (p->local->frame_type_1455_phase+1)%99;
                p->local->do_it_1454 += 1;
            }

            {  /* encoder2I39.x264_v1_Encode16x16I0.Xscan_zigzag_4x4fullI69 (class CGCXscan_zigzag_4x4full) */
                if (p->local->output_1404[p->local->do_it_1467]) {
                    scan_zigzag_4x4full(p->local->luma4x4_1314.data, p->local->dct4x4_out_1307.data);
                }
                p->local->do_it_1467 += 1;
            }

            memcpy(&(p->local->e.luma4x4), &(p->local->luma4x4_1314), 64); 
            { 
                int sdfLoopCounter_19;
                for (sdfLoopCounter_19 = 0; sdfLoopCounter_19 < 16; sdfLoopCounter_19++) 
                {

                    {  /* encoder2I39.x264_v1_EncodeInterI9.Xsub4x4_dctI38 (class CGCXsub4x4_dct) */
                        if (p->local->output_1358[p->local->do_it_1477]) 
                            sub4x4_dct(p->local->dct4x4_1355.data, p->local->src_block_4x4_1353[p->local->src_block_1478].data, 4, p->local->best_block_4x4_1354[p->local->best_block_1479].data, 4);
                        p->local->do_it_1477 += 1;
                        if (p->local->do_it_1477 >= 16)
                            p->local->do_it_1477 -= 16;
                        p->local->src_block_1478 += 1;
                        if (p->local->src_block_1478 >= 16)
                            p->local->src_block_1478 -= 16;
                        p->local->best_block_1479 += 1;
                        if (p->local->best_block_1479 >= 16)
                            p->local->best_block_1479 -= 16;
                    }

                    {  /* encoder2I39.x264_v1_EncodeInterI9.ConstIntI129 (class CGCConstInt) */
                        p->local->output_1384 = 0.0;
                    }

                    {  /* encoder2I39.x264_v1_EncodeInterI9.Xquant_4x4I64 (class CGCXquant_4x4) */
                        if (p->local->output_1358[p->local->do_it_1480]) {
                            memcpy(p->local->dct4x4_out_1362[p->local->dct4x4_out_1482].data, p->local->dct4x4_1355.data, sizeof(int16_t)*16);
                            quant_4x4(p->local->dct4x4_out_1362[p->local->dct4x4_out_1482].data, p->local->output_1369[p->local->frame_type_1481] ? QP_CONSTANT : QP_CONSTANT_I, p->local->output_1384);
                        }
                        p->local->frame_type_1481_phase = (p->local->frame_type_1481_phase+1)%99;
                        p->local->do_it_1480 += 1;
                        if (p->local->do_it_1480 >= 16)
                            p->local->do_it_1480 -= 16;
                        if (p->local->frame_type_1481_phase == 0)
                            p->local->frame_type_1481 += 1;
                        if (p->local->frame_type_1481 >= 16)
                            p->local->frame_type_1481 -= 16;
                        p->local->dct4x4_out_1482 += 1;
                        if (p->local->dct4x4_out_1482 >= 16)
                            p->local->dct4x4_out_1482 -= 16;
                    }

                    {  /*encoder2I39.x264_v1_EncodeInterI9.Xscan_zigzag_4x4fullI74 (class CGCXscan_zigzag_4x4full) */
                        if (p->local->output_1358[p->local->do_it_1483]) {
                            scan_zigzag_4x4full(p->local->luma4x4_1365[p->local->luma4x4_1485].data, p->local->dct4x4_out_1362[p->local->dct4x4_1484].data);
                        }
                        p->local->do_it_1483 += 1;
                        if (p->local->do_it_1483 >= 16)
                            p->local->do_it_1483 -= 16;
                        p->local->dct4x4_1484 += 1;
                        if (p->local->dct4x4_1484 >= 16)
                            p->local->dct4x4_1484 -= 16;
                        p->local->luma4x4_1485 += 1;
                        if (p->local->luma4x4_1485 >= 16)
                            p->local->luma4x4_1485 -= 16;
                    }

                    {  /*encoder2I39.x264_v1_EncodeInterI9.ConstIntI127 (class CGCConstInt) */
                        p->local->output_1383 = 16.0;
                    }

                    {  /* encoder2I39.x264_v1_EncodeInterI9.Xmb_decimate_scoreI94 (class CGCXmb_decimate_score) */
                        if (p->local->output_1358[p->local->do_it_1486]) {
                            p->local->i_decimate_1374[p->local->i_decimate_1488] = x264_mb_decimate_score(p->local->luma4x4_1365[p->local->luma4x4_1487].data, p->local->output_1383);
                        }
                        p->local->do_it_1486 += 1;
                        if (p->local->do_it_1486 >= 16)
                            p->local->do_it_1486 -= 16;
                        p->local->luma4x4_1487 += 1;
                        if (p->local->luma4x4_1487 >= 16)
                            p->local->luma4x4_1487 -= 16;
                        p->local->i_decimate_1488 += 1;
                        if (p->local->i_decimate_1488 >= 16)
                            p->local->i_decimate_1488 -= 16;
                    }

                    {  /* encoder2I39.x264_v1_Encode16x16I0.Xdequant_4x4I44 (class CGCXdequant_4x4) */
                        if (p->local->output_1326[p->local->do_it_1456]) {
                            memcpy(p->local->dct4x4_out_1310[p->local->dct4x4_out_1459].data, p->local->dct4x4_out_1306[p->local->dct4x4_in_1458].data, sizeof(int16_t)*16);
                            dequant_4x4(p->local->dct4x4_out_1310[p->local->dct4x4_out_1459].data, p->local->output_1346[p->local->frame_type_1457] ? QP_CONSTANT : QP_CONSTANT_I);
                        }
                        p->local->frame_type_1457_phase = (p->local->frame_type_1457_phase+1)%99;
                        p->local->do_it_1456 += 1;
                        if (p->local->frame_type_1457_phase == 0)
                            p->local->frame_type_1457 += 1;
                        if (p->local->frame_type_1457 >= 16)
                            p->local->frame_type_1457 -= 16;
                        p->local->dct4x4_in_1458 += 1;
                        if (p->local->dct4x4_in_1458 >= 16)
                            p->local->dct4x4_in_1458 -= 16;
                        p->local->dct4x4_out_1459 += 1;
                        if (p->local->dct4x4_out_1459 >= 1569)
                            p->local->dct4x4_out_1459 -= 1569;
                    }

                    {  /*encoder2I39.x264_v1_Encode16x16I0.X16x16DcMergeI49 (class CGCX16x16DcMerge) */
                        if (p->local->output_1326[p->local->do_it_1460]) {
                            x264_16x16_dc_merge(p->local->dct4x4_out_1310[p->local->src_block_4x4_1461].data, p->local->dct4x4_out_1309.data, p->local->dst_block_4x4_1311.data, p->local->dc_block_4x4_1462_phase);
                        }
                        p->local->dc_block_4x4_1462_phase = (p->local->dc_block_4x4_1462_phase+1)%16;
                        p->local->do_it_1460 += 1;
                        p->local->src_block_4x4_1461 += 1;
                        if (p->local->src_block_4x4_1461 >= 1569)
                            p->local->src_block_4x4_1461 -= 1569;
                    }

                    {  /*encoder2I39.x264_v1_Encode16x16I0.Xadd4x4_idctI54 (class CGCXadd4x4_idct) */
                        if (p->local->output_1326[p->local->do_it_1463]) {
                            memcpy(p->local->dst_block_1312[p->local->dst_block_1465].data, p->local->best_block_4x4_1301[p->local->best_block_1464].data, sizeof(uint8_t)*16);
                            add4x4_idct(p->local->dst_block_1312[p->local->dst_block_1465].data, 4, p->local->dst_block_4x4_1311.data);
                        }
                        p->local->do_it_1463 += 1;
                        p->local->best_block_1464 += 1;
                        p->local->dst_block_1465 += 1;
                        if (p->local->dst_block_1465 >= 16)
                            p->local->dst_block_1465 -= 16;
                    }
                }
            } /* end repeat, depth 3*/

            {  /*encoder2I39.x264_v1_EncodeInterI9.XSetNNZI99 (class CGCXSetNNZ) */
                x264_set_nnz(&(p->local->i_decimate_1374[(15-(15))]), &(p->local->luma4x4_1365[(15-(15))]), &(p->local->luma4x4_out_1375[(p->local->luma4x4_out_1489-(15))]), &(p->local->do_it_out_1376[(15-(15))]));
                p->local->luma4x4_out_1489 += 16;
                if (p->local->luma4x4_out_1489 >= 1584)
                    p->local->luma4x4_out_1489 -= 1584;
            }
            p->local->output_1557 += 1;
            if (p->local->output_1555_phase==0)
                p->local->output_1433 = p->local->m_o_intFrameType;
            p->local->output_1555_phase = (p->local->output_1555_phase+1)%99;

            // fixed by jwlee
            p->local->output_1563 += 1;
            {  /* encoder2I39.XNonZeroCountI41 (class CGCXNonZeroCount) */
                memcpy(&(p->local->non_zero_count_1393.data), p->local->output_1437[p->local->non_zero_count_in_1514].data, sizeof(int)*48);
                p->local->i_cbp_luma_1391 = 0x00;
                p->local->i_cbp_chroma_1392 = 0x00;

                if (!p->local->output_1433 && p->local->output_1434[p->local->i_mb_type_1509] == I_4x4)
                    calc_luma_patern_nzc(p->local->e_r_info.luma_4x4, p->local->non_zero_count_1393.data, p->local->luma4x4_out_1394.data, &(p->local->i_cbp_luma_1391));
                else if (p->local->output_1433 && p->local->output_1434[p->local->i_mb_type_1509] == I_4x4)
                    calc_luma_patern_nzc(p->local->e_r_info.luma_4x4, p->local->non_zero_count_1393.data, p->local->luma4x4_out_1394.data, &(p->local->i_cbp_luma_1391));			
                else if (p->local->output_1434[p->local->i_mb_type_1509] == I_16x16)
                    calc_luma_patern_nzc_16(&(p->local->residual_ac_1319[(p->local->residual_ac_16_1512-(15))]), p->local->non_zero_count_1393.data, p->local->luma_residual_ac_1395.data, &(p->local->i_cbp_luma_1391));
                else 
                    calc_luma_patern_nzc(&(p->local->luma4x4_out_1375[(p->local->luma4x4_Inter_1513-(15))]), p->local->non_zero_count_1393.data, p->local->luma4x4_out_1394.data, &(p->local->i_cbp_luma_1391));

                calc_chroma_patern_nzc(p->local->chroma_residual_ac_1387.data, p->local->chroma_dc_1388.data, p->local->non_zero_count_1393.data, &(p->local->i_cbp_chroma_1392));
                p->local->i_frame_type_1508_phase = (p->local->i_frame_type_1508_phase+1)%99;
                p->local->i_mb_type_1509 += 1;
                p->local->luma4x4_4I_1510 += 16;
                if (p->local->luma4x4_4I_1510 >= 1584)
                    p->local->luma4x4_4I_1510 -= 1584;
                p->local->luma4x4_4P_1511 += 16;
                if (p->local->luma4x4_4P_1511 >= 1584)
                    p->local->luma4x4_4P_1511 -= 1584;
                p->local->residual_ac_16_1512 += 16;
                if (p->local->residual_ac_16_1512 >= 1584)
                    p->local->residual_ac_16_1512 -= 1584;
                p->local->luma4x4_Inter_1513 += 16;
                if (p->local->luma4x4_Inter_1513 >= 1584)
                    p->local->luma4x4_Inter_1513 -= 1584;
                p->local->non_zero_count_in_1514 += 1;
            }
 
            p->local->e.i_cbp_luma = p->local->i_cbp_luma_1391;
            p->local->e.i_cbp_chroma = p->local->i_cbp_chroma_1392;
            p->local->e.intFrameType = p->local->m_o_intFrameType;
            memcpy(&(p->local->e.luma4x4_out), &(p->local->luma4x4_out_1394), 1024);
            memcpy(&(p->local->e.luma_residual_ac), &(p->local->luma_residual_ac_1395), 960);
            memcpy(&(p->local->e.non_zero_count), &(p->local->non_zero_count_1393), 192);
            memcpy(&(p->local->e.msgSliceHeader), &(p->local->m_o_msgSliceHeader), 40);
            memcpy(&(p->local->e.e_s_info), &(p->local->m.e_s_info), sizeof(encoder_send_info));
            //if(count == 3)  printf("%d %d\n", i_cbp_luma_1391, i_cbp_chroma_1392);
            
            DAL_write((void*)PORT_OUT1, &(p->local->e), sizeof(EncVLCPacket), p);
            
            memcpy(&(p->local->d.non_zero_count), &(p->local->non_zero_count_1393), 192);
            
            {  /* star CIC_TM.Complete_galaxy_TMI0.Complete_galaxy_TMI0_Encoder_1.Complete_galaxy_TMI0_arm926ej_s_1.CIC_TM.Complete_galaxy_TMI0.Complete_galaxy_TM_GI0.encoder2I39.x264_v1_Encode16x16I0.XPost16x16DctI59 (class CGCXPost16x16Dct) */
                if (p->local->output_1404[p->local->do_it_1466]) {
                    int i;

                    for (i = 0 ; i < 16 ; i++)			
                        x264_post_16x16_dct(i, p->local->dst_block_1312[(15-((15-i)))].data, p->local->recon_block_1313.data);
                }
                p->local->do_it_1466 += 1;
            }

            memcpy(&(p->local->d.recon_block_1313), &(p->local->recon_block_1313), 256);

            int sdfLoopCounter_20;
            for (sdfLoopCounter_20 = 0; sdfLoopCounter_20 < 16; sdfLoopCounter_20++) 
            {

                {  /* encoder2I39.x264_v1_EncodeInterI9.Xdequant_4x4I108 (class CGCXdequant_4x4) */
                    if (p->local->do_it_out_1376[p->local->do_it_1490]) {
                        memcpy(p->local->dct4x4_out_1379.data, p->local->dct4x4_out_1362[p->local->dct4x4_in_1492].data, sizeof(int16_t)*16);
                        dequant_4x4(p->local->dct4x4_out_1379.data, p->local->output_1369[p->local->frame_type_1491] ? QP_CONSTANT : QP_CONSTANT_I);
                    }
                    p->local->frame_type_1491_phase = (p->local->frame_type_1491_phase+1)%99;
                    p->local->do_it_1490 += 1;
                    if (p->local->do_it_1490 >= 16)
                        p->local->do_it_1490 -= 16;
                    if (p->local->frame_type_1491_phase == 0)
                        p->local->frame_type_1491 += 1;
                    if (p->local->frame_type_1491 >= 16)
                        p->local->frame_type_1491 -= 16;
                    p->local->dct4x4_in_1492 += 1;
                    if (p->local->dct4x4_in_1492 >= 16)
                        p->local->dct4x4_in_1492 -= 16;
                }

                {  /* encoder2I39.x264_v1_EncodeInterI9.Xadd4x4_idctI113 (class CGCXadd4x4_idct) */
                    if (p->local->output_1358[p->local->do_it_1493]) {
                        memcpy(p->local->dst_block_1380.data, p->local->best_block_4x4_1354[p->local->best_block_1494].data, sizeof(uint8_t)*16);
                        add4x4_idct(p->local->dst_block_1380.data, 4, p->local->dct4x4_out_1379.data);
                    }
                    p->local->do_it_1493 += 1;
                    if (p->local->do_it_1493 >= 16)
                        p->local->do_it_1493 -= 16;
                    p->local->best_block_1494 += 1;
                    if (p->local->best_block_1494 >= 16)
                        p->local->best_block_1494 -= 16;
                }

                {  /* encoder2I39.x264_v1_EncodeInterI9.XPostDctInterMuxI118 (class CGCXPostDctInterMux) */
                    if (p->local->do_it_out_1376[p->local->do_it_1495]) {
                        memcpy(p->local->src_block_1381[p->local->src_block_1497].data, p->local->dst_block_1380.data, sizeof(uint8_t)*16);
                    }
                    else {
                        memcpy(p->local->src_block_1381[p->local->src_block_1497].data, p->local->best_block_4x4_1354[p->local->src_block_predct_1496].data, sizeof(uint8_t)*16);
                    }
                    p->local->do_it_1495 += 1;
                    if (p->local->do_it_1495 >= 16)
                        p->local->do_it_1495 -= 16;
                    p->local->src_block_predct_1496 += 1;
                    if (p->local->src_block_predct_1496 >= 16)
                        p->local->src_block_predct_1496 -= 16;
                    p->local->src_block_1497 += 1;
                    if (p->local->src_block_1497 >= 16)
                        p->local->src_block_1497 -= 16;
                }
            }

            p->local->output_1425 = p->local->m_output[p->local->sdfLoopCounter_16];
            {  /*encoder2I39.x264_v1_EncodeInterI9.XPost16x16DctI123 (class CGCXPost16x16Dct) */
                if (p->local->output_1425) {
                    int i;

                    for (i = 0 ; i < 16 ; i++)			
                        x264_post_16x16_dct(i, p->local->src_block_1381[(15-((15-i)))].data, p->local->recon_block_1382.data);
                }
            }

            memcpy(&(p->local->d.d_s_info), &(p->local->m.d_s_info), sizeof(deblock_send_info));
            memcpy(&(p->local->d.recon_block_1382), &(p->local->recon_block_1382), 256);
            p->local->d.intFrameType = p->local->m_o_intFrameType;

            DAL_write((void*)PORT_OUT2, &(p->local->d), sizeof(EncDbkPacket), p);
        }
    } /* end repeat, depth 2*/

    p->local->sdfLoopCounter_16++;
    if(p->local->sdfLoopCounter_16 == 99){
        p->local->sdfLoopCounter_16 = 0;

        p->local->output_1515 = 0;
        p->local->input_1471 = 0;
        p->local->recon_block_U_1475 = 0;
        p->local->recon_block_V_1476 = 0;
        p->local->do_it_1438 = 0;
        p->local->do_it_1439 = 0;
        p->local->output_1547 = 0;
        p->local->output_1545 = 0;
        p->local->output_1539 = 0;
        p->local->output_1543 = 0;
        p->local->output_1549 = 0;
        p->local->output_1551 = 0;
        p->local->do_it_1498 = 0;
        p->local->i_mb_type_1499 = 0;
        p->local->chroma_pred_mode_1501 = 0;
        p->local->src_block_U_1502 = 0;
        p->local->src_block_V_1503 = 0;
        p->local->recon_block_U_in_1504 = 0;
        p->local->recon_block_V_in_1505 = 0;
        p->local->preddata8_U_1506 = 0;
        p->local->preddata8_V_1507 = 0;
        p->local->do_it_1441 = 0;
        p->local->best_block_1443 = 0;
        p->local->do_it_1446 = 0;
        p->local->do_it_1468 = 0;
        p->local->residual_ac_1470 = 0;
        p->local->do_it_1445 = 0;
        p->local->do_it_1473 = 0;
        p->local->dst_block_1474 = 0;
        p->local->do_it_1450 = 0;
        p->local->dct4x4_in_1452 = 0;
        p->local->do_it_1453 = 0;
        p->local->do_it_1454 = 0;
        p->local->do_it_1467 = 0;
        p->local->do_it_1456 = 0;
        p->local->do_it_1460 = 0;    
        p->local->do_it_1463 = 0;
        p->local->best_block_1464 = 0;
        p->local->output_1557 = 0;
        p->local->output_1563 = 0;
        p->local->i_mb_type_1509 = 0;
        p->local->non_zero_count_in_1514 = 0;
        p->local->do_it_1466 = 0;
    }

    p->local->counter++;

    return 0;
}

void encoder_finish(DALProcess *p) {
}
