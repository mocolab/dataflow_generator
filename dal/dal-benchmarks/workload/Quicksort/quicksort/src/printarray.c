#include <stdio.h>
#include <stdlib.h>

#include "printarray.h"

void printarray_init(DALProcess *p) {
    p->local->index = 0;
}

int printarray_fire(DALProcess *p) {

  int len;
  int * array;
  int i;
  DAL_read((void*)PORT_IN1, &len, sizeof(int), p); 
#ifdef OUTPUT
  printf("lengh is %d\n",len); fflush(stdout); 
#endif
  array = (int *)malloc(len*sizeof(int));
  DAL_read((void*)PORT_IN2, array, sizeof(int)*len, p); 

#ifdef OUTPUT
  printf("sorted output\n"); fflush(stdout); 
  for(i=0;i<len;i++)
    printf("%d ", array[i]); fflush(stdout); 
  printf("\n"); fflush(stdout); 
#endif

  free(array);
  p->local->index++; 
  
  if (p->local->index >= NUMBER_OF_ARRAYS) {
    DAL_send_event(EVENT_1, p); 
	return 1; 
  }
  
  return 0;
}

void printarray_finish(DALProcess *p) {
}
