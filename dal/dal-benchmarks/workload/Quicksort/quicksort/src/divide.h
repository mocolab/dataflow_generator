#ifndef DIVIDE_H
#define DIVIDE_H

#include <dal.h>
#include "global.h"
#include "stdlib.h"

#define PORT_IN1 1
#define PORT_IN2 2
#define PORT_OUT1 3
#define PORT_OUT2 4
#define PORT_OUT3 5
#define PORT_OUT4 6

typedef struct _local_states {
    int index;
} Divide_State;

void divide_init(DALProcess *);
int divide_fire(DALProcess *);
void divide_finish(DALProcess *);

#endif
