#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "arraygen.h"

// initialization function
void arraygen_init(DALProcess *p) {
    p->local->index = 0;
    srand((unsigned) 0);
}

int arraygen_fire(DALProcess *p) {

  /* generate a random integer array of length L */
  //int num = rand() % 1000;

  int i;
  int len = ARRAY_LEN;
  int * array;
  array = (int *) malloc(sizeof(int)*len);

  for(i=0;i<len;i++)
    array[i] = rand() % (len*2);
 
#if DEBUG 
  for(i=0;i<ARRAY_LEN;i++) {
    printf("%d ", array[i]); fflush(stdout); 
  }	
  printf("\n");fflush(stdout); 
  printf("generated\n"); fflush(stdout); 
#endif

  DAL_write((void*)PORT_OUT1, &(len), sizeof(int), p);
  DAL_write((void*)PORT_OUT2, array, sizeof(int)*len, p);

  free(array);
  p->local->index++; 
  
  if (p->local->index >= NUMBER_OF_ARRAYS) {
	return 1; 
  }
  
  return 0;
    
}

void arraygen_finish(DALProcess *p) {
}