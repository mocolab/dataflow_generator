#ifndef PRINTARRAY_H
#define PRINTARRAY_H

#include <dal.h>
#include "global.h"

#define PORT_IN1 1
#define PORT_IN2 2

#define EVENT_1 "stop_quicksort"

typedef struct _local_states {
    int index;
} Printarray_State;

void printarray_init(DALProcess *);
int printarray_fire(DALProcess *);
void printarray_finish(DALProcess *);

#endif
