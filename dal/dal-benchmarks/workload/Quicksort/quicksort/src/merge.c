#include <stdio.h>
#include <math.h>

#include "merge.h"

void merge_init(DALProcess *p) {
    p->local->index = 0;
}

int merge_fire(DALProcess *p) {

  int len=ARRAY_LEN;
  int up, down;
  int * array, *arr1, *arr2;
  int median;
  int cur_ind,i;

  /* receive the 'size' / 'array' */
  DAL_read((void*)PORT_IN1, &down, sizeof(int), p); 
  arr1 = (int *) malloc(down*sizeof(int));
  DAL_read((void*)PORT_IN2, arr1, sizeof(int)*down, p); 

  DAL_read((void*)PORT_IN3, &up, sizeof(int), p); 
  arr2 = (int *) malloc(up*sizeof(int));
  DAL_read((void*)PORT_IN4, arr2, sizeof(int)*up, p); 


  cur_ind=0;

  array = (int *) malloc(len*sizeof(int));
  for(i=0;i<down;i++)
    array[cur_ind++] = arr1[i];
  //for(i=0;i<(len-down-up);i++)
  //  in[cur_ind++] = median;
  for(i=0;i<up;i++)
    array[cur_ind++] = arr2[i];

  /* send the 'size' / 'sorted array' */
  DAL_write((void*)PORT_OUT1, &len, sizeof(int), p); 
  DAL_write((void*)PORT_OUT2, array, sizeof(int)*len, p); 

  free(array);
  free(arr1); 
  free(arr2); 

  p->local->index++; 
  
  if (p->local->index >= NUMBER_OF_ARRAYS) {
	return 1; 
  }
  
  return 0;
}

void merge_finish(DALProcess *p) {
}
