#ifndef ARRAYGEN_H
#define ARRAYGEN_H

#include <dal.h>
#include "global.h"

#define  PORT_OUT1 1
#define  PORT_OUT2 2


typedef struct _local_states {
    int index;
} Arraygen_State;

void arraygen_init(DALProcess *);
int arraygen_fire(DALProcess *);
void arraygen_finish(DALProcess *);

#endif
