#ifndef MERGE_H
#define MERGE_H

#include <dal.h>
#include "global.h"
#include "stdlib.h"

#define PORT_IN1 1
#define PORT_IN2 2
#define PORT_IN3 3
#define PORT_IN4 4
#define PORT_OUT1 5
#define PORT_OUT2 6

typedef struct _local_states {
    int index;
} Merge_State;

void merge_init(DALProcess *);
int merge_fire(DALProcess *);
void merge_finish(DALProcess *);

#endif
