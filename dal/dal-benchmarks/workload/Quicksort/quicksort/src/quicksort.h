#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <dal.h>
#include "global.h"
#include "stdlib.h"

#define PORT_IN1 1
#define PORT_IN2 2
#define PORT_OUT1 3
#define PORT_OUT2 4

typedef struct _local_states {
    int index;
    int * arr1;
    int * arr2;
} Quicksort_State;

void quicksort_init(DALProcess *);
int quicksort_fire(DALProcess *);
void quicksort_finish(DALProcess *);

#endif
