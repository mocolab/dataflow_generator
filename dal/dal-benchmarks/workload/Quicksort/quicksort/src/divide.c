#include <stdio.h>
#include <math.h>

#include "divide.h"

int smaller_arr_divide(int *dst, int *src, int pivot,int len)
{
  int i;
  int num=0;
  for(i=0;i<len;i++)
  {
    if(src[i]<pivot)
    {
      dst[num++]=src[i];
    }
  }
  return num;
}

int bigger_arr_divide(int *dst, int *src, int pivot,int len)
{
  int i;
  int num=0;
  for(i=0;i<len;i++)
  {
    if(src[i]>pivot)
    {
      dst[num++]=src[i];
    }
  }
  return num;
}

int median_const_divide(int len, int * array)
{
  int i,j;
  int desired_rank;

  if(len==1) desired_rank=1;
  else if(len==2) desired_rank=1;
  else if(len==3) desired_rank=2;
  else if(len==4) desired_rank=2;
  else if(len==5) desired_rank=3;
  else desired_rank=1;
 
#ifdef DEBUG 
//  printf("======\n");
//  printf("median_const - Input:");
//  for(i=0;i<len;i++)
//    printf("%d ",array[i]);
//  printf("\n");
//  printf("desired rank is %d\n",desired_rank);
#endif

  for(i=0;i<len;i++)
  {
    int rank = 1;
    for(j=0;j<len;j++)
    {
      /* FIXME: exception handling */
      if(i!=j && array[j]>array[i])
        rank++;
    }

    if(rank==desired_rank) {
#ifdef DEBUG
//      printf("median is %d\n",array[i]);
#endif
      return array[i];
    }
  }
#ifdef DEBUG
//      printf("median is %d\n",array[0]);
#endif
  return array[0];
}

int select_med_divide(int size, int * array)
{
  int ret;
  int * array_of_median;
  int len = ceil((float)size/(float)5);
  int i,j;

  if(size<3) return array[0];
#ifdef DEBUG
//  printf("======\n");
//  printf("select_med called for size %d, intermediate array %d gen.\n",size,len);
//  printf("Select_med: Input: ");
//  for(i=0;i<size;i++)
//    printf("%d ",array[i]);
//  printf("\n");
//  getchar();
#endif

  /* divide array into groups of five elements */
  array_of_median = (int *) malloc(sizeof(int)*len);

  for(i=0;i<len;i++)
  {
    int curr_len;

    if(size<5) {
      array_of_median[i] = median_const_divide(size,&(array[5*i]));
    } else if(i!=len-1){
      array_of_median[i] = median_const_divide(5,&(array[5*i]));
    } else if(i==(len-1) && len%5==0){
      array_of_median[i] = median_const_divide(5,&(array[5*i]));
    } else if(i==(len-1)) {
      if(size%5==0) array_of_median[i] = median_const_divide(5,&(array[5*i]));
      else array_of_median[i] = median_const_divide(5,&(array[5*i]));
    }
  }
  //getchar();

  /* call itself recursively */
  ret = select_med_divide(len, array_of_median);

  /* release */
  free(array_of_median);

  return ret;
}


void divide_init(DALProcess *p) {
    p->local->index = 0;
}

int divide_fire(DALProcess *p) {

  int len=ARRAY_LEN;
  int * array, *arr1, *arr2;
  int median;
  int down,up;

  /* receive the 'size' / 'array' */
  DAL_read((void*)PORT_IN1, &len, sizeof(int), p); 
  array = (int *) malloc(len*sizeof(int));
  DAL_read((void*)PORT_IN2, array, sizeof(int)*len, p); 

  /* select */
  median = select_med_divide(len, array);
 
  /* divide */
  down = 0; up = 0;
  arr1 = (int *) malloc(len*sizeof(int));
  arr2 = (int *) malloc(len*sizeof(int));
  down=smaller_arr_divide(arr1,array,median,len);
  up=bigger_arr_divide(arr2,array,median,len);

  /* send the 'size' / 'sorted array' */
  DAL_write((void*)PORT_OUT1, &down, sizeof(int), p); 
  DAL_write((void*)PORT_OUT2, arr1, sizeof(int)*down, p); 
  DAL_write((void*)PORT_OUT3, &up, sizeof(int), p); 
  DAL_write((void*)PORT_OUT4, arr2, sizeof(int)*up, p); 

  free(arr1);
  free(arr2);
  free(array);
  
  
  p->local->index++; 
  
  if (p->local->index >= NUMBER_OF_ARRAYS) {
	return 1; 
  }
  
  return 0;
}

void divide_finish(DALProcess *p) {
}