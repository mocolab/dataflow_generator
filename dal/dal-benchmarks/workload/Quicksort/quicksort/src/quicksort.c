#include <stdio.h>
#include <math.h>

#include "quicksort.h"

int smaller_arr(int *dst, int *src, int pivot,int len)
{
  int i;
  int num=0;
  for(i=0;i<len;i++)
  {
    if(src[i]<pivot)
    {
      dst[num++]=src[i];
    }
  }
  return num;
}

int bigger_arr(int *dst, int *src, int pivot,int len)
{
  int i;
  int num=0;
  for(i=0;i<len;i++)
  {
    if(src[i]>pivot)
    {
      dst[num++]=src[i];
    }
  }
  return num;
}

int median_const(int len, int * array)
{
  int i,j;
  int desired_rank;

  if(len==1) desired_rank=1;
  else if(len==2) desired_rank=1;
  else if(len==3) desired_rank=2;
  else if(len==4) desired_rank=2;
  else if(len==5) desired_rank=3;
  else desired_rank=1;
 
#ifdef DEBUG 
//  printf("======\n");
//  printf("median_const - Input:");
//  for(i=0;i<len;i++)
//    printf("%d ",array[i]);
//  printf("\n");
//  printf("desired rank is %d\n",desired_rank);
#endif

  for(i=0;i<len;i++)
  {
    int rank = 1;
    for(j=0;j<len;j++)
    {
      /* FIXME: exception handling */
      if(i!=j && array[j]>array[i])
        rank++;
    }

    if(rank==desired_rank) {
#ifdef DEBUG
//      printf("median is %d\n",array[i]);
#endif
      return array[i];
    }
  }
#ifdef DEBUG
//      printf("median is %d\n",array[0]);
#endif
  return array[0];
}

int select_med(int size, int * array)
{
  int ret;
  int * array_of_median;
  int len = ceil((float)size/(float)5);
  int i,j;

  if(size<3) return array[0];
#ifdef DEBUG
//  printf("======\n");
//  printf("select_med called for size %d, intermediate array %d gen.\n",size,len);
//  printf("Select_med: Input: ");
//  for(i=0;i<size;i++)
//    printf("%d ",array[i]);
//  printf("\n");
//  getchar();
#endif

  /* divide array into groups of five elements */
  array_of_median = (int *) malloc(sizeof(int)*len);

  for(i=0;i<len;i++)
  {
    int curr_len;

    if(size<5) {
      array_of_median[i] = median_const(size,&(array[5*i]));
    } else if(i!=len-1){
      array_of_median[i] = median_const(5,&(array[5*i]));
    } else if(i==(len-1) && len%5==0){
      array_of_median[i] = median_const(5,&(array[5*i]));
    } else if(i==(len-1)) {
      if(size%5==0) array_of_median[i] = median_const(5,&(array[5*i]));
      else array_of_median[i] = median_const(5,&(array[5*i]));
    }
  }
  //getchar();

  /* call itself recursively */
  ret = select_med(len, array_of_median);

  /* release */
  free(array_of_median);

  return ret;
}

void quicksort(int len, int * in)
{
  int median;
  int down, up;
  int i;
  int * arr1, * arr2;
  int cur_ind;

  if(len<2) return;

  #ifdef DEBUG
  printf("======\n");
  printf("Quicksort: Input (%d):\n",len);
  for(i=0;i<len;i++)
    printf("%d ",in[i]);
  printf("\n");
  #endif

  /* select */
  median = select_med(len, in);
  down = 0; up = 0;
  #ifdef DEBUG
//  printf("median is %d\n",median);
//  getchar();
  #endif

  /* divide */
  arr1 = (int *) malloc(len*sizeof(int));
  arr2 = (int *) malloc(len*sizeof(int));
  down=smaller_arr(arr1,in,median,len);
  up=bigger_arr(arr2,in,median,len);

  #ifdef DEBUG
//  printf("Arr1:\n");
//  for(i=0;i<down;i++)
//    printf("%d ",arr1[i]);
//  printf("\n");
//  printf("Arr2:\n");
//  for(i=0;i<up;i++)
//    printf("%d ",arr2[i]);
//  printf("\n");
//  getchar();
  #endif

  /* recursive call and merge */
  quicksort(down,arr1);
  quicksort(up,arr2);

  /* merge */
  cur_ind=0;
  for(i=0;i<down;i++)
    in[cur_ind++] = arr1[i];
  for(i=0;i<(len-down-up);i++)
    in[cur_ind++] = median;
  for(i=0;i<up;i++)
    in[cur_ind++] = arr2[i];

  #ifdef DEBUG
  printf("======\n");
  printf("Quicksort: Output:\n");
  for(i=0;i<len;i++)
    printf("%d ",in[i]);
  printf("\n");
  //getchar();
  printf("before freeing \n");
  #endif

  free(arr1);
  free(arr2);
  return ;
}



void quicksort_init(DALProcess *p) {
    p->local->index = 0;
}

int quicksort_fire(DALProcess *p) {

  int len=ARRAY_LEN;
  int * array;
  int median;
  int down,up;

  /* receive the 'size' / 'array' */
  DAL_read((void*)PORT_IN1, &len, sizeof(int), p); 
  array = (int *) malloc(len*sizeof(int));
  DAL_read((void*)PORT_IN2, array, sizeof(int)*len, p); 

  quicksort(len,array);

  /* send the 'size' / 'sorted array' */
  DAL_write((void*)PORT_OUT1, &len, sizeof(int), p); 
  DAL_write((void*)PORT_OUT2, array, sizeof(int)*len, p); 

  free(array);
  
  p->local->index++; 
  if (p->local->index >= NUMBER_OF_ARRAYS) {
    return 1;
  }
  
  return 0; 
}

void quicksort_finish(DALProcess *p) {
}