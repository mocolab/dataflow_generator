#include "VLC.h"

static inline int bs_size_ue( unsigned int val )
{
    static const int i_size0_254[255] =
    {
        1, 3, 3, 5, 5, 5, 5, 7, 7, 7, 7, 7, 7, 7, 7,
        9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
        11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
        11,11,11,11,11,11,11,11,11,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
        13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
        13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
        13,13,13,13,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
        15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
        15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
        15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
        15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
        15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15
    };

    if( val < 255 )
    {
        return i_size0_254[val];
    }
    else
    {
        int i_size = 0;

        val++;

        if( val >= 0x10000 )
        {
            i_size += 32;
            val = (val >> 16) - 1;
        }
        if( val >= 0x100 )
        {
            i_size += 16;
            val = (val >> 8) - 1;
        }
        return i_size0_254[val] + i_size;
    }
}

static inline int bs_size_te( int x, int val )
{
    if( x == 1 )
    {
        return 1;
    }
    else if( x > 1 )
    {
        return bs_size_ue( val );
    }
    return 0;
}

static inline void bs_init( bs_t *s, void *p_data, int i_data )
{
    s->p_start = (uint8_t*)p_data;
    s->p       = (uint8_t*)p_data;
    s->p_end   = s->p + i_data;
    s->i_left  = 8;
    memset(s->p_start,0,i_data);
}

static inline int bs_pos( bs_t *s )
{
    return( 8 * ( s->p - s->p_start ) + 8 - s->i_left );
}

static inline int bs_eof( bs_t *s )
{
    return( s->p >= s->p_end ? 1: 0 );
}

static inline uint32_t bs_read( bs_t *s, int i_count )
{
    static uint32_t i_mask[33] ={0x00,
        0x01,      0x03,      0x07,      0x0f,
        0x1f,      0x3f,      0x7f,      0xff,
        0x1ff,     0x3ff,     0x7ff,     0xfff,
        0x1fff,    0x3fff,    0x7fff,    0xffff,
        0x1ffff,   0x3ffff,   0x7ffff,   0xfffff,
        0x1fffff,  0x3fffff,  0x7fffff,  0xffffff,
        0x1ffffff, 0x3ffffff, 0x7ffffff, 0xfffffff,
        0x1fffffff,0x3fffffff,0x7fffffff,0xffffffff};
    int      i_shr;
    uint32_t i_result = 0;

    while( i_count > 0 )
    {
        if( s->p >= s->p_end )
        {
            break;
        }

        if( ( i_shr = s->i_left - i_count ) >= 0 )
        {
            /* more in the buffer than requested */
            i_result |= ( *s->p >> i_shr )&i_mask[i_count];
            s->i_left -= i_count;
            if( s->i_left == 0 )
            {
                s->p++;
                s->i_left = 8;
            }
            return( i_result );
        }
        else
        {
            /* less in the buffer than requested */
            i_result |= (*s->p&i_mask[s->i_left]) << -i_shr;
            i_count  -= s->i_left;
            s->p++;
            s->i_left = 8;
        }
    }

    return( i_result );
}

static inline uint32_t bs_read1( bs_t *s )
{

    if( s->p < s->p_end )
    {
        unsigned int i_result;

        s->i_left--;
        i_result = ( *s->p >> s->i_left )&0x01;
        if( s->i_left == 0 )
        {
            s->p++;
            s->i_left = 8;
        }
        return i_result;
    }

    return 0;
}

static inline uint32_t bs_show( bs_t *s, int i_count )
{
    if( s->p < s->p_end && i_count > 0 )
    {
        uint32_t i_cache = ((s->p[0] << 24)+(s->p[1] << 16)+(s->p[2] << 8)+s->p[3]) << (8-s->i_left);
        return( i_cache >> ( 32 - i_count) );
    }
    return 0;
}

/* TODO optimize */
static inline void bs_skip( bs_t *s, int i_count )
{
    s->i_left -= i_count;

    while( s->i_left <= 0 )
    {
        s->p++;
        s->i_left += 8;
    }
}

static inline int bs_read_ue( bs_t *s )
{
    int i = 0;

    while( bs_read1( s ) == 0 && s->p < s->p_end && i < 32 )
    {
        i++;
    }
    return( ( 1 << i) - 1 + bs_read( s, i ) );
}

static inline int bs_read_se( bs_t *s )
{
    int val = bs_read_ue( s );

    return val&0x01 ? (val+1)/2 : -(val/2);
}

static inline int bs_read_te( bs_t *s, int x )
{
    if( x == 1 )
    {
        return 1 - bs_read1( s );
    }
    else if( x > 1 )
    {
        return bs_read_ue( s );
    }
    return 0;
}

static inline void bs_write( bs_t *s, int i_count, uint32_t i_bits )
{
    if( s->p >= s->p_end - 4 )
        return;
    while( i_count > 0 )
    {
        if( i_count < 32 )
            i_bits &= (1<<i_count)-1;
        if( i_count < s->i_left )
        {
            *s->p = (*s->p << i_count) | i_bits;
            s->i_left -= i_count;
            break;
        }
        else
        {
            *s->p = (*s->p << s->i_left) | (i_bits >> (i_count - s->i_left));
            i_count -= s->i_left;
            s->p++;
            s->i_left = 8;
        }
    }
}

static inline void bs_write1( bs_t *s, uint32_t i_bit )
{
    if( s->p < s->p_end )
    {
        *s->p <<= 1;
        *s->p |= i_bit;
        s->i_left--;
        if( s->i_left == 0 )
        {
            s->p++;
            s->i_left = 8;
        }
    }
}

static inline void bs_align_0( bs_t *s )
{
    if( s->i_left != 8 )
    {
        *s->p <<= s->i_left;
        s->i_left = 8;
        s->p++;
    }
}

static inline void bs_align_1( bs_t *s )
{
    if( s->i_left != 8 )
    {
        *s->p <<= s->i_left;
        *s->p |= (1 << s->i_left) - 1;
        s->i_left = 8;
        s->p++;
    }
}

static inline void bs_align( bs_t *s )
{
    bs_align_0( s );
}

/* golomb functions */
static inline void bs_write_ue( bs_t *s, unsigned int val )
{
    int i_size = 0;
    static const int i_size0_255[256] =
    {
        1,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
        8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
        8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,
        8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8
    };

    if( val == 0 )
    {
        bs_write1( s, 1 );
    }
    else
    {
        unsigned int tmp = ++val;

        if( tmp >= 0x00010000 )
        {
            i_size += 16;
            tmp >>= 16;
        }
        if( tmp >= 0x100 )
        {
            i_size += 8;
            tmp >>= 8;
        }
        i_size += i_size0_255[tmp];

        bs_write( s, 2 * i_size - 1, val );
    }
}

static inline void bs_write_se( bs_t *s, int val )
{
    bs_write_ue( s, val <= 0 ? -val * 2 : val * 2 - 1);
}

static inline void bs_write_te( bs_t *s, int x, int val )
{
    if( x == 1 )
    {
        bs_write1( s, 1&~val );
    }
    else if( x > 1 )
    {
        bs_write_ue( s, val );
    }
}

static inline void bs_rbsp_trailing( bs_t *s )
{
    bs_write1( s, 1 );
    if( s->i_left != 8 )
    {
        bs_write( s, s->i_left, 0x00 );
    }
}

static inline int bs_size_se( int val )
{
    return bs_size_ue( val <= 0 ? -val * 2 : val * 2 - 1);
}

static inline int x264_median( int a, int b, int c )
{
    int min = a, max =a;
    if( b < min )
        min = b;
    else
        max = b;    /* no need to do 'b > max' (more consuming than always doing affectation) */

    if( c < min )
        min = c;
    else if( c > max )
        max = c;

    return a + b + c - min - max;
}

static void x264_mb_predict_mv(int16_t mv[2][48][2], int8_t ref[2][48], int i_list, int idx, int i_width, int mvp[2], int i_partition)
{
    const int i8 = x264_scan8[idx];
    const int i_ref= ref[i_list][i8];
    int     i_refa = ref[i_list][i8 - 1];
    int16_t *mv_a  = mv[i_list][i8 - 1];
    int     i_refb = ref[i_list][i8 - 8];
    int16_t *mv_b  = mv[i_list][i8 - 8];
    int     i_refc = ref[i_list][i8 - 8 + i_width ];
    int16_t *mv_c  = mv[i_list][i8 - 8 + i_width];
    int i_count;

    if( (idx&0x03) == 3 || ( i_width == 2 && (idx&0x3) == 2 )|| i_refc == -2 )
    {
        i_refc = ref[i_list][i8 - 8 - 1];
        mv_c   = mv[i_list][i8 - 8 - 1];
    }

    if( i_partition == D_16x8 )
    {
        if( idx == 0 && i_refb == i_ref )
        {
            mvp[0] = mv_b[0];
            mvp[1] = mv_b[1];
            return;
        }
        else if( idx != 0 && i_refa == i_ref )
        {
            mvp[0] = mv_a[0];
            mvp[1] = mv_a[1];
            return;
        }
    }
    else if( i_partition == D_8x16 )
    {
        if( idx == 0 && i_refa == i_ref )
        {
            mvp[0] = mv_a[0];
            mvp[1] = mv_a[1];
            return;
        }
        else if( idx != 0 && i_refc == i_ref )
        {
            mvp[0] = mv_c[0];
            mvp[1] = mv_c[1];
            return;
        }
    }

    i_count = 0;
    if( i_refa == i_ref ) i_count++;
    if( i_refb == i_ref ) i_count++;
    if( i_refc == i_ref ) i_count++;

    if( i_count > 1 )
    {
        mvp[0] = x264_median( mv_a[0], mv_b[0], mv_c[0] );
        mvp[1] = x264_median( mv_a[1], mv_b[1], mv_c[1] );
    }
    else if( i_count == 1 )
    {
        if( i_refa == i_ref )
        {
            mvp[0] = mv_a[0];
            mvp[1] = mv_a[1];
        }
        else if( i_refb == i_ref )
        {
            mvp[0] = mv_b[0];
            mvp[1] = mv_b[1];
        }
        else
        {                   mvp[0] = mv_c[0];
            mvp[1] = mv_c[1];

        }
    }
    else if( i_refb == -2 && i_refc == -2 && i_refa != -2 )
    {
        mvp[0] = mv_a[0];
        mvp[1] = mv_a[1];
    }
    else
    {
        mvp[0] = x264_median( mv_a[0], mv_b[0], mv_c[0] );
        mvp[1] = x264_median( mv_a[1], mv_b[1], mv_c[1] );
    }
}

static inline void bs_write_vlc( bs_t *s, vlc_t v )
{
    bs_write( s, v.i_size, v.i_bits );
}

static int x264_mb_predict_intra4x4_mode(int intra4x4_pred_mode[48], int idx )
{
    const int ma = intra4x4_pred_mode[x264_scan8[idx] - 1];
    const int mb = intra4x4_pred_mode[x264_scan8[idx] - 8];
    const int m  = X264_MIN( x264_mb_pred_mode4x4_fix[ma+1], x264_mb_pred_mode4x4_fix[mb+1] );

    if( m < 0 )
        return I_PRED_4x4_DC;

    return m;
}

static int x264_mb_predict_non_zero_code( int non_zero_count[48], int idx )
{
    const int za = non_zero_count[x264_scan8[idx] - 1];
    const int zb = non_zero_count[x264_scan8[idx] - 8];

    int i_ret = za + zb;

    if( i_ret < 0x80 )
    {
        i_ret = ( i_ret + 1 ) >> 1;
    }
    return i_ret & 0x7f;
}

static void block_residual_write_cavlc( int non_zero_count[48], bs_t *s, int i_idx, int *l, int i_count )
{
    int level[16], run[16];
    int i_total, i_trailing;
    int i_total_zero;
    int i_last;
    unsigned int i_sign;

    int i;
    int i_zero_left;
    int i_suffix_length;

    /* first find i_last */
    i_last = i_count - 1;
    while( i_last >= 0 && l[i_last] == 0 )
    {
        i_last--;
    }

    i_sign = 0;
    i_total = 0;
    i_trailing = 0;
    i_total_zero = 0;

    if( i_last >= 0 )
    {
        int b_trailing = 1;
        int idx = 0;

        /* level and run and total */
        while( i_last >= 0 )
        {
            level[idx] = l[i_last--];

            run[idx] = 0;
            while( i_last >= 0 && l[i_last] == 0 )
            {
                run[idx]++;
                i_last--;
            }

            i_total++;
            i_total_zero += run[idx];

            if( b_trailing && abs( level[idx] ) == 1 && i_trailing < 3 )
            {
                i_sign <<= 1;
                if( level[idx] < 0 )
                {
                    i_sign |= 0x01;
                }

                i_trailing++;
            }
            else
            {
                b_trailing = 0;
            }

            idx++;
        }
    }

    /* total/trailing */
    if( i_idx == BLOCK_INDEX_CHROMA_DC )
    {
        bs_write_vlc( s, x264_coeff_token[4][i_total*4+i_trailing] );
    }
    else
    {
        /* x264_mb_predict_non_zero_code return 0 <-> (16+16+1)>>1 = 16 */
        static const int ct_index[17] = {0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,3 };
        int nC;

        if( i_idx == BLOCK_INDEX_LUMA_DC )
        {
            nC = x264_mb_predict_non_zero_code( non_zero_count, 0 );
        }
        else
        {
            nC = x264_mb_predict_non_zero_code( non_zero_count, i_idx );
        }

        bs_write_vlc( s, x264_coeff_token[ct_index[nC]][i_total*4+i_trailing] );
    }

    if( i_total <= 0 )
    {
        return;
    }

    i_suffix_length = i_total > 10 && i_trailing < 3 ? 1 : 0;
    if( i_trailing > 0 )
    {
        bs_write( s, i_trailing, i_sign );
    }
    for( i = i_trailing; i < i_total; i++ )
    {
        int i_level_code;

        /* calculate level code */
        if( level[i] < 0 )
        {
            i_level_code = -2*level[i] - 1;
        }
        else /* if( level[i] > 0 ) */
        {
            i_level_code = 2 * level[i] - 2;
        }
        if( i == i_trailing && i_trailing < 3 )
        {
            i_level_code -=2; /* as level[i] can't be 1 for the first one if i_trailing < 3 */
        }

        if( ( i_level_code >> i_suffix_length ) < 14 )
        {
            bs_write_vlc( s, x264_level_prefix[i_level_code >> i_suffix_length] );
            if( i_suffix_length > 0 )
            {
                bs_write( s, i_suffix_length, i_level_code );
            }
        }
        else if( i_suffix_length == 0 && i_level_code < 30 )
        {
            bs_write_vlc( s, x264_level_prefix[14] );
            bs_write( s, 4, i_level_code - 14 );
        }
        else if( i_suffix_length > 0 && ( i_level_code >> i_suffix_length ) == 14 )
        {
            bs_write_vlc( s, x264_level_prefix[14] );
            bs_write( s, i_suffix_length, i_level_code );
        }
        else
        {
            bs_write_vlc( s, x264_level_prefix[15] );
            i_level_code -= 15 << i_suffix_length;
            if( i_suffix_length == 0 )
            {
                i_level_code -= 15;
            }

            if( i_level_code >= ( 1 << 12 ) || i_level_code < 0 )
            {
                printf( "OVERFLOW levelcode=%d\n", i_level_code );
            }

            bs_write( s, 12, i_level_code );    /* check overflow ?? */
        }

        if( i_suffix_length == 0 )
        {
            i_suffix_length++;
        }
        if( abs( level[i] ) > ( 3 << ( i_suffix_length - 1 ) ) && i_suffix_length < 6 )
        {
            i_suffix_length++;
        }
    }

    if( i_total < i_count )
    {
        if( i_idx == BLOCK_INDEX_CHROMA_DC )
        {
            bs_write_vlc( s, x264_total_zeros_dc[i_total-1][i_total_zero] );
        }
        else
        {
            bs_write_vlc( s, x264_total_zeros[i_total-1][i_total_zero] );
        }
    }

    for( i = 0, i_zero_left = i_total_zero; i < i_total - 1; i++ )
    {
        int i_zl;

        if( i_zero_left <= 0 )
        {
            break;
        }

        i_zl = X264_MIN( i_zero_left - 1, 6 );

        bs_write_vlc( s, x264_run_before[i_zl][run[i]] );

        i_zero_left -= run[i];
    }
}

static void x264_sub_mb_mv_write_cavlc(int16_t mv[2][48][2], int8_t ref[2][48], int partition, int sub_partition[4], bs_t *s )
{
    int i;
    for( i = 0; i < 4; i++ )
    {
        int mvp[2];

        if( !x264_mb_partition_listX_table[0][ sub_partition[i] ] )
        {
            continue;
        }
        switch( sub_partition[i] )
        {
            case D_L0_8x8:
                x264_mb_predict_mv( mv, ref, 0, 4*i, 2, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i]][1] - mvp[1] );
                break;
            case D_L0_8x4:
                x264_mb_predict_mv( mv, ref, 0, 4*i+0, 2, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i]][1] - mvp[1] );

                x264_mb_predict_mv( mv, ref, 0, 4*i+2, 2, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i+2]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i+2]][1] - mvp[1] );
                break;
            case D_L0_4x8:
                x264_mb_predict_mv( mv, ref, 0, 4*i+0, 1, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i]][1] - mvp[1] );

                x264_mb_predict_mv( mv, ref, 0, 4*i+1, 1, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i+1]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i+1]][1] - mvp[1] );
                break;
            case D_L0_4x4:
                x264_mb_predict_mv( mv, ref, 0, 4*i+0, 1, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i]][1] - mvp[1] );

                x264_mb_predict_mv( mv, ref, 0, 4*i+1, 1, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i+1]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i+1]][1] - mvp[1] );

                x264_mb_predict_mv( mv, ref, 0, 4*i+2, 1, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i+2]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i+2]][1] - mvp[1] );

                x264_mb_predict_mv( mv, ref, 0, 4*i+3, 1, mvp, partition );
                bs_write_se( s, mv[0][x264_scan8[4*i+3]][0] - mvp[0] );
                bs_write_se( s, mv[0][x264_scan8[4*i+3]][1] - mvp[1] );
                break;
        }
    }
}

static void x264_macroblock_luma_write_cavlc( int i_cbp_luma, int non_zero_count[48], int dct_block[24][16], bs_t *s )
{
    int i8, i4, i;

    for( i8 = 0; i8 < 4; i8++ )
        if( i_cbp_luma & (1 << i8) )
            for( i4 = 0; i4 < 4; i4++ )
                block_residual_write_cavlc( non_zero_count, s, i4+i8*4, dct_block[i4+i8*4], 16 );
}   

static void x264_slice_header_write(SliceHeader *sh, bs_t *s)
{
    bs_write_ue( s, sh->i_first_mb );
    if (FRAME_TYPE_IDR == sh->i_type)
        bs_write_ue( s, 7 );   /* same type things */
    else
        bs_write_ue( s, 5 );   /* same type things */
    bs_write_ue( s, 0);
    bs_write( s, SPS_MAX_FRAME_NUM, sh->i_frame_num );

    if( sh->i_idr_pic_id >= 0 ) /* NAL IDR */
    {
        bs_write_ue( s, sh->i_idr_pic_id );
    }

    bs_write( s, SPS_MAX_POC_LSB, sh->i_poc_lsb );

    if( sh->i_type == FRAME_TYPE_P)
    {
        bs_write1( s, sh->b_num_ref_idx_override );
        if( sh->b_num_ref_idx_override )
        {
            bs_write_ue( s, 0 );
        }
    }

    /* ref pic list reordering */
    if(FRAME_TYPE_P == sh->i_type )
    {
        bs_write1( s, 0);
    }

    if( sh->i_idr_pic_id >= 0 )
    {
        bs_write1( s, 0 );  /* no output of prior pics flag */
        bs_write1( s, 0 );  /* long term reference flag */
    }
    else
    {
        bs_write1( s, 0 );  /* adaptive_ref_pic_marking_mode_flag */
    }

    bs_write_se( s, sh->i_qp_delta );      /* slice qp delta */

    {
        bs_write_ue( s, sh->i_disable_deblocking_filter_idc );
        if( sh->i_disable_deblocking_filter_idc != 1 )
        {
            bs_write_se( s, 0);//sh->i_alpha_c0_offset >> 1 );
            bs_write_se( s, 0);//sh->i_beta_offset >> 1 );
        }
    }
} 

static void x264_init_data(int i_mb_type, int i_pred4x4[48], int non_zero_count[48])
{
    /* load intra4x4 */
    i_pred4x4[x264_scan8[0] - 8] =
        i_pred4x4[x264_scan8[1] - 8] =
        i_pred4x4[x264_scan8[4] - 8] =
        i_pred4x4[x264_scan8[5] - 8] = -1;

    /* load non_zero_count */
    non_zero_count[x264_scan8[0] - 8] =
        non_zero_count[x264_scan8[1] - 8] =
        non_zero_count[x264_scan8[4] - 8] =
        non_zero_count[x264_scan8[5] - 8] =
        non_zero_count[x264_scan8[16+0] - 8] =
        non_zero_count[x264_scan8[16+1] - 8] =
        non_zero_count[x264_scan8[16+4+0] - 8] =
        non_zero_count[x264_scan8[16+4+1] - 8] = 0x80;

    i_pred4x4[x264_scan8[0 ] - 1] =
        i_pred4x4[x264_scan8[2 ] - 1] =
        i_pred4x4[x264_scan8[8 ] - 1] =
        i_pred4x4[x264_scan8[10] - 1] = -1;

    /* load non_zero_count */
    non_zero_count[x264_scan8[0 ] - 1] =
        non_zero_count[x264_scan8[2 ] - 1] =
        non_zero_count[x264_scan8[8 ] - 1] =
        non_zero_count[x264_scan8[10] - 1] =
        non_zero_count[x264_scan8[16+0] - 1] =
        non_zero_count[x264_scan8[16+2] - 1] =
        non_zero_count[x264_scan8[16+4+0] - 1] =
        non_zero_count[x264_scan8[16+4+2] - 1] = 0x80;
}

static int x264_nal_encode( void *p_data, int *pi_data, int b_annexeb, x264_nal_t *nal )
{
    uint8_t *dst = (uint8_t *)p_data;
    uint8_t *src = nal->p_payload;
    uint8_t *end = &nal->p_payload[nal->i_payload];

    int i_count = 0;

    /* FIXME this code doesn't check overflow */

    if( b_annexeb )
    {
        /* long nal start code (we always use long ones)*/
        *dst++ = 0x00;
        *dst++ = 0x00;
        *dst++ = 0x00;
        *dst++ = 0x01;
    }

    /* nal header */
    *dst++ = ( 0x00 << 7 ) | ( nal->i_ref_idc << 5 ) | nal->i_type;

    while( src < end )
    {
        if( i_count == 2 && *src <= 0x03 )
        {
            *dst++ = 0x03;
            i_count = 0;
        }
        if( *src == 0 )
        {
            i_count++;
        }
        else
        {
            i_count = 0;
        }
        *dst++ = *src++;
    }
    *pi_data = dst - (uint8_t*)p_data;

    return *pi_data;
}

static void x264_sei_version_write(FILE *fpWrite)
{
    bs_t bitStream;
    x264_nal_t nal;
    char szBuffer[256], szTemp[64], version[256];
    int i, length;
    // random ID number generated according to ISO-11578
    const uint8_t uuid[16] = {
        0xdc, 0x45, 0xe9, 0xbd, 0xe6, 0xd9, 0x48, 0xb7,
        0x96, 0x2c, 0xd8, 0x20, 0xd9, 0x23, 0xee, 0xef
    };

    bs_init(&bitStream, szBuffer, sizeof(szBuffer));

    nal.i_ref_idc = NAL_PRIORITY_DISPOSABLE;
    nal.i_type = NAL_SEI;
    nal.i_payload = 0;
    nal.p_payload = (uint8_t *)szBuffer;

    sprintf( version, " ref=%d", REFERENCE_FRAME_COUNT);
    sprintf( szTemp, " deblock=%d:%d:%d", 1, 0, 0); strcat(version, szTemp);
    sprintf( szTemp, " analyse=%#x:%#x", X264_ANALYSE_INTRA, X264_ANALYSE_INTER ); strcat(version, szTemp);
    sprintf( szTemp, " me=%s", "hex"); strcat(version, szTemp);
    sprintf( szTemp, " subme=%d", SUBPIXEL_ME_P ); strcat(version, szTemp);
    sprintf( szTemp, " mixed_ref=%d", 0 ); strcat(version, szTemp);
    sprintf( szTemp, " me_range=%d", MAX_ME_RANGE ); strcat(version, szTemp);
    sprintf( szTemp, " chroma_me=%d", 1 ); strcat(version, szTemp);
    sprintf( szTemp, " 8x8dct=%d", 0 ); strcat(version, szTemp);
    sprintf( szTemp, " cqm=%d", 0 ); strcat(version, szTemp);
    sprintf( szTemp, " chroma_qp_offset=%d", 0 ); strcat(version, szTemp);
    sprintf( szTemp, " slices=%d", 1 ); strcat(version, szTemp);
    sprintf( szTemp, " keyint=%d keyint_min=%d scenecut=%d", MAX_GOP_SIZE, MIN_GOP_SIZE, SCENE_CUT); strcat(version, szTemp);
    sprintf( szTemp, " pass=%d", 1 ); strcat(version, szTemp);
    sprintf( szTemp, " qp=%d", QP_CONSTANT ); strcat(version, szTemp);
    sprintf( szTemp, " ip_ratio=%.2f", IP_FACTOR ); strcat(version, szTemp);

    length = strlen(version)+1+16;

    bs_write( &bitStream, 8, 0x5 ); // payload_type = user_data_unregistered
    // payload_size
    for( i = 0; i <= length-255; i += 255 )
        bs_write( &bitStream, 8, 255 );
    bs_write( &bitStream, 8, length-i );

    for( i = 0; i < 16; i++ )
        bs_write( &bitStream, 8, uuid[i] );
    for( i = 0; i < length-16; i++ )
        bs_write( &bitStream, 8, version[i] );

    bs_rbsp_trailing( &bitStream );

    nal.i_payload = bs_pos(&bitStream)/8; //&szBuffer[bs_pos(&bitStream)/8] - nal.p_payload;
    i = DATA_MAX;
    length = x264_nal_encode(version, &i, 1, &nal);
    fwrite(version, 1, length, fpWrite);
}

static void x264_sps_write(FILE *fpWrite)
{
    bs_t bitStream;
    x264_nal_t nal;
    char szBuffer[32], szOut[32];
    int i, length;

    bs_init(&bitStream, szBuffer, sizeof(szBuffer));
    nal.i_ref_idc = NAL_PRIORITY_HIGHEST;
    nal.i_type = NAL_SPS;
    nal.i_payload = 0;
    nal.p_payload = (uint8_t *)szBuffer;

    bs_write( &bitStream, 8, 66); //PROFILE_BASELINE); //sps->i_profile_idc );
    bs_write( &bitStream, 1, 0);//sps->b_constraint_set0 );
    bs_write( &bitStream, 1, 0);//sps->b_constraint_set1 );
    bs_write( &bitStream, 1, 0);//sps->b_constraint_set2 );

    bs_write( &bitStream, 5, 0 );    /* reserved */
    bs_write( &bitStream, 8, LEVEL_IDC );

    bs_write_ue( &bitStream, 0 );

    bs_write_ue( &bitStream, SPS_MAX_FRAME_NUM - 4 );
    bs_write_ue( &bitStream, 0);
    bs_write_ue( &bitStream, SPS_MAX_POC_LSB - 4 );

    bs_write_ue( &bitStream, REFERENCE_FRAME_COUNT);//sps->i_num_ref_frames );
    bs_write( &bitStream, 1, 0); // sps->b_gaps_in_frame_num_value_allowed );
    bs_write_ue( &bitStream, SPS_MB_WIDTH - 1 );
    bs_write_ue( &bitStream, SPS_MB_HEIGHT - 1);
    bs_write( &bitStream, 1, 1);//sps->b_frame_mbs_only );

    bs_write( &bitStream, 1, 0);//sps->b_direct8x8_inference );

    bs_write( &bitStream, 1, SPS_B_CROP );
    if( SPS_B_CROP )
    {
        bs_write_ue( &bitStream, 0 ); //sps->crop.i_left   / 2
        bs_write_ue( &bitStream, SPS_CROP_RIGHT / 2 );
        bs_write_ue( &bitStream, 0);//sps->crop.i_top    / 2 );
        bs_write_ue( &bitStream, SPS_CROP_BOTTOM / 2 );
    }

    bs_write( &bitStream, 1, 1);//sps->b_vui );
    {
        bs_write1( &bitStream, 0);//sps->vui.b_aspect_ratio_info_present );
        bs_write1( &bitStream, 0);//sps->vui.b_overscan_info_present );
        bs_write1( &bitStream, 0);//sps->vui.b_signal_type_present );

        bs_write1( &bitStream, 0); //sps->vui.b_chroma_loc_info_present );
        bs_write1( &bitStream, 1); //sps->vui.b_timing_info_present );
        {
            bs_write( &bitStream, 32, FPS_DEN); //sps->vui.i_num_units_in_tick );
            bs_write( &bitStream, 32, FPS_NUM * 2); //sps->vui.i_time_scale );
            bs_write1( &bitStream, 1); //sps->vui.b_fixed_frame_rate );
        }

        bs_write1( &bitStream, 0 );      /* nal_hrd_parameters_present_flag */
        bs_write1( &bitStream, 0 );      /* vcl_hrd_parameters_present_flag */
        bs_write1( &bitStream, 0 );      /* pic_struct_present_flag */
        bs_write1( &bitStream, 1); //sps->vui.b_bitstream_restriction );
        {
            bs_write1( &bitStream, 1); //sps->vui.b_motion_vectors_over_pic_boundaries );
            bs_write_ue( &bitStream, 0); //sps->vui.i_max_bytes_per_pic_denom );
            bs_write_ue( &bitStream, 0); //sps->vui.i_max_bits_per_mb_denom );
            bs_write_ue( &bitStream, 11);//sps->vui.i_log2_max_mv_length_horizontal );
            bs_write_ue( &bitStream, 11);//sps->vui.i_log2_max_mv_length_vertical );
            bs_write_ue( &bitStream, 0); //sps->vui.i_num_reorder_frames );
            bs_write_ue( &bitStream, REFERENCE_FRAME_COUNT); //sps->vui.i_max_dec_frame_buffering );
        }
    }

    bs_rbsp_trailing( &bitStream );

    nal.i_payload = bs_pos(&bitStream)/8; //&szBuffer[bs_pos(&bitStream)/8] - nal.p_payload;
    i = DATA_MAX;
    length = x264_nal_encode(szOut, &i, 1, &nal);
    fwrite(szOut, 1, length, fpWrite);
}

static void x264_pps_write(FILE *fpWrite)
{
    bs_t bitStream;
    x264_nal_t nal;
    char szBuffer[8], szOut[8];
    int i, length;

    bs_init(&bitStream, szBuffer, sizeof(szBuffer));
    nal.i_ref_idc = NAL_PRIORITY_HIGHEST;
    nal.i_type = NAL_PPS;
    nal.i_payload = 0;
    nal.p_payload = (uint8_t *)szBuffer;

    bs_write_ue( &bitStream, 0 );
    bs_write_ue( &bitStream, 0); //PPS_SPS_ID );

    bs_write( &bitStream, 1, 0);//pps->b_cabac );
    bs_write( &bitStream, 1, 0);//PPS_PIC_ORDER );
    bs_write_ue( &bitStream, 0);//PPS_SLICE_GROUP - 1 );

    bs_write_ue( &bitStream, 0 );
    bs_write_ue( &bitStream, 0 );
    bs_write( &bitStream, 3, 0 );
    bs_write_se( &bitStream, 0);//PPS_PIC_INIT_QP - 26 );
    bs_write_se( &bitStream, 0);//PPS_PIC_INIT_QS - 26 );
    bs_write_se( &bitStream, 0); // i_chroma_qp_index_offset

    bs_write( &bitStream, 1, 1);//PPS_DEBLOCK_FLITER_CONTROL );
    bs_write( &bitStream, 1, 0);//PPS_CONSTRAINED_INTRA_PRED );
    bs_write( &bitStream, 1, 0);//PPS_REDUNDANT_PIC_CNT );

    bs_rbsp_trailing( &bitStream );

    nal.i_payload = bs_pos(&bitStream)/8; //&szBuffer[bs_pos(&bitStream)/8] - nal.p_payload;
    i = DATA_MAX;
    length = x264_nal_encode(szOut, &i, 1, &nal);
    fwrite(szOut, 1, length, fpWrite);
}

/*
create_MB_indexes:
Creates an array of coordinates of the indexes of MBs that must be accessed on every MB_phase,
according to the wavefront parallelism scheme.
*/
static void create_MB_indexes(coords* ptr)
{
    int i,x,y,step;

    for(i=0,step=1;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;step++)
        for (x=0;x<SPS_MB_WIDTH;x++){
            for (y=0;y<SPS_MB_HEIGHT;y++)
                if (x+2*y+1 == step){
                    ptr[i].x = x;
                    ptr[i].y = y;
                    i++;
                }
            }
}


//shift_bs:
//function that shifts the contents of bitstream bs by count number of bits
void shift_bs(bs_t* bs, int count)
{
    int i;
    int byte_size = bs->p - bs->p_start + 1;
    for (i=0;i<byte_size-2;i++){
        bs->p_start[i] <<= count;
        bs->p_start[i] |= bs->p_start[i+1] >> (8-count);
    }
    
    if (count > 8- bs->i_left){
        bs->p_start[byte_size-2] <<= 8-bs->i_left;
        bs->p_start[byte_size-2] |= bs->p_start[byte_size-1] & ((1<<(8-bs->i_left))-1);
    }
    else{
        bs->p_start[byte_size-2] <<= count;
        bs->p_start[byte_size-2] |= bs->p_start[byte_size-1] >> (8-bs->i_left-count);
        bs->p_start[byte_size-1] &= (1<< (8-bs->i_left-count)) - 1;
    }

    if (count > (8-bs->i_left))
        bs->p--;
}


//combine_bs:
//function that writes the contents of bitstream bs2 to the end of bitstream bs1
void combine_bs(bs_t* bs1, bs_t* bs2)
{
    int size1 = 8*(bs1->p - bs1->p_start) + 8 - bs1->i_left;
    int size2 = 8*(bs2->p - bs2->p_start) + 8 - bs2->i_left;
    int original_bs2_i_left = bs2->i_left;
    int modulo = 8 - bs1->i_left;

    if (modulo==0){ //if size1 is a multiplicand of 8, bs2 can be concatenated directly
        memcpy(bs1->p_start + size1/8, bs2->p_start, (size2%8 ==0)? size2/8 : size2/8 +1 );
        bs1->i_left = bs2->i_left;
        bs1->p += bs2->p - bs2->p_start;
    }
    else{ //to size1 den einai pollaplasio tou 8:
        bs1->p_start[size1/8] <<= bs1->i_left;
        bs1->p_start[size1/8] |= ((uint8_t)(bs2->p_start[0]) >> (size2<8?modulo-bs2->i_left:modulo));

        //shift changes bs2, careful...
        shift_bs(bs2, bs1->i_left);

        memcpy(bs1->p_start + (size1/8) + 1, bs2->p_start ,(size2%8 ==0)? size2/8 : size2/8 +1 );

        bs1->p += bs2->p - bs2->p_start + 1;
        if (bs1->i_left <= (8-original_bs2_i_left)){
            bs1->i_left = bs1->i_left + original_bs2_i_left;
        }
        else{
            bs1->i_left = bs1->i_left - (8-original_bs2_i_left);
        }
    }
}


/*********************
  DAL FUNCTIONS
**********************/

void VLC_init(DALProcess * p)
{
    p->local->fp264_1986 = NULL;
    p->local->o_msgBS_data_1985_phase = 0;
    p->local->fp264_1986 = fopen("FOREMAN.264", "wb");
    #if(WRITE_SEI_VERSION)
        x264_sei_version_write(p->local->fp264_1986);
    #endif
    p->local->output_1948 = 0;
    {int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1949[i] = 0;}
    {int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1950[i] = 0;}
    {int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1955[i] = 0;}
    {int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1956[i] = 0;}
    {int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1957[i] = 0;}
    {int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1958[i] = 0;}
    p->local->output_1966 = 0;

    p->local->sdfLoopCounter_31 = 0;

    create_MB_indexes(p->local->wvfp_MB_indexes);
}

int VLC_fire(DALProcess* p)
{
    int intFrameType;

    //read the signal from init
    int terminate;
    DAL_read((void*)PORT_IN_INIT, &(terminate), sizeof(int), p);
    if (terminate == (int) 1){
        DAL_send_event((void *)EVENT_DONE, p);
        return 1;
    }

    int mb_index = p->local->wvfp_MB_indexes[p->local->o_msgBS_data_1985_phase].x + (p->local->wvfp_MB_indexes[p->local->o_msgBS_data_1985_phase].y)*SPS_MB_WIDTH;

    EncVLCPacket e;
    DAL_read((void*)PORT_IN_ENCODER, &e, sizeof(EncVLCPacket), p);

    memcpy(&(p->local->vlc_r_info), &e.e_s_info, sizeof(encoder_send_info));
    memcpy((unsigned char *)&(p->local->output_1959[mb_index]),&(p->local->vlc_r_info.i_sub_partition), sizeof(struct IntArr4)); // i_sub_partition
    memcpy((unsigned char *)&(p->local->output_1958[mb_index]),&(p->local->vlc_r_info.i_partition) , sizeof(int)); // i_partition
    memcpy((unsigned char *)&(p->local->output_1957[mb_index]),&(p->local->vlc_r_info.chroma_pred_mode), sizeof(int)); // chroma_pred_mode
    memcpy((unsigned char *)&(p->local->output_1953[mb_index]),&(p->local->vlc_r_info.mv_mb), sizeof(struct mv_mb)); // mv_mb data
    memcpy((unsigned char *)&(p->local->output_1952[mb_index]),&(p->local->vlc_r_info.ref_mb), sizeof(struct ref_mb)); // ref_mb
    memcpy((unsigned char *)&(p->local->output_1951[mb_index]),&(p->local->vlc_r_info.intra4x4_pred_mode), sizeof(struct IntArr48)); // intra4x4_pred_mdoe
    memcpy((unsigned char *)&(p->local->output_1950[mb_index]),&(p->local->vlc_r_info.i_pred16x16), sizeof(int));  // i_pred16x16
    memcpy((unsigned char *)&(p->local->output_1949[mb_index]),&(p->local->vlc_r_info.mb_type), sizeof(int)); // mb_type
    // from Enc
    memcpy(&(p->local->output_1962[mb_index]), &e.chroma_dc, sizeof(struct IntArr2x4));
    memcpy(&(p->local->output_1964[mb_index]), &e.chroma_residual_ac, sizeof(struct IntArr8x15));
    memcpy(&(p->local->output_1961[mb_index]), &e.luma4x4_out, sizeof(struct IntBlock16x16));
    memcpy(&(p->local->output_1963[mb_index]), &e.luma_residual_ac, sizeof(struct IntArr16x15));
    memcpy(&(p->local->output_1954[mb_index]), &e.non_zero_count, sizeof(struct IntArr48));
    memcpy(&(p->local->output_1960[mb_index]), &e.luma4x4, sizeof(struct IntArr16));
    p->local->output_1955[mb_index] = e.i_cbp_luma;
    p->local->output_1956[mb_index] = e.i_cbp_chroma;
    intFrameType = e.intFrameType;


    if (mb_index==0){
        memcpy(&(p->local->output_1965), &e.msgSliceHeader, sizeof(SliceHeader));
        p->local->output_1948 = e.intFrameType;
    }

    const int i_mb_type = p->local->output_1949[mb_index];
    int i_mb_i_offset;
    int i;

    switch( p->local->output_1948 ){
        case FRAME_TYPE_IDR:
            i_mb_i_offset = 0;
            break;
        case FRAME_TYPE_P:
            i_mb_i_offset = 5;
            break;
    }

    if (0 == p->local->o_msgBS_data_1985_phase){
        bs_init(&(p->local->bitstream), p->local->o_msgBS_data_1947.data, sizeof(p->local->o_msgBS_data_1947.data));            
        bs_init(&(p->local->temp_bs[0]), p->local->temp_mb_data[0].data, sizeof(p->local->temp_mb_data[0].data));
        x264_slice_header_write( &(p->local->output_1965), &(p->local->temp_bs[0]) );
        p->local->i_skip = 0;
        x264_init_data(i_mb_type, p->local->output_1951[mb_index].data, p->local->output_1954[mb_index].data);
        int i;
        for (i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++)
            p->local->bs_available[i] = BS_MISSING ;
    }
    if (mb_index!=0)
        bs_init(&(p->local->temp_bs[mb_index]), p->local->temp_mb_data[mb_index].data, sizeof(p->local->temp_mb_data[mb_index].data));

    /* Write:
       - type
       - prediction
       - mv */
    if (i_mb_type == P_SKIP){
        p->local->i_skip++;
        goto skip;
    }

    if (p->local->output_1948 != FRAME_TYPE_IDR){
        bs_write_ue( &(p->local->temp_bs[mb_index]), p->local->i_skip );  /* skip run */
        p->local->i_skip = 0;
    }

    if( i_mb_type == I_4x4 || i_mb_type == I_8x8 ){
        int di = i_mb_type == I_8x8 ? 4 : 1;
        bs_write_ue( &(p->local->temp_bs[mb_index]), i_mb_i_offset + 0 );

        /* Prediction: Luma */
        for( i = 0; i < 16; i += di ){
            int i_pred = x264_mb_predict_intra4x4_mode( p->local->output_1951[mb_index].data, i );
            int i_mode = x264_mb_pred_mode4x4_fix[p->local->output_1951[mb_index].data[x264_scan8[i]]+1];


            if( i_pred == i_mode){
                bs_write1( &(p->local->temp_bs[mb_index]), 1 );  /* b_prev_intra4x4_pred_mode */
            }
            else{
                bs_write1( &(p->local->temp_bs[mb_index]), 0 );  /* b_prev_intra4x4_pred_mode */
                if( i_mode < i_pred )
                    bs_write( &(p->local->temp_bs[mb_index]), 3, i_mode );
                else
                    bs_write( &(p->local->temp_bs[mb_index]), 3, i_mode - 1 );
            }
        }
        bs_write_ue( &(p->local->temp_bs[mb_index]), x264_mb_pred_mode8x8c_fix[ p->local->output_1957[mb_index] ] );
    }
    else if( i_mb_type == I_16x16 ){
        bs_write_ue( &(p->local->temp_bs[mb_index]), i_mb_i_offset + 1 + x264_mb_pred_mode16x16_fix[p->local->output_1950[mb_index]] +
                p->local->output_1956[mb_index] * 4 + ( p->local->output_1955[mb_index] == 0 ? 0 : 12 ) );
        bs_write_ue( &(p->local->temp_bs[mb_index]), x264_mb_pred_mode8x8c_fix[ p->local->output_1957[mb_index] ] );
    }
    else if( i_mb_type == P_L0 ){
        int mvp[2];

        if( p->local->output_1958[mb_index] == D_16x16 ){
            bs_write_ue( &(p->local->temp_bs[mb_index]), 0 );

            x264_mb_predict_mv( p->local->output_1953[mb_index].data, p->local->output_1952[mb_index].data, 0, 0, 4, mvp, p->local->output_1958[mb_index] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[0]][0] - mvp[0] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[0]][1] - mvp[1] );
        }
        else if( p->local->output_1958[mb_index] == D_16x8 ){
            bs_write_ue( &(p->local->temp_bs[mb_index]), 1 );

            x264_mb_predict_mv( p->local->output_1953[mb_index].data, p->local->output_1952[mb_index].data, 0, 0, 4, mvp, p->local->output_1958[mb_index] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[0]][0] - mvp[0] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[0]][1] - mvp[1] );

            x264_mb_predict_mv( p->local->output_1953[mb_index].data, p->local->output_1952[mb_index].data, 0, 8, 4, mvp, p->local->output_1958[mb_index] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[8]][0] - mvp[0] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[8]][1] - mvp[1] );
        }
        else if( p->local->output_1958[mb_index] == D_8x16 ){
            bs_write_ue( &(p->local->temp_bs[mb_index]), 2 );

            x264_mb_predict_mv( p->local->output_1953[mb_index].data, p->local->output_1952[mb_index].data, 0, 0, 2, mvp, p->local->output_1958[mb_index]);
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[0]][0] - mvp[0] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[0]][1] - mvp[1] );

            x264_mb_predict_mv( p->local->output_1953[mb_index].data, p->local->output_1952[mb_index].data, 0, 4, 2, mvp, p->local->output_1958[mb_index] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[4]][0] - mvp[0] );
            bs_write_se( &(p->local->temp_bs[mb_index]), p->local->output_1953[mb_index].data[0][x264_scan8[4]][1] - mvp[1] );
        }
    }
    else if( i_mb_type == P_8x8 ){
        int b_sub_ref0;
        if( p->local->output_1952[mb_index].data[0][x264_scan8[0]] == 0 && p->local->output_1952[mb_index].data[0][x264_scan8[4]] == 0 &&
                p->local->output_1952[mb_index].data[0][x264_scan8[8]] == 0 && p->local->output_1952[mb_index].data[0][x264_scan8[12]] == 0 ){
            bs_write_ue( &(p->local->temp_bs[mb_index]), 4 );
            b_sub_ref0 = 0;
        }
        else{
            bs_write_ue( &(p->local->temp_bs[mb_index]), 3 );
            b_sub_ref0 = 1;
        }

        /* sub mb type */
        for( i = 0; i < 4; i++ )
            bs_write_ue( &(p->local->temp_bs[mb_index]), sub_mb_type_p_to_golomb[ p->local->output_1959[mb_index].data[i] ] );

        x264_sub_mb_mv_write_cavlc( p->local->output_1953[mb_index].data, p->local->output_1952[mb_index].data, p->local->output_1958[mb_index], p->local->output_1959[mb_index].data, &(p->local->temp_bs[mb_index]) );
    }
    else{
        /*
           printf( "invalid/unhandled mb_type\n" );
           return;
         */
    }

    /* Coded block patern */
    if( i_mb_type == I_4x4 || i_mb_type == I_8x8 )
        bs_write_ue( &(p->local->temp_bs[mb_index]), intra4x4_cbp_to_golomb[( p->local->output_1956[mb_index] << 4 )| p->local->output_1955[mb_index]] );
    else if( i_mb_type != I_16x16 )
        bs_write_ue( &(p->local->temp_bs[mb_index]), inter_cbp_to_golomb[( p->local->output_1956[mb_index] << 4 )| p->local->output_1955[mb_index]] );

    /* write residual */
    if( i_mb_type == I_16x16 ){
        bs_write_se( &(p->local->temp_bs[mb_index]), 0); // h->mb.i_qp - h->mb.i_last_qp );

        /* DC Luma */
        block_residual_write_cavlc( p->local->output_1954[mb_index].data, &(p->local->temp_bs[mb_index]), BLOCK_INDEX_LUMA_DC ,p->local->output_1960[mb_index].data, 16 );

        /* AC Luma */
        if( p->local->output_1955[mb_index] != 0 ){
            for( i = 0; i < 16; i++ )
                block_residual_write_cavlc( p->local->output_1954[mb_index].data, &(p->local->temp_bs[mb_index]), i, p->local->output_1963[mb_index].data[i], 15 );
        }
    }
    else if( p->local->output_1955[mb_index] != 0 || p->local->output_1956[mb_index] != 0 ){
        int i8, i4;

        bs_write_se( &(p->local->temp_bs[mb_index]), 0); // h->mb.i_qp - h->mb.i_last_qp );
        for (i8=0; i8 < 4; i8++)
            if (p->local->output_1955[mb_index] & (1<<i8)){
                for (i4=0; i4 < 4; i4++)
                    block_residual_write_cavlc( p->local->output_1954[mb_index].data, &(p->local->temp_bs[mb_index]), i4+i8*4, p->local->output_1961[mb_index].data[i4+i8*4], 16 );
            }
    }

    if( p->local->output_1956[mb_index] != 0 ){
        /* Chroma DC residual present */
        block_residual_write_cavlc( p->local->output_1954[mb_index].data, &(p->local->temp_bs[mb_index]), BLOCK_INDEX_CHROMA_DC, p->local->output_1962[mb_index].data[0], 4 );
        block_residual_write_cavlc( p->local->output_1954[mb_index].data, &(p->local->temp_bs[mb_index]), BLOCK_INDEX_CHROMA_DC, p->local->output_1962[mb_index].data[1], 4 );
        if( p->local->output_1956[mb_index]&0x02 ){ /* Chroma AC residual present */                  
            for( i = 0; i < 8; i++ )
                block_residual_write_cavlc( p->local->output_1954[mb_index].data, &(p->local->temp_bs[mb_index]), 16 + i, p->local->output_1964[mb_index].data[i], 15 );
        }
    }
skip :
    p->local->bs_available[mb_index] = BS_READY;
    if (mb_index == 0){
        combine_bs(&(p->local->bitstream), &(p->local->temp_bs[0]));
        p->local->bs_available[0] = BS_WRITTEN;
    }
    else if(p->local->bs_available[mb_index-1]==BS_WRITTEN){
        int next = mb_index;
        while(p->local->bs_available[next] == BS_READY){
            combine_bs(&(p->local->bitstream), &(p->local->temp_bs[next]));
            p->local->bs_available[next] = BS_WRITTEN;

            if (next!=SPS_MB_WIDTH*SPS_MB_HEIGHT-1)
                next++;
            else
                break;
        }
    }

    if (SPS_MB_WIDTH*SPS_MB_HEIGHT-1 == p->local->o_msgBS_data_1985_phase){
        if( p->local->i_skip > 0 ){
            bs_write_ue( &(p->local->bitstream), p->local->i_skip );  /* last skip run */
        }
        bs_rbsp_trailing( &(p->local->bitstream) );
        p->local->o_msgBS_data_1947.size = bs_pos(&(p->local->bitstream))/8;
    }
    p->local->o_msgBS_data_1985_phase = (p->local->o_msgBS_data_1985_phase+1)%(SPS_MB_WIDTH*SPS_MB_HEIGHT);

    p->local->sdfLoopCounter_31++;
    if(p->local->sdfLoopCounter_31 == SPS_MB_WIDTH*SPS_MB_HEIGHT){
        p->local->output_1966 = intFrameType;

        uint8_t szBuffer[FRAME_WIDTH*FRAME_HEIGHT];
        x264_nal_t nal;
        int i, length;

        if (p->local->output_1966 == FRAME_TYPE_IDR){
            x264_sps_write(p->local->fp264_1986);
            x264_pps_write(p->local->fp264_1986);
        }

        if (p->local->output_1966 == FRAME_TYPE_IDR){
            nal.i_ref_idc = NAL_PRIORITY_HIGHEST;
            nal.i_type = NAL_SLICE_IDR;
        }
        else{
            nal.i_ref_idc = NAL_PRIORITY_HIGH;
            nal.i_type = NAL_SLICE;
        }
        nal.i_payload = p->local->o_msgBS_data_1947.size;
        nal.p_payload = p->local->o_msgBS_data_1947.data;

        i = DATA_MAX;
        length = x264_nal_encode( szBuffer, &i, 1, &nal );
        fwrite(szBuffer, 1, length, p->local->fp264_1986);

        p->local->sdfLoopCounter_31 = 0;
    }

    return 0;
}

void VLC_finish(DALProcess *p)
{
	fclose(p->local->fp264_1986);
}
