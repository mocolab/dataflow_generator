#ifndef ENCODER_H
#define ENCODER_H

#define PORT_IN_ME "fromME"
#define PORT_OUT_VLC "toVLC"
#define PORT_OUT_DEBLOCK "toDeblock"

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <dal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "parameters.h"
#include "x264.h"
#include "structs.h"


/*********************
  STRUCT DECLARATIONS
**********************/

typedef struct
{
    int i_type;
    int i_first_mb;
    int i_last_mb;		
    int i_frame_num;		
    int i_idr_pic_id;
    int i_poc_lsb;		
    int b_num_ref_idx_override;		
    int i_qp;
    int i_qp_delta;		
    int i_disable_deblocking_filter_idc;		
} SliceHeader;

typedef struct{
    int i_cbp_luma;
    int i_cbp_chroma;
    struct IntBlock16x16 luma4x4_out;
    struct IntArr16x15 luma_residual_ac;
    struct IntArr48 non_zero_count;
    struct IntArr8x15 chroma_residual_ac;
    struct IntArr2x4 chroma_dc;
    struct IntArr16 luma4x4;
    SliceHeader msgSliceHeader;
    int intFrameType;
    encoder_send_info e_s_info;
} EncVLCPacket;


typedef struct{
    int intFrameType;
    int MB_index_Enc;
    struct Uint8_tArr64 recon_block_U;
    struct Uint8_tArr64 recon_block_V;
    struct Uint8_tArr256 recon_block_1313;
    struct Uint8_tArr256 recon_block_1382;
    struct IntArr48 non_zero_count;
    deblock_send_info d_s_info;
} EncDbkPacket;


typedef struct {
   int output[SPS_MB_WIDTH*SPS_MB_HEIGHT];
   int o_intFrameType;
   SliceHeader o_msgSliceHeader;
   encoder_send_info e_s_info;
   deblock_send_info d_s_info;
   init_info i_info;
} MEEncPacket;

typedef struct _coords{
	int x;
	int y;
} coords;

/****************************
  LOCAL STATE OF THE PROCESS
*****************************/

typedef struct _local_states {
	struct Uint8_tArr256 best_block_1299;
	struct Uint8_tArr16 src_block_4x4_1300[16];
	struct Uint8_tArr16 best_block_4x4_1301[16*SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int src_block_1442;
	struct Int16_tBlock4x4 dct4x4_1302[16];
	int dct4x4_1444;
	struct Int16_tBlock4x4 dc_block_4x4_1303;
	int do_it_1446;
	int frame_type_1447;
	int frame_type_1447_phase;
	int dct4x4_in_1448;
	struct Int16_tBlock4x4 dct4x4_out_1306[16];
	int dct4x4_out_1449;
	int dct4x4_in_1452;
	struct Int16_tBlock4x4 dct4x4_out_1307;
	int do_it_1453;
	struct Int16_tBlock4x4 dst_block_1308;
	struct Int16_tBlock4x4 dct4x4_out_1309;
	int frame_type_1457;
	int frame_type_1457_phase;
	int dct4x4_in_1458;
	struct Int16_tBlock4x4 dct4x4_out_1310;
	int dc_block_4x4_1462_phase;
	struct Int16_tBlock4x4 dst_block_4x4_1311;
	struct Uint8_tArr16 dst_block_1312[16];
	int dst_block_1465;
	struct Uint8_tArr256 recon_block_1313;
	struct IntArr16 luma4x4_1314;
	int dct4x4_1469;
	struct IntArr15 residual_ac_1319[16*SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int output_1320;
	int output_1326[16*SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int output_1346[16];
	struct Int16_tBlock4x4 dst_block_1349[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct Uint8_tArr256 recon_block_1350;
	struct Uint8_tArr64 recon_block_U_1351[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct Uint8_tArr64 recon_block_V_1352[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct Uint8_tArr16 src_block_4x4_1353[16];
	struct Uint8_tArr16 best_block_4x4_1354[16];
	int do_it_1477;
	int src_block_1478;
	int best_block_1479;
	struct Int16_tBlock4x4 dct4x4_1355;
	int output_1358[16];
	int do_it_1480;
	int frame_type_1481;
	int frame_type_1481_phase;
	struct Int16_tBlock4x4 dct4x4_out_1362[16];
	int dct4x4_out_1482;
	int do_it_1483;
	int dct4x4_1484;
	struct IntArr16 luma4x4_1365[16];
	int luma4x4_1485;
	int output_1369[16];
	int do_it_1486;
	int luma4x4_1487;
	int i_decimate_1374[16];
	int i_decimate_1488;
	struct IntArr16 luma4x4_out_1375[16*SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int do_it_out_1376[16];
	int do_it_1490;
	int frame_type_1491;
	int frame_type_1491_phase;
	int dct4x4_in_1492;
	struct Int16_tBlock4x4 dct4x4_out_1379;
	int do_it_1493;
	int best_block_1494;
	struct Uint8_tArr16 dst_block_1380;
	int do_it_1495;
	int src_block_predct_1496;
	struct Uint8_tArr16 src_block_1381[16];
	int src_block_1497;
	struct Uint8_tArr256 recon_block_1382;
	int output_1383;
	int output_1384;
	struct IntArr8x15 chroma_residual_ac_1387;
	struct IntArr2x4 chroma_dc_1388;
	struct Uint8_tArr64 recon_block_U_1389;
	struct Uint8_tArr64 recon_block_V_1390;
	int i_cbp_luma_1391;
	int i_cbp_chroma_1392;
	struct IntArr48 non_zero_count_1393;
	struct IntBlock16x16 luma4x4_out_1394;
	struct IntArr16x15 luma_residual_ac_1395;
	int output_1404[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int output_1410;
	int output_1519_phase;
	int output_1411;
	int output_1521_phase;
	int output_1412;
	int output_1413;
	int output_1421;
	int output_1424;
	int output_1426[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int output_1427;
	int output_1541_phase;
	int output_1428[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct Uint8_tArr64 output_1429[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct Uint8_tArr64 output_1430[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct preddata8 output_1431[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct preddata8 output_1432[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int output_1433;
	int output_1555_phase;
	int output_1434[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct IntArr16 output_1435[16*SPS_MB_WIDTH*SPS_MB_HEIGHT];
	struct IntArr48 output_1437[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	encoder_send_info e_r_info; 
	init_info i_to_e_info;

	EncVLCPacket e;
	EncDbkPacket d;
	MEEncPacket m;

	int m_output[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int sdfLoopCounter_16;
	SliceHeader m_o_msgSliceHeader;
	int m_o_intFrameType;

    coords wvfp_MB_indexes[SPS_MB_WIDTH*SPS_MB_HEIGHT];

	#if(MEASURE_TIME)
		int counter;
	  struct rusage start, end;
	  long long myutime;
	  long long mystime;
	#endif

    #if (RECORD_TIMING)
        FILE * fp_timing;
    #endif
} encoder_State;

void encoder_init(DALProcess *);
int encoder_fire(DALProcess *);
void encoder_finish(DALProcess *);

#endif
