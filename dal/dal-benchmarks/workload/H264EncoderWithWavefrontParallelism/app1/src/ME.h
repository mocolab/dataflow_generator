#ifndef ME_H
#define ME_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <dal.h>

#include "parameters.h"
#include "x264.h"
#include "structs.h"


#define PORT_IN_INIT "fromInit"
#define PORT_IN_DEBLOCK "fromDeblock"
#define PORT_OUT_ENCODER "toEncoder"


/*********************
  STRUCT DECLARATIONS
**********************/

typedef struct
{
    int i_type;
    int i_first_mb;
    int i_last_mb;		
    int i_frame_num;		
    int i_idr_pic_id;
    int i_poc_lsb;		
    int b_num_ref_idx_override;		
    int i_qp;
    int i_qp_delta;		
    int i_disable_deblocking_filter_idc;		
} SliceHeader;

typedef struct{
    int output[SPS_MB_WIDTH*SPS_MB_HEIGHT];
    int o_intFrameType;
    SliceHeader o_msgSliceHeader;
    init_info i_s_info;
} InitMEPacket;

typedef struct {
   int output[SPS_MB_WIDTH*SPS_MB_HEIGHT];
   int o_intFrameType;
   SliceHeader o_msgSliceHeader;
   encoder_send_info e_s_info;
   deblock_send_info d_s_info;
   init_info i_info;
} MEEncPacket;


/****************************
  LOCAL STATE OF THE PROCESS
*****************************/

typedef struct _local_states {
	int ME_index;
	struct IntArr4 predict_mode_703;
	int i_max_704;
	int i_predict16x16_705;
	int i_sad_i16x16_706;
	int do_it_1030;
	struct IntArr9 predict_mode_707;
	int i_max_708;
	int do_it_1031;
	int i_predict4x4_709;
	int i_sad_i4x4_710;
	struct Uint8_tArr16 best_4x4_block_711;
	int do_it_1032;
	int src_block_1034_phase;
	int recon_block_4_1036;
	struct Uint8_tArr16 src_block_4_712;
	struct preddata4 preddata4_713;
	int i_neighbour4_714;
	struct Uint8_tArr256 recon_block_16_715[2];
	int recon_block_16_1037;
	int recon_block_16_1037_phase;
	int do_it_out_716;
	int do_it_1039;
	int i_sad_i4x4_723;
	int i_sad_i4x4_1041_phase;
	struct IntBlock4x4 i_predict4x4_724;
	int i_predict4x4_1042_phase;
	int do_it_1043;
	struct Int16_tBlock4x4 dct4x4_725;
	int do_it_1045;
	struct Int16_tBlock4x4 dct4x4_out_727;
	int do_it_1048;
	int do_it_1049;
	struct Int16_tBlock4x4 dct4x4_out_731;
	int do_it_1051;
	struct Uint8_tArr16 dst_block_732[16];
	int dst_block_1052;
	int output_749[32];
	int output_1053;
	struct IntArr4x2 mv8x8_753;
	int i_cost8x8_754;
	struct IntArr4 i_cost8x8_sub_755;
	struct IntArr4 cost_mv_756;
	struct mv_mb mv_mb_757;
	struct IntArr2 mv_758;
	int cost_759;
	struct ref_mb ref_mb_760;
	struct IntArr4x4x2 mv4x4_767;
	struct IntArr4x2x2 mv8x4_768;
	struct IntArr4x2x2 mv4x8_769;
	struct IntArr4 i_cost4x4_770;
	struct IntArr4 i_cost8x4_771;
	struct IntArr4 i_cost4x8_772;
	struct IntArr4x4 i_cost4x4_sub_773;
	struct IntArr4x2 i_cost8x4_sub_774;
	struct IntArr4x2 i_cost4x8_sub_775;
	int i_cost8x8_776;
	struct mv_mb mv_mb_777;
	struct IntArr2x2 mv16x8_778;
	struct IntArr2x2 mv8x16_779;
	int i_cost16x8_780;
	int i_cost8x16_781;
	struct IntArr2 i_cost16x8_sub_782;
	struct IntArr2 i_cost8x16_sub_783;
	struct mv_mb mv_mb_784;
	struct ref_mb ref_mb_785;
	int do_it_out_792;
	int i_type_799;
	int i_cost_804;
	struct IntArr4x4x2 mv_805;
	int do_it_out_816;
	struct mv_mb mv_mb_817;
	struct IntArr4 predict_mode_848;
	int i_max_849;
	int i_predict16x16_850;
	int i_sad_i16x16_851;
	struct IntArr9 predict_mode_852;
	int i_max_853;
	int i_predict4x4_854;
	int i_sad_i4x4_855;
	struct Uint8_tArr16 best_4x4_block_856;
	int src_block_1091_phase;
	int recon_block_4_1093;
	struct Uint8_tArr16 src_block_4_857;
	struct preddata4 preddata4_858;
	int i_neighbour4_859;
	struct Uint8_tArr256 recon_block_16_860;
	int do_it_out_861;
	int i_sad_i4x4_868;
	int i_sad_i4x4_1100_phase;
	struct IntBlock4x4 i_predict4x4_869;
	int i_predict4x4_1101_phase;
	struct Int16_tBlock4x4 dct4x4_870;
	struct Int16_tBlock4x4 dct4x4_out_872;
	struct Int16_tBlock4x4 dct4x4_out_876;
	struct Uint8_tArr16 dst_block_877[16];
	int dst_block_1111;
	int output_894;
	int i_sad_i8x8chroma_898;
	int i_chroma_pred_mode_899;
	struct Uint8_tArr64 dst_block_U_900;
	struct Uint8_tArr64 dst_block_V_901;
	int i_cost_903;
	int do_it_out_904;
	int i_sad_i8x8chroma_912;
	int i_chroma_pred_mode_913;
	struct Uint8_tArr64 dst_block_U_914;
	struct Uint8_tArr64 dst_block_V_915;
	struct IntBlock4x4 i_pred4x4_918;
	struct Uint8_tArr256 recon_block_4x4_919;

	init_info i_info;
	neighbour_info n_info;
	deblock_send_info d_s_info;
	encoder_send_info e_s_info;
	int phase_16;

	int times;

	#if(MEASURE_TIME)
		int counter;
	    struct rusage start, end;
	    long long myutime;
	    long long mystime;
	#endif

    #if (RECORD_TIMING)
        FILE * fp_timing;
    #endif
	
} ME_State;

void ME_init(DALProcess *);
int ME_fire(DALProcess *);
void ME_finish(DALProcess *);

#endif
