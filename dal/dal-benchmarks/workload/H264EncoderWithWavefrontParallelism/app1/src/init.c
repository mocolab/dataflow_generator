#include "init.h"

/***********************
  FUNCTION DECLARATIONS
***********************/

static inline void plane_copy( uint8_t *dst, int i_dst,
        uint8_t *src, int i_src, int w, int h)
{
    for( ; h > 0; h-- )
    {
        memcpy( dst, src, w );
        dst += i_dst;
        src += i_src;
    }
}

static int read_frame_yuv( YUVFrameData *pFrameData, int iFrame, FILE *handle)
{
    uint8_t buffer[FRAME_WIDTH*FRAME_HEIGHT];

    if (ftell(handle) != iFrame * FRAME_WIDTH*FRAME_HEIGHT * 3 / 2)
        fseek(handle, iFrame * FRAME_WIDTH*FRAME_HEIGHT * 3 / 2, SEEK_SET);

    if (fread(buffer, 1, FRAME_WIDTH*FRAME_HEIGHT, handle ) <= 0)
        return -1;
    else
    {
        plane_copy( pFrameData->bufYData, FRAME_WIDTH+64,
                buffer, FRAME_WIDTH, FRAME_WIDTH, FRAME_HEIGHT );	
    }

    if (fread( buffer, 1, FRAME_WIDTH * FRAME_HEIGHT/4, handle ) <= 0)
        return -1;
    else
    {
        plane_copy( pFrameData->bufUData, (FRAME_WIDTH+64)/2,
                buffer, FRAME_WIDTH/2, FRAME_WIDTH/2, FRAME_HEIGHT/2 );	
    }

    if (fread( buffer, 1, FRAME_WIDTH * FRAME_HEIGHT/4, handle ) <= 0)
        return -1;
    else
    {
        plane_copy( pFrameData->bufVData, (FRAME_WIDTH+64)/2,
                buffer, FRAME_WIDTH/2, FRAME_WIDTH/2, FRAME_HEIGHT/2 );	
    }

    return 0;
}


/*
create_MB_indexes:
Creates an array of coordinates of the indexes of MBs that must be accessed on every MB_phase,
according to the wavefront parallelism scheme.
*/
static void create_MB_indexes(coords* ptr)
{
	int i,x,y,step;

    for(i=0,step=1;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;step++)
        for (x=0;x<SPS_MB_WIDTH;x++){
            for (y=0;y<SPS_MB_HEIGHT;y++)
				if (x+2*y+1 == step){
					ptr[i].x = x;
					ptr[i].y = y;
					i++;
				}
			}
}

/*********************
  DAL FUNCTIONS
**********************/

void init_init(DALProcess *p) {
    CREATEPORTVAR(port);
    {
        int i;
        InitMEPacket t;

        for(i=0;i<NUMBER_OF_ME_PROCESSES;i++){
            //the integer o_intFrameType will be used to send the index of the ME process despite its name
            t.o_intFrameType = i;
            CREATEPORT(port, PORT_OUT_ME, 1, i, NUMBER_OF_ME_PROCESSES);
            DAL_write( (void*)port, &t, sizeof(InitMEPacket), p); // to ME
        }
    }

	p->local->fpYUV_1872 = NULL;
	p->local->g_iFrame_1873 = -1;
	p->local->i_last_idr_1874 = -MAX_GOP_SIZE;
	p->local->i_idr_pic_id = 0;

	p->local->o_intFrameNum_1838 = 0;
	p->local->o_intFrameType_1839 = 0;
	p->local->fpYUV_1872 = fopen(FILE_NAME, "rb");

	if (NULL == p->local->fpYUV_1872)
	{
		printf("%s open error...\n", FILE_NAME);
		exit(0);
	}
	{int i; for(i=0;i<SPS_MB_WIDTH*SPS_MB_HEIGHT;i++) p->local->output_1857[i] = 0;}

	p->local->o_MB_index = 0 ;
	p->local->phase_99 = 0;

    create_MB_indexes(p->local->wvfp_MB_indexes);
}

int init_fire(DALProcess *p) {
    InitMEPacket t;

    int terminate = 0;

    if ( p->local->phase_99 == 0 ){
        p->local->g_iFrame_1873++;
        if (read_frame_yuv(&(p->local->o_msgFrameData_1837), p->local->g_iFrame_1873, p->local->fpYUV_1872) <0) {
            //signal VLC process to send the event to terminate the application
            terminate = 1;
            DAL_write((void*) PORT_OUT_VLC, &terminate, sizeof(int), p);
            return 1;
        }
        
        p->local->o_intFrameNum_1838 = p->local->g_iFrame_1873;

        if (p->local->g_iFrame_1873 - p->local->i_last_idr_1874 >= MAX_GOP_SIZE){
            p->local->o_intFrameType_1839 = FRAME_TYPE_IDR;

            p->local->i_s_info.inter_do_it = FRAME_TYPE_IDR;
            p->local->i_last_idr_1874 = p->local->g_iFrame_1873;
            p->local->o_msgIPoc_1840.m_nCurrRef = 0;
        }
        else{
            p->local->o_intFrameType_1839 = FRAME_TYPE_P;

            p->local->i_s_info.inter_do_it = FRAME_TYPE_P;
            p->local->o_msgIPoc_1840.m_nCurrRef = 2 * (p->local->g_iFrame_1873 - p->local->i_last_idr_1874);
        }

        p->local->o_msgIPoc_1840.m_nCurrFrame = 2 * (p->local->g_iFrame_1873 - p->local->i_last_idr_1874);


        {
            int i;
            for (i = 0; i < SPS_MB_WIDTH*SPS_MB_HEIGHT; i++) {
                p->local->output_1857[i] = p->local->i_s_info.inter_do_it;
            }
        }

        p->local->o_msgSliceHeader_1841.i_type = p->local->o_intFrameType_1839;
        p->local->o_msgSliceHeader_1841.i_first_mb = 0;
        p->local->o_msgSliceHeader_1841.i_last_mb = SPS_MB_WIDTH * SPS_MB_HEIGHT;
        p->local->o_msgSliceHeader_1841.i_frame_num = p->local->o_intFrameNum_1838;
        p->local->o_msgSliceHeader_1841.i_poc_lsb = 0;
        p->local->o_msgSliceHeader_1841.i_disable_deblocking_filter_idc = 0;

        if (FRAME_TYPE_IDR == p->local->o_intFrameType_1839)
        {
            p->local->o_msgSliceHeader_1841.i_idr_pic_id = p->local->i_idr_pic_id;
            p->local->o_msgSliceHeader_1841.b_num_ref_idx_override = 0;
            p->local->o_msgSliceHeader_1841.i_qp = QP_CONSTANT_I;
            p->local->i_idr_pic_id = (p->local->i_idr_pic_id + 1) % 65536;
        }
        else
        {
            p->local->o_msgSliceHeader_1841.i_idr_pic_id = -1;
            p->local->o_msgSliceHeader_1841.b_num_ref_idx_override = 1;
            p->local->o_msgSliceHeader_1841.i_qp = QP_CONSTANT;
        }

        p->local->o_msgSliceHeader_1841.i_qp_delta = p->local->o_msgSliceHeader_1841.i_qp - QP_CONSTANT;
        p->local->o_msgSliceHeader_1841.i_poc_lsb = p->local->o_msgIPoc_1840.m_nCurrRef & ((1<<(SPS_MAX_POC_LSB))-1);
    }

    memcpy(&t.output, &(p->local->output_1857), sizeof(int)*SPS_MB_WIDTH*SPS_MB_HEIGHT);
    memcpy(&t.o_msgSliceHeader, &(p->local->o_msgSliceHeader_1841), sizeof(SliceHeader));
    t.o_intFrameType = p->local->o_intFrameType_1839;

    int mb_phase = p->local->phase_99;

    int i_mb_x = p->local->wvfp_MB_indexes[mb_phase].x;
    int i_mb_y = p->local->wvfp_MB_indexes[mb_phase].y;
    uint8_t *p_plane;
    uint8_t *p_buf;

    p->local->o_MB_index= i_mb_x + SPS_MB_WIDTH*i_mb_y;

	{
        int i, j;
        for (i = 0; i < 3; i++){
            const int w = (i == 0 ? 16 : 8);
            const int i_stride = !i ? (FRAME_WIDTH+64) : (FRAME_WIDTH+64)/2;
            p_plane = !i ? p->local->o_msgFrameData_1837.bufYData : ((i == 1) ? p->local->o_msgFrameData_1837.bufUData : p->local->o_msgFrameData_1837.bufVData);
            p_buf = !i ? p->local->i_s_info.src_block_Y.data : ((i == 1) ? p->local->i_s_info.src_block_U.data : p->local->i_s_info.src_block_V.data);

            for (j = 0; j < w; j++)
                memcpy(&p_buf[j*w],  &p_plane[ w * ( i_mb_x + i_mb_y * i_stride ) + j * i_stride], w);
        }
    }

    p->local->i_s_info.intra_do_it = !p->local->output_1857[p->local->o_MB_index];

    memcpy(&t.i_s_info, &(p->local->i_s_info), sizeof(init_info));

    //signal VLC
    DAL_write((void*) PORT_OUT_VLC, &terminate, sizeof(int), p);

    CREATEPORTVAR(port);
    CREATEPORT(port, PORT_OUT_ME, 1, i_mb_y%NUMBER_OF_ME_PROCESSES,NUMBER_OF_ME_PROCESSES);
    DAL_write( (void*)port, &t, sizeof(InitMEPacket), p); // to ME

    p->local->phase_99++;

    if(p->local->phase_99 == SPS_MB_WIDTH*SPS_MB_HEIGHT){
        p->local->phase_99 = 0;
    }

    return 0; //successful execution
}

void init_finish(DALProcess *p) {
    fclose(p->local->fpYUV_1872);
}
