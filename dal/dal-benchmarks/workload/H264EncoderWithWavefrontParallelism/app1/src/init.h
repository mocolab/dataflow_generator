#ifndef INIT_H
#define INIT_H

#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <dal.h>

#include "parameters.h"
#include "structs.h"

#define PORT_OUT_ME "toME"
#define PORT_OUT_VLC "toVLC"


/***********************
  CONSTANT DECLARATIONS
***********************/

static const int i_qp0_cost_table[52] = 
{
    1, 1, 1, 1, 1, 1, 1, 1,  /*  0-7 */
    1, 1, 1, 1,              /*  8-11 */
    1, 1, 1, 1, 2, 2, 2, 2,  /* 12-19 */
    3, 3, 3, 4, 4, 4, 5, 6,  /* 20-27 */
    6, 7, 8, 9,10,11,13,14,  /* 28-35 */
    16,18,20,23,25,29,32,36,  /* 36-43 */
    40,45,51,57,64,72,81,91   /* 44-51 */
};


/*********************
  STRUCT DECLARATIONS
**********************/
typedef struct
{
    uint8_t bufYData[(FRAME_WIDTH+64) * FRAME_WIDTH];
    uint8_t bufUData[(FRAME_WIDTH/2+32) * FRAME_WIDTH/2];
    uint8_t bufVData[(FRAME_WIDTH/2+32) * FRAME_WIDTH/2];
} YUVFrameData;

typedef struct { int m_nCurrFrame; int m_nCurrRef; int m_nPrevRef; } IPoc;

typedef struct
{
    int i_type;
    int i_first_mb;
    int i_last_mb;		
    int i_frame_num;		
    int i_idr_pic_id;
    int i_poc_lsb;		
    int b_num_ref_idx_override;		
    int i_qp;
    int i_qp_delta;		
    int i_disable_deblocking_filter_idc;		
} SliceHeader;

typedef struct{
    int output[SPS_MB_WIDTH*SPS_MB_HEIGHT];
    int o_intFrameType;
    SliceHeader o_msgSliceHeader;
    init_info i_s_info;
} InitMEPacket;

typedef struct _coords{
	int x;
	int y;
} coords;
/****************************
  LOCAL STATE OF THE PROCESS
*****************************/

typedef struct _local_states {

	FILE *fpYUV_1872;
	int g_iFrame_1873;
	int i_last_idr_1874;
	int i_idr_pic_id;


	YUVFrameData o_msgFrameData_1837;
	int o_intFrameNum_1838;
	int o_intFrameType_1839;
	IPoc o_msgIPoc_1840;
	SliceHeader o_msgSliceHeader_1841;
	int output_1857[SPS_MB_WIDTH*SPS_MB_HEIGHT];
	int o_MB_index;
	init_info i_s_info;
	int phase_99;
    coords wvfp_MB_indexes[SPS_MB_WIDTH*SPS_MB_HEIGHT];

	#if (MEASURE_TIME)
		int counter;
	    struct rusage start, end;
	    long long myutime[25];
	    long long mystime[25];
	    struct timeval total_start, total_end;
    #endif

    #if (RECORD_TIMING)
        FILE * fp_timing;
        FILE * fp_fps;
    #endif
        
} init_State;

void init_init(DALProcess *);
int init_fire(DALProcess *);
void init_finish(DALProcess *);

#endif
