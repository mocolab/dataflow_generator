#ifndef DEBLOCK_H
#define DEBLOCK_H

#define PORT_IN_ENCODER "fromEncoder"
#define PORT_OUT_ME "toME"

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <dal.h>

#include "parameters.h"
#include "x264.h"
#include "structs.h"


#define IS_INTRA(type) ( (type) == I_4x4 || (type) == I_8x8 || (type) == I_16x16)
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define mmax(a, b) (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define mmin(a, b) (((a) < (b)) ? (a) : (b))

typedef void (*x264_deblock_inter_t)( uint8_t *pix, int stride, int alpha, int beta, int8_t *tc0 );
typedef void (*x264_deblock_intra_t)( uint8_t *pix, int stride, int alpha, int beta );
typedef struct {
    uint8_t data[FRAME_WIDTH*FRAME_HEIGHT];
} Frame1;
typedef struct {
    uint8_t data[FRAME_WIDTH*FRAME_HEIGHT/4];
} HalfFrame1;


typedef struct{
    int intFrameType;
    int MB_index_Enc;
    struct Uint8_tArr64 recon_block_U;
    struct Uint8_tArr64 recon_block_V;
    struct Uint8_tArr256 recon_block_1313;
    struct Uint8_tArr256 recon_block_1382;
    struct IntArr48 non_zero_count;
    deblock_send_info d_s_info;
} EncDbkPacket;

typedef struct _coords{
	int x;
	int y;
} coords;

/****************************
  LOCAL STATE OF THE PROCESS
*****************************/

typedef struct _local_states {
	int intra4x4_pred_mode_frm_1662[SPS_MB_WIDTH*SPS_MB_HEIGHT][7];   
	uint8_t *p_Y_frm_buf_1663;
	uint8_t *p_U_frm_buf_1664;
	uint8_t *p_V_frm_buf_1665;

	uint8_t *plane0_1666;
	uint8_t *plane1_1667;
	uint8_t *plane2_1668;
	uint8_t *plane0_1669;
	uint8_t *filtered1_1670;
	uint8_t *filtered2_1671;
	uint8_t *filtered3_1672;

	struct FrameLuma recon_frame_Y_1599;
	struct FrameChroma recon_frame_U_1600;
	struct FrameChroma recon_frame_V_1601;
	struct ref_mb ref_mb_1604;
	struct mb_frm mb_frm_1609;
	struct nzc_frm nzc_frm_1610;
	struct ref_frm ref_frm_1611;
	struct mv_frm mv_frm_1612;
	struct FrameLuma recon_frame_Y_1613;
	struct FrameChroma recon_frame_U_1614;
	struct FrameChroma recon_frame_V_1615;
	struct FrameLuma Filtered_frame_1_1619;
	struct FrameLuma Filtered_frame_2_1620;
	struct FrameLuma Filtered_frame_3_1621;
	int output_1630;
	struct Uint8_tArr256 output_1634;
	struct Uint8_tArr256 output_1635;
	struct Uint8_tArr64 output_1636;
	struct Uint8_tArr64 output_1637;
	struct IntArr48 output_1639;
	int output_1642;
	
	int mb_index_Debl;

	deblock_send_info d_r_info;
	neighbour_info info_R;

	#if(MEASURE_TIME)
		int counter[2];
	  struct rusage start, end;
	  long long myutime[2];
	  long long mystime[2];
	#endif

    #if (RECORD_TIMING)
        FILE * fp_timing;
    #endif

    coords wvfp_MB_indexes[SPS_MB_WIDTH*SPS_MB_HEIGHT];
    int no_steps;
    int* MBs_per_step;
    int current_step;
    int phase;

} deblock_State;


void deblock_init(DALProcess *);
int deblock_fire(DALProcess *);
void deblock_finish(DALProcess *);

#endif
