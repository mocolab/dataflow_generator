#ifndef PARAMETERS_H
#define PARAMETERS_H

//input options
//#define FILE_NAME "../app1/src/FOREMAN.CIF"
//#define FILE_NAME "../app1/src/deadline_cif.yuv"
//#define FILE_NAME "../app1/src/soccer_cif.yuv"
#define FILE_NAME "app/APP1/FOREMAN.CIF"
//#define FILE_NAME "../app1/src/test.yuv"
#define FRAME_WIDTH 	352
#define FRAME_HEIGHT 	288

//debugging options
#define MEASURE_TIME 		1
#define RECORD_TIMING		0 //needs MEASURE_TIME enabled to compile

//encoding options
#define WRITE_SEI_VERSION 	0 //sei nal units are not recognised by the accompanied decoder, this should be 0 when using it
#define REFERENCE_FRAME_COUNT 	1
#define MAX_GOP_SIZE			250
#define MIN_GOP_SIZE			25
#define SCENE_CUT				40 // How aggressively to insert extra I-frames
#define SUBPIXEL_ME_P			5
#define FPS_NUM					30
#define FPS_DEN					2
#define IP_FACTOR				1.4
#define LEVEL_IDC				51
#define QP_CONSTANT 			26
#define QP_CONSTANT_I 			23
#define SPS_MAX_FRAME_NUM		8
#define X264_SCAN8_0 			12
#define MAX_ME_RANGE 			16
#define DATA_MAX 				3000000

//#define MIN_QP_VALUE		10
//#define MAX_QP_VALUE		51
//#define BITRATE_VALUE		1000
//#define MAX_QP_STEP			4
//#define RATE_TOLERANCE		1.0
//#define LEVEL_FRAME_SIZE	36864 	// max frame size (macroblocks)
//#define LEVEL_DPB			70778880 // max decoded picture buffer (bytes)
//#define LEVEL_MBPS			983040   // max macroblock processing rate (macroblocks/sec)
//#define LEVEL_MV_RANGE		512
//#define X264_SCAN8_SIZE 48 

// DO NOT CHANGE THE FOLLOWING:
#define FRAME_TYPE_IDR		0
#define FRAME_TYPE_P		1
#define SPS_MB_WIDTH ((FRAME_WIDTH+15)/16)
#define SPS_MB_HEIGHT ((FRAME_HEIGHT+15)/16)
#define SPS_CROP_RIGHT		((-FRAME_WIDTH)&5)
#define SPS_CROP_BOTTOM		((-FRAME_HEIGHT)&15)
#define SPS_B_CROP			(SPS_CROP_RIGHT||SPS_CROP_BOTTOM)
#define SPS_MAX_POC_LSB		(SPS_MAX_FRAME_NUM+1)
#define X264_ANALYSE_I4x4       0x0001  /* Analyse i4x4 */
#define X264_ANALYSE_PSUB16x16  0x0010  /* Analyse p16x8, p8x16 and p8x8 */
#define X264_ANALYSE_PSUB8x8    0x0020  /* Analyse p8x4, p4x8, p4x4 */
#define X264_ANALYSE_INTRA      (X264_ANALYSE_I4x4)
#define X264_ANALYSE_INTER      (X264_ANALYSE_I4x4 | X264_ANALYSE_PSUB16x16 | X264_ANALYSE_PSUB8x8)


#endif