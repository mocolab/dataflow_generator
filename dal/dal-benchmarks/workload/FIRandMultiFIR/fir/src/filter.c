#include <stdio.h>
#include <math.h>
#include "filter.h"

/**
 * Write zero to feedback output.
 */
void filter_init(DALProcess *p)
{
  p->local->zero = 0.0;
  p->local->factor = 0.5;
  p->local->firstiteration = 1;
  p->local->index = 0;
}

/**
 * Filter.
 */
int filter_fire(DALProcess *p)
{
  if (p->local->firstiteration) {
      DAL_write((void*)PORT_OUTB, &(p->local->zero), sizeof(float), p);
      p->local->firstiteration = 0;
  }

  DAL_read((void*)PORT_INA, &(p->local->inA), sizeof(float), p);
  DAL_read((void*)PORT_INB, &(p->local->inB), sizeof(float), p);
  p->local->out = p->local->inA + p->local->factor * p->local->inB;
  DAL_write((void*)PORT_OUTA, &(p->local->out), sizeof(float), p);
  DAL_write((void*)PORT_OUTB, &(p->local->out), sizeof(float), p);

  p->local->index++;

  if (p->local->index >= 10) {
    return(1);
  }

  return 0;
}

void filter_finish(DALProcess *p) {
	fflush(stdout);
}
