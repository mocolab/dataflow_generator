#include <stdio.h>

#include "consumer.h"

void consumer_init(DALProcess *p)
{
  ; //nothing to be done here
}

int consumer_fire(DALProcess *p)
{
  static int index;
  static float sample;

  for (index = 0; index < 10; index++)
  {
    DAL_read((void*)PORT_IN, &sample, sizeof(float), p);
    printf("%8s:                             Read sample[%02d]: %+6.4f\n",
           "fir_cons", index, sample);
    fflush(stdout);
  }

  DAL_send_event((void *)EVENT_1, p);
  return(1);
}


void consumer_finish(DALProcess *p) {
	fflush(stdout);
}
