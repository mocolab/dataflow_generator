#ifndef FILTER_H
#define FILTER_H

#include <dal.h>

#define PORT_INA  "inA"
#define PORT_INB  "inB"
#define PORT_OUTA "outA"
#define PORT_OUTB "outB"

typedef struct _local_states
{
  int firstiteration;
  float inA, inB, out, zero, factor;
  int index;
} Filter_State;

void filter_init(DALProcess *);
int filter_fire(DALProcess *);
void filter_finish(DALProcess *);

#endif
