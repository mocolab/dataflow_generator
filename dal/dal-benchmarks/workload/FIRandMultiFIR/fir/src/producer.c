#include <stdio.h>
#include <string.h>

#include "producer.h"

/**
 * Returns a random integer in the range between lower_bound and
 * upper_bound, where the bounding values are included in the interval.
 */
int getRandomNumber(int lower_bound, int upper_bound)
{
  return (rand() % (upper_bound - lower_bound + 1)) + lower_bound;
}


void producer_init(DALProcess *p)
{
  ; //nothing to be done here
}


int producer_fire(DALProcess *p)
{
  static int index;

  srand(0); //initialize random number generator

  char str[1024];
  //generate input samples and display them
  strcpy (str, "fir_prod: samples = { ");

  for (index = 0; index < 10; index++) {
    p->local->sample[index] = (float) getRandomNumber(-9, 9);
    char tmp[20];
    if (index < 9) {
    	sprintf(tmp, "%+3.1f, ", p->local->sample[index]);
    }
    else {
    	sprintf(tmp, "%+3.1f }\n", p->local->sample[index]);
    }
    strcat (str, tmp);
  }
  printf("%s", str); fflush(stdout);

  //write samples to output port
  for (index = 0; index < 10; index++) {
    printf("%8s: Write sample[%02d]: %+6.4f\n",
           "fir_prod", index, p->local->sample[index]);
    fflush(stdout);
    DAL_write((void*)PORT_OUT, &(p->local->sample[index]),
            sizeof(float), p);
  }

  return(1);
}

void producer_finish(DALProcess *p) {
	fflush(stdout);
}

