#ifndef SQUARE_H
#define SQUARE_H

#include <dal.h>
#include "global.h"

#define PORT_IN  0
#define PORT_OUT 1

typedef struct _local_states {
    int index;
    int len;
} Square_State;

void square_init(DALProcess *);
int square_fire(DALProcess *);
void square_finish(DALProcess *);

#endif
