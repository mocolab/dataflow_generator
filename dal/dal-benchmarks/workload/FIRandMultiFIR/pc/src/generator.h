#ifndef GENERATOR_H
#define GENERATOR_H

#include <dal.h>
#include "global.h"

#define  PORT_OUT 10

typedef struct _local_states {
    int index;
    int len;
} Generator_State;

void generator_init(DALProcess *);
int generator_fire(DALProcess *);
void generator_finish(DALProcess *);

#endif
