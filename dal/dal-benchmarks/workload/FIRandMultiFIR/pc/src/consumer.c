#include <stdio.h>

#include "consumer.h"

void consumer_init(DALProcess *p) {
    sprintf(p->local->name, "pc_cons");
    p->local->index = 0;
    p->local->len = LENGTH;
}

int consumer_fire(DALProcess *p) {
    float c;
    if (p->local->index < p->local->len) {
        DAL_read((void*)PORT_IN, &c, sizeof(float), p);
        printf("%s: %f\n", p->local->name, c);fflush(stdout);
        p->local->index++;
    }

    if (p->local->index >= p->local->len) {
    	DAL_send_event((void *)EVENT_1, p);
    	return(1);
    }

    return 0;
}

void consumer_finish(DALProcess *p) {
    fflush(stdout);
}
