#include <stdio.h>
#include <stdlib.h>
#include "filter.h"

/**
 * Init.
 */
void filter_init(DALProcess *p)
{
  int k;
  p->local->process_index = GETINDEX(0);

  for (k = 0; k < p->local->process_index; k++)
    rand();

  sprintf(p->local->id, "filter_%d",
          GETINDEX(0));
  p->local->first_invocation = 1;
  p->local->index = 0;
  //generate a random filter coefficient between -1 and 1
  p->local->filter_coefficient = ((float) (GETINDEX(0) % 21) - 10)/10.0;
  p->local->zero = 0.0;
  printf("init %s: filter coefficient = %+2.1f\n",
         p->local->id, p->local->filter_coefficient);
  fflush(stdout);
}

/**
 * Filter.
 */
int filter_fire(DALProcess *p)
{
  //behaviour of the top filter stage
  if (p->local->process_index == 0) {
    if (p->local->first_invocation) {
      DAL_read((void*)PORT_INA, &(p->local->inA), sizeof(float), p);
      p->local->out = p->local->inA;
      p->local->inB = 0.0;
      p->local->first_invocation = 0;
    }
    else {
      DAL_read((void*)PORT_INA, &(p->local->inA), sizeof(float), p);
      DAL_read((void*)PORT_INB, &(p->local->inB), sizeof(float), p);
      p->local->out = p->local->inB + p->local->inA;
    }

    printf("%8s: inA: %6.4f, inB: %6.4f, outA = outB: %6.4f\n",
           p->local->id, p->local->inA, p->local->inB, p->local->out);
    fflush(stdout);

    DAL_write((void*)PORT_OUTA, &(p->local->out), sizeof(float), p);
    DAL_write((void*)PORT_OUTB, &(p->local->out), sizeof(float), p);

  }
  //behaviour of the intermediate filter stages
  else {
    if (p->local->first_invocation) {
      DAL_read((void*)PORT_INA, &(p->local->inA), sizeof(float), p);
      p->local->out = p->local->filter_coefficient * p->local->inA;
      p->local->inB = 0.0;
      p->local->first_invocation = 0;
    }
    else {
      DAL_read((void*)PORT_INA, &(p->local->inA), sizeof(float), p);
      DAL_read((void*)PORT_INB, &(p->local->inB), sizeof(float), p);
      p->local->out = p->local->inB + p->local->filter_coefficient
              * p->local->inA;
    }

    printf("%8s: inA: %6.4f, inB: %6.4f, outA: %6.4f, outB: %6.4f\n",
           p->local->id, p->local->inA, p->local->inB, p->local->inA,
           p->local->out);
    fflush(stdout);

    if (p->local->process_index < NUMBER_OF_FILTER - 1) {
      DAL_write((void*)PORT_OUTA, &(p->local->inA), sizeof(float), p);
      DAL_write((void*)PORT_OUTB, &(p->local->out), sizeof(float), p);
    }
    //behaviour of the bottom filter stage
    else {
      DAL_write((void*)PORT_OUTA, &(p->local->zero), sizeof(float), p);
      DAL_write((void*)PORT_OUTB, &(p->local->out), sizeof(float), p);
    }
  }

  p->local->index++;

  if (p->local->index >= NUMBER_OF_SAMPLES - p->local->process_index) {
	  return(1);
  }

  return 0;
}

void filter_finish(DALProcess *p) {
	fflush(stdout);
}
