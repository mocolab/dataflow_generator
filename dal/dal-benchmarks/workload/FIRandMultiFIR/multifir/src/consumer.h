#ifndef CONSUMER_H
#define CONSUMER_H

#include <dal.h>
#include "global.h"

#define PORT_IN "in"
#define EVENT_1 "stop_multifir"

typedef struct _local_states
{
} Consumer_State;

void consumer_init(DALProcess *);
int consumer_fire(DALProcess *);
void consumer_finish(DALProcess *);

#endif
