#include <stdio.h>
#include <string.h>

#include "producer.h"

void producer_init(DALProcess *p)
{
  printf("producer: init\n");
}


int producer_fire(DALProcess *p)
{
  //generate input samples and display them
  char str[1024];
  strcpy (str, "producer: samples = { ");

  for (p->local->index = 0; p->local->index < NUMBER_OF_SAMPLES; p->local->index++) {
    p->local->sample[p->local->index] = (float) p->local->index;
    char tmp[20];
    if (p->local->index < NUMBER_OF_SAMPLES - 1) {
      sprintf(tmp, "%+3.1f, ", p->local->sample[p->local->index]);
      strcat (str, tmp);
    }
    else {
      sprintf(tmp, "%+3.1f }\n", p->local->sample[p->local->index]);
      strcat (str, tmp);
    }
  }
  printf("%s", str);
  fflush(stdout);

  //write samples to output port
  for (p->local->index = 0; p->local->index < NUMBER_OF_SAMPLES; p->local->index++) {
    printf("%8s: Write sample[%02d]: %+6.4f\n",
           "producer", p->local->index, p->local->sample[p->local->index]);
    fflush(stdout);
    DAL_write((void*)PORT_OUT, &(p->local->sample[p->local->index]),
            sizeof(float), p);
  }

  return(1);
}

void producer_finish(DALProcess *p) {
	fflush(stdout);
}
