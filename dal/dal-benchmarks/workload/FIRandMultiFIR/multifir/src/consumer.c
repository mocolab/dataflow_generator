#include <stdio.h>

#include "consumer.h"

void consumer_init(DALProcess *p)
{
  printf("multi_co: init\n");
}

int consumer_fire(DALProcess *p)
{
  static int index;
  static float sample;

  for (index = 0; index < NUMBER_OF_SAMPLES; index++)
  {
    DAL_read((void*)PORT_IN, &sample, sizeof(float), p);
    printf("%8s:                             Read sample[%02d]: %+6.4f\n",
           "multi_co", index, sample);
    fflush(stdout);
  }

  DAL_send_event((void *)EVENT_1, p);
  return(1);
}

void consumer_finish(DALProcess *p) {
	fflush(stdout);
}
