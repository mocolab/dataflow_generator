#ifndef PRODUCER_H
#define PRODUCER_H

#include <dal.h>
#include "global.h"
#include "stdlib.h"

#define PORT_OUT "out"

typedef struct _local_states
{
  int index;
  float sample[NUMBER_OF_SAMPLES];
} Producer_State;

void producer_init(DALProcess *);
int producer_fire(DALProcess *);
void producer_finish(DALProcess *);

#endif
