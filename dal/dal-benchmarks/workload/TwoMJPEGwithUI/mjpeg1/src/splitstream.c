/************************************************************************
 * dispatch frames                                                      *
 ************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "splitstream.h"
#include "jpeg.h"
#include "fileString.h" //read video from string

//#include "mjpeg.c"

void splitstream_init(DALProcess *p) {
    p->local->port_count = 0;
    p->local->frame_number = 0;
    p->local->len = 0;
}

int splitstream_fire(DALProcess *p) {
	if (p->local->len == 0) {
		p->local->len = sizeof(_STR_1);
        p->local->hptr = _STR_1;
        p->local->fptr = p->local->hptr;
	}

    CREATEPORTVAR(port1); //size
    CREATEPORTVAR(port2); //frame
    CREATEPORT(port1, PORT_OUT1, 1, p->local->port_count, NUM_OF_PAR_PORTS);
    CREATEPORT(port2, PORT_OUT2, 1, p->local->port_count, NUM_OF_PAR_PORTS);

    while (p->local->len != 0) {
    	p->local->ptr = (unsigned char *)memchr(p->local->hptr, 0xFF, p->local->len);
        if (*(p->local->ptr + 1) == (unsigned char) '\xD9') { //end of image 0xFFD9

            int i = 0;
        	DAL_read((void*)PORT_IN, &(i), sizeof(int), p);

        	p->local->ptr += 2;
            int fLen = p->local->ptr - p->local->fptr;

            DAL_write((void*)port1, &fLen, sizeof(fLen), p);
            DAL_write((void*)port2, p->local->fptr, fLen, p);

            p->local->len -= (p->local->ptr - p->local->hptr);
            p->local->hptr = p->local->ptr;
            p->local->fptr = p->local->hptr;

            //create the next port for next picture
            p->local->port_count = (p->local->port_count + 1) % NUM_OF_PAR_PORTS;
            return 0;

        } else {
        	p->local->ptr += 2;
        	p->local->len -= (p->local->ptr - p->local->hptr);
        	p->local->hptr = p->local->ptr;
        }

    }
    //DAL_detach(p);
    dbgprintf_1(VERBOSE, "Traffic Generator : end of file reached");
    return 0;
    //return 1;
}

void splitstream_finish(DALProcess *p) {
    fflush(stdout);
}
