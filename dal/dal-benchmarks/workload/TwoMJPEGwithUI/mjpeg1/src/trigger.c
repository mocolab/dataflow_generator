#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "trigger.h"

// initialization function
void trigger_init(DALProcess *p) {
    p->local->index = 0;
}

int trigger_fire(DALProcess *p) {
    int x = p->local->index;
    DAL_write((void*)PORT_OUT, &(x), sizeof(int), p);
    p->local->index++;

    //if (p->local->index >= NUMBER_OF_FRAMES) {
        //DAL_detach(p);
    //	return 1;
    //}

    return 0;
}

void trigger_finish(DALProcess *p) {
    fflush(stdout);
}
