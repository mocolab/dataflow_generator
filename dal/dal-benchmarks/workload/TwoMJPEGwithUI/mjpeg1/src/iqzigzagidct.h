#ifndef IQZIGZAGIDCT_H
#define IQZIGZAGIDCT_H

#include <dal.h>

#define PORT_IN1   "in1"
#define PORT_IN2   "in2"
#define PORT_IN3   "in3"
#define PORT_OUT1  "out1"

//local variables
typedef struct _local_states {
    int num_iter; //number of iterations
} Iqzigzagidct_State;

void iqzigzagidct_init(DALProcess *);
int iqzigzagidct_fire(DALProcess *);
void iqzigzagidct_finish(DALProcess *);

#endif
