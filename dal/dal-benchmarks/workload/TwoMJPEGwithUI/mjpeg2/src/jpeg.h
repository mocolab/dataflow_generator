/************************************************************************
 * MJPEG-JFIF decoder                                                   *
 ************************************************************************/

#ifndef __JPEG_H__
#define __JPEG_H__

#include <dal.h>
#include "mjpeg.h"

/* The tg use a TDMA schema to dispatch frames to output
   ports. The number of parallel ports is defined below */
#define NUM_OF_PAR_PORTS 1

// Number of frames, to end each process separately, especially
// for MPARM and VSP, where the simulation ends only when every process ends
#define NUMBER_OF_FRAMES 31  //1, 2, 31, 8

/* define maximum picture size */
#define MAX_WIDTH  320 //1024
#define MAX_HEIGHT 240 //1024
// maximum size of an encoded frame. should be multiple of 4 bytes
#define MAX_ENCODED_FRAME  10000

// define fixed MCU size (8)
#define MCU_sx     8
#define MCU_sy     8
#define MAX_YMCU   MAX_WIDTH/MCU_sy
#define MAX_XMCU   MAX_HEIGHT/MCU_sx
#define MAX_BLOCK  MAX_YMCU*MAX_XMCU
/* size of maximum send element in fifo */
/* NB_SEND * MAX_SEND = 64              */
#define MAX_SEND   64
#define NB_SEND    1


/*----------------------------------*/
/* JPEG format parsing markers here */
/*----------------------------------*/
#define SOI_MK     0xFFD8		/* start of image       */
#define APP_MK     0xFFE0		/* custom, up to FFEF   */
#define COM_MK     0xFFFE		/* commment segment     */
#define SOF_MK     0xFFC0		/* start of frame       */
#define SOS_MK     0xFFDA		/* start of scan        */
#define DHT_MK     0xFFC4		/* Huffman table        */
#define DQT_MK     0xFFDB		/* Quant. table	        */
#define DRI_MK     0xFFDD		/* restart interval     */
#define EOI_MK     0xFFD9		/* end of image         */
#define MK_MSK     0xFFF0
#define RST_MK(x)  ( (0xFFF8&(x)) == 0xFFD0 ) /* is x a restart interval ? */

/*-------------------------------------------------------- */
/* all kinds of macros here				*/
/*-------------------------------------------------------- */
#define first_quad(c)   ((c)>>4)        /* first 4 bits in file order */
#define second_quad(c)  ((c)&15)
#define HUFF_ID(class, id)  (2*(class)+(id))
#define DC_CLASS        0
#define AC_CLASS        1
#define CASS            1
#define MAX_SIZE(Mclass) ((Mclass)?162:14) /* Memory size of HTables */

/*-------------------------------------------------------*/
/* JPEG data types here					*/
/*-------------------------------------------------------*/
/* component descriptor structure */
typedef struct {
	unsigned char	CID;	/* component ID */
	char		QT;	/* QTable index, 2bits 	*/
	char		DC_HT;	/* DC table index, 1bit */
	char		AC_HT;	/* AC table index, 1bit */
	int		PRED;	/* DC predictor value */
	} cd_t_2;

/*--------------------------------------------*/
/* global variables here                      */
/*--------------------------------------------*/
#ifdef CASS
#define EOF             (-1)
#endif

/*-----------------------------------------*/
/* prototypes from utils.c		   */
/*-----------------------------------------*/
int intceil_2(int N, int D);
int intfloor_2(int N, int D);
int reformat_2(unsigned long S, int good);

/*-----------------------------------------*/
/* prototypes from parse.c		   */
/*-----------------------------------------*/
//parse3.c
int get_bits2_2(const void *, DALProcess *, int, unsigned long *);
int get_one_bit2_2(const void *, DALProcess *, unsigned char *);
void clear_bits_2(DALProcess *);
static unsigned int get_size3_2(const void *, DALProcess *);

//parse.c
static unsigned int get_size2_2(const void *, DALProcess *);
unsigned int get_next_MK2_2(const void *, DALProcess *);
void skip_segment2_2(const void *, DALProcess *);
int load_quant_tables2_2(const void *, const void *, DALProcess *);

/*-----------------------------------------*/
/* prototypes from table_vld.c or tree_vld.c */
/*-----------------------------------------*/
int load_huff_tables2_2(const void *, DALProcess *);
int get_symbol2_2(const void *, DALProcess *, int, unsigned char *);

/*-----------------------------------------*/
/* prototypes from huffman.c 		   */
/*-----------------------------------------*/
int unpack_block2_2(const void *, const void *, DALProcess *, cd_t_2 *);

/*-----------------------------------------*/
/* command for VLD                     	   */
/*-----------------------------------------*/
#define DHT_CMD	0xFA  /* command Define Huffman Table for VLD */
#define SOS_CMD	0xFB  /* command Start Of Scan for VLD        */
#define RPS_CMD	0xFC  /* command Read Picture Size for VLD    */
#define ABR_CMD	0xFD  /* command abort picture                */

#endif	// __JPEG_H__
