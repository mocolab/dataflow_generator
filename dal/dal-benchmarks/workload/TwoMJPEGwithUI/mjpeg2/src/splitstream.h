#ifndef SPLITSTREAM_H
#define SPLITSTREAM_H

#include <dal.h>

#define  PORT_IN  "20"

#define PORT_OUT1 "out1"
#define PORT_OUT2 "out2"

//local variables
typedef struct _local_states {
    int port_count;
    int frame_number;
    int len;
    unsigned char *ptr;
    unsigned char *hptr;
    unsigned char *fptr;
} Splitstream_State;

void splitstream_init(DALProcess *);
int splitstream_fire(DALProcess *);
void splitstream_finish(DALProcess *);

#endif
