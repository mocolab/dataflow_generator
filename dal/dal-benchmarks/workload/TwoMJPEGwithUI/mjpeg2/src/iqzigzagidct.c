/************************************************************************
 * inverse quantization                                                 *
 ************************************************************************/

#include <stdio.h>
#include "iqzigzagidct.h"
#include "jpeg.h"

void iqzigzagidct_init(DALProcess *p) {
    p->local->num_iter = 0;
}

int iqzigzagidct_fire(DALProcess *p) {
    //three quantization tables/image for JPEG,
    //only one table for JFIF, 64 elements for JFIF table.
    int i, j, nb_block;
    int block[64];
    int block2[8][8];
    unsigned char idct[8][8];
    unsigned char qtable[64];

    DAL_read((void*)PORT_IN2, &qtable[0], sizeof(qtable), p);
    //read number of blocks to unquantify
    DAL_read((void*)PORT_IN3, &nb_block, sizeof(int), p);

    for (j = 0; j < nb_block; j++) {
        DAL_read((void*)PORT_IN1, block, 64 * sizeof(block[0]), p);

        unquantify_2(block, qtable);
        unZigZag_2((int*)block2, block);
        IDCT_2(block2, idct);

        DAL_write((void*)PORT_OUT1, idct, 64 * sizeof(idct[0][0]), p);
    }

    //end this process
    p->local->num_iter++;

    return 0;
}

void iqzigzagidct_finish(DALProcess *p) {
    fflush(stdout);
}
