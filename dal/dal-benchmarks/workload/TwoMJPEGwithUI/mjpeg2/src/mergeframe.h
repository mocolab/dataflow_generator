#ifndef MERGEFRAME_H
#define MERGEFRAME_H

#include <dal.h>
#include "jpeg.h"

#define PORT_IN1   "in1"
#define PORT_IN2   "in2"
#define PORT_OUT1  "out1"
#define PORT_OUT2  "out2"

//local variables
typedef struct _local_states {
    int x_s;
    int y_s;
    unsigned int disp_nb;
    unsigned char ColorBuffer[MCU_sx * MCU_sy];
    unsigned char LineBuffer[MAX_WIDTH * MCU_sx];
    int dataBuff[(MAX_WIDTH * MCU_sy) / 4];
    unsigned int send_size;
    int num_iter;
} Mergeframe_State;

void mergeframe_init(DALProcess *);
int mergeframe_fire(DALProcess *);
void mergeframe_finish(DALProcess *);

#endif
