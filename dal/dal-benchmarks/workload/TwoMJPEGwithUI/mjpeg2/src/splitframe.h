#ifndef SPLITFRAME_H
#define SPLITFRAME_H

#include <dal.h>
#include "jpeg.h"

#define PORT_IN1   "in1"
#define PORT_IN2   "in2"
#define PORT_OUT1  "out1"
#define PORT_OUT2  "out2"
#define PORT_OUT3  "out3"
#define PORT_OUT4  "out4"

typedef struct _vld_state_2 {
    int mx_size;
    int my_size; //picture size in units of MCUs
    int x_size;
    int y_size; //picture size in pixels
    unsigned char DC_Table0[MAX_SIZE(DC_CLASS)];
    unsigned char DC_Table1[MAX_SIZE(DC_CLASS)];
    unsigned char AC_Table0[MAX_SIZE(AC_CLASS)];
    unsigned char AC_Table1[MAX_SIZE(AC_CLASS)];
    unsigned char *HTable[4];
    int MinCode[4][16];
    int MaxCode[4][16];
    int ValPtr[4][16];
    unsigned char window;
    unsigned char bit_count; //available bits in the window
} VLD_State_2;

//local variables
typedef struct _local_states {
    char frame[MAX_ENCODED_FRAME]; //frame
    int fLen;                      //frame size
    char *fptr;                    //current read ptr
    VLD_State_2 vld;                 //VLD state
    int num_iter;                  //number of iteration
} Splitframe_State;

void splitframe_init(DALProcess *);
int splitframe_fire(DALProcess *);
void splitframe_finish(DALProcess *);

#endif
