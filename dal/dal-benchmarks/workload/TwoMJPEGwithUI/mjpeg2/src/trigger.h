#ifndef TRIGGER_H
#define TRIGGER_H

#include <dal.h>
#include "jpeg.h"

#define  PORT_OUT "10"

// local variables for a process. Different process can has different
// variables included in this structure.
typedef struct _local_states {
    int index;
} Trigger_State;

void trigger_init(DALProcess *);
int trigger_fire(DALProcess *);
void trigger_finish(DALProcess *);

#endif
