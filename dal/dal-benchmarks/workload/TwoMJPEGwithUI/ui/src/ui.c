#include <stdio.h>
#include <string.h>

#include "ui.h"

// initialization function
void ui_init(DALProcess *p) {
	p->local->index = 0;
	p->local->len = 0;
	p->local->App1State = STOP;
	p->local->App2State = STOP;
}

int ui_fire(DALProcess *p) {

	char input[80];

	if (p->local->App1State == STOP && p->local->App2State == STOP) {
		printf("Next action to perform [start1, start2, exit]:");
	} else if (p->local->App1State == STOP && p->local->App2State == RUN) {
		printf("Next action to perform [start1, stop2, pause2]:");
	} else if (p->local->App1State == STOP && p->local->App2State == PAUSE) {
		printf("Next action to perform [start1, resume2, stop2]:");
	} else if (p->local->App1State == RUN && p->local->App2State == STOP) {
		printf("Next action to perform [stop1, pause1, start2]:");
	} else if (p->local->App1State == RUN && p->local->App2State == RUN) {
		printf("Next action to perform [stop1, pause1, stop2, pause2]:");
	} else if (p->local->App1State == RUN && p->local->App2State == PAUSE) {
		printf("Next action to perform [stop1, pause1, resume2, stop2]:");
	} else if (p->local->App1State == PAUSE && p->local->App2State == STOP) {
		printf("Next action to perform [resume1, stop1, start2]:");
	} else if (p->local->App1State == PAUSE && p->local->App2State == RUN) {
		printf("Next action to perform [resume1, stop1, stop2, pause2]:");
	} else if (p->local->App1State == PAUSE && p->local->App2State == PAUSE) {
		printf("Next action to perform [resume1, stop1, resume2, stop2]:");
	} else {
		perror("Invalid state. The systems stops.\n");
		return 1;
	}

	fflush(stdout);
	scanf("%s", input);

	if (p->local->App1State == STOP && p->local->App2State == STOP) {
		if (!(strcmp("start1", input) == 0 || strcmp("start2", input) == 0 || strcmp("exit", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == STOP && p->local->App2State == RUN) {
		if (!(strcmp("start1", input) == 0 || strcmp("stop2", input) == 0 || strcmp("pause2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == STOP && p->local->App2State == PAUSE) {
		if (!(strcmp("start1", input) == 0 || strcmp("resume2", input) == 0 || strcmp("stop2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == RUN && p->local->App2State == STOP) {
		if (!(strcmp("stop1", input) == 0 || strcmp("pause1", input) == 0 || strcmp("start2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == RUN && p->local->App2State == RUN) {
		if (!(strcmp("stop1", input) == 0 || strcmp("pause1", input) == 0 || strcmp("stop2", input) == 0 || strcmp("pause2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == RUN && p->local->App2State == PAUSE) {
		if (!(strcmp("stop1", input) == 0 || strcmp("pause1", input) == 0 || strcmp("resume2", input) == 0 || strcmp("stop2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == PAUSE && p->local->App2State == STOP) {
		if (!(strcmp("resume1", input) == 0 || strcmp("stop1", input) == 0 || strcmp("start2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == PAUSE && p->local->App2State == RUN) {
		if (!(strcmp("resume1", input) == 0 || strcmp("stop1", input) == 0 || strcmp("stop2", input) == 0 || strcmp("pause2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	} else if (p->local->App1State == PAUSE && p->local->App2State == PAUSE) {
		if (!(strcmp("resume1", input) == 0 || strcmp("stop1", input) == 0 || strcmp("resume2", input) == 0 || strcmp("stop2", input) == 0)) {
			printf(WRONG_STATE_MESSAGE); fflush(stdout); return 0;
		}
	}


	if (strcmp("start1", input) == 0) { // Start MJPEG 1
		DAL_send_event((void *) EVENT_1, p);
		p->local->App1State = RUN;
	} else if (strcmp("stop1", input) == 0) { // Stop MJPEG 1
		DAL_send_event((void *) EVENT_2, p);
		p->local->App1State = STOP;
	} else if (strcmp("pause1", input) == 0) { // Pause MJPEG 1
		DAL_send_event((void *) EVENT_3, p);
		p->local->App1State = PAUSE;
	} else if (strcmp("resume1", input) == 0) { // Resume MJPEG 1
		DAL_send_event((void *) EVENT_4, p);
		p->local->App1State = RUN;

	} else if (strcmp("start2", input) == 0) { // Start MJPEG 2
		DAL_send_event((void *) EVENT_5, p);
		p->local->App2State = RUN;
	} else if (strcmp("stop2", input) == 0) { // Stop MJPEG 2
		DAL_send_event((void *) EVENT_6, p);
		p->local->App2State = STOP;
	} else if (strcmp("pause2", input) == 0) { // Pause MJPEG 2
		DAL_send_event((void *) EVENT_7, p);
		p->local->App2State = PAUSE;
	} else if (strcmp("resume2", input) == 0) { // Resume MJPEG 2
		DAL_send_event((void *) EVENT_8, p);
		p->local->App2State = RUN;

	} else if (strcmp("exit", input) == 0) { // Exit the application
		DAL_send_event((void *) EVENT_9, p);
		p->local->App1State = STOP;
		p->local->App2State = STOP;
		return 1;
	} else {
		printf("Invalid input\n");
		fflush(stdout);
	}

	return 0;
}

void ui_finish(DALProcess *p) {
	fflush(stdout);
}

