#ifndef UI_H
#define UI_H

#include <dal.h>
#include "global.h"

#define EVENT_1 "start_mjpeg1"
#define EVENT_2 "stop_mjpeg1"
#define EVENT_3 "pause_mjpeg1"
#define EVENT_4 "resume_mjpeg1"

#define EVENT_5 "start_mjpeg2"
#define EVENT_6 "stop_mjpeg2"
#define EVENT_7 "pause_mjpeg2"
#define EVENT_8 "resume_mjpeg2"

#define EVENT_9 "exit"

enum UI_STATES {STOP, RUN, PAUSE};
#define WRONG_STATE_MESSAGE "Input not allowed in this state\n"

typedef struct _local_states {
    int index;
    int len;
    enum UI_STATES App1State;
    enum UI_STATES App2State;
} Ui_State;

void ui_init(DALProcess *);
int ui_fire(DALProcess *);
void ui_finish(DALProcess *);

#endif
