#ifndef SOBELFILTER_H
#define SOBELFILTER_H

#include <dal.h>

#define PORT_in1  "in1"
#define PORT_out1 "out1"
#define PORT_out2 "out2"

typedef unsigned char TOKEN_in1_t;
typedef unsigned char TOKEN_out1_t;
typedef unsigned char TOKEN_out2_t;

#define TOKEN_in1_RATE	76800
#define TOKEN_out1_RATE	76800
#define BLOCK_out1_SIZE	1
#define BLOCK_out1_COUNT (TOKEN_out1_RATE / BLOCK_out1_SIZE)
#define TOKEN_out2_RATE	76800
#define BLOCK_out2_SIZE	1
#define BLOCK_out2_COUNT (TOKEN_out2_RATE / BLOCK_out2_SIZE)

typedef struct _local_states {
} Sobelfilter_State;

void sobelfilter_init(DALProcess *);
int sobelfilter_fire(DALProcess *);
void sobelfilter_finish(DALProcess *);

#endif
