#include "sobelfilter.h"
#include <math.h>

void sobelfilter_init(DALProcess *p)
{
}

int sobelfilter_fire(DALProcess *p)
{
	unsigned int width = 320;
	
	TOKEN_in1_t *rbuf = (TOKEN_in1_t *)DAL_read_begin(PORT_in1, sizeof(TOKEN_in1_t), TOKEN_in1_RATE, p);
	
	DAL_foreach(blk : PORT_out1)
	{
		unsigned int idx = (blk < width+1 || blk+width+1 > TOKEN_in1_RATE) ? width+1 : blk;

		int sobel_gradx = -(int)rbuf[idx-width-1] + rbuf[idx-width+1] - 2*(int)rbuf[idx-1] + 2*(int)rbuf[idx+1] - rbuf[idx+width-1] + rbuf[idx+width+1];
		sobel_gradx *= sobel_gradx;

		int sobel_grady = +(int)rbuf[idx-width-1] + 2*(int)rbuf[idx-width] + rbuf[idx-width+1] - rbuf[idx+width-1] - 2*(int)rbuf[idx+width] - rbuf[idx+width+1];
		sobel_grady *= sobel_grady;

		sobel_gradx += sobel_grady;

		unsigned char OutVal = (unsigned char)(sqrt((float)sobel_gradx));

		TOKEN_out1_t *wbuf1 = (TOKEN_out1_t *)DAL_write_begin(PORT_out1, sizeof(TOKEN_out1_t), TOKEN_out1_RATE, BLOCK_out1_SIZE, idx, p);
		TOKEN_out1_t *wbuf2 = (TOKEN_out2_t *)DAL_write_begin(PORT_out2, sizeof(TOKEN_out2_t), TOKEN_out2_RATE, BLOCK_out2_SIZE, idx, p);
		*wbuf1 = OutVal;
		*wbuf2 = OutVal;
		DAL_write_end(PORT_out1, wbuf1, p);
		DAL_write_end(PORT_out2, wbuf2, p);
	}

	DAL_read_end(PORT_in1, rbuf, p);

    return 0;
}

void sobelfilter_finish(DALProcess *p)
{
}
