#ifndef __MJPEG_H__
#define __MJPEG_H__

#include <stdarg.h>
#include <stdio.h>

#define SILENT    0
#define VERBOSE   1
#define INFO      2
#define VERBOSITY SILENT

void dbgprintf(int urgency, const char* format, ...);

static const int ZIGZAG_COEFFS[64] = {
     0,  1,  8, 16,  9,  2,  3, 10,
    17, 24, 32, 25, 18, 11,  4,  5,
    12, 19, 26, 33, 40, 48, 41, 34,
    27, 20, 13,  6,  7, 14, 21, 28,
    35, 42, 49, 56, 57, 50, 43, 36,
    29, 22, 15, 23, 30, 37, 44, 51,
    58, 59, 52, 45, 38, 31, 39, 46,
    53, 60, 61, 54, 47, 55, 62, 63
};

int* unZigZag(int* dest, int* src);

int* unquantify(int* block, unsigned char* qtable);

//minimum and maximum values a `signed int' can hold.
#define INT_MAX2  2147483647
#define INT_MIN2  (-INT_MAX2 - 1)

//useful constants
//ck = cos(k*pi/16) = s8-k = sin((8-k)*pi/16) times 1 << C_BITS and rounded
#define c0_1   16384
#define c0_s2  23170
#define c1_1   16069
#define c1_s2  22725
#define c2_1   15137
#define c2_s2  21407
#define c3_1   13623
#define c3_s2  19266
#define c4_1   11585
#define c4_s2  16384
#define c5_1   9102
#define c5_s2  12873
#define c6_1   6270
#define c6_s2  8867
#define c7_1   3196
#define c7_s2  4520
#define c8_1   0
#define c8_s2  0
#define sqrt2  c0_s2

// the number of bits of accuracy in all (signed) integer operations:
// may lie between 1 and 32 (bounds inclusive).
#define ARITH_BITS      16

// the minimum signed integer value that fits in ARITH_BITS:
#define ARITH_MIN       (-1 << (ARITH_BITS-1))
// the maximum signed integer value that fits in ARITH_BITS:
#define ARITH_MAX       (~ARITH_MIN)

// the number of bits coefficients are scaled up before 2-D IDCT:
#define S_BITS           3
// the number of bits in the fractional part of a fixed point constant:
#define C_BITS          14

#define SCALE(x, n)     ((x) << (n))

/* This version is vital in passing overall mean error test. */
inline int DESCALE(int x, int n);
inline int ADD(int x, int y);
inline int SUB(int x, int y);
inline int CMUL(int c, int x);

/* Rotate (x,y) over angle k*pi/16 (counter-clockwise) and scale with f. */
inline void rot(int f, int k, int x, int y, int *rx, int *ry);

/* Butterfly: but(a,b,x,y) = rot(sqrt(2),4,a,b,x,y) */
#define but(a,b,x,y) { x = SUB(a,b); y = ADD(a,b); }

inline void idct_1d(int *Y);
void IDCT(int input[8][8], unsigned char output[8][8]);

int intceil(int N, int D);
int intfloor(int N, int D);
int reformat(unsigned long s, int good);

#endif	// __MJPEG_H__
