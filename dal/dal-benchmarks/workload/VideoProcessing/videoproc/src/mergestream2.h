#ifndef MERGESTREAM_H
#define MERGESTREAM_H

#include <dal.h>
#include "jpeg.h"
#include <ctime>
#include <unistd.h>

#define PORT_IN1    "in1"
#define PORT_IN2    "in2"
#define PORT_IN3    "in3"

#define EVENT_1  "stop_video"

#ifdef VIEWER
#include "CImg.h"
using namespace cimg_library;
#endif


//local variables
typedef struct _local_states {
    unsigned int x_size;
    unsigned int y_size;
    int port_count;
    unsigned int ii;
    int num_iter; // nummber of fire
    unsigned char display_buffer[MAX_WIDTH * MAX_HEIGHT];
    timespec timestamp;
    double framerate;
    timespec startTime;
#ifdef VIEWER
    CImgDisplay *main_disp;
    CImgDisplay *main_disp2;
#endif
} Mergestream_State;

void mergestream2_init(DALProcess *);
int mergestream2_fire(DALProcess *);
void mergestream2_finish(DALProcess *);

#endif
