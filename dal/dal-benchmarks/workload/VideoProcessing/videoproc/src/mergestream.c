/************************************************************************
 * merge frames into stream                                             *
 ************************************************************************/

#include "mergestream.h"

void mergestream_init(DALProcess *p) {
    p->local->port_count = 0;
    p->local->num_iter = 0;
}

int mergestream_fire(DALProcess *p) {
    unsigned int i;
    unsigned int *x_size = &(p->local->x_size);
    unsigned int *y_size = &(p->local->y_size);
    CREATEPORTVAR(output_port1);
    CREATEPORTVAR(output_port2);

    dbgprintf(VERBOSE, "\tMERGESTREAM\tthread is alive !\n");

    // create port
    CREATEPORT(output_port1, PORT_IN1, 1, p->local->port_count, NUM_OF_PAR_PORTS);
    CREATEPORT(output_port2, PORT_IN2, 1, p->local->port_count, NUM_OF_PAR_PORTS);
    p->local->port_count = (p->local->port_count + 1) % NUM_OF_PAR_PORTS;

    // get the size
    DAL_read((void*)output_port2, x_size, sizeof(*x_size), p);
    DAL_read((void*)output_port2, y_size, sizeof(*y_size), p);

    //read picture row-by-row
    unsigned char *FrameBuffer = p->local->display_buffer;
	DAL_read((void*)output_port1, FrameBuffer, *x_size * *y_size, p);

	// Write
	DAL_write((void*)PORT_OUT3, x_size, sizeof(*x_size), p);
	DAL_write((void*)PORT_OUT3, y_size, sizeof(*y_size), p);
	DAL_write((void*)PORT_OUT1, FrameBuffer, *x_size * *y_size, p);
	DAL_write((void*)PORT_OUT2, FrameBuffer, *x_size * *y_size, p);

    return 0;
}

void mergestream_finish(DALProcess *p) {
    fflush(stdout);
}
