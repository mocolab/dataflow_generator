#include "opticalflow.h"

void opticalflow_init(DALProcess *p)
{
}

int opticalflow_fire(DALProcess *p)
{
	unsigned int width = 320;
	float threshold = 0.001;

	TOKEN_in1_t *rbuf1 = (TOKEN_in1_t *)DAL_read_begin(PORT_in1, sizeof(TOKEN_in1_t), TOKEN_in1_RATE, p);
	TOKEN_in2_t *rbuf2 = (TOKEN_in2_t *)DAL_read_begin(PORT_in2, sizeof(TOKEN_in2_t), TOKEN_in2_RATE, p);

	DAL_foreach(blk : PORT_out1)
	{
		unsigned int idx = (blk < width || blk+width > TOKEN_in1_RATE) ? width : blk;

		TOKEN_out1_t *wbuf = (TOKEN_out1_t *)DAL_write_begin(PORT_out1, sizeof(TOKEN_out1_t), TOKEN_out1_RATE, BLOCK_out1_SIZE, idx, p);
		*wbuf = rbuf1[idx] + rbuf2[idx];
		DAL_write_end(PORT_out1, wbuf, p);
	}

	DAL_read_end(PORT_in1, rbuf1, p);
	DAL_read_end(PORT_in2, rbuf2, p);

	return 0;
}

void opticalflow_finish(DALProcess *p)
{
}
