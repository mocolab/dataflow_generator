/************************************************************************
 * merge frames into stream                                             *
 ************************************************************************/

#include "mergestream2.h"
#include <ctime>

void mergestream2_init(DALProcess *p) {
    p->local->port_count = 0;
    p->local->num_iter = 0;
    p->local->framerate = 2;
    clock_gettime(CLOCK_MONOTONIC, &p->local->timestamp);
#ifdef VIEWER
    p->local->main_disp = new CImgDisplay(320, 240, "DAL MJPEG");
    p->local->main_disp2 = new CImgDisplay(320, 240, "DAL MJPEG2");
#endif
}

int mergestream2_fire(DALProcess *p) {
    timespec old_ts = p->local->timestamp;
    timespec new_ts;

    long constant_diff = 1e9/p->local->framerate;
    clock_gettime(CLOCK_MONOTONIC, &new_ts);

    long diff = (new_ts.tv_nsec-old_ts.tv_nsec+ 1e9*(new_ts.tv_sec-old_ts.tv_sec));
    double framerate = 1e9/diff;

    p->local->timestamp = new_ts;
    if (p->local->num_iter == 0) p->local->startTime = new_ts;

    unsigned int i;
    unsigned int *x_size = &(p->local->x_size);
    unsigned int *y_size = &(p->local->y_size);

    // get the size
    DAL_read((void*)PORT_IN3, x_size, sizeof(*x_size), p);
    DAL_read((void*)PORT_IN3, y_size, sizeof(*y_size), p);

    //read picture 1
    unsigned char *FrameBuffer = p->local->display_buffer;
    DAL_read((void*)PORT_IN1, FrameBuffer,*x_size * *y_size, p);

    printf("Decoded frame %d\n", p->local->num_iter);

#ifdef VIEWER
    //create image from display_buffer and display it
    CImg<unsigned char> img(p->local->display_buffer,
            p->local->x_size, p->local->y_size),
            visu(p->local->x_size, p->local->y_size, 1, 1, 0);
    p->local->main_disp->display(img);
    p->local->main_disp->paint();
#endif

    //read picture 2
    DAL_read((void*)PORT_IN2, FrameBuffer,*x_size * *y_size, p);

#ifdef VIEWER
    //create image from display_buffer and display it
    CImg<unsigned char> img2(p->local->display_buffer,
            p->local->x_size, p->local->y_size),
            visu2(p->local->x_size, p->local->y_size, 1, 1, 0);
    p->local->main_disp2->display(img2);
    p->local->main_disp2->paint();
#endif

    //end this process
    p->local->num_iter++;
    if (p->local->num_iter == MAX_NUMBER_OF_FRAMES) {
    	timespec now;
        clock_gettime(CLOCK_MONOTONIC, &now);
    	printf("elapsed time: %f s\n", (now.tv_sec - p->local->startTime.tv_sec) + (now.tv_nsec - p->local->startTime.tv_nsec) / 1e9);

        DAL_send_event(EVENT_1, p); 
        return 1;
    }

    return 0;
}

void mergestream2_finish(DALProcess *p) {

#ifdef VIEWER
    p->local->main_disp->close();
    delete p->local->main_disp;
#endif

#ifdef VIEWER
    p->local->main_disp2->close();
    delete p->local->main_disp2;
#endif

    fflush(stdout);
}
