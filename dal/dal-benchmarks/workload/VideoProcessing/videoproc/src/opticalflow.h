#ifndef OPTICALFLOW_H
#define OPTICALFLOW_H

#include <dal.h>

#define PORT_in1  "in1"
#define PORT_in2  "in2"
#define PORT_out1 "out1"

typedef unsigned char TOKEN_in1_t;
typedef unsigned char TOKEN_in2_t;
typedef unsigned char TOKEN_out1_t;

#define TOKEN_in1_RATE	76800
#define TOKEN_in2_RATE	76800
#define TOKEN_out1_RATE	76800
#define BLOCK_out1_SIZE	1
#define BLOCK_out1_COUNT (TOKEN_out1_RATE / BLOCK_out1_SIZE)

typedef struct _local_states {
} Opticalflow_State;

void opticalflow_init(DALProcess *);
int opticalflow_fire(DALProcess *);
void opticalflow_finish(DALProcess *);

#endif
