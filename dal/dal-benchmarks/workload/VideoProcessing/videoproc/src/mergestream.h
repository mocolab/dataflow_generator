#ifndef MERGESTREAM_H
#define MERGESTREAM_H

#include <dal.h>
#include "jpeg.h"

#define PORT_IN1   "in1"
#define PORT_IN2   "in2"
#define PORT_OUT1   "out1"
#define PORT_OUT2   "out2"
#define PORT_OUT3   "out3"

#ifdef VIEWER
#include "CImg.h"
using namespace cimg_library;
#endif


//local variables
typedef struct _local_states {
    unsigned int x_size;
    unsigned int y_size;
    int port_count;
    unsigned int ii;
    int num_iter; // nummber of fire
    unsigned char display_buffer[MAX_WIDTH * MAX_HEIGHT];
} Mergestream_State;

void mergestream_init(DALProcess *);
int mergestream_fire(DALProcess *);
void mergestream_finish(DALProcess *);

#endif
