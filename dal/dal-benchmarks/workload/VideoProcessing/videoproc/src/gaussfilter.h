#ifndef GAUSSFILTER_H
#define GAUSSFILTER_H

#include <dal.h>

#define PORT_in1  "in1"
#define PORT_out1 "out1"

typedef unsigned char TOKEN_in1_t;
typedef unsigned char TOKEN_out1_t;

#define TOKEN_in1_RATE	76800
#define TOKEN_out1_RATE	76800
#define BLOCK_out1_SIZE	1
#define BLOCK_out1_COUNT (TOKEN_out1_RATE / BLOCK_out1_SIZE)

typedef struct _local_states {
} Gaussfilter_State;

void gaussfilter_init(DALProcess *);
int gaussfilter_fire(DALProcess *);
void gaussfilter_finish(DALProcess *);

#endif
