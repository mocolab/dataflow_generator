#ifndef SQUARE_H
#define SQUARE_H

#include <dal.h>
#include "global.h"

#define PORT_IN  1
#define PORT_OUT 2
#define EVENT_1 "s_done"

typedef struct _local_states {
    int index;
    int len;
} Square_State;

void square_init(DALProcess *);
int square_fire(DALProcess *);
void square_finish(DALProcess *);

#endif
