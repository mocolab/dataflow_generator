#include <stdio.h>

#include "square.h"

void square_init(DALProcess *p) {
    p->local->index = 0;
    p->local->len = LENGTH;
}

int square_fire(DALProcess *p) {
    float i;

    if (p->local->index < p->local->len) {
        DAL_read((void*)PORT_IN, &i, sizeof(float), p);
        i = i*i;
        DAL_write((void*)PORT_OUT, &i, sizeof(float), p);
        p->local->index++;
    }

    if (p->local->index >= p->local->len) {
    	DAL_send_event((void *)EVENT_1, p);
    	return(1);
    }

    return 0;
}

void square_finish(DALProcess *p) {
    fflush(stdout);
}
