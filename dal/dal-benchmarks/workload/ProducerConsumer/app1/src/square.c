#include "square.h"

void square_init(DALProcess *p)
{
}

int square_fire(DALProcess *p)
{
	TOKEN_in1_t *rbuf = (TOKEN_in1_t *)DAL_read_begin(PORT_in1, sizeof(TOKEN_in1_t), TOKEN_in1_RATE, p);

	DAL_foreach (blk : PORT_out1)
	{
		TOKEN_out1_t *wbuf = (TOKEN_out1_t *)DAL_write_begin(PORT_out1, sizeof(TOKEN_out1_t), TOKEN_out1_RATE, BLOCK_out1_SIZE, blk, p);
		*wbuf = rbuf[blk] * rbuf[blk];
		DAL_write_end(PORT_out1, wbuf, p);
	}

	DAL_read_end(PORT_in1, rbuf, p);

    return 0;
}

void square_finish(DALProcess *p)
{
}
