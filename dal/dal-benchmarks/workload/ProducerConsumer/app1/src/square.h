#ifndef SQUARE_H
#define SQUARE_H

#include <dal.h>

#define PORT_in1  "in1"
#define PORT_out1 "out1"

typedef float TOKEN_in1_t;
typedef float TOKEN_out1_t;

#define TOKEN_in1_RATE	256
#define TOKEN_out1_RATE	256
#define BLOCK_out1_SIZE	1
#define BLOCK_out1_COUNT (TOKEN_out1_RATE / BLOCK_out1_SIZE)

typedef struct _local_states {
} Square_State;

void square_init(DALProcess *);
int square_fire(DALProcess *);
void square_finish(DALProcess *);

#endif
