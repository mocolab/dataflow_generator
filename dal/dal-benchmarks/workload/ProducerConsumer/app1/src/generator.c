#include <stdio.h>
#include <string.h>

#include "generator.h"

// initialization function
void generator_init(DALProcess *p) {
    p->local->index = 0;
    p->local->len = LENGTH;
}

int generator_fire(DALProcess *p) {

    if (p->local->index < p->local->len) {
    	float *wbuf = (float *)DAL_write_begin(PORT_OUT, sizeof(float), 1, 1, 0, p);
    	*wbuf = (float)p->local->index;
    	DAL_write_end(PORT_OUT, wbuf, p);

        //float x = (float)p->local->index;

        //DAL_write((void*)PORT_OUT, &(x), sizeof(float), p);
        p->local->index++;
    }

    if (p->local->index >= p->local->len) {
    	return(1);
    }

    return 0;
}

void generator_finish(DALProcess *p) {
    fflush(stdout);
}

