#ifndef SUMMATION_H
#define SUMMATION_H

#include <dal.h>
#include "raylib.h"

#define PORT_IN  0

#define EVENT_DONE "stop_state1"

typedef struct _local_states {
    int deCount;
    int pixelCount;
    int sampleCount;
    Vec pixels[NUM_ROW*NUM_COL];
    int count;

   long comm_sum;
   struct timespec start_sum, end_sum;
} Summation_State;

void summation_init(DALProcess *);
int summation_fire(DALProcess *);
int summation_finish(DALProcess *);

#endif
