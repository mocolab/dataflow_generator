#include "intersect.h"
#include <time.h>

Vec radiance(const Ray &r, int depth, unsigned short *Xi){

   if (depth > MAX_REC_DEPTH) return Vec();

   double t;                               // distance to intersection
   int id=0;                               // id of intersected object
   if (!intersect(r, t, id)) return Vec(); // if miss, return black
   const Sphere &obj = spheres[id];        // the hit object

   Vec x = r.o + r.d * t;		   // ray intersection point
   Vec n = (x - obj.p).norm();		   // sphere normal
   Vec nl = n.dot(r.d) < 0 ? n : n * -1;   // properly oriented surface normal
   Vec f = obj.c;			   // object color (BDRF modulator)

   double p = f.x > f.y && f.x > f.z ? f.x : f.y > f.z ? f.y : f.z; // max component (r,g,b)

   if (++depth > 5) 
   {
      if (erand48(Xi) < p) 
      {
         f = f * (1/p); 
      }
      else 
      {
         return obj.e; //R.R.
      }
   }

   if (obj.refl == DIFF)	                        // Ideal DIFFUSE reflection
   {
      double r1 = 2 * M_PI * erand48(Xi);
      double r2 = erand48(Xi);
      double r2s = sqrt(r2);
      Vec w = nl;
      Vec u = ((fabs(w.x) > .1 ? Vec(0,1) : Vec(1)) % w).norm();
      Vec v = w % u;
      Vec d = (u * cos(r1) * r2s + v * sin(r1) * r2s + w * sqrt(1-r2)).norm();

      return obj.e + f.mult(radiance(Ray(x,d), depth, Xi));
   }
   else if (obj.refl == SPEC)                    // Ideal SPECULAR reflection
   {
      return obj.e + f.mult(radiance(Ray(x, r.d - n * 2 * n.dot(r.d)), depth, Xi));
   }

   Ray reflRay(x, r.d - n * 2 * n.dot(r.d));     // Ideal dielectric REFRACTION
   bool into = n.dot(nl) > 0;                    // Ray from outside going in?
   double nc = 1;
   double nt = 1.5;
   double nnt = into ? nc/nt : nt/nc;
   double ddn = r.d.dot(nl);
   double cos2t;

   if ((cos2t = 1 - nnt*nnt*(1 - ddn*ddn)) < 0) // Total internal reflection
   {
      return obj.e + f.mult(radiance(reflRay, depth, Xi));
   }

   Vec tdir = (r.d*nnt - n*((into ? 1 : -1) * (ddn*nnt + sqrt(cos2t)))).norm();
   double a = nt - nc;
   double b = nt + nc;
   double R0 = a * a / (b * b);
   double c = 1 - (into ? -ddn : tdir.dot(n));
   double Re = R0 + (1 - R0) * c * c * c * c * c;
   double Tr = 1 - Re;

   return obj.e + f.mult(radiance(reflRay,depth,Xi)*Re + radiance(Ray(x,tdir),depth,Xi)*Tr);
}

void intersect_init(DALProcess *p) {
   p->local->count=0;
   p->local->maxDepth = 0;
   p->local->comm_int = 0;
}

int intersect_finish(DALProcess *p) {
}
int intersect_fire(DALProcess *p) {

   Ray inRay;
   unsigned short Xi[3];
   long diff;
   Vec r;

   //if (COMM_MEASUREMENTS)
   //   clock_gettime(CLOCK_THREAD_CPUTIME_ID, &p->local->start_int);
   DAL_read((void*)PORT_IN, &inRay, sizeof(Ray), p);
   DAL_read((void*)PORT_IN, &Xi, sizeof(Xi), p);
   //if (COMM_MEASUREMENTS)   
   //   clock_gettime(CLOCK_THREAD_CPUTIME_ID, &p->local->end_int);
   //if (COMM_MEASUREMENTS)
   //{
//      diff = (p->local->end_int.tv_sec*1000000 + p->local->end_int.tv_nsec/1000)-(p->local->start_int.tv_sec*1000000 + p->local->start_int.tv_nsec/1000);
//      if (diff < 0)
//         printf("Start: %ld:%ld\nEnd: %ld:%ld\nDiff: %ld\n", p->local->start_int.tv_sec, p->local->start_int.tv_nsec, p->local->end_int.tv_sec, p->local->end_int.tv_nsec, diff);
//      p->local->comm_int = p->local->comm_int + diff;
   //}

   r = radiance(inRay, 0, Xi);

   //if (COMM_MEASUREMENTS)
   //   clock_gettime(CLOCK_THREAD_CPUTIME_ID, &p->local->start_int);
   DAL_write((void*)PORT_OUT, &r, sizeof(Vec), p);
   //if (COMM_MEASUREMENTS)
   //{
    //  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &p->local->end_int);
    //  diff = (p->local->end_int.tv_sec*1000000 + p->local->end_int.tv_nsec/1000)-(p->local->start_int.tv_sec*1000000 + p->local->start_int.tv_nsec/1000);
    //  if (diff < 0)
    //     printf("Start: %ld:%ld\nEnd: %ld:%ld\nDiff: %ld\n", p->local->start_int.tv_sec, p->local->start_int.tv_nsec, p->local->end_int.tv_sec, p->local->end_int.tv_nsec, diff);
    //  p->local->comm_int = p->local->comm_int + diff;
   //}

   if(++p->local->count == TOTAL_COUNT/NUM_REP)
   {
      //if (COMM_MEASUREMENTS)
      //   printf("Intersect Communication Time: %ld us\n", p->local->comm_int);
   }

   return 0;
}

