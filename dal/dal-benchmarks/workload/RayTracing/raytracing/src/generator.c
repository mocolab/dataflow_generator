#include "generator.h"
#include <time.h>

void generator_init(DALProcess *p) {
   p->local->comm_gen = 0;
}
int generator_finish(DALProcess *p) {

}
int generator_fire(DALProcess *p) {
 
   Ray cam(Vec(50,52,295.6), Vec(0,-0.042612,-1).norm());
   Vec cx=Vec(NUM_ROW*.5135/NUM_COL), cy=(cx%cam.d).norm()*.5135, r;

   CREATEPORTVAR(port);

   int index=0;

   for (int y=0; y<NUM_COL; y++)
   {
      for (unsigned short x=0, Xi[3]={0,0,y*y*y}; x<NUM_ROW; x++)
         for (int sy=0, i=(NUM_COL-y-1)*NUM_ROW+x; sy<NUM_SUB_COL; sy++)
            for (int sx=0; sx<NUM_SUB_ROW; sx++, r=Vec())
            {
               for (int s=0; s<NUM_SAM; s++)
               {
                  double r1=2*erand48(Xi), dx=r1<1 ? sqrt(r1)-1: 1-sqrt(2-r1);
                  double r2=2*erand48(Xi), dy=r2<1 ? sqrt(r2)-1: 1-sqrt(2-r2);
                  Vec d = cx*( ( (sx+.5 + dx)/2 + x)/NUM_ROW - .5) +
                     cy*( ( (sy+.5 + dy)/2 + y)/NUM_COL - .5) + cam.d;
                  Ray ray(cam.o+d*140 , d.norm());

                  CREATEPORT(port, PORT_OUT, 1, index, NUM_REP);
                  DAL_write((void*)port, &(ray), sizeof(Ray), p);
                  DAL_write((void*)port, &(Xi), sizeof(Xi), p);
                  
                  index = (index+1)%NUM_REP;
               }
            }
   }

   return 1;
}
