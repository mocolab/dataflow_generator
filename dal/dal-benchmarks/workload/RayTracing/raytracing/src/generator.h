#ifndef GENERATOR_H
#define GENERATOR_H

#include <dal.h>
#include "raylib.h"

#define PORT_OUT 0

typedef struct _local_states {
   long comm_gen;
   struct timespec start_gen, end_gen;
} Generator_State;

void generator_init(DALProcess *);
int generator_fire(DALProcess *);
int generator_finish(DALProcess *);

#endif
