#ifndef INTERSECT_H
#define INTERSECT_H

#include <dal.h>
#include "raylib.h"

#define PORT_IN    0
#define PORT_OUT   1

typedef struct _local_states {
   int maxDepth;
   int count;
   long comm_int;
   struct timespec start_int, end_int;
} Intersect_State;

void intersect_init(DALProcess *);
int intersect_fire(DALProcess *);
int intersect_finish(DALProcess *);

#endif
