#include "summation.h"
#include <time.h>

void writeImage(DALProcess *p) {
   FILE *f = fopen("image.ppm", "w");
   fprintf(f, "P3\n%d %d\n%d\n", NUM_ROW, NUM_COL, 255);
    
   for (int y = NUM_COL - 1; y >= 0; y--)
   {
      for (int x = 0; x < NUM_ROW; ++x)
      {
         int i = y * NUM_ROW + x;

         fprintf(f, "%d %d %d ", toInt(p->local->pixels[i].x), 
            toInt(p->local->pixels[i].y), toInt(p->local->pixels[i].z));
      }
   }

   printf("Success: Image written to image.ppm!\n");
}

int summation_finish(DALProcess *p) {
}
void summation_init(DALProcess *p) {

   p->local->comm_sum = 0;
   p->local->count = 0;

   for (int i=0; i<(NUM_ROW * NUM_COL); ++i)
      p->local->pixels[i] = Vec();
}

int summation_fire(DALProcess *p) {

   Vec a, result;
   int index;

   CREATEPORTVAR(port);
   index = p->local->count % NUM_REP;
   CREATEPORT(port, PORT_IN, 1, index, NUM_REP);

   DAL_read((void*)port, &a, sizeof(Vec), p);

   a = a * (1./NUM_SAM);
   result = Vec(clamp(a.x), clamp(a.y), clamp(a.z)) * .25;

   int currentPixel = (int) ((p->local->count)/(NUM_SUB_ROW*NUM_SUB_COL*NUM_SAM));

   p->local->pixels[currentPixel] = p->local->pixels[currentPixel] + result;

   ++p->local->count;

   if(p->local->count == TOTAL_COUNT)
   {
      writeImage(p);

      DAL_send_event((void *) EVENT_DONE, p);
      return 1;
   }

   return 0;
}
