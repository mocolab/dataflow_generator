#ifndef CONSUMER_H
#define CONSUMER_H

#include <dal.h>
#include "global.h"

#define PORT_IN 1
#define EVENT_DONE "stop_state1"

typedef struct _local_states {
    char name[10];
    int index;
    int len;
} Consumer_State;

void consumer_init(DALProcess *);
int consumer_fire(DALProcess *);
void consumer_finish(DALProcess *);

int case1484147150(void);
int case1484147152(void);
int case1484147153(void);
int case1484147157(void);
int case1484147159(void);
#endif
