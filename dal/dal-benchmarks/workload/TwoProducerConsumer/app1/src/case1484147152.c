/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      3357886799
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   int8_t  f0;
   int16_t  f1;
   uint32_t  f2;
   const int32_t  f3;
   uint16_t  f4;
   const uint16_t  f5;
   int16_t  f6;
   uint64_t  f7;
   const uint64_t  f8;
};

#pragma pack(push)
#pragma pack(1)
struct S1 {
   unsigned f0 : 12;
   uint32_t  f1;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int32_t g_2[4] = {0L,0L,0L,0L};
static struct S1 g_67 = {62,0UL};
static int32_t g_69 = (-1L);
static uint8_t g_88 = 1UL;
static int16_t g_90 = 0x604CL;
static uint32_t g_101 = 0UL;
static int32_t g_104 = 0x22E340D7L;
static uint64_t g_109 = 0x4C6B90ED9ABD1A96LL;
static uint64_t *g_108[3][2] = {{&g_109,&g_109},{&g_109,&g_109},{&g_109,&g_109}};
static uint64_t **g_107 = &g_108[1][1];
static uint32_t g_112 = 4294967295UL;
static struct S0 g_117 = {1L,0xC5F8L,0x4B9EA69DL,0xFC0A4C35L,0xD1F7L,0x3E9DL,0x8425L,0xBBF2B79F9BF173AALL,0UL};
static uint32_t g_127 = 0UL;
static int64_t g_146 = 0x79B21912DA420BE3LL;
static int32_t *g_156[10][8] = {{&g_69,&g_69,&g_104,&g_69,&g_69,&g_104,&g_69,&g_69},{&g_2[3],&g_69,&g_2[3],&g_2[3],&g_69,&g_2[3],&g_2[3],&g_69},{&g_69,&g_2[3],&g_2[3],&g_69,&g_2[3],&g_2[3],&g_69,&g_2[3]},{&g_69,&g_69,&g_104,&g_69,&g_69,&g_104,&g_69,&g_69},{&g_2[3],&g_69,&g_2[3],&g_2[3],&g_2[3],&g_104,&g_104,&g_2[3]},{&g_2[3],&g_104,&g_104,&g_2[3],&g_104,&g_104,&g_2[3],&g_104},{&g_2[3],&g_2[3],&g_69,&g_2[3],&g_2[3],&g_69,&g_2[3],&g_2[3]},{&g_104,&g_2[3],&g_104,&g_104,&g_2[3],&g_104,&g_104,&g_2[3]},{&g_2[3],&g_104,&g_104,&g_2[3],&g_104,&g_104,&g_2[3],&g_104},{&g_2[3],&g_2[3],&g_69,&g_2[3],&g_2[3],&g_69,&g_2[3],&g_2[3]}};
static int64_t g_179 = 9L;
static int8_t *g_189[7][5] = {{&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0},{&g_117.f0,(void*)0,&g_117.f0,(void*)0,&g_117.f0},{&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0},{&g_117.f0,(void*)0,&g_117.f0,&g_117.f0,&g_117.f0},{&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0},{&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0},{&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0,&g_117.f0}};
static int8_t **g_188 = &g_189[4][4];
static uint64_t g_243 = 0xA0D85813E30775B5LL;
static int64_t g_305 = (-1L);
static int16_t g_311 = 0x9F77L;
static uint16_t *g_375 = &g_117.f4;
static int16_t g_386 = (-1L);
static int64_t g_419 = 0x64A319C6820D95D4LL;
static uint16_t g_455[8] = {0x7E95L,0x7E95L,0x7E95L,0x7E95L,0x7E95L,0x7E95L,0x7E95L,0x7E95L};
static struct S0 **g_624 = (void*)0;
static uint8_t *g_660 = &g_88;
static int64_t g_673 = 0x742E3045BCB36226LL;
static int32_t g_713 = (-10L);
static int32_t *g_715 = &g_713;
static int32_t **g_714 = &g_715;
static uint8_t g_727 = 0x2CL;
static int8_t ***g_750 = &g_188;
static int8_t ****g_749 = &g_750;
static int32_t g_752 = 1L;
static struct S1 *g_814 = &g_67;
static uint16_t * const *g_816 = &g_375;
static uint16_t * const **g_815 = &g_816;
static int64_t g_840 = 0xF16A0010824D6244LL;
static int32_t g_851 = 0xE12E63FCL;
static struct S1 g_852[8] = {{38,0x4D143CB4L},{38,0x4D143CB4L},{52,18446744073709551615UL},{38,0x4D143CB4L},{38,0x4D143CB4L},{52,18446744073709551615UL},{38,0x4D143CB4L},{38,0x4D143CB4L}};
static int64_t *g_886 = &g_305;
static int64_t **g_885 = &g_886;
static uint32_t g_895[6][10] = {{0x2F04970BL,0x7595F99EL,0x221FE457L,0x221FE457L,0x7595F99EL,0x2F04970BL,0x6185F11FL,0x7595F99EL,0x6185F11FL,0x2F04970BL},{0xA3FC5320L,0x7595F99EL,8UL,0x7595F99EL,0xA3FC5320L,8UL,0UL,0UL,8UL,0xA3FC5320L},{0xA3FC5320L,0x6185F11FL,0x6185F11FL,0xA3FC5320L,0x221FE457L,0x2F04970BL,0xA3FC5320L,0x2F04970BL,0x221FE457L,0xA3FC5320L},{0x2F04970BL,0xA3FC5320L,0x2F04970BL,0x221FE457L,0xA3FC5320L,0x6185F11FL,0x6185F11FL,0xA3FC5320L,0x221FE457L,0x2F04970BL},{0UL,0UL,8UL,0xA3FC5320L,0x7595F99EL,8UL,0x7595F99EL,0xA3FC5320L,8UL,0UL},{0x7595F99EL,0x6185F11FL,0x2F04970BL,0x7595F99EL,0x221FE457L,0x221FE457L,0x7595F99EL,0x2F04970BL,0x6185F11FL,0x7595F99EL}};
static uint8_t **g_1077 = &g_660;
static uint8_t ***g_1076 = &g_1077;
static int32_t g_1175 = 0xF6701082L;
static int64_t g_1204[10] = {(-7L),0xAB7783E37B38F240LL,(-7L),0xAB7783E37B38F240LL,(-7L),0xAB7783E37B38F240LL,(-7L),0xAB7783E37B38F240LL,(-7L),0xAB7783E37B38F240LL};
static int16_t * const g_1224 = &g_117.f1;
static int16_t * const *g_1223 = &g_1224;
static int16_t g_1336[1][8] = {{(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)}};
static struct S0 g_1416 = {0L,0x89C2L,0x63D1240AL,0xF90A3273L,0xF68FL,65528UL,0x05D0L,0xDF670325D6D8F00ELL,18446744073709551607UL};
static struct S0 *g_1415 = &g_1416;
static uint16_t g_1433 = 0xBEA2L;
static const struct S1 g_1438 = {59,0x812C1B1BL};
static int32_t g_1439[2][3][10] = {{{(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)},{(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)},{(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)}},{{(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)},{(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)},{(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)}}};
static struct S0 g_1461 = {-3L,1L,0x65DF7818L,0xAEACF08FL,1UL,0x0368L,0xAEFCL,0xBF2FF96352765B29LL,0xC552D6561CD7FBA2LL};
static uint16_t **g_1529[5] = {&g_375,&g_375,&g_375,&g_375,&g_375};
static uint16_t ***g_1528[1] = {&g_1529[2]};
static uint16_t ***g_1533 = &g_1529[2];


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int32_t  func_9(uint8_t  p_10, int8_t  p_11);
static int64_t  func_16(const int64_t  p_17, const uint16_t  p_18, const struct S1  p_19);
static uint64_t  func_23(const int32_t  p_24, uint64_t  p_25, uint64_t  p_26);
static uint8_t  func_29(int16_t  p_30, int8_t  p_31);
static int16_t  func_38(int8_t  p_39, uint16_t  p_40, struct S1  p_41);
static uint16_t  func_44(uint64_t  p_45, int8_t  p_46, struct S1  p_47, uint32_t  p_48);
static int64_t  func_55(uint8_t  p_56, struct S1  p_57, const uint64_t  p_58, const uint32_t  p_59);
static uint8_t  func_60(uint16_t  p_61, int32_t  p_62, int64_t  p_63, uint16_t  p_64);
static int32_t  func_65(struct S1  p_66);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_1077 g_660 g_88 g_895 g_107 g_108 g_109 g_816 g_375 g_117.f4 g_750 g_188 g_189 g_886 g_305 g_885 g_69 g_673 g_1076 g_727 g_117.f0 g_1433 g_1223 g_1224 g_117.f1 g_179 g_243 g_814 g_67 g_1416.f4 g_1439
 * writes: g_2 g_895 g_117.f0 g_69 g_673 g_156 g_727 g_1433 g_305 g_179
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint64_t l_82 = 0x2EA1289665FEC6B0LL;
    int32_t l_86 = (-1L);
    struct S1 l_696 = {5,1UL};
    uint8_t l_1522 = 255UL;
    uint64_t l_1524[1];
    struct S0 **l_1550 = &g_1415;
    const int32_t l_1559[2][8] = {{0x35F04092L,0x30A41AA7L,0x35F04092L,0x30A41AA7L,0x35F04092L,0x30A41AA7L,0x35F04092L,0x30A41AA7L},{0x35F04092L,0x30A41AA7L,0x35F04092L,0x30A41AA7L,0x35F04092L,0x30A41AA7L,0x35F04092L,0x30A41AA7L}};
    int32_t l_1576 = 5L;
    int64_t l_1577 = 0x56D16FE0627F7B80LL;
    uint16_t l_1627 = 0xED33L;
    int32_t l_1634 = 0x62102095L;
    int32_t l_1636[4] = {0x89ACB22BL,0x89ACB22BL,0x89ACB22BL,0x89ACB22BL};
    int8_t l_1637 = 0xAEL;
    int64_t l_1638 = (-1L);
    uint32_t *l_1646 = (void*)0;
    uint32_t *l_1647 = &g_895[5][7];
    int8_t *l_1650 = &g_117.f0;
    int16_t *l_1651[8] = {&g_311,(void*)0,&g_311,(void*)0,&g_311,(void*)0,&g_311,(void*)0};
    int16_t *l_1653 = &g_117.f1;
    int32_t l_1705 = 0xFE1A20C2L;
    uint32_t l_1706 = 4UL;
    uint64_t l_1716 = 18446744073709551609UL;
    uint16_t *l_1721 = &g_1433;
    int32_t *l_1727 = &l_86;
    int8_t l_1755 = 0x32L;
    int16_t l_1761 = 2L;
    int i, j;
    for (i = 0; i < 1; i++)
        l_1524[i] = 0xB128092992D3D9E5LL;
    for (g_2[3] = 29; (g_2[3] >= 18); g_2[3] = safe_sub_func_int32_t_s_s(g_2[3], 1))
    { /* block id: 3 */
        uint16_t l_83 = 0x9428L;
        uint64_t *l_84 = (void*)0;
        uint64_t *l_85[8] = {&l_82,&l_82,&l_82,&l_82,&l_82,&l_82,&l_82,&l_82};
        uint8_t *l_87 = &g_88;
        int16_t *l_89 = &g_90;
        int32_t *l_1509 = &g_1439[0][2][7];
        const int32_t l_1523 = 1L;
        int32_t *l_1525 = &g_104;
        uint16_t ***l_1531 = &g_1529[2];
        int16_t **l_1547 = &l_89;
        const int32_t l_1555 = (-1L);
        int32_t l_1574 = 0x34D54124L;
        int8_t ****l_1604 = &g_750;
        int32_t l_1625 = 0x31630B6BL;
        int32_t *l_1631 = &g_1439[0][1][4];
        int32_t *l_1632 = (void*)0;
        int32_t *l_1633[1];
        int8_t l_1635 = 0x84L;
        uint32_t l_1639[8];
        int i;
        for (i = 0; i < 1; i++)
            l_1633[i] = &g_1439[1][0][6];
        for (i = 0; i < 8; i++)
            l_1639[i] = 0UL;
    }
    if (((safe_mod_func_int64_t_s_s(l_86, ((safe_lshift_func_int8_t_s_u(((*l_1650) = (((l_1634 | (**g_1077)) < ((*l_1647)++)) > (**g_107))), (l_1651[1] == (((&l_1637 == ((l_1577 >= (~(**g_816))) , (**g_750))) > (**g_107)) , l_1653)))) && (*g_660)))) | l_1559[0][5]))
    { /* block id: 801 */
        int32_t l_1658[8][10][3] = {{{0xE9471816L,0x22C94EA3L,0xE9471816L},{(-9L),0x06ABA797L,1L},{1L,0x22C94EA3L,0x885DCCAAL},{0x69D01750L,0x00C15E08L,1L},{0xE9471816L,0x0C1A514CL,0xE9471816L},{0x69D01750L,0x06ABA797L,(-1L)},{1L,0x0C1A514CL,0x885DCCAAL},{(-9L),0x00C15E08L,(-1L)},{0xE9471816L,0x22C94EA3L,0xE9471816L},{(-9L),0x06ABA797L,1L}},{{1L,0x22C94EA3L,0x885DCCAAL},{0x69D01750L,0x00C15E08L,1L},{0xE9471816L,0x0C1A514CL,0xE9471816L},{0x69D01750L,0x06ABA797L,(-1L)},{1L,0x0C1A514CL,0x885DCCAAL},{(-9L),0x00C15E08L,(-1L)},{0xE9471816L,0x22C94EA3L,0xE9471816L},{(-9L),0x06ABA797L,1L},{1L,0x22C94EA3L,0x885DCCAAL},{0x69D01750L,0x00C15E08L,1L}},{{0xE9471816L,0x0C1A514CL,0xE9471816L},{0x69D01750L,0x06ABA797L,(-1L)},{1L,0x0C1A514CL,0x885DCCAAL},{(-9L),0x00C15E08L,(-1L)},{0xE9471816L,0x22C94EA3L,0xE9471816L},{(-9L),0x06ABA797L,1L},{1L,0x22C94EA3L,0x885DCCAAL},{0x69D01750L,0x00C15E08L,1L},{0xE9471816L,0x0C1A514CL,0xE9471816L},{0x69D01750L,0x06ABA797L,(-1L)}},{{1L,0x0C1A514CL,0x885DCCAAL},{(-9L),0x00C15E08L,(-1L)},{0xE9471816L,0x22C94EA3L,0xE9471816L},{(-9L),0x06ABA797L,1L},{1L,0x22C94EA3L,0x885DCCAAL},{0x69D01750L,(-9L),1L},{0x05C100FCL,0L,0x05C100FCL},{(-1L),1L,0xA75308F3L},{0x97A32040L,0L,0xE2954D1CL},{(-1L),(-9L),0xA75308F3L}},{{0x05C100FCL,1L,0x05C100FCL},{(-1L),1L,1L},{0x97A32040L,1L,0xE2954D1CL},{(-1L),(-9L),1L},{0x05C100FCL,0L,0x05C100FCL},{(-1L),1L,0xA75308F3L},{0x97A32040L,0L,0xE2954D1CL},{(-1L),(-9L),0xA75308F3L},{0x05C100FCL,1L,0x05C100FCL},{(-1L),1L,1L}},{{0x97A32040L,1L,0xE2954D1CL},{(-1L),(-9L),1L},{0x05C100FCL,0L,0x05C100FCL},{(-1L),1L,0xA75308F3L},{0x97A32040L,0L,0xE2954D1CL},{(-1L),(-9L),0xA75308F3L},{0x05C100FCL,1L,0x05C100FCL},{(-1L),1L,1L},{0x97A32040L,1L,0xE2954D1CL},{(-1L),(-9L),1L}},{{0x05C100FCL,0L,0x05C100FCL},{(-1L),1L,0xA75308F3L},{0x97A32040L,0L,0xE2954D1CL},{(-1L),(-9L),0xA75308F3L},{0x05C100FCL,1L,0x05C100FCL},{(-1L),1L,1L},{0x97A32040L,1L,0xE2954D1CL},{(-1L),(-9L),1L},{0x05C100FCL,0L,0x05C100FCL},{(-1L),1L,0xA75308F3L}},{{0x97A32040L,0L,0xE2954D1CL},{(-1L),(-9L),0xA75308F3L},{0x05C100FCL,1L,0x05C100FCL},{(-1L),1L,1L},{0x97A32040L,1L,0xE2954D1CL},{(-1L),(-9L),1L},{0x05C100FCL,0L,0x05C100FCL},{(-1L),1L,0xA75308F3L},{0x97A32040L,0L,0xE2954D1CL},{(-1L),(-9L),0xA75308F3L}}};
        const int64_t l_1672[8] = {0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL,0xA4AD046F39ACEEE3LL};
        int32_t *l_1673 = &g_69;
        int32_t l_1677 = 0x8B454774L;
        int32_t l_1678 = 2L;
        int32_t l_1679 = 0x5D45DA61L;
        uint32_t l_1683 = 0x76B478B6L;
        uint32_t l_1687 = 7UL;
        int i, j, k;
        l_1634 |= ((*l_1673) &= ((l_1576 != ((1L < (safe_mul_func_uint16_t_u_u((((safe_mul_func_int16_t_s_s(l_1658[3][3][0], ((((safe_rshift_func_uint16_t_u_u(((safe_unary_minus_func_uint64_t_u(l_1576)) > (safe_unary_minus_func_uint32_t_u((l_1658[5][5][2] || ((*g_886) != (-1L)))))), 3)) , (safe_sub_func_uint64_t_u_u(0xC953FA24B3C8273ALL, (((safe_rshift_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u((!l_1636[0]), 6)), 4)), 4)) >= l_1658[2][1][1]) , (**g_885))))) , &g_156[5][2]) != (void*)0))) > 0x13L) == (*g_375)), l_1672[7]))) == l_1672[7])) <= (*g_375)));
        for (g_673 = (-23); (g_673 <= 1); g_673 = safe_add_func_uint32_t_u_u(g_673, 4))
        { /* block id: 806 */
            int32_t *l_1676[8] = {&g_104,&g_2[3],&g_104,&g_2[3],&g_104,&g_2[3],&g_104,&g_2[3]};
            uint32_t l_1680 = 0x7F86A78BL;
            int i;
            --l_1680;
            if (((l_1683 & (safe_rshift_func_uint8_t_u_u(((*g_660) || (safe_unary_minus_func_int64_t_s((0x4D030F4BA08DDD80LL != l_1687)))), 2))) >= (*l_1673)))
            { /* block id: 808 */
                (*l_1673) ^= (-4L);
            }
            else
            { /* block id: 810 */
                g_156[5][2] = &g_69;
            }
        }
        (*l_1673) &= (safe_sub_func_uint8_t_u_u((&g_715 == &g_715), (***g_1076)));
    }
    else
    { /* block id: 815 */
        int64_t l_1690 = 0x3AFA34C4BB4E8F1ALL;
        int32_t *l_1691 = &g_2[2];
        int32_t *l_1692 = &g_2[3];
        int32_t *l_1693 = &g_752;
        int32_t *l_1694 = &l_1576;
        int32_t *l_1695 = (void*)0;
        int32_t *l_1696 = &g_69;
        int32_t *l_1697 = &g_104;
        int32_t *l_1698 = &l_1636[1];
        int32_t *l_1699 = &g_2[1];
        int32_t *l_1700 = &l_1576;
        int32_t *l_1701 = &g_2[3];
        int32_t *l_1702 = &l_1636[2];
        int32_t *l_1703 = &g_2[2];
        int32_t *l_1704[1];
        int i;
        for (i = 0; i < 1; i++)
            l_1704[i] = &g_69;
        ++l_1706;
        for (g_727 = 0; (g_727 <= 1); g_727 += 1)
        { /* block id: 819 */
            int32_t l_1709 = 0x67CB2617L;
            (*l_1699) ^= (l_1709 ^ (**g_885));
            for (g_117.f0 = 0; (g_117.f0 <= 1); g_117.f0 += 1)
            { /* block id: 823 */
                uint32_t l_1710 = 0x0FFC460EL;
                int32_t * const l_1711 = (void*)0;
                int32_t l_1714 = (-7L);
                int32_t l_1715 = 0x71FB5D3FL;
                int32_t l_1719 = 0x77E8059CL;
                (*l_1701) ^= l_1709;
            }
        }
    }
    (*l_1727) ^= ((((((l_1637 && l_1577) , (l_1636[2] > (**g_816))) != (0xC419L && ((*l_1721)--))) || ((**g_885) = (l_1577 , ((void*)0 != (*g_1223))))) , (((!(safe_sub_func_uint64_t_u_u((l_696.f1 | l_1706), l_1559[0][5]))) != l_1716) ^ 3L)) != (*g_1224));
    if ((safe_sub_func_int16_t_s_s((safe_rshift_func_int8_t_s_s(((*l_1650) = 1L), 0)), (*l_1727))))
    { /* block id: 848 */
        int32_t *l_1732 = &g_1439[1][2][0];
        int32_t *l_1733 = &g_752;
        int32_t *l_1734 = &g_752;
        int32_t *l_1735 = &l_1705;
        int32_t l_1736[8][1][3] = {{{1L,0L,0xBB29B0FCL}},{{(-1L),(-1L),0x1A940FD3L}},{{1L,0L,0xBB29B0FCL}},{{(-1L),(-1L),0x1A940FD3L}},{{1L,0L,0xBB29B0FCL}},{{(-1L),(-1L),0x1A940FD3L}},{{1L,0L,0xBB29B0FCL}},{{(-1L),(-1L),0x1A940FD3L}}};
        int32_t *l_1737 = &g_2[3];
        int32_t *l_1738[4][5] = {{&g_104,(void*)0,&l_1636[3],(void*)0,&g_104},{&l_1705,&l_1634,&g_752,(void*)0,&l_1634},{&g_104,&g_752,&g_752,&g_104,(void*)0},{(void*)0,&g_104,&l_1634,&l_1705,&l_1705}};
        int32_t l_1739[3][9] = {{0xECCE5FBCL,0L,0xDBE21EEDL,0L,0xECCE5FBCL,0xECCE5FBCL,0L,0xDBE21EEDL,0L},{1L,0x4ABDBC74L,0xFB7CBA64L,0xFB7CBA64L,0x4ABDBC74L,1L,0x4ABDBC74L,0xFB7CBA64L,0xFB7CBA64L},{0xECCE5FBCL,0xECCE5FBCL,0L,0xDBE21EEDL,0L,0xECCE5FBCL,0xECCE5FBCL,0L,0xDBE21EEDL}};
        uint32_t l_1740 = 0x9E4610C6L;
        int i, j, k;
        ++l_1740;
    }
    else
    { /* block id: 850 */
        const uint64_t l_1754 = 18446744073709551615UL;
        uint32_t *l_1760[1][6][8] = {{{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112}}};
        int32_t *l_1762 = &l_1636[3];
        int i, j, k;
        for (g_179 = (-5); (g_179 >= 19); ++g_179)
        { /* block id: 853 */
            return g_243;
        }
        (*l_1762) |= (safe_lshift_func_int16_t_s_u(((*l_1727) | ((safe_lshift_func_int8_t_s_u((safe_unary_minus_func_int8_t_s(7L)), 0)) <= ((*g_814) , ((((**g_885) <= ((l_1761 = ((((safe_lshift_func_uint16_t_u_u(0xF630L, 3)) | (l_1754 == ((l_1755 < ((*l_1647) = g_1416.f4)) || (safe_add_func_int32_t_s_s((safe_lshift_func_int8_t_s_u(0x57L, 1)), g_1439[1][2][0]))))) ^ l_1754) >= (*l_1727))) , 0xF72C65FF276A1E64LL)) , l_1754) & 0xB9L)))), l_1754));
    }
    return g_2[3];
}


/* ------------------------------------------ */
/* 
 * reads : g_815 g_816 g_375 g_117.f4 g_104 g_1416.f1 g_88
 * writes: g_455 g_104 g_1416.f1 g_88
 */
static int32_t  func_9(uint8_t  p_10, int8_t  p_11)
{ /* block id: 727 */
    int64_t *l_1480 = &g_305;
    int64_t *l_1481[10] = {&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146,&g_146};
    uint16_t *l_1482 = &g_455[4];
    int32_t *l_1483 = &g_104;
    struct S1 l_1486[6][2][2] = {{{{12,18446744073709551615UL},{39,0x0041DF42L}},{{57,5UL},{34,1UL}}},{{{34,1UL},{57,5UL}},{{39,0x0041DF42L},{12,18446744073709551615UL}}},{{{39,0x0041DF42L},{57,5UL}},{{34,1UL},{34,1UL}}},{{{57,5UL},{39,0x0041DF42L}},{{12,18446744073709551615UL},{39,0x0041DF42L}}},{{{57,5UL},{34,1UL}},{{34,1UL},{57,5UL}}},{{{39,0x0041DF42L},{12,18446744073709551615UL}},{{39,0x0041DF42L},{57,5UL}}}};
    int32_t l_1507 = 0x2EC26F1AL;
    uint8_t l_1508 = 5UL;
    int i, j, k;
    (*l_1483) &= (p_11 <= (((***g_815) | (safe_rshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u(p_10, 2)), ((*l_1482) = ((l_1480 = l_1480) != l_1481[8])))), 6))) <= (p_11 , 1UL)));
    for (g_1416.f1 = (-11); (g_1416.f1 < 24); g_1416.f1++)
    { /* block id: 733 */
        struct S1 *l_1487 = (void*)0;
        int32_t l_1490[9] = {0x77418AA2L,0x77418AA2L,0x77418AA2L,0x77418AA2L,0x77418AA2L,0x77418AA2L,0x77418AA2L,0x77418AA2L,0x77418AA2L};
        uint8_t *l_1495 = &g_88;
        int i;
        (*l_1483) = 0xFDA2A06FL;
        if (p_11)
            continue;
        l_1486[5][0][1] = l_1486[5][0][1];
        (*l_1483) = (((safe_mod_func_int8_t_s_s(l_1490[0], p_11)) || (((safe_lshift_func_uint16_t_u_s(65535UL, (((*l_1482) = (*l_1483)) != ((++(*l_1495)) == ((safe_sub_func_uint64_t_u_u(((safe_rshift_func_uint8_t_u_u((p_10 = ((p_10 , (safe_rshift_func_int8_t_s_s((2UL || p_10), 5))) ^ ((safe_unary_minus_func_uint8_t_u(0x80L)) & ((((safe_div_func_int8_t_s_s(l_1507, (*l_1483))) , (void*)0) != l_1482) <= p_10)))), p_11)) | p_11), l_1508)) & l_1490[0]))))) | (*l_1483)) != 0x06C7L)) , p_10);
    }
    return (*l_1483);
}


/* ------------------------------------------ */
/* 
 * reads : g_1439 g_117.f2 g_90 g_1224 g_117.f1 g_885 g_886 g_305
 * writes: g_117.f2 g_90 g_88
 */
static int64_t  func_16(const int64_t  p_17, const uint16_t  p_18, const struct S1  p_19)
{ /* block id: 707 */
    int32_t *l_1440 = &g_104;
    int32_t **l_1441 = (void*)0;
    int32_t l_1446 = 1L;
    int32_t l_1447 = 0xEC2246D9L;
    int32_t l_1448 = (-1L);
    int32_t l_1449 = 4L;
    int32_t l_1450 = 1L;
    int32_t l_1451[7];
    int64_t l_1452 = 0x2DEB59E885A10DA3LL;
    int8_t l_1453 = 0L;
    int i;
    for (i = 0; i < 7; i++)
        l_1451[i] = 0x289375F4L;
    l_1440 = (g_1439[1][2][0] , l_1440);
    for (g_117.f2 = (-18); (g_117.f2 > 52); g_117.f2 = safe_add_func_int16_t_s_s(g_117.f2, 9))
    { /* block id: 711 */
        int32_t *l_1444 = (void*)0;
        int32_t *l_1445[2][3] = {{&g_69,&g_69,&g_69},{&g_1439[0][1][9],&g_1439[0][1][9],&g_1439[0][1][9]}};
        uint64_t l_1454 = 0xB69CF42C1B8145BBLL;
        struct S1 **l_1464 = &g_814;
        int i, j;
        if (p_19.f1)
            break;
        l_1454++;
        for (g_90 = (-9); (g_90 < (-24)); g_90 = safe_sub_func_int8_t_s_s(g_90, 8))
        { /* block id: 716 */
            struct S0 **l_1459 = (void*)0;
            struct S0 *l_1460 = &g_1461;
            int32_t l_1467 = 0L;
            struct S1 l_1470[5][7] = {{{63,0xA5D999B3L},{37,18446744073709551615UL},{15,0x959577C7L},{16,7UL},{16,7UL},{15,0x959577C7L},{37,18446744073709551615UL}},{{29,0xCEBE7436L},{46,0xAA561044L},{3,0x804E2644L},{35,0xF9CE8EB3L},{35,0xF9CE8EB3L},{3,0x804E2644L},{46,0xAA561044L}},{{63,0xA5D999B3L},{37,18446744073709551615UL},{15,0x959577C7L},{16,7UL},{16,7UL},{15,0x959577C7L},{37,18446744073709551615UL}},{{29,0xCEBE7436L},{46,0xAA561044L},{3,0x804E2644L},{35,0xF9CE8EB3L},{35,0xF9CE8EB3L},{3,0x804E2644L},{46,0xAA561044L}},{{63,0xA5D999B3L},{37,18446744073709551615UL},{15,0x959577C7L},{16,7UL},{16,7UL},{15,0x959577C7L},{37,18446744073709551615UL}}};
            int32_t l_1471 = 0x90ACA224L;
            int32_t l_1472 = 0xE8DA6B69L;
            int32_t **l_1473 = &l_1440;
            int i, j;
            l_1460 = &g_1416;
            l_1472 ^= ((safe_add_func_uint8_t_u_u((g_88 = p_19.f1), ((p_18 , 1L) <= ((0x18A77CF2L > ((void*)0 == l_1464)) || ((safe_lshift_func_int8_t_s_s(l_1467, (l_1471 ^= (safe_div_func_uint8_t_u_u(p_19.f1, (l_1470[2][5] , 1L)))))) || (*g_1224)))))) <= l_1470[2][5].f0);
            (*l_1473) = l_1445[0][0];
            return (**g_885);
        }
    }
    return (*g_886);
}


/* ------------------------------------------ */
/* 
 * reads : g_179 g_104 g_752 g_1433
 * writes: g_104 g_179 g_752 g_1433
 */
static uint64_t  func_23(const int32_t  p_24, uint64_t  p_25, uint64_t  p_26)
{ /* block id: 696 */
    int32_t *l_1417 = &g_104;
    int32_t l_1429 = (-1L);
    int32_t l_1430 = 0xD021FE39L;
    int32_t l_1431[7] = {0x450274E4L,0x450274E4L,0x450274E4L,0x450274E4L,0x450274E4L,0x450274E4L,0x450274E4L};
    uint16_t l_1437 = 0xAD5AL;
    int i;
    (*l_1417) = p_24;
    for (g_179 = (-19); (g_179 > (-7)); g_179 = safe_add_func_int32_t_s_s(g_179, 1))
    { /* block id: 700 */
        int32_t *l_1420 = (void*)0;
        int32_t *l_1421 = &g_752;
        int32_t *l_1422 = &g_104;
        int32_t l_1423[7] = {0L,0L,0L,0L,0L,0L,0L};
        int32_t *l_1424 = &g_752;
        int32_t *l_1425 = &g_69;
        int32_t *l_1426 = &g_752;
        int32_t *l_1427 = &l_1423[3];
        int32_t *l_1428[9][2] = {{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752},{&g_752,&g_752}};
        int64_t l_1432[7][7] = {{0x89AABC165D5686F8LL,0x3406427581BAFF05LL,0xD6F6113D3ADA7BF3LL,0x89AABC165D5686F8LL,0x1B01A35D4CBD78D4LL,0x1B01A35D4CBD78D4LL,0x89AABC165D5686F8LL},{0xD6F6113D3ADA7BF3LL,4L,0xD6F6113D3ADA7BF3LL,0x1B01A35D4CBD78D4LL,4L,0x3406427581BAFF05LL,0x3406427581BAFF05LL},{4L,0x89AABC165D5686F8LL,1L,0x89AABC165D5686F8LL,4L,1L,0L},{0L,0x3406427581BAFF05LL,0x1B01A35D4CBD78D4LL,0L,0x1B01A35D4CBD78D4LL,0x3406427581BAFF05LL,0L},{0xD6F6113D3ADA7BF3LL,0L,0x3406427581BAFF05LL,0x1B01A35D4CBD78D4LL,0L,0x1B01A35D4CBD78D4LL,0x3406427581BAFF05LL},{0L,0L,1L,4L,0x89AABC165D5686F8LL,1L,0x89AABC165D5686F8LL},{4L,0x3406427581BAFF05LL,0x3406427581BAFF05LL,4L,0x1B01A35D4CBD78D4LL,0xD6F6113D3ADA7BF3LL,4L}};
        int32_t **l_1436 = &l_1428[0][0];
        int i, j;
        (*l_1421) |= ((*l_1417) = (*l_1417));
        g_1433--;
        (*l_1436) = (void*)0;
    }
    return l_1437;
}


/* ------------------------------------------ */
/* 
 * reads : g_104 g_88 g_895 g_816 g_375 g_117.f4 g_752 g_305 g_90 g_886 g_1076 g_117.f3 g_112 g_117.f8 g_815 g_885 g_69 g_727 g_713 g_624 g_1223 g_852 g_1224 g_117.f1 g_750 g_188 g_117 g_1336 g_67 g_840 g_127
 * writes: g_156 g_104 g_895 g_69 g_752 g_713 g_90 g_305 g_109 g_727 g_88 g_1175 g_851 g_117.f7 g_852 g_311 g_243 g_117.f1 g_179 g_112 g_840 g_127 g_117.f4 g_108 g_67.f1 g_1415
 */
static uint8_t  func_29(int16_t  p_30, int8_t  p_31)
{ /* block id: 479 */
    int32_t **l_1016 = (void*)0;
    int32_t **l_1017 = &g_156[9][2];
    int32_t *l_1064[4] = {&g_713,&g_713,&g_713,&g_713};
    const uint32_t l_1081[3] = {0x74C0D6B5L,0x74C0D6B5L,0x74C0D6B5L};
    int32_t l_1087[5][9][5] = {{{0x81AF6606L,6L,(-5L),0L,0L},{(-1L),(-1L),(-1L),0xE5C84EACL,4L},{(-5L),6L,0x81AF6606L,6L,8L},{(-6L),1L,0x5FC1ECDFL,0xD9D8BC7DL,0x0BC496F9L},{0x92F487D8L,0x4CFB222BL,0x81AF6606L,8L,8L},{0xAA9E04F3L,1L,(-1L),4L,0x289E8CDBL},{0x52A9FA63L,0x1C4BF9E8L,(-5L),8L,(-4L)},{0xE3B3A64DL,1L,(-6L),0xD9D8BC7DL,1L},{1L,2L,0x92F487D8L,6L,(-4L)}},{{(-1L),0xA0585E87L,0xAA9E04F3L,0xE5C84EACL,0x289E8CDBL},{0L,0xC8C808A0L,0x52A9FA63L,0L,8L},{(-1L),1L,0xE3B3A64DL,0x0BC496F9L,0x0BC496F9L},{1L,0x3206418EL,1L,(-8L),8L},{0xE3B3A64DL,1L,(-1L),1L,4L},{0x52A9FA63L,0xC8C808A0L,0L,0xC1D27CE4L,0L},{0xAA9E04F3L,0xA0585E87L,(-1L),4L,0xEFBB074CL},{0x92F487D8L,2L,1L,0xB1813858L,0x2BA0714BL},{(-6L),1L,0xE3B3A64DL,4L,0x260E2C82L}},{{(-5L),0x1C4BF9E8L,0x52A9FA63L,0xC1D27CE4L,6L},{(-1L),1L,0xAA9E04F3L,1L,0x260E2C82L},{0x81AF6606L,0x4CFB222BL,0x92F487D8L,(-8L),0x2BA0714BL},{0x5FC1ECDFL,1L,(-6L),0x0BC496F9L,0xEFBB074CL},{0x81AF6606L,6L,(-5L),0L,0L},{(-1L),(-1L),(-1L),0xE5C84EACL,4L},{(-5L),6L,0x81AF6606L,6L,8L},{(-6L),1L,0x5FC1ECDFL,0xD9D8BC7DL,0x0BC496F9L},{0x92F487D8L,0x4CFB222BL,0L,0x97CAFC44L,0x92F487D8L}},{{0x14967A05L,0x3E8970A7L,0L,(-1L),0xE3B3A64DL},{1L,0x60BB61DEL,2L,0x97CAFC44L,(-6L)},{(-3L),6L,(-5L),0x5FC1ECDFL,(-6L)},{9L,0x93C38C9AL,(-8L),0x52A9FA63L,(-6L)},{5L,0x9BCE4391L,0x14967A05L,(-1L),0xE3B3A64DL},{0xAF3B9A1DL,0xC50C55EAL,1L,1L,0x92F487D8L},{5L,0x5336F56BL,(-3L),(-1L),(-1L)},{9L,0xF4BA8F9CL,9L,0x81AF6606L,0x97CAFC44L},{(-3L),0x5336F56BL,5L,(-6L),(-1L)}},{{1L,0xC50C55EAL,0xAF3B9A1DL,0L,1L},{0x14967A05L,0x9BCE4391L,5L,(-1L),0xAA9E04F3L},{(-8L),0x93C38C9AL,9L,9L,(-5L)},{(-5L),6L,(-3L),(-1L),1L},{2L,0x60BB61DEL,1L,0L,0x52A9FA63L},{0L,0x3E8970A7L,0x14967A05L,(-6L),1L},{0L,0xC8A79925L,(-8L),0x81AF6606L,(-5L)},{0xC619F137L,(-6L),(-5L),(-1L),0xAA9E04F3L},{0L,(-7L),2L,1L,1L}}};
    uint32_t l_1114 = 0xF2EBEFD5L;
    uint16_t l_1207 = 0xBA68L;
    int16_t * const * const l_1225[8][8] = {{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224},{&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224,&g_1224}};
    struct S0 *l_1276 = &g_117;
    struct S1 l_1298[5] = {{59,0x2C99D359L},{59,0x2C99D359L},{59,0x2C99D359L},{59,0x2C99D359L},{59,0x2C99D359L}};
    int8_t ****l_1307 = &g_750;
    int32_t *l_1340[5][7][7] = {{{(void*)0,&g_2[3],&g_69,(void*)0,&l_1087[3][1][3],&l_1087[4][8][3],&g_104},{&g_752,&l_1087[3][1][3],(void*)0,&g_2[3],&g_752,&g_69,(void*)0},{&l_1087[3][1][3],&l_1087[3][3][1],&l_1087[4][8][3],&g_2[3],&g_2[3],&g_104,&g_752},{&l_1087[3][1][3],(void*)0,&g_2[0],&g_2[0],(void*)0,&l_1087[3][1][3],&g_69},{&g_104,&l_1087[4][7][4],&g_2[3],&l_1087[3][1][3],&g_104,&l_1087[2][2][4],(void*)0},{&g_752,&g_2[3],&l_1087[4][8][3],(void*)0,(void*)0,&g_2[0],(void*)0},{(void*)0,&l_1087[4][7][4],&g_752,&g_69,(void*)0,(void*)0,&g_752}},{{&g_69,&g_752,&g_752,&l_1087[3][5][4],&g_104,&g_2[3],(void*)0},{&l_1087[3][1][3],&g_2[0],&g_104,&l_1087[3][3][1],&l_1087[3][1][3],&g_752,&g_104},{&g_104,&g_752,&g_2[0],(void*)0,&l_1087[3][1][3],&l_1087[3][1][3],&g_752},{&g_752,(void*)0,&g_752,&l_1087[2][7][2],&g_2[3],(void*)0,&g_104},{&g_69,(void*)0,(void*)0,&l_1087[3][1][3],&g_69,&l_1087[4][2][3],&g_69},{(void*)0,&g_752,&g_104,(void*)0,&g_104,&l_1087[4][8][3],&g_69},{&g_2[0],&g_2[3],&g_104,&l_1087[3][1][3],(void*)0,&g_104,&g_2[3]}},{{&l_1087[3][1][3],&g_2[3],(void*)0,&g_752,&g_2[3],&g_752,(void*)0},{(void*)0,&g_752,&g_752,(void*)0,&g_2[2],&g_69,&g_752},{(void*)0,&g_2[3],&g_2[0],&g_2[3],&g_69,&g_69,(void*)0},{(void*)0,&g_104,&g_104,(void*)0,&g_752,(void*)0,&g_69},{&g_2[2],&l_1087[3][3][1],&g_752,(void*)0,(void*)0,&g_752,&l_1087[3][3][1]},{&g_104,(void*)0,&g_752,&g_69,&g_2[3],&g_69,&g_2[3]},{&l_1087[3][1][3],&g_2[3],&l_1087[4][8][3],(void*)0,&g_69,&g_69,(void*)0}},{{&l_1087[2][5][4],(void*)0,&g_2[3],&g_69,(void*)0,&l_1087[3][1][3],&g_2[3]},{&g_752,(void*)0,&g_752,(void*)0,&l_1087[2][2][0],&l_1087[0][0][1],&g_2[2]},{&g_752,(void*)0,&l_1087[2][5][4],(void*)0,&l_1087[4][8][3],&l_1087[3][1][3],(void*)0},{(void*)0,&g_752,&g_2[3],&g_2[3],(void*)0,&g_69,&l_1087[2][7][2]},{(void*)0,&l_1087[3][1][3],&l_1087[3][1][3],(void*)0,&g_752,&l_1087[3][1][3],(void*)0},{&l_1087[3][1][3],&l_1087[2][5][4],&g_752,&g_752,&g_2[3],&g_2[0],(void*)0},{&g_104,(void*)0,&l_1087[4][2][3],&l_1087[3][1][3],&g_2[0],(void*)0,&l_1087[3][1][3]}},{{&l_1087[2][2][4],&g_2[2],(void*)0,(void*)0,&g_2[0],&g_2[3],&g_752},{(void*)0,&g_2[3],(void*)0,&l_1087[3][1][3],&g_2[3],&l_1087[4][7][4],&g_104},{(void*)0,&g_104,&g_69,&l_1087[2][7][2],&g_752,(void*)0,&g_2[0]},{&g_2[3],(void*)0,&g_752,&g_104,&g_69,&g_2[3],&l_1087[2][2][0]},{&g_104,(void*)0,&g_752,&g_2[0],&l_1087[2][5][4],&g_2[3],&l_1087[3][1][3]},{&g_69,(void*)0,(void*)0,(void*)0,&l_1087[3][1][3],&l_1087[3][1][3],(void*)0},{&g_104,(void*)0,&g_104,&g_752,&g_2[3],&g_104,&g_752}}};
    uint64_t l_1364[2];
    uint64_t l_1408 = 0x44ABC4058B6E03CBLL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1364[i] = 1UL;
lbl_1018:
    (*l_1017) = (void*)0;
    if (p_31)
    { /* block id: 481 */
        struct S1 l_1019 = {17,0xCCF601DBL};
        const int32_t l_1032 = 0x05FCC612L;
        int32_t *l_1033 = &g_752;
lbl_1030:
        if (p_31)
            goto lbl_1018;
        if ((l_1019 , (-6L)))
        { /* block id: 483 */
            int16_t l_1020 = 0xF8E2L;
            int32_t *l_1021 = &g_104;
            struct S1 *l_1026 = &l_1019;
            (*l_1021) |= l_1020;
            if (g_104)
                goto lbl_1031;
            if (((*l_1021) = (*l_1021)))
            { /* block id: 486 */
                return p_30;
            }
            else
            { /* block id: 488 */
                uint32_t *l_1027 = (void*)0;
                uint32_t *l_1028 = &g_895[1][9];
                int32_t *l_1029 = &g_69;
                (*l_1029) = (((((((((*l_1021) ^= ((safe_lshift_func_uint8_t_u_s((((safe_rshift_func_int8_t_s_u(1L, (g_88 ^ g_88))) , &g_67) != l_1026), 2)) , 0x5F6A607AL)) , ((*l_1028) |= 0UL)) ^ p_30) > 1L) | (**g_816)) , p_31) != p_31) <= p_30);
            }
        }
        else
        { /* block id: 493 */
lbl_1031:
            if (p_30)
                goto lbl_1030;
            (*l_1033) &= l_1032;
        }
    }
    else
    { /* block id: 498 */
        uint32_t l_1055 = 4UL;
        int32_t l_1056 = 0x213AC18EL;
        int8_t ** const *l_1066 = &g_188;
        int32_t l_1160[1];
        int64_t l_1185 = 0x88525A47B72F3AF7LL;
        int32_t l_1202[10][7][3] = {{{3L,1L,1L},{0xE4B03F0BL,7L,0x35282C24L},{(-1L),0x89B83578L,(-3L)},{0x3C18D8D5L,0x35282C24L,0xE4B03F0BL},{0x05E4D5D2L,3L,0L},{0x35282C24L,0x35282C24L,0x19872678L},{0x3D4A683EL,0x89B83578L,0xC1895CE5L}},{{0x4274E617L,7L,0x9FC2C6A6L},{(-3L),1L,0x89B83578L},{0x9FC2C6A6L,0x4274E617L,0x9FC2C6A6L},{0L,0x5C33FBB5L,0xC1895CE5L},{4L,0x87D527BFL,0x19872678L},{1L,0x3D4A683EL,0L},{0x0CF95DF4L,0xE4B03F0BL,0xE4B03F0BL}},{{1L,0xE77265C3L,(-3L)},{4L,0x72B88429L,0x35282C24L},{0L,(-3L),1L},{0x9FC2C6A6L,0x19872678L,(-1L)},{(-3L),(-3L),3L},{0x4274E617L,0x72B88429L,0xD8B6D3C2L},{0x3D4A683EL,0xE77265C3L,0x05E4D5D2L}},{{0x35282C24L,0xE4B03F0BL,0x72B88429L},{0x05E4D5D2L,0x3D4A683EL,0x05E4D5D2L},{0x3C18D8D5L,0x87D527BFL,0xD8B6D3C2L},{(-1L),0x5C33FBB5L,3L},{0xE4B03F0BL,0x4274E617L,(-1L)},{3L,1L,1L},{0xE4B03F0BL,7L,0x35282C24L}},{{(-1L),0x89B83578L,(-3L)},{0x3C18D8D5L,0x35282C24L,0xE4B03F0BL},{0x05E4D5D2L,3L,0L},{0x35282C24L,0x35282C24L,0x19872678L},{0x3D4A683EL,0x89B83578L,0xC1895CE5L},{0x4274E617L,7L,0x9FC2C6A6L},{(-3L),1L,0x89B83578L}},{{0x9FC2C6A6L,0x4274E617L,0x9FC2C6A6L},{0L,0x5C33FBB5L,0xC1895CE5L},{4L,0x87D527BFL,0x19872678L},{1L,0x3D4A683EL,0L},{0x0CF95DF4L,0xE4B03F0BL,0xE4B03F0BL},{1L,0xE77265C3L,(-3L)},{4L,0x72B88429L,0x35282C24L}},{{0L,(-3L),1L},{0x9FC2C6A6L,0x19872678L,(-1L)},{(-3L),(-3L),3L},{0x4274E617L,0x72B88429L,0xD8B6D3C2L},{0x3D4A683EL,0xE77265C3L,0x05E4D5D2L},{0x35282C24L,0xE4B03F0BL,0x72B88429L},{0x05E4D5D2L,0x3D4A683EL,0x05E4D5D2L}},{{0x3C18D8D5L,0x87D527BFL,0xD8B6D3C2L},{(-1L),0x5C33FBB5L,3L},{0xE4B03F0BL,0x4274E617L,(-1L)},{3L,1L,1L},{0xE4B03F0BL,7L,0x35282C24L},{(-1L),0x89B83578L,(-3L)},{0x87D527BFL,0x19872678L,0x0CF95DF4L}},{{0x89B83578L,0x3D4A683EL,(-1L)},{0x19872678L,0x19872678L,0x4274E617L},{0L,1L,0L},{0x3C18D8D5L,0xD8B6D3C2L,0x72B88429L},{3L,3L,1L},{0x72B88429L,0x3C18D8D5L,0x72B88429L},{0x5C33FBB5L,0xE77265C3L,0L}},{{0x9FC2C6A6L,7L,0x4274E617L},{3L,0L,(-1L)},{0x35282C24L,0x0CF95DF4L,0x0CF95DF4L},{3L,0xC1895CE5L,3L},{0x9FC2C6A6L,0xE4B03F0BL,0x19872678L},{0x5C33FBB5L,3L,3L},{0x72B88429L,0x4274E617L,4L}}};
        struct S0 *l_1236 = &g_117;
        const int16_t l_1247[9][2] = {{2L,0xF493L},{(-10L),0xF493L},{2L,0xF493L},{(-10L),0xF493L},{2L,0xF493L},{(-10L),0xF493L},{2L,0xF493L},{(-10L),0xF493L},{2L,0xF493L}};
        uint16_t l_1270 = 65532UL;
        struct S0 l_1278[1][10][6] = {{{{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL}},{{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{0x17L,0x7ACDL,0x6DC9D135L,0xD7E5A985L,0UL,0xA2B7L,0L,18446744073709551615UL,0UL},{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL}},{{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL}},{{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL}},{{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL},{0x17L,0x7ACDL,0x6DC9D135L,0xD7E5A985L,0UL,0xA2B7L,0L,18446744073709551615UL,0UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL}},{{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL}},{{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{0x17L,0x7ACDL,0x6DC9D135L,0xD7E5A985L,0UL,0xA2B7L,0L,18446744073709551615UL,0UL},{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL}},{{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL}},{{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{-3L,6L,18446744073709551614UL,1L,1UL,0x4BC0L,0xF7FBL,4UL,0xC0A9AF67545F30C6LL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{0xB4L,-1L,0x66C2645EL,9L,0xF745L,65535UL,0x1DF8L,3UL,18446744073709551610UL}},{{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL},{0x17L,0x7ACDL,0x6DC9D135L,0xD7E5A985L,0UL,0xA2B7L,0L,18446744073709551615UL,0UL},{0x36L,0x5F01L,0x9E3B0164L,0xBEAD544CL,0xF616L,0xB3CDL,0xC775L,0x74BE50924F097B5FLL,0UL},{1L,0xD7DAL,0x3F7DFA91L,0x9D4B8B29L,65527UL,0xD06FL,0x2A89L,0xC716B96C0A8B619DLL,3UL},{1L,0xBE8FL,7UL,0x829A2AB8L,0x7FDCL,0x8929L,1L,7UL,18446744073709551615UL},{0x16L,0xA65EL,18446744073709551615UL,1L,0xCE5FL,1UL,0x2CBDL,0x0EE4204A3EC422D7LL,6UL}}}};
        struct S1 l_1306 = {33,0xBE1C78ADL};
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_1160[i] = 0x9E8F7B76L;
lbl_1394:
        for (g_713 = 5; (g_713 >= 0); g_713 -= 1)
        { /* block id: 501 */
            int8_t l_1036[9] = {0L,(-1L),0L,0L,(-1L),0L,0L,(-1L),0L};
            int16_t *l_1049 = &g_90;
            int32_t l_1080[4][4] = {{0L,0x69C3EE96L,0L,0L},{0x69C3EE96L,0x69C3EE96L,0x598C1292L,0x69C3EE96L},{0x69C3EE96L,0L,0L,0x69C3EE96L},{0L,0x69C3EE96L,0L,0L}};
            int32_t l_1174 = 0x52AB47F7L;
            uint16_t l_1241 = 1UL;
            int32_t *l_1264[7][6][5] = {{{(void*)0,&l_1087[3][1][3],(void*)0,&l_1080[1][2],&l_1080[2][0]},{&l_1160[0],&l_1080[1][2],&g_104,&l_1080[2][2],&l_1080[2][0]},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_1160[0],(void*)0,&g_752,&l_1080[2][0],&l_1080[1][2]},{&l_1080[1][2],&g_2[3],(void*)0,&g_104,&l_1160[0]},{(void*)0,&l_1080[2][2],&l_1160[0],&l_1080[2][0],(void*)0}},{{&l_1160[0],&l_1056,&g_69,(void*)0,&l_1056},{(void*)0,&g_2[3],&l_1080[1][2],&l_1080[2][2],&l_1080[2][1]},{&g_2[3],&g_2[0],&l_1056,&g_104,&l_1056},{&l_1056,&l_1056,&l_1080[2][1],&l_1080[2][2],&l_1080[1][2]},{&l_1056,&l_1080[1][2],(void*)0,&g_104,&l_1080[2][2]},{&l_1080[2][2],&l_1087[3][7][0],&g_104,&g_104,&l_1160[0]}},{{(void*)0,&l_1080[1][2],&l_1087[3][1][3],&g_2[0],&g_2[0]},{&g_2[3],&l_1056,&l_1080[1][2],&g_2[3],&l_1080[2][2]},{&l_1160[0],&g_2[3],&l_1056,&g_2[0],&l_1174},{&l_1160[0],&l_1160[0],&l_1056,&l_1160[0],&l_1160[0]},{&g_2[3],&g_2[0],(void*)0,&l_1160[0],&l_1056},{(void*)0,&g_69,&g_2[3],(void*)0,&g_69}},{{&l_1080[2][2],&l_1160[0],&l_1160[0],&g_2[0],&l_1056},{&l_1056,(void*)0,&l_1056,&g_752,&l_1160[0]},{&l_1056,&l_1080[1][2],&g_2[3],&l_1080[2][2],&l_1174},{&l_1160[0],&l_1080[1][2],(void*)0,&l_1080[2][2],&l_1080[2][2]},{&g_104,&l_1087[3][1][3],&g_104,&g_752,&g_2[0]},{(void*)0,&l_1174,&l_1056,&g_2[0],&l_1160[0]}},{{(void*)0,&l_1056,&l_1080[1][2],(void*)0,&l_1080[2][2]},{&l_1056,(void*)0,&l_1056,&l_1160[0],&l_1080[1][2]},{&l_1160[0],&l_1056,&g_104,&l_1160[0],&l_1056},{(void*)0,&g_2[0],(void*)0,&g_2[0],&l_1056},{(void*)0,&g_2[0],&g_2[3],&g_2[3],&g_2[0]},{&l_1080[2][2],&l_1056,&l_1056,&g_2[0],&l_1080[1][2]}},{{&l_1160[0],(void*)0,&l_1160[0],&g_104,&l_1160[0]},{&l_1080[1][2],&l_1056,&g_2[3],&g_104,&l_1080[1][2]},{&l_1160[0],&l_1174,(void*)0,&l_1080[2][2],&g_104},{&l_1080[2][2],&l_1087[3][1][3],&l_1056,&g_104,&g_2[0]},{(void*)0,&l_1080[1][2],&l_1056,&g_69,&g_2[0]},{(void*)0,&l_1080[1][2],&l_1080[1][2],(void*)0,&g_104}},{{&l_1160[0],(void*)0,&l_1087[3][1][3],&g_2[0],&l_1080[1][2]},{&l_1056,&l_1160[0],&g_104,&l_1056,&l_1160[0]},{(void*)0,&g_69,(void*)0,&g_2[0],&l_1080[1][2]},{(void*)0,&g_2[0],&l_1080[2][1],(void*)0,&g_2[0]},{&g_104,&l_1160[0],&l_1056,&g_69,&l_1056},{&l_1160[0],&g_2[3],&l_1056,&g_104,&l_1056}}};
            struct S0 *l_1277 = (void*)0;
            int i, j, k;
            if ((l_1056 = (safe_div_func_int8_t_s_s(((l_1036[1] >= p_31) & (((((g_305 , (safe_rshift_func_uint16_t_u_u(((safe_div_func_int64_t_s_s(((*g_886) = (safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(((safe_add_func_int64_t_s_s((p_30 && p_31), (((safe_lshift_func_int16_t_s_u(((((*l_1049) |= p_30) == (safe_add_func_uint8_t_u_u((~0xF0L), (safe_add_func_int32_t_s_s(l_1055, p_31))))) & g_752), p_31)) && l_1055) != l_1036[1]))) ^ p_31), (-1L))), 255UL))), 0x11B7780E6EF8687DLL)) > 0x8BL), (*g_375)))) == 1UL) || l_1036[1]) | l_1036[3]) != (**g_816))), l_1055))))
            { /* block id: 505 */
                int32_t l_1059 = 6L;
                int32_t * const l_1065 = &g_713;
                int8_t ** const **l_1067 = &l_1066;
                int32_t l_1068 = 0x0F5F17DBL;
                uint32_t l_1110[1];
                struct S0 l_1113[7] = {{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL},{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL},{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL},{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL},{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL},{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL},{0x6CL,0L,18446744073709551610UL,0L,0x0179L,0x164AL,0x9B91L,0x4BDDAE2C19EF808FLL,0x3D88481231908C00LL}};
                int32_t l_1176 = 0x7F0DF2ACL;
                int32_t l_1183 = 0x6F64BEAFL;
                int32_t l_1184 = 8L;
                int i;
                for (i = 0; i < 1; i++)
                    l_1110[i] = 0xE1126E12L;
                if (((safe_mod_func_int64_t_s_s(((((l_1059 | (safe_add_func_int8_t_s_s(((safe_mul_func_uint8_t_u_u((p_31 != 0x77F5L), (p_31 <= ((l_1068 |= (((*l_1067) = ((((-8L) & (l_1064[2] != l_1065)) || l_1056) , l_1066)) == (void*)0)) > 0xFEL)))) ^ p_30), 0x55L))) || 0x453A114498A994CDLL) , p_30) >= p_31), l_1056)) == 0L))
                { /* block id: 508 */
                    uint32_t l_1071 = 0x90EBEC69L;
                    int32_t l_1086 = (-8L);
                    int32_t l_1109 = 0x4A1A3F00L;
                    int32_t l_1137[4][10][3] = {{{(-1L),0x45A0F97CL,0x787F41CDL},{0x8BCF9DB6L,0x3B0AA9B9L,(-10L)},{1L,(-1L),0x88727F9DL},{1L,0x8BCF9DB6L,(-10L)},{0x779C33AFL,0L,0x787F41CDL},{(-1L),1L,0x88727F9DL},{0xCB991D94L,0L,0x3B0AA9B9L},{(-10L),0xFE44849AL,(-1L)},{0x45A0F97CL,0xCB991D94L,3L},{0x45A0F97CL,0x8BCF9DB6L,0xCB991D94L}},{{(-10L),(-1L),(-1L)},{0xCB991D94L,(-1L),0xA5E46A00L},{(-1L),0L,(-1L)},{0x5426BF3CL,0xCB991D94L,0x3B0AA9B9L},{0x8BCF9DB6L,0x787F41CDL,(-1L)},{0x225C0866L,0xCB991D94L,(-1L)},{(-1L),0L,7L},{(-10L),(-1L),0x225C0866L},{0x5426BF3CL,(-1L),0xA5E46A00L},{0x787F41CDL,0x8BCF9DB6L,0x88727F9DL}},{{0x577D0C3BL,0xCB991D94L,0x88727F9DL},{1L,0xFE44849AL,0xA5E46A00L},{0x225C0866L,0L,0x225C0866L},{0x45A0F97CL,1L,7L},{0x8BCF9DB6L,0x45A0F97CL,(-1L)},{0x577D0C3BL,(-1L),(-1L)},{(-8L),0L,0x3B0AA9B9L},{0x577D0C3BL,0L,(-1L)},{0x8BCF9DB6L,(-8L),0xA5E46A00L},{0x45A0F97CL,0x5426BF3CL,(-1L)}},{{0x225C0866L,1L,0xCB991D94L},{1L,(-1L),3L},{0x577D0C3BL,(-1L),(-1L)},{0x787F41CDL,1L,0x3B0AA9B9L},{0x5426BF3CL,0x5426BF3CL,0x88727F9DL},{(-10L),(-8L),(-1L)},{(-1L),0L,3L},{0x225C0866L,0L,0x577D0C3BL},{0x8BCF9DB6L,(-1L),3L},{0x5426BF3CL,0x45A0F97CL,(-1L)}}};
                    int i, j, k;
                    if ((p_30 , ((safe_mod_func_int32_t_s_s((l_1071 = 0x9F133487L), (l_1036[6] ^ p_30))) | (safe_lshift_func_int16_t_s_u(((p_31 != (safe_sub_func_int64_t_s_s(((void*)0 == g_1076), ((safe_div_func_int8_t_s_s(((**g_816) & (((l_1080[1][2] = (1L >= l_1036[4])) >= l_1036[1]) | 0x9D6FC778CA5EC448LL)), p_31)) != p_31)))) <= 0x3AC0L), l_1055)))))
                    { /* block id: 511 */
                        int32_t *l_1082 = &l_1068;
                        int32_t *l_1083 = (void*)0;
                        int32_t *l_1084 = &l_1080[1][2];
                        int32_t *l_1085 = (void*)0;
                        int32_t *l_1088 = &l_1056;
                        int32_t *l_1089 = &l_1056;
                        int32_t *l_1090 = &g_104;
                        int32_t *l_1091 = &g_69;
                        int32_t *l_1092 = (void*)0;
                        int32_t *l_1093 = &l_1059;
                        int32_t *l_1094 = &g_104;
                        int32_t *l_1095 = (void*)0;
                        int32_t *l_1096 = &g_752;
                        int32_t *l_1097 = &l_1087[3][1][3];
                        int32_t *l_1098 = (void*)0;
                        int32_t *l_1099 = &l_1087[1][6][3];
                        int32_t *l_1100 = &l_1068;
                        int32_t *l_1101 = &g_69;
                        int32_t *l_1102 = &g_69;
                        int32_t *l_1103 = &g_752;
                        int32_t *l_1104 = &l_1080[0][2];
                        int32_t *l_1105 = &l_1086;
                        int32_t *l_1106 = &l_1087[4][6][4];
                        int32_t *l_1107 = &l_1056;
                        int32_t *l_1108[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1108[i] = &l_1087[3][1][3];
                        if (l_1081[1])
                            break;
                        ++l_1110[0];
                        (*l_1106) = ((((*g_886) |= (0x38L > (l_1113[4] , l_1036[1]))) || l_1114) & (safe_mul_func_int8_t_s_s(((-1L) == (safe_add_func_uint32_t_u_u((safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((safe_add_func_int64_t_s_s((safe_add_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((safe_mod_func_int32_t_s_s((((safe_add_func_uint32_t_u_u((0x86A5010C2A9AFF2ALL && p_30), ((*l_1093) = ((*l_1103) &= (&g_715 == (void*)0))))) != 1UL) < g_117.f3), p_31)), p_30)), 0xB478190482B9C91DLL)), g_112)), 0)), (*l_1099))) & l_1137[0][0][2]), 5)), 1)), 4294967290UL))), 0xD0L)));
                        (*l_1017) = &l_1080[3][2];
                    }
                    else
                    { /* block id: 519 */
                        int32_t *l_1140 = &g_104;
                        (*l_1140) ^= (safe_rshift_func_int16_t_s_u(g_117.f8, (***g_815)));
                    }
                }
                else
                { /* block id: 522 */
                    uint64_t *l_1161 = &g_109;
                    int32_t *l_1162 = &g_69;
                    int32_t l_1181 = 0L;
                    int32_t l_1182[10][4] = {{1L,(-7L),0xD883A3C9L,1L},{5L,0x1CBA2862L,5L,0xD883A3C9L},{0x61A6765DL,0x1CBA2862L,0x8ABE1673L,1L},{0x1CBA2862L,(-7L),(-7L),0x1CBA2862L},{5L,1L,(-7L),0xD883A3C9L},{0x1CBA2862L,0x61A6765DL,0x8ABE1673L,0x61A6765DL},{0x61A6765DL,(-7L),5L,0x61A6765DL},{5L,0x61A6765DL,0xD883A3C9L,0xD883A3C9L},{1L,1L,0x8ABE1673L,0x1CBA2862L},{1L,(-7L),0xD883A3C9L,1L}};
                    uint64_t l_1186 = 0UL;
                    struct S1 l_1189 = {38,0UL};
                    int i, j;
                    (*l_1162) &= (safe_mod_func_uint16_t_u_u((~(safe_mul_func_uint8_t_u_u((~p_31), (safe_add_func_uint16_t_u_u(((((*l_1161) = ((safe_mul_func_int8_t_s_s((l_1059 = (l_1056 ^ ((l_1080[1][2] && (safe_mul_func_int8_t_s_s((0x648883B1AF3E8815LL >= (((safe_lshift_func_uint16_t_u_s(((safe_unary_minus_func_int16_t_s((((safe_lshift_func_uint8_t_u_s(l_1113[4].f0, 7)) , (safe_div_func_int8_t_s_s(((&g_101 != (void*)0) == (0x46E555D5B9A873B4LL & (l_1056 && l_1080[0][0]))), (-10L)))) == (*g_886)))) < 0xE41FL), 1)) , l_1160[0]) | 0x46B4L)), p_31))) && l_1113[4].f1))), p_30)) == (**g_885))) , l_1113[4].f6) , 0x1E9CL), 0x07ACL))))), (*g_375)));
                    for (g_104 = 0; (g_104 <= 4); g_104 += 1)
                    { /* block id: 528 */
                        uint8_t *l_1167 = &g_727;
                        int8_t *l_1172[1];
                        uint8_t *l_1173 = &g_88;
                        int32_t *l_1177 = &l_1068;
                        int32_t *l_1178 = (void*)0;
                        int32_t *l_1179 = &l_1087[3][1][3];
                        int32_t *l_1180[3];
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_1172[i] = (void*)0;
                        for (i = 0; i < 3; i++)
                            l_1180[i] = &l_1160[0];
                        l_1160[0] &= ((g_1175 = (l_1174 = ((*l_1173) = (((safe_mod_func_uint64_t_u_u(((safe_mul_func_int8_t_s_s(((((((((--(*l_1167)) < (safe_div_func_int64_t_s_s(g_895[g_713][g_713], 0xA154B1A4C3AB7A97LL))) | (5UL == ((((p_31 = l_1113[4].f0) != (l_1080[1][2] = (-9L))) != l_1036[2]) <= ((*g_375) == (*l_1162))))) && p_30) , p_30) == l_1036[1]) , (void*)0) == &g_156[(g_104 + 2)][g_104]), p_30)) , l_1056), l_1055)) , g_624) == (void*)0)))) != 1UL);
                        g_156[(g_104 + 2)][(g_104 + 2)] = &l_1068;
                        ++l_1186;
                        (*l_1017) = (void*)0;
                    }
                    if (l_1036[7])
                        continue;
                    for (l_1059 = 0; (l_1059 <= 4); l_1059 += 1)
                    { /* block id: 543 */
                        l_1189 = l_1189;
                        if (l_1186)
                            goto lbl_1018;
                        g_156[1][0] = &g_2[3];
                    }
                }
                for (g_727 = 0; (g_727 <= 4); g_727 += 1)
                { /* block id: 551 */
                    int16_t l_1203 = (-1L);
                    int32_t l_1205 = (-1L);
                    int32_t l_1206[1][6][5] = {{{0x335CF5D6L,0xFC480A2DL,0x335CF5D6L,0x69F936A1L,0xFC480A2DL},{0xCCE0CD64L,0xDE58AC5AL,0x69F936A1L,0xCCE0CD64L,0x69F936A1L},{0xCCE0CD64L,0xCCE0CD64L,4L,0xFC480A2DL,0x58CAB563L},{0x335CF5D6L,0x58CAB563L,0x69F936A1L,0x69F936A1L,0x58CAB563L},{0x58CAB563L,0xDE58AC5AL,0x335CF5D6L,0x58CAB563L,0x69F936A1L},{0xFC480A2DL,0x58CAB563L,4L,0x58CAB563L,0xFC480A2DL}}};
                    int i, j, k;
                    for (g_851 = 4; (g_851 >= 0); g_851 -= 1)
                    { /* block id: 554 */
                        int32_t *l_1190 = &l_1174;
                        int32_t *l_1191 = &l_1080[1][1];
                        int32_t l_1192 = 1L;
                        int32_t l_1193[2];
                        int32_t *l_1194 = &l_1087[2][8][1];
                        int32_t *l_1195 = &l_1192;
                        int32_t *l_1196 = &l_1193[0];
                        int32_t *l_1197 = &l_1193[1];
                        int32_t *l_1198 = &l_1176;
                        int32_t *l_1199 = &l_1193[0];
                        int32_t *l_1200 = &l_1056;
                        int32_t *l_1201[6][5] = {{&l_1176,(void*)0,&l_1176,&l_1176,(void*)0},{(void*)0,&l_1176,&l_1176,(void*)0,&l_1176},{(void*)0,(void*)0,&l_1193[0],(void*)0,(void*)0},{&l_1176,(void*)0,&l_1193[0],&l_1193[0],&l_1176},{&l_1176,&l_1193[0],&l_1193[0],&l_1176,&l_1193[0]},{&l_1176,&l_1176,(void*)0,&l_1176,&l_1176}};
                        int i, j;
                        for (i = 0; i < 2; i++)
                            l_1193[i] = 0L;
                        --l_1207;
                    }
                }
                if (l_1113[4].f5)
                    break;
            }
            else
            { /* block id: 559 */
                int32_t **l_1221 = &g_156[8][0];
                uint64_t *l_1254[4][5] = {{&g_109,&g_243,&g_109,&g_109,&g_243},{&g_109,&g_243,&g_243,&g_109,&g_243},{&g_243,&g_243,(void*)0,&g_243,&g_243},{&g_243,&g_109,&g_243,&g_243,&g_109}};
                int32_t l_1255 = 0L;
                int32_t l_1256 = 1L;
                int32_t l_1257[4][9];
                uint32_t l_1258 = 0x0AC07C50L;
                int8_t *l_1263 = &l_1036[2];
                int i, j;
                for (i = 0; i < 4; i++)
                {
                    for (j = 0; j < 9; j++)
                        l_1257[i][j] = 0xE05CC0A8L;
                }
                if ((p_30 <= ((safe_rshift_func_uint16_t_u_s(65535UL, 5)) && (*g_886))))
                { /* block id: 560 */
                    struct S0 *l_1235 = &g_117;
                    for (g_117.f7 = 0; (g_117.f7 <= 4); g_117.f7 += 1)
                    { /* block id: 563 */
                        const int32_t *l_1220 = &g_117.f3;
                        const int32_t **l_1219 = &l_1220;
                        const int32_t ***l_1218 = &l_1219;
                        int32_t ***l_1222 = &l_1016;
                        int32_t l_1227 = 0x496422F9L;
                        struct S1 *l_1228 = &g_852[0];
                        int32_t *l_1237 = (void*)0;
                        int32_t *l_1238[5][9][5] = {{{&l_1080[1][2],&l_1080[3][0],&g_104,&l_1160[0],&l_1087[0][2][1]},{&l_1160[0],&g_104,&g_104,&g_2[3],(void*)0},{(void*)0,&l_1227,(void*)0,&l_1227,(void*)0},{&g_2[3],(void*)0,&l_1160[0],&l_1056,&l_1087[3][1][3]},{(void*)0,&g_104,&l_1174,&l_1087[3][5][1],&l_1080[1][2]},{&l_1080[3][0],&g_69,&l_1087[3][1][3],(void*)0,&l_1087[3][1][3]},{&g_2[2],&l_1087[3][5][1],&g_104,&g_104,(void*)0},{&l_1087[3][1][3],&g_2[3],&g_104,&g_69,(void*)0},{&l_1174,(void*)0,&g_752,&l_1056,&l_1087[0][2][1]}},{{&g_2[3],&g_104,(void*)0,&l_1080[1][2],&g_752},{(void*)0,&g_2[1],&l_1080[1][2],&l_1160[0],&g_2[3]},{&g_104,&l_1087[0][2][1],&l_1080[1][2],(void*)0,&g_104},{&l_1227,&l_1174,(void*)0,&l_1056,(void*)0},{(void*)0,(void*)0,&g_752,&l_1087[3][1][3],&g_104},{&l_1174,(void*)0,&g_104,&g_104,(void*)0},{&l_1174,&l_1227,&g_104,(void*)0,(void*)0},{&g_69,&g_2[3],&l_1087[3][1][3],&l_1174,&g_2[2]},{&l_1080[3][2],&l_1160[0],&l_1174,&g_104,(void*)0}},{{&g_69,(void*)0,&l_1160[0],(void*)0,&l_1056},{&l_1174,&g_2[3],(void*)0,&l_1056,&l_1174},{&l_1174,&g_752,&g_104,&l_1080[1][2],&g_104},{(void*)0,&l_1080[1][2],&g_104,(void*)0,&l_1080[3][0]},{&l_1227,&g_104,&l_1227,&l_1087[3][1][3],&l_1160[0]},{&g_104,(void*)0,(void*)0,&l_1087[3][1][3],&l_1160[0]},{(void*)0,&g_2[2],(void*)0,(void*)0,(void*)0},{&g_2[3],&l_1056,&l_1174,&l_1080[1][2],&g_2[3]},{&l_1174,&g_752,(void*)0,&l_1056,(void*)0}},{{&l_1087[3][1][3],&l_1087[3][1][3],&l_1056,(void*)0,(void*)0},{&g_2[2],&g_104,&l_1087[3][1][3],&g_104,&l_1174},{&l_1080[3][0],&g_69,&l_1227,&l_1174,&l_1056},{(void*)0,&g_104,&g_2[3],(void*)0,&l_1080[1][2]},{&g_2[3],&l_1087[3][1][3],&g_104,(void*)0,&g_69},{&g_104,(void*)0,&l_1087[3][1][3],(void*)0,(void*)0},{&l_1087[3][1][3],&l_1174,&l_1080[3][0],&l_1087[0][2][1],&g_2[3]},{(void*)0,&g_104,&l_1087[3][1][3],&l_1087[3][5][1],&g_104},{&g_2[0],(void*)0,&g_752,&l_1087[3][1][3],&g_104}},{{&l_1080[3][0],&g_104,&g_752,&l_1174,&g_2[3]},{&l_1080[3][2],&l_1174,&g_69,&l_1227,(void*)0},{(void*)0,&g_2[3],&g_752,&g_69,&g_69},{&g_104,(void*)0,&g_104,&l_1160[0],&l_1174},{&l_1087[3][1][3],&l_1080[3][2],&l_1174,&g_104,&l_1174},{&l_1087[0][2][1],&g_104,&l_1174,(void*)0,&g_2[3]},{(void*)0,&l_1227,&l_1174,&l_1174,&l_1080[1][2]},{&l_1227,&l_1227,&g_104,(void*)0,&l_1080[3][2]},{(void*)0,&l_1080[1][2],&g_752,(void*)0,(void*)0}}};
                        int i, j, k;
                        l_1160[0] ^= (safe_add_func_int64_t_s_s(((safe_mod_func_int64_t_s_s((safe_div_func_int64_t_s_s((((p_31 = ((((((*l_1218) = (void*)0) != ((*l_1222) = l_1221)) , (g_1223 != l_1225[0][1])) & p_31) & p_30)) ^ 0x03L) & (l_1227 = ((safe_unary_minus_func_uint16_t_u((*l_1220))) <= (*g_886)))), p_30)), 0x152E6BE1D0C139D2LL)) , 0xACB8AE9AE26CAC90LL), (*l_1220)));
                        (*l_1228) = g_852[0];
                        l_1087[3][1][3] = (safe_rshift_func_int8_t_s_s(((+((safe_lshift_func_int16_t_s_u((*g_1224), (+p_31))) > (l_1235 == l_1236))) <= p_31), 0));
                        if (p_31)
                            break;
                    }
                }
                else
                { /* block id: 573 */
                    uint16_t l_1242[6][5] = {{0x92B1L,0x967EL,0x92B1L,0UL,1UL},{0x7709L,0xD898L,0xB0E4L,0xD898L,0x7709L},{0x92B1L,0UL,0x967EL,5UL,0x967EL},{65526UL,65526UL,0xB0E4L,0x7709L,9UL},{0UL,0x92B1L,0x92B1L,0UL,0x967EL},{0xD898L,0x7709L,65535UL,65535UL,0x7709L}};
                    int i, j;
                    for (g_311 = 0; (g_311 <= 1); g_311 += 1)
                    { /* block id: 576 */
                        uint16_t l_1239 = 0x76A3L;
                        int32_t *l_1240 = &l_1174;
                        (*l_1240) = l_1239;
                    }
                    if (l_1241)
                        continue;
                    return l_1242[3][1];
                }
                l_1160[0] = (safe_add_func_int16_t_s_s(((**g_885) ^ (safe_lshift_func_uint8_t_u_s(l_1247[3][1], (safe_rshift_func_uint8_t_u_s(l_1174, (0x11B0L >= ((safe_rshift_func_uint16_t_u_u(((((*l_1263) = (((p_31 < (safe_add_func_int32_t_s_s(((((*g_886) ^ (l_1258++)) && p_30) || ((p_30 != (safe_mul_func_uint16_t_u_u(l_1080[1][2], l_1056))) != (**g_885))), p_31))) & p_30) , l_1202[5][2][0])) || p_31) ^ p_30), p_30)) <= l_1241))))))), (**g_1223)));
            }
            l_1264[2][1][3] = (void*)0;
            if (p_30)
                continue;
            l_1087[3][1][3] &= ((((*g_816) == (((0L > ((safe_sub_func_int64_t_s_s((safe_rshift_func_uint16_t_u_u((safe_unary_minus_func_uint8_t_u(l_1270)), 13)), (safe_rshift_func_uint16_t_u_u((0x66L & (p_30 || ((+(+(!(l_1276 != l_1277)))) <= p_31))), 12)))) >= 2UL)) < 0UL) , (*g_816))) & 0xC224CF4B2A0E2E7FLL) , 0xAA5EE8CCL);
            for (g_243 = 0; (g_243 <= 4); g_243 += 1)
            { /* block id: 591 */
                uint32_t l_1279 = 0x0DE2B380L;
                int32_t l_1280 = 1L;
                struct S1 *l_1281[4][7] = {{&g_67,&g_852[0],&g_852[0],&g_852[0],&g_852[0],&g_67,&g_67},{&g_852[0],&g_852[0],&g_852[6],&g_67,&g_852[2],&g_852[2],&g_67},{&g_852[2],&g_852[3],&g_852[2],&g_852[5],&g_67,&g_852[0],&g_67},{&g_852[6],&g_852[0],&g_852[0],&g_852[5],&g_852[0],&g_852[0],&g_852[6]}};
                int8_t **l_1293[6];
                int32_t l_1309 = 0x0685107BL;
                uint32_t l_1310 = 0xBB801F71L;
                struct S1 **l_1319 = &l_1281[3][5];
                uint8_t l_1320 = 0xD2L;
                struct S0 **l_1326 = &l_1276;
                int64_t *l_1337 = &g_179;
                int i, j;
                for (i = 0; i < 6; i++)
                    l_1293[i] = &g_189[3][1];
                if ((((g_727 , p_31) | (l_1278[0][7][5] , l_1279)) >= (***g_815)))
                { /* block id: 592 */
                    int32_t l_1297 = 0xBD0A0BE2L;
                    l_1056 &= (((((l_1280 = p_31) , 0x71AD821AL) , &g_852[0]) == l_1281[3][5]) > (p_30 < (((safe_add_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(((safe_div_func_int16_t_s_s((safe_rshift_func_uint16_t_u_s((safe_unary_minus_func_int32_t_s(p_31)), 1)), ((((0L & (l_1293[1] == ((((~((*g_886) = ((safe_rshift_func_uint8_t_u_u(l_1279, l_1055)) & p_30))) < l_1279) , p_30) , (*g_750)))) < l_1297) && l_1279) , (*g_1224)))) , (***g_815)), 0xCE11L)), p_31)), (**g_1223))) , l_1279) , 0xE2L)));
                    return l_1297;
                }
                else
                { /* block id: 597 */
                    const int8_t *l_1304 = (void*)0;
                    const int8_t **l_1303 = &l_1304;
                    const int8_t ***l_1302 = &l_1303;
                    const int8_t ****l_1301 = &l_1302;
                    const int8_t *****l_1305 = &l_1301;
                    int32_t l_1308[7][4][9] = {{{2L,7L,3L,7L,0x74159CA4L,0L,0L,0x86DDCE12L,0L},{0x7550D650L,0x39DFD303L,0x4EFF4695L,(-1L),(-7L),(-1L),0xF6B44D10L,0L,(-7L)},{0xA29EBA9BL,0L,0L,(-10L),0x86DDCE12L,(-8L),0xB168E10BL,0xD72DD7CBL,0L},{0xF3F01A1AL,0x24CD2990L,3L,9L,1L,9L,3L,0x24CD2990L,0xF3F01A1AL}},{{0xAE058DB8L,(-10L),0x40EE0528L,0x01E03914L,2L,0xD72DD7CBL,7L,0xAE058DB8L,0L},{(-7L),0L,0xF6B44D10L,(-1L),(-7L),(-1L),0x4EFF4695L,0x39DFD303L,0x7550D650L},{0xAE058DB8L,2L,7L,3L,7L,0x74159CA4L,0L,0L,0x86DDCE12L},{0xF3F01A1AL,(-1L),3L,(-7L),0xF3F01A1AL,0xB78D3C1EL,0x8DDAF5E9L,0x10C20282L,4L}},{{0L,3L,0xF8640EB1L,7L,1L,0L,0x40EE0528L,0xAE058DB8L,(-1L)},{0xD2356BA2L,0x39DFD303L,(-7L),0x7870CB23L,1L,0x0FBA98D0L,1L,0x7870CB23L,(-7L)},{0xEC36CECEL,0xEC36CECEL,0x40EE0528L,3L,0L,1L,0xB168E10BL,0x307EBF9AL,0xEC36CECEL},{4L,0x7870CB23L,0xF6D9D4ADL,(-1L),1L,0x7870CB23L,0x8DDAF5E9L,9L,0x81B64690L}},{{1L,7L,0x40EE0528L,0L,(-1L),0x718BFE90L,0xF8640EB1L,1L,0xAE058DB8L},{(-7L),0xA99EB7A5L,(-7L),0xB78D3C1EL,0xDF3A75A0L,0x24CD2990L,0xF6B44D10L,0x39DFD303L,1L},{0x8B3FCBD5L,0x01E03914L,0xF8640EB1L,0xB168E10BL,2L,0x74159CA4L,0L,0x307EBF9AL,(-8L)},{0x81B64690L,0x0FBA98D0L,3L,0xB78D3C1EL,9L,(-1L),0xA6664337L,(-1L),9L}},{{0L,(-1L),(-1L),0L,0x8B3FCBD5L,0L,0x307EBF9AL,0xAE058DB8L,0x01E03914L},{1L,0x10C20282L,0xDF3A75A0L,(-1L),0x7550D650L,0x0FBA98D0L,0xDF3A75A0L,0xA99EB7A5L,1L},{0x01E03914L,0L,0x307EBF9AL,3L,0x8B3FCBD5L,0xAE058DB8L,(-1L),0L,0L},{9L,0x7870CB23L,0xA6664337L,0x7870CB23L,9L,0xF9F4AD6CL,3L,(-1L),0x81B64690L}},{{(-8L),0xD24038DDL,0L,7L,2L,0x40EE0528L,0xF8640EB1L,(-8L),0x8B3FCBD5L},{1L,9L,0xF6B44D10L,(-7L),0xDF3A75A0L,0xF9F4AD6CL,(-7L),0x10C20282L,(-7L)},{0xAE058DB8L,(-1L),0xF8640EB1L,0xF8640EB1L,(-1L),0xAE058DB8L,0x40EE0528L,0L,1L},{0x81B64690L,0x39DFD303L,0x8DDAF5E9L,(-1L),1L,0x0FBA98D0L,0xF6D9D4ADL,(-1L),4L}},{{0xEC36CECEL,0xD24038DDL,0xB168E10BL,(-1L),0L,0L,0x40EE0528L,0x8B3FCBD5L,0xEC36CECEL},{(-7L),(-1L),1L,(-1L),1L,(-1L),(-7L),9L,0xD2356BA2L},{(-1L),0L,0x40EE0528L,7L,1L,0x74159CA4L,0xF8640EB1L,0L,0L},{4L,0xA99EB7A5L,0x8DDAF5E9L,0xF9F4AD6CL,0xF3F01A1AL,0x24CD2990L,3L,9L,1L}}};
                    uint8_t l_1316 = 0UL;
                    int i, j, k;
                    if (p_31)
                        break;
                    l_1298[4] = g_852[0];
                    if ((safe_sub_func_int8_t_s_s((((*l_1305) = l_1301) != ((l_1278[0][7][5].f4 <= l_1279) , (l_1306 , l_1307))), p_31)))
                    { /* block id: 601 */
                        int32_t l_1313 = 2L;
                        --l_1310;
                        if (l_1313)
                            continue;
                        if (p_30)
                            break;
                    }
                    else
                    { /* block id: 605 */
                        int8_t l_1314 = 0xAEL;
                        int32_t l_1315[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
                        int i;
                        l_1316++;
                    }
                }
                (*l_1319) = &g_852[5];
                if (((p_31 >= l_1320) < ((~0x9AB9DC5378F64E9DLL) <= ((safe_lshift_func_int8_t_s_u((l_1056 &= (g_117 , (safe_sub_func_int32_t_s_s(0x80002D13L, ((l_1326 != (void*)0) != ((((*l_1049) = ((safe_lshift_func_uint8_t_u_u(((((safe_add_func_uint32_t_u_u(((~((((*l_1337) = ((*g_886) &= (l_1160[0] &= ((!((*g_1224) = (**g_1223))) == (safe_unary_minus_func_int16_t_s((safe_rshift_func_uint8_t_u_s((g_1336[0][6] < l_1320), p_30)))))))) ^ 0xF96BF00298EB9EACLL) , (-1L))) , l_1309), p_30)) || 0x8FL) != l_1202[1][5][0]) <= (-1L)), 1)) > l_1306.f0)) != 0x1697L) || 0x7E8FL)))))), 3)) >= p_30))))
                { /* block id: 616 */
                    int16_t l_1341 = 1L;
                    int32_t l_1345 = 0xB96BD6A6L;
                    for (g_117.f1 = 4; (g_117.f1 >= 0); g_117.f1 -= 1)
                    { /* block id: 619 */
                        int32_t **l_1338 = (void*)0;
                        int32_t **l_1339[1][1][4];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 1; j++)
                            {
                                for (k = 0; k < 4; k++)
                                    l_1339[i][j][k] = &l_1264[4][3][0];
                            }
                        }
                        l_1340[2][4][2] = ((*l_1017) = &g_2[3]);
                        l_1280 &= l_1087[g_117.f1][(g_117.f1 + 1)][g_117.f1];
                        (*l_1017) = &l_1160[0];
                        if (l_1341)
                            continue;
                    }
                    for (g_112 = 0; (g_112 <= 4); g_112 += 1)
                    { /* block id: 628 */
                        uint64_t *l_1344 = &g_117.f7;
                        l_1345 = ((((safe_lshift_func_int16_t_s_u((p_30 ^ g_852[0].f1), l_1320)) , &g_108[1][1]) == (void*)0) && (((*l_1344) = p_31) && (-1L)));
                    }
                }
                else
                { /* block id: 632 */
                    if (g_117.f4)
                        goto lbl_1018;
                    if (l_1280)
                        break;
                }
                l_1280 = (-1L);
            }
        }
        if (p_31)
        { /* block id: 639 */
            int8_t l_1348[4][3] = {{5L,5L,5L},{0x68L,0x68L,0x68L},{5L,5L,5L},{0x68L,0x68L,0x68L}};
            struct S0 l_1349 = {0x38L,1L,1UL,0x1A84E1B6L,0xAC61L,0x324FL,0x5278L,18446744073709551607UL,1UL};
            struct S1 l_1351[1] = {{1,0xFA98EF6CL}};
            int32_t l_1352[7] = {0x455E0BC3L,0x7E968811L,0x7E968811L,0x455E0BC3L,0x7E968811L,0x7E968811L,0x455E0BC3L};
            int64_t l_1363 = 0x46616C59B4606141LL;
            int32_t l_1386[1];
            int i, j;
            for (i = 0; i < 1; i++)
                l_1386[i] = 0x9A5B476DL;
            if ((((((safe_sub_func_int64_t_s_s(0L, (18446744073709551615UL <= p_31))) , l_1278[0][7][5].f6) , ((p_31 = (1UL == l_1348[3][0])) && ((((l_1349 , (-5L)) ^ 0x6638D7C4L) , (void*)0) == (*g_815)))) | l_1278[0][7][5].f5) || p_31))
            { /* block id: 641 */
                struct S1 *l_1350[9] = {&g_852[5],&l_1298[3],&g_852[5],&g_852[5],&l_1298[3],&g_852[5],&g_852[5],&l_1298[3],&g_852[5]};
                int32_t l_1353 = 0L;
                int32_t l_1354 = 2L;
                int32_t l_1355 = (-1L);
                int32_t l_1356 = 0x19C930EFL;
                int32_t l_1357 = 0x9AD5457DL;
                int32_t l_1358 = 0x57CF37D7L;
                int32_t l_1359 = 0x259F00B6L;
                int32_t l_1360 = 0xE87DFED8L;
                int32_t l_1361 = 0x0B298435L;
                int32_t l_1362[10] = {0x795EF68FL,0xEC8F61A5L,0x795EF68FL,0xEC8F61A5L,0x795EF68FL,0xEC8F61A5L,0x795EF68FL,0xEC8F61A5L,0x795EF68FL,0xEC8F61A5L};
                int i;
                l_1351[0] = g_67;
                l_1351[0] = (g_117 , l_1306);
                l_1364[0]--;
            }
            else
            { /* block id: 645 */
                uint32_t l_1387 = 4294967295UL;
                struct S0 *l_1389 = &l_1278[0][5][4];
                struct S0 **l_1390 = &l_1236;
                for (g_840 = 29; (g_840 != (-8)); --g_840)
                { /* block id: 648 */
                    const int32_t *l_1372[2][9] = {{&g_104,&g_104,&l_1087[3][1][3],&g_104,&g_104,&l_1087[3][1][3],&g_104,&g_104,&l_1087[3][1][3]},{&g_104,&g_104,&l_1087[3][1][3],&g_104,&g_104,&l_1087[3][1][3],&g_104,&g_104,&l_1087[3][1][3]}};
                    const uint8_t l_1373 = 0x75L;
                    int32_t l_1388 = 0xF4E92CE2L;
                    int i, j;
                    for (g_127 = (-29); (g_127 <= 5); ++g_127)
                    { /* block id: 651 */
                        const int32_t *l_1371 = &l_1278[0][7][5].f3;
                        struct S1 **l_1376 = &g_814;
                        if (p_30)
                            break;
                        l_1372[0][3] = l_1371;
                        l_1388 = (((**g_816) = (l_1373 ^ ((l_1352[0] = 0x7747ADD5DF2CFF05LL) | (safe_div_func_uint16_t_u_u(((void*)0 == l_1376), (**g_1223)))))) < ((safe_mod_func_int64_t_s_s(((safe_lshift_func_int16_t_s_s(p_31, 4)) > (((*g_886) &= ((((safe_add_func_int8_t_s_s(((p_31 < (+(l_1386[0] = ((safe_add_func_int8_t_s_s(0L, (((0x56EF623BL || 0xA4D4D689L) > p_31) , 5UL))) || 1L)))) >= l_1387), p_31)) || 0x7FA70E5BDA46D421LL) ^ l_1349.f1) == p_31)) || p_30)), 0xA974EE92FA80DBD2LL)) ^ 0x5F847E8AL));
                    }
                    if (l_1387)
                        break;
                    if (p_30)
                        continue;
                }
                (*l_1390) = l_1389;
            }
            for (g_840 = 0; (g_840 >= 3); ++g_840)
            { /* block id: 667 */
                int32_t *l_1393 = (void*)0;
                l_1393 = &l_1160[0];
                if (l_1055)
                    continue;
                (*l_1017) = (void*)0;
                if (g_88)
                    goto lbl_1394;
            }
        }
        else
        { /* block id: 673 */
            struct S0 **l_1395 = &l_1236;
            uint8_t *l_1396 = &g_88;
            int32_t l_1401 = (-1L);
            l_1401 ^= ((l_1395 == &l_1276) ^ ((l_1160[0] = (p_30 == ((*l_1396) |= (8UL >= l_1278[0][7][5].f3)))) <= ((safe_div_func_int32_t_s_s(l_1055, 0x8DC704EBL)) , ((l_1056 &= (p_30 ^ (safe_add_func_uint16_t_u_u(((void*)0 == &l_1340[2][4][2]), 1L)))) & 0x27BA8617L))));
            (*l_1017) = &l_1401;
        }
        for (g_305 = 0; (g_305 != (-28)); g_305 = safe_sub_func_uint64_t_u_u(g_305, 6))
        { /* block id: 682 */
            int32_t l_1404 = 0L;
            int32_t l_1405 = 0L;
            int8_t l_1406 = (-10L);
            int32_t l_1407 = 0xB071480FL;
            uint64_t **l_1411 = (void*)0;
            l_1408--;
            if (g_117.f8)
                goto lbl_1394;
            l_1160[0] &= (((g_108[1][1] = (void*)0) == (void*)0) , 0L);
        }
        for (g_67.f1 = 0; (g_67.f1 == 5); g_67.f1 = safe_add_func_int16_t_s_s(g_67.f1, 9))
        { /* block id: 690 */
            int32_t l_1414 = 0x5C713967L;
            if (l_1414)
                break;
        }
    }
    g_1415 = (void*)0;
    return p_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_386 g_117.f3 g_816 g_375 g_117.f4 g_752 g_112 g_885 g_895 g_117.f0 g_713 g_117.f1 g_117.f7 g_243 g_146 g_117.f6 g_69 g_886 g_305 g_311 g_815 g_852.f0 g_419
 * writes: g_752 g_112 g_419 g_895 g_117.f0 g_156 g_713 g_117.f1 g_243 g_117.f6 g_69 g_305
 */
static int16_t  func_38(int8_t  p_39, uint16_t  p_40, struct S1  p_41)
{ /* block id: 407 */
    int32_t *l_863 = &g_752;
    int32_t l_871 = 0xD8E77DCEL;
    int32_t l_874 = 0x6A39C093L;
    int32_t *l_877[6][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
    uint16_t l_878 = 0x2B0DL;
    uint64_t l_879 = 18446744073709551611UL;
    uint32_t *l_882 = &g_112;
    int64_t *l_884 = &g_419;
    int64_t **l_883 = &l_884;
    uint32_t *l_894 = &g_895[5][7];
    uint32_t l_904 = 0UL;
    uint64_t l_905 = 0xBF30B0119387A372LL;
    int64_t l_906 = 0xFC36A52E8799B4FDLL;
    int32_t l_925 = 0x0B99B9CBL;
    int32_t l_1007 = 0xAEAA27E3L;
    int i, j;
lbl_989:
    l_879 = ((safe_div_func_int32_t_s_s((safe_rshift_func_int16_t_s_s(0x90A0L, (safe_sub_func_int32_t_s_s(((*l_863) = 0xA9F047CBL), (0x9F6125F3L && (l_878 = (~(safe_mod_func_uint8_t_u_u((p_41.f1 < (safe_sub_func_uint8_t_u_u(g_386, (safe_add_func_uint16_t_u_u(0xF013L, l_871))))), (safe_rshift_func_uint16_t_u_u((l_874 > (safe_lshift_func_uint16_t_u_u(0UL, 12))), 14))))))))))), g_117.f3)) > (**g_816));
    l_906 ^= (safe_sub_func_uint32_t_u_u(((*l_882) |= g_752), (p_40 || (p_41 , (((l_883 == g_885) >= (((safe_sub_func_int64_t_s_s(((*l_884) = (!(-5L))), ((safe_div_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u((++(*l_894)), ((*l_863) , ((safe_mod_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(((safe_div_func_int32_t_s_s((l_904 &= (p_39 == p_39)), p_40)) < g_117.f0), (*l_863))), p_41.f1)) <= p_40)))) && 0L), p_41.f1)) || p_41.f0))) < (*l_863)) <= l_905)) > p_39)))));
lbl_1002:
    for (g_117.f0 = 3; (g_117.f0 >= (-22)); g_117.f0 = safe_sub_func_int64_t_s_s(g_117.f0, 9))
    { /* block id: 418 */
        int32_t **l_909 = (void*)0;
        g_156[5][2] = &g_2[1];
    }
    for (g_713 = 0; (g_713 != (-20)); g_713 = safe_sub_func_int64_t_s_s(g_713, 2))
    { /* block id: 423 */
        int16_t *l_913 = &g_117.f1;
        int32_t l_924 = 2L;
        int32_t l_926 = 0x3C5C59AEL;
        struct S1 *l_972 = &g_67;
        int32_t l_1012 = 0x930A3C17L;
        if (((((((((*l_913) &= (p_40 | (!p_41.f0))) >= (safe_mul_func_uint16_t_u_u((p_39 , p_41.f0), p_41.f0))) ^ ((safe_sub_func_uint64_t_u_u(((18446744073709551615UL > p_40) == ((safe_div_func_uint32_t_u_u((0x49ECL | (l_924 = (safe_mod_func_uint64_t_u_u((g_117.f7 != p_39), (*l_863))))), 0xA8DEF3FCL)) >= 0x46D95621L)), l_925)) | 0x9B2B0380C2F086B4LL)) >= 0x93L) ^ p_39) <= l_926) == 0xF098L))
        { /* block id: 426 */
            int32_t **l_929 = &l_863;
            for (g_243 = 0; (g_243 <= 7); g_243 += 1)
            { /* block id: 429 */
                return g_146;
            }
            for (g_117.f6 = 0; (g_117.f6 > (-30)); g_117.f6--)
            { /* block id: 434 */
                return p_41.f0;
            }
            (*l_929) = &l_874;
        }
        else
        { /* block id: 438 */
            uint32_t l_932 = 0xA585356FL;
            const int8_t l_941 = 3L;
            uint8_t ** const l_955 = &g_660;
            struct S0 *l_973 = &g_117;
            uint16_t l_986 = 1UL;
            uint32_t l_1003 = 0xA3145A28L;
            int32_t **l_1008 = &g_156[5][2];
            int32_t l_1009 = 6L;
            int32_t l_1010[7] = {0x45A67776L,0x45A67776L,0x45A67776L,0x45A67776L,0x45A67776L,0x45A67776L,0x45A67776L};
            int32_t l_1011 = (-10L);
            uint64_t l_1013 = 18446744073709551613UL;
            int i;
            for (g_69 = (-25); (g_69 <= (-28)); g_69 = safe_sub_func_int32_t_s_s(g_69, 1))
            { /* block id: 441 */
                uint32_t l_950 = 18446744073709551614UL;
                uint32_t l_970[2];
                int32_t l_980 = 0xF881AE77L;
                int32_t l_982 = 0x53C20E9AL;
                int32_t l_983 = 0xBB778861L;
                int i;
                for (i = 0; i < 2; i++)
                    l_970[i] = 0x7D896727L;
                ++l_932;
                if ((safe_add_func_uint16_t_u_u((p_39 , ((*l_863) || (((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(p_39, ((p_41.f0 || (l_941 != ((safe_add_func_uint16_t_u_u((((safe_lshift_func_uint16_t_u_s(((((!((l_924 != ((*l_882) = ((safe_mul_func_int8_t_s_s(p_41.f0, (((!0L) , p_41.f0) && l_924))) >= 0L))) && l_950)) > l_950) , l_932) > p_39), l_950)) , (*g_886)) < p_41.f0), 0x532AL)) >= p_41.f1))) & 0xBBC5L))), l_950)) != p_40) , l_932))), 0x0847L)))
                { /* block id: 444 */
                    uint8_t **l_953 = &g_660;
                    int16_t *l_963 = (void*)0;
                    int32_t l_969 = 0L;
                    struct S1 *l_971 = &g_852[7];
                    int32_t l_984 = 0xAE0CB896L;
                    for (g_305 = 4; (g_305 >= 0); g_305 -= 1)
                    { /* block id: 447 */
                        uint8_t ***l_954 = &l_953;
                        int16_t **l_962 = &l_913;
                        uint8_t *l_966 = (void*)0;
                        uint8_t *l_967[8][1] = {{&g_727},{&g_727},{&g_727},{&g_727},{&g_727},{&g_727},{&g_727},{&g_727}};
                        int32_t l_968 = 0x8889CA5AL;
                        struct S0 **l_974[4][1] = {{&l_973},{(void*)0},{&l_973},{(void*)0}};
                        int i, j;
                        (*l_863) = (((safe_mod_func_uint32_t_u_u(((((*l_954) = l_953) == l_955) , (safe_add_func_uint16_t_u_u(((safe_add_func_int32_t_s_s((safe_rshift_func_uint8_t_u_u((((((*l_962) = (void*)0) == l_963) != l_924) < (l_968 = (safe_mod_func_uint8_t_u_u(((((l_950 < (65532UL >= p_41.f1)) < 1L) || p_41.f0) , 0UL), (*l_863))))), 1)), l_969)) & g_311), p_41.f0))), l_970[1])) == (***g_815)) < g_852[0].f0);
                        l_972 = l_971;
                        l_973 = ((p_41 , p_41.f0) , l_973);
                        l_968 = l_932;
                    }
                    for (g_243 = 25; (g_243 != 5); g_243 = safe_sub_func_uint64_t_u_u(g_243, 5))
                    { /* block id: 458 */
                        int64_t l_977 = 0x8086632F4B6E3504LL;
                        int32_t l_978 = 0xD9D558CBL;
                        int32_t l_979 = (-1L);
                        int32_t l_981 = 0xC2166D9CL;
                        int8_t l_985[7][8] = {{0x52L,0x52L,0L,9L,0L,0x52L,0x52L,0L},{0x0FL,0L,0L,0x0FL,(-1L),0x0FL,0L,0L},{0L,(-1L),9L,9L,(-1L),0L,(-1L),9L},{0x0FL,(-1L),0x0FL,0L,0L,0x0FL,(-1L),0x0FL},{0x52L,0L,9L,0L,0x52L,0x52L,0L,9L},{0x52L,0x52L,0L,9L,0L,0x52L,0x52L,0L},{0x0FL,0L,0L,0x0FL,(-1L),0x0FL,0L,0L}};
                        int i, j;
                        --l_986;
                        if (p_41.f0)
                            break;
                        if (g_117.f4)
                            goto lbl_989;
                        (*l_863) = ((safe_lshift_func_uint16_t_u_u(0x624CL, p_39)) > (safe_rshift_func_int16_t_s_s((p_41.f1 >= (safe_sub_func_uint64_t_u_u((safe_rshift_func_uint16_t_u_s(p_41.f1, (*l_863))), ((safe_mod_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u((g_419 || (-7L)), 6)), p_41.f0)) & p_40)))), p_41.f0)));
                    }
                    if (l_926)
                        goto lbl_1002;
                }
                else
                { /* block id: 465 */
                    struct S1 **l_1006 = &l_972;
                    --l_1003;
                    (*l_1006) = &p_41;
                    return l_1007;
                }
                if (l_1003)
                    break;
            }
            (*l_1008) = &g_2[0];
            (*l_1008) = (void*)0;
            --l_1013;
        }
        return p_41.f0;
    }
    return p_39;
}


/* ------------------------------------------ */
/* 
 * reads : g_375 g_117.f4
 * writes:
 */
static uint16_t  func_44(uint64_t  p_45, int8_t  p_46, struct S1  p_47, uint32_t  p_48)
{ /* block id: 404 */
    int32_t *l_853[6][6][7] = {{{(void*)0,&g_69,&g_752,&g_69,&g_2[3],&g_2[3],(void*)0},{&g_69,&g_69,&g_752,&g_69,&g_752,&g_69,&g_69},{&g_69,&g_2[3],&g_2[3],&g_752,&g_752,&g_2[0],&g_752},{&g_69,&g_2[3],(void*)0,&g_752,&g_752,(void*)0,&g_752},{&g_69,&g_69,&g_2[3],(void*)0,&g_104,&g_2[3],&g_2[3]},{&g_2[0],&g_2[1],(void*)0,&g_752,&g_69,&g_69,&g_69}},{{&g_2[0],&g_752,&g_752,&g_752,&g_104,&g_2[3],&g_752},{&g_752,&g_2[2],&g_69,&g_69,&g_69,&g_104,(void*)0},{&g_2[3],&g_2[2],&g_752,&g_2[3],&g_69,&g_2[3],&g_69},{&g_2[3],&g_2[3],&g_2[3],&g_2[3],&g_2[3],(void*)0,&g_752},{&g_2[3],&g_2[3],&g_2[3],&g_69,&g_752,&g_104,&g_752},{&g_752,&g_2[3],&g_69,&g_2[3],&g_69,(void*)0,&g_752}},{{&g_2[2],(void*)0,&g_2[3],(void*)0,&g_2[3],&g_2[3],&g_69},{&g_752,&g_2[0],&g_69,&g_2[3],&g_69,&g_104,(void*)0},{&g_2[3],&g_104,&g_104,&g_2[3],&g_69,&g_752,&g_752},{&g_752,&g_752,&g_2[3],&g_69,&g_752,&g_69,&g_69},{&g_69,&g_2[3],&g_69,&g_104,&g_104,&g_69,&g_2[3]},{&g_752,(void*)0,(void*)0,&g_2[0],&g_104,(void*)0,&g_752}},{{&g_2[3],&g_69,&g_2[3],&g_2[2],&g_104,&g_752,&g_752},{&g_69,&g_752,&g_104,&g_2[0],&g_752,&g_752,(void*)0},{&g_752,&g_752,(void*)0,&g_104,&g_104,(void*)0,&g_2[3]},{&g_2[3],&g_104,&g_752,&g_69,&g_752,&g_2[3],&g_69},{&g_104,&g_752,(void*)0,&g_2[3],&g_2[3],&g_104,&g_69},{&g_752,&g_752,&g_69,&g_2[3],(void*)0,&g_2[3],&g_2[3]}},{{&g_104,(void*)0,&g_2[3],(void*)0,&g_104,&g_752,&g_752},{(void*)0,&g_69,&g_69,&g_2[3],(void*)0,&g_69,&g_2[3]},{&g_104,&g_2[3],&g_2[3],&g_69,&g_752,&g_104,&g_69},{(void*)0,&g_2[3],&g_752,&g_2[3],&g_2[3],&g_69,&g_2[1]},{&g_104,&g_69,&g_2[0],&g_2[3],(void*)0,&g_2[3],&g_752},{&g_752,(void*)0,&g_69,&g_69,&g_69,&g_752,&g_104}},{{&g_104,&g_69,(void*)0,&g_69,&g_104,&g_752,&g_2[3]},{&g_752,&g_69,&g_752,&g_752,&g_2[0],&g_752,&g_752},{&g_752,&g_752,&g_69,&g_2[3],&g_752,&g_2[3],&g_69},{&g_104,&g_752,&g_2[3],(void*)0,&g_752,&g_2[1],&g_2[0]},{&g_104,&g_2[3],(void*)0,(void*)0,&g_752,(void*)0,&g_2[3]},{&g_2[3],&g_752,&g_752,&g_752,&g_2[0],&g_69,(void*)0}}};
    uint32_t l_854 = 8UL;
    int i, j, k;
    --l_854;
    return (*g_375);
}


/* ------------------------------------------ */
/* 
 * reads : g_117.f1 g_117.f8 g_104 g_455 g_88 g_375 g_117.f4 g_117.f2 g_752 g_67.f1 g_112 g_69 g_419 g_127 g_815 g_2 g_840 g_386 g_311 g_727 g_101 g_146 g_117.f6
 * writes: g_69 g_67.f1 g_386 g_104 g_88 g_146 g_714 g_156 g_117.f4 g_749 g_752 g_117.f6 g_112 g_455 g_117.f1 g_814 g_840 g_311
 */
static int64_t  func_55(uint8_t  p_56, struct S1  p_57, const uint64_t  p_58, const uint32_t  p_59)
{ /* block id: 286 */
    int8_t ***l_716 = &g_188;
    int32_t l_746 = 0x9DFB7D55L;
    uint16_t l_835 = 65535UL;
    int32_t *l_847[10][10][2] = {{{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0}},{{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0}},{{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]}},{{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0}},{{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0}},{{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]}},{{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0}},{{&g_2[3],(void*)0},{(void*)0,&g_2[3]},{(void*)0,(void*)0},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]}},{{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69}},{{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]},{&g_69,&g_2[3]},{&g_2[3],&g_69},{&g_2[3],&g_2[3]}}};
    int i, j, k;
lbl_753:
    for (g_69 = 0; (g_69 <= (-18)); --g_69)
    { /* block id: 289 */
        for (g_67.f1 = (-12); (g_67.f1 != 51); g_67.f1 = safe_add_func_uint32_t_u_u(g_67.f1, 1))
        { /* block id: 292 */
            for (g_386 = 0; (g_386 <= 1); g_386 += 1)
            { /* block id: 295 */
                int32_t *l_701 = &g_104;
                (*l_701) = (g_117.f1 > 0x02L);
            }
        }
        for (g_88 = 0; (g_88 >= 16); ++g_88)
        { /* block id: 301 */
            uint8_t l_708 = 0xC6L;
            for (g_146 = 16; (g_146 > 8); g_146--)
            { /* block id: 304 */
                int32_t *l_706[10][1] = {{&g_2[2]},{&g_2[2]},{&g_2[3]},{&g_104},{&g_2[3]},{&g_2[2]},{&g_2[2]},{&g_2[3]},{&g_104},{&g_2[3]}};
                int64_t l_707 = 0x127E855BAE4EACC3LL;
                int i, j;
                ++l_708;
            }
        }
        return g_117.f8;
    }
    for (g_69 = 4; (g_69 >= 0); g_69 -= 1)
    { /* block id: 312 */
        int32_t *l_712 = &g_713;
        int32_t **l_711 = &l_712;
        int32_t l_740 = (-10L);
        uint16_t **l_744[5][10] = {{&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375},{(void*)0,(void*)0,&g_375,&g_375,&g_375,&g_375,&g_375,(void*)0,(void*)0,&g_375},{&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375},{&g_375,(void*)0,&g_375,&g_375,&g_375,&g_375,&g_375,&g_375,(void*)0,&g_375},{&g_375,&g_375,(void*)0,&g_375,(void*)0,&g_375,&g_375,&g_375,&g_375,(void*)0}};
        uint16_t ***l_743 = &l_744[2][0];
        int32_t l_808 = (-1L);
        const int32_t l_809 = 9L;
        struct S1 l_810 = {57,1UL};
        int i, j;
        g_714 = l_711;
        for (g_67.f1 = 0; (g_67.f1 <= 7); g_67.f1 += 1)
        { /* block id: 316 */
            int8_t ****l_717 = &l_716;
            int8_t ***l_718 = &g_188;
            int32_t *l_719 = &g_104;
            int32_t **l_720 = &g_156[5][2];
            (*l_719) ^= (((*l_717) = l_716) == (l_718 = (void*)0));
            (*l_720) = &g_69;
            for (g_386 = 0; (g_386 <= 7); g_386 += 1)
            { /* block id: 323 */
                uint64_t l_728 = 18446744073709551615UL;
                for (p_56 = 2; (p_56 <= 7); p_56 += 1)
                { /* block id: 326 */
                    uint8_t *l_723 = &g_88;
                    uint8_t *l_726[8] = {(void*)0,&g_727,(void*)0,(void*)0,&g_727,(void*)0,(void*)0,&g_727};
                    int8_t ****l_735 = &l_716;
                    int i;
                    if ((safe_sub_func_int16_t_s_s(((g_455[p_56] >= 4L) != (4294967290UL <= (((*l_723)--) | (l_728++)))), ((*g_375)--))))
                    { /* block id: 330 */
                        struct S1 *l_734 = &g_67;
                        struct S1 **l_733 = &l_734;
                        g_156[5][2] = &g_104;
                        (*l_733) = &g_67;
                    }
                    else
                    { /* block id: 333 */
                        int64_t *l_745 = (void*)0;
                        int8_t ****l_747 = &l_716;
                        int8_t *****l_748[9];
                        int32_t *l_751 = &g_752;
                        int i;
                        for (i = 0; i < 9; i++)
                            l_748[i] = (void*)0;
                        (*l_751) |= ((l_735 = l_735) == (g_749 = ((((((safe_div_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(((((g_455[p_56] <= 0x8CL) >= (g_104 != l_740)) , (l_746 = ((((safe_lshift_func_uint16_t_u_u(((void*)0 == &g_108[1][1]), ((void*)0 == l_743))) , 4294967290UL) ^ 0UL) <= l_728))) <= g_117.f2), 9)), 0xC6L)) , (void*)0) == (void*)0) < p_59) && p_59) , l_747)));
                    }
                }
                (*l_720) = &l_740;
            }
        }
        for (g_117.f6 = 5; (g_117.f6 >= 1); g_117.f6 -= 1)
        { /* block id: 345 */
            int8_t l_791 = (-1L);
            int64_t l_806 = (-1L);
            int32_t l_821 = 0xB9723DFEL;
            int32_t *l_844 = &g_752;
            for (p_56 = 0; (p_56 <= 4); p_56 += 1)
            { /* block id: 348 */
                uint8_t l_757 = 248UL;
                int32_t l_818 = 0xA9DAC3A7L;
                int16_t *l_841 = &g_386;
                int32_t *l_842 = (void*)0;
                int32_t *l_843[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_843[i] = &l_818;
                if (g_67.f1)
                    goto lbl_753;
                for (g_88 = 0; (g_88 <= 1); g_88 += 1)
                { /* block id: 352 */
                    int32_t *l_754 = &g_752;
                    int32_t *l_755 = &l_746;
                    int32_t *l_756[4];
                    int i, j;
                    for (i = 0; i < 4; i++)
                        l_756[i] = &l_746;
                    ++l_757;
                }
                for (g_104 = 1; (g_104 <= 4); g_104 += 1)
                { /* block id: 357 */
                    uint16_t l_760 = 0x6A2EL;
                    int32_t l_761 = 0xCA74C791L;
                    uint32_t *l_762 = &g_112;
                    int16_t *l_792 = &g_117.f1;
                    int32_t *l_822 = (void*)0;
                    int32_t *l_823 = &l_761;
                    int32_t *l_824 = &l_821;
                    int32_t *l_825 = (void*)0;
                    int32_t *l_826 = &g_752;
                    int32_t *l_827 = &l_808;
                    int32_t *l_828 = &l_740;
                    int32_t *l_829 = (void*)0;
                    int32_t *l_830 = &l_746;
                    int32_t *l_831 = &l_740;
                    int32_t *l_832 = &g_752;
                    int32_t *l_833 = &l_746;
                    int32_t *l_834[10][1] = {{&l_818},{&g_2[0]},{&l_818},{&l_818},{&g_2[0]},{&l_818},{&l_818},{&g_2[0]},{&l_818},{&l_818}};
                    int i, j;
                    if (((l_761 = l_760) || (((*l_762)--) || ((safe_unary_minus_func_int32_t_s((((safe_rshift_func_uint16_t_u_s((g_69 >= ((*l_792) = (((((p_57.f0 <= (safe_add_func_int16_t_s_s((((safe_mod_func_int64_t_s_s(((p_57.f1 <= ((safe_rshift_func_int8_t_s_s(((safe_sub_func_uint64_t_u_u((l_746 || (safe_rshift_func_int8_t_s_s((safe_div_func_int32_t_s_s((!(((p_57.f1 != (((safe_rshift_func_int16_t_s_u(p_57.f0, 7)) >= (safe_rshift_func_int16_t_s_u(l_746, (g_455[(p_56 + 2)]++)))) > (safe_add_func_uint64_t_u_u((0x42L <= p_58), l_791)))) == (-10L)) , l_740)), (-2L))), p_57.f0))), (-2L))) == p_56), 7)) < l_757)) == p_59), p_57.f1)) && 0UL) ^ p_57.f0), 7L))) , 18446744073709551607UL) , p_56) ^ p_58) < p_56))), g_419)) < p_56) < p_57.f0))) , 0xDA3BBF69L))))
                    { /* block id: 362 */
                        int8_t l_803 = 2L;
                        int32_t *l_807[8][10];
                        int i, j;
                        for (i = 0; i < 8; i++)
                        {
                            for (j = 0; j < 10; j++)
                                l_807[i][j] = &g_752;
                        }
                        l_761 = ((0xD948F821L ^ (l_760 > ((safe_lshift_func_int8_t_s_s((l_746 = (((safe_rshift_func_int16_t_s_s(p_59, 14)) | ((g_455[(p_56 + 2)] >= (((l_740 = (((l_808 ^= (((*g_375) |= (safe_sub_func_uint8_t_u_u(7UL, (safe_rshift_func_int8_t_s_s(((l_740 != ((l_803 = (safe_lshift_func_int16_t_s_u(p_57.f0, 15))) >= (((((safe_add_func_int64_t_s_s(6L, 1L)) & p_57.f0) ^ l_806) >= 1UL) ^ 0L))) || (-7L)), 2))))) < l_746)) >= 5UL) > 0x974840B4L)) >= 0x49AD31B837AF167BLL) <= 4UL)) && 65528UL)) ^ l_809)), 6)) ^ g_127))) && 0x28D9FED6B403202ALL);
                        l_761 = (l_810 , p_57.f0);
                        if (p_56)
                            break;
                    }
                    else
                    { /* block id: 371 */
                        uint16_t l_811 = 1UL;
                        uint16_t ****l_817 = &l_743;
                        int32_t *l_820[10][1][9] = {{{(void*)0,&l_740,&l_818,&l_818,&l_740,(void*)0,&l_761,&l_808,&g_104}},{{&g_2[0],&l_761,&l_740,(void*)0,&g_2[2],&g_2[2],(void*)0,&l_740,&l_761}},{{&l_740,&g_69,(void*)0,&l_761,&l_740,&l_761,&l_761,&l_761,&l_761}},{{&g_104,&l_740,&g_2[3],&l_740,&g_104,(void*)0,(void*)0,&g_2[1],(void*)0}},{{&g_69,&g_69,&l_761,&l_818,&l_808,&l_818,&l_761,&g_69,&g_69}},{{&g_69,&l_761,(void*)0,&g_104,&g_2[1],(void*)0,&g_2[2],(void*)0,&g_2[1]}},{{&g_104,&l_740,&l_740,&g_104,&l_818,&g_69,&l_761,&l_818,&l_808}},{{&l_761,&l_740,(void*)0,&g_2[2],&g_2[2],(void*)0,&l_740,&l_761,&g_2[0]}},{{&l_761,&l_740,&l_808,&l_761,&g_69,&l_818,&l_818,&g_69,&l_761}},{{&g_104,&g_69,&g_104,(void*)0,&l_740,&g_2[2],&g_104,&g_2[0],&g_2[0]}}};
                        int i, j, k;
                        l_811++;
                        g_814 = &p_57;
                        l_818 = (g_815 == (p_58 , ((*l_817) = (void*)0)));
                        l_821 = (~0x579F20D3L);
                    }
                    --l_835;
                }
                l_821 = (p_56 ^ (l_746 = (safe_sub_func_int16_t_s_s(0xD1B1L, (l_810.f1 && ((*l_841) |= (l_740 , (g_840 ^= g_2[3]))))))));
            }
            (*l_844) = (-8L);
            for (g_311 = 2; (g_311 <= 7); g_311 += 1)
            { /* block id: 388 */
                int32_t * const l_845 = &l_808;
                int32_t **l_846 = &l_844;
                if (p_57.f1)
                    goto lbl_753;
                if (p_59)
                    break;
                (*l_846) = l_845;
                (*l_846) = l_847[8][2][0];
            }
            l_821 &= p_59;
        }
    }
    if (p_57.f0)
    { /* block id: 397 */
        int32_t l_848 = 0x6756D986L;
        return l_848;
    }
    else
    { /* block id: 399 */
        uint32_t *l_849 = &g_67.f1;
        int32_t l_850[9][6] = {{0x9B7F3BC2L,5L,0xB68E2DD5L,0xB68E2DD5L,5L,0x9B7F3BC2L},{0xB68E2DD5L,5L,0x9B7F3BC2L,0x28985619L,(-1L),0x5F742094L},{0x5F742094L,0xA1FABE16L,0x6C4F1FDBL,0xA1FABE16L,0x5F742094L,5L},{0x5F742094L,0xB68E2DD5L,0xA1FABE16L,0x28985619L,0x1C6A0209L,0x1C6A0209L},{0xB68E2DD5L,(-1L),(-1L),0xB68E2DD5L,0x6C4F1FDBL,0x1C6A0209L},{0x9B7F3BC2L,0x1C6A0209L,0xA1FABE16L,5L,0x28985619L,5L},{0x6C4F1FDBL,6L,0x6C4F1FDBL,0x6D5972E2L,0x28985619L,0x5F742094L},{0xA1FABE16L,0x1C6A0209L,0x9B7F3BC2L,0x6C4F1FDBL,0x6C4F1FDBL,0x9B7F3BC2L},{(-1L),(-1L),0xB68E2DD5L,0x6C4F1FDBL,0x1C6A0209L,0x6D5972E2L}};
        int i, j;
        l_850[4][4] ^= (((*l_849) = g_727) , p_57.f1);
    }
    return g_101;
}


/* ------------------------------------------ */
/* 
 * reads : g_90 g_67.f1 g_2 g_117 g_104 g_69 g_127 g_101 g_108 g_67 g_146 g_179 g_188 g_243 g_375 g_107 g_88 g_311 g_624 g_109 g_455 g_660
 * writes: g_69 g_101 g_104 g_107 g_67.f0 g_127 g_146 g_117.f0 g_156 g_108 g_179 g_117.f1 g_188 g_117.f6 g_117.f4 g_243 g_117.f2 g_109 g_112 g_88 g_311 g_67.f1 g_660 g_386
 */
static uint8_t  func_60(uint16_t  p_61, int32_t  p_62, int64_t  p_63, uint16_t  p_64)
{ /* block id: 10 */
    uint16_t l_93 = 0x20CEL;
    int16_t *l_94[7][6][6] = {{{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,(void*)0,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,(void*)0,&g_90}},{{&g_90,(void*)0,&g_90,&g_90,&g_90,&g_90},{&g_90,(void*)0,&g_90,&g_90,(void*)0,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,(void*)0,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90}},{{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,(void*)0,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90}},{{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90}},{{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90}},{{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90}},{{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90},{&g_90,&g_90,&g_90,&g_90,&g_90,&g_90}}};
    uint8_t *l_96 = (void*)0;
    uint64_t *l_97 = (void*)0;
    int8_t l_98 = 0xEBL;
    int32_t *l_99 = &g_69;
    uint64_t l_100 = 0xA6E977603CC2ED11LL;
    int32_t l_102 = 0x5E5E7DF3L;
    int32_t *l_103 = &g_104;
    uint64_t **l_147 = &g_108[0][0];
    int32_t l_199[8];
    uint64_t l_205 = 0x56468E3BE8019E2ELL;
    uint32_t l_241[4];
    int64_t l_331 = 0x125DACD163C3191CLL;
    uint8_t l_335 = 0x98L;
    struct S0 *l_393 = &g_117;
    int64_t l_405 = 0L;
    int64_t l_502 = 0x78395CC4C92B0F0ALL;
    struct S1 l_521 = {42,0x2A51389EL};
    uint16_t l_603 = 1UL;
    int32_t l_627 = 0L;
    struct S0 * const l_683 = &g_117;
    int32_t *l_691[4] = {&l_199[0],&l_199[0],&l_199[0],&l_199[0]};
    int16_t l_692 = 0x0F59L;
    uint64_t l_693[10] = {0x43EF2E26B740A50ELL,1UL,0xBC81794A0F0AFC87LL,0xBC81794A0F0AFC87LL,1UL,0x43EF2E26B740A50ELL,1UL,0xBC81794A0F0AFC87LL,0xBC81794A0F0AFC87LL,1UL};
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_199[i] = (-6L);
    for (i = 0; i < 4; i++)
        l_241[i] = 1UL;
    if (((*l_103) = ((l_102 = ((g_101 = (((safe_sub_func_uint8_t_u_u(((g_90 , p_63) | (((l_93 , l_94[3][1][3]) != (((*l_99) = ((~l_93) , (g_67.f1 & (g_2[3] && (((((0x43L ^ ((void*)0 == l_96)) , (void*)0) != l_97) >= g_90) >= l_98))))) , l_94[3][1][3])) <= 6UL)), 0UL)) >= l_100) != 3UL)) || p_64)) & p_61)))
    { /* block id: 15 */
        uint64_t **l_105 = (void*)0;
        uint64_t ***l_106[1];
        uint32_t *l_111[7][8] = {{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112},{&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112,&g_112}};
        int32_t l_113 = 0xD1E45324L;
        int32_t l_114 = (-7L);
        int64_t l_181[3];
        int32_t l_200 = 0xBC1D56B4L;
        int32_t l_202 = 0x9A9BC9FAL;
        int32_t l_203[8][9][3] = {{{7L,0L,0xDE1EE176L},{(-1L),0x9BF4135FL,0x854730C7L},{0x9BF4135FL,0L,0xFA9D3D98L},{0L,0x723CBD7EL,1L},{0L,0L,1L},{1L,(-1L),0xFA9D3D98L},{1L,0L,0x854730C7L},{0L,9L,0xDE1EE176L},{1L,0x08C03050L,0xB302614CL}},{{1L,1L,(-1L)},{0L,1L,1L},{0L,0x08C03050L,0xCDA35C18L},{0x9BF4135FL,9L,0L},{(-1L),0L,0xCDA35C18L},{7L,(-1L),1L},{0x70F4BDAFL,0L,(-1L)},{0x70F4BDAFL,0x723CBD7EL,0xB302614CL},{7L,0L,0xDE1EE176L}},{{(-1L),0x9BF4135FL,0x854730C7L},{0x9BF4135FL,0L,0xFA9D3D98L},{0L,0x723CBD7EL,1L},{0L,0L,1L},{1L,(-1L),0xFA9D3D98L},{1L,0L,0x854730C7L},{0L,9L,0xDE1EE176L},{1L,0x08C03050L,0xB302614CL},{1L,1L,(-1L)}},{{0L,1L,1L},{0L,0x08C03050L,0xCDA35C18L},{0x9BF4135FL,9L,0L},{(-1L),0L,0xCDA35C18L},{7L,(-1L),1L},{0x70F4BDAFL,0L,(-1L)},{0x70F4BDAFL,0x723CBD7EL,0xB302614CL},{7L,0L,0xDE1EE176L},{(-1L),0x9BF4135FL,0x854730C7L}},{{0x9BF4135FL,0L,0xFA9D3D98L},{0L,0x723CBD7EL,1L},{0L,0L,1L},{1L,(-1L),0xFA9D3D98L},{1L,0L,0x854730C7L},{0L,9L,0xDE1EE176L},{1L,0x08C03050L,0xB302614CL},{1L,1L,(-1L)},{0L,1L,1L}},{{0L,0x08C03050L,0xCDA35C18L},{0x9BF4135FL,9L,0L},{(-1L),0L,0xCDA35C18L},{7L,(-1L),1L},{0x70F4BDAFL,0L,(-1L)},{0x70F4BDAFL,0x723CBD7EL,0xB302614CL},{7L,0L,0xDE1EE176L},{(-1L),0x9BF4135FL,0x854730C7L},{0x9BF4135FL,0L,0xFA9D3D98L}},{{0L,0x723CBD7EL,1L},{0x3F050AB9L,0x6B952414L,0x1862B91AL},{0x15B5CB29L,0x85255F6BL,0L},{0x05E1E163L,(-1L),(-6L)},{0x6B952414L,0x9A809641L,0L},{0x05E1E163L,2L,0L},{0x15B5CB29L,0x15B5CB29L,9L},{0x3F050AB9L,0x15B5CB29L,(-6L)},{(-1L),2L,(-1L)}},{{3L,0x9A809641L,1L},{0x8666C923L,(-1L),(-1L)},{2L,0x85255F6BL,(-6L)},{0x6BF210CAL,0x6B952414L,9L},{0x6BF210CAL,(-1L),0L},{2L,0x3F050AB9L,0L},{0x8666C923L,3L,(-6L)},{3L,0x3F050AB9L,0L},{(-1L),(-1L),0x1862B91AL}}};
        int16_t l_204 = 0x01ABL;
        int64_t l_212 = (-8L);
        int8_t ***l_225 = &g_188;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_106[i] = &l_105;
        for (i = 0; i < 3; i++)
            l_181[i] = 0x82581418936BB2FCLL;
        g_107 = l_105;
        if ((!((g_67.f0 = (l_114 = (l_113 = g_90))) , ((((*l_99) = 0L) < (safe_add_func_int16_t_s_s(((((g_117 , (safe_div_func_int32_t_s_s(3L, ((*l_103) |= (-1L))))) , ((-1L) | (safe_rshift_func_uint8_t_u_s(g_117.f8, (g_117.f3 & p_62))))) , p_63) && g_117.f7), 9UL))) || g_69))))
        { /* block id: 22 */
            int32_t *l_122 = &l_102;
            int32_t *l_123 = (void*)0;
            int32_t *l_124 = &l_102;
            int32_t *l_125 = &g_69;
            int32_t *l_126[4] = {&l_114,&l_114,&l_114,&l_114};
            int16_t *l_154[8][5][6] = {{{(void*)0,&g_90,&g_90,&g_90,&g_117.f1,(void*)0},{(void*)0,&g_117.f6,&g_90,(void*)0,&g_117.f6,&g_117.f6},{&g_90,&g_90,(void*)0,&g_117.f1,&g_90,&g_117.f6},{&g_90,&g_90,(void*)0,&g_90,&g_90,&g_90},{&g_117.f1,(void*)0,&g_90,&g_117.f6,&g_90,(void*)0}},{{&g_90,&g_90,&g_117.f6,(void*)0,(void*)0,(void*)0},{&g_90,(void*)0,&g_90,&g_117.f6,&g_117.f6,&g_90},{(void*)0,(void*)0,(void*)0,&g_117.f1,&g_117.f6,&g_117.f6},{&g_117.f6,(void*)0,(void*)0,&g_117.f6,&g_90,&g_117.f6},{(void*)0,&g_117.f6,&g_90,&g_90,(void*)0,(void*)0}},{{(void*)0,&g_90,&g_90,&g_117.f6,(void*)0,&g_90},{&g_90,&g_117.f6,&g_117.f1,(void*)0,&g_90,&g_90},{&g_90,(void*)0,&g_117.f1,&g_90,&g_117.f6,&g_90},{&g_117.f6,(void*)0,&g_117.f6,&g_90,&g_117.f6,&g_117.f6},{&g_117.f6,(void*)0,&g_117.f6,(void*)0,(void*)0,&g_117.f1}},{{&g_117.f6,&g_90,(void*)0,(void*)0,&g_90,&g_90},{&g_117.f6,(void*)0,&g_90,&g_90,&g_90,(void*)0},{&g_117.f6,&g_90,&g_117.f6,&g_90,&g_90,&g_117.f6},{&g_90,&g_90,(void*)0,(void*)0,&g_117.f6,&g_90},{&g_90,&g_117.f6,&g_117.f6,&g_117.f6,&g_117.f1,(void*)0}},{{(void*)0,&g_90,&g_117.f6,&g_90,&g_90,&g_90},{(void*)0,&g_90,(void*)0,&g_117.f6,(void*)0,&g_117.f6},{&g_117.f6,(void*)0,&g_117.f6,(void*)0,&g_90,&g_90},{(void*)0,&g_117.f6,&g_90,&g_117.f6,&g_117.f6,(void*)0},{&g_117.f6,(void*)0,&g_90,&g_90,&g_90,&g_90}},{{(void*)0,(void*)0,&g_117.f6,(void*)0,&g_117.f6,(void*)0},{(void*)0,&g_117.f6,(void*)0,&g_117.f6,&g_90,(void*)0},{&g_117.f1,&g_90,&g_90,&g_90,&g_90,&g_117.f1},{&g_117.f6,&g_90,(void*)0,&g_117.f6,(void*)0,(void*)0},{&g_90,(void*)0,&g_117.f6,&g_117.f6,&g_90,(void*)0}},{{&g_90,&g_90,&g_117.f6,&g_117.f6,(void*)0,&g_117.f6},{&g_117.f6,(void*)0,&g_90,&g_90,(void*)0,&g_117.f6},{&g_117.f1,&g_117.f6,&g_90,&g_117.f6,&g_117.f1,&g_90},{(void*)0,&g_90,&g_117.f1,(void*)0,(void*)0,&g_117.f6},{(void*)0,&g_117.f1,&g_117.f6,&g_90,(void*)0,&g_117.f6}},{{&g_117.f6,(void*)0,&g_117.f1,&g_117.f6,&g_117.f6,&g_90},{(void*)0,&g_117.f1,&g_90,(void*)0,&g_117.f6,&g_117.f6},{&g_117.f6,&g_90,&g_90,&g_117.f6,&g_90,&g_117.f6},{&g_90,(void*)0,&g_117.f6,&g_90,&g_117.f6,(void*)0},{(void*)0,&g_90,&g_117.f6,&g_117.f6,&g_117.f6,(void*)0}}};
            int i, j, k;
            g_127--;
            for (l_100 = 2; (l_100 <= 6); l_100 += 1)
            { /* block id: 26 */
                uint64_t *l_136 = &g_117.f7;
                uint64_t l_138 = 0xEA0BD5792D6E4BE9LL;
                uint8_t *l_143[8];
                int32_t *l_157 = &g_69;
                int i;
                for (i = 0; i < 8; i++)
                    l_143[i] = &g_88;
                if (g_117.f1)
                { /* block id: 27 */
                    int64_t *l_144 = (void*)0;
                    int64_t *l_145 = &g_146;
                    int8_t *l_148[6];
                    int32_t l_149 = 0x94937C96L;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_148[i] = &l_98;
                    (*l_122) &= (0x90L != 0xEFL);
                    (*l_122) = ((safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s((safe_rshift_func_int8_t_s_s((&l_100 == l_136), (safe_unary_minus_func_int64_t_s(l_138)))), p_61)), (0xCBB879D152B013A5LL | ((safe_div_func_int8_t_s_s((safe_div_func_int8_t_s_s(((g_117.f0 = ((((l_114 >= ((*l_145) = (l_143[1] != &g_88))) && ((l_147 = &l_97) != (void*)0)) ^ p_64) ^ 0xEAB3F101L)) , l_149), g_117.f2)), 0x15L)) & l_114)))) , l_138);
                }
                else
                { /* block id: 33 */
                    int32_t *l_150[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_150[i] = (void*)0;
                    l_150[5] = l_124;
                }
                for (p_62 = 0; (p_62 <= 1); p_62 += 1)
                { /* block id: 38 */
                    int i;
                    for (l_138 = 0; (l_138 <= 5); l_138 += 1)
                    { /* block id: 41 */
                        int16_t **l_151 = (void*)0;
                        int16_t **l_152 = (void*)0;
                        int16_t **l_153 = &l_94[6][1][3];
                        int32_t **l_155[4][1];
                        int i, j, k;
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_155[i][j] = &l_125;
                        }
                        (*l_124) = ((((*l_153) = l_94[(l_138 + 1)][(p_62 + 2)][l_138]) != l_154[5][4][2]) , g_117.f5);
                        l_126[(p_62 + 2)] = l_111[l_100][(l_100 + 1)];
                        g_156[5][2] = &l_114;
                        l_157 = (void*)0;
                    }
                    (*l_124) = 0x44F0CC1BL;
                }
            }
        }
        else
        { /* block id: 51 */
            uint16_t l_158 = 0x9EACL;
            int8_t *l_169 = &g_117.f0;
            int64_t *l_175 = &g_146;
            int64_t *l_176 = (void*)0;
            int64_t *l_177 = (void*)0;
            int64_t *l_178[6] = {&g_179,&g_179,&g_179,&g_179,&g_179,&g_179};
            int16_t l_180 = 0x578BL;
            int32_t l_201[2];
            struct S0 l_216 = {0x31L,0x326EL,1UL,9L,5UL,0x0701L,3L,0x50C56BD4BA926959LL,1UL};
            int8_t * const *l_224 = &l_169;
            int8_t * const * const *l_223 = &l_224;
            int i;
            for (i = 0; i < 2; i++)
                l_201[i] = 0L;
            if ((((*l_103) = l_158) >= (g_101 , ((((*l_147) = (*l_147)) == ((!((l_158 & (safe_mod_func_int16_t_s_s((g_127 != (~(l_114 = (safe_lshift_func_int8_t_s_s(0x39L, 0))))), (g_117.f1 ^= ((safe_mod_func_uint8_t_u_u((g_67 , (safe_mul_func_int8_t_s_s(((*l_169) = 0x7DL), (!(g_179 ^= (safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(((*l_175) ^= l_113), 0xB7F66EBC83D46320LL)), p_62))))))), l_180)) >= l_181[2]))))) <= (-9L))) , l_175)) != 0xA4L))))
            { /* block id: 59 */
                uint32_t l_184 = 3UL;
                int32_t l_196 = 0x5A33BAAAL;
                int32_t l_197 = 2L;
                int32_t l_198[2];
                int32_t *l_208 = &l_203[1][0][1];
                int32_t *l_209 = &l_201[1];
                int32_t *l_210 = &l_200;
                int32_t *l_211[3][10][1] = {{{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102}},{{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102}},{{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102},{&l_198[1]},{&l_102}}};
                uint32_t l_213 = 18446744073709551606UL;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_198[i] = 0xCEE7FFB0L;
                for (g_117.f1 = 0; (g_117.f1 <= (-5)); g_117.f1 = safe_sub_func_int32_t_s_s(g_117.f1, 8))
                { /* block id: 62 */
                    int32_t **l_187 = &l_103;
                    l_184--;
                    (*l_187) = (void*)0;
                }
                for (g_117.f1 = 6; (g_117.f1 >= 0); g_117.f1 -= 1)
                { /* block id: 68 */
                    int8_t ***l_190[6] = {&g_188,&g_188,&g_188,&g_188,&g_188,&g_188};
                    int i;
                    g_188 = g_188;
                    for (l_184 = 0; (l_184 <= 6); l_184 += 1)
                    { /* block id: 72 */
                        if (l_114)
                            break;
                    }
                }
                for (g_117.f6 = (-9); (g_117.f6 < 4); ++g_117.f6)
                { /* block id: 78 */
                    int32_t *l_193 = &g_104;
                    int32_t *l_194 = &l_102;
                    int32_t *l_195[7][9][4] = {{{&l_114,&g_2[3],(void*)0,(void*)0},{&g_69,&g_104,(void*)0,&g_2[3]},{&g_104,(void*)0,&l_102,&g_2[3]},{&g_2[2],&g_104,&l_113,(void*)0},{&g_2[3],&g_2[3],&g_2[3],&g_2[3]},{&g_2[3],(void*)0,&l_114,(void*)0},{&g_104,&g_2[3],&g_2[3],&g_2[2]},{&g_2[0],&g_69,&l_114,&l_114},{&l_102,&g_69,&g_2[3],&g_2[3]}},{{&l_114,&g_2[3],&g_104,&l_113},{&g_69,&l_113,&g_2[2],&l_102},{&g_2[2],&g_69,&g_2[3],(void*)0},{&g_2[3],&l_113,(void*)0,&g_2[3]},{&l_114,&g_2[2],&g_2[3],&l_113},{&l_114,(void*)0,&g_2[2],(void*)0},{(void*)0,&l_102,&l_114,&g_2[3]},{&g_69,&g_2[3],&g_2[3],(void*)0},{&l_113,&l_114,&g_69,&g_2[3]}},{{&l_113,&g_69,&g_2[3],&g_104},{&g_69,&g_2[3],&l_114,(void*)0},{(void*)0,&g_2[3],&g_2[2],&g_2[3]},{&l_114,&l_114,&g_2[3],&g_104},{&l_114,&l_114,(void*)0,&l_114},{&g_2[3],&l_102,&g_2[3],&g_2[2]},{&g_2[2],&g_2[3],&l_113,&g_2[3]},{&g_2[3],(void*)0,&g_2[2],&g_2[3]},{&g_104,&l_114,&l_114,&l_113}},{{&l_102,(void*)0,&g_104,&g_69},{&g_2[3],(void*)0,(void*)0,&g_2[3]},{&g_69,&l_102,&g_69,&g_69},{&g_2[3],&g_2[3],&g_2[3],&g_69},{(void*)0,&g_2[3],&g_2[3],&g_2[3]},{&g_2[3],&l_113,&g_2[3],&g_2[3]},{&l_102,&l_113,&g_104,&g_2[3]},{&l_113,&g_2[3],&l_113,&g_69},{&g_2[3],&g_2[3],&g_2[3],&g_69}},{{&l_114,&l_102,&l_113,&g_2[3]},{&g_2[3],(void*)0,(void*)0,&g_69},{&l_113,(void*)0,&g_2[3],&l_113},{&g_104,&l_114,&l_102,&g_2[3]},{&g_2[3],(void*)0,&g_2[3],&g_2[3]},{&g_2[0],&g_2[3],(void*)0,&g_2[2]},{&g_104,&l_102,(void*)0,&l_114},{&l_102,&l_114,&l_114,&g_104},{(void*)0,&l_114,&g_2[3],&g_2[3]}},{{&l_114,&g_2[3],&l_114,(void*)0},{&g_2[3],&g_2[3],&l_114,&g_104},{&l_114,&g_69,&g_2[3],&g_2[3]},{&l_114,&l_114,&g_2[3],(void*)0},{&l_114,&g_2[3],&l_114,&g_2[3]},{&g_2[3],&l_102,&l_114,(void*)0},{&l_114,(void*)0,&g_2[3],&l_113},{(void*)0,&g_2[2],&l_114,&g_2[3]},{&l_102,&l_113,(void*)0,(void*)0}},{{&g_104,&g_69,(void*)0,&g_2[2]},{&g_2[0],&g_2[3],&g_2[3],&l_114},{&g_2[3],(void*)0,&l_102,&l_114},{&g_104,&g_2[3],&g_2[3],&l_113},{&l_113,&g_2[3],(void*)0,&g_2[2]},{(void*)0,&l_102,&g_2[3],&l_113},{&g_69,&l_114,&l_114,&g_69},{&g_2[3],(void*)0,&g_69,&g_2[3]},{&g_2[3],&l_102,&g_2[3],&l_114}}};
                    int i, j, k;
                    l_205++;
                }
                l_213--;
            }
            else
            { /* block id: 82 */
                uint16_t *l_220[9][3][6] = {{{&l_93,&l_93,&l_216.f4,&l_216.f4,&l_93,&l_93},{(void*)0,&l_93,&l_216.f4,&l_93,(void*)0,(void*)0},{(void*)0,&l_93,&l_93,(void*)0,&l_93,(void*)0}},{{(void*)0,&l_93,(void*)0,&l_93,&l_93,(void*)0},{(void*)0,(void*)0,&l_93,&l_216.f4,&l_93,(void*)0},{&l_93,&l_93,&l_216.f4,&l_216.f4,&l_93,&l_93}},{{(void*)0,&l_93,&l_216.f4,&l_93,(void*)0,(void*)0},{(void*)0,&l_93,&l_93,(void*)0,&l_93,(void*)0},{(void*)0,&l_93,(void*)0,&l_93,&l_93,(void*)0}},{{(void*)0,(void*)0,&l_93,&l_216.f4,&l_93,(void*)0},{&l_93,&l_93,&l_216.f4,&l_216.f4,&l_93,&l_93},{(void*)0,&l_93,&l_216.f4,&l_93,(void*)0,(void*)0}},{{(void*)0,&l_93,&l_93,(void*)0,&l_93,(void*)0},{(void*)0,&l_93,(void*)0,&l_93,&l_93,(void*)0},{(void*)0,(void*)0,&l_93,&l_216.f4,&l_93,(void*)0}},{{&l_93,&l_93,&l_216.f4,&l_216.f4,&l_93,&l_93},{(void*)0,&l_93,&l_216.f4,&l_93,(void*)0,(void*)0},{(void*)0,&l_93,&l_93,(void*)0,&l_93,(void*)0}},{{(void*)0,&l_93,(void*)0,&l_93,&l_93,(void*)0},{(void*)0,(void*)0,&l_93,&l_216.f4,&l_93,(void*)0},{&l_93,&l_93,&l_216.f4,&l_216.f4,&l_93,&l_93}},{{(void*)0,&l_93,&l_216.f4,&l_93,(void*)0,(void*)0},{(void*)0,&l_93,&l_93,(void*)0,&l_93,(void*)0},{(void*)0,&l_93,(void*)0,&l_93,&l_93,(void*)0}},{{(void*)0,(void*)0,&l_93,&l_216.f4,&l_93,(void*)0},{&l_93,&l_93,&l_216.f4,&l_216.f4,&l_93,&l_93},{(void*)0,&l_93,&l_216.f4,&l_93,(void*)0,(void*)0}}};
                int i, j, k;
                (*l_99) &= (g_2[3] && ((g_117.f1 , (((l_216 , ((*l_147) = &g_109)) == l_175) == ((*l_175) = (~(p_64 &= (0xBC34L | (++p_61))))))) , (((((safe_mod_func_uint16_t_u_u(((((l_223 != l_225) , (0L <= (-1L))) || p_64) < (*l_103)), 65535UL)) >= 8L) ^ 7UL) == 0x6E3F8BE5L) ^ 1L)));
            }
        }
        for (l_212 = 0; (l_212 == (-26)); l_212 = safe_sub_func_uint8_t_u_u(l_212, 7))
        { /* block id: 92 */
            uint16_t *l_230[5];
            int32_t l_242 = 3L;
            int i;
            for (i = 0; i < 5; i++)
                l_230[i] = &g_117.f4;
            (*l_99) |= ((safe_add_func_int8_t_s_s((((g_117.f4 = 0x078EL) , ((void*)0 != &p_64)) == 0UL), (((safe_lshift_func_int16_t_s_s(g_117.f3, 15)) || (safe_add_func_uint64_t_u_u((((safe_lshift_func_uint16_t_u_s(((safe_mod_func_uint16_t_u_u((g_243 &= (((safe_lshift_func_int8_t_s_u(0x6EL, ((l_241[3] = ((g_67 , ((l_147 = &g_108[1][1]) != (g_107 = (void*)0))) != p_63)) & l_242))) && 0x60L) >= 0x61DCL)), 9UL)) || p_61), 10)) != 0xDFABC0FDL) , 0x795880F0426A3448LL), 0x4827AF0C913C8FD3LL))) | p_61))) == p_62);
        }
    }
    else
    { /* block id: 100 */
        struct S0 l_250 = {1L,1L,0x1BE3DCF1L,5L,0xB6BEL,1UL,0x72DDL,0xA36B09E236CD4D35LL,0xB0D30FA18C335714LL};
        uint16_t l_281 = 2UL;
        uint8_t *l_287 = &g_88;
        int32_t *l_307 = (void*)0;
        int32_t l_321 = 1L;
        int32_t l_322 = 0xB517CF08L;
        int32_t l_326 = 0x26A7AC40L;
        int32_t l_330 = 8L;
        int32_t l_333 = 7L;
        struct S1 l_341[10] = {{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL},{28,18446744073709551609UL}};
        const uint16_t l_394[8] = {0x8146L,0x8146L,0x8146L,0x8146L,0x8146L,0x8146L,0x8146L,0x8146L};
        uint8_t *l_411 = &g_88;
        uint16_t l_477[7][9] = {{65535UL,0x0D8CL,0x0D8CL,65535UL,65535UL,0x0D8CL,0x1276L,65535UL,65535UL},{6UL,0x0924L,65532UL,0x829FL,65532UL,0x0924L,6UL,0x0924L,65532UL},{65535UL,65535UL,0x0D8CL,0x1276L,65535UL,65535UL,0x1276L,0x0D8CL,65535UL},{65529UL,0x0924L,0xE886L,0x829FL,0xE886L,0x0924L,65529UL,0x0924L,0xE886L},{65535UL,0x0D8CL,0x0D8CL,65535UL,65535UL,0x0D8CL,0x1276L,65535UL,65535UL},{6UL,0x0924L,65532UL,0x829FL,65532UL,0x0924L,6UL,0x0924L,65532UL},{65535UL,65535UL,0x0D8CL,0x1276L,65535UL,65535UL,0x1276L,0x0D8CL,65535UL}};
        int16_t l_503 = 0xBF36L;
        uint16_t l_543[6] = {65535UL,65535UL,65535UL,65535UL,65535UL,65535UL};
        int32_t l_567 = (-2L);
        int64_t *l_582 = &l_405;
        int32_t *l_604 = (void*)0;
        int32_t *l_605 = &l_199[4];
        uint8_t l_662 = 0x6BL;
        int32_t l_672[2][4][9] = {{{0xF7C13F40L,0xF7C13F40L,0x57E246B9L,0L,0x2A18054CL,(-1L),0x0C1E57B8L,(-1L),0x2A18054CL},{0x57E246B9L,0xF7C13F40L,0xF7C13F40L,0x57E246B9L,0L,0x2A18054CL,(-1L),0x0C1E57B8L,(-1L)},{0x0C1E57B8L,(-1L),0x57E246B9L,0x57E246B9L,(-1L),0x0C1E57B8L,(-3L),0xF7C13F40L,0L},{(-1L),0x2A18054CL,0x0C1E57B8L,0L,0L,0x0C1E57B8L,0x2A18054CL,(-1L),(-1L)}},{{0L,0x57E246B9L,(-1L),(-3L),0x2A18054CL,0x2A18054CL,(-3L),(-1L),0x57E246B9L},{0L,(-1L),0L,(-1L),0xF7C13F40L,(-1L),(-1L),0xF7C13F40L,(-1L)},{0L,0xA29CBE50L,0L,(-1L),(-3L),0L,0x0C1E57B8L,0x0C1E57B8L,0L},{0L,(-1L),0L,(-1L),0L,(-1L),0xF7C13F40L,(-1L),(-1L)}}};
        uint32_t l_674[4][6] = {{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL,18446744073709551608UL,18446744073709551615UL,18446744073709551608UL,18446744073709551615UL},{18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{18446744073709551608UL,18446744073709551615UL,18446744073709551608UL,18446744073709551615UL,18446744073709551608UL,18446744073709551615UL}};
        int i, j, k;
        for (g_179 = (-17); (g_179 > 21); g_179++)
        { /* block id: 103 */
            uint64_t l_251 = 0xFFAA04E7043E7B37LL;
            int32_t *l_308 = &l_199[3];
            int32_t l_312 = 0L;
            int32_t l_324[4] = {0x1C19745DL,0x1C19745DL,0x1C19745DL,0x1C19745DL};
            int32_t l_328 = 0x591DE52BL;
            const int16_t *l_354 = (void*)0;
            const int8_t *l_391 = &l_250.f0;
            const int8_t **l_390 = &l_391;
            struct S0 l_404 = {-1L,0x0064L,0x2C96C9ECL,0x2B712848L,1UL,0x1212L,0xD7BFL,6UL,18446744073709551612UL};
            struct S1 l_522 = {6,0xFCB88178L};
            uint8_t l_568[1][2];
            int64_t *l_581[3][1];
            int64_t **l_580 = &l_581[1][0];
            uint32_t l_583 = 4294967295UL;
            int8_t *l_584 = &g_117.f0;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_568[i][j] = 8UL;
            }
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 1; j++)
                    l_581[i][j] = &g_305;
            }
            for (g_117.f2 = 1; (g_117.f2 <= 4); g_117.f2 += 1)
            { /* block id: 106 */
                int32_t *l_284 = &l_199[3];
                int64_t *l_303 = &g_146;
                int64_t *l_304 = &g_305;
                int32_t l_314 = 0x3DEE0DAAL;
                int32_t l_325 = 0xBBA59C75L;
                int32_t l_327[10] = {(-10L),0x45E241E9L,(-10L),(-10L),0x45E241E9L,(-10L),(-10L),0x45E241E9L,(-10L),(-10L)};
                int8_t l_329 = 0x57L;
                struct S0 *l_392 = &g_117;
                int i, j;
            }
            (*l_308) = 1L;
            for (l_250.f4 = (-15); (l_250.f4 != 9); l_250.f4 = safe_add_func_int64_t_s_s(l_250.f4, 2))
            { /* block id: 192 */
                struct S1 l_530 = {43,0UL};
                int64_t *l_539 = &l_502;
                if ((((**g_107) = (((safe_add_func_uint32_t_u_u(((*g_375) , (*l_99)), (0xECL != (safe_div_func_uint64_t_u_u((*l_103), (4294967288UL & (l_530 , (safe_rshift_func_int8_t_s_u((((*l_103) && (safe_add_func_int16_t_s_s((p_63 != (((safe_rshift_func_int8_t_s_s((((safe_mod_func_int16_t_s_s(((((((*l_539) = p_61) && g_117.f2) | (*l_99)) != (*l_308)) > p_64), l_530.f0)) || p_61) , (-1L)), p_63)) , p_62) , 0UL)), p_61))) && 18446744073709551615UL), p_61))))))))) < g_117.f7) == p_64)) && p_61))
                { /* block id: 195 */
                    uint16_t l_540[9][5][5] = {{{65535UL,0UL,65535UL,65535UL,65535UL},{65532UL,65533UL,0x88F6L,65533UL,65532UL},{1UL,65535UL,0xD226L,65535UL,0xD226L},{0xD342L,0xD342L,0x88F6L,65532UL,0x4434L},{65535UL,1UL,1UL,65535UL,0xD226L}},{{65533UL,65532UL,1UL,1UL,65532UL},{0xD226L,1UL,65535UL,0UL,0UL},{65530UL,0xD342L,65530UL,1UL,0x88F6L},{65535UL,65535UL,0UL,65535UL,65535UL},{65530UL,65533UL,0xD342L,65532UL,0xD342L}},{{0xD226L,0xD226L,0UL,65535UL,1UL},{65533UL,65530UL,65530UL,65533UL,0xD342L},{65535UL,65535UL,65535UL,65535UL,65535UL},{0xD342L,65530UL,1UL,0x88F6L,0x88F6L},{1UL,0xD226L,1UL,65535UL,0UL}},{{65532UL,65533UL,0x88F6L,65533UL,65532UL},{1UL,65535UL,0xD226L,65535UL,0xD226L},{0xD342L,0xD342L,0x88F6L,65532UL,0x4434L},{65535UL,1UL,1UL,65535UL,0xD226L},{65533UL,65532UL,1UL,1UL,65532UL}},{{0xD226L,1UL,65535UL,0UL,0UL},{65530UL,0xD342L,65530UL,1UL,0x88F6L},{65535UL,65535UL,0UL,65535UL,65535UL},{65530UL,65533UL,0xD342L,65532UL,0xD342L},{0xD226L,0xD226L,0UL,65535UL,1UL}},{{65533UL,65530UL,65530UL,65533UL,0xD342L},{65535UL,65535UL,65535UL,65535UL,65535UL},{0xD342L,65530UL,1UL,0x88F6L,0x88F6L},{1UL,0xD226L,1UL,65535UL,0UL},{65532UL,65533UL,0x88F6L,65533UL,65532UL}},{{1UL,65535UL,0xD226L,65535UL,0xD226L},{0xD342L,0xD342L,0x88F6L,65532UL,0x4434L},{65535UL,1UL,1UL,65535UL,0xD226L},{65533UL,65532UL,1UL,1UL,65532UL},{0xD226L,1UL,65535UL,0UL,0UL}},{{65530UL,0xD342L,65530UL,1UL,0x88F6L},{65535UL,65535UL,0UL,65535UL,65535UL},{65530UL,65533UL,0xD342L,65532UL,0xD342L},{0xD226L,0xD226L,0UL,65535UL,1UL},{65530UL,65532UL,65532UL,65530UL,0x4434L}},{{1UL,65535UL,0UL,0UL,65535UL},{0x4434L,65532UL,0x88F6L,0xD342L,0xD342L},{65535UL,1UL,65535UL,0UL,0xD226L},{1UL,65530UL,0xD342L,65530UL,1UL},{65535UL,1UL,1UL,65535UL,1UL}}};
                    int i, j, k;
                    l_540[7][3][3]++;
                }
                else
                { /* block id: 197 */
                    for (l_530.f1 = 0; l_530.f1 < 8; l_530.f1 += 1)
                    {
                        l_199[l_530.f1] = 0L;
                    }
                    ++l_543[5];
                }
                if (p_63)
                    continue;
                for (g_117.f6 = 6; (g_117.f6 == 28); ++g_117.f6)
                { /* block id: 204 */
                    struct S1 l_548 = {10,6UL};
                    struct S1 *l_549 = &l_521;
                    uint32_t *l_556 = &g_112;
                    uint16_t l_565 = 65535UL;
                    uint64_t l_566 = 0UL;
                    (*l_549) = l_548;
                    if (((safe_mod_func_int64_t_s_s((((safe_mod_func_int8_t_s_s((l_548.f1 , (((void*)0 != &g_108[1][1]) != ((((*l_556) = g_2[3]) > (l_522 , ((safe_mod_func_uint16_t_u_u((safe_add_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((*g_375), ((((safe_mod_func_uint64_t_u_u(l_565, 0xC6A03658734C24D7LL)) & 1UL) | (*l_103)) & l_566))), l_530.f1)), l_567)) > 0x78EFL))) > 18446744073709551607UL))), 0xE2L)) , (void*)0) == l_411), l_530.f1)) | (-8L)))
                    { /* block id: 207 */
                        return l_568[0][1];
                    }
                    else
                    { /* block id: 209 */
                        (*l_308) = (~p_62);
                    }
                    (*l_549) = l_530;
                }
            }
            (*l_103) = (((safe_mod_func_uint8_t_u_u(((&l_307 == &l_99) || p_64), (safe_sub_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u((g_117 , ((*g_375) |= (p_64 ^ (safe_sub_func_int16_t_s_s((g_117.f1 |= ((*l_308) = (((((*l_584) = (((0UL && p_64) <= ((safe_mod_func_uint16_t_u_u((((*l_580) = &g_146) == (((l_522 , &p_63) != l_582) , (void*)0)), (*l_99))) != l_583)) >= 0x52F6L)) & 0UL) || p_64) <= p_62))), p_62))))), p_63)), p_64)))) < p_64) >= 0x1F455A96E44A02A5LL);
        }
        (*l_99) = (246UL || ((void*)0 != &l_98));
        (*l_605) ^= (((safe_rshift_func_uint16_t_u_s(((safe_mul_func_uint8_t_u_u(((p_64 & 0x06L) , ((void*)0 == &p_61)), ((void*)0 == &l_98))) > (safe_mul_func_uint8_t_u_u((*l_99), (safe_div_func_uint16_t_u_u((((safe_mul_func_int16_t_s_s(((((((safe_mul_func_int8_t_s_s((p_61 >= ((safe_rshift_func_uint8_t_u_u((++(*l_287)), 1)) != (((safe_rshift_func_uint16_t_u_u(((1L || p_64) | p_62), 5)) | l_603) <= (*l_103)))), p_61)) , (*l_103)) , 18446744073709551613UL) , p_64) && 65533UL) || p_63), p_62)) || p_61) != 0xD8555250L), (-10L)))))), 0)) , l_250) , p_62);
        if (p_63)
        { /* block id: 225 */
            uint32_t l_609 = 0xC281CBDBL;
            struct S1 l_623[6] = {{37,18446744073709551611UL},{37,18446744073709551611UL},{42,18446744073709551611UL},{37,18446744073709551611UL},{37,18446744073709551611UL},{42,18446744073709551611UL}};
            const struct S0 ** const l_625 = (void*)0;
            uint16_t l_661 = 0xB8A4L;
            int32_t l_668 = 0x922BF956L;
            int32_t l_669 = 1L;
            int32_t l_670 = (-1L);
            int32_t l_671[6][3] = {{0xD205BF10L,0xD205BF10L,0x7567CD13L},{1L,(-1L),0x7567CD13L},{(-1L),1L,0x7567CD13L},{0xD205BF10L,0xD205BF10L,0x7567CD13L},{1L,(-1L),0x7567CD13L},{(-1L),1L,0x7567CD13L}};
            int i, j;
            for (g_311 = 0; (g_311 == (-15)); g_311 = safe_sub_func_uint8_t_u_u(g_311, 6))
            { /* block id: 228 */
                struct S1 *l_608 = &l_341[5];
                struct S1 l_612 = {18,0x874AF0E6L};
                (*l_608) = l_341[5];
                if (l_609)
                    break;
                for (l_321 = 0; (l_321 > (-2)); l_321--)
                { /* block id: 233 */
                    l_612 = ((*l_608) = l_341[2]);
                }
            }
            for (l_281 = 0; (l_281 == 16); ++l_281)
            { /* block id: 240 */
                uint8_t l_626 = 0UL;
                uint64_t *l_650 = (void*)0;
                int32_t *l_664 = (void*)0;
                int32_t *l_665 = &l_567;
                int32_t *l_666 = &l_322;
                int32_t *l_667[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_667[i] = &l_102;
                for (g_67.f1 = 0; (g_67.f1 < 32); g_67.f1 = safe_add_func_int64_t_s_s(g_67.f1, 3))
                { /* block id: 243 */
                    int8_t ***l_617 = &g_188;
                    int32_t l_628 = 0x06EB7407L;
                    const uint16_t *l_633 = (void*)0;
                    const uint16_t * const *l_632 = &l_633;
                    int32_t l_646 = 0xA1E5535AL;
                    uint8_t *l_659[2][9] = {{&l_626,&l_626,&g_88,&l_626,&l_626,&g_88,&l_626,&l_626,&g_88},{&l_626,&l_626,&g_88,&l_626,&l_626,&g_88,&l_626,&l_626,&g_88}};
                    int32_t *l_663 = (void*)0;
                    int i, j;
                    for (g_88 = 0; (g_88 <= 5); g_88 += 1)
                    { /* block id: 246 */
                        struct S1 *l_622[1][5];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 5; j++)
                                l_622[i][j] = &l_521;
                        }
                        (*l_605) ^= ((((l_617 == l_617) >= (safe_mod_func_int64_t_s_s(0x51366962AACFC69BLL, ((((safe_rshift_func_uint8_t_u_u((((l_623[1] = g_67) , g_624) != l_625), 7)) == (((void*)0 == l_94[(g_88 + 1)][g_88][g_88]) != ((&g_107 != (void*)0) , l_626))) || l_623[1].f0) ^ l_626)))) > 0xEDACL) == 18446744073709551615UL);
                    }
                    if (((*l_103) >= ((((0x4DEEADCCL == 0x2CDE5788L) < (0xBAL == g_67.f1)) , ((l_628 = (l_627 = 0x54L)) , (safe_sub_func_int64_t_s_s(((((**l_147) |= (&g_2[3] != ((((g_88 ^ ((&g_107 != &g_107) || p_62)) , p_64) == 0x7BBAL) , (void*)0))) != g_455[4]) , (-1L)), 0UL)))) , p_61)))
                    { /* block id: 253 */
                        const uint16_t * const **l_634 = &l_632;
                        (*l_605) = (l_646 = (+(p_64 > (((*l_634) = l_632) == ((g_69 > (~((!g_117.f7) >= ((~g_243) , (safe_lshift_func_uint16_t_u_s((safe_sub_func_uint16_t_u_u((*g_375), (*g_375))), (((safe_lshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s((*l_605), 0xF9L)), 4)) <= p_63) & 0UL))))))) , &l_633)))));
                    }
                    else
                    { /* block id: 257 */
                        int32_t **l_647[4][7][2] = {{{&g_156[5][2],&g_156[5][2]},{(void*)0,&l_99},{&g_156[5][2],(void*)0},{&g_156[5][2],&l_99},{(void*)0,&g_156[5][2]},{&g_156[5][2],(void*)0},{&g_156[5][2],&g_156[5][2]}},{{(void*)0,&l_99},{&g_156[5][2],(void*)0},{&g_156[5][2],&l_99},{(void*)0,&g_156[5][2]},{&g_156[5][2],(void*)0},{&g_156[5][2],&g_156[5][2]},{(void*)0,&l_99}},{{&g_156[5][2],(void*)0},{&g_156[5][2],&l_99},{(void*)0,&g_156[5][2]},{&g_156[5][2],(void*)0},{&g_156[5][2],&g_156[5][2]},{(void*)0,&l_99},{&g_156[5][2],(void*)0}},{{&g_156[5][2],&l_99},{(void*)0,&g_156[5][2]},{&g_156[5][2],(void*)0},{&g_156[5][2],&g_156[5][2]},{(void*)0,&l_99},{&g_156[5][2],(void*)0},{&g_156[5][2],&l_99}}};
                        uint8_t **l_658[1][9][1];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 9; j++)
                            {
                                for (k = 0; k < 1; k++)
                                    l_658[i][j][k] = &l_96;
                            }
                        }
                        l_605 = &g_104;
                        (*l_99) = (l_662 = ((safe_rshift_func_int8_t_s_s((((void*)0 != l_650) > (safe_mod_func_int16_t_s_s((safe_unary_minus_func_uint8_t_u(p_62)), (safe_sub_func_int64_t_s_s(((safe_div_func_uint64_t_u_u(l_623[1].f0, ((p_61 , l_646) , ((-4L) | ((l_659[1][3] = &g_88) == (g_660 = (void*)0)))))) , g_67.f1), l_661))))), 4)) , l_609));
                        g_156[0][3] = (l_663 = &g_2[0]);
                        g_156[4][1] = &l_567;
                    }
                }
                ++l_674[1][3];
            }
            return p_62;
        }
        else
        { /* block id: 271 */
            int8_t l_684[4][3][6] = {{{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)}},{{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)}},{{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)}},{{(-1L),0xFEL,(-1L),(-1L),0xFEL,(-1L)},{(-1L),0xFEL,(-1L),0xB8L,(-1L),0xB8L},{0xB8L,(-1L),0xB8L,0xB8L,(-1L),0xB8L}}};
            int16_t *l_685[2];
            uint8_t **l_686 = &l_96;
            int32_t l_687[10] = {0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L,0xCDFC9015L};
            int32_t l_688[2][9] = {{(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L),(-4L)},{0xA1293615L,0xA1293615L,0xA1293615L,0xA1293615L,0xA1293615L,0xA1293615L,0xA1293615L,0xA1293615L,0xA1293615L}};
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_685[i] = &g_117.f6;
            l_688[1][0] ^= ((*l_605) &= ((safe_lshift_func_int8_t_s_s((safe_add_func_uint16_t_u_u(p_63, (((((++(**g_107)) , l_683) == l_683) ^ ((18446744073709551608UL && l_684[0][0][3]) , (l_685[0] != (void*)0))) >= (g_386 = (l_687[5] = ((&g_660 == ((((g_117 , 6L) != p_64) , g_146) , l_686)) != (*g_375))))))), 0)) | (*l_103)));
        }
    }
    for (l_205 = 0; (l_205 > 59); l_205 = safe_add_func_int64_t_s_s(l_205, 6))
    { /* block id: 281 */
        return (*g_660);
    }
    l_693[5]--;
    return p_64;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_67.f1 g_69
 */
static int32_t  func_65(struct S1  p_66)
{ /* block id: 4 */
    uint32_t l_68[1][1][3];
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 3; k++)
                l_68[i][j][k] = 0xF90F5F2FL;
        }
    }
    for (g_67.f1 = 0; g_67.f1 < 1; g_67.f1 += 1)
    {
        for (p_66.f1 = 0; p_66.f1 < 1; p_66.f1 += 1)
        {
            for (g_69 = 0; g_69 < 3; g_69 += 1)
            {
                l_68[g_67.f1][p_66.f1][g_69] = 0x5DA1F797L;
            }
        }
    }
    return p_66.f1;
}




/* ---------------------------------------- */
//testcase_id 1484147152
int case1484147152(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_67.f0, "g_67.f0", print_hash_value);
    transparent_crc(g_67.f1, "g_67.f1", print_hash_value);
    transparent_crc(g_69, "g_69", print_hash_value);
    transparent_crc(g_88, "g_88", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    transparent_crc(g_109, "g_109", print_hash_value);
    transparent_crc(g_112, "g_112", print_hash_value);
    transparent_crc(g_117.f0, "g_117.f0", print_hash_value);
    transparent_crc(g_117.f1, "g_117.f1", print_hash_value);
    transparent_crc(g_117.f2, "g_117.f2", print_hash_value);
    transparent_crc(g_117.f3, "g_117.f3", print_hash_value);
    transparent_crc(g_117.f4, "g_117.f4", print_hash_value);
    transparent_crc(g_117.f5, "g_117.f5", print_hash_value);
    transparent_crc(g_117.f6, "g_117.f6", print_hash_value);
    transparent_crc(g_117.f7, "g_117.f7", print_hash_value);
    transparent_crc(g_117.f8, "g_117.f8", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_146, "g_146", print_hash_value);
    transparent_crc(g_179, "g_179", print_hash_value);
    transparent_crc(g_243, "g_243", print_hash_value);
    transparent_crc(g_305, "g_305", print_hash_value);
    transparent_crc(g_311, "g_311", print_hash_value);
    transparent_crc(g_386, "g_386", print_hash_value);
    transparent_crc(g_419, "g_419", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_455[i], "g_455[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_713, "g_713", print_hash_value);
    transparent_crc(g_727, "g_727", print_hash_value);
    transparent_crc(g_752, "g_752", print_hash_value);
    transparent_crc(g_840, "g_840", print_hash_value);
    transparent_crc(g_851, "g_851", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_852[i].f0, "g_852[i].f0", print_hash_value);
        transparent_crc(g_852[i].f1, "g_852[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_895[i][j], "g_895[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1175, "g_1175", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1204[i], "g_1204[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1336[i][j], "g_1336[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1416.f0, "g_1416.f0", print_hash_value);
    transparent_crc(g_1416.f1, "g_1416.f1", print_hash_value);
    transparent_crc(g_1416.f2, "g_1416.f2", print_hash_value);
    transparent_crc(g_1416.f3, "g_1416.f3", print_hash_value);
    transparent_crc(g_1416.f4, "g_1416.f4", print_hash_value);
    transparent_crc(g_1416.f5, "g_1416.f5", print_hash_value);
    transparent_crc(g_1416.f6, "g_1416.f6", print_hash_value);
    transparent_crc(g_1416.f7, "g_1416.f7", print_hash_value);
    transparent_crc(g_1416.f8, "g_1416.f8", print_hash_value);
    transparent_crc(g_1433, "g_1433", print_hash_value);
    transparent_crc(g_1438.f0, "g_1438.f0", print_hash_value);
    transparent_crc(g_1438.f1, "g_1438.f1", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_1439[i][j][k], "g_1439[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1461.f0, "g_1461.f0", print_hash_value);
    transparent_crc(g_1461.f1, "g_1461.f1", print_hash_value);
    transparent_crc(g_1461.f2, "g_1461.f2", print_hash_value);
    transparent_crc(g_1461.f3, "g_1461.f3", print_hash_value);
    transparent_crc(g_1461.f4, "g_1461.f4", print_hash_value);
    transparent_crc(g_1461.f5, "g_1461.f5", print_hash_value);
    transparent_crc(g_1461.f6, "g_1461.f6", print_hash_value);
    transparent_crc(g_1461.f7, "g_1461.f7", print_hash_value);
    transparent_crc(g_1461.f8, "g_1461.f8", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 421
   depth: 1, occurrence: 26
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 1
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 27
breakdown:
   indirect level: 0, occurrence: 19
   indirect level: 1, occurrence: 5
   indirect level: 2, occurrence: 3
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 37
XXX times a bitfields struct on LHS: 8
XXX times a bitfields struct on RHS: 39
XXX times a single bitfield on LHS: 1
XXX times a single bitfield on RHS: 38

XXX max expression depth: 39
breakdown:
   depth: 1, occurrence: 308
   depth: 2, occurrence: 76
   depth: 3, occurrence: 4
   depth: 4, occurrence: 6
   depth: 5, occurrence: 1
   depth: 6, occurrence: 2
   depth: 7, occurrence: 1
   depth: 8, occurrence: 2
   depth: 9, occurrence: 1
   depth: 11, occurrence: 2
   depth: 14, occurrence: 2
   depth: 15, occurrence: 4
   depth: 16, occurrence: 3
   depth: 17, occurrence: 3
   depth: 18, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 5
   depth: 22, occurrence: 3
   depth: 23, occurrence: 2
   depth: 24, occurrence: 1
   depth: 25, occurrence: 4
   depth: 26, occurrence: 4
   depth: 27, occurrence: 4
   depth: 28, occurrence: 1
   depth: 30, occurrence: 2
   depth: 32, occurrence: 1
   depth: 39, occurrence: 1

XXX total number of pointers: 417

XXX times a variable address is taken: 1198
XXX times a pointer is dereferenced on RHS: 141
breakdown:
   depth: 1, occurrence: 108
   depth: 2, occurrence: 27
   depth: 3, occurrence: 6
XXX times a pointer is dereferenced on LHS: 196
breakdown:
   depth: 1, occurrence: 188
   depth: 2, occurrence: 8
XXX times a pointer is compared with null: 28
XXX times a pointer is compared with address of another variable: 4
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 5271

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 709
   level: 2, occurrence: 156
   level: 3, occurrence: 60
   level: 4, occurrence: 7
   level: 5, occurrence: 1
XXX number of pointers point to pointers: 113
XXX number of pointers point to scalars: 276
XXX number of pointers point to structs: 28
XXX percent of pointers has null in alias set: 26.6
XXX average alias set size: 1.38

XXX times a non-volatile is read: 1233
XXX times a non-volatile is write: 659
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 1
XXX backward jumps: 11

XXX stmts: 300
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 35
   depth: 1, occurrence: 43
   depth: 2, occurrence: 55
   depth: 3, occurrence: 55
   depth: 4, occurrence: 51
   depth: 5, occurrence: 61

XXX percentage a fresh-made variable is used: 18.3
XXX percentage an existing variable is used: 81.7
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/

