#ifndef CONSUMER_H
#define CONSUMER_H

#include <dal.h>
#include "global.h"

#define PORT_IN 1
#define EVENT_DONE "stop_fsm"

typedef struct _local_states {
    char name[10];
    int index;
    int len;
} Consumer_State;

void consumer_init(DALProcess *);
int consumer_fire(DALProcess *);
void consumer_finish(DALProcess *);

#endif
