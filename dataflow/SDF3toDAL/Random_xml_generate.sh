#! /bin/bash
#echo -n "enter setting(opt) file name(ex : sdf3.opt) : "
#read SDF3_OPT

#echo -n "enter output(xml) file name(ex : output.xml) : "
#read OUTPUT_XML

SDF3_OPT=$1
XML_NAME=output.xml
NUM=$2

mv -b ../mip_solver/loop_dir ../mip_solver/backup_loop_dir/
mv -b ../mip_solver/req_time_dir ../mip_solver/backup_req_time_dir/
mv -b ./SDF3_xml_dir ./backup_sdf3_xml_dir/
mv -b ./DAL_xml_dir ./backup_dal_xml_dir/
rm -r ./SDF3_xml_dir ./DAL_xml_dir ./fsm_xml_dir

mkdir SDF3_xml_dir
mkdir DAL_xml_dir
mkdir fsm_xml_dir
mkdir ../mip_solver/loop_dir
mkdir ../mip_solver/req_time_dir

mkdir dal_src_dir 

for((i=0;i<$NUM;i++)); do
	./SDF3_generation.sh $SDF3_OPT $i
	./SDF3toDAL.sh $XML_NAME $i
done
