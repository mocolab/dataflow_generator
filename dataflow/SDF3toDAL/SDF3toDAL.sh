#! /bin/bash
OUTPUT_XML=$1
NUM=$2
mkdir ./dal_src_dir/df_$NUM\_backup
mv -b ./dal_src_dir/df_$NUM ./dal_src_dir/df_$NUM\_backup/
rm -r ./dal_src_dir/df_$NUM
mkdir ./dal_src_dir/df_$NUM
python xmlparsing.py $NUM 
#echo -n "enter output(xml) file name(ex : output.xml) : "
#read OUTPUT_XML
mv output.xml ./DAL_xml_dir/
#mv req_time_$2.txt ../mip_solver/req_time_dir/
cd ./DAL_xml_dir
mkdir $NUM\_$OUTPUT_XML
xmllint --format output.xml > ./$NUM\_$OUTPUT_XML/pn.xml
rm output.xml
cd ..
cd ./fsm_xml_dir
NUMB=$NUM
xmllint --format fsm_$NUMB\.xml > ./$NUMB\_fsm.xml
#rm fsm_$NUMB\.xml
cd ..

