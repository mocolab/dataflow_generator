#! /bin/bash
#echo -n "enter setting(opt) file name(ex : sdf3.opt) : "
#read SDF3_OPT
#echo "setting(opt) file is $SDF3_OPT"

#echo -n "enter output(xml) file name(ex : output.xml) : "
#read OUTPUT_XML
#echo "setting(opt) file is $OUTPUT_XML"
cmd=$1
cmd2=$2
sdf3generate-sdf --settings $cmd --output temp_sdf.xml
xmllint --format temp_sdf.xml > ./SDF3_xml_dir/temp_sdf_new_$cmd2.xml 
sdf3print-sdf --format dot --graph temp_sdf.xml --output dot_out.dot
dot -Tpng -o./SDF3_xml_dir/temp_sdf_new_$cmd2.png dot_out.dot
rm temp_sdf.xml dot_out.dot
