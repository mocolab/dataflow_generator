#ifndef a9_H
#define a9_H

#include <dal.h>

#define PORT_IN0 "p0"
#define PORT_OUT999 "p999"

typedef struct _local_states{
	int index;
	int len;
}a9_State;

void a9_init(DALProcess *);
int a9_fire(DALProcess *);
void a9_finish(DALProcess *);

int case1484147150(void);
int case1484147152(void);
int case1484147153(void);
int case1484147157(void);
int case1484147159(void);
int case1484153229(void);
int case1484153230(void);
int case1484153234(void);
int case1484153239(void);
int case1484153240(void);
int case1484153244(void);
int case1484153245(void);
int case1484153248(void);
int case1484153250(void);
int case1484153251(void);
int case1484153255(void);
int case1484153263(void);
int case1484153267(void);
int case1484153271(void);
int case1484153273(void);
int case1484153279(void);
int case1484153281(void);
int case1484153286(void);
int case1484153287(void);
int case1484153290(void);
int case1484153378(void);
int case1484153381(void);
int case1484153503(void);
int case1484153504(void);
int case1484153505(void);


#endif
