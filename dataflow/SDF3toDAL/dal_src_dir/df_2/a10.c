#include <stdio.h>
#include <time.h>
#include "a10.h"

void a10_init(DALProcess *p) {
	p->local->index=0;
	p->local->len=1;
}

int a10_fire(DALProcess *p) {
	timespec start2,end2;
	long diff_time2,diff_ntime2;
	int x,n;
	if(p->local->index < p->local->len){
		printf("a10_start\n");
		DAL_read((void*)PORT_IN0, &x,sizeof(int),p);
		DAL_read((void*)PORT_IN1, &x,sizeof(int),p);
		printf("a10_loop\n");
		clock_gettime(CLOCK_MONOTONIC,&start2);
		printf("a10_end\n");
		p->local->index++;
		clock_gettime(CLOCK_MONOTONIC,&end2);
		diff_time2 = (end2.tv_sec - start2.tv_sec);
		diff_ntime2 = (end2.tv_nsec-start2.tv_nsec);
		printf("%0.6lf second(s) - time of a10 loop\n",diff_time2+(double)diff_ntime2/1000000000);	}
	if(p->local->index >= p->local->len){
		DAL_send_event((void *)EVENT_DONE,p);
		return(1);
	}

	return 0;
}

void a10_finish(DALProcess *p) {
	fflush(stdout);
}
