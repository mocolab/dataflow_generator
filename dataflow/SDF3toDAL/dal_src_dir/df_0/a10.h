#ifndef a10_H
#define a10_H

#include <dal.h>

#define PORT_IN0 "p0"
#define PORT_IN1 "p1"
#define EVENT_DONE "stop_fsm"

typedef struct _local_states{
	int index;
	int len;
}a10_State;

void a10_init(DALProcess *);
int a10_fire(DALProcess *);
void a10_finish(DALProcess *);

#endif
