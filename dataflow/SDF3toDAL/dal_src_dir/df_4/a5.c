#include <stdio.h>
#include <time.h>
#include "a5.h"

void a5_init(DALProcess *p) {
	p->local->index=0;
	p->local->len=1;
}

int a5_fire(DALProcess *p) {
	timespec start2,end2;
	long diff_time2,diff_ntime2;
	int x,n;
	if(p->local->index < p->local->len){
		printf("a5_start\n");
		DAL_read((void*)PORT_IN0, &x,sizeof(int),p);
		clock_gettime(CLOCK_MONOTONIC,&start2);
		printf("a5_loop\n");
		for(n=0;n<0;n++){
			case1484147150();
		}
		for(n=0;n<0;n++){
			case1484147152();
		}
		for(n=0;n<5;n++){
			case1484147153();
		}
		for(n=0;n<0;n++){
			case1484147157();
		}
		for(n=0;n<0;n++){
			case1484147159();
		}
		for(n=0;n<0;n++){
			case1484153229();
		}
		for(n=0;n<0;n++){
			case1484153230();
		}
		for(n=0;n<0;n++){
			case1484153234();
		}
		for(n=0;n<0;n++){
			case1484153239();
		}
		for(n=0;n<0;n++){
			case1484153240();
		}
		for(n=0;n<0;n++){
			case1484153244();
		}
		for(n=0;n<0;n++){
			case1484153245();
		}
		for(n=0;n<0;n++){
			case1484153248();
		}
		for(n=0;n<0;n++){
			case1484153250();
		}
		for(n=0;n<0;n++){
			case1484153251();
		}
		for(n=0;n<1;n++){
			case1484153255();
		}
		for(n=0;n<0;n++){
			case1484153263();
		}
		for(n=0;n<0;n++){
			case1484153267();
		}
		for(n=0;n<0;n++){
			case1484153271();
		}
		for(n=0;n<0;n++){
			case1484153273();
		}
		for(n=0;n<0;n++){
			case1484153279();
		}
		for(n=0;n<0;n++){
			case1484153281();
		}
		for(n=0;n<1429;n++){
			case1484153286();
		}
		for(n=0;n<3814;n++){
			case1484153287();
		}
		for(n=0;n<0;n++){
			case1484153290();
		}
		for(n=0;n<0;n++){
			case1484153378();
		}
		for(n=0;n<12;n++){
			case1484153381();
		}
		for(n=0;n<381;n++){
			case1484153503();
		}
		for(n=0;n<0;n++){
			case1484153504();
		}
		for(n=0;n<5558;n++){
			case1484153505();
		}
		x=x+1;
		printf("a5_loop_end\n");
		DAL_write((void*)PORT_OUT1,&x, sizeof(int), p);
		printf("a5_end\n");
		p->local->index++;
		clock_gettime(CLOCK_MONOTONIC,&end2);
		diff_time2 = (end2.tv_sec - start2.tv_sec);
		diff_ntime2 = (end2.tv_nsec-start2.tv_nsec);
		printf("%0.6lf second(s) - time of a5 loop\n",diff_time2+(double)diff_ntime2/1000000000);	}
	if(p->local->index >= p->local->len){
		return(1);
	}

	return 0;
}

void a5_finish(DALProcess *p) {
	fflush(stdout);
}
