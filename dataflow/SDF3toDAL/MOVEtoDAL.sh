#! /bin/bash

NUM=$1
NAME=erdfg_$NUM
mkdir ../../dal/dal-benchmarks/workload/$NAME
cp -r ./DAL_resource/erdfg/* ../../dal/dal-benchmarks/workload/$NAME
cp ./dal_src_dir/df_$NUM/* ../../dal/dal-benchmarks/workload/$NAME/app1/src/ 
cp ./DAL_xml_dir/$NUM\_output.xml/pn.xml ../../dal/dal-benchmarks/workload/$NAME/app1/pn.xml
cp ./fsm_xml_dir/$NUM\_fsm.xml ../../dal/dal-benchmarks/workload/$NAME/fsm.xml
cp ../DB_gen/testcase/ca* ../../dal/dal-benchmarks/workload/$NAME/app1/src/
