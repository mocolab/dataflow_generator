import xml.etree.ElementTree as ET
import sys

sys.path.append("/home/goekr/dataflow_generation/dataflow/mip_solver/")
from mip import mip_solver
from final_actor import dal_final_header_gen
from final_actor import dal_final_src_gen

def testcase(filename):
		input_file = open(filenamem,"r")
		for line in input_file:
			line = line.strip()
			line = line[14:]
			print line
		input_file.close()

def sel_io_local(process,actor):
	flag = 0
	port_temp = actor.find("port")
	temp = str(port_temp.attrib["type"])
	if temp == "in":
		flag = 1
#	print temp
	for port_sdf in actor.findall("port"):
		if(temp != str(port_sdf.attrib["type"])):
			return "local"
		temp = port_sdf.attrib["type"]
	if flag == 0 :
		return "io"
	elif flag == 1:
		return "io_final"

def fsm_xml_gen(end_vertex_num,sdf_num):
	fsm_xml = ET.Element("fsm")
	fsm_xml.attrib["xmlns"]="http://www.tik.ee.ethz.ch/~euretile/schema/FSM"
	fsm_xml.attrib["xmlns:xsi"]="http://www.w3.org/2001/XMLSchema-instance"
	fsm_xml.attrib["name"]="fsm_example1"
	fsm_xml.attrib["xsi:schemaLocation"]="http://www.tik.ee.ethz.ch/~euretile/schema/FSM http://www.tik.ee.ethz.ch/~euretile/schema/fsm.xml"
	application = ET.SubElement(fsm_xml,"application")
	application.attrib["name"]="APP1"
	application.attrib["src"]="app1/pn.xml"
	application.attrib["critical"]="0"
	transition = ET.SubElement(fsm_xml,"transition")
	transition.attrib["name"]="trans_1"
	transition.attrib["nextstate"]="State_1"
	state = ET.SubElement(fsm_xml,"state")
	state.attrib["name"]="State_1"
	transition_state = ET.SubElement(state,"transition")
	transition_state.attrib["name"]="trans_2"
	transition_state.attrib["nextstate"]="END_STATE"
	event = ET.SubElement(transition_state,"event")
#for i in range(end_vertex_num):
#		event.attrib["name"]="stop_fsm_"+str(i)
	event.attrib["name"]="stop_fsm"#event error
	run = ET.SubElement(state,"run")
	run.attrib["application"]="APP1"
	state = ET.SubElement(fsm_xml,"state")
	state.attrib["name"]="END_STATE"
	endstate = ET.SubElement(fsm_xml,"endstate")
	endstate.attrib["name"]="END_STATE"
	out_xml=ET.ElementTree(fsm_xml)
	out_xml.write("./fsm_xml_dir/fsm_"+str(sdf_num)+".xml",encoding="utf-8",xml_declaration=True)

def main():
	final_actor_num = 0
	final_actor_name = []
	channel_num = 0
	actor_num = 0
	#--output data list
	actor_req_time = []
	rate_list=[]
	#---------------
#input_xml = raw_input("enter input xml file name(ex : input_name.xml) : ")
	input_xml = "temp_sdf_new_"+sys.argv[1]+".xml"
	out = ET.Element("processnetwork")
	out.attrib["xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance"
	out.attrib["xmlns"]= "http://www.tik.ee.ethz.ch/~euretile/schema/PROCESSNETWORK"
	out.attrib["xsi:schemaLocation"] = "http://www.tik.ee.ethz.ch/~euretile/schema/PROCESSNETWORK http://www.tik.ee.ethz.ch/~euretile/schema/processnetwork.xsd"
	out.attrib["name"]="APP1"
	out.attrib["id"]="0"
	doc = ET.parse('./SDF3_xml_dir/'+input_xml)
	root = doc.getroot()
	for actor in root.iter("actor"):
		final_flag = 0
		process = ET.SubElement(out,"process")
		process.attrib["basename"]=str(actor.attrib["name"])
		process.attrib["name"]=str(actor.attrib["name"])
		process.attrib["id"]=str(actor.attrib["name"])[1:]	
		sel_io_local_result=str(sel_io_local(process,actor))
		if sel_io_local_result == "io_final" :
			final_actor_num = final_actor_num + 1
			final_actor_name.append(str(actor.attrib["name"]))
			final_flag = 1
			sel_io_local_result = "local"
		process.attrib["type"]=sel_io_local_result
		actor_number_temp=str(actor.attrib["name"])[1:]
		for port_sdf in actor.findall("port"):
			rate_temp = [actor_number_temp,0,0,0]
			port = ET.SubElement(process,"port")
			port.attrib["name"]=str(port_sdf.attrib["name"])
			port.attrib["type"]=str(port_sdf.attrib["type"])+"put"
			rate_temp[1]=str(port_sdf.attrib["name"])[1:]
			rate_temp[2]=str(port_sdf.attrib["type"])
			rate_temp[3]=str(port_sdf.attrib["rate"])
			rate_list.append(rate_temp)
		if final_flag == 1 :
			rate_temp = [actor_number_temp,0,0,0]
			port = ET.SubElement(process,"port")
			port.attrib["name"]="p999"
			port.attrib["type"]="output"
			rate_temp[1]="999"
			rate_temp[2]="out"
			rate_temp[3]="1"
			rate_list.append(rate_temp)
		source = ET.SubElement(process,"source")
		source.attrib["location"]=str(actor.attrib["name"])+".c"#to do
		source.attrib["type"]="c"#??
		actor_num = actor_num + 1
	for final_actor in range(1):
		process = ET.SubElement(out,"process")
		process.attrib["basename"]="a"+str(actor_num)
		process.attrib["name"]="a"+str(actor_num)
		process.attrib["id"]=str(actor_num)
		process.attrib["type"]="io"
		#actor_number_temp=str(actor.attrib["name"])[1:]
		for port_final in range(final_actor_num) :
			#rate_temp = [actor_number_temp,0,0,0]
			port = ET.SubElement(process,"port")
			port.attrib["name"]="p"+str(port_final)
			port.attrib["type"]="input"
			#rate_temp[1]=str(port_sdf.attrib["name"])[1:]
			#rate_temp[2]=str(port_sdf.attrib["type"])
			#rate_temp[3]=str(port_sdf.attrib["rate"])
			#rate_list.append(rate_temp)
		source = ET.SubElement(process,"source")
		source.attrib["location"]="a"+str(actor_num)+".c"#to do
		source.attrib["type"]="c"#??
	for channel in root.iter("channel"):
		sw_channel=ET.SubElement(out,"sw_channel")
		sw_channel.attrib["basename"]=str(channel.attrib["name"])
		sw_channel.attrib["name"]=str(channel.attrib["name"])
		sw_channel.attrib["id"]=str(channel.attrib["name"])[2:]
		sw_channel.attrib["size"]="10"
		sw_channel.attrib["tokensize"]="1"
		sw_channel.attrib["type"]="fifo"
		port_ch_in = ET.SubElement(sw_channel,"port")
		port_ch_in.attrib["name"]="p0"
		port_ch_in.attrib["type"]="input"
		port_ch_out = ET.SubElement(sw_channel,"port")
		port_ch_out.attrib["name"]="p1"
		port_ch_out.attrib["type"]="output"
		channel_num = channel_num + 1
	for channel6 in range(final_actor_num) :#final channel
		sw_channel=ET.SubElement(out,"sw_channel")
		sw_channel.attrib["basename"]="ch"+str(channel_num+channel6)
		sw_channel.attrib["name"]="ch"+str(channel_num+channel6)
		sw_channel.attrib["id"]=str(channel_num+channel6)
		sw_channel.attrib["size"]="10"
		sw_channel.attrib["tokensize"]="1"
		sw_channel.attrib["type"]="fifo"
		port_ch_in = ET.SubElement(sw_channel,"port")
		port_ch_in.attrib["name"]="p0"
		port_ch_in.attrib["type"]="input"
		port_ch_out = ET.SubElement(sw_channel,"port")
		port_ch_out.attrib["name"]="p1"
		port_ch_out.attrib["type"]="output"
	for channel2 in root.iter("channel"):
		connection=ET.SubElement(out,"connection")
		connection.attrib["name"]=str(channel2.attrib["srcActor"])+"-"+str(channel2.attrib["name"])
		origin = ET.SubElement(connection,"origin")
		origin.attrib["name"]=str(channel2.attrib["srcActor"])
		port_con_org = ET.SubElement(origin,"port")
		port_con_org.attrib["name"]=str(channel2.attrib["srcPort"])
		target = ET.SubElement(connection,"target")
		target.attrib["name"]=str(channel2.attrib["name"])
		port_con_tar = ET.SubElement(target,"port")
		port_con_tar.attrib["name"]="p0"
	for channel4 in range(final_actor_num):#final in connection
		connection=ET.SubElement(out,"connection")
		connection.attrib["name"]="in_"+str(channel4)+"_"+str(final_actor_num)
		origin = ET.SubElement(connection,"origin")
		origin.attrib["name"]=final_actor_name[channel4]
		port_con_org = ET.SubElement(origin,"port")
		port_con_org.attrib["name"]="p999"#max?
		target = ET.SubElement(connection,"target")
		target.attrib["name"]="ch"+str(channel_num+channel4)
		port_con_tar = ET.SubElement(target,"port")
		port_con_tar.attrib["name"]="p0"
	for channel3 in root.iter("channel"):
		connection=ET.SubElement(out,"connection")
		connection.attrib["name"]=str(channel3.attrib["name"])+"-"+str(channel3.attrib["dstActor"])
		origin = ET.SubElement(connection,"origin")
		origin.attrib["name"]=str(channel3.attrib["name"])
		port_con_org = ET.SubElement(origin,"port")
		port_con_org.attrib["name"]="p1"
		target = ET.SubElement(connection,"target")
		target.attrib["name"]=str(channel3.attrib["dstActor"])
		port_con_tar = ET.SubElement(target,"port")
		port_con_tar.attrib["name"]=str(channel3.attrib["dstPort"])
	for channel5 in range(final_actor_num):#final out connection
		connection=ET.SubElement(out,"connection")
		connection.attrib["name"]="out_"+str(channel5)+"_"+str(final_actor_num)
		origin = ET.SubElement(connection,"origin")
		origin.attrib["name"]="ch"+str(channel_num+channel5)
		port_con_org = ET.SubElement(origin,"port")
		port_con_org.attrib["name"]="p1"
		target = ET.SubElement(connection,"target")
		target.attrib["name"]="a"+str(actor_num)
		port_con_tar = ET.SubElement(target,"port")
		port_con_tar.attrib["name"]="p"+str(channel5)	
	a_n=0
	for actorProper in root.iter("actorProperties"):
		actor_req_time.append([]) 
		actor_req_time[a_n].append(str(actorProper.attrib["actor"]))#insert list  
		for processor in actorProper.findall("processor"):
			proc_num = str(processor.attrib["type"])
			for exe_time in processor.findall("executionTime"):
				exe_time_temp = str(exe_time.attrib["time"])
				actor_req_time[a_n].append(proc_num+"="+exe_time_temp)
		a_n = a_n + 1
	f=open('../mip_solver/req_time_dir/req_time_'+sys.argv[1]+'.txt','w')
	f.write(str(a_n)+"\n")
	for k in range(a_n):
		for j in range(len(actor_req_time[k])):
			f.write(actor_req_time[k][j]+' ')
		f.write("\n")
	f.close()
	case_num = 30 # to do !!
	hw_num = 2 # to do !!
	end_vertex_num = mip_solver(case_num,hw_num,a_n,int(sys.argv[1]),rate_list)
	print("end_vertex_num_xmlparsing")
	print(end_vertex_num)
	dal_final_header_gen(final_actor_num,int(sys.argv[1]),actor_num)#event error
	dal_final_src_gen(final_actor_num,int(sys.argv[1]),actor_num)#event error

	out_xml=ET.ElementTree(out)
	out_xml.write("output.xml",encoding="utf-8",xml_declaration=True)

	fsm_xml_gen(end_vertex_num,sys.argv[1])

if __name__ == "__main__":
	main()

