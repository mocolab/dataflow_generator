#include<stdio.h>
#include<string.h>
#define AVR_CNT 5000

void generate_prepro(int func_num, char* id_list,int* id_list_int){
	int i;
	FILE *pre = fopen("./prepro_g.c","w");
	fprintf(pre,"%s\n","#include<stdio.h>");
	fprintf(pre,"%s\n","#include<sys/time.h>");
	fprintf(pre,"%s\n","#include<stdlib.h>");
	fprintf(pre,"%s\n","#include<string.h>");
	fprintf(pre,"%s %d\n","#define AVR_CNT",AVR_CNT);
	fprintf(pre,"%s {%s}\n","#define FUNC_LIST",id_list);
	fprintf(pre,"%s %d\n","#define FUNC_NUM",func_num);
	for(i=0;i<func_num;i++){
		fprintf(pre,"%s%d%s\n","int case",id_list_int[i],"(void);");
	}
	fprintf(pre,"%s\n","int (*pfunc[])(void) = FUNC_LIST;");
	fprintf(pre,"%s\n","int main(int argc, char* argv[]){");
	fprintf(pre,"%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n","	int i,j;","	double avr,sum;","	struct timeval start,end,result;","	char str[20] = {0,};","	FILE *f = fopen(\"./testcase/pre_result.txt\",\"w\");","	for(i=0;i<FUNC_NUM;i++,sum=0){","		gettimeofday(&start,NULL);","		for(j=0;j<AVR_CNT;j++){","		pfunc[i]();","		}","		gettimeofday(&end,NULL);","		timersub(&end,&start,&result);","		sum = (double)result.tv_sec + (double)result.tv_usec/1000000;","	avr = sum/AVR_CNT;","	//printf(\"%d, %lf\\n\",i,avr);","	fprintf(f,\"%4d %10lf\\n\",i,avr);","	}","	fclose(f);","	}");
	//fprintf(pre,"%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n","	int i,j;","	double avr,sum;","	struct timeval start,end,result;","	char str[20] = {0,};","	FILE *f = fopen(\"./testcase/pre_result.txt\",\"w\");","	for(i=0;i<FUNC_NUM;i++,sum=0){","		for(j=0;j<AVR_CNT;j++){","		gettimeofday(&start,NULL);","		pfunc[i]();","		gettimeofday(&end,NULL);","		timersub(&end,&start,&result);","		sum += (double)result.tv_sec + (double)result.tv_usec/1000000;","		}","	avr = sum/AVR_CNT;","	printf(\"%d, %lf\\n\",i,avr);","	fprintf(f,\"%4d %10lf\\n\",i,avr);","	}","	fclose(f);","	}");

}

int main(int argc, char* argv[]){
	int CASE_NUM = atoi(argv[1]);
	char str[100] = "gcc -w -O2 -o prepro_g ./prepro_g.c";
	char nul[100] = {0,};
	int i=0,func_num=0;
	char dotc[3] = ".c";
	char semcol[2] = ",";
	char function_name[20]=" ./testcase/case";
	char function_name_l[20]=" case";
	char read_trash[20];
	char read_id[20];
	char compile_out_str[1000] = {0,};
	char read_temp[40];
	char read_temp2[40];
	char id_list[1000]={0,};
	int id_list_int[100];
	FILE *f = fopen("./testcase/testcase_id.txt","r");
	FILE *pre_sh = fopen("./pre_sh.sh","w");
	
	strcpy(compile_out_str,str);
	
	while(EOF!=fscanf(f,"%s %s",read_trash,read_id)){
		id_list_int[func_num] = atoi(read_id);
		strcpy(read_temp,function_name);
		strcpy(read_temp2,function_name_l);
		strcat(read_temp,read_id);
		strcat(read_temp2,read_id);
		strcat(read_temp,dotc);
		strcat(read_temp2,semcol);
		strcat(compile_out_str,read_temp);
		strcat(id_list,read_temp2);
		strcpy(read_temp,nul);
		strcpy(read_temp2,nul);
		strcpy(read_id,nul);
		func_num++;
	}
	id_list[16*func_num-1] = NULL;
	generate_prepro(func_num,id_list,id_list_int);
	fprintf(pre_sh,"%s\n",compile_out_str);
	fclose(pre_sh);
}

