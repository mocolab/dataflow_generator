/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      1569141486
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_8[3][9] = {{0xB63E8E1BL,0x5DE05DAEL,0x5DE05DAEL,0xB63E8E1BL,0xB63E8E1BL,0x5DE05DAEL,0x5DE05DAEL,0xB63E8E1BL,0xB63E8E1BL},{(-6L),0x00585B7FL,(-6L),0x00585B7FL,(-6L),0x00585B7FL,(-6L),0x00585B7FL,(-6L)},{0xB63E8E1BL,0xB63E8E1BL,0x5DE05DAEL,0x5DE05DAEL,0xB63E8E1BL,0xB63E8E1BL,0x5DE05DAEL,0x5DE05DAEL,0xB63E8E1BL}};
static int32_t g_18 = 0L;
static int32_t g_21[6][8][2] = {{{0x4DB503B5L,0x77DA19AEL},{2L,0x0D69B45CL},{(-4L),0x07E294B5L},{0x8642E5F0L,0x8642E5F0L},{0xB756FD8CL,0x58163090L},{0x77DA19AEL,0x7606E82AL},{0x944DCD6AL,0x6552DED1L},{0x58163090L,0x944DCD6AL}},{{0L,0xDE5A82A6L},{0L,0x7606E82AL},{8L,(-9L)},{0x7606E82AL,0x07E294B5L},{0xB756FD8CL,8L},{0x6552DED1L,0L},{0L,0x8642E5F0L},{0x77DA19AEL,0xDE5A82A6L}},{{(-10L),0xB756FD8CL},{(-4L),2L},{0x07E294B5L,2L},{(-4L),0xB756FD8CL},{(-10L),0xDE5A82A6L},{0x77DA19AEL,0x8642E5F0L},{0L,0L},{0x6552DED1L,8L}},{{0xB756FD8CL,0x07E294B5L},{0x7606E82AL,(-9L)},{8L,0x7606E82AL},{8L,1L},{8L,0x7606E82AL},{8L,(-9L)},{0x7606E82AL,0x07E294B5L},{0xB756FD8CL,8L}},{{0x6552DED1L,0L},{0L,0x8642E5F0L},{0x77DA19AEL,0xDE5A82A6L},{(-10L),0xB756FD8CL},{(-4L),2L},{0x07E294B5L,2L},{(-4L),0xB756FD8CL},{(-10L),0xDE5A82A6L}},{{0x77DA19AEL,0x8642E5F0L},{0L,0L},{0x6552DED1L,8L},{0xB756FD8CL,0x07E294B5L},{0x7606E82AL,(-9L)},{8L,0x7606E82AL},{8L,1L},{8L,0x7606E82AL}}};
static int16_t g_51 = 0L;
static int8_t g_62 = 4L;
static int32_t *g_77 = &g_8[2][6];
static int32_t ** const g_76 = &g_77;
static uint16_t g_85 = 65535UL;
static uint64_t g_87 = 18446744073709551615UL;
static uint32_t g_102[6] = {1UL,1UL,1UL,1UL,1UL,1UL};
static int32_t g_106 = 0x6A191928L;
static int32_t g_108 = 0x563F1C15L;
static int8_t g_110 = (-3L);
static uint8_t g_117 = 255UL;
static uint16_t g_122 = 0UL;
static uint16_t *g_121 = &g_122;
static int64_t g_178 = 0x2B9C605374556D82LL;
static uint8_t g_179[4] = {0x3AL,0x3AL,0x3AL,0x3AL};
static int32_t g_226[3] = {7L,7L,7L};
static int8_t g_235 = 0x3AL;
static int32_t g_236 = 1L;
static int32_t g_237 = 0xA2DDA9A1L;
static int32_t g_238 = (-1L);
static int16_t g_239 = 0x1D7DL;
static int32_t g_240 = 0xA7AC5CE1L;
static uint64_t g_241 = 18446744073709551607UL;
static uint32_t *g_359 = &g_102[0];
static uint32_t **g_358 = &g_359;
static uint8_t g_384 = 249UL;
static int32_t g_408 = 0xF9680008L;
static int64_t g_411 = 0L;
static uint64_t g_412 = 0UL;
static uint32_t g_529 = 0xF0A5DBACL;
static int16_t *g_554 = &g_51;
static uint16_t ***g_594 = (void*)0;
static uint64_t ** const g_599 = (void*)0;
static uint16_t **g_601[2] = {&g_121,&g_121};
static uint16_t ***g_600 = &g_601[0];
static uint16_t g_667 = 0xC8ECL;
static uint32_t g_755 = 0xE89D5DC6L;
static uint32_t *g_754[2][8] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
static int64_t g_785 = 0L;
static uint32_t g_786 = 0xCDDDF7CCL;
static int32_t g_913 = (-1L);
static uint32_t g_918 = 0UL;
static int8_t g_991[8][7] = {{0x48L,0x48L,1L,0x4BL,0x80L,1L,0x80L},{0x4BL,0x3CL,0x3CL,0x4BL,1L,0xACL,0x4BL},{0xACL,0x80L,1L,1L,0x80L,0xACL,0x3CL},{0x80L,0x4BL,1L,0x48L,0x48L,1L,0x4BL},{0x80L,0x3CL,0xACL,0x80L,1L,1L,0x80L},{0xACL,0x4BL,0xACL,1L,0x4BL,0x3CL,0x3CL},{0x4BL,0x80L,1L,0x80L,0x4BL,1L,0x48L},{0x48L,0x3CL,1L,0x48L,1L,0x3CL,0x48L}};
static const int64_t g_1005 = 0L;
static int64_t g_1030 = 7L;
static int16_t g_1031[1] = {7L};
static uint64_t g_1032 = 0UL;
static uint64_t g_1042 = 18446744073709551611UL;
static uint64_t g_1111 = 0xD566C443B8741A48LL;
static int16_t g_1169 = 0x821BL;
static int16_t g_1227 = 0L;
static uint32_t *g_1243 = (void*)0;
static int16_t g_1244 = 1L;
static uint32_t g_1281 = 18446744073709551615UL;
static uint64_t *g_1302 = &g_87;
static uint64_t **g_1301[4] = {&g_1302,&g_1302,&g_1302,&g_1302};
static uint64_t ***g_1300 = &g_1301[1];
static const int32_t *g_1319 = (void*)0;
static const int32_t **g_1318 = &g_1319;
static uint32_t g_1374[6][5] = {{0x86EA7796L,0xB851AF82L,18446744073709551613UL,18446744073709551613UL,0xB851AF82L},{0x8B6FD656L,1UL,0UL,0UL,1UL},{0x86EA7796L,0xB851AF82L,18446744073709551613UL,18446744073709551613UL,0xB851AF82L},{0x8B6FD656L,1UL,0UL,0UL,1UL},{0x86EA7796L,0xB851AF82L,0xB851AF82L,0xB851AF82L,0UL},{0x0C8359A8L,0x461F4046L,1UL,1UL,0x461F4046L}};
static uint32_t **g_1393[10] = {&g_754[0][7],(void*)0,&g_754[0][6],&g_754[0][6],(void*)0,&g_754[0][7],(void*)0,&g_754[0][6],&g_754[0][6],(void*)0};
static uint32_t ***g_1392 = &g_1393[7];
static int64_t *g_1404 = (void*)0;
static int32_t g_1568[4][5] = {{1L,0x28634E58L,1L,0x28634E58L,1L},{0L,1L,1L,0L,0L},{0xDA255D88L,0x28634E58L,0xDA255D88L,0x28634E58L,0xDA255D88L},{0L,0L,1L,1L,0L}};
static const uint8_t g_1584 = 0x38L;
static int16_t g_1638 = 0L;
static const uint16_t g_1674 = 65531UL;
static uint32_t * const g_1706 = &g_529;
static uint16_t g_1743 = 0x9E68L;
static uint32_t g_1914 = 6UL;
static int8_t g_1940 = 0x93L;
static uint32_t g_1983 = 0UL;
static const uint32_t ***g_1990[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t *g_2008[5] = {&g_384,&g_384,&g_384,&g_384,&g_384};
static uint8_t **g_2007 = &g_2008[0];
static int32_t g_2035[6][6] = {{2L,2L,2L,2L,2L,2L},{2L,2L,2L,2L,2L,2L},{2L,2L,2L,2L,2L,2L},{2L,2L,2L,2L,2L,2L},{2L,2L,2L,2L,2L,2L},{2L,2L,2L,2L,2L,2L}};
static int8_t *g_2198 = &g_991[6][6];
static int8_t **g_2197 = &g_2198;
static int8_t ***g_2196 = &g_2197;
static uint32_t ***g_2434 = &g_358;
static uint32_t ****g_2433 = &g_2434;
static uint16_t ****g_2458 = (void*)0;
static int16_t **g_2500 = &g_554;
static int16_t ***g_2499 = &g_2500;
static uint16_t g_2524 = 0xF981L;
static int32_t *g_2563 = &g_21[1][6][1];
static int32_t **g_2715 = (void*)0;
static int8_t g_2744 = 0xF4L;
static int16_t g_2750 = 0xBEF9L;
static int64_t g_2856[6][6] = {{1L,(-1L),(-1L),1L,(-7L),0xC83CB998D2AFC125LL},{0x17B81014FF18D15BLL,0xBF279EF38A02719ALL,1L,1L,1L,0x12A134F0F6561F06LL},{1L,(-7L),(-2L),8L,1L,8L},{0xE9AC74C569794F27LL,0xBF279EF38A02719ALL,0xE9AC74C569794F27LL,0x51765E3C8BA8A239LL,(-7L),(-7L)},{0xC83CB998D2AFC125LL,(-1L),1L,0x12A134F0F6561F06LL,0x17B81014FF18D15BLL,(-7L)},{1L,8L,0x51765E3C8BA8A239LL,0x12A134F0F6561F06LL,0x12A134F0F6561F06LL,0x51765E3C8BA8A239LL}};
static const int16_t *g_2957 = &g_51;
static const int16_t **g_2956 = &g_2957;
static const int16_t ***g_2955 = &g_2956;
static const int16_t ****g_2954 = &g_2955;
static const uint16_t *g_3079 = &g_1674;
static const uint16_t **g_3078 = &g_3079;
static const uint16_t ***g_3077[8] = {&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078,&g_3078};
static int32_t g_3119 = 1L;
static int16_t *** const *g_3243 = &g_2499;
static int16_t *** const **g_3242 = &g_3243;
static int16_t *** const **g_3244 = (void*)0;
static uint32_t ****g_3303 = &g_1392;
static uint32_t *****g_3302 = &g_3303;
static uint16_t g_3304 = 65535UL;
static uint8_t g_3370 = 0xC4L;
static int16_t g_3405 = 1L;
static uint64_t g_3406 = 9UL;
static int16_t g_3454 = 0xCFD2L;
static uint64_t g_3455[6][4][1] = {{{1UL},{0x7CD56D1AA78B3536LL},{18446744073709551615UL},{0x7CD56D1AA78B3536LL}},{{1UL},{0x6C58E230AC855737LL},{0x6C58E230AC855737LL},{1UL}},{{0x7CD56D1AA78B3536LL},{18446744073709551615UL},{0x7CD56D1AA78B3536LL},{1UL}},{{0x6C58E230AC855737LL},{0x6C58E230AC855737LL},{1UL},{0x7CD56D1AA78B3536LL}},{{18446744073709551615UL},{0x7CD56D1AA78B3536LL},{1UL},{0x6C58E230AC855737LL}},{{0x6C58E230AC855737LL},{1UL},{0x7CD56D1AA78B3536LL},{18446744073709551615UL}}};
static uint32_t g_3460[1][7][8] = {{{18446744073709551615UL,0x4F064B3FL,0x4F064B3FL,18446744073709551615UL,0xF469985FL,0UL,0x0BC867C8L,4UL},{0xF469985FL,0UL,0x0BC867C8L,4UL,1UL,0x55DAA33CL,0xE7C3D291L,18446744073709551615UL},{0x26ACF8B4L,0UL,0x98429E26L,18446744073709551611UL,0x98429E26L,0UL,0x26ACF8B4L,0xD21006A5L},{18446744073709551611UL,0x4F064B3FL,0xD21006A5L,0UL,18446744073709551615UL,18446744073709551612UL,0x06909D50L,0x7B4E44C4L},{0x7B4E44C4L,18446744073709551615UL,18446744073709551611UL,0x0BC867C8L,0x55DAA33CL,0x55DAA33CL,0x0BC867C8L,0xD21006A5L},{18446744073709551612UL,18446744073709551612UL,0xE7C3D291L,0x4F064B3FL,0xF469985FL,18446744073709551615UL,0xD21006A5L,0x26ACF8B4L},{18446744073709551615UL,0x7B4E44C4L,18446744073709551612UL,0xF469985FL,0x0BC867C8L,0x26ACF8B4L,18446744073709551611UL,0x26ACF8B4L}}};
static int32_t g_3550[5][8] = {{6L,0xE624D46AL,0xE624D46AL,6L,1L,1L,1L,6L},{0xE624D46AL,1L,0xE624D46AL,0L,0x3E487C40L,0x3E487C40L,0L,0xE624D46AL},{1L,1L,0x3E487C40L,1L,(-1L),1L,0x3E487C40L,1L},{1L,0xE624D46AL,0L,0x3E487C40L,0x3E487C40L,0L,0xE624D46AL,1L},{0xE624D46AL,6L,1L,1L,1L,6L,0xE624D46AL,0xE624D46AL}};


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static uint64_t  func_9(int32_t  p_10, int64_t  p_11);
static uint8_t  func_14(int64_t  p_15);
static uint16_t  func_30(int32_t * p_31, int32_t * p_32);
static int32_t * func_33(int32_t * p_34, int32_t  p_35, int8_t  p_36);
static uint32_t  func_37(uint32_t  p_38, uint64_t  p_39);
static int8_t  func_40(int32_t * p_41);
static int32_t * func_42(int32_t * p_43, uint8_t  p_44, uint32_t  p_45, int32_t * p_46);
static uint8_t  func_52(uint32_t  p_53, int32_t * const * p_54, int32_t  p_55, uint32_t  p_56, int32_t  p_57);
static uint8_t  func_68(uint64_t  p_69, uint16_t  p_70, int32_t * p_71, int64_t  p_72, int64_t  p_73);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_77 g_8
 * writes:
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    int16_t l_2 = 0xDDE0L;
    int32_t l_1984 = (-1L);
    int32_t l_2674 = 0L;
    int32_t * const *l_2712 = &g_77;
    int8_t l_2718 = 1L;
    uint16_t l_2778 = 1UL;
    uint8_t l_2783 = 0UL;
    int32_t l_2787 = (-7L);
    int32_t l_2788 = 0L;
    int8_t l_2805 = 0xB0L;
    int32_t l_2812[7] = {0x763C835CL,0xE0D8111BL,0xE0D8111BL,0x763C835CL,0xE0D8111BL,0xE0D8111BL,0x763C835CL};
    int16_t l_2813 = 0xDEE2L;
    int32_t l_2814 = 0L;
    uint32_t **l_2826 = &g_359;
    int16_t ****l_2827 = &g_2499;
    uint64_t l_2832[6] = {0xB12F7B3F139512A7LL,0xB12F7B3F139512A7LL,0xB12F7B3F139512A7LL,0xB12F7B3F139512A7LL,0xB12F7B3F139512A7LL,0xB12F7B3F139512A7LL};
    uint64_t ***l_2854 = &g_1301[3];
    int64_t l_2858 = 0xA8F2E2FDF17A85BCLL;
    int64_t *l_2935 = &g_1030;
    uint32_t l_2948[8] = {0x1099A8DDL,0xE8555FF0L,0x1099A8DDL,0xE8555FF0L,0x1099A8DDL,0xE8555FF0L,0x1099A8DDL,0xE8555FF0L};
    uint32_t l_2979 = 6UL;
    uint8_t l_2998 = 253UL;
    const uint64_t l_3025[10] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
    const uint16_t ***l_3081 = (void*)0;
    uint16_t ****l_3097 = &g_600;
    const uint32_t *l_3115 = (void*)0;
    const uint32_t **l_3114 = &l_3115;
    uint8_t l_3126 = 1UL;
    uint64_t l_3161 = 0xEE8C599A1FEC8C58LL;
    uint32_t l_3162 = 4294967295UL;
    int64_t l_3225[1];
    int16_t l_3226 = 2L;
    uint16_t l_3263 = 65534UL;
    const int32_t l_3290 = 0x838C2C5CL;
    int64_t l_3293[6][9][4] = {{{0x01D46DDDA50130BDLL,0xEF58199E1D3A19CDLL,0x96823B8B04937E18LL,0x856F85125D174A4ELL},{0x4A8B8A9E34A1F9C7LL,(-1L),0xE6C38B7838DAF927LL,0xDE503E5B4A07C2FALL},{(-1L),1L,0x856F85125D174A4ELL,0x4A8B8A9E34A1F9C7LL},{0x01D46DDDA50130BDLL,0xDE503E5B4A07C2FALL,0x01D46DDDA50130BDLL,0x96823B8B04937E18LL},{0xEF58199E1D3A19CDLL,9L,0L,(-1L)},{9L,1L,4L,9L},{0x1FA469E0E8E86CDFLL,0x4A8B8A9E34A1F9C7LL,4L,0x856F85125D174A4ELL},{9L,0xEF0CCBDA0A4AC4B5LL,0L,0xEF58199E1D3A19CDLL},{0xEF58199E1D3A19CDLL,4L,0x01D46DDDA50130BDLL,0xF41B01F3A26FD9B1LL}},{{0x01D46DDDA50130BDLL,0xF41B01F3A26FD9B1LL,0x856F85125D174A4ELL,0x856F85125D174A4ELL},{(-1L),(-1L),0xE6C38B7838DAF927LL,0xD45486AED00FCFD2LL},{0x4A8B8A9E34A1F9C7LL,1L,0x96823B8B04937E18LL,(-1L)},{0x01D46DDDA50130BDLL,0xD45486AED00FCFD2LL,3L,0x96823B8B04937E18LL},{0xF41B01F3A26FD9B1LL,0xD45486AED00FCFD2LL,0L,(-1L)},{0xD45486AED00FCFD2LL,1L,0xBCAFC6E9FDD4BB73LL,0xD45486AED00FCFD2LL},{0x1FA469E0E8E86CDFLL,(-1L),1L,0x856F85125D174A4ELL},{0xDE503E5B4A07C2FALL,0xF41B01F3A26FD9B1LL,0L,0xF41B01F3A26FD9B1LL},{0xEF0CCBDA0A4AC4B5LL,4L,0x1FA469E0E8E86CDFLL,0xEF58199E1D3A19CDLL}},{{0x01D46DDDA50130BDLL,0xEF0CCBDA0A4AC4B5LL,0x1A92EE0055759506LL,0x856F85125D174A4ELL},{(-1L),0x4A8B8A9E34A1F9C7LL,0xE6C38B7838DAF927LL,9L},{(-1L),1L,0x1A92EE0055759506LL,(-1L)},{0x01D46DDDA50130BDLL,9L,0x1FA469E0E8E86CDFLL,0x96823B8B04937E18LL},{0xEF0CCBDA0A4AC4B5LL,0xDE503E5B4A07C2FALL,0L,0x4A8B8A9E34A1F9C7LL},{0xDE503E5B4A07C2FALL,1L,1L,0xDE503E5B4A07C2FALL},{0x1FA469E0E8E86CDFLL,(-1L),0xBCAFC6E9FDD4BB73LL,0x00CA15951910FAEFLL},{1L,0x96823B8B04937E18LL,(-1L),0x856F85125D174A4ELL},{0x1A92EE0055759506LL,0L,0xB48B8C355F3B8518LL,0x856F85125D174A4ELL}},{{0x2E0B640F5DAF3774LL,0x96823B8B04937E18LL,2L,0x00CA15951910FAEFLL},{0x1FA469E0E8E86CDFLL,0x01D46DDDA50130BDLL,0x4A8B8A9E34A1F9C7LL,0xBCAFC6E9FDD4BB73LL},{3L,9L,0x00CA15951910FAEFLL,0x1FA469E0E8E86CDFLL},{0x2E0B640F5DAF3774LL,0xBCAFC6E9FDD4BB73LL,0x2E0B640F5DAF3774LL,2L},{0x96823B8B04937E18LL,4L,(-1L),3L},{4L,9L,0L,4L},{0x6141EC6DEED17B13LL,0x1FA469E0E8E86CDFLL,0L,0x00CA15951910FAEFLL},{4L,0x856F85125D174A4ELL,(-1L),0x96823B8B04937E18LL},{0x96823B8B04937E18LL,0L,0x2E0B640F5DAF3774LL,0x1A92EE0055759506LL}},{{0x2E0B640F5DAF3774LL,0x1A92EE0055759506LL,0x00CA15951910FAEFLL,0x00CA15951910FAEFLL},{3L,3L,0x4A8B8A9E34A1F9C7LL,1L},{0x1FA469E0E8E86CDFLL,9L,2L,0x01D46DDDA50130BDLL},{0x2E0B640F5DAF3774LL,1L,0xB48B8C355F3B8518LL,2L},{0x1A92EE0055759506LL,1L,(-1L),0x01D46DDDA50130BDLL},{1L,9L,0xE6C38B7838DAF927LL,1L},{0x6141EC6DEED17B13LL,3L,9L,0x00CA15951910FAEFLL},{0xBCAFC6E9FDD4BB73LL,0x1A92EE0055759506LL,(-1L),0x1A92EE0055759506LL},{0x856F85125D174A4ELL,0L,0x6141EC6DEED17B13LL,0x96823B8B04937E18LL}},{{0x2E0B640F5DAF3774LL,0x856F85125D174A4ELL,0L,0x00CA15951910FAEFLL},{0x01D46DDDA50130BDLL,0x1FA469E0E8E86CDFLL,0x4A8B8A9E34A1F9C7LL,4L},{0x01D46DDDA50130BDLL,9L,0L,3L},{0x2E0B640F5DAF3774LL,4L,0x6141EC6DEED17B13LL,2L},{0x856F85125D174A4ELL,0xBCAFC6E9FDD4BB73LL,(-1L),0x1FA469E0E8E86CDFLL},{0xBCAFC6E9FDD4BB73LL,9L,9L,0xBCAFC6E9FDD4BB73LL},{0x6141EC6DEED17B13LL,0x01D46DDDA50130BDLL,0xE6C38B7838DAF927LL,0x00CA15951910FAEFLL},{1L,0x96823B8B04937E18LL,(-1L),0x856F85125D174A4ELL},{0x1A92EE0055759506LL,0L,0xB48B8C355F3B8518LL,0x856F85125D174A4ELL}}};
    uint32_t ****l_3299 = &g_1392;
    uint32_t *****l_3298 = &l_3299;
    uint16_t l_3323[6][3][1] = {{{0xBEF9L},{1UL},{0x486EL}},{{1UL},{0xBEF9L},{1UL}},{{0x486EL},{1UL},{0xBEF9L}},{{1UL},{0x486EL},{1UL}},{{0xBEF9L},{1UL},{0x486EL}},{{1UL},{0xBEF9L},{1UL}}};
    uint32_t l_3338 = 18446744073709551615UL;
    uint16_t ****l_3392 = &g_594;
    uint16_t l_3431 = 0x384BL;
    int8_t l_3459 = 0L;
    uint8_t **l_3464 = (void*)0;
    uint8_t l_3489 = 0UL;
    int32_t l_3514 = 0L;
    int64_t l_3549 = (-1L);
    uint32_t l_3551 = 0x811FB853L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_3225[i] = 0L;
    return (**l_2712);
}


/* ------------------------------------------ */
/* 
 * reads : g_235 g_87 g_2563 g_21 g_1568
 * writes: g_235 g_87 g_1568
 */
static uint64_t  func_9(int32_t  p_10, int64_t  p_11)
{ /* block id: 875 */
    const uint32_t l_2009 = 18446744073709551614UL;
    int32_t *l_2012 = &g_1568[1][0];
    int32_t l_2020 = (-7L);
    int32_t l_2027 = (-9L);
    int32_t l_2032[2];
    uint32_t ***l_2093 = &g_1393[7];
    int8_t l_2107[2];
    uint8_t **l_2139 = &g_2008[4];
    uint64_t ***l_2148[5] = {&g_1301[1],&g_1301[1],&g_1301[1],&g_1301[1],&g_1301[1]};
    int32_t l_2195 = 0x760B7A46L;
    uint8_t l_2284[5][3];
    uint32_t **l_2290[4];
    uint8_t l_2371[5] = {0UL,0UL,0UL,0UL,0UL};
    uint32_t ***l_2451 = &g_358;
    int64_t l_2502 = 0x66DB78D82316B0F5LL;
    uint8_t l_2557 = 255UL;
    int i, j;
    for (i = 0; i < 2; i++)
        l_2032[i] = (-1L);
    for (i = 0; i < 2; i++)
        l_2107[i] = 0L;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
            l_2284[i][j] = 0xCAL;
    }
    for (i = 0; i < 4; i++)
        l_2290[i] = &g_1243;
    for (g_235 = 0; (g_235 == (-20)); --g_235)
    { /* block id: 878 */
        uint32_t ***l_1989 = &g_358;
        const uint32_t ****l_1991[9][10] = {{&g_1990[7],&g_1990[7],&g_1990[2],&g_1990[7],&g_1990[7],&g_1990[7],(void*)0,&g_1990[7],&g_1990[7],&g_1990[7]},{&g_1990[0],&g_1990[7],&g_1990[0],(void*)0,&g_1990[6],&g_1990[7],&g_1990[7],&g_1990[7],&g_1990[7],(void*)0},{(void*)0,&g_1990[7],&g_1990[7],(void*)0,&g_1990[7],(void*)0,(void*)0,&g_1990[7],(void*)0,&g_1990[7]},{&g_1990[2],&g_1990[2],&g_1990[0],&g_1990[6],&g_1990[7],&g_1990[7],&g_1990[5],&g_1990[7],&g_1990[7],&g_1990[7]},{(void*)0,&g_1990[7],&g_1990[2],&g_1990[7],&g_1990[5],&g_1990[6],(void*)0,&g_1990[5],(void*)0,&g_1990[7]},{&g_1990[7],(void*)0,&g_1990[2],(void*)0,&g_1990[7],&g_1990[0],&g_1990[3],&g_1990[7],(void*)0,&g_1990[7]},{(void*)0,&g_1990[7],(void*)0,&g_1990[7],&g_1990[0],&g_1990[0],&g_1990[7],(void*)0,&g_1990[7],(void*)0},{&g_1990[7],&g_1990[7],&g_1990[0],(void*)0,(void*)0,&g_1990[7],&g_1990[2],&g_1990[7],&g_1990[5],&g_1990[6]},{&g_1990[3],&g_1990[6],&g_1990[7],&g_1990[7],(void*)0,&g_1990[7],&g_1990[7],&g_1990[6],&g_1990[3],(void*)0}};
        int8_t **l_1992 = (void*)0;
        int32_t l_2001 = 0xCAB1FA29L;
        uint8_t *l_2005 = &g_117;
        int32_t l_2028[6];
        int32_t *l_2048 = &g_108;
        int32_t *l_2049 = &l_2001;
        int32_t *l_2050 = &l_2001;
        int32_t *l_2051 = &g_18;
        int32_t *l_2052 = (void*)0;
        int32_t *l_2053 = &g_240;
        int32_t *l_2054 = (void*)0;
        int32_t *l_2055 = &g_236;
        int32_t *l_2056 = &l_2032[1];
        int32_t *l_2057 = (void*)0;
        int32_t *l_2058 = (void*)0;
        int32_t *l_2059 = &g_108;
        int32_t *l_2060 = &g_21[4][2][0];
        int32_t *l_2061 = (void*)0;
        int32_t *l_2062[1][7] = {{&g_913,&g_913,&l_2032[1],&g_913,&g_913,&l_2032[1],&g_913}};
        uint32_t l_2063[1][10][3] = {{{0UL,0x42B78A2EL,6UL},{0x5448835AL,0x42B78A2EL,0x42B78A2EL},{0x66CBBC9BL,0x42B78A2EL,1UL},{0UL,0x42B78A2EL,6UL},{0x5448835AL,0x42B78A2EL,0x42B78A2EL},{0x66CBBC9BL,0x42B78A2EL,1UL},{0UL,0x42B78A2EL,6UL},{0x5448835AL,0x42B78A2EL,0x42B78A2EL},{0x66CBBC9BL,0x42B78A2EL,1UL},{0UL,0x42B78A2EL,6UL}}};
        int16_t *l_2076 = (void*)0;
        const uint32_t l_2137 = 0UL;
        uint16_t l_2138 = 0x4BD0L;
        uint64_t l_2170 = 0xD45AFC3D0804E3F5LL;
        int8_t * const **l_2200 = (void*)0;
        uint16_t l_2278 = 0x87A2L;
        uint64_t l_2409[6][6] = {{0UL,4UL,4UL,0UL,4UL,4UL},{0UL,4UL,4UL,0UL,4UL,4UL},{0UL,4UL,4UL,0UL,4UL,4UL},{0UL,4UL,4UL,0UL,4UL,4UL},{0UL,4UL,4UL,0UL,4UL,4UL},{0UL,4UL,4UL,0UL,4UL,4UL}};
        uint32_t *** const *l_2432 = &l_1989;
        int16_t * const *l_2498 = &g_554;
        int16_t * const **l_2497 = &l_2498;
        uint16_t **l_2606 = &g_121;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_2028[i] = (-6L);
    }
    for (g_87 = 0; (g_87 <= 7); g_87 += 1)
    { /* block id: 1131 */
        (*l_2012) &= (*g_2563);
    }
    return (*l_2012);
}


/* ------------------------------------------ */
/* 
 * reads : g_18 g_106 g_1281 g_102 g_1302 g_87 g_121 g_122 g_77 g_8 g_554 g_51 g_1169 g_991 g_179 g_1983
 * writes: g_18 g_21 g_411 g_991 g_106 g_1281 g_87 g_1568
 */
static uint8_t  func_14(int64_t  p_15)
{ /* block id: 2 */
    uint32_t l_16[3];
    int32_t *l_17[7][2][5] = {{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}},{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}},{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}},{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}},{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}},{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}},{{&g_18,&g_18,&g_8[2][5],&g_18,&g_18},{&g_8[0][4],(void*)0,(void*)0,(void*)0,&g_8[0][4]}}};
    uint16_t **l_1752 = (void*)0;
    int16_t **l_1763 = &g_554;
    uint32_t l_1779[5];
    int32_t l_1907 = 0x3444BCA7L;
    int64_t **l_1927 = (void*)0;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_16[i] = 0x9D60A221L;
    for (i = 0; i < 5; i++)
        l_1779[i] = 0UL;
    g_18 = l_16[0];
    for (g_18 = 0; (g_18 == 25); g_18++)
    { /* block id: 6 */
        int16_t l_1744 = 0L;
        int16_t **l_1761 = &g_554;
        int32_t l_1764 = 1L;
        int32_t l_1770 = (-10L);
        int32_t l_1772 = 0x6DDDCA58L;
        int32_t l_1773 = 0xFD3D741CL;
        int32_t l_1775 = 0xEA3F3B71L;
        int32_t l_1776 = (-6L);
        int32_t l_1777[10];
        uint8_t l_1800 = 0UL;
        const uint32_t *l_1858[3];
        const uint32_t **l_1857 = &l_1858[2];
        const int8_t *l_1882 = (void*)0;
        const int8_t **l_1881 = &l_1882;
        int32_t *l_1883[4] = {&g_21[3][0][0],&g_21[3][0][0],&g_21[3][0][0],&g_21[3][0][0]};
        int16_t l_1945 = (-1L);
        uint32_t ** const *l_1981 = (void*)0;
        int i;
        for (i = 0; i < 10; i++)
            l_1777[i] = 2L;
        for (i = 0; i < 3; i++)
            l_1858[i] = &g_918;
        if (p_15)
            break;
        for (p_15 = 0; (p_15 <= 2); p_15 += 1)
        { /* block id: 10 */
            int32_t *l_49[3];
            uint32_t l_1802 = 1UL;
            int8_t ***l_1896 = (void*)0;
            uint32_t l_1932 = 0x6656C3EEL;
            int i;
            for (i = 0; i < 3; i++)
                l_49[i] = &g_21[4][5][1];
            for (g_21[4][2][0] = 0; (g_21[4][2][0] <= 1); g_21[4][2][0] += 1)
            { /* block id: 13 */
                int32_t *l_48 = &g_21[4][2][0];
                int32_t **l_47 = &l_48;
                int16_t *l_50 = &g_51;
                uint64_t *l_1041 = &g_1042;
                int32_t l_1723 = 1L;
                int i, j, k;
            }
            for (g_411 = 0; g_411 < 8; g_411 += 1)
            {
                for (l_1744 = 0; l_1744 < 7; l_1744 += 1)
                {
                    g_991[g_411][l_1744] = 0x16L;
                }
            }
        }
        for (g_106 = (-30); (g_106 > (-29)); g_106 = safe_add_func_int64_t_s_s(g_106, 4))
        { /* block id: 862 */
            uint64_t l_1982 = 18446744073709551614UL;
            for (g_1281 = 0; (g_1281 <= 5); g_1281 += 1)
            { /* block id: 865 */
                int i;
                return g_102[g_1281];
            }
            g_1568[0][3] = (((safe_lshift_func_uint8_t_u_u((~((safe_mod_func_uint64_t_u_u(((*g_1302) &= 0x256BEA36A465758ELL), (((safe_rshift_func_int8_t_s_u((-1L), (safe_div_func_int8_t_s_s(((((((safe_sub_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((safe_add_func_uint32_t_u_u(((safe_mod_func_uint8_t_u_u(((safe_mod_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(p_15, (safe_unary_minus_func_int16_t_s(p_15)))), ((safe_sub_func_int8_t_s_s((l_1981 == (void*)0), 0xC5L)) | ((*g_121) > l_1982)))) , 0xACL), l_1982)) , p_15), (*g_77))), 0xD7E749348E7588DBLL)), p_15)) || (*g_554)) || p_15) || g_1169) || 6UL) != p_15), g_991[7][4])))) > 0UL) | p_15))) != 0x5531L)), g_179[3])) , l_1982) != p_15);
        }
    }
    return g_1983;
}


/* ------------------------------------------ */
/* 
 * reads : g_21 g_236 g_359 g_87 g_121 g_102 g_1374 g_529 g_106 g_178 g_1318 g_1319 g_384 g_76 g_1392 g_226 g_1300 g_1301 g_554 g_239 g_51 g_1031 g_1302 g_1281 g_110 g_1244 g_358 g_122 g_240 g_667 g_918 g_991 g_1042 g_411 g_412 g_1584 g_600 g_601 g_117 g_913 g_1568 g_77 g_1638 g_1169 g_1674 g_18 g_1393 g_235
 * writes: g_102 g_122 g_1319 g_384 g_77 g_1404 g_235 g_1031 g_87 g_1244 g_991 g_106 g_51 g_913 g_1568 g_117 g_236 g_237 g_412 g_1281
 */
static uint16_t  func_30(int32_t * p_31, int32_t * p_32)
{ /* block id: 615 */
    int16_t l_1366 = 0x2069L;
    int32_t l_1367 = 0x337A2600L;
    int8_t l_1377 = 9L;
    uint32_t *l_1382 = &g_918;
    uint32_t *l_1383 = &g_1281;
    int32_t l_1416[8] = {1L,1L,0xE6E997E7L,1L,1L,0xE6E997E7L,1L,1L};
    int32_t l_1427 = 0x99BCE413L;
    uint32_t ***l_1428 = &g_358;
    uint64_t l_1513 = 18446744073709551609UL;
    uint16_t l_1580 = 0x7BCCL;
    uint64_t l_1601 = 0x9743D66310974B49LL;
    int32_t *l_1712 = &l_1416[6];
    int32_t *l_1713[5] = {&g_913,&g_913,&g_913,&g_913,&g_913};
    uint64_t l_1714 = 0xE77A0B7CDB006B8CLL;
    int i;
    if (((safe_sub_func_uint8_t_u_u((safe_div_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((0xF244L < (safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((4294967295UL | (*p_32)), 6)), (safe_mul_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((((*p_31) ^ ((*g_359) = 0xE57D3210L)) , (((safe_mul_func_int8_t_s_s((((safe_div_func_int32_t_s_s((*p_31), l_1366)) != ((l_1367 = 65535UL) > ((*g_121) = (safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_s((safe_div_func_uint32_t_u_u(3UL, l_1366)), 14)), g_87))))) < (*g_359)), l_1366)) & g_1374[2][2]) && g_529)), l_1366)), 7)) == l_1366), 0xCCL))))), 0L)), g_106)), 0x0EL)) | g_178))
    { /* block id: 619 */
lbl_1711:
        (*g_1318) = (*g_1318);
    }
    else
    { /* block id: 621 */
        int8_t l_1378 = 0xB5L;
        int64_t *l_1403[9];
        int32_t l_1405[6][6] = {{0L,0xECA60A71L,0L,0xCFC83F27L,0L,0xECA60A71L},{0x0F00DB27L,0xECA60A71L,0x7E5A6562L,0xECA60A71L,0x0F00DB27L,0xECA60A71L},{0L,0xCFC83F27L,0L,0xECA60A71L,0L,0xCFC83F27L},{0x0F00DB27L,0xCFC83F27L,0x7E5A6562L,0xCFC83F27L,0x0F00DB27L,0xCFC83F27L},{0L,0xECA60A71L,0L,0xCFC83F27L,0L,0xECA60A71L},{0x0F00DB27L,0xECA60A71L,0x7E5A6562L,0xECA60A71L,0x0F00DB27L,0xECA60A71L}};
        int32_t l_1406 = 0x611F4A78L;
        const uint32_t *l_1454 = &g_786;
        const uint32_t **l_1453 = &l_1454;
        const uint32_t ***l_1452 = &l_1453;
        int8_t l_1538 = 0x6EL;
        uint16_t l_1644 = 65535UL;
        uint32_t l_1652[6][8][3] = {{{4294967295UL,0x0630BA7BL,0x45AB7854L},{1UL,4294967295UL,0xBDEB863BL},{0xA14C276BL,0xB2AD2937L,4UL},{0x0630BA7BL,1UL,0xBDEB863BL},{0x0120AE67L,0xA9BB338FL,0x45AB7854L},{1UL,0x0120AE67L,0x3C772947L},{0x1607A910L,0xA14C276BL,0xA14C276BL},{0x1607A910L,0x3C772947L,0x0120AE67L}},{{1UL,0x45AB7854L,0xA9BB338FL},{0x0120AE67L,0xBDEB863BL,1UL},{0x0630BA7BL,4UL,0xB2AD2937L},{0xA14C276BL,0xBDEB863BL,4294967295UL},{1UL,0x45AB7854L,0x0630BA7BL},{4294967295UL,0x3C772947L,1UL},{1UL,0xA14C276BL,1UL},{0UL,0x0120AE67L,0x0630BA7BL}},{{0xA693BA52L,0xA9BB338FL,4294967295UL},{8UL,1UL,0xB2AD2937L},{0xA9BB338FL,0xB2AD2937L,1UL},{8UL,4294967295UL,0xA9BB338FL},{0xA693BA52L,0x0630BA7BL,0x0120AE67L},{0UL,1UL,0xA14C276BL},{1UL,1UL,0x3C772947L},{4294967295UL,0x0630BA7BL,0x45AB7854L}},{{1UL,4294967295UL,0xBDEB863BL},{0xA14C276BL,0xB2AD2937L,4UL},{0x0630BA7BL,1UL,0xBDEB863BL},{0x0120AE67L,0xA9BB338FL,0x45AB7854L},{1UL,0x0120AE67L,0x3C772947L},{0x1607A910L,0xA14C276BL,0xA14C276BL},{0x1607A910L,0x3C772947L,0x0120AE67L},{1UL,0x45AB7854L,0xA9BB338FL}},{{0x0120AE67L,0xBDEB863BL,1UL},{0x0630BA7BL,4UL,0xB2AD2937L},{0xA14C276BL,0xBDEB863BL,4294967295UL},{1UL,0x45AB7854L,0x0630BA7BL},{4294967295UL,0x3C772947L,1UL},{1UL,0xA14C276BL,1UL},{0UL,0x0120AE67L,0x0630BA7BL},{0xA693BA52L,0xA9BB338FL,4294967295UL}},{{8UL,1UL,0xB2AD2937L},{0xA9BB338FL,0xB2AD2937L,1UL},{8UL,4294967295UL,0xA9BB338FL},{0xA693BA52L,0x0630BA7BL,0x0120AE67L},{0UL,1UL,0xA14C276BL},{1UL,1UL,0x3C772947L},{4294967295UL,0x0630BA7BL,0x45AB7854L},{0xA693BA52L,1UL,4294967295UL}}};
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1403[i] = (void*)0;
        for (g_384 = 16; (g_384 >= 2); --g_384)
        { /* block id: 624 */
            return l_1377;
        }
        if ((*p_32))
        { /* block id: 627 */
            const int32_t **l_1379 = (void*)0;
            const int32_t *l_1381 = &g_238;
            const int32_t **l_1380 = &l_1381;
            (*l_1380) = ((*g_1318) = p_32);
        }
        else
        { /* block id: 630 */
            (*g_76) = p_31;
        }
lbl_1574:
        l_1367 ^= (l_1378 & ((l_1382 = p_31) != l_1383));
        if ((l_1406 = ((safe_lshift_func_uint8_t_u_s(((((((255UL & ((((safe_add_func_int8_t_s_s((safe_mod_func_uint32_t_u_u((safe_sub_func_int64_t_s_s(g_102[3], (((void*)0 == g_1392) >= (safe_rshift_func_uint8_t_u_s(((safe_sub_func_uint64_t_u_u(l_1366, (((safe_unary_minus_func_int16_t_s(l_1377)) , (((safe_rshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(((g_1404 = l_1403[2]) != ((l_1405[1][2] |= l_1378) , l_1403[4])), l_1378)), 5)) , 0x664CL) ^ l_1378)) == g_226[1]))) | 1UL), 6))))), l_1378)), 255UL)) ^ l_1367) != l_1378) ^ 0x10B47BD4L)) , l_1367) || 0xF4ABL) && l_1377) != 65526UL) || l_1378), 5)) , 0L)))
        { /* block id: 638 */
            uint16_t l_1410[4] = {65531UL,65531UL,65531UL,65531UL};
            int8_t *l_1415 = (void*)0;
            int16_t *l_1422 = &g_1031[0];
            int32_t l_1487 = 0xDDF756E6L;
            int32_t l_1510[3];
            uint16_t l_1550 = 0x0F84L;
            int8_t l_1647 = 6L;
            int32_t *l_1673 = &l_1510[2];
            int i;
            for (i = 0; i < 3; i++)
                l_1510[i] = (-10L);
            for (g_235 = 0; (g_235 >= (-13)); --g_235)
            { /* block id: 641 */
                (*g_1318) = p_31;
            }
            if (((((~l_1410[0]) < (((((safe_rshift_func_uint16_t_u_u(l_1367, (safe_sub_func_int64_t_s_s((l_1416[6] = ((void*)0 != l_1415)), ((0xB67EC819L && (~(((*g_1300) == (*g_1300)) ^ (safe_mul_func_uint16_t_u_u(((l_1427 = (((safe_lshift_func_int16_t_s_u(((*l_1422) &= (*g_554)), 5)) , ((safe_sub_func_uint64_t_u_u((--(*g_1302)), 0xAE4461F04F845A27LL)) || g_1281)) != g_1374[2][2])) , 65535UL), l_1367))))) & 6UL))))) , l_1428) == (void*)0) || g_110) | 0UL)) != l_1405[1][2]) == l_1406))
            { /* block id: 648 */
                uint16_t l_1457 = 65532UL;
                int32_t l_1488 = 0x7C2C2297L;
                int32_t l_1506 = 0x10060CFFL;
                int32_t l_1508 = 0x197A094BL;
                int32_t l_1512[9] = {7L,0x9C79FA74L,7L,0x9C79FA74L,7L,0x9C79FA74L,7L,0x9C79FA74L,7L};
                uint32_t **l_1531 = &l_1382;
                int32_t *l_1533 = &l_1405[1][2];
                int32_t *l_1534 = &g_240;
                int32_t *l_1535 = &l_1512[7];
                int32_t *l_1536 = &g_108;
                int32_t *l_1537[10][3][5] = {{{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]},{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406},{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]}},{{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0},{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]},{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406}},{{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]},{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0},{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]}},{{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406},{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]},{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0}},{{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]},{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406},{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]}},{{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0},{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]},{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406}},{{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]},{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0},{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]}},{{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406},{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]},{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0}},{{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]},{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406},{&l_1510[2],&l_1367,(void*)0,&g_236,&l_1510[2]}},{{(void*)0,&l_1427,&l_1510[2],&l_1427,(void*)0},{&l_1510[2],&g_236,(void*)0,&l_1367,&l_1510[2]},{&l_1406,&l_1427,&l_1512[1],&l_1427,&l_1406}}};
                uint64_t l_1539 = 0x070AEDC64EE9B59ALL;
                int i, j, k;
                for (g_1244 = (-13); (g_1244 == (-6)); g_1244 = safe_add_func_int32_t_s_s(g_1244, 1))
                { /* block id: 651 */
                    int8_t *l_1437 = (void*)0;
                    int8_t *l_1438 = (void*)0;
                    int8_t *l_1439 = &l_1377;
                    uint64_t **l_1472[9];
                    int32_t l_1486 = 0x952A91D5L;
                    uint32_t *l_1489 = &g_1374[2][2];
                    int8_t *l_1490[3];
                    int32_t l_1509 = 9L;
                    int32_t l_1511[3];
                    int32_t *l_1532 = &g_106;
                    int i;
                    for (i = 0; i < 9; i++)
                        l_1472[i] = (void*)0;
                    for (i = 0; i < 3; i++)
                        l_1490[i] = &g_235;
                    for (i = 0; i < 3; i++)
                        l_1511[i] = (-1L);
                    if ((((((((g_991[7][4] = ((safe_mod_func_int32_t_s_s(((safe_sub_func_uint64_t_u_u(((**g_358) != (safe_mul_func_int8_t_s_s(((*l_1439) = 6L), ((safe_lshift_func_uint8_t_u_u((((*g_1302) ^= ((safe_mul_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_add_func_int16_t_s_s(((((((((safe_mod_func_int32_t_s_s(0xC63FEDDDL, l_1405[1][2])) && (l_1452 != (void*)0)) <= (safe_rshift_func_int16_t_s_u(((*l_1422) = (((l_1457 < (safe_lshift_func_uint16_t_u_u(((safe_add_func_int32_t_s_s((safe_div_func_int16_t_s_s(((l_1406 |= (safe_mul_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_s((safe_div_func_int8_t_s_s((safe_mul_func_int16_t_s_s((l_1472[1] == ((safe_add_func_uint16_t_u_u((!(((*g_121)--) || ((l_1487 = (safe_rshift_func_int8_t_s_u(((safe_rshift_func_int16_t_s_u(((safe_mod_func_uint16_t_u_u(((safe_div_func_int8_t_s_s((((void*)0 != &p_32) || 0x47AAACA8CA5D37FBLL), l_1486)) != l_1486), l_1416[4])) >= l_1405[0][3]), 8)) & l_1486), l_1486))) >= 0xCA11821B963A2907LL))), l_1457)) , (void*)0)), l_1410[3])), l_1405[5][3])), g_240)) == 0x8D129EABL), l_1410[2]))) , l_1487), (*g_554))), l_1488)) == l_1405[4][2]), 6))) > g_667) <= 0x5D04B057L)), l_1410[0]))) , (*p_31)) ^ (*p_31)) , l_1489) == p_31) > l_1366), 65535UL)), 5)), g_918)), l_1486)) && 0x36EA67A7CCBD3A9CLL)) >= g_991[3][2]), 3)) || 6UL)))), l_1427)) <= 1UL), l_1378)) > l_1457)) , g_1042) , l_1416[7]) && g_411) <= l_1405[1][2]) | 0x62L) > 0x91C8D9CFF6365B61LL))
                    { /* block id: 659 */
                        int32_t *l_1491 = &l_1416[7];
                        int32_t *l_1492 = &g_236;
                        int32_t *l_1493 = (void*)0;
                        int32_t *l_1494 = &l_1487;
                        int32_t *l_1495 = &l_1487;
                        int32_t *l_1496 = (void*)0;
                        int32_t *l_1497 = &l_1367;
                        int32_t l_1498 = (-1L);
                        int32_t *l_1499 = (void*)0;
                        int32_t *l_1500 = (void*)0;
                        int32_t *l_1501 = &g_236;
                        int32_t *l_1502 = &l_1486;
                        int32_t *l_1503 = (void*)0;
                        int32_t *l_1504 = &l_1405[2][0];
                        int32_t *l_1505 = &g_238;
                        int32_t *l_1507[4];
                        int i;
                        for (i = 0; i < 4; i++)
                            l_1507[i] = (void*)0;
                        if ((*p_32))
                            break;
                        l_1513--;
                        (*g_76) = &l_1510[2];
                        (*g_76) = p_31;
                    }
                    else
                    { /* block id: 664 */
                        uint8_t l_1516 = 0x14L;
                        if (l_1516)
                            break;
                    }
                    l_1416[6] = ((*l_1532) = (safe_rshift_func_uint16_t_u_u((*g_121), (l_1406 <= (l_1410[0] <= (l_1508 , (safe_mul_func_uint8_t_u_u(((((safe_mul_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(((safe_div_func_int64_t_s_s((safe_add_func_int16_t_s_s(l_1508, (safe_sub_func_int8_t_s_s((l_1486 , ((((void*)0 != &g_178) == (0L < 0x30A89ED6E1BE5C37LL)) , (-1L))), g_412)))), g_1031[0])) > l_1510[2]), l_1410[0])), (*g_121))) & 0x9BF54379F8A23666LL) , (void*)0) != l_1531), g_1042))))))));
                }
                l_1539++;
                (*l_1535) = (*p_32);
            }
            else
            { /* block id: 672 */
                int32_t *l_1542 = &g_106;
                int32_t *l_1543 = &g_913;
                int32_t *l_1544 = &g_238;
                int32_t *l_1545 = &l_1406;
                int32_t *l_1546 = &g_236;
                int32_t *l_1547 = (void*)0;
                int32_t *l_1548 = &l_1416[4];
                int32_t *l_1549[2][6][9] = {{{&l_1416[1],&g_18,&l_1405[4][1],&l_1405[4][1],&g_18,&l_1416[1],&l_1487,&g_18,&l_1487},{&l_1416[1],&g_18,&l_1405[4][1],&l_1405[4][1],&g_18,&l_1416[1],&l_1487,&g_18,&l_1487},{&l_1416[1],&g_18,&l_1405[4][1],&l_1405[4][1],&g_18,&l_1416[1],&l_1487,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237}},{{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237},{&l_1416[6],&l_1405[4][1],&g_238,&g_238,&l_1405[4][1],&l_1416[6],&g_237,&l_1405[4][1],&g_237}}};
                int i, j, k;
                ++l_1550;
            }
            for (l_1377 = (-9); (l_1377 >= (-1)); l_1377++)
            { /* block id: 677 */
                int16_t l_1557[10];
                int32_t l_1565 = 0L;
                int32_t l_1567[2];
                int32_t *l_1598 = &g_236;
                int32_t *l_1599 = &g_1568[0][3];
                int32_t *l_1600[10];
                uint16_t l_1606 = 6UL;
                uint16_t **l_1609 = &g_121;
                int i;
                for (i = 0; i < 10; i++)
                    l_1557[i] = 0xE52CL;
                for (i = 0; i < 2; i++)
                    l_1567[i] = (-1L);
                for (i = 0; i < 10; i++)
                    l_1600[i] = &l_1565;
                for (g_51 = (-29); (g_51 == (-13)); g_51 = safe_add_func_int32_t_s_s(g_51, 8))
                { /* block id: 680 */
                    int8_t l_1566 = 0L;
                    int32_t l_1569 = 0x33D85675L;
                    int32_t l_1570[7][5] = {{(-7L),0x0DFA28FBL,0x43F26032L,0x43F26032L,0x0DFA28FBL},{0x0DFA28FBL,0xA1EBA4EBL,(-7L),0x0DFA28FBL,0x43F26032L},{(-2L),0x0DFA28FBL,0xBCA134B3L,0x0DFA28FBL,(-2L)},{(-7L),0x9F63467EL,0xA1EBA4EBL,0x43F26032L,0x9F63467EL},{(-2L),0xA1EBA4EBL,0xA1EBA4EBL,(-2L),0x43F26032L},{0x0DFA28FBL,(-2L),0xBCA134B3L,0x9F63467EL,0x9F63467EL},{(-7L),(-2L),(-7L),0x43F26032L,(-2L)}};
                    int i, j;
                    if ((*p_32))
                    { /* block id: 681 */
                        int32_t *l_1558 = (void*)0;
                        int32_t *l_1559 = &g_913;
                        int32_t *l_1560 = &l_1416[0];
                        int32_t *l_1561 = &l_1510[1];
                        int32_t *l_1562 = (void*)0;
                        int32_t *l_1563 = &g_238;
                        int32_t *l_1564[6] = {&g_236,(void*)0,(void*)0,&g_236,(void*)0,(void*)0};
                        uint32_t l_1571 = 0x7841A36EL;
                        int i;
                        --l_1571;
                        if (g_667)
                            goto lbl_1574;
                    }
                    else
                    { /* block id: 684 */
                        int32_t *l_1575 = &l_1405[1][2];
                        int32_t *l_1576 = (void*)0;
                        int32_t *l_1577 = &l_1567[0];
                        int32_t *l_1578[8][4][6] = {{{&l_1510[2],&l_1510[0],&l_1565,&l_1565,&l_1510[0],&l_1510[2]},{&l_1569,&l_1510[2],&l_1565,&l_1510[2],&l_1569,&l_1569},{(void*)0,&l_1510[2],&l_1510[2],(void*)0,&l_1510[0],(void*)0},{(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2],(void*)0}},{{&l_1569,&l_1569,&l_1510[2],&l_1565,&l_1510[2],&l_1569},{&l_1510[2],&l_1510[0],&l_1565,&l_1565,&l_1510[0],&l_1510[2]},{&l_1569,&l_1510[2],&l_1565,&l_1510[2],&l_1569,&l_1569},{(void*)0,&l_1510[2],&l_1510[2],(void*)0,&l_1510[0],(void*)0}},{{(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2],(void*)0},{&l_1569,&l_1569,&l_1510[2],&l_1565,&l_1510[2],&l_1569},{&l_1510[2],&l_1510[0],&l_1565,&l_1565,&l_1510[0],&l_1510[2]},{&l_1569,(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2]}},{{&l_1565,(void*)0,(void*)0,&l_1565,&l_1569,&l_1565},{&l_1565,&l_1569,&l_1565,(void*)0,(void*)0,&l_1565},{&l_1510[2],&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2]},{(void*)0,&l_1569,&l_1510[0],&l_1510[0],&l_1569,(void*)0}},{{&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2]},{&l_1565,(void*)0,(void*)0,&l_1565,&l_1569,&l_1565},{&l_1565,&l_1569,&l_1565,(void*)0,(void*)0,&l_1565},{&l_1510[2],&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2]}},{{(void*)0,&l_1569,&l_1510[0],&l_1510[0],&l_1569,(void*)0},{&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2]},{&l_1565,(void*)0,(void*)0,&l_1565,&l_1569,&l_1565},{&l_1565,&l_1569,&l_1565,(void*)0,(void*)0,&l_1565}},{{&l_1510[2],&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2]},{(void*)0,&l_1569,&l_1510[0],&l_1510[0],&l_1569,(void*)0},{&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2]},{&l_1565,(void*)0,(void*)0,&l_1565,&l_1569,&l_1565}},{{&l_1565,&l_1569,&l_1565,(void*)0,(void*)0,&l_1565},{&l_1510[2],&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2]},{(void*)0,&l_1569,&l_1510[0],&l_1510[0],&l_1569,(void*)0},{&l_1510[2],(void*)0,&l_1510[0],(void*)0,&l_1510[2],&l_1510[2]}}};
                        int64_t l_1579 = 0L;
                        int i, j, k;
                        --l_1580;
                        if (l_1580)
                            continue;
                    }
                }
                g_913 ^= ((~(**g_358)) && ((((g_1584 || l_1567[0]) <= (safe_mod_func_int16_t_s_s((*g_554), ((***g_600) = l_1410[1])))) >= l_1427) ^ ((((safe_lshift_func_int16_t_s_u(((safe_mul_func_uint8_t_u_u((((safe_lshift_func_int8_t_s_u((l_1510[2] , g_117), 3)) || (safe_sub_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s((safe_unary_minus_func_uint16_t_u((l_1367 = 0x8FFEL))), l_1510[2])), l_1567[0]))) <= 65535UL), g_384)) , l_1487), 8)) , (**g_358)) > 0x00FAEDF5L) < l_1557[0])));
                ++l_1601;
                for (g_913 = 2; (g_913 < 2); g_913 = safe_add_func_int64_t_s_s(g_913, 2))
                { /* block id: 695 */
                    int8_t l_1637[8][1];
                    uint64_t l_1639 = 18446744073709551611UL;
                    int32_t l_1641 = 1L;
                    int32_t l_1642 = 0x7260CF0EL;
                    int32_t l_1643 = 1L;
                    int32_t l_1648 = 0L;
                    uint8_t l_1649 = 251UL;
                    int i, j;
                    for (i = 0; i < 8; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_1637[i][j] = (-3L);
                    }
                    if (((*l_1599) ^= l_1538))
                    { /* block id: 697 */
                        uint16_t **l_1607[7] = {&g_121,&g_121,&g_121,&g_121,&g_121,&g_121,&g_121};
                        uint16_t ***l_1608 = (void*)0;
                        int32_t l_1619[9];
                        uint8_t *l_1630 = &g_384;
                        uint8_t *l_1633 = &g_117;
                        int64_t **l_1636 = &l_1403[5];
                        int8_t l_1640 = (-1L);
                        int i;
                        for (i = 0; i < 9; i++)
                            l_1619[i] = 0x04710825L;
                        (*g_76) = (*g_76);
                        l_1640 &= (((*g_600) == (l_1609 = l_1607[0])) > ((safe_lshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u(((((0x108CL == (((safe_mod_func_int32_t_s_s((safe_mul_func_int16_t_s_s((~l_1619[2]), (safe_add_func_int8_t_s_s(((~0xBCAC2025L) || ((g_237 = ((*l_1599) |= ((*l_1598) = (safe_mod_func_int32_t_s_s((safe_unary_minus_func_uint8_t_u((safe_sub_func_uint16_t_u_u((((safe_mul_func_int16_t_s_s((((*l_1633) = (++(*l_1630))) > (l_1416[2] = (safe_add_func_int32_t_s_s((0x4C0D1116L <= (*p_32)), 8UL)))), (l_1636 == l_1636))) <= l_1637[0][0]) != l_1405[0][3]), 1L)))), g_1638))))) != 0xDADA5BB8L)), l_1406)))), (*p_32))) | g_1169) < (-1L))) && 0x2EF7B1107B357C44LL) , l_1639) != 0UL), l_1410[0])), 0)) || 0x69E27DCAL));
                        l_1644--;
                        l_1649++;
                    }
                    else
                    { /* block id: 709 */
                        uint64_t l_1653[6] = {0x8AD24DAB2EFBC964LL,0x399B09AC22E3E958LL,0x8AD24DAB2EFBC964LL,0x8AD24DAB2EFBC964LL,0x399B09AC22E3E958LL,0x8AD24DAB2EFBC964LL};
                        int i;
                        l_1642 |= 0L;
                        if (l_1652[2][2][2])
                            continue;
                        --l_1653[2];
                        return (*g_121);
                    }
                }
            }
            if ((5L >= (safe_lshift_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_s(((safe_rshift_func_int8_t_s_s((((l_1510[2] , (0xB2C8L | ((&l_1366 == ((safe_rshift_func_int16_t_s_u((safe_add_func_int8_t_s_s(3L, (((*p_32) < ((~((void*)0 == &p_31)) , (safe_div_func_int8_t_s_s((safe_add_func_int32_t_s_s(((*l_1673) &= (*p_32)), l_1416[3])), l_1427)))) > 0x3501B543D8AB5E69LL))), 10)) , (void*)0)) , 0UL))) && l_1377) || l_1427), 4)) >= g_1674), g_18)) && (**g_358)), l_1580))))
            { /* block id: 718 */
                (*g_76) = &l_1416[6];
            }
            else
            { /* block id: 720 */
                uint32_t l_1679[4];
                uint32_t ***l_1680 = (void*)0;
                int i;
                for (i = 0; i < 4; i++)
                    l_1679[i] = 0x2DF4E566L;
                for (g_412 = 0; (g_412 > 44); g_412 = safe_add_func_int32_t_s_s(g_412, 9))
                { /* block id: 723 */
                    int64_t l_1681 = (-2L);
                    int32_t *l_1682 = &g_106;
                    for (l_1644 = 0; l_1644 < 8; l_1644 += 1)
                    {
                        l_1416[l_1644] = 0L;
                    }
                    if ((*p_31))
                        break;
                    (*l_1682) ^= (safe_add_func_int16_t_s_s(((((*l_1383) = (*l_1673)) , ((*g_121) = l_1679[1])) & (((((void*)0 != (*g_1392)) , l_1652[0][3][1]) , &l_1453) != l_1680)), l_1681));
                    (*g_1318) = p_32;
                }
            }
        }
        else
        { /* block id: 732 */
            int16_t l_1695 = 0xEFA9L;
            int32_t *l_1702 = &l_1406;
            (*l_1702) = ((safe_rshift_func_int8_t_s_s((safe_div_func_int32_t_s_s((safe_lshift_func_int16_t_s_s((255UL && ((safe_mul_func_uint16_t_u_u((((safe_rshift_func_uint16_t_u_u((l_1406 , l_1644), ((safe_lshift_func_int8_t_s_s(7L, 3)) & l_1695))) > ((*g_1302)--)) | (*g_554)), ((-2L) < (((l_1652[2][2][2] == (safe_sub_func_int8_t_s_s((safe_div_func_uint8_t_u_u(l_1416[6], 1L)), l_1695))) <= (-1L)) < 0x6FL)))) | l_1405[1][2])), 4)), (*p_31))), 7)) == g_51);
            g_236 &= ((*l_1702) = ((l_1652[1][5][2] < 4294967291UL) & 0UL));
            (*g_76) = &l_1406;
            if (g_106)
                goto lbl_1711;
        }
    }
    l_1714--;
    (*l_1712) = (safe_div_func_uint64_t_u_u(((&g_1393[1] != &g_1393[3]) && (safe_lshift_func_uint8_t_u_u(0xC6L, 7))), ((safe_mul_func_int16_t_s_s((*g_554), (((void*)0 == &l_1366) ^ ((*p_32) , (*l_1712))))) & g_226[1])));
    return (***g_600);
}


/* ------------------------------------------ */
/* 
 * reads : g_241 g_21 g_786 g_236 g_554 g_239 g_51 g_1300 g_600 g_601 g_121 g_359 g_102 g_1318 g_85 g_1030 g_179 g_110 g_76 g_77 g_1568 g_8 g_913 g_106
 * writes: g_241 g_786 g_236 g_122 g_239 g_51 g_108 g_179 g_412 g_77 g_1319 g_2524
 */
static int32_t * func_33(int32_t * p_34, int32_t  p_35, int8_t  p_36)
{ /* block id: 429 */
    uint64_t *l_1058 = &g_412;
    uint64_t **l_1057[1][2];
    uint16_t *** const *l_1064 = &g_594;
    int32_t l_1065 = 9L;
    int16_t l_1110 = 0L;
    int32_t l_1136[4][3][1] = {{{0x992687E4L},{0x05E2D945L},{0x05E2D945L}},{{0x992687E4L},{0x05E2D945L},{0x05E2D945L}},{{0x992687E4L},{0x05E2D945L},{0x05E2D945L}},{{0x992687E4L},{0x05E2D945L},{0x05E2D945L}}};
    uint16_t l_1157 = 1UL;
    uint8_t l_1158 = 0xC6L;
    int32_t *l_1175[4][1][10] = {{{&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106,&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106}},{{&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106,&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106}},{{&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106,&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106}},{{&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106,&l_1065,&l_1136[1][2][0],&l_1136[1][2][0],&l_1065,&g_106}}};
    int16_t **l_1187 = (void*)0;
    const uint32_t *l_1234 = &g_102[3];
    const uint32_t **l_1233[7][6] = {{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234},{&l_1234,&l_1234,&l_1234,&l_1234,&l_1234,&l_1234}};
    const int32_t **l_1321 = &g_1319;
    uint32_t l_1330 = 0xD870F5E7L;
    int32_t *l_1345 = &g_236;
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
            l_1057[i][j] = &l_1058;
    }
    for (g_241 = 0; (g_241 <= 1); g_241 += 1)
    { /* block id: 432 */
        uint32_t *l_1059 = &g_786;
        int32_t l_1066[3][5][6] = {{{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L}},{{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L}},{{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L},{(-2L),(-2L),1L,(-2L),(-2L),1L}}};
        uint16_t *l_1068 = (void*)0;
        int64_t *l_1069[10][2][2] = {{{&g_785,&g_1030},{&g_1030,&g_785}},{{&g_1030,&g_1030},{&g_411,&g_785}},{{&g_411,&g_1030},{&g_1030,&g_785}},{{&g_1030,&g_1030},{&g_785,(void*)0}},{{&g_178,&g_178},{&g_178,&g_411}},{{&g_785,&g_178},{&g_785,&g_178}},{{(void*)0,&g_178},{&g_785,&g_178}},{{&g_785,&g_411},{&g_178,&g_178}},{{&g_178,(void*)0},{&g_785,&g_1030}},{{&g_1030,&g_785},{&g_1030,&g_1030}}};
        int32_t l_1113 = (-1L);
        int32_t l_1270 = 0xD1733E4CL;
        int64_t l_1272 = 0xE0DB266DCBA9ED7FLL;
        int8_t l_1277[9];
        int i, j, k;
        for (i = 0; i < 9; i++)
            l_1277[i] = (-6L);
    }
    if ((*p_34))
    { /* block id: 581 */
        uint32_t l_1304 = 18446744073709551613UL;
        uint64_t * const *l_1306 = &g_1302;
        uint64_t * const **l_1305 = &l_1306;
        uint64_t * const **l_1308[9];
        int32_t l_1309 = 0x98C3A6E0L;
        const int32_t **l_1322[9][1] = {{&g_1319},{&g_1319},{&g_1319},{&g_1319},{&g_1319},{&g_1319},{&g_1319},{&g_1319},{&g_1319}};
        uint32_t l_1325 = 4UL;
        uint8_t *l_1342 = &g_179[3];
        int i, j;
        for (i = 0; i < 9; i++)
            l_1308[i] = &l_1306;
        for (g_786 = 0; (g_786 > 16); ++g_786)
        { /* block id: 584 */
            uint64_t l_1288 = 7UL;
            uint64_t ***l_1303 = &g_1301[1];
            int64_t l_1334[8];
            int i;
            for (i = 0; i < 8; i++)
                l_1334[i] = 0x85ACEB91506E949CLL;
            for (g_236 = (-23); (g_236 < 19); g_236 = safe_add_func_uint32_t_u_u(g_236, 4))
            { /* block id: 587 */
                uint8_t l_1289 = 0x1BL;
                uint8_t *l_1299[5];
                uint8_t **l_1298 = &l_1299[1];
                uint64_t * const ***l_1307[1][2];
                int32_t l_1311 = (-1L);
                const int32_t ***l_1320[8] = {&g_1318,&g_1318,&g_1318,&g_1318,&g_1318,&g_1318,&g_1318,&g_1318};
                const uint8_t l_1333 = 0UL;
                int i, j;
                for (i = 0; i < 5; i++)
                    l_1299[i] = &g_117;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_1307[i][j] = &l_1305;
                }
                l_1289 = (l_1288 = 0xDBE12073L);
                l_1311 ^= (safe_add_func_int8_t_s_s(((safe_mod_func_int32_t_s_s(((*g_554) & (safe_add_func_uint32_t_u_u(((safe_lshift_func_uint16_t_u_u((((((((l_1309 ^= (((void*)0 != l_1298) , ((0xD72461FFL != ((l_1303 = g_1300) == (l_1308[0] = (l_1304 , l_1305)))) & 9L))) != (((***g_600) = (!(p_36 ^ l_1304))) < l_1288)) , 0x54L) <= 7L) || 4294967295UL) || p_36) , l_1309), 1)) , p_36), (*g_359)))), (*p_34))) , l_1289), 2UL));
                l_1311 ^= (safe_sub_func_int16_t_s_s(((*g_554) = ((3UL <= ((safe_mul_func_uint8_t_u_u(0xC6L, (&g_77 != (l_1322[1][0] = (l_1321 = g_1318))))) | ((*p_34) != (safe_mod_func_int8_t_s_s(l_1325, g_85))))) && ((safe_add_func_int16_t_s_s(((safe_add_func_uint8_t_u_u(((l_1330 == (((safe_mul_func_uint16_t_u_u((p_36 , p_36), (*g_554))) , &g_599) != &l_1057[0][0])) && 0L), g_236)) || p_36), 0x9648L)) >= 1UL))), 0xC2D3L));
                if (l_1333)
                    continue;
            }
            g_108 = l_1288;
            if (l_1334[7])
                continue;
        }
        l_1309 &= (g_1030 || ((safe_mul_func_int8_t_s_s(p_36, ((((0x29L >= 0x54L) >= (safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint8_t_u_u(((*l_1342) ^= (!(*g_359))), 0)), (251UL & (0x09L != (safe_mul_func_int8_t_s_s((p_35 == ((*l_1058) = p_35)), g_110))))))) & p_35) | p_36))) & p_36));
        l_1309 ^= (*p_34);
        (*g_1318) = ((*g_76) = p_34);
    }
    else
    { /* block id: 610 */
        return l_1345;
    }
    (*l_1345) ^= 0L;
    return (*g_76);
}


/* ------------------------------------------ */
/* 
 * reads : g_76 g_359 g_102
 * writes: g_77
 */
static uint32_t  func_37(uint32_t  p_38, uint64_t  p_39)
{ /* block id: 425 */
    int32_t *l_1043 = (void*)0;
    (*g_76) = l_1043;
    (*g_76) = l_1043;
    return (*g_359);
}


/* ------------------------------------------ */
/* 
 * reads : g_106 g_102 g_226 g_241 g_358 g_359 g_117 g_21 g_62 g_240 g_667 g_51 g_87 g_237 g_76 g_239 g_600 g_601 g_121 g_122 g_529 g_384 g_85 g_236 g_8 g_554 g_238 g_786 g_108 g_179 g_599 g_411 g_235 g_110 g_18 g_918 g_412 g_755 g_991 g_1005 g_785 g_77 g_1032 g_178
 * writes: g_51 g_106 g_110 g_87 g_241 g_62 g_240 g_667 g_235 g_77 g_122 g_384 g_239 g_102 g_178 g_237 g_554 g_754 g_238 g_786 g_108 g_411 g_918 g_1032
 */
static int8_t  func_40(int32_t * p_41)
{ /* block id: 247 */
    int8_t l_608[4][2];
    int32_t l_609 = 0xF8EEAB73L;
    int32_t l_639 = 0L;
    int32_t l_657 = 0x585978C7L;
    int32_t l_660 = 2L;
    int32_t l_662[9][10] = {{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L},{(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L,(-1L),0x38C349F5L}};
    const uint16_t **l_681 = (void*)0;
    uint32_t * const l_743[6] = {&g_102[3],&g_102[3],&g_102[3],&g_102[3],&g_102[3],&g_102[3]};
    int8_t *l_798 = (void*)0;
    int16_t l_820 = 0x6194L;
    int32_t l_837[4];
    uint64_t l_856 = 0x86767FDD2E6EECF2LL;
    uint8_t l_857 = 0xDAL;
    int16_t l_892 = 0xBDB9L;
    int64_t l_989[7] = {1L,1L,1L,1L,1L,1L,1L};
    int16_t l_1008[5];
    uint32_t **l_1018 = (void*)0;
    uint32_t ***l_1017 = &l_1018;
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 2; j++)
            l_608[i][j] = (-1L);
    }
    for (i = 0; i < 4; i++)
        l_837[i] = 0x5EC06F56L;
    for (i = 0; i < 5; i++)
        l_1008[i] = 1L;
lbl_802:
    l_609 = l_608[2][0];
    for (g_51 = 0; (g_51 <= 5); g_51 += 1)
    { /* block id: 251 */
        int16_t l_637 = 0x27BCL;
        int32_t l_640 = 2L;
        int32_t l_658 = 3L;
        int32_t l_661 = 1L;
        int32_t l_663[6];
        uint16_t l_702[7][4][8] = {{{0x4FA3L,0xD994L,7UL,0xA995L,5UL,5UL,0xA995L,7UL},{65526UL,65526UL,0x29C5L,0UL,5UL,0x8BC8L,0x4B5AL,65527UL},{0x4FA3L,6UL,0UL,7UL,0x4FA3L,0xFED9L,0x51D0L,65527UL},{6UL,5UL,0x4B5AL,0UL,65530UL,0UL,0xD476L,7UL}},{{0xD994L,0xFED9L,0x4B5AL,0xA995L,65526UL,0x4FA3L,0x51D0L,0xA995L},{65530UL,65533UL,0UL,65527UL,65535UL,0x4FA3L,0x4B5AL,0x4B5AL},{0UL,0xFED9L,0x29C5L,0x29C5L,0xFED9L,0UL,0xA995L,0x51D0L},{0UL,5UL,7UL,65532UL,65535UL,0xFED9L,7UL,0xD476L}},{{65530UL,6UL,0xB9F7L,65532UL,65526UL,0x8BC8L,65527UL,0x51D0L},{0xD994L,65526UL,0x51D0L,0x29C5L,65530UL,5UL,65527UL,0x4B5AL},{6UL,0xD994L,0xB9F7L,65527UL,0x4FA3L,0xD994L,7UL,0xA995L},{0x4FA3L,0xD994L,1UL,0xD476L,0x4FA3L,0x4FA3L,0xD476L,1UL}},{{0x8BC8L,0x8BC8L,0x51D0L,65532UL,0x4FA3L,6UL,0UL,7UL},{0UL,65533UL,65532UL,1UL,0UL,0xD994L,65527UL,7UL},{65533UL,0x4FA3L,0UL,65532UL,0xFED9L,65530UL,0xB9F7L,1UL},{65526UL,0xD994L,0UL,0xD476L,0x8BC8L,0UL,65527UL,0xD476L}},{{0xFED9L,65535UL,65532UL,7UL,5UL,0UL,0UL,0UL},{65530UL,0xD994L,0x51D0L,0x51D0L,0xD994L,65530UL,0xD476L,65527UL},{65530UL,0x4FA3L,1UL,0x29C5L,5UL,0xD994L,1UL,0xB9F7L},{0xFED9L,65533UL,0x4B5AL,0x29C5L,0x8BC8L,6UL,7UL,65527UL}},{{65526UL,0x8BC8L,65527UL,0x51D0L,0xFED9L,0x4FA3L,7UL,0UL},{65533UL,65526UL,0x4B5AL,7UL,0UL,65526UL,1UL,0xD476L},{0UL,65526UL,1UL,0xD476L,0x4FA3L,0x4FA3L,0xD476L,1UL},{0x8BC8L,0x8BC8L,0x51D0L,65532UL,0x4FA3L,6UL,0UL,7UL}},{{0UL,65533UL,65532UL,1UL,0UL,0xD994L,65527UL,7UL},{65533UL,0x4FA3L,0UL,65532UL,0xFED9L,65530UL,0xB9F7L,1UL},{65526UL,0xD994L,0UL,0xD476L,0x8BC8L,0UL,65527UL,0xD476L},{0xFED9L,65535UL,65532UL,7UL,5UL,0UL,0UL,0UL}}};
        uint32_t *l_706 = (void*)0;
        uint32_t **l_705 = &l_706;
        int8_t *l_797 = &l_608[2][0];
        int32_t *l_822 = &l_609;
        int8_t l_912 = 0xCEL;
        uint8_t *l_965 = &g_117;
        int32_t l_966[7] = {0xC064BCF5L,0xC064BCF5L,(-3L),0xC064BCF5L,0xC064BCF5L,(-3L),0xC064BCF5L};
        uint8_t l_1021[9];
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_663[i] = 0x03385127L;
        for (i = 0; i < 9; i++)
            l_1021[i] = 0xEBL;
        for (g_106 = 0; (g_106 <= 1); g_106 += 1)
        { /* block id: 254 */
            uint32_t l_622 = 4294967286UL;
            int32_t l_656 = 0x756E6480L;
            int32_t l_659 = 1L;
            int32_t l_664 = 0xC01F14E6L;
            int32_t l_665 = 0x65BDC0FDL;
            int32_t l_666 = 0x12FCC374L;
            uint32_t l_670 = 0xBEA22D9CL;
            int8_t l_701 = 1L;
            int i;
            for (l_609 = 5; (l_609 >= 1); l_609 -= 1)
            { /* block id: 257 */
                int8_t *l_612 = &g_110;
                uint64_t *l_620 = (void*)0;
                uint64_t *l_621 = &g_87;
                int32_t l_623 = 0xF4A5F867L;
                uint64_t *l_624 = &g_241;
                uint8_t l_635 = 0x5CL;
                int8_t *l_636 = &g_62;
                int32_t *l_638 = &g_240;
                int32_t *l_641 = &g_240;
                int32_t *l_642 = &g_236;
                int32_t *l_643 = &g_236;
                int32_t *l_644 = &l_639;
                int32_t *l_645 = &g_108;
                int32_t *l_646 = &l_623;
                int32_t *l_647 = &l_639;
                int32_t *l_648 = &g_237;
                int32_t *l_649 = &g_238;
                int32_t *l_650 = (void*)0;
                int32_t *l_651 = &l_640;
                int32_t *l_652 = &g_240;
                int32_t *l_653 = &g_238;
                int32_t *l_654 = &g_237;
                int32_t *l_655[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int i, j;
                (*l_638) |= ((g_102[(g_106 + 3)] == ((*l_636) |= ((safe_mod_func_uint64_t_u_u(((g_226[(g_106 + 1)] | l_608[(g_106 + 2)][g_106]) | (((*l_612) = l_608[2][0]) || (safe_add_func_uint16_t_u_u((((safe_div_func_int32_t_s_s((((safe_div_func_int8_t_s_s((safe_unary_minus_func_uint8_t_u(((((((*l_621) = 18446744073709551615UL) ^ ((*l_624) ^= (l_623 = (g_226[(g_106 + 1)] != l_622)))) , (**g_358)) != (safe_sub_func_uint64_t_u_u((!((((safe_lshift_func_uint16_t_u_u((safe_mod_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u(((safe_unary_minus_func_int64_t_s(g_117)) != l_609), 1UL)) , l_608[(g_106 + 1)][g_106]), g_102[(g_106 + 3)])), 11)) <= 1UL) || 0x2FL) <= g_21[0][3][1])), l_622))) , 255UL))), 3UL)) , l_635) , g_226[(g_106 + 1)]), l_622)) , (void*)0) != (void*)0), l_608[3][0])))), g_226[(g_106 + 1)])) , (-1L)))) && l_637);
                if ((*l_638))
                    continue;
                g_667++;
                if (l_670)
                    break;
            }
            if (g_102[g_51])
                continue;
            for (l_637 = 1; (l_637 >= 0); l_637 -= 1)
            { /* block id: 271 */
                uint16_t l_677 = 0x33FFL;
                uint32_t *l_678 = (void*)0;
                uint64_t *l_700 = (void*)0;
                int64_t l_746 = 0L;
                for (g_235 = 0; (g_235 <= 5); g_235 += 1)
                { /* block id: 274 */
                    uint32_t l_679 = 1UL;
                    int32_t *l_680 = &l_660;
                    uint8_t *l_726 = &g_384;
                    int16_t *l_731 = &g_239;
                    int i;
                    if ((((*l_680) = (l_662[0][3] = (g_87 , ((safe_sub_func_int64_t_s_s((-1L), 6L)) | (safe_rshift_func_int16_t_s_u((l_677 & ((((((0L == 1UL) & (l_678 != p_41)) & (l_679 >= l_656)) | l_665) > (**g_358)) > g_102[g_51])), 6)))))) ^ l_661))
                    { /* block id: 277 */
                        const uint16_t ***l_682 = &l_681;
                        int32_t l_691 = (-2L);
                        (*l_680) |= l_677;
                        (*g_76) = ((((*l_682) = l_681) == (void*)0) , (((safe_div_func_int8_t_s_s(0x68L, (safe_sub_func_int32_t_s_s((safe_sub_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_u(l_608[2][0], 11)), l_691)), (safe_mul_func_int16_t_s_s((-10L), (0xC4C83DA6L || l_657))))))) , (safe_lshift_func_int8_t_s_s(0x54L, g_237))) , (void*)0));
                        l_656 = (3UL && (((((safe_add_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u((((void*)0 != l_700) == l_662[8][0]), ((*l_680) || g_239))), (l_701 | l_691))) && 0x5F2B9AD6L) , l_691) < l_702[3][2][1]) >= l_677));
                    }
                    else
                    { /* block id: 282 */
                        uint32_t ***l_707 = &l_705;
                        uint32_t **l_709 = (void*)0;
                        uint32_t ***l_708 = &l_709;
                        if (l_666)
                            break;
                        l_664 &= (p_41 == (void*)0);
                        (*l_708) = ((*l_707) = (((***g_600) || (++(***g_600))) , l_705));
                    }
                    l_640 |= ((safe_sub_func_int32_t_s_s((+4294967289UL), ((g_102[g_51] = ((safe_sub_func_uint32_t_u_u((safe_lshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_u((l_659 || (l_677 != (safe_mod_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((g_529 , (l_677 || (safe_rshift_func_uint16_t_u_u(((!(((*l_731) ^= (((g_87 || ((*l_726) &= g_667)) < (safe_add_func_int64_t_s_s(g_85, (safe_mod_func_uint64_t_u_u(18446744073709551615UL, l_702[3][2][1]))))) ^ 0xDCBB73B891639FA7LL)) < 0L)) & (*l_680)), l_677)))), 5)), l_677)))), 12)), l_662[1][7])), l_639)) || (**g_358))) == l_657))) > l_608[2][0]);
                }
                for (l_657 = 0; (l_657 <= 1); l_657 += 1)
                { /* block id: 296 */
                    uint8_t l_740[3][7] = {{0UL,0x72L,0x71L,0x72L,0UL,0UL,0x72L},{255UL,0x60L,255UL,0x72L,0x72L,255UL,0x60L},{0x72L,0x60L,0x71L,0x71L,0x60L,0x72L,0x60L}};
                    int64_t *l_744 = &g_178;
                    int32_t *l_745 = (void*)0;
                    int i, j;
                    l_609 |= (safe_sub_func_int64_t_s_s((g_236 , (g_8[1][2] | ((safe_div_func_uint16_t_u_u(l_662[1][8], (safe_lshift_func_uint16_t_u_u(((((((safe_add_func_uint8_t_u_u((((l_740[1][6] < (safe_add_func_uint8_t_u_u(((*g_358) != l_743[3]), (0x7A5E7E34BBF4C27ELL && (((*l_744) = 0x208D6E23A6A89854LL) | l_639))))) ^ 1UL) , 1UL), 0xC5L)) || l_656) < g_102[g_51]) && (*g_554)) >= 0L) ^ l_677), (*g_121))))) && 9L))), 0x92D0285222EF674CLL));
                }
                if (l_746)
                    continue;
            }
        }
        for (g_237 = 1; (g_237 >= 0); g_237 -= 1)
        { /* block id: 305 */
            int32_t l_751 = (-2L);
            int i, j;
            l_751 &= (((void*)0 != &l_608[g_237][g_237]) < (safe_lshift_func_uint8_t_u_u(((((((*g_359) ^= 4294967295UL) == ((g_554 = &g_239) == &g_51)) | l_662[1][8]) | ((l_702[3][2][1] , (((((safe_mul_func_int8_t_s_s(l_662[4][0], l_658)) == l_608[2][0]) != 251UL) > l_662[1][8]) >= l_609)) == l_658)) | l_661), l_662[1][8])));
        }
        if ((safe_rshift_func_uint16_t_u_s(((*g_121) = (g_359 == (g_754[0][6] = p_41))), 10)))
        { /* block id: 312 */
            uint8_t l_757[8][5][2] = {{{0UL,5UL},{0UL,0x37L},{0x37L,5UL},{0x37L,0x37L},{0UL,5UL}},{{0UL,0x37L},{0x37L,5UL},{0x37L,0x37L},{0UL,5UL},{0UL,0x37L}},{{0x37L,5UL},{0x37L,0x37L},{0UL,5UL},{0UL,0x37L},{0x37L,5UL}},{{0x37L,0x37L},{0UL,5UL},{0UL,0x37L},{0x37L,5UL},{0x37L,0x37L}},{{0UL,5UL},{0UL,0x37L},{0x37L,5UL},{0x37L,0x37L},{0UL,5UL}},{{0UL,0x37L},{0x37L,5UL},{0x37L,0x37L},{0UL,5UL},{0UL,0x37L}},{{0x37L,5UL},{0x37L,0x37L},{0UL,5UL},{0UL,0x37L},{0x37L,5UL}},{{0x37L,0x37L},{0UL,5UL},{0UL,0x37L},{0x37L,5UL},{0x37L,0x37L}}};
            int32_t l_784[9][5] = {{0L,0x86955E5BL,(-7L),(-1L),(-7L)},{(-1L),(-1L),0xBFFAA09FL,0x18F13946L,1L},{0xD666DD0EL,0x73ED2B4FL,0xBFFAA09FL,0L,0L},{0x46D125DBL,0xBFFAA09FL,(-7L),0xBFFAA09FL,0x46D125DBL},{0x18F13946L,0x73ED2B4FL,1L,0x46D125DBL,(-1L)},{0x18F13946L,(-1L),0L,9L,9L},{0x46D125DBL,0x86955E5BL,0x46D125DBL,0x73ED2B4FL,(-1L)},{0xD666DD0EL,9L,(-1L),0x73ED2B4FL,0x46D125DBL},{(-1L),0L,9L,9L,0L}};
            int32_t *l_801[9][6][4] = {{{&g_108,&l_661,&l_784[2][3],&g_240},{&g_21[2][3][1],&g_8[2][6],&l_660,&g_236},{&l_784[2][3],&l_663[1],&g_238,&g_106},{&l_784[2][3],&l_639,&l_660,&g_236},{&g_21[2][3][1],&g_106,&l_784[2][3],&g_8[2][6]},{&g_108,&l_784[2][3],&l_661,&l_639}},{{&l_662[1][8],&l_784[2][3],&l_662[1][8],&g_8[2][6]},{&g_8[2][5],&g_106,(void*)0,&g_236},{&l_657,&l_639,&g_108,&g_106},{(void*)0,&l_663[1],&g_108,&g_236},{&l_657,&g_8[2][6],(void*)0,&g_240},{&g_8[2][5],&l_661,&l_662[1][8],&g_240}},{{&l_662[1][8],&g_240,&l_661,&g_240},{&g_108,&l_661,&l_784[2][3],&g_240},{&g_21[2][3][1],&g_8[2][6],&l_660,&g_236},{&l_784[2][3],&l_663[1],&g_238,&g_106},{&l_784[2][3],&l_639,&l_660,&g_236},{&g_21[2][3][1],&g_106,&l_784[2][3],&g_8[2][6]}},{{&g_108,&l_784[2][3],&l_661,&l_639},{&l_662[1][8],&l_784[2][3],&l_662[1][8],&g_8[2][6]},{&g_8[2][5],&g_106,(void*)0,&g_236},{&l_657,&l_639,&g_108,&g_106},{&l_784[2][3],&g_236,(void*)0,&l_639},{&l_640,&l_661,&l_661,&g_106}},{{&l_657,&l_640,&g_238,&l_784[2][3]},{&g_238,&l_784[2][3],&g_21[2][3][1],&l_784[2][3]},{(void*)0,&l_640,&l_660,&g_106},{&l_662[1][8],&l_661,&g_8[2][5],&l_639},{&l_660,&g_236,&g_108,&l_663[1]},{&l_660,&g_240,&g_8[2][5],&g_8[2][6]}},{{&l_662[1][8],&l_663[1],&l_660,&l_661},{(void*)0,&g_236,&g_21[2][3][1],&g_240},{&g_238,&g_236,&g_238,&l_661},{&l_657,&l_663[1],&l_661,&g_8[2][6]},{&l_640,&g_240,(void*)0,&l_663[1]},{&l_784[2][3],&g_236,(void*)0,&l_639}},{{&l_640,&l_661,&l_661,&g_106},{&l_657,&l_640,&g_238,&l_784[2][3]},{&g_238,&l_784[2][3],&g_21[2][3][1],&l_784[2][3]},{(void*)0,&l_640,&l_660,&g_106},{&l_662[1][8],&l_661,&g_8[2][5],&l_639},{&l_660,&g_236,&g_108,&l_663[1]}},{{&l_660,&g_240,&g_8[2][5],&g_8[2][6]},{&l_662[1][8],&l_663[1],&l_660,&l_661},{(void*)0,&g_236,&g_21[2][3][1],&g_240},{&g_238,&g_236,&g_238,&l_661},{&l_657,&l_663[1],&l_661,&g_8[2][6]},{&l_640,&g_240,(void*)0,&l_663[1]}},{{&l_784[2][3],&g_236,(void*)0,&l_639},{&l_640,&l_661,&l_661,&g_106},{&l_657,&l_640,&g_238,&l_784[2][3]},{&g_238,&l_784[2][3],&g_21[2][3][1],&l_784[2][3]},{(void*)0,&l_640,&l_660,&g_106},{&l_662[1][8],&l_661,&g_8[2][5],&l_639}}};
            uint16_t ** const *l_812 = &g_601[0];
            uint16_t ** const **l_811 = &l_812;
            uint32_t l_937[8] = {0x8ABE553DL,0x8ABE553DL,0x8ABE553DL,0x8ABE553DL,0x8ABE553DL,0x8ABE553DL,0x8ABE553DL,0x8ABE553DL};
            int16_t *l_969 = &l_820;
            int16_t *l_971 = &l_637;
            int i, j, k;
            if (l_702[2][0][7])
            { /* block id: 313 */
                int32_t *l_756 = &g_238;
                (*g_76) = p_41;
                l_757[3][4][0] = ((*l_756) = 0L);
                (*l_756) |= 0L;
            }
            else
            { /* block id: 318 */
                uint32_t l_758[7];
                int32_t l_759 = 0L;
                int32_t *l_760 = &l_759;
                int32_t *l_761 = &l_663[5];
                int32_t *l_762 = (void*)0;
                int32_t *l_763 = &l_663[3];
                int32_t *l_764 = &l_663[1];
                int32_t *l_765 = &l_657;
                int32_t *l_766 = &l_663[3];
                int32_t *l_767 = (void*)0;
                int32_t *l_768 = (void*)0;
                int32_t l_769 = 1L;
                int32_t *l_770 = (void*)0;
                int32_t *l_771 = &l_639;
                int32_t *l_772 = (void*)0;
                int32_t *l_773 = &l_662[3][8];
                int32_t *l_774 = &l_769;
                int32_t *l_775 = &l_657;
                int32_t *l_776 = &g_240;
                int32_t *l_777 = &g_238;
                int32_t *l_778 = &l_662[0][2];
                int32_t *l_779 = &g_236;
                int32_t *l_780 = (void*)0;
                int32_t *l_781 = &l_660;
                int32_t *l_782 = &g_237;
                int32_t *l_783[3][10][8] = {{{(void*)0,(void*)0,&g_8[0][4],(void*)0,(void*)0,&l_639,&l_661,&l_660},{&l_663[2],&l_657,&g_18,(void*)0,&l_640,&g_8[0][4],&l_759,&l_640},{&g_21[4][2][0],(void*)0,&g_21[4][2][0],&l_662[5][6],&g_106,(void*)0,(void*)0,(void*)0},{&l_661,(void*)0,&l_640,&l_640,(void*)0,&l_661,&g_21[1][2][0],&g_106},{&g_106,&l_639,&g_18,&l_769,&l_657,&l_662[1][8],(void*)0,&g_8[0][4]},{&l_640,&g_106,&g_18,&l_769,&g_21[4][2][0],&l_661,&l_640,&g_106},{&l_661,&g_21[4][2][0],&l_759,&l_640,&l_662[5][6],&g_108,&l_759,&g_8[0][4]},{&g_21[1][2][0],(void*)0,(void*)0,&l_640,&g_21[4][2][0],(void*)0,&l_661,&g_236},{&l_759,&g_21[1][2][0],&l_661,&g_8[0][4],&g_18,(void*)0,&l_662[1][8],(void*)0},{&l_769,&l_657,&l_662[3][4],(void*)0,&g_8[0][4],(void*)0,&l_662[3][4],&l_657}},{{&l_663[3],&g_236,&g_18,&g_108,&g_106,(void*)0,&g_18,&l_662[3][4]},{&l_657,&l_661,&l_663[2],&g_18,&l_663[3],&l_639,&g_18,&l_640},{(void*)0,&g_18,&g_18,(void*)0,(void*)0,(void*)0,&l_662[3][4],&l_640},{(void*)0,(void*)0,&l_662[3][4],&l_640,(void*)0,&g_21[4][2][0],&l_662[1][8],&l_769},{&l_759,(void*)0,&l_661,&l_661,&l_639,(void*)0,&l_661,&l_640},{&l_660,&l_759,(void*)0,(void*)0,&l_662[1][8],&l_661,&l_661,&l_662[1][8]},{&g_108,(void*)0,(void*)0,&g_108,&l_661,(void*)0,&l_640,&l_662[1][8]},{&l_759,&g_8[0][4],&l_640,&l_661,&l_662[1][8],&l_639,&l_657,(void*)0},{&l_661,&g_8[0][4],&l_769,&g_18,(void*)0,(void*)0,&l_660,&g_21[1][2][0]},{&g_18,(void*)0,&l_661,&l_640,&l_662[6][9],&l_661,&g_8[0][4],&g_18}},{{&l_657,&l_759,&g_106,(void*)0,&l_759,(void*)0,&g_108,&l_663[3]},{&l_662[6][9],(void*)0,&l_769,&g_236,&g_8[0][4],&g_21[4][2][0],&l_662[1][8],&g_18},{&l_640,(void*)0,(void*)0,&g_21[1][2][0],(void*)0,(void*)0,&l_640,&l_662[6][9]},{&l_759,&g_18,(void*)0,(void*)0,&l_640,&l_639,&l_639,&g_8[0][4]},{&g_236,&l_661,(void*)0,&g_21[4][2][0],&l_640,(void*)0,(void*)0,&g_21[1][2][0]},{&l_759,&g_236,(void*)0,&g_8[0][4],(void*)0,(void*)0,&l_662[1][8],&g_18},{&l_640,&l_657,&l_662[5][6],&l_661,&g_8[0][4],(void*)0,&l_662[1][8],&l_657},{&l_662[6][9],&g_21[1][2][0],&g_18,&l_660,&l_759,(void*)0,(void*)0,&l_662[1][8]},{&l_657,(void*)0,&g_21[4][2][0],&g_18,&l_662[6][9],&l_662[6][9],&g_18,&g_21[4][2][0]},{&g_18,&g_18,&l_639,&l_661,(void*)0,&l_661,&l_662[1][8],&l_640}}};
                int i, j, k;
                for (i = 0; i < 7; i++)
                    l_758[i] = 0UL;
                l_758[1] = 0xC077D0EFL;
                --g_786;
            }
            if ((g_108 &= ((safe_mul_func_uint16_t_u_u(65535UL, (safe_rshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_s(((l_639 , l_797) != (l_798 = (void*)0)), 2)), 1)))) != (((0x40648736D49BB807LL < 0x4CD6A3BBE9B1B0B0LL) > (safe_div_func_uint64_t_u_u(18446744073709551611UL, (((void*)0 == &l_702[4][2][2]) || g_786)))) > l_784[3][0]))))
            { /* block id: 324 */
                g_238 = 0xA3100D9AL;
                return l_637;
            }
            else
            { /* block id: 327 */
                int16_t l_803 = 0x8BC1L;
                const uint16_t ***l_814 = &l_681;
                const uint16_t ****l_813 = &l_814;
                uint8_t *l_818 = &g_117;
                uint8_t **l_817 = &l_818;
                int32_t *l_821 = (void*)0;
                const uint64_t *l_840 = &g_412;
                int8_t l_874 = 0x3FL;
                int64_t *l_875 = (void*)0;
                int64_t *l_876 = &g_411;
                int32_t l_909 = 0L;
                int32_t l_911 = 0x418C7CEBL;
                int32_t l_915 = 0x09A9D602L;
                int32_t l_916 = 1L;
                int32_t l_917[3][1];
                uint16_t l_935 = 0x8AFAL;
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_917[i][j] = 0xDA6ED2ABL;
                }
                if (g_384)
                    goto lbl_802;
                (*g_76) = (l_822 = (((l_803 && ((g_240 = (safe_div_func_int8_t_s_s((+l_663[3]), (safe_mul_func_int8_t_s_s(((((safe_mul_func_int16_t_s_s(((((l_811 != l_813) > 7L) == (l_608[2][0] | (((*l_817) = l_797) == &g_384))) && (+(***g_600))), 0xDB29L)) ^ g_179[2]) != 0xD351FC2CL) & l_663[3]), 255UL))))) || l_820)) | l_803) , l_821));
                g_106 = (safe_mul_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u(((((safe_mul_func_int8_t_s_s((safe_add_func_uint16_t_u_u((++(***l_812)), 0xCF59L)), g_108)) <= ((safe_lshift_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(l_837[1], l_639)), 15)) > (safe_div_func_uint64_t_u_u(((((**g_358) & 0x51570755L) , l_840) != ((l_609 = (((((((+(safe_rshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s((l_660 , ((l_837[3] == 254UL) < (*g_554))), 0x1AL)), g_108))) , &p_41) == &g_77) >= l_663[3]) && g_8[2][6]) & 18446744073709551615UL) > l_837[1])) , (void*)0)), 0x6F1219227DC3925CLL)))) > 0x3EL) < 5UL), 0xA19AL)) > 0xC91C74BCBA6B844FLL), 0x263FL));
                if ((((6L | ((safe_rshift_func_uint16_t_u_s(1UL, 11)) != ((safe_add_func_int8_t_s_s(((l_639 || (safe_mod_func_int8_t_s_s((safe_mod_func_int8_t_s_s((safe_add_func_int8_t_s_s(((void*)0 != g_599), ((((((l_856 >= l_857) > (safe_add_func_int16_t_s_s((((*l_876) |= ((safe_div_func_uint16_t_u_u((safe_mul_func_int8_t_s_s(((safe_mul_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((safe_div_func_uint64_t_u_u((safe_sub_func_uint32_t_u_u(l_608[2][0], ((safe_add_func_uint8_t_u_u(g_529, 2L)) <= 0x874735EA6B152402LL))), g_8[2][3])) , l_660), g_226[1])), g_108)) , l_874), g_21[4][2][0])), (*g_554))) > l_658)) , (*g_554)), l_609))) , l_637) , (void*)0) == (void*)0) != g_179[1]))), g_117)), g_235))) , 0xE1L), g_667)) , g_106))) , g_179[3]) , g_110))
                { /* block id: 337 */
                    int8_t l_902 = (-1L);
                    int32_t l_910 = 0x66BAC947L;
                    int32_t l_914[9] = {(-2L),8L,(-2L),8L,(-2L),8L,(-2L),8L,(-2L)};
                    int i;
                    for (l_660 = 5; (l_660 >= 1); l_660 -= 1)
                    { /* block id: 340 */
                        int32_t l_893 = 0xFF779D49L;
                        uint64_t *l_907 = &g_241;
                        int32_t l_908 = 0x375974EAL;
                        int i;
                        l_909 |= ((safe_sub_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(0x45B2C6952C1296DDLL, (((safe_lshift_func_uint8_t_u_s(((safe_sub_func_uint16_t_u_u((((!l_663[g_51]) == ((safe_sub_func_uint64_t_u_u((safe_mod_func_int8_t_s_s(g_237, ((*l_797) |= ((-1L) | ((l_893 = l_892) <= (0UL == (+(safe_rshift_func_uint8_t_u_u(g_179[3], 7))))))))), (~(safe_sub_func_uint16_t_u_u(((safe_rshift_func_uint16_t_u_s((((l_902 ^ (safe_lshift_func_uint8_t_u_s((((safe_div_func_int8_t_s_s((((*l_907) = l_857) && l_892), 0x0DL)) , (void*)0) == (void*)0), g_18))) , g_237) & g_179[0]), l_902)) || 0L), (*g_121)))))) || 0xC796L)) >= l_902), (-1L))) > l_908), 1)) && 0UL) , l_874))), l_908)) && 0x8843DC12L);
                        g_918--;
                        return g_62;
                    }
                }
                else
                { /* block id: 348 */
                    int32_t l_928 = 0xFDB1C209L;
                    uint16_t *l_936[5] = {&l_702[3][2][1],&l_702[3][2][1],&l_702[3][2][1],&l_702[3][2][1],&l_702[3][2][1]};
                    int i;
                    if ((!((safe_lshift_func_uint8_t_u_u((((safe_mod_func_uint64_t_u_u((l_937[2] = (((((((*g_359) != ((6L <= (safe_add_func_int8_t_s_s(((*l_797) = l_608[1][1]), l_928))) != l_663[3])) >= (l_640 &= (l_928 , (((g_786 , (((safe_sub_func_int32_t_s_s((l_657 < (safe_rshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_u(l_928, (***g_600))), 13))), l_637)) ^ l_935) < g_102[1])) || l_928) > l_928)))) ^ l_837[1]) , (-1L)) <= l_857) != 6UL)), g_411)) , &g_601[0]) != (void*)0), 7)) , 9L)))
                    { /* block id: 352 */
                        uint8_t l_938 = 0UL;
                        uint8_t **l_961 = (void*)0;
                        uint8_t **l_962 = (void*)0;
                        uint8_t *l_964 = &l_757[3][4][0];
                        uint8_t **l_963[10][3] = {{&l_964,&l_964,(void*)0},{&l_964,&l_964,&l_964},{&l_964,&l_964,(void*)0},{&l_964,&l_964,&l_964},{&l_964,&l_964,&l_964},{&l_964,&l_964,&l_964},{&l_964,&l_964,&l_964},{&l_964,&l_964,&l_964},{(void*)0,&l_964,&l_964},{&l_964,&l_964,&l_964}};
                        int i, j;
                        if (l_938)
                            break;
                        l_966[4] = (safe_rshift_func_int16_t_s_u(((((((safe_mul_func_int8_t_s_s((((l_928 >= ((safe_div_func_int16_t_s_s((*g_554), (safe_sub_func_uint8_t_u_u((((~(+((safe_mul_func_int16_t_s_s(((safe_add_func_uint32_t_u_u((((((l_640 = (~(safe_add_func_uint16_t_u_u((g_412 ^ (((((l_928 && (safe_lshift_func_int16_t_s_u((0x6975L != ((+(0x633FD2AAL > g_226[1])) <= ((((safe_mod_func_int16_t_s_s((((**g_358) = ((((*l_817) = &l_938) != (l_965 = &g_384)) == 0x60FBL)) != l_928), (*g_121))) <= 18446744073709551606UL) <= 0x7025579BE016CAF7LL) , l_928))), 11))) & 4294967290UL) | l_657) , l_928) == l_938)), (*g_554))))) , g_755) , l_938) || g_8[2][6]) != 0UL), 0x2772697CL)) , l_837[3]), (*g_554))) || (-1L)))) , l_938) <= l_938), g_18)))) & l_938)) <= l_938) , 0L), g_412)) || l_928) && l_892) , 0x02L) || g_235) <= l_658), 9));
                        if (l_609)
                            continue;
                        if (g_529)
                            goto lbl_802;
                    }
                    else
                    { /* block id: 361 */
                        return l_928;
                    }
                }
            }
            if (l_609)
                break;
            g_240 &= (((((*l_969) = (safe_mul_func_int16_t_s_s((&l_702[3][2][1] != &l_702[3][2][1]), (*g_554)))) & (~((*l_971) |= (l_892 , (-2L))))) > l_657) & (-1L));
        }
        else
        { /* block id: 370 */
            for (g_110 = 0; (g_110 <= 1); g_110 += 1)
            { /* block id: 373 */
                uint8_t l_972[6][5] = {{0x4DL,0x9DL,0xEDL,0UL,0x0BL},{0x4DL,0x0BL,255UL,0x6FL,0x6FL},{0UL,0UL,0UL,0x9DL,0x0BL},{252UL,0x6FL,0x0BL,0x9DL,0UL},{0x0BL,255UL,0x6FL,0x6FL,255UL},{255UL,0xE5L,0x0BL,0UL,0xEDL}};
                int i, j;
                return l_972[2][0];
            }
        }
        for (g_87 = 0; (g_87 <= 1); g_87 += 1)
        { /* block id: 379 */
            int16_t l_988 = 3L;
            int16_t l_992[7];
            int32_t **l_996 = &l_822;
            int32_t **l_997 = &g_77;
            uint8_t **l_1002[5][9][5] = {{{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,&l_965,&l_965,(void*)0,&l_965},{&l_965,&l_965,(void*)0,&l_965,(void*)0},{(void*)0,&l_965,&l_965,&l_965,&l_965},{&l_965,&l_965,&l_965,&l_965,&l_965},{(void*)0,&l_965,(void*)0,&l_965,&l_965},{&l_965,(void*)0,(void*)0,(void*)0,&l_965},{&l_965,(void*)0,&l_965,&l_965,&l_965},{&l_965,(void*)0,(void*)0,&l_965,&l_965}},{{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,(void*)0,&l_965,&l_965,(void*)0},{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,&l_965,&l_965,(void*)0,&l_965},{&l_965,(void*)0,&l_965,&l_965,&l_965},{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,&l_965,(void*)0,&l_965,&l_965},{(void*)0,(void*)0,&l_965,(void*)0,&l_965}},{{(void*)0,&l_965,(void*)0,&l_965,(void*)0},{(void*)0,&l_965,(void*)0,&l_965,&l_965},{&l_965,(void*)0,&l_965,(void*)0,&l_965},{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,(void*)0,(void*)0,&l_965,(void*)0},{&l_965,(void*)0,&l_965,&l_965,&l_965},{&l_965,(void*)0,&l_965,&l_965,&l_965},{(void*)0,&l_965,(void*)0,(void*)0,(void*)0},{(void*)0,&l_965,&l_965,&l_965,&l_965}},{{&l_965,&l_965,(void*)0,&l_965,&l_965},{&l_965,(void*)0,&l_965,&l_965,&l_965},{(void*)0,&l_965,&l_965,&l_965,(void*)0},{&l_965,(void*)0,(void*)0,(void*)0,&l_965},{&l_965,&l_965,&l_965,&l_965,&l_965},{(void*)0,&l_965,&l_965,&l_965,(void*)0},{&l_965,&l_965,(void*)0,(void*)0,&l_965},{&l_965,(void*)0,&l_965,&l_965,&l_965},{(void*)0,&l_965,&l_965,&l_965,&l_965}},{{&l_965,&l_965,&l_965,&l_965,(void*)0},{&l_965,&l_965,&l_965,&l_965,&l_965},{(void*)0,(void*)0,(void*)0,&l_965,&l_965},{(void*)0,(void*)0,(void*)0,&l_965,(void*)0},{&l_965,&l_965,&l_965,&l_965,&l_965},{&l_965,(void*)0,(void*)0,(void*)0,&l_965},{&l_965,(void*)0,(void*)0,&l_965,&l_965},{(void*)0,(void*)0,&l_965,&l_965,(void*)0},{&l_965,(void*)0,&l_965,(void*)0,&l_965}}};
            uint32_t **l_1011 = &l_706;
            uint64_t *l_1012 = (void*)0;
            uint64_t *l_1013 = (void*)0;
            uint64_t *l_1014 = &g_241;
            int32_t *l_1022 = &l_661;
            int32_t l_1025 = 0xDA2B9F84L;
            int32_t l_1026 = (-1L);
            int32_t l_1027 = 0L;
            int32_t l_1028 = 0x8297135EL;
            int32_t l_1029[8] = {0L,0x13C151B6L,0x13C151B6L,0L,0x13C151B6L,0x13C151B6L,0L,0x13C151B6L};
            int i, j, k;
            for (i = 0; i < 7; i++)
                l_992[i] = 0x033EL;
            for (g_108 = 0; (g_108 <= 1); g_108 += 1)
            { /* block id: 382 */
                uint16_t **l_973 = &g_121;
                const int32_t l_987[1][9][2] = {{{6L,6L},{6L,6L},{6L,6L},{6L,6L},{6L,6L},{6L,6L},{6L,6L},{6L,6L},{6L,6L}}};
                int i, j, k;
                for (l_857 = 0; (l_857 <= 1); l_857 += 1)
                { /* block id: 385 */
                    int32_t *l_990[4] = {&g_913,&g_913,&g_913,&g_913};
                    int i, j;
                    l_660 = (l_608[(g_108 + 1)][l_857] , ((9UL > ((void*)0 == l_973)) > (safe_mod_func_int8_t_s_s(((safe_add_func_uint32_t_u_u((safe_mod_func_int32_t_s_s((safe_mul_func_uint16_t_u_u(((((safe_mod_func_uint32_t_u_u((safe_add_func_uint16_t_u_u((+(((0x15B3L < (g_786 > (((((**g_358) == l_987[0][5][0]) | (g_529 || l_987[0][5][0])) , l_988) > 0UL))) & g_384) > (*g_554))), l_637)), l_837[0])) >= 0xB347665CL) > l_608[(g_108 + 1)][l_857]) , (*g_121)), l_987[0][5][0])), 0xCBDC186CL)), l_989[5])) != 4294967289UL), l_987[0][5][0]))));
                    for (l_658 = 1; (l_658 >= 0); l_658 -= 1)
                    { /* block id: 389 */
                        return g_991[7][4];
                    }
                    l_639 = l_992[0];
                    for (l_661 = 0; (l_661 <= 1); l_661 += 1)
                    { /* block id: 395 */
                        uint8_t l_993 = 0x81L;
                        l_993--;
                    }
                }
            }
            (*l_997) = ((l_1008[1] = (((l_996 = &g_77) == l_997) | (safe_div_func_int64_t_s_s(g_108, (safe_add_func_uint8_t_u_u((((l_663[3] = (((l_965 = (void*)0) == l_797) != ((((*g_359) != 0xB9965FDAL) ^ (safe_rshift_func_uint8_t_u_s(g_1005, (l_608[(g_87 + 2)][g_87] = (((safe_div_func_int16_t_s_s((-9L), (*g_554))) && (-1L)) , (-1L)))))) & 0xF41D58D55A0DDBA5LL))) & l_662[6][3]) >= g_785), g_18)))))) , &l_662[4][7]);
            if (((((safe_mod_func_int16_t_s_s(l_608[2][0], ((*g_77) | ((l_1011 == &l_706) != ((--(*l_1014)) & g_755))))) , l_1017) != (void*)0) , ((*l_1022) |= (0xC25AL | (((safe_sub_func_int32_t_s_s((**l_996), l_1021[2])) , l_1021[5]) , l_1008[4])))))
            { /* block id: 408 */
                int32_t *l_1023 = &g_913;
                int32_t *l_1024[6];
                int i;
                for (i = 0; i < 6; i++)
                    l_1024[i] = &g_237;
                g_1032--;
            }
            else
            { /* block id: 410 */
                uint32_t l_1035 = 0UL;
                l_1035--;
                for (g_178 = 7; (g_178 >= 19); g_178 = safe_add_func_uint64_t_u_u(g_178, 4))
                { /* block id: 414 */
                    int64_t l_1040 = 0x3FE8B1CAE15F4CFDLL;
                    (**g_76) |= 0x4B6D5D7EL;
                    l_1040 = (*g_77);
                    (*g_76) = ((**l_996) , (*g_76));
                }
                if ((**g_76))
                    continue;
            }
        }
    }
    return g_236;
}


/* ------------------------------------------ */
/* 
 * reads : g_76 g_8 g_21 g_85 g_62 g_51 g_77 g_241 g_226 g_239 g_106 g_117 g_178 g_87 g_18 g_121 g_122 g_102 g_237 g_236 g_238 g_240 g_235 g_179 g_358 g_359 g_384 g_412 g_108 g_411 g_110 g_554 g_594 g_599 g_600 g_601
 * writes: g_85 g_87 g_62 g_241 g_110 g_178 g_240 g_122 g_102 g_106 g_179 g_239 g_77 g_235 g_51 g_384 g_117 g_412 g_236 g_529 g_594 g_600 g_237
 */
static int32_t * func_42(int32_t * p_43, uint8_t  p_44, uint32_t  p_45, int32_t * p_46)
{ /* block id: 19 */
    const int32_t l_82[2][7] = {{0xE7A9F478L,0x3DE179BCL,0x3DE179BCL,0xE7A9F478L,0x3DE179BCL,0x3DE179BCL,0xE7A9F478L},{(-1L),1L,(-1L),(-1L),1L,(-1L),(-1L)}};
    uint16_t *l_84 = &g_85;
    uint64_t *l_86 = &g_87;
    int32_t l_256 = (-1L);
    uint32_t *l_271 = &g_102[3];
    uint32_t l_272 = 0x0542C854L;
    int32_t l_333 = 0L;
    uint64_t l_335 = 0UL;
    int32_t l_347 = 0x8340461BL;
    int32_t **l_370 = &g_77;
    int32_t l_410 = 0x7799A8B8L;
    uint32_t l_451 = 0xC46DE2AFL;
    uint8_t l_452 = 0x5FL;
    uint64_t l_475 = 0UL;
    uint32_t **l_531 = &g_359;
    int32_t **l_532 = (void*)0;
    int64_t l_582[4] = {0x9DCAC31AF7C589CCLL,0x9DCAC31AF7C589CCLL,0x9DCAC31AF7C589CCLL,0x9DCAC31AF7C589CCLL};
    uint16_t ****l_595 = &g_594;
    uint16_t ****l_602 = &g_600;
    uint8_t *l_603 = &g_384;
    int32_t l_604 = 0xE0AA5D13L;
    int32_t *l_605 = (void*)0;
    int32_t *l_606 = (void*)0;
    int32_t *l_607 = &g_240;
    int i, j;
    if (((safe_mod_func_int8_t_s_s(0L, (safe_rshift_func_int16_t_s_u(0xC51CL, ((((func_68(((*l_86) = (((safe_sub_func_int8_t_s_s((((g_76 != &p_46) , (safe_rshift_func_uint8_t_u_s(254UL, ((safe_rshift_func_int8_t_s_s((l_82[1][3] >= g_8[2][6]), (~(g_21[3][7][0] && (((*l_84) ^= ((&g_51 != (void*)0) != p_45)) | (-3L)))))) > 0x248FL)))) | l_82[1][5]), l_82[1][3])) && 0xAE2A06A03D0BAECBLL) == g_62)), g_51, (*g_76), g_21[4][2][0], p_44) && g_239) < 0xD6L) >= 0L) & 0xB42E5B4E503AC011LL))))) != p_44))
    { /* block id: 88 */
        int8_t *l_252 = &g_62;
        int8_t *l_253 = &g_110;
        int32_t l_254 = 0x6B099175L;
        int32_t *l_255[8] = {&g_240,&g_240,&g_240,&g_240,&g_240,&g_240,&g_240,&g_240};
        uint32_t **l_268 = (void*)0;
        uint32_t *l_270[5][9] = {{&g_102[3],&g_102[3],&g_102[5],&g_102[3],&g_102[3],&g_102[5],&g_102[3],&g_102[3],&g_102[5]},{&g_102[0],&g_102[3],&g_102[5],&g_102[3],&g_102[0],(void*)0,&g_102[0],&g_102[3],&g_102[5]},{&g_102[3],&g_102[3],&g_102[5],&g_102[3],&g_102[3],&g_102[5],&g_102[3],&g_102[3],&g_102[5]},{&g_102[0],&g_102[3],&g_102[5],&g_102[3],&g_102[0],&g_102[3],&g_102[4],&g_102[3],&g_102[0]},{&g_102[4],&g_102[4],&g_102[3],&g_102[4],&g_102[4],&g_102[3],&g_102[4],&g_102[4],&g_102[3]}};
        uint32_t **l_269 = &l_270[2][8];
        uint8_t *l_348 = &g_179[3];
        int i, j;
        l_256 &= ((g_106 && (safe_sub_func_int16_t_s_s(((((p_45 | (g_178 &= (safe_mul_func_int8_t_s_s(((*l_253) = ((safe_rshift_func_int8_t_s_s((((void*)0 != g_77) , ((18446744073709551609UL == ((l_82[0][2] && (((*l_252) &= (p_44 < (3UL && ((safe_sub_func_int64_t_s_s(p_45, 0x702E61DC0C745326LL)) | g_117)))) && 0x65L)) == g_226[1])) , 0x8EL)), 0)) == p_45)), 0xF9L)))) | 4294967295UL) , l_86) != &g_87), l_254))) && 0xF9FCL);
        l_256 = (g_240 = ((((safe_rshift_func_int16_t_s_s((safe_lshift_func_uint16_t_u_s((((*l_252) = l_82[1][3]) <= g_241), g_87)), 3)) == (p_44 > (+(safe_rshift_func_int8_t_s_u((safe_sub_func_int16_t_s_s(p_45, (safe_mul_func_int8_t_s_s(l_82[1][4], (&p_45 != (void*)0))))), p_44))))) , (*p_46)) , (*g_77)));
        if (((p_44 , ((((0xD32F5137L != (p_44 , (l_256 = (((*l_269) = p_43) != l_271)))) ^ p_45) >= (((void*)0 == &g_51) | 0xFC11A16BL)) >= l_82[1][3])) & g_18))
        { /* block id: 98 */
            l_272++;
            if ((safe_mod_func_uint16_t_u_u((++(*g_121)), ((safe_div_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((g_178 = (safe_add_func_int32_t_s_s(0x264CA559L, (((safe_mul_func_int16_t_s_s(p_45, ((safe_div_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s(5L, 7)), g_8[0][1])) <= (p_44 < (safe_add_func_int16_t_s_s(p_45, (safe_div_func_uint8_t_u_u((~(0x31FFB44109209464LL >= (safe_sub_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s((((1UL >= (((g_102[1] ^= (((((p_45 == 0x1BL) & g_87) , l_256) & p_45) <= g_241)) , p_45) , 0x3BA48C56L)) && g_85) | 18446744073709551615UL), l_256)) , 5UL), g_51)))), 0xAAL)))))))) != 0L) >= 1UL)))), l_272)), p_44)) && (-10L)))))
            { /* block id: 103 */
                return (*g_76);
            }
            else
            { /* block id: 105 */
                return p_43;
            }
        }
        else
        { /* block id: 108 */
            int8_t l_316 = (-1L);
            int64_t l_332 = 0xE1314E473470AD2DLL;
            int32_t l_334[2][3][3] = {{{0x3CCA9ACFL,1L,1L},{0x434D6C30L,0x434D6C30L,1L},{1L,0x3CCA9ACFL,1L}},{{0x3CCA9ACFL,1L,1L},{0x434D6C30L,0x434D6C30L,1L},{1L,0x3CCA9ACFL,1L}}};
            int i, j, k;
            g_240 = ((((0xF3L && (p_45 ^ g_237)) , ((safe_mul_func_uint8_t_u_u(255UL, ((safe_mul_func_uint8_t_u_u(g_106, g_122)) && (safe_add_func_int64_t_s_s((safe_lshift_func_int8_t_s_s(((*l_252) = (safe_div_func_uint16_t_u_u(p_45, g_236))), (&l_271 != &l_271))), g_238))))) , (**g_76))) <= g_239) || l_82[0][1]);
            g_106 = (l_256 = (safe_div_func_int8_t_s_s(p_45, (safe_rshift_func_uint16_t_u_u(0xD6ADL, 14)))));
            l_256 = (l_316 || (((safe_div_func_uint8_t_u_u(((g_178 = (l_82[1][3] ^ (((safe_mod_func_int32_t_s_s(l_272, ((((p_45 , (safe_add_func_uint32_t_u_u(((safe_mul_func_uint16_t_u_u(((p_44 ^ l_82[1][3]) ^ ((safe_rshift_func_int8_t_s_s(((safe_rshift_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(((g_77 == (((safe_unary_minus_func_uint8_t_u(p_44)) & (g_102[3] & 7UL)) , &p_45)) > l_316), p_45)), 0)) ^ g_8[2][6]), 6)) ^ 1L)), l_316)) != l_82[1][3]), 0L))) | g_178) == l_82[1][3]) , (*g_77)))) , l_316) || g_102[2]))) > g_240), p_45)) == 4294967288UL) && (-5L)));
            l_335++;
        }
        g_106 = (safe_mul_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(p_45, (l_333 = (((safe_lshift_func_uint8_t_u_s(p_45, (safe_unary_minus_func_int16_t_s(0L)))) != g_235) , (l_333 == (((((((safe_lshift_func_int8_t_s_s((((*l_252) = (l_347 , 0x50L)) || l_272), 6)) || (((((*l_348)++) == (safe_lshift_func_int8_t_s_s(l_333, 0))) >= g_85) , l_272)) <= (**g_76)) < (**g_76)) ^ (**g_76)) || 0x436942E82D893DD2LL) >= (*g_121))))))), 0x9596L));
    }
    else
    { /* block id: 121 */
        uint32_t l_376[9][9][3] = {{{0x4425E713L,0UL,0x98808A8CL},{0x0F3FFA15L,1UL,0x8C2DD40FL},{0xB562EA41L,1UL,0xB562EA41L},{0x23176A2DL,0xEC8D1C3FL,1UL},{0x4425E713L,1UL,0xEDAA46B1L},{1UL,1UL,0xA39CCFACL},{0UL,0UL,0xB562EA41L},{1UL,0x8C2DD40FL,0xEC8D1C3FL},{0x4425E713L,1UL,0x98808A8CL}},{{0x23176A2DL,1UL,0xEC8D1C3FL},{0xB562EA41L,3UL,0xB562EA41L},{0x0F3FFA15L,0xEC8D1C3FL,0xA39CCFACL},{0x4425E713L,3UL,0xEDAA46B1L},{0x73403350L,1UL,1UL},{0UL,1UL,0xB562EA41L},{0x73403350L,0x8C2DD40FL,0x8C2DD40FL},{0x4425E713L,0UL,0x98808A8CL},{0x0F3FFA15L,1UL,0x8C2DD40FL}},{{0xB562EA41L,1UL,0xB562EA41L},{0x23176A2DL,0xEC8D1C3FL,1UL},{0x4425E713L,1UL,0xEDAA46B1L},{1UL,1UL,0xA39CCFACL},{0UL,0UL,0xB562EA41L},{1UL,0x8C2DD40FL,0xEC8D1C3FL},{0x4425E713L,1UL,0x98808A8CL},{0x23176A2DL,1UL,0xEC8D1C3FL},{0xB562EA41L,3UL,0xB562EA41L}},{{0x0F3FFA15L,0xEC8D1C3FL,0xA39CCFACL},{0x4425E713L,3UL,0xEDAA46B1L},{0x73403350L,1UL,1UL},{0UL,1UL,0xB562EA41L},{0x73403350L,0x8C2DD40FL,0x8C2DD40FL},{0x4425E713L,0UL,0x98808A8CL},{0x0F3FFA15L,1UL,0x8C2DD40FL},{0xB562EA41L,1UL,0xB562EA41L},{0x23176A2DL,0xEC8D1C3FL,1UL}},{{0x4425E713L,1UL,0xEDAA46B1L},{1UL,1UL,0xA39CCFACL},{0UL,0UL,0xB562EA41L},{1UL,0x8C2DD40FL,0xEC8D1C3FL},{0x4425E713L,1UL,0x98808A8CL},{0x23176A2DL,1UL,0xEC8D1C3FL},{0xB562EA41L,3UL,0xB562EA41L},{0x0F3FFA15L,0xEC8D1C3FL,0xA39CCFACL},{0x4425E713L,3UL,0xEDAA46B1L}},{{0x73403350L,1UL,1UL},{0UL,1UL,0xB562EA41L},{0x73403350L,0x8C2DD40FL,0x8C2DD40FL},{0x4425E713L,0UL,0x98808A8CL},{0x0F3FFA15L,1UL,0x8C2DD40FL},{0xB562EA41L,1UL,0xB562EA41L},{0x23176A2DL,0xEC8D1C3FL,1UL},{0x4425E713L,1UL,0xEDAA46B1L},{1UL,1UL,0xA39CCFACL}},{{0UL,0UL,0xB562EA41L},{1UL,0x8C2DD40FL,0xEC8D1C3FL},{0x4425E713L,1UL,0x98808A8CL},{0x23176A2DL,1UL,0xEC8D1C3FL},{0xB562EA41L,3UL,0xB562EA41L},{0x0F3FFA15L,0xEC8D1C3FL,0xA39CCFACL},{0x4425E713L,3UL,0xEDAA46B1L},{0x73403350L,1UL,1UL},{0UL,1UL,0xB562EA41L}},{{0x73403350L,0x8C2DD40FL,0x8C2DD40FL},{0x4425E713L,0UL,0x98808A8CL},{0x0F3FFA15L,1UL,0x8C2DD40FL},{0xB562EA41L,1UL,0xB562EA41L},{0x23176A2DL,0xEC8D1C3FL,1UL},{0x4425E713L,1UL,0xEDAA46B1L},{1UL,1UL,0xA39CCFACL},{0UL,0UL,0xB562EA41L},{1UL,0x8C2DD40FL,0xEC8D1C3FL}},{{0x4425E713L,1UL,0x98808A8CL},{0x23176A2DL,1UL,0xEC8D1C3FL},{0xB562EA41L,3UL,0xB562EA41L},{0x0F3FFA15L,0xEC8D1C3FL,0xA39CCFACL},{0x4425E713L,3UL,0xEDAA46B1L},{0x73403350L,1UL,1UL},{0UL,1UL,0xB562EA41L},{0x73403350L,0x8C2DD40FL,0x8C2DD40FL},{0x4425E713L,0UL,0x98808A8CL}}};
        int32_t l_402 = 0xAD22CCE8L;
        int32_t l_409[3];
        int32_t *l_442 = &g_240;
        uint8_t *l_468 = &g_179[3];
        uint64_t l_583 = 18446744073709551613UL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_409[i] = 0L;
lbl_513:
        for (g_122 = 0; (g_122 >= 18); g_122++)
        { /* block id: 124 */
            int32_t *l_405 = (void*)0;
            int32_t *l_406 = &l_402;
            int32_t *l_407[10][7][3] = {{{&g_236,&l_347,&g_21[1][4][1]},{(void*)0,&g_237,&g_237},{&l_256,&g_8[0][8],&g_237},{&g_8[2][0],(void*)0,&l_347},{&g_8[2][0],&g_18,&l_333},{&l_256,&g_106,&g_238},{(void*)0,&l_256,&g_21[4][2][0]}},{{&g_236,&g_106,&l_402},{&g_106,(void*)0,&g_237},{&g_18,&g_21[4][2][0],&g_18},{&l_402,(void*)0,&g_238},{&g_106,&g_106,&l_347},{&l_256,&l_256,(void*)0},{&g_106,&g_106,&g_21[4][2][0]}},{{&g_21[4][2][0],&g_18,&g_21[1][4][1]},{&l_256,(void*)0,&g_21[1][4][1]},{&l_347,&g_8[0][8],&g_21[4][2][0]},{&l_402,&g_237,(void*)0},{&g_8[2][0],&l_347,&l_347},{&g_18,&g_8[2][0],&g_238},{&l_256,&g_106,&g_18}},{{&g_240,&g_106,&g_237},{&l_256,&g_237,&l_402},{&g_18,&g_240,&g_21[4][2][0]},{&g_8[2][0],&g_106,&g_238},{&l_402,&l_347,&l_333},{&l_347,&l_256,&l_347},{&l_256,&l_256,&g_237}},{{&g_21[4][2][0],&l_347,&g_237},{&g_106,&g_106,&g_21[1][4][1]},{&l_256,&g_240,&g_18},{&g_106,&g_237,&l_347},{&l_402,&g_106,&g_21[4][2][0]},{&g_18,&g_106,&l_347},{&g_106,&g_8[2][0],&g_18}},{{&g_236,&l_347,&l_256},{(void*)0,(void*)0,&g_237},{(void*)0,&g_237,&g_18},{&g_237,(void*)0,&g_8[0][8]},{&g_237,&g_237,&g_18},{(void*)0,&g_8[1][2],&g_236},{(void*)0,&g_21[3][5][1],&g_108}},{{&g_236,&g_8[1][6],&g_8[2][6]},{(void*)0,(void*)0,&g_237},{&g_237,(void*)0,&g_240},{&g_18,(void*)0,&g_236},{&g_8[1][2],&g_8[1][6],(void*)0},{(void*)0,&g_21[3][5][1],&g_237},{(void*)0,&g_8[1][2],&g_108}},{{(void*)0,&g_237,&l_256},{(void*)0,(void*)0,&l_256},{&g_237,&g_237,&g_108},{&g_18,(void*)0,&g_237},{&g_237,&g_237,(void*)0},{&g_237,&g_237,&g_236},{(void*)0,&g_8[1][2],&g_240}},{{&g_21[0][1][1],&g_8[1][6],&g_237},{(void*)0,(void*)0,&g_8[2][6]},{&g_237,&g_21[0][1][1],&g_108},{&g_237,(void*)0,&g_236},{&g_18,&g_237,&g_18},{&g_237,&g_21[3][5][1],&g_8[0][8]},{(void*)0,&g_21[3][5][1],&g_18}},{{(void*)0,&g_237,&g_237},{(void*)0,(void*)0,&l_256},{(void*)0,&g_21[0][1][1],&g_240},{&g_8[1][2],(void*)0,&g_8[0][8]},{&g_18,&g_8[1][6],&g_21[4][2][0]},{&g_237,&g_8[1][2],&g_8[0][8]},{(void*)0,&g_237,&g_240}}};
            int i, j, k;
            for (g_241 = 0; (g_241 <= 3); g_241 += 1)
            { /* block id: 127 */
                uint16_t **l_355 = (void*)0;
                uint16_t ***l_356 = &l_355;
                int32_t l_375 = 0x4D1D02DAL;
                int16_t *l_397 = &g_239;
                (*l_356) = l_355;
                for (g_239 = 3; (g_239 >= 0); g_239 -= 1)
                { /* block id: 131 */
                    for (g_106 = 0; (g_106 <= 3); g_106 += 1)
                    { /* block id: 134 */
                        uint32_t *l_357 = &l_272;
                        int i;
                        (*g_76) = (((*l_357) ^= g_179[g_239]) , (void*)0);
                        if (g_179[g_239])
                            continue;
                    }
                }
                if ((g_358 != (void*)0))
                { /* block id: 140 */
                    int8_t *l_361 = &g_235;
                    int16_t *l_377 = (void*)0;
                    int16_t *l_378 = &g_239;
                    int32_t l_381 = (-10L);
                    int16_t *l_382 = &g_51;
                    uint64_t *l_391 = (void*)0;
                    uint64_t *l_392 = (void*)0;
                    int i;
                    if ((((-1L) > (((*l_361) = (+g_102[(g_241 + 2)])) == ((safe_sub_func_int32_t_s_s((safe_add_func_uint16_t_u_u((*g_121), (safe_add_func_uint64_t_u_u(0x88D4735693799B5ALL, ((safe_rshift_func_uint8_t_u_u((&p_46 == l_370), (((((safe_mul_func_int16_t_s_s((((*l_382) = ((safe_lshift_func_uint16_t_u_s(((*l_84) = l_375), ((*l_378) = l_376[0][5][2]))) , ((*l_378) = (l_381 &= (((safe_mul_func_uint16_t_u_u((0x755FFD0329CD6936LL > p_44), p_44)) ^ (**g_358)) >= 4294967295UL))))) || g_102[(g_241 + 2)]), g_102[(g_241 + 2)])) , 18446744073709551608UL) || g_238) <= 0x886CF4F7B4804EB2LL) & p_44))) || 1UL))))), 0L)) <= 0x2767L))) <= 0L))
                    { /* block id: 147 */
                        g_240 &= 0L;
                        return p_46;
                    }
                    else
                    { /* block id: 150 */
                        int32_t *l_383[7][4] = {{&g_237,&l_256,&g_108,&l_256},{(void*)0,&g_8[2][6],&g_236,&g_108},{&l_256,&g_8[2][6],&g_8[2][6],&l_256},{&g_8[2][6],&l_256,(void*)0,&g_237},{&g_8[2][6],(void*)0,&g_8[2][6],&g_236},{&l_256,&g_237,&g_236,&g_236},{(void*)0,(void*)0,&g_108,&g_237}};
                        int i, j;
                        ++g_384;
                    }
                    l_402 = (0xE882L | (safe_add_func_int64_t_s_s(((safe_rshift_func_int8_t_s_u(p_45, 2)) | (p_45 & ((l_391 = &l_335) == l_392))), ((safe_div_func_uint8_t_u_u(((safe_div_func_uint64_t_u_u((l_397 == &g_239), (safe_lshift_func_int8_t_s_u(5L, 1)))) && (((((((safe_mod_func_int16_t_s_s(((*l_378) = (&g_384 != (void*)0)), 0x8BCEL)) > (-8L)) >= p_44) <= (*p_46)) , l_376[2][1][0]) < 249UL) > 8L)), p_44)) && g_21[5][6][0]))));
                }
                else
                { /* block id: 156 */
                    for (g_117 = 0; (g_117 <= 5); g_117 += 1)
                    { /* block id: 159 */
                        (*g_76) = (*g_76);
                    }
                }
                for (p_44 = 1; (p_44 <= 5); p_44 += 1)
                { /* block id: 165 */
                    l_375 &= (safe_lshift_func_uint8_t_u_u(p_44, 5));
                    (*g_76) = (*l_370);
                    l_402 = 1L;
                    (*g_76) = (*g_76);
                }
            }
            --g_412;
        }
        for (l_272 = 0; (l_272 >= 10); ++l_272)
        { /* block id: 176 */
            int32_t l_423 = (-8L);
            uint64_t **l_426 = &l_86;
            uint8_t *l_429 = (void*)0;
            int32_t *l_453[3];
            int16_t *l_479 = &g_239;
            uint32_t **l_530 = &l_271;
            int i;
            for (i = 0; i < 3; i++)
                l_453[i] = &l_402;
            if ((safe_mod_func_int16_t_s_s((safe_sub_func_int64_t_s_s((safe_rshift_func_int16_t_s_u((p_46 != p_46), p_44)), ((g_117 |= (((g_51 == (l_423 , ((safe_mod_func_uint32_t_u_u(0x30BF8AFAL, 5L)) & (((*l_426) = &g_412) != ((safe_div_func_int64_t_s_s(2L, 0xD2C6C615AB4ACBCALL)) , &l_335))))) < g_21[3][3][1]) <= l_409[0])) , 0x40751C18EDC463A2LL))), (*g_121))))
            { /* block id: 179 */
                uint32_t l_441 = 0xCB8A5D56L;
                int16_t *l_449 = (void*)0;
                int16_t *l_450[9] = {&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51};
                int8_t * const l_469[6] = {&g_110,(void*)0,(void*)0,&g_110,(void*)0,(void*)0};
                uint8_t *l_474 = &g_117;
                int32_t l_503 = (-2L);
                uint64_t l_512 = 0x623CBA5BB6677C5DLL;
                int i;
                (*g_76) = p_43;
                l_453[2] = (((*g_76) != ((safe_mul_func_uint8_t_u_u(((g_51 ^= ((safe_mod_func_int32_t_s_s(((safe_div_func_int16_t_s_s((g_239 ^= ((safe_lshift_func_uint16_t_u_s(((+(((g_117 == l_441) || (((*g_121) , l_442) != (*g_76))) < (((*l_442) = (g_241 == (safe_lshift_func_int8_t_s_s((safe_add_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u(6UL, (p_45 , 0x04L))), l_441)), 1)))) != p_45))) , p_45), g_8[2][6])) || p_45)), l_451)) | 0x9AL), (*g_359))) < l_452)) & l_423), g_179[2])) , p_46)) , (void*)0);
                if (((safe_sub_func_int16_t_s_s((p_45 , (safe_lshift_func_int8_t_s_u((((safe_add_func_uint64_t_u_u(p_45, 0x66CC3914C09D5D71LL)) <= (((((safe_sub_func_uint16_t_u_u(((((safe_sub_func_uint32_t_u_u((safe_rshift_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((l_468 != &g_179[3]), ((*l_468) = ((void*)0 != l_469[4])))) != p_44), (safe_sub_func_uint8_t_u_u((g_384 ^= ((*l_474) = (safe_mul_func_uint8_t_u_u((0UL ^ 0x7A1F8DC3EBF0EA03LL), g_117)))), p_45)))), 0xD88CE5F2L)) , p_45) || p_44) , (*g_121)), l_441)) != (*g_121)) && (-1L)) == p_45) | g_237)) | g_21[2][5][0]), g_108))), (*g_121))) && l_475))
                { /* block id: 188 */
                    int32_t **l_476 = &g_77;
                    uint64_t *l_477 = (void*)0;
                    uint64_t *l_478 = &g_87;
                    int16_t *l_480[1][3];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_480[i][j] = &g_51;
                    }
                    (*l_442) = (((*l_478) |= ((**l_426) = ((void*)0 == l_476))) | ((((l_441 || (l_479 == (l_480[0][0] = l_84))) && ((*l_479) &= (((((*l_474) = ((safe_add_func_int32_t_s_s(((safe_lshift_func_int8_t_s_u(0x0AL, 2)) > (*p_46)), ((((((safe_div_func_int8_t_s_s(((((safe_add_func_uint64_t_u_u(0x36CD70112BE47FC4LL, (safe_div_func_int64_t_s_s(((safe_div_func_uint64_t_u_u((safe_mod_func_int32_t_s_s((safe_mod_func_int32_t_s_s(((safe_mod_func_int32_t_s_s(((safe_rshift_func_int8_t_s_s(((-8L) == g_8[2][6]), g_384)) && p_45), p_44)) >= 0UL), 0xFAF10594L)), 7L)), 8L)) , 1L), g_179[3])))) , (*g_121)) > (*g_121)) , p_45), 0xE1L)) , g_411) != 1L) > p_44) | 0xA9402B95A193536BLL) <= 254UL))) >= (*l_442))) && g_226[1]) ^ p_44) >= (-2L)))) , p_45) , l_441));
                }
                else
                { /* block id: 195 */
                    for (g_236 = 13; (g_236 > (-23)); g_236--)
                    { /* block id: 198 */
                        uint64_t l_504[6][5][1] = {{{1UL},{0x9D1F0378D528E527LL},{0x4C53656B147C340DLL},{0xF1D312E5505EC4B1LL},{0x9D1F0378D528E527LL}},{{0xF1D312E5505EC4B1LL},{0x4C53656B147C340DLL},{0x9D1F0378D528E527LL},{1UL},{1UL}},{{0x9D1F0378D528E527LL},{0x4C53656B147C340DLL},{0xF1D312E5505EC4B1LL},{0x9D1F0378D528E527LL},{0xF1D312E5505EC4B1LL}},{{0x4C53656B147C340DLL},{0x9D1F0378D528E527LL},{1UL},{1UL},{0x9D1F0378D528E527LL}},{{0x4C53656B147C340DLL},{0xF1D312E5505EC4B1LL},{0x9D1F0378D528E527LL},{0xF1D312E5505EC4B1LL},{0x4C53656B147C340DLL}},{{0x9D1F0378D528E527LL},{1UL},{1UL},{0x9D1F0378D528E527LL},{0x4C53656B147C340DLL}}};
                        int i, j, k;
                        l_504[2][4][0]++;
                    }
                }
                for (l_335 = (-18); (l_335 == 30); l_335 = safe_add_func_uint8_t_u_u(l_335, 6))
                { /* block id: 204 */
                    uint16_t l_509 = 5UL;
                    --l_509;
                    (*g_76) = p_46;
                    (*g_76) = (l_512 , &l_409[0]);
                    if ((*p_46))
                        continue;
                }
            }
            else
            { /* block id: 210 */
                const int16_t l_560 = 0x72DEL;
                if ((*l_442))
                { /* block id: 211 */
                    uint8_t l_533 = 1UL;
                    (*g_76) = p_46;
                    if (g_237)
                        goto lbl_513;
                    if ((((safe_div_func_int16_t_s_s(g_236, (*g_121))) && (safe_lshift_func_int16_t_s_u(0xE1E5L, (safe_lshift_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s(8UL, 2)), ((*l_468)--))), 12))))) , (**g_76)))
                    { /* block id: 215 */
                        uint16_t l_526 = 0x0192L;
                        l_526--;
                        g_529 = 0xEBB1BF4CL;
                        l_531 = l_530;
                    }
                    else
                    { /* block id: 219 */
                        return (*g_76);
                    }
                    (*l_442) = (((*g_358) != l_442) >= ((l_532 != (void*)0) == ((*l_84) |= ((*g_121) ^= l_533))));
                }
                else
                { /* block id: 225 */
                    uint64_t l_536 = 0x5044645AC530D032LL;
                    int8_t *l_549 = (void*)0;
                    int8_t *l_550[1][1];
                    int32_t l_553 = 0L;
                    uint8_t l_555 = 0x90L;
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_550[i][j] = (void*)0;
                    }
                    if ((safe_sub_func_uint16_t_u_u((((void*)0 != &g_77) || ((p_45 , l_536) , ((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s(((!(safe_rshift_func_uint8_t_u_s(((((safe_sub_func_int32_t_s_s(((safe_rshift_func_uint8_t_u_s((safe_unary_minus_func_uint8_t_u(g_179[3])), ((*l_442) = (g_411 ^ g_102[0])))) , (safe_sub_func_int16_t_s_s(g_110, (((**g_358) & l_536) >= 1UL)))), 0xD6E467D8L)) > g_241) && p_45) > 7L), g_62))) < l_553), p_45)), g_110)) <= g_411))), (*g_121))))
                    { /* block id: 227 */
                        uint64_t **l_563 = &l_86;
                        int32_t l_564[5] = {4L,4L,4L,4L,4L};
                        int i;
                        (*l_442) &= ((g_554 == (void*)0) <= ((l_555 | ((safe_mul_func_uint16_t_u_u(((*g_121) = (((safe_rshift_func_int16_t_s_u((((l_560 >= l_536) | (*p_46)) ^ ((safe_mod_func_uint16_t_u_u(((l_563 == l_426) >= (*g_359)), p_44)) & 248UL)), l_564[4])) , 0x28C0L) , (*g_121))), (*g_554))) & p_44)) <= 0x575C291767AF3AADLL));
                        if (l_564[2])
                            continue;
                    }
                    else
                    { /* block id: 231 */
                        return p_43;
                    }
                }
            }
        }
        l_256 |= (!(safe_add_func_uint16_t_u_u(((*l_442) <= (*g_121)), (safe_rshift_func_int16_t_s_u((*g_554), (safe_div_func_uint8_t_u_u(0xDAL, ((safe_mul_func_int16_t_s_s((0xD6L && 7UL), 0x1CDFL)) , (safe_lshift_func_uint8_t_u_u(((safe_div_func_int32_t_s_s(((p_45 < (((safe_div_func_uint64_t_u_u((safe_mod_func_int32_t_s_s((((*l_442) && l_582[2]) , l_583), (*l_442))), (*l_442))) ^ 6L) > g_238)) < 0xBE92EC83L), (*l_442))) , g_239), 7))))))))));
    }
    l_604 = (g_179[3] <= (safe_div_func_int8_t_s_s(((((((*l_603) |= (safe_add_func_uint16_t_u_u(((255UL & 0x37L) < (safe_mul_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(g_236, ((++(*l_84)) , p_45))), ((((*l_595) = g_594) == ((*l_602) = ((safe_unary_minus_func_uint64_t_u((safe_sub_func_uint16_t_u_u((((((void*)0 == g_599) || ((*g_121) , g_106)) && p_44) | (*g_121)), 0x5796L)))) , g_600))) , g_238)))), (*g_554)))) , (**l_602)) != (void*)0) > 3L) < (*g_554)), g_18)));
    g_237 = ((*l_607) = (*p_46));
    return p_43;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_52(uint32_t  p_53, int32_t * const * p_54, int32_t  p_55, uint32_t  p_56, int32_t  p_57)
{ /* block id: 17 */
    uint8_t l_63 = 0xBDL;
    return l_63;
}


/* ------------------------------------------ */
/* 
 * reads : g_62 g_241 g_226
 * writes: g_62 g_241
 */
static uint8_t  func_68(uint64_t  p_69, uint16_t  p_70, int32_t * p_71, int64_t  p_72, int64_t  p_73)
{ /* block id: 22 */
    int32_t l_103 = 0x3EC0817FL;
    int32_t l_111 = 0x12D82128L;
    int32_t l_153 = 1L;
    int32_t l_174 = (-7L);
    int32_t l_175[1];
    uint16_t l_182 = 1UL;
    int32_t l_227 = 0x20BBD9F0L;
    int16_t l_229 = 1L;
    int32_t *l_232 = (void*)0;
    int32_t *l_233 = &l_174;
    int32_t *l_234[10][5][5] = {{{(void*)0,&g_21[4][2][0],&g_21[4][2][0],(void*)0,&l_175[0]},{&g_106,&g_21[4][2][0],&g_106,&g_106,(void*)0},{&l_227,&l_174,&l_227,&l_175[0],&l_153},{(void*)0,&l_175[0],&g_8[2][6],&l_174,&l_174},{&l_175[0],&l_227,&l_175[0],&l_111,&l_175[0]}},{{&l_175[0],&g_21[4][2][0],&l_175[0],&l_227,&g_106},{(void*)0,&g_8[2][6],&l_174,&g_106,&g_106},{&l_227,&l_174,&l_175[0],&g_106,&l_175[0]},{&l_227,&l_227,&l_175[0],&g_21[4][2][0],&l_175[0]},{&l_111,&l_175[0],&g_8[2][6],&l_153,&g_106}},{{&g_21[0][6][1],&l_153,&l_227,&l_153,&g_106},{&l_153,&l_175[0],(void*)0,(void*)0,&l_175[0]},{&g_106,&l_227,&g_106,(void*)0,&l_174},{&l_174,&l_174,&g_8[2][6],&l_153,&l_153},{&g_21[4][2][0],&g_8[2][6],&g_106,&l_153,&l_111}},{{&l_174,&g_21[4][2][0],&l_111,&g_21[4][2][0],&l_174},{&g_106,&l_227,&l_111,&g_106,&l_227},{&l_153,&l_175[0],&g_106,&g_106,&g_21[0][6][1]},{&g_21[0][6][1],&l_174,&g_8[2][6],&l_227,&l_227},{&l_111,&g_106,&g_106,&l_111,&l_174}},{{&l_227,&g_106,(void*)0,&l_174,&l_111},{&l_227,&l_174,&l_227,&l_175[0],&l_153},{(void*)0,&l_175[0],&g_8[2][6],&l_174,&l_174},{&l_175[0],&l_227,&l_175[0],&l_111,&l_175[0]},{&l_175[0],&g_21[4][2][0],&l_175[0],&l_227,&g_106}},{{(void*)0,&g_8[2][6],&l_174,&g_106,&g_106},{&l_227,&l_174,&l_175[0],&g_106,&l_175[0]},{&l_227,&l_227,&l_175[0],&g_21[4][2][0],&l_175[0]},{&l_111,&l_175[0],&g_8[2][6],&l_153,&g_106},{&g_21[0][6][1],&l_153,&l_227,&l_153,&g_106}},{{&l_153,&l_175[0],(void*)0,(void*)0,&l_175[0]},{&g_106,&l_227,&g_106,(void*)0,&l_174},{&l_174,&l_174,&g_8[2][6],&l_153,&l_153},{&g_21[4][2][0],&g_8[2][6],&g_106,&l_153,&l_111},{&l_174,&g_21[4][2][0],&l_111,&g_21[4][2][0],&l_174}},{{&g_106,&l_227,&l_111,&g_106,&l_227},{&l_153,&l_175[0],&g_106,&g_106,&g_21[0][6][1]},{&g_21[0][6][1],&l_174,&g_8[2][6],&l_227,&l_227},{&l_111,&g_106,&g_106,&l_111,&l_174},{&l_227,&g_106,(void*)0,&l_174,&l_111}},{{&l_227,&g_21[4][2][0],&g_21[0][6][1],&l_174,&g_8[2][6]},{&l_111,&g_106,&l_153,&l_175[0],&l_175[0]},{&g_106,&g_8[2][6],&g_106,&l_175[0],&l_153},{&g_106,&g_106,&l_174,&g_8[2][6],(void*)0},{&l_111,&l_153,&g_21[4][2][0],&l_227,&l_174}},{{&g_8[2][6],&l_175[0],&l_174,(void*)0,&l_174},{&g_21[0][6][1],&g_21[0][6][1],&g_106,&g_106,&l_174},{&l_175[0],&l_153,&l_153,&g_8[2][6],&l_174},{(void*)0,&g_8[2][6],&g_21[0][6][1],&g_106,(void*)0},{&g_106,&l_153,&l_111,&l_111,&l_153}}};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_175[i] = (-10L);
    for (g_62 = 4; (g_62 <= 20); g_62 = safe_add_func_int32_t_s_s(g_62, 1))
    { /* block id: 25 */
        uint32_t l_104 = 0xE2B08325L;
        int32_t *l_105 = &g_106;
        int32_t *l_107[4] = {&g_108,&g_108,&g_108,&g_108};
        int i;
    }
    l_175[0] ^= l_182;
    g_241--;
    return g_226[2];
}




/* ---------------------------------------- */
//testcase_id 1484153290
int case1484153290(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_8[i][j], "g_8[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_18, "g_18", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_21[i][j][k], "g_21[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_51, "g_51", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_85, "g_85", print_hash_value);
    transparent_crc(g_87, "g_87", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_102[i], "g_102[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_106, "g_106", print_hash_value);
    transparent_crc(g_108, "g_108", print_hash_value);
    transparent_crc(g_110, "g_110", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc(g_178, "g_178", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_179[i], "g_179[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_226[i], "g_226[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_235, "g_235", print_hash_value);
    transparent_crc(g_236, "g_236", print_hash_value);
    transparent_crc(g_237, "g_237", print_hash_value);
    transparent_crc(g_238, "g_238", print_hash_value);
    transparent_crc(g_239, "g_239", print_hash_value);
    transparent_crc(g_240, "g_240", print_hash_value);
    transparent_crc(g_241, "g_241", print_hash_value);
    transparent_crc(g_384, "g_384", print_hash_value);
    transparent_crc(g_408, "g_408", print_hash_value);
    transparent_crc(g_411, "g_411", print_hash_value);
    transparent_crc(g_412, "g_412", print_hash_value);
    transparent_crc(g_529, "g_529", print_hash_value);
    transparent_crc(g_667, "g_667", print_hash_value);
    transparent_crc(g_755, "g_755", print_hash_value);
    transparent_crc(g_785, "g_785", print_hash_value);
    transparent_crc(g_786, "g_786", print_hash_value);
    transparent_crc(g_913, "g_913", print_hash_value);
    transparent_crc(g_918, "g_918", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_991[i][j], "g_991[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1005, "g_1005", print_hash_value);
    transparent_crc(g_1030, "g_1030", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1031[i], "g_1031[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1032, "g_1032", print_hash_value);
    transparent_crc(g_1042, "g_1042", print_hash_value);
    transparent_crc(g_1111, "g_1111", print_hash_value);
    transparent_crc(g_1169, "g_1169", print_hash_value);
    transparent_crc(g_1227, "g_1227", print_hash_value);
    transparent_crc(g_1244, "g_1244", print_hash_value);
    transparent_crc(g_1281, "g_1281", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1374[i][j], "g_1374[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1568[i][j], "g_1568[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1584, "g_1584", print_hash_value);
    transparent_crc(g_1638, "g_1638", print_hash_value);
    transparent_crc(g_1674, "g_1674", print_hash_value);
    transparent_crc(g_1743, "g_1743", print_hash_value);
    transparent_crc(g_1914, "g_1914", print_hash_value);
    transparent_crc(g_1940, "g_1940", print_hash_value);
    transparent_crc(g_1983, "g_1983", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_2035[i][j], "g_2035[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2524, "g_2524", print_hash_value);
    transparent_crc(g_2744, "g_2744", print_hash_value);
    transparent_crc(g_2750, "g_2750", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_2856[i][j], "g_2856[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3119, "g_3119", print_hash_value);
    transparent_crc(g_3304, "g_3304", print_hash_value);
    transparent_crc(g_3370, "g_3370", print_hash_value);
    transparent_crc(g_3405, "g_3405", print_hash_value);
    transparent_crc(g_3406, "g_3406", print_hash_value);
    transparent_crc(g_3454, "g_3454", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_3455[i][j][k], "g_3455[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_3460[i][j][k], "g_3460[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_3550[i][j], "g_3550[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 859
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 65
breakdown:
   depth: 1, occurrence: 231
   depth: 2, occurrence: 53
   depth: 3, occurrence: 1
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 7, occurrence: 1
   depth: 9, occurrence: 2
   depth: 10, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 14, occurrence: 1
   depth: 15, occurrence: 3
   depth: 16, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 4
   depth: 22, occurrence: 3
   depth: 23, occurrence: 3
   depth: 24, occurrence: 1
   depth: 25, occurrence: 2
   depth: 26, occurrence: 3
   depth: 27, occurrence: 1
   depth: 28, occurrence: 4
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 2
   depth: 33, occurrence: 1
   depth: 34, occurrence: 2
   depth: 40, occurrence: 1
   depth: 46, occurrence: 1
   depth: 65, occurrence: 1

XXX total number of pointers: 604

XXX times a variable address is taken: 1644
XXX times a pointer is dereferenced on RHS: 577
breakdown:
   depth: 1, occurrence: 388
   depth: 2, occurrence: 123
   depth: 3, occurrence: 55
   depth: 4, occurrence: 11
XXX times a pointer is dereferenced on LHS: 438
breakdown:
   depth: 1, occurrence: 376
   depth: 2, occurrence: 39
   depth: 3, occurrence: 19
   depth: 4, occurrence: 4
XXX times a pointer is compared with null: 80
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 27
XXX times a pointer is qualified to be dereferenced: 12031

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1904
   level: 2, occurrence: 625
   level: 3, occurrence: 226
   level: 4, occurrence: 55
   level: 5, occurrence: 5
XXX number of pointers point to pointers: 229
XXX number of pointers point to scalars: 375
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 33.1
XXX average alias set size: 1.68

XXX times a non-volatile is read: 3249
XXX times a non-volatile is write: 1422
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 11

XXX stmts: 231
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 25
   depth: 2, occurrence: 39
   depth: 3, occurrence: 51
   depth: 4, occurrence: 39
   depth: 5, occurrence: 47

XXX percentage a fresh-made variable is used: 15.6
XXX percentage an existing variable is used: 84.4
********************* end of statistics **********************/

