/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      4199411016
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_2 = 0x8CFF0EA7L;
static int32_t g_5 = 0x108F8256L;
static uint16_t g_20 = 0x58F9L;
static uint32_t g_32 = 0x61BE360DL;
static uint32_t g_60 = 18446744073709551610UL;
static uint32_t *g_59 = &g_60;
static int32_t g_63 = 0x3D73C12DL;
static int64_t g_65 = (-1L);
static int16_t g_66 = 2L;
static int64_t g_67[8][10] = {{0xB1F15F1D345D1904LL,0xB2E1D86E6AF57566LL,4L,0xB2E1D86E6AF57566LL,0xB1F15F1D345D1904LL,(-1L),(-6L),0L,(-1L),0x57C1F20CF68020C2LL},{(-6L),0xA24791A13C53D173LL,0x57C1F20CF68020C2LL,(-1L),(-1L),0xA9F139888327BC4ALL,0xA9F139888327BC4ALL,(-1L),(-1L),0x57C1F20CF68020C2LL},{(-1L),(-1L),0x4F120B252307D3E6LL,0x57C1F20CF68020C2LL,0xB1F15F1D345D1904LL,(-1L),(-1L),4L,(-10L),0xDF123EFBB05D0942LL},{4L,0xC99CF48B058B506ELL,(-1L),(-1L),0xB2E1D86E6AF57566LL,(-1L),(-1L),0xC99CF48B058B506ELL,4L,(-1L)},{0xA24791A13C53D173LL,(-1L),(-10L),0xB1F15F1D345D1904LL,0xDF123EFBB05D0942LL,0x5767A065BBD97C29LL,0x4F120B252307D3E6LL,(-1L),(-1L),0x4F120B252307D3E6LL},{0xB2E1D86E6AF57566LL,(-10L),(-1L),(-1L),(-10L),0xB2E1D86E6AF57566LL,0x5767A065BBD97C29LL,0x57C1F20CF68020C2LL,0L,0xA9F139888327BC4ALL},{0xA9F139888327BC4ALL,(-1L),4L,0xC99CF48B058B506ELL,(-1L),(-1L),0xB2E1D86E6AF57566LL,(-1L),(-1L),0xC99CF48B058B506ELL},{0xA9F139888327BC4ALL,(-1L),0xA9F139888327BC4ALL,0xA24791A13C53D173LL,0x4F120B252307D3E6LL,0xB2E1D86E6AF57566LL,0xC99CF48B058B506ELL,0xB1F15F1D345D1904LL,4L,0x57C1F20CF68020C2LL}};
static uint16_t g_68 = 0x4235L;
static const uint16_t g_79 = 1UL;
static int32_t *g_97 = (void*)0;
static uint32_t g_104 = 1UL;
static int64_t g_110[5] = {4L,4L,4L,4L,4L};
static uint32_t g_114 = 18446744073709551611UL;
static uint32_t *g_113[9][5][2] = {{{&g_114,&g_114},{(void*)0,(void*)0},{&g_114,(void*)0},{(void*)0,&g_114},{&g_114,&g_114}},{{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114}},{{&g_114,&g_114},{&g_114,(void*)0},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114}},{{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{(void*)0,&g_114}},{{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114}},{{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{(void*)0,&g_114},{&g_114,&g_114}},{{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114}},{{&g_114,(void*)0},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114}},{{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{&g_114,&g_114},{(void*)0,(void*)0}}};
static int32_t g_144[3][2][2] = {{{0x1DC2E7BDL,0x1DC2E7BDL},{1L,0x1DC2E7BDL}},{{0x1DC2E7BDL,1L},{0x1DC2E7BDL,0x1DC2E7BDL}},{{1L,0x1DC2E7BDL},{0x1DC2E7BDL,1L}}};
static int32_t g_165 = (-1L);
static int8_t g_168 = 7L;
static uint8_t g_180 = 1UL;
static const uint16_t * const g_269[7][5] = {{&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79},{&g_79,&g_79,&g_79,&g_79,&g_79}};
static const uint16_t * const *g_268 = &g_269[6][1];
static const uint16_t * const **g_267[6] = {(void*)0,&g_268,(void*)0,(void*)0,&g_268,(void*)0};
static uint64_t g_365 = 18446744073709551610UL;
static uint64_t g_395 = 0UL;
static int16_t g_418[3][6][10] = {{{(-6L),0x2BCAL,0L,1L,0x2BCAL,1L,0L,0x2BCAL,(-6L),(-1L)},{1L,0L,0x2BCAL,(-6L),(-6L),0x2BCAL,0L,1L,0x2BCAL,1L},{(-6L),0x3039L,0xA3C5L,(-6L),0xA3C5L,0x3039L,(-6L),(-1L),(-1L),(-6L)},{(-1L),1L,0xA3C5L,0xA3C5L,1L,(-1L),0x3039L,1L,0x3039L,(-1L)},{0L,1L,0x2BCAL,1L,0L,0x2BCAL,(-6L),(-6L),0x2BCAL,0L},{0L,0x3039L,0x3039L,0L,0xA3C5L,(-1L),0L,(-1L),0xA3C5L,0L}},{{(-1L),0L,(-1L),0xA3C5L,0L,0x3039L,0x3039L,0L,0xA3C5L,(-1L)},{(-6L),(-6L),0x2BCAL,0L,1L,0x2BCAL,1L,0L,0x2BCAL,(-6L)},{1L,0x3039L,(-1L),1L,0xA3C5L,0xA3C5L,1L,(-1L),0x3039L,1L},{(-1L),(-6L),0x3039L,0xA3C5L,(-6L),0xA3C5L,0x3039L,(-6L),(-1L),(-1L)},{1L,0L,0x2BCAL,(-6L),(-6L),0x2BCAL,0L,1L,0x2BCAL,1L},{(-6L),0x3039L,0xA3C5L,(-6L),0xA3C5L,0x3039L,(-6L),(-1L),(-1L),(-6L)}},{{(-1L),1L,0xA3C5L,0xA3C5L,1L,(-1L),0x3039L,1L,0x3039L,(-1L)},{0L,1L,0x2BCAL,1L,0L,0x2BCAL,(-6L),(-6L),0x2BCAL,0L},{0L,0x3039L,0x3039L,0L,0xA3C5L,(-1L),0L,(-1L),0xA3C5L,0L},{(-1L),0L,(-1L),0xA3C5L,0L,0x3039L,0x3039L,0L,0xA3C5L,(-1L)},{(-6L),(-6L),0x2BCAL,0L,1L,0x2BCAL,1L,0L,0x2BCAL,(-6L)},{1L,0x3039L,(-1L),1L,0xA3C5L,0xA3C5L,1L,(-1L),0x3039L,1L}}};
static int64_t g_432[4] = {0x2D4EA37ED070690ELL,0x2D4EA37ED070690ELL,0x2D4EA37ED070690ELL,0x2D4EA37ED070690ELL};
static uint8_t g_433 = 255UL;
static uint16_t g_450 = 1UL;
static uint16_t g_451 = 0xCAC0L;
static uint16_t g_452[4] = {3UL,3UL,3UL,3UL};
static uint16_t ***g_485 = (void*)0;
static uint32_t g_520 = 0UL;
static uint16_t ****g_545 = &g_485;
static uint16_t **** const *g_544 = &g_545;
static int64_t * const g_559 = &g_65;
static int64_t * const *g_558 = &g_559;
static int32_t **g_586 = &g_97;
static int32_t ***g_585 = &g_586;
static int32_t ****g_607 = &g_585;
static int32_t ****g_612 = &g_585;
static int8_t *g_626 = (void*)0;
static int8_t **g_625 = &g_626;
static int64_t g_656 = 2L;
static int8_t g_682 = 0xACL;
static int64_t g_689 = 0x9E8EEF2E73AD137BLL;
static uint32_t g_712 = 0UL;
static uint16_t * const g_885 = &g_451;
static uint16_t * const *g_884 = &g_885;
static uint16_t * const **g_883 = &g_884;
static uint8_t *g_922 = &g_433;
static uint8_t **g_921 = &g_922;
static uint8_t g_977 = 0xF4L;
static const int8_t g_985 = 0x57L;
static const int8_t *g_984 = &g_985;
static int32_t g_989 = 0x5050DA7DL;
static const uint32_t g_1214 = 0UL;
static uint32_t g_1258 = 7UL;
static uint64_t g_1282 = 0x8513EE2201449BFCLL;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t * func_8(uint8_t  p_9, const int32_t * p_10, int64_t  p_11, int32_t * p_12);
static uint8_t  func_13(uint32_t  p_14);
static uint32_t  func_15(int16_t  p_16, int32_t * p_17, uint32_t  p_18);
static int16_t  func_26(int8_t  p_27, const uint32_t  p_28, uint16_t * const  p_29, uint16_t * p_30);
static uint16_t * func_35(int32_t * p_36, int64_t  p_37);
static int32_t * func_38(uint32_t * p_39, uint32_t * p_40, uint64_t  p_41, int32_t  p_42);
static uint32_t * func_43(int32_t * p_44);
static int32_t * func_45(uint32_t * p_46, int32_t  p_47, uint32_t  p_48);
static uint32_t * func_49(uint32_t  p_50, uint16_t * p_51);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_433
 * writes: g_2 g_5 g_558 g_433
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int32_t l_21 = 0x74B28ED4L;
    uint16_t *l_686 = &g_452[0];
    int32_t *l_815 = &g_2;
    for (g_2 = (-22); (g_2 < (-17)); g_2 = safe_add_func_int16_t_s_s(g_2, 3))
    { /* block id: 3 */
        int8_t l_115 = 0xF0L;
        int32_t *l_690 = &g_2;
        int64_t *l_1284 = &g_67[0][1];
        int64_t **l_1283 = &l_1284;
        int64_t ***l_1285 = &l_1283;
        for (g_5 = 0; (g_5 < 3); g_5 = safe_add_func_uint8_t_u_u(g_5, 7))
        { /* block id: 6 */
            uint16_t *l_19[5] = {&g_20,&g_20,&g_20,&g_20,&g_20};
            uint32_t *l_31 = &g_32;
            uint32_t **l_111 = (void*)0;
            uint32_t **l_112 = &g_59;
            int32_t **l_243 = (void*)0;
            int32_t **l_244 = &g_97;
            int64_t *l_688 = &g_689;
            int i;
        }
        if ((*l_815))
            continue;
        g_558 = ((*l_1285) = l_1283);
        for (g_433 = 0; (g_433 <= 5); g_433 += 1)
        { /* block id: 553 */
            int32_t l_1286 = 0xFEAE8509L;
            return l_1286;
        }
    }
    return (*l_815);
}


/* ------------------------------------------ */
/* 
 * reads : g_68 g_144 g_67 g_66 g_2 g_5 g_110 g_559 g_65 g_60 g_432 g_922 g_883 g_884 g_885 g_451 g_20 g_682 g_114 g_558 g_450 g_104 g_921 g_433 g_520 g_984 g_985 g_689 g_418 g_712 g_585 g_586 g_168 g_625 g_626 g_1214 g_268 g_269 g_79 g_977 g_452 g_59 g_656 g_165 g_1258 g_989 g_1282
 * writes: g_144 g_104 g_60 g_450 g_68 g_432 g_433 g_451 g_65 g_365 g_97 g_395 g_418 g_520 g_712 g_168 g_165 g_1258 g_977 g_989 g_1282
 */
static int32_t * func_8(uint8_t  p_9, const int32_t * p_10, int64_t  p_11, int32_t * p_12)
{ /* block id: 325 */
    int32_t l_820 = 0x3F189AC6L;
    int8_t ***l_826[5][7][7] = {{{&g_625,&g_625,&g_625,(void*)0,&g_625,(void*)0,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,(void*)0,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,(void*)0},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625}},{{&g_625,(void*)0,&g_625,&g_625,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,(void*)0,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,(void*)0},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625}},{{&g_625,(void*)0,&g_625,&g_625,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,(void*)0,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,(void*)0},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625}},{{&g_625,(void*)0,&g_625,&g_625,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,(void*)0,(void*)0,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,(void*)0},{&g_625,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625}},{{&g_625,&g_625,(void*)0,(void*)0,&g_625,&g_625,&g_625},{(void*)0,&g_625,&g_625,&g_625,&g_625,&g_625,(void*)0},{&g_625,&g_625,&g_625,&g_625,(void*)0,&g_625,&g_625},{(void*)0,&g_625,&g_625,&g_625,&g_625,&g_625,&g_625},{&g_625,&g_625,(void*)0,&g_625,&g_625,&g_625,(void*)0},{(void*)0,&g_625,(void*)0,(void*)0,&g_625,&g_625,&g_625},{&g_625,&g_625,(void*)0,&g_625,&g_625,&g_625,&g_625}}};
    int32_t *l_827 = &g_144[2][0][1];
    uint16_t *l_843 = &g_451;
    uint16_t **l_842[1];
    uint16_t ** const *l_841 = &l_842[0];
    uint16_t ** const **l_840 = &l_841;
    uint16_t ** const ***l_844 = &l_840;
    int32_t l_845 = 0x4B1B05A5L;
    uint32_t *l_846 = &g_104;
    int32_t l_847 = 0x56EF2548L;
    int32_t l_860 = 0x967F2D53L;
    int32_t l_862 = 0x83D91B2AL;
    int32_t l_864[8][6] = {{(-1L),8L,1L,8L,(-1L),(-1L)},{0L,8L,8L,0L,0xD3512627L,0L},{0L,0xD3512627L,0L,8L,8L,0L},{(-1L),(-1L),8L,1L,8L,(-1L)},{8L,0xD3512627L,1L,1L,0xD3512627L,8L},{(-1L),8L,1L,8L,(-1L),(-1L)},{0L,8L,8L,0L,0xD3512627L,0L},{0L,0xD3512627L,0L,8L,8L,0L}};
    int16_t l_865 = 0x9DDAL;
    const int32_t *l_929 = &l_820;
    uint8_t l_939 = 0x16L;
    int32_t * const *l_965 = &g_97;
    int32_t * const * const *l_964 = &l_965;
    int8_t l_1056 = 2L;
    const int32_t **l_1092 = &l_929;
    const int32_t ***l_1091 = &l_1092;
    const int32_t ****l_1090 = &l_1091;
    const int32_t *****l_1089 = &l_1090;
    int32_t l_1110[8][3][7] = {{{0xE5DD858CL,1L,0x7D6F2896L,0L,0L,0x7D6F2896L,1L},{0x7D6F2896L,(-1L),1L,0x4200911FL,0x0F72F482L,1L,0xE5DD858CL},{0x7D6F2896L,0x2AE07B7BL,0x9707D655L,1L,0x9707D655L,0x2AE07B7BL,0x7D6F2896L}},{{0xE5DD858CL,1L,0x0F72F482L,0x4200911FL,1L,(-1L),0x7D6F2896L},{1L,0x7D6F2896L,0L,0L,0x7D6F2896L,1L,0xE5DD858CL},{0x2AE07B7BL,0L,0x0F72F482L,0xE5DD858CL,8L,1L,1L}},{{0x4200911FL,8L,0x9707D655L,8L,0x4200911FL,(-1L),0x2AE07B7BL},{0L,0L,1L,1L,0x4200911FL,0x2AE07B7BL,0x4200911FL},{(-1L),0x7D6F2896L,0x7D6F2896L,(-1L),8L,1L,0L}},{{0L,1L,8L,(-1L),0x7D6F2896L,0x7D6F2896L,(-1L)},{0x4200911FL,0x2AE07B7BL,0x4200911FL,1L,1L,0L,0L},{0x2AE07B7BL,(-1L),0x4200911FL,8L,0x9707D655L,8L,0x4200911FL}},{{1L,1L,8L,0xE5DD858CL,0x0F72F482L,0L,0x2AE07B7BL},{0xE5DD858CL,1L,0x7D6F2896L,0L,0x7D6F2896L,(-1L),8L},{(-1L),0x0F72F482L,8L,0L,0x9707D655L,(-1L),1L}},{{(-1L),0xE5DD858CL,0L,8L,0L,0xE5DD858CL,(-1L)},{1L,(-1L),0x9707D655L,0L,8L,0x0F72F482L,(-1L)},{8L,(-1L),0x7D6F2896L,0x7D6F2896L,(-1L),8L,1L}},{{0xE5DD858CL,0x7D6F2896L,0x9707D655L,1L,0x2AE07B7BL,8L,8L},{0L,0x2AE07B7BL,0L,0x2AE07B7BL,0L,0x0F72F482L,0xE5DD858CL},{0x4200911FL,0x7D6F2896L,8L,(-1L),0L,0xE5DD858CL,0L}},{{1L,(-1L),(-1L),1L,0x2AE07B7BL,(-1L),0x4200911FL},{0x4200911FL,(-1L),0x2AE07B7BL,1L,(-1L),(-1L),1L},{0L,0xE5DD858CL,0L,(-1L),8L,0x7D6F2896L,0x4200911FL}}};
    uint8_t l_1241 = 0xCAL;
    int64_t l_1277 = 0xF1D46240DC2861CDLL;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_842[i] = &l_843;
    l_847 = (((*l_846) = (((safe_add_func_uint8_t_u_u((safe_add_func_uint32_t_u_u((l_820 , (safe_sub_func_uint16_t_u_u((((safe_unary_minus_func_int16_t_s(((safe_mod_func_uint64_t_u_u((((l_820 ^ p_9) == (g_68 ^ ((*l_827) = (l_826[4][6][5] != l_826[4][6][5])))) || (((((safe_mod_func_uint8_t_u_u(((*l_827) >= (((l_820 = (safe_unary_minus_func_uint8_t_u((((safe_mod_func_uint64_t_u_u(((safe_mod_func_int32_t_s_s((safe_unary_minus_func_uint64_t_u((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u(((*l_827) < ((((*l_844) = l_840) != &l_841) > g_67[1][6])), g_144[1][1][1])), p_11)))), (*l_827))) == 4294967295UL), (*l_827))) ^ p_9) , 255UL)))) , p_11) ^ g_66)), g_2)) , (*l_827)) || p_9) ^ (*p_12)) & 1L)), g_5)) , p_9))) & (*p_10)) <= 8UL), g_110[1]))), l_845)), (-1L))) , (*g_559)) , p_9)) ^ l_847);
    (*l_827) = ((p_11 ^ p_9) < (((*l_827) == (((void*)0 != &g_712) , (safe_rshift_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(p_9, (safe_lshift_func_uint16_t_u_s(p_11, (*l_827))))), (safe_lshift_func_uint8_t_u_s((*l_827), (*l_827))))))) , p_9));
    for (g_60 = 0; (g_60 <= 4); g_60 += 1)
    { /* block id: 334 */
        int16_t l_858[7][2][5] = {{{0L,(-4L),(-6L),0L,1L},{1L,(-6L),(-6L),1L,(-7L)}},{{1L,(-4L),0x6F9EL,1L,1L},{0L,(-4L),(-6L),0L,1L}},{{1L,(-6L),(-6L),1L,(-7L)},{1L,(-4L),0x6F9EL,1L,1L}},{{0L,(-4L),(-6L),0L,1L},{1L,(-6L),(-6L),1L,(-7L)}},{{1L,(-4L),0x6F9EL,1L,1L},{0L,(-4L),(-6L),0L,1L}},{{1L,(-6L),(-6L),1L,(-7L)},{1L,(-4L),0x6F9EL,1L,1L}},{{0L,(-4L),(-6L),0L,1L},{1L,(-6L),(-6L),1L,(-7L)}}};
        int32_t l_859 = 0L;
        int32_t l_861 = (-4L);
        int32_t l_863[1];
        uint16_t l_897 = 1UL;
        uint8_t **l_926 = &g_922;
        int32_t l_1022 = 0xECE010F8L;
        int32_t l_1054 = (-1L);
        int32_t *****l_1088[10][2] = {{&g_612,&g_612},{&g_607,&g_612},{&g_612,&g_607},{&g_612,&g_612},{&g_607,&g_612},{&g_612,&g_607},{&g_612,&g_612},{&g_607,&g_612},{&g_612,&g_607},{&g_612,&g_612}};
        uint16_t l_1145 = 0x91CFL;
        uint32_t l_1183 = 18446744073709551613UL;
        uint16_t **l_1225 = (void*)0;
        uint64_t l_1281 = 18446744073709551608UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_863[i] = 1L;
        for (g_450 = 0; (g_450 <= 4); g_450 += 1)
        { /* block id: 337 */
            int32_t *l_856 = (void*)0;
            int32_t *l_857[6][7][6] = {{{&g_5,(void*)0,(void*)0,&g_5,&l_847,&l_820},{(void*)0,&l_820,(void*)0,(void*)0,&l_820,(void*)0},{(void*)0,&g_144[2][1][0],&l_820,&g_5,&l_820,&g_5},{&g_5,&l_820,&g_5,&g_63,&l_847,(void*)0},{&g_5,(void*)0,&g_63,&g_5,(void*)0,&g_5},{&l_845,&g_5,&g_144[2][1][0],&l_847,&g_5,&g_165},{&g_165,(void*)0,(void*)0,&g_2,&g_5,(void*)0}},{{&l_820,&g_5,&g_5,&g_144[2][1][0],(void*)0,&g_165},{&l_820,&g_63,&l_847,&g_2,(void*)0,&g_5},{&g_165,&g_5,&l_847,&l_847,&g_5,&g_165},{&l_845,(void*)0,&g_5,&g_2,&g_5,(void*)0},{&g_2,&g_5,(void*)0,&g_144[2][1][0],(void*)0,&g_165},{&g_2,&g_63,&g_144[2][1][0],&g_2,(void*)0,&g_5},{&l_845,&g_5,&g_144[2][1][0],&l_847,&g_5,&g_165}},{{&g_165,(void*)0,(void*)0,&g_2,&g_5,(void*)0},{&l_820,&g_5,&g_5,&g_144[2][1][0],(void*)0,&g_165},{&l_820,&g_63,&l_847,&g_2,(void*)0,&g_5},{&g_165,&g_5,&l_847,&l_847,&g_5,&g_165},{&l_845,(void*)0,&g_5,&g_2,&g_5,(void*)0},{&g_2,&g_5,(void*)0,&g_144[2][1][0],(void*)0,&g_165},{&g_2,&g_63,&g_144[2][1][0],&g_2,(void*)0,&g_5}},{{&l_845,&g_5,&g_144[2][1][0],&l_847,&g_5,&g_165},{&g_165,(void*)0,(void*)0,&g_2,&g_5,(void*)0},{&l_820,&g_5,&g_5,&g_144[2][1][0],(void*)0,&g_165},{&l_820,&g_63,&l_847,&g_2,(void*)0,&g_5},{&g_165,&g_5,&l_847,&l_847,&g_5,&g_165},{&l_845,(void*)0,&g_5,&g_2,&g_5,(void*)0},{&g_2,&g_5,(void*)0,&g_144[2][1][0],(void*)0,&g_165}},{{&g_2,&g_63,&g_144[2][1][0],&g_2,(void*)0,&g_5},{&l_845,&g_5,&g_144[2][1][0],&l_847,&g_5,&g_165},{&g_165,(void*)0,(void*)0,&g_2,&g_5,(void*)0},{&l_820,&g_5,&g_5,&g_144[2][1][0],(void*)0,&g_165},{&l_820,&g_63,&l_847,&g_2,(void*)0,&g_5},{&g_165,&g_5,&l_847,&l_847,&g_5,&g_165},{&l_845,(void*)0,&g_5,&g_2,&g_5,(void*)0}},{{&g_2,&g_5,(void*)0,&g_144[2][1][0],(void*)0,&g_165},{&g_2,&g_63,(void*)0,&g_63,&l_845,&l_847},{(void*)0,&l_820,(void*)0,&g_144[2][1][0],&g_2,&l_847},{&l_847,&l_847,(void*)0,&g_63,&g_2,(void*)0},{&l_820,&l_820,&l_847,(void*)0,&l_845,&l_847},{&l_820,&g_144[2][1][0],&g_144[2][1][0],&g_63,&g_165,&l_847},{&l_847,&l_820,&g_144[2][1][0],&g_144[2][1][0],&l_820,&l_847}}};
            uint32_t l_866 = 4294967295UL;
            int32_t l_919 = 0x20E90DF0L;
            uint16_t l_966[7] = {8UL,8UL,8UL,8UL,8UL,8UL,8UL};
            const int64_t l_967[10] = {0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};
            uint8_t l_1057[6][6][4] = {{{0UL,0x7FL,0xADL,255UL},{0x48L,0UL,248UL,255UL},{0xADL,0UL,0x98L,0x98L},{0xADL,0xADL,248UL,0x5FL},{0x48L,0x98L,0xADL,0UL},{0UL,5UL,0xB6L,0xADL}},{{255UL,5UL,255UL,0UL},{5UL,0x98L,0x18L,0x5FL},{0x5FL,0xADL,0UL,0x98L},{255UL,0UL,0UL,255UL},{0x5FL,0UL,0x18L,255UL},{5UL,0x7FL,255UL,248UL}},{{255UL,248UL,0xB6L,248UL},{0UL,0x7FL,0xADL,255UL},{0x48L,0UL,248UL,255UL},{0xADL,0UL,0x98L,0x98L},{0xADL,0xADL,248UL,0x5FL},{0x48L,0x98L,0xADL,0UL}},{{0UL,5UL,0xB6L,0xADL},{255UL,5UL,255UL,0UL},{5UL,0x98L,0x18L,0x5FL},{0x5FL,0xADL,0UL,0x98L},{255UL,0UL,0UL,255UL},{0x5FL,0UL,0x18L,255UL}},{{5UL,0x7FL,255UL,248UL},{255UL,248UL,0xB6L,248UL},{0UL,0x7FL,0xADL,255UL},{0x48L,0UL,248UL,255UL},{0xADL,0UL,0x98L,0x98L},{0xADL,0xADL,248UL,0x5FL}},{{0x48L,0x98L,0xADL,0UL},{0UL,5UL,0xB6L,0xADL},{255UL,5UL,255UL,0UL},{5UL,0x98L,0x18L,0x5FL},{0x5FL,0xADL,0UL,0x98L},{0xADL,255UL,255UL,0xADL}}};
            uint8_t l_1095 = 4UL;
            uint16_t ******l_1168 = (void*)0;
            int16_t *l_1195 = (void*)0;
            int16_t **l_1194[5];
            int8_t ** const l_1205 = &g_626;
            int32_t l_1237 = 5L;
            int i, j, k;
            for (i = 0; i < 5; i++)
                l_1194[i] = &l_1195;
            l_866++;
            for (g_68 = 0; (g_68 <= 4); g_68 += 1)
            { /* block id: 341 */
                int32_t l_891 = (-2L);
                int32_t l_893 = 0x2A91942CL;
                int32_t l_894 = 0x032D7058L;
                int32_t l_895 = 9L;
                int32_t l_896[6][8] = {{(-4L),0xAE13C86FL,0x545F31E0L,0xFE1BDFFBL,0xAE13C86FL,0xB4DB5F72L,(-1L),0x545F31E0L},{0x183BB6E2L,(-1L),0xE0D0DB6AL,(-4L),(-4L),0xE0D0DB6AL,(-1L),0x183BB6E2L},{0x6A6977C9L,(-4L),0x545F31E0L,0xB4DB5F72L,(-5L),0x6A6977C9L,0xB4DB5F72L,0xFE1BDFFBL},{(-5L),0x6A6977C9L,0xB4DB5F72L,0xFE1BDFFBL,0xB4DB5F72L,0x6A6977C9L,(-5L),0xB4DB5F72L},{0x183BB6E2L,(-4L),0L,0x183BB6E2L,(-1L),0xE0D0DB6AL,(-4L),(-4L)},{0xB4DB5F72L,(-1L),0x545F31E0L,0x545F31E0L,(-1L),0xB4DB5F72L,0xAE13C86FL,0xFE1BDFFBL}};
                const uint32_t l_913 = 0xC296BC89L;
                uint64_t l_914 = 0x6C72C8E323DAE3CBLL;
                uint32_t l_943 = 0UL;
                const uint16_t *****l_1041 = (void*)0;
                uint16_t l_1045[1][3];
                int32_t l_1053 = 0xEDFD2158L;
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_1045[i][j] = 0x29B2L;
                }
                for (l_859 = 4; (l_859 >= 0); l_859 -= 1)
                { /* block id: 344 */
                    int32_t **l_871 = &l_827;
                    int32_t l_888[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
                    int16_t *l_912 = &l_858[3][0][2];
                    int16_t **l_911 = &l_912;
                    uint16_t *l_920 = (void*)0;
                    int i;
                    if ((*p_10))
                        break;
                }
            }
            if ((*p_12))
                break;
            for (l_1022 = 3; (l_1022 >= 0); l_1022 -= 1)
            { /* block id: 427 */
                uint64_t l_1120 = 2UL;
                uint16_t ** const ****l_1140 = &l_844;
                int32_t l_1143 = (-1L);
                int32_t l_1144 = 1L;
                uint16_t l_1202 = 0x7310L;
                uint16_t **l_1226[9];
                int32_t l_1227 = 0xB206639CL;
                int32_t *l_1238 = &l_1110[1][0][6];
                int8_t l_1242 = 0xC5L;
                uint32_t l_1253 = 18446744073709551615UL;
                int i;
                for (i = 0; i < 9; i++)
                    l_1226[i] = &l_843;
                for (l_860 = 3; (l_860 >= 0); l_860 -= 1)
                { /* block id: 430 */
                    int i;
                    (*l_827) = (-1L);
                    (*l_1092) = func_45((((**g_558) = (safe_lshift_func_int8_t_s_u((safe_lshift_func_uint16_t_u_s((safe_mod_func_uint64_t_u_u((((*g_922) = ((g_432[l_1022] = (g_110[(l_860 + 1)] == g_110[g_60])) , g_432[l_860])) , (safe_sub_func_uint8_t_u_u((2UL ^ (p_9 & (!(p_9 > (safe_sub_func_uint64_t_u_u(((~(((***g_883) &= l_1110[1][0][6]) == p_11)) | (~(safe_sub_func_int32_t_s_s((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(g_20, p_11)), p_9)), g_682)), l_1120)))), g_114)))))), p_9))), p_11)), p_11)), l_1120))) , (void*)0), p_11, g_144[2][1][1]);
                }
                for (l_939 = 0; (l_939 <= 0); l_939 += 1)
                { /* block id: 440 */
                    uint8_t *l_1164 = &g_433;
                    uint64_t l_1165 = 18446744073709551615UL;
                    int32_t l_1177 = 0xB8D85FC7L;
                    int i, j, k;
                    for (l_861 = 0; (l_861 <= 5); l_861 += 1)
                    { /* block id: 443 */
                        uint64_t *l_1135 = &g_365;
                        int i, j, k;
                        if (l_864[(l_1022 + 3)][(g_450 + 1)])
                            break;
                        l_864[l_861][l_1022] = (p_9 >= ((*g_559) == ((((p_11 < (safe_rshift_func_int8_t_s_s(((g_104 , p_9) == (safe_add_func_int8_t_s_s(0xF7L, (**g_921)))), 1))) >= ((void*)0 != &g_545)) | l_864[(l_1022 + 3)][(g_450 + 1)]) && l_1120)));
                        l_1143 ^= ((safe_rshift_func_int16_t_s_s(((safe_div_func_int32_t_s_s((((safe_add_func_uint8_t_u_u((safe_add_func_uint8_t_u_u((((p_9 == ((*l_1135) = (4UL | 0x60L))) , 65533UL) & (((safe_rshift_func_uint16_t_u_u((safe_add_func_int8_t_s_s(((l_1140 != &g_544) && 0xEE92AF41L), (safe_mod_func_int16_t_s_s(0x5974L, p_11)))), 0)) | (*p_12)) < g_520)), (*g_984))), p_11)) ^ 0UL) , (*p_10)), g_689)) && 1L), 15)) | 18446744073709551609UL);
                    }
                    --l_1145;
                    if ((safe_div_func_int8_t_s_s(((safe_add_func_int16_t_s_s((safe_div_func_int8_t_s_s(p_9, (*g_984))), ((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(((safe_sub_func_int32_t_s_s((safe_mod_func_uint64_t_u_u(p_9, ((*g_559) = (((0xF4609BEF51C3E17FLL ^ (0xEDL || p_9)) != (safe_mod_func_uint32_t_u_u(((*l_846) = (l_1144 ^ ((l_1164 = (*g_921)) == (void*)0))), (*p_12)))) & g_418[1][5][8])))), g_712)) | g_432[0]), l_1143)), l_1144)) , p_11))) | 0x64A6L), (**g_921))))
                    { /* block id: 453 */
                        (**g_585) = p_12;
                    }
                    else
                    { /* block id: 455 */
                        int16_t *l_1178 = &l_865;
                        uint64_t *l_1181 = &g_395;
                        int32_t l_1182[1][7];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 7; j++)
                                l_1182[i][j] = (-4L);
                        }
                        (*l_827) = ((l_1165 < ((*g_922) = (safe_rshift_func_int8_t_s_u((((l_1120 ^ ((*l_846) |= ((((65528UL && ((l_1168 != &l_844) < ((((***l_841)++) || (((-3L) | (safe_mul_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u(((*l_1181) = (((*l_1178) = (0xB8L != (l_1177 = (*g_984)))) < (safe_mod_func_uint16_t_u_u(0x8EBDL, 1UL)))), g_520)), 0x4C87L))) , p_11)) && p_11))) , p_9) != p_11) ^ (*g_922)))) & (-1L)) | p_11), 1)))) >= 6L);
                        ++l_1183;
                        (**l_1091) = p_10;
                    }
                }
                for (p_9 = 0; (p_9 <= 4); p_9 += 1)
                { /* block id: 469 */
                    const int16_t * const l_1197 = &g_418[0][4][6];
                    const int16_t * const *l_1196 = &l_1197;
                    int32_t l_1203[9][7] = {{0L,(-2L),(-2L),0L,(-2L),(-2L),0L},{(-1L),0xCBB380B9L,(-1L),(-1L),0xCBB380B9L,(-1L),(-1L)},{0L,0L,0xED9439F6L,0L,0L,0xED9439F6L,0L},{0xCBB380B9L,(-1L),(-1L),0xCBB380B9L,(-1L),(-1L),0xCBB380B9L},{(-2L),0L,(-2L),(-2L),0L,(-2L),(-2L)},{0xCBB380B9L,0xCBB380B9L,(-1L),0xCBB380B9L,0xCBB380B9L,(-1L),0xCBB380B9L},{0L,(-2L),(-2L),0L,(-2L),(-2L),0L},{(-1L),0xCBB380B9L,(-1L),(-1L),0xCBB380B9L,(-1L),(-1L)},{0L,0L,0xED9439F6L,0L,0L,0xED9439F6L,0L}};
                    uint64_t *l_1204 = &g_395;
                    int i, j;
                    (*l_827) = ((safe_mod_func_uint8_t_u_u(p_11, ((0xCE71925E61EE2B42LL < (safe_mod_func_uint16_t_u_u((safe_rshift_func_int8_t_s_s((((l_1143 = ((*****l_844) = (safe_lshift_func_uint8_t_u_s((l_1194[3] == l_1196), ((p_9 >= (((((safe_rshift_func_int8_t_s_u(l_1144, 5)) , (((*l_1204) = (0x92E7L ^ ((safe_div_func_uint8_t_u_u(((g_712 >= ((*p_10) == l_1202)) == g_433), l_1203[2][1])) == (-1L)))) || 0xB9417E2ED06B6466LL)) ^ 0L) , l_1143) == g_65)) , (*g_984)))))) , (void*)0) == l_1205), p_11)), g_168))) , p_9))) & (*p_12));
                    if ((((void*)0 != (*g_625)) > ((*l_1204) = (safe_lshift_func_int8_t_s_u((((safe_sub_func_uint8_t_u_u(((+0x7CL) , (~(((*l_827) ^= ((void*)0 == &l_1120)) != (safe_mul_func_int8_t_s_s(((((((g_1214 || ((g_418[2][5][0] = p_9) , (((!(**g_558)) , ((((*p_12) > l_1143) , &l_840) != (void*)0)) == p_11))) || p_9) > g_67[4][6]) | (**g_268)) , (void*)0) != (void*)0), p_11))))), 0xEEL)) <= p_9) , 0x78L), 2)))))
                    { /* block id: 477 */
                        uint16_t ***l_1222 = &l_842[0];
                        uint32_t *l_1228 = &g_520;
                        (**g_585) = func_49((p_11 & (((*l_1228) = ((safe_div_func_int64_t_s_s((((((((safe_mul_func_int16_t_s_s((safe_add_func_uint64_t_u_u(l_1120, g_977)), (((((**l_844) != (l_1222 = &l_842[0])) , (safe_sub_func_int16_t_s_s((l_1203[2][1] = 0x80E7L), (p_9 , ((l_1226[8] = l_1225) != l_1225))))) ^ (*g_984)) , g_452[1]))) , (*g_922)) < 0xC4L) == (-4L)) , (void*)0) != (void*)0) ^ 0UL), (**g_558))) < l_1227)) , 1L)), (*****l_1140));
                        if ((*p_12))
                            break;
                    }
                    else
                    { /* block id: 484 */
                        l_1203[0][6] ^= (*p_10);
                    }
                }
                for (g_433 = 0; (g_433 <= 0); g_433 += 1)
                { /* block id: 490 */
                    uint32_t l_1231 = 0x781A2DF5L;
                    uint64_t l_1236 = 0x239CF35AF60CA728LL;
                    int i;
                    for (g_712 = 1; (g_712 <= 5); g_712 += 1)
                    { /* block id: 493 */
                        int i, j;
                        l_864[(g_712 + 2)][(g_433 + 1)] |= (safe_div_func_int32_t_s_s(1L, l_863[g_433]));
                        if (l_1120)
                            break;
                        (*l_827) = l_1231;
                    }
                    l_863[g_433] = (safe_lshift_func_int16_t_s_u(0xBCBDL, (safe_lshift_func_uint16_t_u_u((l_1236 &= ((*g_885) = l_863[g_433])), l_1237))));
                    l_1238 = func_49(((l_1143 = (g_168 = (*g_984))) | p_9), (*****l_1140));
                }
                for (l_1095 = 0; (l_1095 <= 4); l_1095 += 1)
                { /* block id: 507 */
                    int32_t l_1254 = (-1L);
                    int32_t l_1256 = 0xA8A0BAA1L;
                    for (g_104 = 1; (g_104 <= 4); g_104 += 1)
                    { /* block id: 510 */
                        int32_t l_1255[6];
                        int32_t l_1257 = 0x6C45E132L;
                        int i;
                        for (i = 0; i < 6; i++)
                            l_1255[i] = 0x3926D64BL;
                        g_165 &= ((((safe_rshift_func_int8_t_s_u(0x27L, l_1241)) | (p_11 == ((l_1242 = ((*p_12) || (*p_10))) ^ ((void*)0 == &l_1088[2][1])))) , ((l_1255[0] = (safe_rshift_func_uint16_t_u_s(0x40EFL, (safe_lshift_func_uint16_t_u_s((safe_div_func_int64_t_s_s(((((safe_rshift_func_uint16_t_u_u(((safe_sub_func_int16_t_s_s(0xA49AL, p_9)) >= 0xED762D04DCF7FED9LL), 7)) | g_656) == (*l_827)) | l_1253), l_1254)), 0))))) > p_11)) , 4L);
                        g_1258++;
                    }
                }
            }
        }
        for (g_168 = 1; (g_168 <= 4); g_168 += 1)
        { /* block id: 521 */
            uint16_t ** const ****l_1276 = &l_844;
            int32_t l_1278 = (-1L);
            uint8_t *l_1279 = (void*)0;
            uint8_t *l_1280 = &g_977;
            (*l_827) = (safe_lshift_func_uint8_t_u_s((safe_div_func_uint32_t_u_u((safe_lshift_func_int16_t_s_u((((((((((*l_1280) = ((((safe_unary_minus_func_int8_t_s(((p_11 ^ (safe_mul_func_int8_t_s_s(0L, ((((**l_926) = (p_9 | ((g_67[1][6] || ((((p_11 ^ (((safe_add_func_int16_t_s_s((p_9 || ((((***l_841) = 3UL) | 0x1DEAL) < ((((safe_add_func_int32_t_s_s((safe_rshift_func_int8_t_s_s((0xF197L != p_11), 6)), 0x6D950A3DL)) , p_9) , p_11) ^ (*p_12)))), 1L)) & p_9) , g_66)) , (void*)0) == l_1276) != (-9L))) || (*p_10)))) , 0xBF24953D1AB948A7LL) < g_104)))) & g_68))) ^ l_1277) <= l_1278) , (**g_921))) && p_11) , (void*)0) != &p_9) < 65535UL) , g_67[5][0]) || l_1281) | p_11), (**g_268))), (*p_12))), (*g_984)));
            for (g_65 = 3; (g_65 >= 0); g_65 -= 1)
            { /* block id: 528 */
                for (g_989 = 1; (g_989 <= 5); g_989 += 1)
                { /* block id: 531 */
                    return &g_165;
                }
            }
            g_1282 |= ((*l_827) = (*p_10));
            if ((*p_10))
                continue;
            for (g_989 = 4; (g_989 >= 1); g_989 -= 1)
            { /* block id: 540 */
                return p_12;
            }
        }
    }
    return p_12;
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_712 g_612 g_585 g_586 g_165 g_2 g_59 g_180 g_110 g_144 g_450 g_65 g_66 g_451 g_67 g_520 g_63 g_625 g_626 g_418 g_5 g_558 g_559 g_32
 * writes: g_60 g_712 g_97 g_180 g_144 g_433 g_365 g_63 g_165
 */
static uint8_t  func_13(uint32_t  p_14)
{ /* block id: 293 */
    int16_t l_704 = 0xCA28L;
    int32_t l_709 = 0L;
    int32_t l_710[5][2][2] = {{{7L,1L},{0x8333991CL,1L}},{{7L,1L},{0x8333991CL,1L}},{{7L,1L},{0x8333991CL,1L}},{{7L,1L},{0x8333991CL,1L}},{{7L,1L},{0x8333991CL,1L}}};
    uint16_t *l_717[7];
    int32_t *l_718 = &g_144[1][1][0];
    uint64_t *l_729 = &g_365;
    uint8_t *l_733 = (void*)0;
    uint8_t *l_734 = (void*)0;
    uint8_t *l_735[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int8_t ***l_750 = (void*)0;
    int8_t ***l_751 = &g_625;
    int8_t ***l_752 = &g_625;
    int32_t *l_753 = &l_710[0][1][1];
    int32_t l_762 = 0xD21838F4L;
    uint64_t l_763 = 0x64E8E6CE3D4C4803LL;
    int32_t ***l_798 = &g_586;
    int32_t *l_812 = &l_709;
    int32_t *l_813 = (void*)0;
    int32_t *l_814 = &g_165;
    int i, j, k;
    for (i = 0; i < 7; i++)
        l_717[i] = &g_452[1];
    for (g_60 = 0; (g_60 != 36); g_60 = safe_add_func_uint64_t_u_u(g_60, 8))
    { /* block id: 296 */
        int32_t *l_705 = &g_63;
        int32_t *l_706 = &g_165;
        int32_t *l_707 = &g_144[2][1][0];
        int32_t *l_708[7] = {&g_144[2][1][0],&g_144[2][1][0],&g_144[2][1][0],&g_144[2][1][0],&g_144[2][1][0],&g_144[2][1][0],&g_144[2][1][0]};
        int32_t l_711 = 0x8EF98194L;
        uint8_t *l_719 = (void*)0;
        uint8_t *l_720 = &g_180;
        uint64_t *l_730 = (void*)0;
        int i;
        g_712++;
        if (g_712)
            goto lbl_794;
        (*l_707) &= (safe_sub_func_int32_t_s_s((((***g_612) = &l_710[0][1][1]) == (l_718 = func_49(((*l_706) & 65531UL), l_717[4]))), (((++(*l_720)) ^ (safe_mul_func_int8_t_s_s(p_14, (g_110[3] , (safe_add_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_s((l_729 != l_730), l_710[0][1][1])) || 0x966BAD40532ECA87LL), 0x35L)))))) != 0x4F275445L)));
    }
    l_709 |= ((((safe_div_func_int8_t_s_s((g_144[2][0][1] , p_14), (g_180 = 0xDBL))) && (safe_mod_func_uint64_t_u_u((p_14 > p_14), (safe_add_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_u((g_433 = ((g_450 == (safe_sub_func_uint8_t_u_u(((safe_rshift_func_int8_t_s_u(((safe_rshift_func_uint16_t_u_u(((safe_add_func_uint32_t_u_u(((void*)0 != &g_66), ((*l_753) = ((l_750 = (l_751 = l_750)) == l_752)))) , p_14), p_14)) && (*l_753)), g_65)) | p_14), (-1L)))) & p_14)), g_66)), 0UL))))) >= p_14) > p_14);
lbl_794:
    for (l_704 = (-22); (l_704 > 0); l_704 = safe_add_func_int64_t_s_s(l_704, 9))
    { /* block id: 311 */
        int32_t *l_756 = &l_709;
        int32_t *l_757 = &l_710[4][0][1];
        int32_t *l_758 = &g_63;
        int32_t *l_759 = &l_710[3][1][0];
        int32_t *l_760 = &l_709;
        int32_t *l_761[1][7][9] = {{{(void*)0,&g_165,&g_165,(void*)0,&l_710[4][1][1],&g_144[2][1][0],&l_710[4][1][1],(void*)0,&g_165},{&g_5,&l_710[0][1][1],&g_144[2][1][1],&g_2,&g_2,&g_2,&g_144[2][1][1],&l_710[0][1][1],&g_5},{&g_165,(void*)0,&l_710[4][1][1],&g_144[2][1][0],&l_710[4][1][1],(void*)0,&g_165,&g_165,(void*)0},{&g_5,&g_2,&g_144[2][1][1],&g_2,&g_5,&g_5,&g_144[2][1][1],(void*)0,&g_2},{&g_144[2][1][0],&g_144[0][1][1],&g_144[2][1][0],&g_63,&l_710[4][1][1],&l_710[4][1][1],&g_63,&g_144[2][1][0],&g_144[0][1][1]},{&g_2,&g_2,&g_144[2][1][1],&l_710[0][1][1],&g_5,&g_5,&g_5,&g_63,&g_5},{&g_165,(void*)0,&g_63,&g_63,(void*)0,&g_165,&l_710[4][1][1],&g_165,(void*)0}}};
        int64_t **l_786 = (void*)0;
        int i, j, k;
        --l_763;
        if ((*l_753))
        { /* block id: 313 */
            (*l_756) ^= ((~p_14) >= p_14);
        }
        else
        { /* block id: 315 */
            uint32_t l_785 = 0xD8F5DB09L;
            uint16_t **l_793 = (void*)0;
            (*l_758) ^= ((safe_lshift_func_uint16_t_u_s((safe_add_func_uint32_t_u_u((safe_div_func_uint8_t_u_u(((safe_add_func_uint8_t_u_u(((safe_sub_func_int64_t_s_s((p_14 , ((safe_rshift_func_uint16_t_u_u(((safe_mul_func_int16_t_s_s(((((safe_add_func_int8_t_s_s((((l_785 ^ ((void*)0 == l_786)) >= p_14) != (*l_753)), (safe_rshift_func_uint16_t_u_u((p_14 && ((((((safe_mod_func_uint64_t_u_u(((((*l_729) = ((((void*)0 != l_793) != g_451) < 0x32EAL)) < g_67[1][6]) > g_520), 0x19F40F78660AB0B1LL)) >= 1L) || l_785) <= p_14) >= g_65) > 0x849FAC22516CF802LL)), 10)))) && l_785) || l_785) < p_14), 0L)) & l_785), p_14)) == 0L)), p_14)) | p_14), p_14)) <= p_14), (*l_753))), (*l_756))), p_14)) != l_785);
        }
    }
    (*l_814) = ((*l_812) = (~((safe_sub_func_int64_t_s_s(((void*)0 == l_798), ((~p_14) >= (((safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s((safe_add_func_uint32_t_u_u(g_63, (safe_rshift_func_uint8_t_u_s(p_14, 1)))), ((*l_753) = (((**l_752) == ((0xD0DAB41BL && (safe_lshift_func_int8_t_s_s((((-10L) >= ((safe_rshift_func_uint8_t_u_s(((0xD52AL <= 0xA015L) || g_418[0][3][7]), 0)) < 0x3B548DBEL)) | p_14), (*l_753)))) , (void*)0)) || 252UL)))), g_5)) , (void*)0) != (*g_558))))) || g_32)));
    return g_450;
}


/* ------------------------------------------ */
/* 
 * reads : g_32 g_2 g_165
 * writes: g_32 g_165 g_144
 */
static uint32_t  func_15(int16_t  p_16, int32_t * p_17, uint32_t  p_18)
{ /* block id: 283 */
    uint8_t l_697 = 1UL;
    int32_t * const l_700 = &g_165;
    for (g_32 = 28; (g_32 > 9); g_32 = safe_sub_func_uint32_t_u_u(g_32, 6))
    { /* block id: 286 */
        int32_t *l_693 = &g_144[2][1][0];
        int32_t *l_694 = &g_63;
        int32_t *l_695 = &g_165;
        int32_t *l_696[2];
        int32_t **l_701 = &l_696[1];
        int i;
        for (i = 0; i < 2; i++)
            l_696[i] = &g_144[2][1][0];
        l_697++;
        (*l_701) = l_700;
        (**l_701) = (*p_17);
    }
    return (*l_700);
}


/* ------------------------------------------ */
/* 
 * reads : g_586
 * writes: g_97
 */
static int16_t  func_26(int8_t  p_27, const uint32_t  p_28, uint16_t * const  p_29, uint16_t * p_30)
{ /* block id: 278 */
    int32_t *l_687 = (void*)0;
    l_687 = l_687;
    (*g_586) = l_687;
    return p_27;
}


/* ------------------------------------------ */
/* 
 * reads : g_104 g_267 g_144 g_268 g_269 g_79 g_2 g_68 g_168 g_97 g_60 g_63 g_5 g_365 g_110 g_180 g_113 g_114 g_66 g_418 g_165 g_433 g_65 g_451 g_59 g_432 g_520 g_544 g_558 g_452 g_559 g_585 g_625 g_545 g_485 g_450 g_67 g_656 g_395 g_682
 * writes: g_104 g_110 g_63 g_97 g_60 g_68 g_66 g_365 g_395 g_180 g_144 g_168 g_433 g_485 g_520 g_113 g_65 g_114 g_607 g_612 g_625 g_418 g_165 g_67 g_656 g_682
 */
static uint16_t * func_35(int32_t * p_36, int64_t  p_37)
{ /* block id: 99 */
    uint32_t *l_252 = (void*)0;
    uint32_t *l_253 = &g_104;
    int8_t *l_260[3][1][2] = {{{&g_168,&g_168}},{{&g_168,&g_168}},{{&g_168,&g_168}}};
    int32_t l_261 = 7L;
    int32_t l_262 = 0x303509A9L;
    uint16_t *l_272[10][9][2] = {{{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68}},{{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,&g_68}},{{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68}},{{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68}},{{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68}},{{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68}},{{&g_68,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68}},{{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68}},{{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{&g_68,&g_68},{&g_68,(void*)0}},{{&g_68,(void*)0},{&g_68,&g_68},{&g_68,&g_68},{(void*)0,&g_68},{(void*)0,&g_68},{(void*)0,(void*)0},{&g_68,&g_68},{&g_68,(void*)0},{(void*)0,&g_68}}};
    uint16_t **l_271[4][4] = {{&l_272[5][0][1],&l_272[7][3][0],&l_272[5][8][0],&l_272[7][3][0]},{&l_272[5][0][1],&l_272[5][8][0],&l_272[5][0][1],(void*)0},{&l_272[7][3][0],&l_272[7][3][0],(void*)0,(void*)0},{&l_272[5][8][0],&l_272[5][8][0],&l_272[3][5][0],&l_272[7][3][0]}};
    uint16_t ** const *l_270 = &l_271[1][1];
    uint16_t ** const **l_273 = &l_270;
    int32_t l_278 = 0L;
    int8_t l_300 = (-1L);
    uint16_t ****l_355 = (void*)0;
    int32_t l_373 = 0xF7F3574DL;
    const int8_t *l_384 = (void*)0;
    int8_t l_396 = 2L;
    const int32_t l_419[5][7][2] = {{{0x43E9282CL,(-1L)},{0x5401A2F0L,1L},{1L,0x5401A2F0L},{(-1L),0x43E9282CL},{(-1L),0x5401A2F0L},{1L,1L},{0x5401A2F0L,(-1L)}},{{0x43E9282CL,(-1L)},{0x5401A2F0L,1L},{1L,0x5401A2F0L},{(-1L),0x43E9282CL},{(-1L),0x5401A2F0L},{1L,1L},{0x5401A2F0L,(-1L)}},{{0x43E9282CL,(-1L)},{0x5401A2F0L,1L},{1L,0x5401A2F0L},{(-1L),0x43E9282CL},{(-1L),0x5401A2F0L},{1L,1L},{0x5401A2F0L,(-1L)}},{{0x43E9282CL,(-1L)},{0x5401A2F0L,1L},{1L,0x5401A2F0L},{(-1L),0x43E9282CL},{(-1L),0x5401A2F0L},{1L,1L},{0x5401A2F0L,(-1L)}},{{0x43E9282CL,(-1L)},{0x5401A2F0L,1L},{1L,0x5401A2F0L},{(-1L),0x43E9282CL},{(-1L),0x5401A2F0L},{1L,1L},{0x5401A2F0L,(-1L)}}};
    int8_t l_576 = 0x66L;
    int32_t ****l_609 = &g_585;
    int32_t *****l_608 = &l_609;
    int32_t ****l_611 = &g_585;
    int32_t *****l_610[5][10][5] = {{{&l_611,&l_611,&l_611,&l_611,(void*)0},{&l_611,(void*)0,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611},{(void*)0,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,(void*)0,&l_611}},{{&l_611,&l_611,(void*)0,(void*)0,&l_611},{&l_611,(void*)0,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,(void*)0,&l_611},{&l_611,&l_611,&l_611,(void*)0,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,(void*)0,&l_611},{&l_611,(void*)0,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,(void*)0,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611}},{{&l_611,&l_611,&l_611,(void*)0,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,(void*)0,&l_611,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,(void*)0,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611}}};
    uint64_t *l_616 = &g_365;
    uint64_t *l_619[7] = {&g_395,&g_395,&g_395,&g_395,&g_395,&g_395,&g_395};
    int8_t ***l_627 = (void*)0;
    int8_t ***l_628 = &g_625;
    uint16_t l_629[3][2];
    int16_t *l_630 = (void*)0;
    int16_t *l_631 = &g_418[0][2][7];
    int64_t *l_654 = &g_67[7][5];
    int64_t *l_655 = &g_656;
    uint16_t l_657 = 1UL;
    const uint32_t l_658 = 3UL;
    int32_t l_659[6] = {0xC53ADA38L,0xBE7A6BC3L,0xBE7A6BC3L,0xC53ADA38L,0xBE7A6BC3L,0xBE7A6BC3L};
    uint32_t l_676 = 0x5594F143L;
    int16_t *l_681[8][6][5] = {{{(void*)0,&g_418[0][2][1],&g_418[2][0][6],&g_418[0][3][7],(void*)0},{&g_418[0][3][7],(void*)0,&g_66,&g_418[0][4][1],(void*)0},{&g_418[2][0][7],(void*)0,&g_418[0][3][7],&g_66,&g_418[2][0][6]},{&g_66,&g_418[0][3][7],&g_66,&g_418[2][0][6],&g_66},{&g_418[0][3][7],(void*)0,&g_418[0][3][7],&g_418[2][0][6],(void*)0},{&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66,&g_66}},{{&g_418[0][3][7],&g_66,&g_66,&g_418[0][4][1],&g_66},{&g_418[0][3][7],&g_418[0][3][7],(void*)0,(void*)0,(void*)0},{&g_66,(void*)0,&g_66,&g_66,(void*)0},{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66},{&g_66,(void*)0,(void*)0,&g_418[2][0][6],&g_66},{&g_66,(void*)0,&g_418[0][3][7],&g_66,(void*)0}},{{&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66,&g_66},{&g_418[0][3][7],(void*)0,(void*)0,&g_418[0][4][1],&g_418[2][0][6]},{&g_66,&g_418[0][3][7],&g_66,&g_418[0][3][7],(void*)0},{&g_66,(void*)0,&g_66,&g_418[0][4][1],&g_418[0][3][7]},{&g_418[0][3][7],(void*)0,&g_418[0][3][7],&g_66,&g_418[2][0][6]},{&g_66,(void*)0,&g_66,&g_66,&g_66}},{{&g_418[0][3][7],(void*)0,&g_418[0][3][7],&g_418[2][0][6],&g_418[0][5][8]},{&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66,&g_66},{&g_418[0][3][7],(void*)0,&g_66,&g_66,&g_418[2][0][6]},{&g_418[0][3][7],&g_418[0][3][7],&g_66,(void*)0,&g_418[0][3][7]},{&g_66,(void*)0,&g_66,&g_418[0][4][1],(void*)0},{&g_418[2][0][7],(void*)0,&g_418[0][3][7],&g_66,&g_418[2][0][6]}},{{&g_66,&g_418[0][3][7],&g_66,&g_418[2][0][6],&g_66},{&g_418[0][3][7],(void*)0,&g_418[0][3][7],&g_418[2][0][6],(void*)0},{&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66,&g_66},{&g_418[0][3][7],&g_66,&g_66,&g_418[0][4][1],&g_66},{&g_418[0][3][7],&g_418[0][3][7],(void*)0,(void*)0,(void*)0},{&g_66,(void*)0,&g_66,&g_66,(void*)0}},{{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66},{&g_66,(void*)0,(void*)0,&g_418[2][0][6],&g_66},{&g_66,(void*)0,&g_418[0][3][7],&g_66,(void*)0},{&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66,&g_66},{&g_418[0][3][7],(void*)0,(void*)0,&g_418[0][4][1],&g_418[2][0][6]},{&g_66,&g_418[0][3][7],&g_66,&g_418[0][3][7],(void*)0}},{{&g_66,(void*)0,&g_66,&g_418[0][4][1],&g_418[0][3][7]},{&g_418[0][3][7],(void*)0,&g_418[0][3][7],&g_66,&g_418[2][0][6]},{&g_66,(void*)0,&g_66,&g_66,&g_66},{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66},{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_66,(void*)0},{&g_418[0][3][7],(void*)0,&g_66,&g_418[0][3][7],&g_66}},{{&g_418[1][0][5],&g_418[0][3][7],&g_66,&g_66,(void*)0},{&g_66,&g_418[0][5][7],&g_418[0][3][7],&g_418[0][3][7],&g_66},{&g_66,&g_418[0][3][7],&g_418[0][3][7],&g_66,&g_66},{&g_66,&g_66,&g_418[0][0][7],&g_66,&g_66},{&g_418[1][0][5],&g_418[0][5][7],&g_418[1][0][1],&g_66,&g_418[0][3][7]},{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_66,(void*)0}}};
    uint16_t *l_683 = &l_657;
    uint16_t *l_684 = &l_629[0][0];
    uint16_t *l_685 = &g_451;
    int i, j, k;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
            l_629[i][j] = 0x4ED8L;
    }
    if (((safe_add_func_uint8_t_u_u(((safe_div_func_int16_t_s_s(((((p_37 > (safe_add_func_uint16_t_u_u(((safe_unary_minus_func_uint32_t_u(((*l_253)++))) < (safe_mod_func_int8_t_s_s(((safe_div_func_int8_t_s_s((l_261 = (-8L)), (l_262 , (safe_lshift_func_uint16_t_u_u((l_262 = ((safe_mul_func_int8_t_s_s((g_267[3] == ((*l_273) = l_270)), (g_144[2][1][0] || (safe_add_func_int8_t_s_s(((void*)0 != (*g_268)), (safe_add_func_uint16_t_u_u(p_37, (**g_268)))))))) || l_262)), p_37))))) & p_37), l_278))), p_37))) > g_2) & 4UL) || p_37), 3UL)) , l_261), l_278)) != p_37))
    { /* block id: 104 */
        uint16_t *l_279[10];
        int i;
        for (i = 0; i < 10; i++)
            l_279[i] = &g_68;
        return l_279[9];
    }
    else
    { /* block id: 106 */
        uint32_t l_280[10] = {0x8F8CD498L,0UL,0x8F8CD498L,0xCB02DF14L,0xCB02DF14L,0x8F8CD498L,0UL,0x8F8CD498L,0xCB02DF14L,0xCB02DF14L};
        int64_t *l_284 = &g_110[3];
        int32_t **l_321 = &g_97;
        int32_t ***l_320 = &l_321;
        uint32_t l_328[5] = {0xEF1E78B4L,0xEF1E78B4L,0xEF1E78B4L,0xEF1E78B4L,0xEF1E78B4L};
        int32_t *l_361 = (void*)0;
        const int8_t *l_382 = &g_168;
        int8_t *l_386 = (void*)0;
        int32_t l_429 = 0x15D4A3BAL;
        int32_t l_430[9];
        int8_t l_431 = 0x41L;
        uint16_t *l_505 = &g_450;
        uint32_t *l_532 = (void*)0;
        const uint16_t *l_540 = &g_452[1];
        const uint16_t ** const l_539 = &l_540;
        const uint16_t ** const *l_538 = &l_539;
        const uint16_t ** const **l_537 = &l_538;
        const uint16_t ** const ***l_536 = &l_537;
        int16_t l_548 = 0xC21DL;
        int i;
        for (i = 0; i < 9; i++)
            l_430[i] = 2L;
        if (((*g_97) = ((((*l_284) = (l_280[0] != (safe_sub_func_int64_t_s_s(g_68, (~p_37))))) <= ((safe_div_func_uint32_t_u_u(5UL, (safe_unary_minus_func_uint64_t_u((((*l_253) = p_37) | (safe_lshift_func_uint16_t_u_u((p_37 , 65535UL), ((safe_add_func_uint64_t_u_u((((+g_144[2][1][0]) , (safe_add_func_uint8_t_u_u(((g_144[2][1][1] && 8L) > l_280[5]), g_168))) < 8L), p_37)) , p_37)))))))) < l_280[8])) > (-1L))))
        { /* block id: 110 */
            int32_t **l_295 = &g_97;
            uint16_t ***l_303 = &l_271[1][0];
            (*l_295) = &l_262;
            for (g_60 = (-4); (g_60 < 27); g_60++)
            { /* block id: 114 */
                int32_t *l_305 = &g_63;
                int32_t **l_304 = &l_305;
                for (p_37 = 0; (p_37 >= 17); p_37++)
                { /* block id: 117 */
                    (**l_295) = ((g_68++) != p_37);
                    (**l_295) = (l_303 != (void*)0);
                    (**l_295) |= 1L;
                }
                (**l_295) &= ((void*)0 != (**l_273));
                if ((*p_36))
                    break;
                (*l_304) = ((*l_295) = p_36);
            }
            for (g_60 = 0; (g_60 >= 9); g_60 = safe_add_func_uint16_t_u_u(g_60, 5))
            { /* block id: 130 */
                const uint8_t l_308 = 1UL;
                int32_t *** const l_322 = (void*)0;
                int16_t *l_325 = (void*)0;
                int16_t *l_326 = &g_66;
                const int8_t l_327 = 0xEEL;
                if ((l_308 | (((**l_295) , (safe_add_func_uint8_t_u_u(4UL, (safe_unary_minus_func_uint64_t_u((safe_div_func_uint32_t_u_u((((safe_mod_func_uint8_t_u_u((safe_add_func_int64_t_s_s((((*l_284) = p_37) ^ (safe_mul_func_int8_t_s_s((((l_280[0] || (l_320 == l_322)) && (((safe_mul_func_int16_t_s_s((-9L), ((((((*l_326) = p_37) | p_37) , l_327) || 0xD0L) == 5L))) <= g_168) | 0x98L)) , (-2L)), l_261))), 0x10B4DAB2D90A153BLL)), g_79)) , 0UL) & p_37), (*g_97)))))))) & 0xEC57L)))
                { /* block id: 133 */
                    (*l_321) = p_36;
                }
                else
                { /* block id: 135 */
                    uint16_t *****l_356 = &l_355;
                    int32_t l_357[10][3][1] = {{{0x93390E5CL},{0x02E67E09L},{0x93390E5CL}},{{0xB9C661C2L},{(-1L)},{0xB9C661C2L}},{{0x93390E5CL},{0x02E67E09L},{0x93390E5CL}},{{0xB9C661C2L},{(-1L)},{0xB9C661C2L}},{{0x93390E5CL},{0x02E67E09L},{0x93390E5CL}},{{0xB9C661C2L},{(-1L)},{0xB9C661C2L}},{{0x93390E5CL},{0x02E67E09L},{0x93390E5CL}},{{0xB9C661C2L},{(-1L)},{0xB9C661C2L}},{{0x93390E5CL},{0x02E67E09L},{0x93390E5CL}},{{0xB9C661C2L},{(-1L)},{0xB9C661C2L}}};
                    int32_t *l_358[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i, j, k;
                    if (l_328[2])
                        break;
                    l_261 = ((*p_36) = (safe_div_func_int64_t_s_s(((!p_37) ^ (safe_rshift_func_int8_t_s_s((safe_mul_func_int8_t_s_s(1L, ((((safe_rshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s(((((+((g_2 > p_37) && 0xF16202968CAF494BLL)) || (++g_68)) > (safe_mul_func_int16_t_s_s((safe_sub_func_uint8_t_u_u((safe_add_func_int32_t_s_s((-4L), (safe_mod_func_int64_t_s_s(((((*l_356) = ((**l_295) , ((1L <= (safe_add_func_int64_t_s_s((safe_lshift_func_uint8_t_u_u(p_37, 3)), 4UL))) , l_355))) != (void*)0) || 1UL), g_5)))), p_37)), p_37))) , g_144[2][1][0]), l_357[6][2][0])), 7)) , (*g_97)) & 0xA61598BFL) | p_37))), 3))), (-1L))));
                    (**l_320) = (*l_295);
                }
            }
        }
        else
        { /* block id: 144 */
            int32_t l_393[9][5][5] = {{{0x0BC243EEL,0x0BC243EEL,(-3L),0x0BC243EEL,0x0BC243EEL},{0xE08397DEL,0L,0xE08397DEL,0xE08397DEL,0L},{0x0BC243EEL,(-1L),(-1L),0x0BC243EEL,(-1L)},{0L,0L,6L,0L,0L},{(-1L),0x0BC243EEL,(-1L),(-1L),0x0BC243EEL}},{{0L,0xE08397DEL,0xE08397DEL,0L,0xE08397DEL},{0x0BC243EEL,0x0BC243EEL,(-3L),0x0BC243EEL,0x0BC243EEL},{0xE08397DEL,0L,0xE08397DEL,0xE08397DEL,0L},{0x0BC243EEL,(-1L),(-1L),0x0BC243EEL,(-1L)},{0L,0L,6L,0L,0L}},{{(-1L),0x0BC243EEL,(-1L),(-1L),0x0BC243EEL},{0L,0xE08397DEL,0xE08397DEL,0L,0xE08397DEL},{0x0BC243EEL,0x0BC243EEL,(-3L),0x0BC243EEL,0x0BC243EEL},{0xE08397DEL,0L,0xE08397DEL,0xE08397DEL,0L},{0x0BC243EEL,(-1L),(-1L),0x0BC243EEL,(-1L)}},{{0L,0L,6L,0L,0L},{(-1L),0x0BC243EEL,(-1L),(-1L),0x0BC243EEL},{0L,0xE08397DEL,0xE08397DEL,0L,0xE08397DEL},{0x0BC243EEL,0x0BC243EEL,(-3L),0x0BC243EEL,0x0BC243EEL},{0xE08397DEL,0L,6L,6L,0xE08397DEL}},{{(-1L),(-3L),(-3L),(-1L),(-3L)},{0xE08397DEL,0xE08397DEL,0L,0xE08397DEL,0xE08397DEL},{(-3L),(-1L),(-3L),(-3L),(-1L)},{0xE08397DEL,6L,6L,0xE08397DEL,6L},{(-1L),(-1L),0x0BC243EEL,(-1L),(-1L)}},{{6L,0xE08397DEL,6L,6L,0xE08397DEL},{(-1L),(-3L),(-3L),(-1L),(-3L)},{0xE08397DEL,0xE08397DEL,0L,0xE08397DEL,0xE08397DEL},{(-3L),(-1L),(-3L),(-3L),(-1L)},{0xE08397DEL,6L,6L,0xE08397DEL,6L}},{{(-1L),(-1L),0x0BC243EEL,(-1L),(-1L)},{6L,0xE08397DEL,6L,6L,0xE08397DEL},{(-1L),(-3L),(-3L),(-1L),(-3L)},{0xE08397DEL,0xE08397DEL,0L,0xE08397DEL,0xE08397DEL},{(-3L),(-1L),(-3L),(-3L),(-1L)}},{{0xE08397DEL,6L,6L,0xE08397DEL,6L},{(-1L),(-1L),0x0BC243EEL,(-1L),(-1L)},{6L,0xE08397DEL,6L,6L,0xE08397DEL},{(-1L),(-3L),(-3L),(-1L),(-3L)},{0xE08397DEL,0xE08397DEL,0L,0xE08397DEL,0xE08397DEL}},{{(-3L),(-1L),(-3L),(-3L),(-1L)},{0xE08397DEL,6L,6L,0xE08397DEL,6L},{(-1L),(-1L),0x0BC243EEL,(-1L),(-1L)},{6L,0xE08397DEL,6L,6L,0xE08397DEL},{(-1L),(-3L),(-3L),(-1L),(-3L)}}};
            int32_t l_394 = 7L;
            uint16_t * const l_449[8][1] = {{(void*)0},{&g_450},{(void*)0},{&g_450},{(void*)0},{&g_450},{(void*)0},{&g_450}};
            uint16_t * const *l_448 = &l_449[5][0];
            uint16_t * const **l_447 = &l_448;
            uint16_t * const ***l_446[8] = {&l_447,&l_447,&l_447,&l_447,&l_447,&l_447,&l_447,&l_447};
            int32_t l_468 = 1L;
            uint8_t *l_501 = &g_180;
            uint16_t *l_547 = &g_452[1];
            int i, j, k;
            for (p_37 = 0; (p_37 <= (-14)); p_37--)
            { /* block id: 147 */
                int32_t l_421 = 0x3145D303L;
                int32_t l_423[4][7][7] = {{{0x0EE3C251L,1L,0x98344D15L,0xAE7C292EL,0x97BE0394L,(-2L),(-2L)},{1L,0x9E2CF853L,0x0EE3C251L,0x9E2CF853L,1L,2L,(-2L)},{0xC4DD5E1CL,0xE676C1A8L,0x0D1F6533L,1L,(-2L),0x98344D15L,0x030C408FL},{0x0D1F6533L,2L,0xAE7C292EL,(-10L),(-10L),1L,0x58B9A117L},{0xC4DD5E1CL,1L,(-1L),0x58B9A117L,(-1L),1L,0xC4DD5E1CL},{1L,1L,(-1L),(-2L),0x9E2CF853L,0xC4DD5E1CL,0x0EE3C251L},{0x0EE3C251L,2L,0x12653692L,(-1L),0x58B9A117L,1L,2L}},{{(-2L),2L,1L,0x9E2CF853L,0x0EE3C251L,0x9E2CF853L,1L},{0x97BE0394L,0x97BE0394L,0x12653692L,0x9E2CF853L,5L,0xE676C1A8L,0x58B9A117L},{0x0EE3C251L,5L,(-2L),1L,0x4BA70878L,2L,(-1L)},{(-10L),0xAE7C292EL,0xE676C1A8L,(-1L),5L,1L,5L},{0x030C408FL,0x4BA70878L,0x4BA70878L,0x030C408FL,0x0EE3C251L,1L,(-10L)},{(-1L),0xE676C1A8L,0xAE7C292EL,(-10L),0x030C408FL,2L,0x9E2CF853L},{1L,(-2L),5L,0x0EE3C251L,0x97BE0394L,0xE676C1A8L,(-10L)}},{{0x9E2CF853L,0x12653692L,0x97BE0394L,0x97BE0394L,0x12653692L,0x9E2CF853L,5L},{0x9E2CF853L,1L,2L,(-2L),0x58B9A117L,1L,(-1L)},{1L,(-10L),(-1L),0x4BA70878L,(-1L),0x98344D15L,0x58B9A117L},{(-1L),1L,0x0EE3C251L,0x0D1F6533L,0x0D1F6533L,0x0EE3C251L,1L},{0x030C408FL,0x12653692L,0x0EE3C251L,0x98344D15L,0xC478514BL,0x0D1F6533L,2L},{(-10L),(-2L),(-1L),0xE676C1A8L,1L,0xAE7C292EL,0x4BA70878L},{0x0EE3C251L,0xE676C1A8L,2L,0x98344D15L,(-1L),(-1L),0x98344D15L}},{{0x97BE0394L,0x4BA70878L,0x97BE0394L,0x0D1F6533L,(-1L),(-1L),0x030C408FL},{(-2L),0xAE7C292EL,5L,0x4BA70878L,1L,0x12653692L,1L},{0x4BA70878L,5L,0xAE7C292EL,(-2L),0xC478514BL,(-1L),(-1L)},{0x0D1F6533L,0x97BE0394L,0x4BA70878L,0x97BE0394L,0x0D1F6533L,(-1L),(-1L)},{0x98344D15L,2L,0xE676C1A8L,0x0EE3C251L,(-1L),0xAE7C292EL,1L},{0xE676C1A8L,(-1L),(-2L),(-10L),0x58B9A117L,0x0D1F6533L,0x030C408FL},{0x98344D15L,0x0EE3C251L,0x12653692L,0x030C408FL,0x12653692L,0x0EE3C251L,0x98344D15L}}};
                uint16_t * const *l_444[4][4][6] = {{{&l_272[3][7][0],&l_272[7][3][0],(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[8][6][0],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]},{(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0],&l_272[3][7][0],&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[3][2][1],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]}},{{&l_272[3][7][0],&l_272[7][3][0],(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[8][6][0],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]},{(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0],&l_272[3][7][0],&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[3][2][1],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]}},{{&l_272[3][7][0],&l_272[7][3][0],(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[8][6][0],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]},{(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0],&l_272[3][7][0],&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[3][2][1],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]}},{{&l_272[3][7][0],&l_272[7][3][0],(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[8][6][0],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]},{(void*)0,&l_272[7][3][0],(void*)0,&l_272[7][3][0],&l_272[3][7][0],&l_272[7][3][0]},{&l_272[7][3][0],&l_272[7][3][0],&l_272[3][2][1],&l_272[7][3][0],&l_272[7][3][0],&l_272[7][3][0]}}};
                uint16_t * const **l_443 = &l_444[0][1][1];
                uint16_t * const ***l_442[6][3] = {{&l_443,&l_443,&l_443},{&l_443,&l_443,&l_443},{&l_443,&l_443,&l_443},{&l_443,&l_443,&l_443},{&l_443,&l_443,&l_443},{&l_443,&l_443,&l_443}};
                uint16_t ***l_477 = &l_271[1][1];
                int i, j, k;
                for (g_60 = 0; (g_60 <= 1); g_60 += 1)
                { /* block id: 150 */
                    int16_t l_392[5][9][5] = {{{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L}},{{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL}},{{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3D63L,0x3B0AL},{0L,0L,0x0380L,0x17CDL,0x0380L},{2L,2L,0x3B0AL,0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L}},{{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)}},{{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L},{0x3B0AL,0x3B0AL,(-7L),0x3B27L,(-7L)},{0x0380L,0x0380L,0x0643L,2L,0x0643L}}};
                    int i, j, k;
                    (**l_320) = l_361;
                    if (l_262)
                        break;
                    if ((*p_36))
                        break;
                    for (g_68 = 0; (g_68 <= 0); g_68 += 1)
                    { /* block id: 156 */
                        uint64_t *l_364 = &g_365;
                        const int8_t **l_383[2][9][8] = {{{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382}},{{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382},{&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382,&l_382}}};
                        int8_t **l_385 = (void*)0;
                        int32_t l_397 = 0xAE6D1EB2L;
                        int i, j, k;
                        (*p_36) = (safe_div_func_int32_t_s_s(((p_37 && (((g_180 = (((l_261 = ((--(*l_364)) , ((p_37 & ((safe_rshift_func_int8_t_s_u((((((!((((safe_sub_func_int8_t_s_s((l_373 , ((g_395 = ((safe_mod_func_int16_t_s_s((safe_add_func_uint16_t_u_u((l_394 = (safe_mod_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(((((void*)0 != l_260[(g_68 + 2)][g_68][g_60]) , ((l_384 = l_382) != (l_386 = &g_168))) || 0x0EL), (safe_unary_minus_func_uint32_t_u(((safe_div_func_uint64_t_u_u(((!(safe_unary_minus_func_uint32_t_u(l_392[2][2][1]))) > l_393[6][3][2]), l_392[3][1][3])) < l_392[1][7][1]))))), (*p_36)))), (-8L))), 0x9040L)) ^ l_392[3][2][3])) == p_37)), g_2)) & g_110[1]) != 0x40L) || g_365)) , 0xC0AECC13L) , p_37) , l_393[6][3][2]) | 0x0154L), g_180)) != l_393[8][0][0])) < p_37))) , p_37) < p_37)) && g_110[0]) && 1L)) ^ g_144[0][0][1]), l_396));
                        (*l_321) = g_113[(g_68 + 6)][(g_68 + 2)][g_60];
                        (**l_320) = &g_144[2][1][0];
                        l_394 = l_397;
                    }
                }
                if ((g_114 >= ((~1UL) >= 0x654EL)))
                { /* block id: 170 */
                    int64_t l_399 = 9L;
                    int16_t *l_417[4][7][6] = {{{&g_418[0][3][7],(void*)0,(void*)0,(void*)0,&g_418[0][3][7],&g_418[1][0][4]},{(void*)0,&g_418[0][3][7],&g_418[0][3][7],(void*)0,&g_418[0][4][1],&g_418[0][4][1]},{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[1][0][4]},{(void*)0,&g_418[0][3][7],(void*)0,&g_418[0][5][6],(void*)0,&g_418[0][3][7]},{&g_418[0][3][7],&g_418[1][0][4],(void*)0,&g_418[0][3][7],(void*)0,&g_418[0][5][6]},{(void*)0,&g_418[0][3][7],(void*)0,&g_418[1][0][4],&g_418[0][3][7],&g_418[0][3][7]},{&g_418[0][3][7],&g_418[0][3][7],(void*)0,&g_418[0][4][1],&g_418[0][4][1],(void*)0}},{{&g_418[0][3][7],&g_418[0][3][7],(void*)0,&g_418[1][0][4],&g_418[0][3][7],(void*)0},{(void*)0,(void*)0,&g_418[0][3][7],&g_418[0][3][7],&g_418[0][4][1],(void*)0},{&g_418[0][3][7],(void*)0,&g_418[0][3][7],&g_418[0][5][6],&g_418[0][3][7],(void*)0},{(void*)0,&g_418[0][5][6],(void*)0,&g_418[0][3][7],(void*)0,(void*)0},{&g_418[0][3][7],(void*)0,(void*)0,(void*)0,(void*)0,&g_418[0][3][7]},{(void*)0,&g_418[0][5][6],(void*)0,(void*)0,&g_418[0][3][7],&g_418[0][5][6]},{&g_418[0][3][7],(void*)0,(void*)0,(void*)0,&g_418[0][4][1],&g_418[0][3][7]}},{{&g_418[0][3][7],(void*)0,(void*)0,(void*)0,&g_418[0][3][7],&g_418[1][0][4]},{(void*)0,&g_418[0][3][7],&g_418[0][3][7],(void*)0,&g_418[0][4][1],&g_418[0][4][1]},{&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[1][0][4]},{(void*)0,&g_418[0][3][7],(void*)0,&g_418[0][5][6],(void*)0,&g_418[0][3][7]},{&g_418[0][3][7],&g_418[1][0][4],(void*)0,&g_418[0][3][7],(void*)0,&g_418[0][5][6]},{(void*)0,&g_418[0][3][7],(void*)0,&g_418[1][0][4],&g_418[0][3][7],&g_418[0][3][7]},{&g_418[0][3][7],&g_418[0][3][7],(void*)0,&g_418[0][4][1],&g_418[0][4][1],(void*)0}},{{&g_418[0][3][7],&g_418[0][3][7],(void*)0,&g_418[1][0][4],&g_418[0][3][7],&g_418[0][3][7]},{(void*)0,&g_418[0][3][7],&g_418[1][0][4],&g_418[0][4][1],&g_418[0][3][7],(void*)0},{(void*)0,(void*)0,&g_418[1][0][4],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7]},{&g_418[0][3][7],&g_418[0][3][7],(void*)0,(void*)0,&g_418[0][3][7],(void*)0},{(void*)0,&g_418[0][3][7],(void*)0,(void*)0,&g_418[0][3][7],(void*)0},{(void*)0,&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7],&g_418[0][3][7]},{&g_418[0][3][7],(void*)0,(void*)0,(void*)0,&g_418[0][3][7],&g_418[0][4][1]}}};
                    int32_t l_422[9];
                    uint16_t *** const l_441 = (void*)0;
                    uint16_t *** const *l_440 = &l_441;
                    uint32_t l_469 = 0xECBBD2CCL;
                    uint16_t ***l_476 = &l_271[1][1];
                    uint8_t *l_482 = &g_433;
                    int i, j, k;
                    for (i = 0; i < 9; i++)
                        l_422[i] = (-1L);
                    if (l_399)
                    { /* block id: 171 */
                        int16_t *l_400[7][7] = {{&g_66,(void*)0,&g_66,(void*)0,&g_66,&g_66,(void*)0},{&g_66,&g_66,&g_66,(void*)0,(void*)0,&g_66,&g_66},{(void*)0,&g_66,&g_66,&g_66,&g_66,(void*)0,&g_66},{&g_66,(void*)0,(void*)0,&g_66,&g_66,&g_66,(void*)0},{&g_66,&g_66,(void*)0,&g_66,(void*)0,&g_66,&g_66},{&g_66,(void*)0,&g_66,(void*)0,&g_66,&g_66,(void*)0},{&g_66,&g_66,&g_66,(void*)0,(void*)0,&g_66,&g_66}};
                        uint8_t *l_420[7] = {&g_180,&g_180,&g_180,&g_180,&g_180,&g_180,&g_180};
                        int i, j;
                        (*p_36) |= ((g_66 &= p_37) > ((((safe_div_func_int16_t_s_s((safe_div_func_int16_t_s_s((l_422[6] = ((safe_mul_func_uint8_t_u_u((g_180 ^= (l_421 = ((safe_lshift_func_uint16_t_u_s(((safe_mod_func_uint32_t_u_u((safe_div_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((p_37 ^ (((g_168 &= l_393[7][4][1]) , p_37) ^ ((l_261 &= ((l_393[8][3][4] , (safe_add_func_int16_t_s_s((g_269[1][2] != l_417[0][3][3]), (((void*)0 == &g_104) || 0x4939L)))) | g_418[2][2][9])) && 65535UL))), g_418[0][3][7])), 0x76B7L)), g_165)) , l_419[0][4][0]), 15)) , l_396))), p_37)) , p_37)), l_423[2][6][0])), l_394)) ^ l_393[6][3][2]) > 0xDBL) , l_300));
                        (*l_321) = &g_5;
                    }
                    else
                    { /* block id: 180 */
                        int32_t *l_424 = &l_423[1][0][5];
                        int32_t *l_425 = &l_422[6];
                        int32_t *l_426 = &g_165;
                        int32_t *l_427 = &l_422[6];
                        int32_t *l_428[1][9][2];
                        uint16_t * const ****l_445[4][2];
                        uint16_t *l_470 = &g_452[0];
                        int i, j, k;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 9; j++)
                            {
                                for (k = 0; k < 2; k++)
                                    l_428[i][j][k] = &l_261;
                            }
                        }
                        for (i = 0; i < 4; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_445[i][j] = (void*)0;
                        }
                        g_433--;
                        (*l_424) = ((((l_422[6] = (((0xB451275DL != ((g_65 > p_37) < 0x9D7A3927L)) , (safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s(0xBBA3L, (l_440 == (l_446[2] = l_442[0][0])))), (~(safe_add_func_int32_t_s_s((safe_mul_func_uint16_t_u_u((((safe_div_func_uint32_t_u_u((safe_sub_func_uint16_t_u_u((((safe_div_func_int8_t_s_s(((safe_sub_func_int32_t_s_s(((*p_36) = 0xF0D54F0BL), (g_104 = (safe_sub_func_int8_t_s_s((((l_468 & 0x70L) , l_421) & l_469), g_110[0]))))) , p_37), l_393[6][3][2])) , (void*)0) != (void*)0), 5L)), p_37)) , g_68) , 65529UL), p_37)), p_37)))))) || 4294967288UL)) , g_451) >= l_423[2][6][0]) , 0x954877C5L);
                        (*l_321) = func_49(p_37, l_470);
                    }
                    if ((safe_rshift_func_uint16_t_u_u((!((*l_482) |= (safe_mod_func_int8_t_s_s((&g_268 != (l_477 = l_476)), (((-4L) > (p_37 == 251UL)) | (safe_div_func_uint16_t_u_u((g_65 & l_419[1][1][0]), (safe_add_func_int8_t_s_s(((void*)0 == &p_36), l_300))))))))), 15)))
                    { /* block id: 191 */
                        uint16_t *l_495 = &g_452[3];
                        int32_t *l_496 = &l_373;
                        l_422[6] ^= 0x9D0F5684L;
                        p_36 = (((p_37 , (safe_div_func_uint64_t_u_u(((g_485 = l_477) != &g_268), (((((*l_253) = (safe_mod_func_int32_t_s_s((g_433 , ((*p_36) = l_399)), (safe_mod_func_int32_t_s_s(((*l_496) = (safe_mul_func_uint8_t_u_u(0x44L, (((safe_sub_func_uint8_t_u_u(g_104, (g_365 || (safe_unary_minus_func_int64_t_s(l_373))))) , (*g_268)) == l_495)))), l_399))))) ^ g_79) == 9UL) , p_37)))) >= l_468) , p_36);
                    }
                    else
                    { /* block id: 198 */
                        int32_t *l_502 = &l_423[0][3][0];
                        uint16_t *l_506 = &g_68;
                        (*l_502) ^= (g_104 & (safe_add_func_int32_t_s_s(g_65, ((l_262 , ((((safe_unary_minus_func_uint16_t_u((+(&g_2 == (p_36 = ((*l_321) = &g_144[2][1][0])))))) > (p_37 , (-1L))) , 0x3FL) | (l_501 != (void*)0))) & p_37))));
                        (**l_321) = (((g_432[2] | ((safe_sub_func_uint8_t_u_u((l_505 != l_506), p_37)) | (((safe_add_func_int64_t_s_s(l_421, (g_114 | (~((p_37 | (((safe_lshift_func_uint8_t_u_s(((((safe_rshift_func_uint8_t_u_s(l_469, ((safe_mod_func_uint8_t_u_u((l_399 <= p_37), 0x3EL)) > 0UL))) <= l_393[5][3][2]) | 4294967295UL) & g_365), l_393[1][3][0])) , p_37) == l_423[2][1][0])) != (*g_97)))))) & g_168) , l_469))) > l_423[2][6][0]) && p_37);
                    }
                    if (l_423[3][1][0])
                        continue;
                }
                else
                { /* block id: 205 */
                    uint32_t *l_531 = (void*)0;
                    uint32_t **l_533 = &g_113[3][2][1];
                    const uint16_t ** const ****l_541 = &l_536;
                    const uint16_t ** const ***l_543[1][2][7] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_537,(void*)0,&l_537,(void*)0,&l_537,(void*)0,&l_537}}};
                    const uint16_t ** const ****l_542 = &l_543[0][0][6];
                    int i, j, k;
                    for (g_180 = 21; (g_180 >= 58); ++g_180)
                    { /* block id: 208 */
                        (*p_36) = (l_421 | ((p_37 , &l_260[0][0][0]) != (void*)0));
                        if (l_423[2][4][2])
                            continue;
                    }
                    ++g_520;
                    if (((safe_rshift_func_uint16_t_u_u((((safe_sub_func_int32_t_s_s((*p_36), (safe_div_func_uint64_t_u_u((safe_rshift_func_int16_t_s_u((((l_531 == ((*l_533) = (l_532 = &g_60))) == 0x06212DB133CA85A1LL) < ((*l_284) ^= p_37)), (p_37 <= (safe_sub_func_uint64_t_u_u(((p_37 && ((((*l_542) = ((*l_541) = l_536)) != g_544) | 0x9ED93A25L)) > 0x3FL), (-1L)))))), 0xE7760B8827B0EFC5LL)))) <= g_432[0]) != (**g_268)), 6)) > p_37))
                    { /* block id: 218 */
                        if ((*p_36))
                            break;
                    }
                    else
                    { /* block id: 220 */
                        int32_t *l_546 = &l_423[2][6][0];
                        (*l_546) &= (*p_36);
                    }
                    return l_547;
                }
                (*l_321) = &l_423[1][5][4];
            }
        }
        (*p_36) ^= (l_548 && 0xE3FBEA931C83ED80LL);
        for (g_65 = (-14); (g_65 >= 24); ++g_65)
        { /* block id: 231 */
            uint16_t ***l_555 = &l_271[1][1];
            int32_t l_570 = 4L;
            for (l_429 = 0; (l_429 != (-17)); l_429 = safe_sub_func_int8_t_s_s(l_429, 6))
            { /* block id: 234 */
                l_570 ^= (safe_add_func_int64_t_s_s(((((l_555 != (void*)0) , (safe_add_func_uint64_t_u_u(0x6516586EC8AD4744LL, (g_558 != (void*)0)))) > l_419[2][5][0]) , ((((*l_253) = (((((safe_add_func_int32_t_s_s((((safe_sub_func_uint8_t_u_u(0x9CL, (safe_mul_func_uint8_t_u_u(g_110[4], (l_261 = (safe_mul_func_int16_t_s_s(((safe_add_func_uint16_t_u_u(0xBA13L, p_37)) != 7L), g_2))))))) , g_452[1]) != g_144[2][1][0]), 2L)) && 1UL) == 252UL) >= (**g_558)) <= p_37)) , 65529UL) , (**g_558))), g_365));
            }
        }
        for (l_431 = 10; (l_431 == 21); l_431++)
        { /* block id: 242 */
            int32_t ****l_587 = &l_320;
            int16_t *l_588 = &g_66;
            uint8_t *l_591 = &g_180;
            int32_t *l_592 = &l_278;
            for (g_114 = 0; (g_114 < 24); g_114 = safe_add_func_int64_t_s_s(g_114, 1))
            { /* block id: 245 */
                uint16_t *l_575[10][1] = {{&g_452[3]},{&g_68},{&g_68},{&g_452[3]},{&g_452[3]},{&g_452[3]},{&g_68},{&g_68},{&g_452[3]},{&g_452[3]}};
                int i, j;
                return l_575[6][0];
            }
            (*l_592) &= (p_37 >= (((((**g_558) = ((l_576 | (g_110[3] <= ((safe_div_func_int64_t_s_s(((((safe_add_func_int16_t_s_s((safe_add_func_int8_t_s_s((safe_add_func_int16_t_s_s(((*l_588) = (((*l_587) = g_585) == &g_586)), ((safe_sub_func_uint8_t_u_u(((*l_591) &= (0UL & 65529UL)), ((((*g_559) , 1UL) == (*p_36)) == l_262))) || g_520))), 9UL)), 65535UL)) , 0x8C1E474585F956C7LL) > p_37) > g_110[3]), 18446744073709551615UL)) , 0x8DL))) < 0xAD8BB3EFL)) || l_396) < 7L) | l_300));
            if ((*p_36))
                continue;
        }
    }
    g_165 |= ((*p_36) = (safe_mod_func_int32_t_s_s((((((safe_sub_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((g_66 = ((safe_div_func_uint8_t_u_u(0x20L, (safe_add_func_uint16_t_u_u(65535UL, ((safe_mul_func_int16_t_s_s(((*l_631) = (((((safe_sub_func_uint64_t_u_u((8UL == ((*p_36) || (((g_612 = ((*l_608) = (g_607 = &g_585))) == (void*)0) || ((safe_unary_minus_func_uint64_t_u(g_63)) <= ((safe_lshift_func_int8_t_s_s(((g_395 = (++(*l_616))) < (p_37 == (((safe_add_func_int8_t_s_s((l_629[0][0] &= (safe_div_func_uint8_t_u_u(((+(((*l_628) = g_625) != &g_626)) , g_5), g_114))), p_37)) <= p_37) <= g_520))), 1)) >= p_37))))), 0x7F57D6A18141C7F0LL)) ^ 3L) , p_37) <= g_79) && 0UL)), p_37)) , 1L))))) || p_37)), (**g_268))), 7L)) ^ p_37) || 0x4199L) && 18446744073709551615UL) , (*p_36)), g_180)));
    g_165 &= ((*p_36) = ((**g_544) == (((safe_rshift_func_int16_t_s_s(((safe_sub_func_int32_t_s_s(((safe_rshift_func_uint16_t_u_s((**g_268), (safe_lshift_func_uint16_t_u_u(((safe_sub_func_uint16_t_u_u((safe_lshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s(((*l_655) &= ((*l_654) ^= ((safe_mod_func_uint64_t_u_u((((safe_sub_func_uint8_t_u_u(249UL, g_144[0][1][0])) , ((safe_mod_func_uint64_t_u_u(18446744073709551608UL, p_37)) == (((g_432[0] > p_37) < (safe_div_func_uint64_t_u_u(p_37, g_110[4]))) & (*p_36)))) & g_433), g_450)) , (*g_559)))), g_63)), p_37)), 1UL)) <= 0x47EFBB662A772E0FLL), l_657)))) , (*p_36)), (*p_36))) < 6L), l_658)) , l_659[5]) , (*l_273))));
    g_165 = (safe_add_func_uint64_t_u_u(((*l_616) ^= (safe_mul_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(p_37, 4)), (((p_37 , (--g_395)) >= (safe_mul_func_int16_t_s_s((((*p_36) ^= ((g_682 |= ((g_168 = (safe_add_func_uint32_t_u_u(((*g_559) & 0xE954C0FE5593B035LL), l_676))) & (g_66 != (safe_div_func_uint8_t_u_u((((p_37 , p_37) | (safe_mod_func_int32_t_s_s(((g_67[1][6] != (**g_558)) <= 0x05B1BF3BDB512186LL), g_452[3]))) , g_418[0][3][7]), g_180))))) > 0x427AL)) == p_37), 0x1308L))) <= 6L))), p_37)), p_37))), p_37));
    return l_685;
}


/* ------------------------------------------ */
/* 
 * reads : g_63 g_67 g_2 g_66 g_110 g_60 g_104 g_144 g_68 g_65
 * writes: g_63 g_68 g_66 g_65 g_67 g_60 g_97 g_104
 */
static int32_t * func_38(uint32_t * p_39, uint32_t * p_40, uint64_t  p_41, int32_t  p_42)
{ /* block id: 45 */
    int32_t *l_116 = &g_63;
    uint32_t l_142 = 0xF227602CL;
    int32_t l_145[2];
    int64_t l_153 = (-5L);
    int i;
    for (i = 0; i < 2; i++)
        l_145[i] = 0L;
    (*l_116) = p_41;
    if ((*l_116))
    { /* block id: 47 */
        int32_t **l_118[9] = {&l_116,&l_116,&l_116,&l_116,&l_116,&l_116,&l_116,&l_116,&l_116};
        int32_t ***l_117 = &l_118[1];
        int i;
        (*l_117) = &g_97;
    }
    else
    { /* block id: 49 */
        uint32_t * const l_125 = &g_60;
        int32_t l_139[8][10] = {{(-1L),0x4350B5FEL,1L,0xD53FEA8EL,0x1F153C6AL,(-7L),0x1F153C6AL,0xD53FEA8EL,1L,0x4350B5FEL},{0L,0x4350B5FEL,0x9C57FF43L,0x226F77E7L,0xC5FA4B2AL,(-6L),0x4350B5FEL,9L,1L,(-7L)},{(-7L),1L,0x226F77E7L,0xD53FEA8EL,0x55EC10F0L,(-6L),(-7L),0xE9E3DC10L,0xD53FEA8EL,0xC5FA4B2AL},{0L,0x1F153C6AL,0x226F77E7L,0xE9E3DC10L,0xED7A3FE0L,(-7L),0xC5FA4B2AL,9L,9L,0xC5FA4B2AL},{(-1L),0x55EC10F0L,0x9C57FF43L,0x9C57FF43L,0x55EC10F0L,(-1L),0xC5FA4B2AL,0xD53FEA8EL,0xE9E3DC10L,(-7L)},{0x6476FD32L,0x1F153C6AL,1L,0x9C57FF43L,0xC5FA4B2AL,0x2B09B685L,(-7L),1L,9L,0x4350B5FEL},{0x6476FD32L,1L,9L,0xE9E3DC10L,0xC377FA48L,0x1A01FB82L,0x85A9A92CL,0x55EC10F0L,0x4350B5FEL,0xC377FA48L},{0x1A01FB82L,0x85A9A92CL,0x55EC10F0L,0x4350B5FEL,0xC377FA48L,(-9L),0xC377FA48L,0x4350B5FEL,0x55EC10F0L,0x85A9A92CL}};
        int32_t *l_143[9][3][5] = {{{&l_139[7][8],(void*)0,&g_63,(void*)0,&g_2},{(void*)0,&g_5,(void*)0,&g_144[2][1][0],&l_139[1][7]},{&l_139[7][8],(void*)0,(void*)0,&l_139[3][2],&g_144[2][1][0]}},{{&l_139[3][2],&g_144[2][1][0],&l_139[7][8],(void*)0,(void*)0},{(void*)0,&l_139[1][7],&g_144[2][1][1],&g_63,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,&g_144[2][1][0]}},{{&g_2,(void*)0,&g_144[0][1][0],(void*)0,&l_139[1][7]},{&g_63,&l_139[7][8],&g_144[2][1][0],&g_144[2][1][1],&g_2},{(void*)0,&g_63,(void*)0,(void*)0,(void*)0}},{{&g_2,&g_5,&g_144[1][0][1],(void*)0,&g_5},{&g_144[2][1][0],&l_139[7][8],&g_63,&g_63,&g_5},{&l_139[7][8],(void*)0,&g_63,(void*)0,&g_63}},{{&g_5,&g_5,&g_144[1][0][1],&l_139[3][2],&l_139[1][7]},{&g_144[2][1][1],&l_139[1][7],(void*)0,&g_144[2][1][0],&l_139[3][2]},{&l_139[3][2],&g_144[2][1][0],&g_144[2][1][0],(void*)0,&l_139[0][5]}},{{&g_2,&l_139[1][7],&g_144[0][1][0],&g_5,(void*)0},{(void*)0,&g_5,(void*)0,(void*)0,&l_139[3][2]},{&g_2,(void*)0,&g_144[2][1][1],(void*)0,(void*)0}},{{&g_2,&l_139[7][8],&l_139[7][8],&l_139[7][8],&g_2},{(void*)0,&g_5,(void*)0,&l_139[0][5],&g_5},{&g_2,&g_63,(void*)0,(void*)0,&g_63}},{{&l_139[3][2],&l_139[7][8],&g_63,&g_5,&g_5},{&g_144[2][1][1],(void*)0,&g_63,&g_63,&g_2},{&g_5,(void*)0,(void*)0,&l_139[3][2],(void*)0}},{{&l_139[7][8],&l_139[1][7],&g_63,&l_139[3][2],&l_139[3][2]},{&g_144[2][1][0],&g_144[2][1][0],&g_144[2][1][0],&g_63,(void*)0},{&g_2,(void*)0,&g_144[2][1][1],&g_5,&l_139[0][5]}}};
        uint16_t l_146[1];
        int64_t *l_192[10] = {&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6],&g_67[1][6]};
        int32_t l_239 = 1L;
        uint32_t l_240 = 0UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_146[i] = 65533UL;
        l_145[0] &= (safe_mul_func_int16_t_s_s(g_67[5][0], ((safe_mul_func_uint16_t_u_u(p_41, (((((safe_sub_func_int8_t_s_s(((((p_42 , 4294967293UL) <= ((void*)0 != l_125)) & (safe_sub_func_uint64_t_u_u((((((safe_sub_func_int64_t_s_s(((safe_add_func_int16_t_s_s(((((*l_116) == (safe_lshift_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((!(safe_sub_func_uint16_t_u_u(l_139[7][8], (safe_rshift_func_int16_t_s_u((*l_116), g_2))))), 0x67L)), p_41))) && 0xA5B394F6C93ECD75LL) , g_66), g_110[1])) , l_142), (*l_116))) || 255UL) , l_125) != &g_104) | 0x1142F3EDL), (*l_116)))) < (-1L)), p_42)) <= 0x190269B8F49FF72CLL) , &l_116) == &l_116) == 0x76211E331E6C26E6LL))) >= g_67[1][6])));
lbl_238:
        --l_146[0];
        for (l_142 = 5; (l_142 == 16); ++l_142)
        { /* block id: 54 */
            uint16_t l_154 = 0UL;
            int32_t *l_159 = &g_144[2][0][0];
            int32_t l_204 = (-1L);
            int32_t l_208 = 0x13A13082L;
            int32_t l_211 = 0x0493F100L;
            int32_t l_214 = 0L;
            int32_t l_218 = 0xABCEAACAL;
            int32_t l_219 = 0xE8C22AD4L;
            int32_t l_220 = 0L;
            int32_t l_221 = (-10L);
            int32_t l_222 = (-5L);
            int32_t l_223 = 0L;
            uint8_t l_224 = 255UL;
            uint64_t l_230 = 18446744073709551615UL;
            if ((safe_sub_func_int32_t_s_s(l_153, (l_154 , (safe_lshift_func_uint16_t_u_u((safe_add_func_int16_t_s_s(l_154, ((&l_125 == &g_113[1][3][0]) & g_110[3]))), 11))))))
            { /* block id: 55 */
                int32_t **l_160 = &l_159;
                (*l_160) = l_159;
                if (p_41)
                    continue;
                (*l_160) = func_45(((safe_add_func_int16_t_s_s(0x9B93L, ((safe_lshift_func_uint16_t_u_s(p_42, ((p_42 , func_43(func_43(&p_42))) != &g_114))) > 4294967294UL))) , ((*l_116) , (void*)0)), g_110[4], (**l_160));
            }
            else
            { /* block id: 59 */
                uint8_t l_178 = 1UL;
                int64_t *l_194 = &g_67[1][6];
                int32_t l_199 = 0L;
                int16_t l_200 = 0x9B1BL;
                int32_t l_205 = 0L;
                int32_t l_206[4][3] = {{0x7B2A55D4L,0xF1FA6DF7L,0xF1FA6DF7L},{0x143BA547L,0x4B4608C1L,0x4B4608C1L},{0x7B2A55D4L,0xF1FA6DF7L,0xF1FA6DF7L},{0x143BA547L,0x4B4608C1L,0x4B4608C1L}};
                int32_t *l_229 = &l_205;
                uint32_t l_233 = 4294967295UL;
                int i, j;
                for (p_42 = 0; (p_42 >= 0); p_42 -= 1)
                { /* block id: 62 */
                    int8_t *l_166 = (void*)0;
                    int8_t *l_167[3];
                    uint8_t *l_179 = &g_180;
                    int32_t l_201 = (-10L);
                    int8_t l_202[9] = {0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL,0xEFL};
                    int32_t l_207 = 0xD4DFEEEDL;
                    int32_t l_209 = 1L;
                    int32_t l_210 = 0xDE0E8798L;
                    int32_t l_212 = 0x6FDB34CFL;
                    int32_t l_213 = (-1L);
                    int32_t l_215 = 0x1C2D9155L;
                    int32_t l_216[8] = {1L,0xB295AFFEL,1L,0xB295AFFEL,1L,0xB295AFFEL,1L,0xB295AFFEL};
                    int8_t l_217 = 0x92L;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_167[i] = &g_168;
                }
                for (g_65 = (-1); (g_65 == 20); g_65 = safe_add_func_uint16_t_u_u(g_65, 1))
                { /* block id: 85 */
                    if (g_60)
                        goto lbl_238;
                    for (l_200 = 1; (l_200 >= 0); l_200 -= 1)
                    { /* block id: 89 */
                        int i, j;
                        if (g_67[(l_200 + 3)][(l_200 + 1)])
                            break;
                    }
                }
            }
        }
        ++l_240;
    }
    return &g_63;
}


/* ------------------------------------------ */
/* 
 * reads : g_68 g_60 g_63 g_104 g_2 g_110 g_67
 * writes: g_68 g_66 g_63 g_65 g_67 g_60 g_97 g_104
 */
static uint32_t * func_43(int32_t * p_44)
{ /* block id: 19 */
    const uint16_t *l_83 = (void*)0;
    int32_t l_90[1];
    int i;
    for (i = 0; i < 1; i++)
        l_90[i] = (-1L);
    for (g_68 = 1; (g_68 <= 7); g_68 += 1)
    { /* block id: 22 */
        const uint16_t *l_78 = &g_79;
        const uint16_t **l_77 = &l_78;
        const uint16_t **l_80 = (void*)0;
        const uint16_t *l_82 = &g_79;
        const uint16_t **l_81 = &l_82;
        uint16_t **l_87 = (void*)0;
        uint16_t ***l_86 = &l_87;
        const int32_t l_93 = 0x177E56D7L;
        int64_t *l_94[10] = {&g_67[1][6],&g_65,&g_67[1][6],&g_65,&g_65,&g_67[1][6],&g_65,&g_65,&g_67[1][6],&g_65};
        int32_t *l_96 = &g_63;
        int32_t **l_95[9] = {&l_96,&l_96,&l_96,&l_96,&l_96,&l_96,&l_96,&l_96,&l_96};
        uint16_t l_102[5];
        int i;
        for (i = 0; i < 5; i++)
            l_102[i] = 0x80A6L;
        for (g_66 = 7; (g_66 >= 0); g_66 -= 1)
        { /* block id: 25 */
            uint64_t l_71 = 18446744073709551610UL;
            int32_t *l_72 = &g_63;
            (*l_72) = l_71;
        }
        p_44 = (((safe_mul_func_uint8_t_u_u(((((safe_div_func_uint64_t_u_u((((*l_81) = ((*l_77) = &g_68)) == l_83), (g_67[7][4] = (g_65 = ((safe_sub_func_uint64_t_u_u(((((*l_86) = (void*)0) == (void*)0) , (safe_sub_func_int8_t_s_s(((0x89L | l_90[0]) & 0xCA85E45CF3152899LL), (safe_rshift_func_uint16_t_u_u(l_93, l_93))))), l_90[0])) ^ l_93))))) , (void*)0) == p_44) != 18446744073709551606UL), l_93)) , g_60) , (void*)0);
        if (l_90[0])
            continue;
        for (g_60 = 0; (g_60 <= 7); g_60 += 1)
        { /* block id: 37 */
            uint32_t *l_103 = &g_104;
            int32_t l_107 = (-6L);
            g_97 = &g_2;
            (*l_96) = (((safe_sub_func_int64_t_s_s(((safe_add_func_int16_t_s_s((l_102[3] | (1L >= (((((*l_103) &= (*l_96)) == (safe_add_func_int32_t_s_s(0x772B8ADFL, ((l_107 , g_2) , ((safe_mod_func_uint64_t_u_u(((g_110[4] , &p_44) == &g_97), g_60)) , l_90[0]))))) , (-1L)) , g_67[1][6]))), g_67[1][5])) < l_90[0]), 18446744073709551615UL)) | g_110[3]) || l_90[0]);
        }
    }
    return p_44;
}


/* ------------------------------------------ */
/* 
 * reads : g_68
 * writes: g_68
 */
static int32_t * func_45(uint32_t * p_46, int32_t  p_47, uint32_t  p_48)
{ /* block id: 16 */
    int32_t *l_62 = &g_63;
    int32_t *l_64[9][6][4] = {{{&g_63,&g_2,&g_63,&g_63},{&g_5,(void*)0,&g_2,&g_2},{&g_2,&g_5,&g_63,(void*)0},{&g_5,&g_63,&g_63,&g_63},{&g_2,&g_2,&g_2,&g_2},{&g_5,&g_5,&g_63,&g_5}},{{&g_63,&g_5,(void*)0,&g_63},{&g_5,&g_63,&g_5,&g_63},{&g_2,&g_5,&g_5,&g_5},{&g_5,&g_5,&g_2,&g_2},{(void*)0,&g_2,&g_2,&g_63},{&g_63,&g_63,&g_63,(void*)0}},{{&g_63,&g_5,&g_2,&g_2},{(void*)0,(void*)0,&g_2,&g_63},{&g_5,&g_2,&g_5,&g_5},{&g_2,&g_63,&g_5,&g_5},{&g_5,&g_63,(void*)0,&g_5},{&g_63,&g_2,&g_63,&g_63}},{{&g_5,(void*)0,&g_2,&g_2},{&g_2,&g_5,&g_63,(void*)0},{&g_5,&g_63,&g_63,&g_63},{&g_2,&g_2,&g_2,&g_2},{&g_5,&g_5,&g_63,&g_5},{&g_63,&g_5,(void*)0,&g_63}},{{&g_5,&g_63,&g_5,&g_63},{&g_2,&g_5,&g_5,&g_5},{&g_5,&g_5,&g_2,&g_2},{(void*)0,&g_2,&g_2,&g_63},{&g_63,&g_63,&g_63,(void*)0},{&g_63,&g_5,&g_2,&g_2}},{{(void*)0,(void*)0,&g_2,&g_63},{&g_5,&g_2,&g_5,&g_5},{&g_2,&g_63,&g_5,&g_5},{&g_5,&g_63,(void*)0,&g_5},{&g_63,&g_2,&g_63,&g_63},{&g_5,(void*)0,&g_2,&g_2}},{{&g_2,&g_5,&g_63,(void*)0},{&g_5,&g_63,&g_63,&g_63},{&g_2,&g_2,&g_2,&g_2},{&g_5,&g_5,&g_63,&g_5},{&g_63,&g_5,(void*)0,&g_63},{&g_5,&g_63,&g_5,&g_63}},{{&g_2,&g_5,&g_5,&g_5},{&g_5,&g_2,&g_63,&g_5},{&g_5,&g_5,&g_63,&g_2},{&g_5,&g_63,&g_2,&g_5},{&g_5,&g_2,&g_63,&g_63},{&g_5,&g_5,&g_63,&g_5}},{{&g_63,&g_63,&g_5,&g_2},{&g_63,&g_2,&g_63,&g_5},{&g_5,&g_2,&g_5,&g_2},{&g_2,&g_63,&g_2,&g_5},{&g_2,&g_5,&g_5,&g_63},{&g_5,&g_2,&g_5,&g_5}}};
    int i, j, k;
    g_68--;
    return l_64[3][3][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_2 g_59
 * writes:
 */
static uint32_t * func_49(uint32_t  p_50, uint16_t * p_51)
{ /* block id: 9 */
    int32_t l_56[7] = {0x752E4A6BL,0x752E4A6BL,0x752E4A6BL,0x752E4A6BL,0x752E4A6BL,0x752E4A6BL,0x752E4A6BL};
    uint32_t *l_61 = (void*)0;
    int i;
    if (((safe_sub_func_int64_t_s_s((safe_rshift_func_uint8_t_u_u(l_56[5], g_2)), 0x709E149CFF29C214LL)) , 1L))
    { /* block id: 10 */
        int32_t *l_58[5][3][4] = {{{(void*)0,&g_5,&l_56[5],&g_5},{&l_56[5],&g_5,&l_56[5],&l_56[5]},{(void*)0,&l_56[2],&l_56[5],&l_56[5]}},{{&l_56[4],&g_5,&l_56[4],&g_5},{&l_56[4],&g_5,&l_56[5],&l_56[3]},{(void*)0,&g_5,&l_56[5],&g_5}},{{&l_56[5],&g_5,&l_56[5],&l_56[5]},{(void*)0,&l_56[2],&l_56[5],&l_56[5]},{&l_56[4],&g_5,&l_56[4],&g_5}},{{&l_56[4],&g_5,&l_56[5],&l_56[3]},{(void*)0,&g_5,&l_56[5],&g_5},{&l_56[5],&g_5,&l_56[5],&l_56[5]}},{{(void*)0,&l_56[2],&l_56[5],&l_56[5]},{&l_56[4],&g_5,&l_56[4],&g_5},{&l_56[4],&g_5,&l_56[5],&l_56[3]}}};
        int32_t **l_57 = &l_58[2][2][1];
        int i, j, k;
        (*l_57) = &g_5;
        return g_59;
    }
    else
    { /* block id: 13 */
        return l_61;
    }
}




/* ---------------------------------------- */
//testcase_id 1484153504
int case1484153504(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_5, "g_5", print_hash_value);
    transparent_crc(g_20, "g_20", print_hash_value);
    transparent_crc(g_32, "g_32", print_hash_value);
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_63, "g_63", print_hash_value);
    transparent_crc(g_65, "g_65", print_hash_value);
    transparent_crc(g_66, "g_66", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_67[i][j], "g_67[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_68, "g_68", print_hash_value);
    transparent_crc(g_79, "g_79", print_hash_value);
    transparent_crc(g_104, "g_104", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_110[i], "g_110[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_114, "g_114", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_144[i][j][k], "g_144[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_165, "g_165", print_hash_value);
    transparent_crc(g_168, "g_168", print_hash_value);
    transparent_crc(g_180, "g_180", print_hash_value);
    transparent_crc(g_365, "g_365", print_hash_value);
    transparent_crc(g_395, "g_395", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 10; k++)
            {
                transparent_crc(g_418[i][j][k], "g_418[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_432[i], "g_432[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_433, "g_433", print_hash_value);
    transparent_crc(g_450, "g_450", print_hash_value);
    transparent_crc(g_451, "g_451", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_452[i], "g_452[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_520, "g_520", print_hash_value);
    transparent_crc(g_656, "g_656", print_hash_value);
    transparent_crc(g_682, "g_682", print_hash_value);
    transparent_crc(g_689, "g_689", print_hash_value);
    transparent_crc(g_712, "g_712", print_hash_value);
    transparent_crc(g_977, "g_977", print_hash_value);
    transparent_crc(g_985, "g_985", print_hash_value);
    transparent_crc(g_989, "g_989", print_hash_value);
    transparent_crc(g_1214, "g_1214", print_hash_value);
    transparent_crc(g_1258, "g_1258", print_hash_value);
    transparent_crc(g_1282, "g_1282", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 275
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 43
breakdown:
   depth: 1, occurrence: 161
   depth: 2, occurrence: 49
   depth: 3, occurrence: 3
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 3
   depth: 17, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 4
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 1
   depth: 26, occurrence: 4
   depth: 27, occurrence: 2
   depth: 28, occurrence: 4
   depth: 29, occurrence: 2
   depth: 31, occurrence: 2
   depth: 36, occurrence: 1
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1
   depth: 43, occurrence: 1

XXX total number of pointers: 280

XXX times a variable address is taken: 870
XXX times a pointer is dereferenced on RHS: 141
breakdown:
   depth: 1, occurrence: 111
   depth: 2, occurrence: 28
   depth: 3, occurrence: 0
   depth: 4, occurrence: 0
   depth: 5, occurrence: 2
XXX times a pointer is dereferenced on LHS: 159
breakdown:
   depth: 1, occurrence: 136
   depth: 2, occurrence: 16
   depth: 3, occurrence: 5
   depth: 4, occurrence: 0
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 21
XXX times a pointer is compared with address of another variable: 4
XXX times a pointer is compared with another pointer: 7
XXX times a pointer is qualified to be dereferenced: 4319

XXX max dereference level: 6
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 723
   level: 2, occurrence: 211
   level: 3, occurrence: 87
   level: 4, occurrence: 102
   level: 5, occurrence: 33
   level: 6, occurrence: 6
XXX number of pointers point to pointers: 120
XXX number of pointers point to scalars: 160
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 33.6
XXX average alias set size: 1.52

XXX times a non-volatile is read: 1027
XXX times a non-volatile is write: 517
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 1
XXX backward jumps: 2

XXX stmts: 173
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 31
   depth: 2, occurrence: 24
   depth: 3, occurrence: 23
   depth: 4, occurrence: 33
   depth: 5, occurrence: 33

XXX percentage a fresh-made variable is used: 14.2
XXX percentage an existing variable is used: 85.8
********************* end of statistics **********************/

