/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      2826149612
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   const int32_t  f0;
};

/* --- GLOBAL VARIABLES --- */
static uint16_t g_8 = 0x845DL;
static int8_t g_11 = (-1L);
static int32_t g_22 = 0x9EDCA8FEL;
static struct S0 g_26 = {0x7AB6DAD8L};
static struct S0 g_29 = {-8L};
static struct S0 *g_28 = &g_29;
static int8_t g_44 = 0x49L;
static int32_t g_47 = (-1L);
static uint8_t g_59 = 0x53L;
static int32_t *g_97 = &g_47;
static uint32_t g_113 = 0x69207283L;
static uint8_t g_123 = 247UL;
static uint8_t g_125 = 255UL;
static int16_t g_131 = 0x5E5FL;
static int16_t g_135 = 0xA610L;
static uint64_t g_142 = 18446744073709551612UL;
static int16_t g_151 = 0x550BL;
static int32_t g_174 = (-10L);
static uint32_t g_175 = 0x49C48DCBL;
static int8_t g_177 = 0L;
static uint8_t *g_208 = (void*)0;
static int16_t *g_229 = &g_131;
static uint32_t g_269 = 0x9EE50CD2L;
static uint8_t g_298 = 255UL;
static uint64_t g_334 = 0UL;
static uint32_t g_370 = 0x900326E5L;
static int32_t g_397 = 0x7E60BC75L;
static struct S0 **g_418 = &g_28;
static struct S0 ***g_417 = &g_418;
static struct S0 ** const *g_423 = &g_418;
static struct S0 ** const **g_422 = &g_423;
static int8_t g_500 = 0xEAL;
static uint64_t g_517 = 18446744073709551608UL;
static uint16_t g_534 = 1UL;
static int32_t *g_538[1] = {&g_47};
static uint32_t g_554 = 0x6E49509FL;
static int8_t g_598 = (-10L);
static struct S0 ****g_661 = (void*)0;
static struct S0 *****g_660[10][1] = {{(void*)0},{&g_661},{(void*)0},{&g_661},{(void*)0},{&g_661},{(void*)0},{&g_661},{(void*)0},{&g_661}};
static int64_t g_684 = 0x03CC9D723791C260LL;
static int64_t *g_683 = &g_684;
static int32_t g_687 = 0xAA3B9110L;
static uint16_t g_806 = 0xB908L;
static const uint32_t *g_837 = (void*)0;
static int8_t g_866[5] = {0xBEL,0xBEL,0xBEL,0xBEL,0xBEL};
static int32_t g_892[5][1] = {{(-5L)},{1L},{(-5L)},{1L},{(-5L)}};
static int32_t g_907 = 0L;
static uint32_t *g_914 = (void*)0;
static uint32_t **g_913[7][8] = {{(void*)0,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914},{(void*)0,&g_914,&g_914,&g_914,&g_914,(void*)0,&g_914,(void*)0},{&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914},{&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914},{&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,(void*)0,&g_914},{&g_914,(void*)0,&g_914,&g_914,&g_914,(void*)0,(void*)0,&g_914},{&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914,&g_914}};
static uint64_t g_978 = 1UL;
static uint16_t g_1174[6][4] = {{0x6281L,0x20BFL,0x20BFL,0x6281L},{0x20BFL,0x6281L,0x20BFL,0x20BFL},{0x6281L,0x6281L,1UL,0x6281L},{0x6281L,0x20BFL,0x20BFL,0x6281L},{0x20BFL,0x6281L,0x20BFL,0x20BFL},{0x6281L,0x6281L,1UL,0x6281L}};
static uint8_t g_1225[7][8] = {{255UL,0x45L,0x44L,249UL,1UL,1UL,249UL,0x44L},{6UL,6UL,0x44L,0xF4L,0xBDL,254UL,246UL,255UL},{1UL,246UL,8UL,6UL,8UL,246UL,1UL,255UL},{246UL,254UL,0xBDL,0xF4L,0x44L,6UL,6UL,0x44L},{249UL,1UL,1UL,249UL,0x44L,0x45L,255UL,6UL},{246UL,252UL,0x70L,0x44L,8UL,0x44L,0x70L,252UL},{1UL,252UL,6UL,0x70L,0xBDL,0x45L,0xF4L,0xF4L}};
static struct S0 *** const *g_1278 = &g_417;
static struct S0 *** const **g_1277 = &g_1278;
static const int32_t **g_1353 = (void*)0;
static const int32_t ***g_1352 = &g_1353;
static int32_t * const g_1394 = &g_47;
static struct S0 g_1405[7] = {{0x24EBB0E2L},{0x24EBB0E2L},{0x24EBB0E2L},{0x24EBB0E2L},{0x24EBB0E2L},{0x24EBB0E2L},{0x24EBB0E2L}};
static uint64_t *g_1441[2][10][6] = {{{&g_142,(void*)0,&g_142,&g_142,&g_142,&g_142},{&g_142,&g_142,&g_142,&g_517,(void*)0,&g_517},{(void*)0,&g_142,(void*)0,&g_142,&g_142,&g_142},{&g_517,(void*)0,(void*)0,&g_517,&g_142,&g_517},{&g_517,&g_517,&g_142,&g_517,&g_517,&g_142},{&g_517,&g_517,&g_142,&g_142,&g_517,&g_517},{(void*)0,&g_517,&g_142,&g_517,&g_142,&g_517},{&g_142,(void*)0,&g_142,&g_142,&g_142,&g_142},{&g_142,&g_142,&g_142,&g_517,(void*)0,&g_517},{(void*)0,&g_142,(void*)0,&g_142,&g_142,&g_142}},{{&g_517,(void*)0,(void*)0,&g_517,&g_142,&g_517},{&g_517,&g_517,&g_142,&g_517,&g_517,&g_142},{&g_517,&g_142,&g_142,&g_142,&g_142,(void*)0},{&g_517,(void*)0,(void*)0,&g_142,(void*)0,(void*)0},{(void*)0,&g_517,&g_142,&g_142,&g_142,&g_142},{(void*)0,(void*)0,&g_142,&g_142,&g_517,&g_142},{&g_517,(void*)0,&g_517,&g_142,&g_142,&g_142},{(void*)0,&g_517,&g_517,(void*)0,(void*)0,&g_142},{&g_142,(void*)0,&g_142,(void*)0,&g_142,&g_142},{(void*)0,&g_142,&g_142,&g_142,&g_142,(void*)0}}};
static uint64_t ** const g_1440[5][7][7] = {{{&g_1441[0][9][5],&g_1441[0][2][4],&g_1441[0][1][3],(void*)0,&g_1441[0][8][0],(void*)0,&g_1441[0][8][0]},{&g_1441[0][8][0],&g_1441[0][6][1],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][1][3]},{&g_1441[0][9][5],(void*)0,&g_1441[0][7][5],&g_1441[0][8][0],&g_1441[0][9][5],&g_1441[0][8][0],&g_1441[0][8][0]},{(void*)0,&g_1441[0][8][0],&g_1441[0][2][2],&g_1441[0][8][0],&g_1441[1][8][3],&g_1441[0][8][0],&g_1441[0][8][0]},{&g_1441[0][8][4],&g_1441[0][2][2],&g_1441[0][8][0],&g_1441[0][8][0],(void*)0,&g_1441[0][6][1],(void*)0},{&g_1441[0][8][0],(void*)0,(void*)0,&g_1441[0][8][0],&g_1441[0][2][2],&g_1441[1][4][3],&g_1441[0][8][0]},{&g_1441[1][1][3],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],(void*)0,(void*)0}},{{(void*)0,&g_1441[1][4][3],&g_1441[0][9][5],(void*)0,&g_1441[1][6][4],&g_1441[0][8][0],&g_1441[0][8][0]},{&g_1441[0][8][0],&g_1441[1][9][5],&g_1441[0][8][0],&g_1441[1][3][4],&g_1441[0][8][0],&g_1441[0][8][0],(void*)0},{&g_1441[1][9][5],&g_1441[0][8][4],&g_1441[0][8][0],(void*)0,(void*)0,(void*)0,&g_1441[0][8][0]},{(void*)0,&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0]},{&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[1][8][3],&g_1441[0][1][3]},{&g_1441[1][4][3],&g_1441[0][8][0],&g_1441[0][9][5],(void*)0,&g_1441[0][6][1],&g_1441[0][7][5],&g_1441[0][8][0]},{(void*)0,&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][9][5],&g_1441[0][8][0],&g_1441[1][8][3],&g_1441[0][6][1]}},{{&g_1441[0][8][0],&g_1441[1][6][4],(void*)0,&g_1441[0][9][5],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][9][5]},{&g_1441[1][8][3],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[1][9][5],(void*)0,(void*)0,&g_1441[1][4][3]},{&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][2][2],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][2][2]},{&g_1441[1][6][4],&g_1441[1][6][4],&g_1441[0][7][5],&g_1441[1][8][3],&g_1441[0][6][1],&g_1441[0][8][0],&g_1441[0][8][0]},{&g_1441[0][2][4],&g_1441[0][8][0],&g_1441[0][8][0],(void*)0,&g_1441[1][7][1],(void*)0,&g_1441[0][8][0]},{&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][1][3],&g_1441[0][8][0],&g_1441[0][6][1],&g_1441[1][4][3],(void*)0},{&g_1441[0][8][0],&g_1441[0][8][0],(void*)0,&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[1][3][4],(void*)0}},{{(void*)0,&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][6][1],&g_1441[0][9][5],&g_1441[1][6][4],(void*)0},{(void*)0,&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0]},{&g_1441[0][6][1],(void*)0,&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][2][2],&g_1441[0][8][4],&g_1441[0][9][5]},{&g_1441[0][2][2],(void*)0,&g_1441[0][9][5],(void*)0,&g_1441[0][8][0],&g_1441[1][8][3],&g_1441[0][8][0]},{&g_1441[1][9][5],&g_1441[0][8][0],&g_1441[0][7][5],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[1][7][1]},{&g_1441[0][7][5],&g_1441[0][8][0],&g_1441[0][9][5],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][2][4],&g_1441[1][6][4]},{&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][8][0],&g_1441[0][6][1],&g_1441[1][8][3],&g_1441[0][7][5],&g_1441[1][6][4]}},{{&g_1441[1][7][1],(void*)0,&g_1441[1][6][4],&g_1441[0][8][0],&g_1441[1][6][4],(void*)0,&g_1441[1][7][1]},{(void*)0,&g_1441[0][8][0],&g_1441[0][6][1],&g_1441[0][8][0],&g_1441[0][7][5],&g_1441[0][6][1],&g_1441[0][8][0]},{&g_1441[0][9][5],&g_1441[1][3][4],&g_1441[0][8][0],(void*)0,&g_1441[0][6][1],&g_1441[0][6][1],&g_1441[0][9][5]},{(void*)0,&g_1441[1][9][5],&g_1441[0][6][1],&g_1441[1][7][1],&g_1441[0][8][0],(void*)0,&g_1441[0][8][0]},{&g_1441[1][4][3],&g_1441[0][8][0],&g_1441[1][6][4],&g_1441[0][7][0],&g_1441[0][1][3],&g_1441[0][9][5],(void*)0},{&g_1441[0][8][0],&g_1441[0][6][1],&g_1441[0][8][0],(void*)0,&g_1441[1][7][1],&g_1441[0][9][5],(void*)0},{(void*)0,&g_1441[0][2][4],&g_1441[0][9][5],&g_1441[0][9][5],&g_1441[0][2][4],(void*)0,&g_1441[1][1][3]}}};
static int32_t g_1472[1] = {0xDAAA5FC1L};
static int32_t *g_1549[5] = {&g_892[0][0],&g_892[0][0],&g_892[0][0],&g_892[0][0],&g_892[0][0]};
static int32_t **g_1548[6][7][3] = {{{&g_1549[4],&g_1549[0],&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]},{(void*)0,(void*)0,&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]},{&g_1549[0],&g_1549[4],&g_1549[1]},{&g_1549[1],&g_1549[1],&g_1549[3]},{&g_1549[4],&g_1549[0],&g_1549[1]}},{{(void*)0,(void*)0,&g_1549[3]},{(void*)0,(void*)0,&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]},{&g_1549[0],&g_1549[4],&g_1549[1]},{&g_1549[1],&g_1549[1],&g_1549[3]},{&g_1549[4],&g_1549[0],&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]}},{{(void*)0,(void*)0,&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]},{&g_1549[0],&g_1549[4],&g_1549[1]},{&g_1549[1],&g_1549[1],&g_1549[3]},{&g_1549[4],&g_1549[0],&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]},{(void*)0,(void*)0,&g_1549[1]}},{{(void*)0,(void*)0,&g_1549[3]},{&g_1549[0],&g_1549[4],&g_1549[1]},{&g_1549[1],&g_1549[1],&g_1549[3]},{&g_1549[4],&g_1549[0],&g_1549[1]},{(void*)0,(void*)0,&g_1549[3]},{(void*)0,(void*)0,&g_1549[1]},{(void*)0,(void*)0,&g_1549[1]}},{{&g_1549[0],(void*)0,(void*)0},{&g_1549[3],&g_1549[3],&g_1549[1]},{(void*)0,&g_1549[0],(void*)0},{&g_1549[1],&g_1549[1],&g_1549[1]},{&g_1549[1],&g_1549[1],(void*)0},{&g_1549[1],&g_1549[1],&g_1549[1]},{&g_1549[0],(void*)0,(void*)0}},{{&g_1549[3],&g_1549[3],&g_1549[1]},{(void*)0,&g_1549[0],(void*)0},{&g_1549[1],&g_1549[1],&g_1549[1]},{&g_1549[1],&g_1549[1],(void*)0},{&g_1549[1],&g_1549[1],&g_1549[1]},{&g_1549[0],(void*)0,(void*)0},{&g_1549[3],&g_1549[3],&g_1549[1]}}};
static const int16_t g_1610 = 1L;
static const int16_t g_1612[9][5] = {{(-5L),0L,(-8L),(-8L),0L},{0x06EAL,9L,(-1L),(-6L),0L},{0L,(-8L),1L,0x9839L,1L},{0L,0L,0L,0x06EAL,0xAEE6L},{0L,0xA019L,(-5L),1L,1L},{0x06EAL,0x160BL,0x06EAL,0x8C2BL,0x11A9L},{(-5L),0xA019L,0L,0L,1L},{0L,0L,0L,0L,0x06EAL},{1L,(-8L),0L,1L,0xA019L}};
static const int16_t g_1614 = 0xF36CL;
static const int16_t *g_1613 = &g_1614;
static uint16_t *g_1653[8] = {&g_1174[3][2],&g_1174[3][2],&g_1174[3][2],&g_1174[3][2],&g_1174[3][2],&g_1174[3][2],&g_1174[3][2],&g_1174[3][2]};
static uint16_t **g_1652 = &g_1653[3];
static uint16_t g_1659 = 65533UL;
static uint32_t g_1879 = 0UL;
static uint16_t g_1884 = 0x7CE8L;
static uint32_t g_2034 = 6UL;
static uint32_t g_2058 = 0xC9AE9321L;
static int32_t * const *g_2177 = &g_538[0];
static int32_t * const **g_2176 = &g_2177;
static uint32_t *g_2235 = &g_1879;
static uint32_t **g_2234 = &g_2235;
static uint32_t g_2270 = 0x32E26089L;
static struct S0 *g_2321 = &g_26;
static int32_t **g_2423 = &g_97;
static uint8_t g_2433 = 0xD1L;
static struct S0 g_2480 = {0x2E7CE9F3L};


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);
static struct S0 * func_2(uint32_t  p_3);
static uint64_t  func_16(struct S0 * p_17, struct S0  p_18, uint32_t  p_19, int16_t  p_20);
static struct S0 * func_23(uint32_t  p_24);
static int16_t  func_31(int8_t  p_32, int8_t  p_33, struct S0 ** p_34, int32_t  p_35, int32_t * p_36);
static struct S0  func_66(int8_t  p_67, const uint16_t  p_68, int32_t  p_69, uint64_t  p_70, uint64_t  p_71);
static const int32_t  func_74(uint16_t  p_75, int8_t  p_76, uint32_t  p_77, uint16_t  p_78, uint16_t  p_79);
static uint32_t  func_80(struct S0 ** p_81, int32_t * p_82, uint32_t  p_83, int8_t * p_84);
static struct S0 ** func_85(int8_t  p_86, struct S0 * p_87, struct S0  p_88, uint32_t  p_89);
static const int16_t  func_94(struct S0 * p_95);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_8 g_11 g_22 g_44 g_26.f0 g_59 g_538 g_47 g_1277 g_1278 g_417 g_418 g_28 g_26 g_1610 g_1613 g_1614 g_2034 g_683 g_397 g_2058 g_135 g_1472 g_1652 g_1653 g_1174 g_534 g_1394 g_684 g_97 g_131 g_29.f0 g_113 g_142 g_151 g_174 g_175 g_229 g_125 g_1225 g_1612 g_423
 * writes: g_11 g_22 g_28 g_44 g_47 g_59 g_538 g_97 g_2034 g_684 g_2058 g_806 g_113 g_1174 g_534 g_131 g_135 g_142 g_151 g_174 g_177 g_208 g_418 g_1225
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    int8_t *l_9 = (void*)0;
    int8_t *l_10 = &g_11;
    int32_t *l_21 = &g_22;
    struct S0 *l_25 = &g_26;
    struct S0 **l_27[5] = {&l_25,&l_25,&l_25,&l_25,&l_25};
    uint64_t l_30 = 18446744073709551608UL;
    int8_t *l_43 = &g_44;
    uint8_t l_2105[1][6] = {{0xE8L,0xCCL,0xCCL,0xE8L,0xCCL,0xCCL}};
    struct S0 *l_2479 = &g_2480;
    int32_t l_2483 = 1L;
    int32_t l_2504 = 0xBB087A7CL;
    int32_t l_2505 = 0xFB924F14L;
    int32_t l_2506 = 0x1F1FED34L;
    int32_t l_2507[8][9] = {{(-7L),0xE6A7F8ECL,0x2DB053F5L,(-6L),0xE0999088L,0xC45F469FL,(-7L),(-1L),0x6EDC4577L},{(-6L),0xE6A7F8ECL,5L,0x6BC3FCB6L,0xE0999088L,(-8L),(-6L),(-1L),(-1L)},{0x6BC3FCB6L,0xE6A7F8ECL,0xE0999088L,(-7L),0xE0999088L,0xE6A7F8ECL,0x6BC3FCB6L,(-1L),1L},{(-7L),0xE6A7F8ECL,0x2DB053F5L,(-6L),0xE0999088L,0xC45F469FL,(-7L),(-1L),0x6EDC4577L},{(-6L),0xE6A7F8ECL,5L,0x6BC3FCB6L,0xE0999088L,(-8L),(-6L),(-1L),(-1L)},{0x6BC3FCB6L,0xE6A7F8ECL,0xE0999088L,(-7L),0xE0999088L,0xE6A7F8ECL,0x6BC3FCB6L,(-1L),1L},{(-7L),0xE6A7F8ECL,0x2DB053F5L,(-6L),0xE0999088L,0xC45F469FL,(-7L),(-1L),0x6EDC4577L},{(-6L),0xE6A7F8ECL,5L,0x6BC3FCB6L,0xE0999088L,(-8L),(-6L),(-1L),(-1L)}};
    uint32_t l_2508 = 4294967292UL;
    int i, j;
    l_2479 = func_2((safe_mod_func_int64_t_s_s((safe_mul_func_int8_t_s_s(((*l_10) |= (4L != g_8)), (safe_lshift_func_uint16_t_u_s(((**g_1652) = ((safe_sub_func_uint8_t_u_u((g_8 , (func_16((((*l_21) = g_8) , ((*g_418) = func_23(((l_30 = ((g_28 = l_25) == &g_26)) && func_31((*l_21), ((*l_43) ^= (safe_lshift_func_uint16_t_u_s(((((safe_div_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(g_8, g_22)) == (*l_21)), (*l_21))) | 0UL) != 6L) > g_8), (*l_21)))), &l_25, (*l_21), &g_22))))), (*l_25), g_1610, (*g_1613)) < l_2105[0][1])), 0L)) , 0xCE6FL)), 14)))), l_2105[0][4])));
    for (g_142 = (-13); (g_142 > 49); g_142 = safe_add_func_uint64_t_u_u(g_142, 6))
    { /* block id: 1167 */
        int32_t *l_2484 = &g_174;
        int32_t *l_2485 = &g_22;
        int32_t *l_2486 = &g_397;
        int32_t *l_2487 = &g_174;
        int32_t *l_2488 = &l_2483;
        int32_t *l_2489 = (void*)0;
        int32_t *l_2490 = &l_2483;
        int32_t *l_2491 = (void*)0;
        int32_t *l_2492 = &g_907;
        int32_t *l_2493 = (void*)0;
        int32_t *l_2494 = &g_1472[0];
        int32_t l_2495 = 0x143DC2B6L;
        int32_t *l_2496 = &g_1472[0];
        int32_t *l_2497 = &g_1472[0];
        int32_t *l_2498 = &l_2483;
        int32_t *l_2499 = &g_1472[0];
        int32_t *l_2500 = &g_47;
        int32_t *l_2501 = (void*)0;
        int32_t *l_2502 = &g_397;
        int32_t *l_2503[9];
        int i;
        for (i = 0; i < 9; i++)
            l_2503[i] = &g_907;
        l_2508--;
        return (*l_2484);
    }
    return (*l_21);
}


/* ------------------------------------------ */
/* 
 * reads : g_1652 g_1653 g_1174 g_417 g_418 g_113
 * writes: g_174 g_113
 */
static struct S0 * func_2(uint32_t  p_3)
{ /* block id: 1004 */
    int32_t * const *l_2128 = (void*)0;
    int32_t * const **l_2127 = &l_2128;
    struct S0 l_2131[6] = {{0L},{0L},{0L},{0L},{0L},{0L}};
    struct S0 * const *l_2132 = (void*)0;
    int32_t *l_2136 = &g_47;
    uint16_t l_2152 = 0x55AEL;
    uint64_t **l_2189 = &g_1441[0][8][0];
    int32_t ***l_2192 = &g_1548[3][5][2];
    int32_t l_2196 = 0x38707B87L;
    uint64_t l_2197 = 0x5DA3E49F4AD1A28ELL;
    int16_t **l_2200 = &g_229;
    uint16_t l_2222 = 0x65A3L;
    int8_t l_2267 = 0L;
    int32_t l_2269 = (-1L);
    struct S0 ** const *l_2393[5][3] = {{&g_418,&g_418,&g_418},{&g_418,(void*)0,&g_418},{(void*)0,(void*)0,&g_418},{&g_418,&g_418,&g_418},{&g_418,&g_418,&g_418}};
    struct S0 ***l_2395 = (void*)0;
    uint64_t l_2470[4][9][7] = {{{4UL,2UL,0xE96673E7ED6F1860LL,0xE924B870D84588ECLL,0UL,1UL,0xC01E6E2BFBD3AEB5LL},{0xE4B960FFA28F10A7LL,0xE8E0C2BCD68C2F0ALL,18446744073709551615UL,18446744073709551615UL,0xE8E0C2BCD68C2F0ALL,0xE4B960FFA28F10A7LL,0x9DFFBE5849968CB6LL},{18446744073709551614UL,1UL,0x1D72486FDFA1320FLL,18446744073709551610UL,0x90C4FD3C24C1E0E6LL,0xFA7AD11E7587697ALL,4UL},{0UL,0x012B28DD3BCCE148LL,0x9B42C512DC6D0F06LL,0x6F1FDBD0176DF7E4LL,0xD927F7B6FE6D39C7LL,18446744073709551609UL,0xA9118158FB5C4CE1LL},{18446744073709551608UL,1UL,0xEF968E539E95EEFELL,0UL,0x47AA2967684ABF0ALL,0xA3B75D2D76820624LL,2UL},{0xC01E6E2BFBD3AEB5LL,0xE8E0C2BCD68C2F0ALL,0x004DDE9AB5C29478LL,0UL,18446744073709551608UL,18446744073709551612UL,0x90C4FD3C24C1E0E6LL},{0x9BE7F56FE1B7E0A2LL,2UL,2UL,0x90C4FD3C24C1E0E6LL,0xD927F7B6FE6D39C7LL,0x012B28DD3BCCE148LL,0x0BE39E3CF95751CFLL},{0x9CD17CD9532955F1LL,0x9BE7F56FE1B7E0A2LL,0x9DFFBE5849968CB6LL,0xA0EE142DCFAF9A20LL,0x7826243A5D8D3410LL,0UL,0xB14151C8B9BA5E0BLL},{0x0BE39E3CF95751CFLL,0x90C4FD3C24C1E0E6LL,18446744073709551612UL,18446744073709551608UL,0UL,0x004DDE9AB5C29478LL,0xE8E0C2BCD68C2F0ALL}},{{18446744073709551608UL,0xA3B75D2D76820624LL,0xB14151C8B9BA5E0BLL,18446744073709551608UL,0UL,0x279C4185A1FA147BLL,0x54393D2CEBC61F7CLL},{18446744073709551608UL,0x6F1FDBD0176DF7E4LL,0x0640C903873DF49CLL,0xA0EE142DCFAF9A20LL,0xF5B1298CF86CC800LL,1UL,0x9DFFBE5849968CB6LL},{0UL,4UL,0xFA7AD11E7587697ALL,0x90C4FD3C24C1E0E6LL,18446744073709551610UL,0x1D72486FDFA1320FLL,1UL},{0xE00FE1CA2961C0F8LL,0x012B28DD3BCCE148LL,1UL,0UL,0xA0EE142DCFAF9A20LL,0xA0EE142DCFAF9A20LL,0UL},{0xE4B960FFA28F10A7LL,18446744073709551608UL,0xE4B960FFA28F10A7LL,0UL,4UL,18446744073709551615UL,0xB14151C8B9BA5E0BLL},{0xE924B870D84588ECLL,0xE00FE1CA2961C0F8LL,0xFA7AD11E7587697ALL,0x6F1FDBD0176DF7E4LL,0x54393D2CEBC61F7CLL,18446744073709551612UL,0x9BE7F56FE1B7E0A2LL},{0x7826243A5D8D3410LL,0xB14151C8B9BA5E0BLL,18446744073709551609UL,18446744073709551610UL,18446744073709551615UL,18446744073709551615UL,0x92280643B8A277EBLL},{0x9CD17CD9532955F1LL,18446744073709551614UL,0xA3B75D2D76820624LL,18446744073709551615UL,0x0BE39E3CF95751CFLL,0xA0EE142DCFAF9A20LL,2UL},{18446744073709551610UL,0xA8AD41553109CBD7LL,18446744073709551612UL,0xE924B870D84588ECLL,5UL,0x1D72486FDFA1320FLL,0x6F1FDBD0176DF7E4LL}},{{0x7F5AEE0CD9796A0BLL,0xB14151C8B9BA5E0BLL,0x012B28DD3BCCE148LL,0x54393D2CEBC61F7CLL,0UL,1UL,0xE924B870D84588ECLL},{1UL,5UL,0UL,0xD927F7B6FE6D39C7LL,0xE00FE1CA2961C0F8LL,0x279C4185A1FA147BLL,0x9DFFBE5849968CB6LL},{0x6F1FDBD0176DF7E4LL,0xC01E6E2BFBD3AEB5LL,0x004DDE9AB5C29478LL,18446744073709551615UL,0x9BE7F56FE1B7E0A2LL,0x004DDE9AB5C29478LL,18446744073709551608UL},{0x6F1FDBD0176DF7E4LL,0x012B28DD3BCCE148LL,0x279C4185A1FA147BLL,5UL,18446744073709551615UL,0UL,18446744073709551614UL},{1UL,0x39B642C70BB743D8LL,1UL,0UL,0x7F5AEE0CD9796A0BLL,0x012B28DD3BCCE148LL,0xA3B75D2D76820624LL},{0x7F5AEE0CD9796A0BLL,0xA9118158FB5C4CE1LL,0x1D72486FDFA1320FLL,18446744073709551614UL,0x39B642C70BB743D8LL,18446744073709551612UL,18446744073709551614UL},{18446744073709551610UL,0xA3B75D2D76820624LL,0xA0EE142DCFAF9A20LL,0x9BE7F56FE1B7E0A2LL,0xA0EE142DCFAF9A20LL,0xA3B75D2D76820624LL,18446744073709551610UL},{0x9CD17CD9532955F1LL,0x7826243A5D8D3410LL,18446744073709551615UL,0xD927F7B6FE6D39C7LL,18446744073709551614UL,18446744073709551609UL,0xA3B75D2D76820624LL},{18446744073709551608UL,0x279C4185A1FA147BLL,0x9BE7F56FE1B7E0A2LL,0x012B28DD3BCCE148LL,18446744073709551615UL,0x7826243A5D8D3410LL,2UL}},{{2UL,0xCBF224DB024A3770LL,18446744073709551612UL,0xA3B75D2D76820624LL,0xAB6CE1A812C005D9LL,4UL,1UL},{4UL,18446744073709551609UL,0x02FC3CDBE19BA7E9LL,0x44184EB300A9E1A0LL,18446744073709551609UL,0x142B9193C1DE6FC6LL,0x004DDE9AB5C29478LL},{0UL,0x5D5501BE29775292LL,0x90C4FD3C24C1E0E6LL,1UL,1UL,0x7826243A5D8D3410LL,0xA3B75D2D76820624LL},{18446744073709551609UL,0xFA7AD11E7587697ALL,0x6DBF373E7DABE3DCLL,18446744073709551615UL,0x008CF5A8B060D2F3LL,0xD0628376B67543B5LL,0xA0EE142DCFAF9A20LL},{18446744073709551615UL,0x9DFFBE5849968CB6LL,0x67E307B5CA9869ECLL,0x7BC75286E871D976LL,0xE96673E7ED6F1860LL,18446744073709551613UL,0xCBF224DB024A3770LL},{18446744073709551615UL,0x0640C903873DF49CLL,0x92280643B8A277EBLL,0x78332C681489ECE6LL,0xE96673E7ED6F1860LL,0x9BE7F56FE1B7E0A2LL,18446744073709551608UL},{0x279C4185A1FA147BLL,0xCBF224DB024A3770LL,0xAB6CE1A812C005D9LL,1UL,0x008CF5A8B060D2F3LL,0x004DDE9AB5C29478LL,0x9CD17CD9532955F1LL},{0UL,0x9CD17CD9532955F1LL,0xFA7AD11E7587697ALL,0x02FC3CDBE19BA7E9LL,1UL,0xAA6064292BB69CDDLL,18446744073709551613UL},{0xEF968E539E95EEFELL,1UL,0x9BE7F56FE1B7E0A2LL,1UL,18446744073709551609UL,0x92280643B8A277EBLL,18446744073709551609UL}}};
    struct S0 *l_2478 = &g_29;
    int i, j, k;
    for (p_3 = (-9); (p_3 == 39); ++p_3)
    { /* block id: 1007 */
        int32_t *l_2108 = (void*)0;
        uint8_t *l_2120 = &g_123;
        int16_t *l_2123 = &g_135;
        int16_t *l_2129[9][3] = {{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151},{&g_151,&g_151,&g_151}};
        struct S0 *l_2130 = (void*)0;
        int32_t l_2133 = 0xD34329DDL;
        int i, j;
        l_2108 = l_2108;
        l_2133 = (((0x8E1E39A7L & (safe_mul_func_uint8_t_u_u((safe_add_func_int16_t_s_s((((safe_add_func_uint32_t_u_u((p_3 == ((!(**g_1652)) & 0x36A6EE62L)), (safe_mod_func_int32_t_s_s(0xDAD392CBL, 0x9F94B9C4L)))) , (*g_417)) == l_2132), (-10L))), p_3))) & p_3) != 0x883F73ACD3D779CDLL);
        for (g_174 = (-13); (g_174 == 7); ++g_174)
        { /* block id: 1016 */
            l_2136 = (void*)0;
        }
    }
    for (g_113 = 0; (g_113 < 56); ++g_113)
    { /* block id: 1022 */
        uint64_t l_2139[7][6] = {{18446744073709551607UL,0xB74767495E07A4C6LL,18446744073709551607UL,0xB54A939CBC4D362BLL,18446744073709551607UL,0xB74767495E07A4C6LL},{0xF6218D9FFCA74D53LL,0xB74767495E07A4C6LL,0UL,0xB74767495E07A4C6LL,0xF6218D9FFCA74D53LL,0xB74767495E07A4C6LL},{18446744073709551607UL,0xB54A939CBC4D362BLL,18446744073709551607UL,0xB74767495E07A4C6LL,18446744073709551607UL,0xB54A939CBC4D362BLL},{0xF6218D9FFCA74D53LL,0xB54A939CBC4D362BLL,0UL,0xB54A939CBC4D362BLL,0xF6218D9FFCA74D53LL,0xB54A939CBC4D362BLL},{18446744073709551607UL,0xB74767495E07A4C6LL,18446744073709551607UL,0xB54A939CBC4D362BLL,18446744073709551607UL,0xB74767495E07A4C6LL},{0xF6218D9FFCA74D53LL,0xB74767495E07A4C6LL,0UL,0xB74767495E07A4C6LL,0xF6218D9FFCA74D53LL,0xB74767495E07A4C6LL},{18446744073709551607UL,0xB54A939CBC4D362BLL,18446744073709551607UL,0xB74767495E07A4C6LL,18446744073709551607UL,0xB54A939CBC4D362BLL}};
        int32_t *l_2145 = &g_687;
        uint32_t ***l_2153 = &g_913[4][3];
        uint32_t l_2160 = 18446744073709551615UL;
        struct S0 l_2168 = {0x324EDDBEL};
        int32_t **l_2175[4] = {&g_538[0],&g_538[0],&g_538[0],&g_538[0]};
        int32_t ** const *l_2174[7][8][3] = {{{&l_2175[1],&l_2175[0],&l_2175[1]},{&l_2175[0],&l_2175[0],&l_2175[3]},{&l_2175[0],&l_2175[0],(void*)0},{&l_2175[0],&l_2175[0],&l_2175[0]},{(void*)0,&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],(void*)0,&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]}},{{&l_2175[1],(void*)0,(void*)0},{&l_2175[0],&l_2175[0],&l_2175[3]},{&l_2175[1],&l_2175[0],&l_2175[1]},{&l_2175[0],&l_2175[0],&l_2175[3]},{&l_2175[0],&l_2175[0],(void*)0},{&l_2175[0],&l_2175[0],&l_2175[0]},{(void*)0,&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[3]}},{{(void*)0,&l_2175[1],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[1],&l_2175[1]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{(void*)0,(void*)0,&l_2175[1]},{&l_2175[3],&l_2175[0],&l_2175[0]}},{{&l_2175[1],&l_2175[0],&l_2175[0]},{&l_2175[3],&l_2175[0],&l_2175[3]},{(void*)0,&l_2175[1],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[1],&l_2175[1]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]}},{{(void*)0,(void*)0,&l_2175[1]},{&l_2175[3],&l_2175[0],&l_2175[0]},{&l_2175[1],&l_2175[0],&l_2175[0]},{&l_2175[3],&l_2175[0],&l_2175[3]},{(void*)0,&l_2175[1],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[1],&l_2175[1]},{&l_2175[0],&l_2175[0],&l_2175[0]}},{{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{(void*)0,(void*)0,&l_2175[1]},{&l_2175[3],&l_2175[0],&l_2175[0]},{&l_2175[1],&l_2175[0],&l_2175[0]},{&l_2175[3],&l_2175[0],&l_2175[3]},{(void*)0,&l_2175[1],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]}},{{&l_2175[0],&l_2175[1],&l_2175[1]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{&l_2175[0],&l_2175[0],&l_2175[0]},{(void*)0,(void*)0,&l_2175[1]},{&l_2175[3],&l_2175[0],&l_2175[0]},{&l_2175[1],&l_2175[0],&l_2175[0]},{&l_2175[3],&l_2175[0],&l_2175[3]}}};
        int32_t l_2248[10][2][7] = {{{0x896A47C3L,(-10L),0L,(-10L),0x896A47C3L,9L,0x29FA82A8L},{(-1L),0x1CD81882L,1L,1L,0x05A36907L,1L,1L}},{{0x29FA82A8L,0x29FA82A8L,(-1L),0L,0xCDC7EBB3L,0x2897B070L,(-2L)},{(-1L),1L,(-1L),(-1L),1L,(-1L),0x05A36907L}},{{0x896A47C3L,(-1L),(-10L),0x71EEDBA6L,0xCDC7EBB3L,0xCDC7EBB3L,0x71EEDBA6L},{0x115234F6L,3L,0x115234F6L,0x96EC09C2L,0x05A36907L,6L,(-1L)}},{{(-10L),(-1L),0x896A47C3L,0x2897B070L,0x896A47C3L,(-1L),(-10L)},{(-1L),1L,(-1L),0x05A36907L,0xAC0EA7F3L,6L,0xAC0EA7F3L}},{{(-1L),0x29FA82A8L,0x29FA82A8L,(-1L),0L,0xCDC7EBB3L,0x2897B070L},{1L,0x1CD81882L,(-1L),0x115234F6L,0x115234F6L,(-1L),0x1CD81882L}},{{9L,0x39AA5134L,(-2L),(-1L),0x2897B070L,(-10L),(-10L)},{(-1L),3L,1L,3L,(-1L),(-1L),0x96EC09C2L}},{{0L,0x896A47C3L,0x39AA5134L,(-1L),0xCDC7EBB3L,(-1L),0x39AA5134L},{0x96EC09C2L,0x96EC09C2L,0xAC0EA7F3L,1L,6L,1L,0x115234F6L}},{{0L,(-1L),0x29FA82A8L,0x29FA82A8L,(-1L),0L,0xCDC7EBB3L},{(-1L),0xAC0EA7F3L,3L,0x1CD81882L,6L,6L,0x1CD81882L}},{{9L,0x71EEDBA6L,9L,(-10L),0xCDC7EBB3L,(-2L),0L},{3L,0xAC0EA7F3L,(-1L),1L,(-1L),0xAC0EA7F3L,3L}},{{0x29FA82A8L,(-1L),0L,0xCDC7EBB3L,0x2897B070L,(-2L),0x2897B070L},{0xAC0EA7F3L,0x96EC09C2L,0x96EC09C2L,0xAC0EA7F3L,1L,6L,1L}}};
        uint32_t l_2257 = 0xE11565DFL;
        uint32_t l_2326 = 0x6ECACC11L;
        uint32_t l_2346 = 18446744073709551609UL;
        int32_t ***l_2348[8];
        int32_t ****l_2347 = &l_2348[6];
        uint8_t l_2363 = 0xE5L;
        uint32_t l_2472 = 18446744073709551615UL;
        uint64_t l_2475 = 0xDA9BE8BA66F8A59DLL;
        int i, j, k;
        for (i = 0; i < 8; i++)
            l_2348[i] = (void*)0;
    }
    return l_2478;
}


/* ------------------------------------------ */
/* 
 * reads : g_2034 g_683 g_397 g_22 g_2058 g_135 g_1472 g_59 g_1652 g_1653 g_1174 g_417 g_418 g_28 g_534 g_1394 g_47 g_1613 g_1614 g_684 g_97 g_131 g_29.f0 g_113 g_142 g_26 g_151 g_8 g_174 g_44 g_175 g_229 g_125 g_1225 g_1612 g_423
 * writes: g_97 g_538 g_2034 g_684 g_2058 g_806 g_113 g_1174 g_534 g_47 g_131 g_135 g_142 g_151 g_174 g_177 g_208 g_418 g_22 g_1225
 */
static uint64_t  func_16(struct S0 * p_17, struct S0  p_18, uint32_t  p_19, int16_t  p_20)
{ /* block id: 982 */
    int32_t *l_2027 = &g_22;
    int32_t *l_2028 = &g_397;
    int32_t l_2029 = 2L;
    int32_t *l_2030 = &g_47;
    int32_t *l_2031 = &g_22;
    int32_t *l_2032 = &g_22;
    int32_t *l_2033[2][5][6] = {{{&g_47,&g_907,&g_1472[0],(void*)0,(void*)0,(void*)0},{&g_1472[0],&g_907,&g_907,&g_1472[0],&g_22,(void*)0},{&g_1472[0],&g_22,(void*)0,(void*)0,&g_907,(void*)0},{&g_47,(void*)0,&g_907,&g_174,&g_907,(void*)0},{(void*)0,&g_22,&g_1472[0],&g_174,&g_22,&g_907}},{{&g_47,&g_907,&g_1472[0],(void*)0,(void*)0,(void*)0},{&g_1472[0],&g_907,&g_907,&g_1472[0],&g_22,(void*)0},{&g_1472[0],&g_22,(void*)0,(void*)0,&g_907,(void*)0},{&g_47,(void*)0,&g_907,&g_174,&g_907,(void*)0},{(void*)0,&g_22,&g_1472[0],&g_174,&g_22,&g_907}}};
    int64_t l_2057 = 1L;
    struct S0 ****l_2063 = &g_417;
    uint32_t l_2081 = 1UL;
    const uint64_t l_2103[2] = {1UL,1UL};
    int i, j, k;
    g_538[0] = (g_97 = &g_1472[0]);
    g_2034++;
    if (((p_19 , ((safe_mod_func_uint16_t_u_u(((((safe_sub_func_int8_t_s_s(3L, ((((safe_add_func_uint8_t_u_u(((safe_sub_func_uint32_t_u_u(((safe_rshift_func_uint8_t_u_u((0L > ((safe_mul_func_uint8_t_u_u(((((safe_sub_func_int64_t_s_s(((*g_683) = (&l_2032 == (void*)0)), (safe_add_func_int32_t_s_s((safe_sub_func_int8_t_s_s((*l_2028), (safe_mul_func_int8_t_s_s(l_2057, (*l_2032))))), (((((g_2058 , p_19) && p_19) | 18446744073709551612UL) <= 0xB48A9F3705A2CB6ALL) ^ (-1L)))))) < 0x4295L) ^ g_135) , 0x1EL), 0xE0L)) && p_19)), g_1472[0])) >= 3UL), 0x48CA1A2BL)) >= p_18.f0), 255UL)) ^ 252UL) == g_59) , p_18.f0))) ^ 0UL) == 0x7E9B8D11L) , (**g_1652)), 0x094CL)) != p_18.f0)) >= 0xEAD59A97L))
    { /* block id: 987 */
        uint16_t *l_2064 = &g_534;
        int32_t l_2067 = 0x95F9DD2BL;
        struct S0 *l_2069 = (void*)0;
        struct S0 **l_2068 = &l_2069;
        int64_t l_2073[6] = {0L,0L,0xBC2E722D5A2771A5LL,0L,0L,0xBC2E722D5A2771A5LL};
        int32_t l_2074 = 0xA52C6821L;
        int32_t l_2075 = 0xE188A1CFL;
        int32_t l_2076 = (-5L);
        int32_t l_2077 = 0x507048F0L;
        int32_t l_2078 = 0xB1F54FB5L;
        int32_t l_2079[9] = {0x46947CFFL,(-1L),0x46947CFFL,(-1L),0x46947CFFL,(-1L),0x46947CFFL,(-1L),0x46947CFFL};
        int32_t l_2080 = 0x5330EA55L;
        int i;
        for (g_2058 = 0; g_2058 < 1; g_2058 += 1)
        {
            g_538[g_2058] = &l_2029;
        }
        for (g_806 = 0; g_806 < 6; g_806 += 1)
        {
            for (g_113 = 0; g_113 < 4; g_113 += 1)
            {
                g_1174[g_806][g_113] = 0xAE84L;
            }
        }
        (*l_2027) = (safe_sub_func_uint32_t_u_u(((safe_lshift_func_uint8_t_u_u((l_2063 != (void*)0), 3)) , (((**l_2063) = func_85(g_397, (***l_2063), func_66(p_19, (--(*l_2064)), l_2067, (&p_17 != (l_2068 = &p_17)), (((+(safe_lshift_func_int8_t_s_s((((p_18.f0 == (*g_1394)) > (*g_1613)) , 0x60L), 4))) , (*l_2028)) <= (*g_683))), (*l_2032))) != &l_2069)), l_2067));
        l_2081++;
    }
    else
    { /* block id: 995 */
        uint8_t l_2098[8] = {0UL,0UL,255UL,0UL,0UL,255UL,0UL,0UL};
        uint64_t l_2104 = 18446744073709551607UL;
        int i;
        l_2104 |= (((*p_17) , ((g_1225[4][1]--) >= ((safe_add_func_uint8_t_u_u((safe_div_func_uint16_t_u_u((((*g_683) = (safe_mul_func_int16_t_s_s((func_66(g_1612[7][3], (((safe_sub_func_uint8_t_u_u((safe_add_func_int64_t_s_s((-1L), p_18.f0)), ((9UL != (safe_add_func_uint64_t_u_u(l_2098[2], (((*g_423) == (*g_423)) == (safe_mod_func_int16_t_s_s((safe_sub_func_int8_t_s_s(((((*g_229) = ((l_2098[2] || p_20) <= p_18.f0)) & (*l_2030)) || l_2098[4]), l_2098[2])), 4UL)))))) == 3L))) , (*g_683)) , l_2103[0]), p_20, p_20, p_20) , l_2098[0]), l_2098[0]))) || p_20), 0x86C9L)), p_19)) || l_2098[2]))) == 1L);
        return l_2098[7];
    }
    return p_18.f0;
}


/* ------------------------------------------ */
/* 
 * reads : g_1277 g_1278 g_417 g_418 g_28
 * writes: g_538
 */
static struct S0 * func_23(uint32_t  p_24)
{ /* block id: 978 */
    int32_t * const l_2024[8][5][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}}};
    int32_t **l_2025 = (void*)0;
    int32_t **l_2026 = &g_538[0];
    int i, j, k;
    (*l_2026) = l_2024[6][3][2];
    return (****g_1277);
}


/* ------------------------------------------ */
/* 
 * reads : g_26.f0 g_59 g_22 g_538 g_47
 * writes: g_22 g_47 g_59
 */
static int16_t  func_31(int8_t  p_32, int8_t  p_33, struct S0 ** p_34, int32_t  p_35, int32_t * p_36)
{ /* block id: 6 */
    int32_t l_45[2][6][10] = {{{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L}},{{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L},{0xEB7BFBC3L,(-1L),0xCC3592E7L,(-1L),0xEB7BFBC3L,0xEB7BFBC3L,0x5D1CC0C7L,(-4L),0x5D1CC0C7L,(-1L)},{(-1L),0x5D1CC0C7L,(-4L),0x5D1CC0C7L,(-1L),(-1L),0x5D1CC0C7L,(-4L),0x5D1CC0C7L,(-1L)}}};
    int32_t l_58 = 0xDD571B4DL;
    int16_t l_1586 = 0xCA08L;
    int32_t l_1588 = 0L;
    struct S0 **** const l_1620 = (void*)0;
    int32_t l_1639 = 1L;
    int32_t **l_1650 = &g_538[0];
    uint16_t ***l_1680[4];
    int16_t l_1688 = 1L;
    int16_t * const *l_1696 = &g_229;
    uint64_t l_1701[1];
    int32_t l_1724[10][6] = {{(-1L),1L,(-1L),0x811DD694L,1L,0x811DD694L},{(-1L),0xAC9B8AF3L,(-1L),0x811DD694L,0xAC9B8AF3L,(-1L)},{(-1L),(-1L),0x811DD694L,0x811DD694L,(-1L),(-1L)},{(-1L),1L,(-1L),0x811DD694L,1L,0x811DD694L},{(-1L),0xAC9B8AF3L,(-1L),0x811DD694L,0xAC9B8AF3L,(-1L)},{(-1L),(-1L),0x811DD694L,0x811DD694L,(-1L),(-1L)},{(-1L),1L,(-1L),0x811DD694L,1L,0x811DD694L},{(-1L),0xAC9B8AF3L,(-1L),0x811DD694L,0xAC9B8AF3L,(-1L)},{(-1L),(-1L),0x811DD694L,0x811DD694L,(-1L),(-1L)},{(-1L),1L,(-1L),0x811DD694L,1L,0x811DD694L}};
    const uint64_t l_1788 = 0UL;
    uint32_t *l_1818 = &g_554;
    uint8_t *l_1835[4][6] = {{(void*)0,(void*)0,&g_59,(void*)0,&g_1225[4][1],(void*)0},{(void*)0,(void*)0,(void*)0,&g_123,&g_59,&g_59},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_59,(void*)0,(void*)0,&g_123}};
    struct S0 ***l_1859 = &g_418;
    int64_t *l_1923 = &g_684;
    uint64_t l_1926 = 0x5B7F945F1DC47F50LL;
    uint16_t l_1967[3][2] = {{6UL,6UL},{6UL,6UL},{6UL,6UL}};
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_1680[i] = &g_1652;
    for (i = 0; i < 1; i++)
        l_1701[i] = 1UL;
    for (p_32 = 1; (p_32 >= 0); p_32 -= 1)
    { /* block id: 9 */
        uint32_t l_54 = 0xE0456BBBL;
        int32_t l_57[7] = {0xCB8161DBL,0xA2F90942L,0xCB8161DBL,0xCB8161DBL,0xA2F90942L,0xCB8161DBL,0xCB8161DBL};
        uint64_t l_118 = 18446744073709551615UL;
        uint32_t l_1590 = 18446744073709551615UL;
        const int16_t *l_1609 = &g_1610;
        const int16_t *l_1611 = &g_1612[7][3];
        struct S0 ***l_1638 = &g_418;
        int8_t l_1641 = 0L;
        int32_t *l_1645 = (void*)0;
        int32_t ** const *l_1672 = &l_1650;
        struct S0 *** const **l_1708[9] = {&g_1278,&g_1278,&g_1278,&g_1278,&g_1278,&g_1278,&g_1278,&g_1278,&g_1278};
        uint8_t *l_1717 = &g_1225[3][0];
        int32_t *l_1811 = &g_22;
        int8_t l_1854[7] = {0xE3L,0xE3L,0xE3L,0xE3L,0xE3L,0xE3L,0xE3L};
        int64_t *l_1986[3][3];
        uint64_t l_1993 = 18446744073709551615UL;
        int i, j;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
                l_1986[i][j] = &g_684;
        }
        (*p_36) = l_45[0][5][1];
        for (g_22 = 1; (g_22 >= 0); g_22 -= 1)
        { /* block id: 13 */
            int32_t *l_46 = &g_47;
            int32_t *l_48 = (void*)0;
            int32_t *l_49 = &g_47;
            int32_t *l_50 = (void*)0;
            int32_t *l_51 = &g_47;
            int32_t *l_52 = &g_47;
            int32_t *l_53[9][1][9] = {{{&g_22,&g_47,&g_47,&g_22,(void*)0,&g_47,&g_47,(void*)0,&g_22}},{{&g_47,&g_47,&g_47,&g_22,&g_47,&g_47,(void*)0,&g_47,(void*)0}},{{&g_22,&g_47,&g_22,&g_47,&g_47,&g_47,&g_47,(void*)0,&g_47}},{{&g_47,(void*)0,(void*)0,&g_22,&g_22,&g_47,&g_47,&g_22,(void*)0}},{{&g_47,&g_22,&g_22,&g_22,&g_47,(void*)0,&g_47,&g_22,&g_22}},{{&g_22,&g_22,&g_47,(void*)0,&g_47,&g_22,(void*)0,(void*)0,&g_22}},{{&g_47,(void*)0,&g_22,(void*)0,(void*)0,&g_22,&g_22,&g_47,&g_47}},{{(void*)0,&g_47,&g_22,&g_22,&g_47,&g_22,&g_47,&g_47,&g_22}},{{(void*)0,&g_47,(void*)0,&g_47,(void*)0,(void*)0,&g_47,(void*)0,&g_47}}};
            int32_t **l_1633 = (void*)0;
            uint32_t l_1642 = 0x322B02BFL;
            int i, j, k;
            --l_54;
            g_47 = g_26.f0;
            g_59++;
            for (l_58 = 0; (l_58 <= 1); l_58 += 1)
            { /* block id: 19 */
                uint32_t *l_119 = &l_54;
                uint8_t *l_122 = &g_123;
                uint8_t *l_124[1];
                uint16_t *l_1582 = (void*)0;
                uint16_t *l_1583 = &g_1174[1][0];
                int32_t l_1585 = 1L;
                int64_t l_1615[3];
                struct S0 ***l_1637 = &g_418;
                int32_t **l_1649 = &g_538[0];
                int i, j, k;
                for (i = 0; i < 1; i++)
                    l_124[i] = &g_125;
                for (i = 0; i < 3; i++)
                    l_1615[i] = 0L;
                for (p_35 = 0; (p_35 <= 1); p_35 += 1)
                { /* block id: 22 */
                    int i, j, k;
                    return l_45[p_35][(p_32 + 1)][g_22];
                }
                if (l_45[p_32][(g_22 + 3)][(g_22 + 4)])
                    break;
            }
        }
    }
    return (**l_1650);
}


/* ------------------------------------------ */
/* 
 * reads : g_1394 g_47
 * writes: g_47
 */
static struct S0  func_66(int8_t  p_67, const uint16_t  p_68, int32_t  p_69, uint64_t  p_70, uint64_t  p_71)
{ /* block id: 729 */
    uint8_t l_1574 = 1UL;
    int32_t *l_1575 = &g_1472[0];
    int32_t *l_1576 = &g_1472[0];
    int32_t *l_1577[1];
    uint64_t l_1578 = 0xF18192CF1243F6E6LL;
    struct S0 l_1581 = {1L};
    int i;
    for (i = 0; i < 1; i++)
        l_1577[i] = &g_1472[0];
    (*g_1394) = l_1574;
    (*g_1394) |= l_1574;
    l_1578++;
    return l_1581;
}


/* ------------------------------------------ */
/* 
 * reads : g_1394 g_47
 * writes: g_47
 */
static const int32_t  func_74(uint16_t  p_75, int8_t  p_76, uint32_t  p_77, uint16_t  p_78, uint16_t  p_79)
{ /* block id: 718 */
    int32_t *l_1563[3][10][2] = {{{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]}},{{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]}},{{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]},{&g_1472[0],&g_1472[0]}}};
    int32_t **l_1564 = &l_1563[1][0][0];
    uint32_t l_1565 = 0x21B5B421L;
    uint8_t l_1571 = 9UL;
    int i, j, k;
    (*l_1564) = l_1563[1][0][0];
    l_1565 ^= (*g_1394);
    for (g_47 = 13; (g_47 > 17); g_47 = safe_add_func_int16_t_s_s(g_47, 8))
    { /* block id: 723 */
        uint16_t l_1568 = 0x6FF7L;
        if (p_75)
            break;
        l_1568++;
    }
    ++l_1571;
    return p_76;
}


/* ------------------------------------------ */
/* 
 * reads : g_334 g_866 g_8 g_683 g_684 g_123 g_423 g_418 g_517 g_22 g_1277 g_892 g_229 g_131 g_1394 g_1405 g_1278 g_538 g_47 g_44 g_151 g_422 g_1440 g_500 g_1472 g_175 g_142 g_687 g_174 g_1225 g_370 g_26.f0 g_1548
 * writes: g_866 g_334 g_123 g_28 g_517 g_1277 g_131 g_538 g_370 g_47 g_684 g_1472 g_500 g_174 g_177 g_661 g_978
 */
static uint32_t  func_80(struct S0 ** p_81, int32_t * p_82, uint32_t  p_83, int8_t * p_84)
{ /* block id: 464 */
    int64_t *l_1031 = (void*)0;
    int64_t *l_1038 = &g_684;
    int32_t l_1039 = 0x80E221C9L;
    int32_t l_1040 = (-7L);
    int32_t *l_1041[4][6][3] = {{{(void*)0,&g_687,&g_892[0][0]},{&g_892[4][0],&g_687,&g_892[1][0]},{&g_687,(void*)0,&g_687},{&g_892[0][0],&g_892[0][0],&g_892[4][0]},{&g_687,(void*)0,&g_892[1][0]},{&g_892[3][0],(void*)0,&g_892[0][0]}},{{&g_687,(void*)0,&g_892[4][0]},{(void*)0,&g_892[3][0],&g_892[0][0]},{&g_892[0][0],&g_687,&g_892[1][0]},{&g_892[4][0],(void*)0,&g_892[4][0]},{(void*)0,(void*)0,&g_687},{&g_687,&g_892[4][0],&g_892[1][0]}},{{&g_687,(void*)0,&g_892[0][0]},{&g_687,(void*)0,(void*)0},{&g_687,&g_892[3][0],&g_892[3][0]},{(void*)0,&g_687,&g_892[0][0]},{&g_892[4][0],(void*)0,(void*)0},{&g_892[0][0],(void*)0,&g_687}},{{(void*)0,&g_687,&g_892[1][0]},{&g_687,(void*)0,&g_892[3][0]},{&g_892[3][0],(void*)0,&g_892[0][0]},{&g_687,&g_687,&g_687},{&g_892[0][0],&g_892[3][0],&g_892[0][0]},{&g_687,(void*)0,&g_892[4][0]}}};
    int32_t l_1055 = 1L;
    int32_t l_1060 = 0L;
    int32_t l_1063 = 1L;
    int32_t l_1064[4][5] = {{0x7294881BL,0xDF4ED849L,0xDF4ED849L,0x7294881BL,0xDF4ED849L},{0x7294881BL,0x7294881BL,0L,0x7294881BL,0x7294881BL},{0xDF4ED849L,0x7294881BL,0xDF4ED849L,0xDF4ED849L,0x7294881BL},{0x7294881BL,0xDF4ED849L,0xDF4ED849L,0x7294881BL,0xDF4ED849L}};
    uint8_t *l_1110[10][5][5] = {{{&g_123,&g_298,&g_298,&g_123,&g_125},{(void*)0,(void*)0,(void*)0,&g_59,&g_125},{&g_125,(void*)0,(void*)0,&g_298,&g_123},{&g_298,&g_123,(void*)0,&g_59,&g_123},{(void*)0,&g_59,(void*)0,&g_125,&g_123}},{{&g_298,(void*)0,(void*)0,&g_59,(void*)0},{&g_59,(void*)0,&g_298,&g_298,&g_59},{&g_59,&g_123,&g_123,&g_125,&g_123},{&g_298,&g_59,&g_125,(void*)0,&g_59},{&g_59,&g_125,&g_123,(void*)0,(void*)0}},{{&g_298,&g_123,&g_125,&g_125,&g_123},{(void*)0,&g_123,&g_123,&g_298,&g_298},{(void*)0,&g_123,&g_125,&g_59,(void*)0},{&g_125,&g_123,&g_123,&g_125,&g_123},{(void*)0,&g_125,&g_298,&g_59,&g_123}},{{(void*)0,&g_59,(void*)0,&g_298,(void*)0},{&g_298,&g_123,(void*)0,&g_59,&g_298},{&g_59,(void*)0,(void*)0,&g_125,&g_123},{&g_298,(void*)0,(void*)0,&g_59,(void*)0},{&g_59,(void*)0,&g_298,&g_298,&g_59}},{{&g_59,&g_123,&g_123,&g_125,&g_123},{&g_298,&g_59,&g_125,(void*)0,&g_59},{&g_59,&g_125,&g_123,(void*)0,(void*)0},{&g_298,&g_123,&g_125,&g_125,&g_123},{(void*)0,&g_123,&g_123,&g_298,&g_298}},{{(void*)0,&g_123,&g_125,&g_59,(void*)0},{&g_125,&g_123,&g_123,&g_125,&g_123},{(void*)0,&g_125,&g_298,&g_59,&g_123},{(void*)0,&g_59,(void*)0,&g_298,(void*)0},{&g_298,&g_123,(void*)0,&g_59,&g_298}},{{&g_59,(void*)0,(void*)0,&g_125,&g_123},{&g_298,(void*)0,(void*)0,&g_59,(void*)0},{&g_59,(void*)0,&g_298,&g_298,&g_59},{&g_59,&g_123,&g_123,&g_125,&g_123},{&g_298,&g_59,&g_125,(void*)0,&g_59}},{{&g_59,&g_125,&g_123,(void*)0,(void*)0},{&g_298,&g_123,&g_125,&g_125,&g_123},{(void*)0,&g_123,&g_123,&g_298,&g_298},{(void*)0,&g_123,&g_125,&g_59,(void*)0},{&g_125,&g_123,&g_123,&g_125,&g_123}},{{(void*)0,&g_125,&g_298,&g_59,&g_123},{(void*)0,&g_59,(void*)0,&g_298,(void*)0},{&g_298,&g_123,(void*)0,&g_298,&g_125},{(void*)0,&g_298,(void*)0,&g_125,&g_123},{(void*)0,(void*)0,&g_59,(void*)0,&g_298}},{{&g_298,&g_298,&g_123,(void*)0,(void*)0},{&g_298,&g_59,&g_125,&g_125,&g_125},{(void*)0,&g_59,&g_125,&g_123,(void*)0},{(void*)0,(void*)0,&g_59,&g_123,&g_298},{&g_125,&g_125,(void*)0,&g_125,&g_123}}};
    struct S0 **** const * const l_1281 = &g_661;
    uint16_t l_1312 = 0xB109L;
    int8_t *l_1337 = &g_866[2];
    int32_t *l_1396[3][3][10] = {{{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]},{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]},{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]}},{{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]},{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]},{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]}},{{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]},{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]},{&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0],&g_174,(void*)0,(void*)0,&g_174,&l_1064[3][0]}}};
    const struct S0 **l_1403 = (void*)0;
    const struct S0 ***l_1402 = &l_1403;
    const struct S0 ****l_1401[5];
    uint64_t l_1450 = 18446744073709551607UL;
    int32_t *l_1489[8][6] = {{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0},{&l_1039,(void*)0,(void*)0,&l_1039,(void*)0,(void*)0}};
    uint32_t *l_1553 = (void*)0;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_1401[i] = &l_1402;
    if (((((l_1040 = ((safe_lshift_func_uint8_t_u_s((safe_lshift_func_int16_t_s_u((safe_div_func_uint16_t_u_u((l_1031 == (void*)0), (((safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_u((p_83 && g_334), ((void*)0 == &g_269))) , (((safe_mul_func_int8_t_s_s((l_1031 != l_1038), (l_1039 &= ((*p_84) = (*p_84))))) , p_83) != 0xA117C747784DB6E8LL)), 13)) ^ p_83) , 1UL))), 9)), l_1040)) | 0x7BL)) , g_8) != (*g_683)) > 0x659BL))
    { /* block id: 468 */
        int8_t l_1066 = (-9L);
        uint64_t l_1067 = 0xB6023067DD96E50CLL;
        int32_t *l_1071[5];
        const uint32_t *** const l_1253 = (void*)0;
        struct S0 *l_1270 = (void*)0;
        int8_t *l_1338 = &g_177;
        uint16_t l_1342 = 65527UL;
        uint64_t *l_1358 = (void*)0;
        int32_t **l_1360 = &l_1071[1];
        int32_t ***l_1359[1][1][1];
        uint16_t *l_1379 = &l_1312;
        uint16_t **l_1378[3];
        uint16_t ***l_1377 = &l_1378[2];
        int16_t l_1383 = (-7L);
        int32_t l_1393 = (-1L);
        struct S0 l_1420 = {0xE3E037DCL};
        int i, j, k;
        for (i = 0; i < 5; i++)
            l_1071[i] = (void*)0;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 1; k++)
                    l_1359[i][j][k] = &l_1360;
            }
        }
        for (i = 0; i < 3; i++)
            l_1378[i] = &l_1379;
        for (g_334 = 0; (g_334 <= 4); g_334 += 1)
        { /* block id: 471 */
            int64_t l_1049 = 0L;
            int32_t l_1057 = (-2L);
            int32_t l_1061 = (-2L);
            int32_t l_1062 = 0x8827D691L;
            int32_t l_1065 = (-4L);
            int32_t *l_1093 = &g_47;
            uint16_t l_1100 = 0x3932L;
            struct S0 ***l_1173[2][3][1] = {{{(void*)0},{&g_418},{(void*)0}},{{&g_418},{(void*)0},{&g_418}}};
            uint32_t **l_1210 = &g_914;
            int32_t l_1219 = 0x0A45381BL;
            int32_t l_1222 = 0L;
            int32_t l_1223 = (-6L);
            int32_t l_1224[4][5][3] = {{{0L,9L,0xCA25BD66L},{5L,0x7BCB54D6L,7L},{0L,0L,7L},{9L,0xCA25BD66L,7L},{5L,0x2F39E5EFL,5L}},{{5L,9L,0x2F39E5EFL},{9L,5L,5L},{0x2F39E5EFL,5L,7L},{0xCA25BD66L,9L,0L},{0x2F39E5EFL,0x2F39E5EFL,0L}},{{9L,0xCA25BD66L,7L},{5L,0x2F39E5EFL,5L},{5L,9L,0x2F39E5EFL},{9L,5L,5L},{0x2F39E5EFL,5L,7L}},{{0xCA25BD66L,9L,0L},{0x2F39E5EFL,0x2F39E5EFL,0L},{9L,0xCA25BD66L,7L},{5L,0x2F39E5EFL,5L},{5L,9L,0x2F39E5EFL}}};
            const uint32_t *l_1267 = &g_113;
            const uint32_t **l_1266 = &l_1267;
            int i, j, k;
            for (g_123 = 0; (g_123 <= 4); g_123 += 1)
            { /* block id: 474 */
                int32_t **l_1046 = &l_1041[2][2][1];
                int32_t l_1052 = 0x805B8643L;
                int32_t l_1053 = (-1L);
                int32_t l_1056 = 0xB30D4335L;
                int32_t l_1058 = 0x52F3EEC5L;
                int32_t l_1059[3];
                int8_t l_1175 = 0x83L;
                uint32_t l_1193 = 0xA7D74FDDL;
                uint32_t ***l_1254[6];
                int i;
                for (i = 0; i < 3; i++)
                    l_1059[i] = 6L;
                for (i = 0; i < 6; i++)
                    l_1254[i] = &g_913[5][3];
            }
        }
        (**g_423) = l_1270;
        for (g_517 = 0; (g_517 <= 0); g_517 += 1)
        { /* block id: 573 */
            struct S0 *** const ***l_1279 = (void*)0;
            struct S0 *** const ***l_1280 = &g_1277;
            int32_t l_1290 = 0L;
            int32_t l_1313 = 0xAA21DF9AL;
            uint16_t ***l_1380 = &l_1378[2];
            int64_t *l_1449[4][5] = {{&g_684,&g_684,&g_684,&g_684,&g_684},{&g_684,&g_684,&g_684,&g_684,&g_684},{&g_684,&g_684,&g_684,&g_684,&g_684},{&g_684,&g_684,&g_684,&g_684,&g_684}};
            int8_t *l_1479 = (void*)0;
            int16_t l_1497 = 8L;
            uint8_t l_1508[9] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
            int i, j;
            l_1290 = ((safe_lshift_func_int8_t_s_u(((*p_82) >= (safe_mul_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_u((((((*l_1280) = (((void*)0 == &g_208) , g_1277)) != l_1281) , ((safe_mod_func_int8_t_s_s((safe_rshift_func_int16_t_s_u(0xD381L, g_517)), (safe_unary_minus_func_int8_t_s(((!((safe_sub_func_uint16_t_u_u(l_1290, 0UL)) <= g_892[0][0])) <= g_892[3][0]))))) , l_1290)) & p_83), l_1290)) && 0xD1L), g_517))), 1)) , (*p_82));
            for (g_123 = 0; (g_123 <= 6); g_123 += 1)
            { /* block id: 578 */
                uint32_t *l_1291[9][8][3] = {{{&g_175,&g_175,&g_554},{&g_113,&g_554,(void*)0},{&g_175,(void*)0,&g_554},{&g_175,(void*)0,&g_554},{&g_175,&g_113,&g_175},{&g_175,&g_175,&g_175},{&g_113,&g_113,&g_554},{&g_175,&g_113,&g_554}},{{&g_113,&g_554,&g_554},{&g_554,&g_113,&g_554},{(void*)0,&g_113,&g_175},{&g_175,&g_175,(void*)0},{&g_113,&g_113,(void*)0},{&g_554,(void*)0,(void*)0},{&g_113,(void*)0,(void*)0},{(void*)0,&g_554,&g_175}},{{&g_554,&g_175,&g_554},{&g_175,(void*)0,&g_554},{(void*)0,&g_554,&g_554},{&g_175,&g_113,&g_554},{&g_554,&g_113,&g_175},{(void*)0,&g_113,&g_175},{&g_113,&g_175,&g_554},{&g_554,&g_175,&g_554}},{{&g_113,&g_113,(void*)0},{&g_175,&g_113,&g_554},{(void*)0,&g_113,&g_113},{&g_554,&g_554,(void*)0},{&g_113,(void*)0,&g_113},{&g_175,&g_175,&g_554},{&g_113,&g_554,(void*)0},{&g_175,(void*)0,&g_554}},{{&g_175,(void*)0,&g_554},{&g_175,&g_113,&g_175},{&g_175,&g_175,&g_175},{&g_113,&g_113,&g_554},{&g_175,&g_113,&g_554},{&g_113,&g_113,(void*)0},{&g_175,&g_113,&g_175},{(void*)0,&g_175,&g_113}},{{&g_113,&g_113,&g_113},{(void*)0,&g_554,(void*)0},{(void*)0,&g_554,(void*)0},{&g_175,&g_175,&g_113},{&g_175,&g_113,&g_113},{&g_554,&g_113,&g_175},{&g_175,(void*)0,(void*)0},{&g_113,(void*)0,&g_554}},{{&g_175,&g_113,&g_175},{&g_554,&g_113,&g_113},{&g_175,(void*)0,&g_554},{&g_175,&g_175,&g_554},{(void*)0,&g_175,&g_554},{(void*)0,(void*)0,&g_554},{&g_113,&g_113,&g_175},{(void*)0,&g_113,&g_113}},{{&g_175,(void*)0,&g_113},{&g_113,(void*)0,&g_113},{&g_175,&g_113,&g_175},{&g_554,&g_113,&g_554},{&g_113,&g_175,&g_554},{&g_113,&g_554,&g_554},{&g_113,&g_554,&g_554},{&g_113,&g_113,&g_113}},{{&g_554,&g_175,&g_175},{&g_175,&g_113,&g_554},{&g_113,&g_113,(void*)0},{&g_175,&g_113,&g_175},{(void*)0,&g_175,&g_113},{&g_113,&g_113,&g_113},{(void*)0,&g_554,(void*)0},{(void*)0,&g_554,(void*)0}}};
                struct S0 l_1308 = {7L};
                const int16_t l_1311 = 1L;
                int32_t l_1330 = (-1L);
                uint32_t l_1366 = 0x8B5E5992L;
                int i, j, k;
            }
            if (((l_1377 == l_1380) >= (*g_683)))
            { /* block id: 638 */
                const struct S0 l_1392 = {1L};
                const uint32_t l_1430 = 5UL;
                int32_t l_1431[2][9] = {{0x52FFD24EL,0xE319F582L,0xC98AE49CL,1L,1L,0xC98AE49CL,0xE319F582L,0x52FFD24EL,0xC98AE49CL},{0x52FFD24EL,0xE319F582L,0xC98AE49CL,1L,1L,0xC98AE49CL,0xE319F582L,0x52FFD24EL,0xC98AE49CL}};
                int i, j;
                if (((safe_mod_func_int64_t_s_s((((l_1383 |= p_83) , (((safe_rshift_func_uint8_t_u_s((l_1040 = l_1064[3][3]), (((*g_229) = (safe_rshift_func_int16_t_s_u(0L, (((4294967295UL <= (0x19L > 0x26L)) , (safe_mod_func_uint64_t_u_u(18446744073709551615UL, p_83))) & ((l_1392 , p_83) ^ (*g_229)))))) , l_1290))) || 0xB016D6F6L) >= 65535UL)) != l_1312), (*g_683))) == 65535UL))
                { /* block id: 642 */
                    if ((*p_82))
                        break;
                    if (l_1393)
                        break;
                }
                else
                { /* block id: 645 */
                    int32_t **l_1395 = &g_538[0];
                    const struct S0 *****l_1404 = &l_1401[0];
                    uint32_t *l_1421[6] = {&g_269,&g_269,&g_370,&g_269,&g_269,&g_370};
                    int i;
                    (*l_1395) = g_1394;
                    (*l_1360) = l_1396[2][2][7];
                    if ((((safe_mul_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((((*l_1404) = l_1401[1]) == (g_1405[1] , (*g_1277))), (safe_lshift_func_uint16_t_u_s((safe_div_func_uint32_t_u_u((g_131 , (g_370 = (18446744073709551607UL || (p_83 != ((((safe_lshift_func_uint16_t_u_s((safe_add_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s((((0xE4DDL >= 65535UL) > (l_1420 , p_83)) <= (*g_229)), 9)), 0x1CL)), (*g_229))) <= p_83) > 0x4AL) > l_1392.f0))))), (**l_1395))), 7)))), p_83)) != l_1392.f0) >= g_44))
                    { /* block id: 650 */
                        uint8_t l_1422 = 0x04L;
                        l_1422 = (**l_1395);
                        (*g_1394) &= ((*p_82) ^ (safe_mul_func_uint8_t_u_u(((safe_unary_minus_func_uint8_t_u((((safe_mul_func_uint16_t_u_u(g_151, (*g_229))) != (&p_81 != (((*g_683) < p_83) , ((((safe_mul_func_int8_t_s_s(0L, ((l_1313 <= p_83) && p_83))) ^ p_83) == p_83) , (*g_422))))) && l_1430))) >= (*g_683)), 9L)));
                    }
                    else
                    { /* block id: 653 */
                        if ((*p_82))
                            break;
                        l_1431[1][0] = ((void*)0 != &l_1281);
                        if ((**l_1395))
                            break;
                        (*l_1395) = &l_1064[1][2];
                    }
                }
                for (l_1060 = 2; (l_1060 <= 6); l_1060 += 1)
                { /* block id: 662 */
                    int8_t l_1442 = 0xC0L;
                    struct S0 l_1445 = {0x10ED79AFL};
                    uint64_t *l_1446 = &l_1067;
                    const int8_t l_1471 = 0xAAL;
                    int i, j;
                    (*g_1394) &= (safe_mod_func_int32_t_s_s((((safe_mul_func_uint16_t_u_u((g_892[(g_517 + 4)][g_517] & (safe_mul_func_uint16_t_u_u(p_83, ((p_83 | ((safe_sub_func_int8_t_s_s((g_1440[0][4][6] != &g_1441[0][8][0]), (((((l_1442 > (safe_mod_func_uint64_t_u_u(((*l_1446) = (l_1445 , 0x0CBFD4C513BC32ABLL)), ((safe_rshift_func_uint8_t_u_s(g_151, (*p_84))) && g_892[0][0])))) , l_1449[2][0]) != &g_684) >= (-1L)) || (-1L)))) != 0x5543F91102DC7403LL)) , 7UL)))), 0x2534L)) < l_1450) | 0xCC6127D2L), p_83));
                    (*g_1394) = (safe_mod_func_int64_t_s_s((g_1472[0] = ((*l_1038) = ((safe_rshift_func_int16_t_s_u((safe_add_func_int16_t_s_s((safe_mod_func_int64_t_s_s((((*l_1446) = (safe_sub_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((-3L), 0xEBL)), (safe_rshift_func_int16_t_s_u((*g_229), 6))))) > (p_83 > 0L)), (((*p_84) ^ p_83) && ((((((p_83 & (safe_add_func_uint64_t_u_u((safe_mod_func_int8_t_s_s((p_83 , l_1313), p_83)), 0xDCE7E8B92B6153B9LL))) != p_83) < (*p_82)) >= l_1471) == g_892[(g_517 + 4)][g_517]) | l_1442)))), (*g_229))), p_83)) , (*g_683)))), p_83));
                    for (g_500 = 0; (g_500 <= 0); g_500 += 1)
                    { /* block id: 671 */
                        int i;
                        if (g_1472[g_517])
                            break;
                    }
                    return g_175;
                }
            }
            else
            { /* block id: 676 */
                uint32_t l_1473 = 0x53CCFAA3L;
                int8_t *l_1480 = &g_500;
                (*g_1394) &= (*p_82);
                --l_1473;
                (*g_1394) &= (((safe_unary_minus_func_int64_t_s((safe_mul_func_int16_t_s_s((((l_1480 = l_1479) == &g_866[3]) , (&l_1290 == &l_1290)), (((l_1313 ^ (safe_lshift_func_uint16_t_u_s(((0x809BL && p_83) <= ((l_1290 = ((safe_div_func_uint64_t_u_u(((l_1253 != &g_913[5][3]) < l_1290), p_83)) || p_83)) < p_83)), 13))) == 0xEBAE4F85108A7C5DLL) < g_142))))) || l_1473) , l_1473);
            }
            (*g_1394) |= (0x078056B7L ^ (safe_mod_func_uint8_t_u_u(g_687, (safe_mod_func_int32_t_s_s(0x2F9263D8L, ((l_1489[5][0] == (void*)0) ^ ((p_83 != p_83) < 0xF0E6L)))))));
            for (l_1393 = 0; (l_1393 >= 0); l_1393 -= 1)
            { /* block id: 686 */
                uint8_t l_1496 = 0xF9L;
                int32_t l_1537 = (-6L);
                struct S0 ****l_1559[7] = {&g_417,&g_417,&g_417,&g_417,&g_417,&g_417,&g_417};
                int i;
                for (g_174 = 5; (g_174 >= 0); g_174 -= 1)
                { /* block id: 689 */
                    int16_t l_1492 = 0xD12DL;
                    uint32_t *l_1493 = &g_370;
                    int16_t *l_1495[7] = {&l_1383,&l_1383,&l_1383,&l_1383,&l_1383,&l_1383,&l_1383};
                    int32_t l_1498[3][7][6] = {{{1L,1L,0x9F42035DL,0x66B8B59DL,0xBDA0C787L,(-9L)},{0xF8629B76L,7L,0x97517DA0L,0x8AF23E6EL,0xB2C8DA93L,0x9F42035DL},{0x2B47648DL,0xF8629B76L,0x97517DA0L,0xCD940186L,1L,(-9L)},{0xA716512AL,0xCD940186L,0x9F42035DL,0x48C3B4D6L,1L,1L},{0x48C3B4D6L,1L,1L,(-4L),0x4F85BD2DL,1L},{(-1L),0x9F42035DL,0xA42A1628L,0x80C4A3E6L,0xFBB36563L,0x80C4A3E6L},{0xFBB36563L,0x21438863L,0xFBB36563L,5L,0xA716512AL,0x04EF4EAAL}},{{0x8AF23E6EL,(-5L),0xB2C8DA93L,0xA42A1628L,0x04EF4EAAL,0x4F85BD2DL},{0x08901188L,0x4F85BD2DL,0xA42A1628L,0xBDA0C787L,0x21438863L,1L},{(-4L),0x04EF4EAAL,0L,1L,0xA42A1628L,1L},{0x04EF4EAAL,0x80C4A3E6L,0xCD940186L,(-1L),0xA716512AL,7L},{0x48C3B4D6L,1L,0x08901188L,0x66B8B59DL,0L,0L},{0x9F42035DL,1L,1L,0x9F42035DL,0x66B8B59DL,0xBDA0C787L},{1L,(-9L),0xF8629B76L,0xA42A1628L,1L,0xCD940186L}},{{0x21438863L,0x9F42035DL,0x2B47648DL,(-4L),1L,0xB2C8DA93L},{0L,(-9L),0xA716512AL,0x4F85BD2DL,0x66B8B59DL,0xFBB36563L},{0x783600D6L,1L,0x48C3B4D6L,0x8AF23E6EL,0L,0xA42A1628L},{0xB2C8DA93L,1L,(-1L),0L,0xA716512AL,1L},{0xA42A1628L,0x80C4A3E6L,0xFBB36563L,0x80C4A3E6L,0xA42A1628L,0x9F42035DL},{3L,0x04EF4EAAL,0x8AF23E6EL,(-9L),0x21438863L,0x97517DA0L},{0xBDA0C787L,0x4F85BD2DL,7L,0x04EF4EAAL,(-8L),0x97517DA0L}}};
                    int8_t l_1562 = 2L;
                    int i, j, k;
                    if ((((((((((*l_1338) = (((*g_683) = (safe_lshift_func_int16_t_s_u((0x9D87L == (p_83 <= ((p_83 > (l_1492 = 18446744073709551613UL)) <= (((*l_1493) |= (g_1225[1][5] , 0xAED559D9L)) <= 0x34D362C1L)))), ((safe_unary_minus_func_int16_t_s((l_1290 = ((*g_229) = (0xE8C6L || (*g_229)))))) > p_83)))) & l_1496)) , l_1497) != (-8L)) >= l_1497) , &l_1041[1][0][0]) == (void*)0) | p_83) , (*g_1394)))
                    { /* block id: 696 */
                        uint16_t l_1499 = 3UL;
                        l_1499--;
                    }
                    else
                    { /* block id: 698 */
                        const uint64_t *l_1534 = &g_978;
                        const uint64_t **l_1533 = &l_1534;
                        const uint64_t ***l_1535 = &l_1533;
                        int32_t l_1536 = 6L;
                        const int32_t **l_1547 = (void*)0;
                        const int32_t *l_1557[3][10] = {{&g_892[0][0],(void*)0,(void*)0,(void*)0,&g_29.f0,(void*)0,(void*)0,&g_29.f0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_29.f0,(void*)0,(void*)0,(void*)0,&g_892[0][0],(void*)0,&g_892[0][0],(void*)0},{&g_687,&g_29.f0,&g_26.f0,&g_29.f0,&g_687,&g_26.f0,&g_892[0][0],(void*)0,&g_892[0][0],&g_892[0][0]}};
                        const int32_t **l_1556 = &l_1557[1][5];
                        struct S0 *****l_1558 = &g_661;
                        uint64_t *l_1560 = (void*)0;
                        uint64_t *l_1561 = &g_978;
                        int i, j;
                        l_1537 = (safe_div_func_uint64_t_u_u((((safe_rshift_func_int16_t_s_u(0xDC2DL, (safe_lshift_func_uint8_t_u_u((l_1508[0]++), p_83)))) && (*p_84)) && (((safe_mul_func_uint16_t_u_u(((((g_123 = l_1492) & (safe_unary_minus_func_int8_t_s((*p_84)))) == (safe_mod_func_int8_t_s_s(((((safe_add_func_int64_t_s_s(((+(!0x4DL)) == ((((safe_lshift_func_int8_t_s_s((!(safe_rshift_func_int16_t_s_s((safe_div_func_int16_t_s_s(p_83, (safe_lshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_s((safe_sub_func_int16_t_s_s(((*g_229) &= (&g_1441[0][2][0] != ((*l_1535) = l_1533))), l_1536)), l_1313)), 3)))), p_83))), 0)) , l_1496) ^ 0L) & g_1225[4][1])), (*g_683))) && 65528UL) && p_83) < g_26.f0), l_1313))) == l_1536), l_1508[1])) < (-10L)) || p_83)), l_1498[0][1][1]));
                        (*g_1394) ^= ((((safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(((~p_83) || p_83), 5)), (safe_mul_func_int16_t_s_s((*g_229), (((l_1547 == g_1548[5][4][2]) ^ (*g_229)) < (safe_rshift_func_int16_t_s_s((l_1498[1][5][0] = (+0x878BL)), (((void*)0 == l_1553) , l_1313)))))))) <= 0L) , 0xA1L) != (*p_84));
                        (*g_1394) = (safe_lshift_func_int8_t_s_s(((l_1556 != ((((((*l_1558) = (void*)0) != ((&p_81 != &p_81) , l_1559[1])) & (((*p_84) <= (((((*l_1561) = p_83) ^ l_1562) == ((*g_229) = (-4L))) & p_83)) && l_1562)) , p_83) , &l_1557[1][5])) <= p_83), (*p_84)));
                    }
                }
            }
        }
    }
    else
    { /* block id: 714 */
        g_538[0] = p_82;
    }
    return g_47;
}


/* ------------------------------------------ */
/* 
 * reads : g_97 g_22 g_131 g_135 g_29.f0 g_113 g_59 g_142 g_26 g_151 g_8 g_174 g_44 g_175 g_47 g_229 g_125 g_417 g_418 g_1472
 * writes: g_131 g_135 g_142 g_47 g_151 g_174 g_177 g_97 g_208
 */
static struct S0 ** func_85(int8_t  p_86, struct S0 * p_87, struct S0  p_88, uint32_t  p_89)
{ /* block id: 41 */
    uint32_t *l_140[6] = {&g_113,&g_113,&g_113,&g_113,&g_113,&g_113};
    int32_t l_171 = 0x358B857EL;
    int32_t l_273 = 0x2431AA5CL;
    uint8_t *l_281 = &g_125;
    int32_t l_406 = 0x3219D2F7L;
    struct S0 * const *l_415 = &g_28;
    struct S0 * const **l_414 = &l_415;
    struct S0 ****l_421 = &g_417;
    const uint32_t l_437 = 18446744073709551613UL;
    int16_t *l_472 = &g_131;
    int32_t l_484 = 0x47E36CFAL;
    int32_t l_512 = 0xE7EA1D27L;
    int32_t l_515 = 0x3F32B09EL;
    int32_t *l_539 = &l_273;
    int64_t l_616 = 0x3EEA663EB93BDE2CLL;
    int32_t l_688[1];
    uint32_t l_706 = 18446744073709551607UL;
    int16_t l_809 = 1L;
    struct S0 **l_893 = &g_28;
    uint8_t *l_943 = &g_123;
    uint8_t l_958 = 0xD4L;
    uint16_t *l_995 = &g_8;
    uint16_t **l_994[10][5] = {{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995},{&l_995,&l_995,&l_995,&l_995,&l_995}};
    uint32_t ***l_1010 = &g_913[4][6];
    uint32_t *l_1015 = &g_269;
    uint64_t *l_1024 = &g_142;
    int i, j;
    for (i = 0; i < 1; i++)
        l_688[i] = (-5L);
    if ((*g_97))
    { /* block id: 42 */
        struct S0 **l_126 = (void*)0;
        return l_126;
    }
    else
    { /* block id: 44 */
        int32_t l_127 = 0x2706CD28L;
        int16_t *l_130 = &g_131;
        int16_t *l_134 = &g_135;
        uint64_t *l_141 = &g_142;
        int32_t *l_143 = &g_47;
        uint32_t *l_155 = &g_113;
        uint8_t l_163 = 0x1BL;
        int8_t l_225 = 2L;
        struct S0 **l_247 = &g_28;
        if (((*l_143) = ((((l_127 & (-5L)) , ((*l_141) |= (((safe_mod_func_uint64_t_u_u(((((*l_130) |= p_86) <= g_22) ^ (safe_mod_func_int16_t_s_s(((*l_134) ^= p_89), (safe_div_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((l_140[5] != &p_89), ((g_29.f0 || p_86) <= g_113))), p_88.f0))))), l_127)) && (*g_97)) >= g_59))) != 0xDB14249AA0F805AALL) | (-1L))))
        { /* block id: 49 */
            uint16_t l_150 = 0xD9E3L;
            uint32_t *l_154 = &g_113;
            uint32_t **l_156 = &l_154;
            int32_t *l_172 = (void*)0;
            int32_t *l_173 = &g_174;
            int8_t *l_176 = &g_177;
            int32_t **l_224 = (void*)0;
            (*l_173) ^= ((safe_mul_func_uint16_t_u_u((((*p_87) , (((safe_sub_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u((g_151 &= l_150), (l_150 != (((*l_156) = (l_155 = l_154)) != (void*)0)))), ((safe_sub_func_uint16_t_u_u(p_88.f0, (safe_div_func_uint64_t_u_u((l_163 |= ((*l_141)++)), (~(safe_rshift_func_int8_t_s_s((((safe_sub_func_int32_t_s_s(((*l_143) = (safe_lshift_func_uint16_t_u_u(p_88.f0, g_29.f0))), ((0xA5L < g_59) || g_131))) >= g_8) != g_135), 5))))))) | 0L))) == g_131) , 0xCD18219F0035E4DCLL)) != l_171), p_88.f0)) ^ g_22);
            (*l_143) = (g_44 | ((*l_176) = g_175));
            for (p_89 = 0; (p_89 < 25); p_89 = safe_add_func_uint64_t_u_u(p_89, 8))
            { /* block id: 61 */
                int32_t l_193 = 0x12AA013FL;
                int32_t l_226 = 1L;
                for (g_47 = 5; (g_47 >= 0); g_47 -= 1)
                { /* block id: 64 */
                    int32_t **l_191 = &l_143;
                    uint8_t *l_194 = &l_163;
                    int32_t ***l_223[10][5] = {{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191},{&l_191,&l_191,&l_191,&l_191,&l_191}};
                    int i, j;
                    if ((p_86 >= (((safe_rshift_func_int8_t_s_u(((-4L) || ((~g_135) > (safe_add_func_int64_t_s_s(((safe_rshift_func_uint16_t_u_s((&g_29 != &p_88), 5)) >= (((*l_194) = ((safe_mul_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((((*l_191) = (g_97 = l_140[g_47])) != (void*)0), (((((+4UL) , l_171) , (void*)0) != &p_87) & 0x2539F82EC59D2773LL))), 0x50L)) && l_193)) > p_88.f0)), 0x7F55A02C1B4120CDLL)))), 7)) <= (-2L)) > l_171)))
                    { /* block id: 68 */
                        if (p_86)
                            break;
                        if (l_193)
                            continue;
                    }
                    else
                    { /* block id: 71 */
                        int32_t l_217 = 1L;
                        int32_t *l_218 = &l_171;
                        (*l_218) = ((*l_173) = (safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_sub_func_int64_t_s_s((g_47 || (safe_sub_func_int32_t_s_s((!(((*p_87) , (g_142 = (g_142 || ((g_208 = l_194) == (((safe_sub_func_uint16_t_u_u(g_175, (((safe_mul_func_int8_t_s_s(((((18446744073709551612UL == (!(((safe_add_func_int8_t_s_s((((*l_194) |= (!g_151)) <= g_113), (p_88.f0 != l_217))) == 0L) | l_171))) & 0x1B97L) <= (-3L)) , l_171), p_89)) ^ p_89) , g_8))) || g_135) , (void*)0))))) || l_171)), 0xA5094A51L))), 0x532663EEDCE34460LL)), 1)), 0L)), 255UL)));
                    }
                    if (((*l_173) = (safe_sub_func_uint32_t_u_u((&l_171 == ((18446744073709551611UL || ((safe_lshift_func_uint8_t_u_u(250UL, 2)) < g_26.f0)) , &g_22)), ((((p_86 < ((l_224 = (void*)0) == (l_171 , &g_97))) != g_131) | l_225) < l_226)))))
                    { /* block id: 80 */
                        uint8_t l_239 = 0xC8L;
                        l_239 = ((safe_add_func_uint64_t_u_u(p_89, (((g_229 == &g_135) != (4UL | 0xC37666566E71845DLL)) & ((*l_134) |= p_89)))) | (safe_mod_func_int8_t_s_s((((safe_rshift_func_uint8_t_u_s(l_226, (safe_add_func_int8_t_s_s((safe_unary_minus_func_int32_t_s((safe_div_func_uint32_t_u_u(0x7461C8C1L, p_89)))), 0x83L)))) & p_86) == p_86), g_125)));
                    }
                    else
                    { /* block id: 83 */
                        uint32_t l_240 = 18446744073709551615UL;
                        (*l_191) = &g_22;
                        ++l_240;
                        (*l_191) = &l_226;
                    }
                    if (p_88.f0)
                        continue;
                    (*l_173) = (l_226 = (((((*l_176) = 0x25L) ^ (((void*)0 != &l_140[0]) , 255UL)) != p_86) || ((*g_229) |= (0xACEA086712C952AELL == (((((((void*)0 == &l_163) >= ((safe_rshift_func_uint16_t_u_s(((safe_rshift_func_int8_t_s_u(p_86, p_86)) > l_171), p_86)) , p_88.f0)) , (void*)0) == &g_142) <= l_193) >= (*l_173))))));
                }
                return &g_28;
            }
        }
        else
        { /* block id: 96 */
            return l_247;
        }
    }
    for (g_131 = 1; (g_131 <= 5); g_131 += 1)
    { /* block id: 102 */
        int32_t l_249 = 0x4D4EC0B7L;
        uint16_t l_253[2][3][5] = {{{0xEBDAL,0x75F7L,0xEBDAL,0x75F7L,0xEBDAL},{0x155CL,0x155CL,0UL,0UL,0x155CL},{0x50B4L,0x75F7L,0x50B4L,0x75F7L,0x50B4L}},{{0x155CL,0UL,0UL,0x155CL,0x155CL},{0xEBDAL,0x75F7L,0xEBDAL,0x75F7L,0xEBDAL},{0x155CL,0x155CL,0UL,0UL,0x155CL}}};
        int i, j, k;
        for (g_47 = 0; (g_47 <= 5); g_47 += 1)
        { /* block id: 105 */
            int32_t **l_248 = &g_97;
            int32_t *l_250 = &l_171;
            int32_t *l_251 = &l_171;
            int32_t *l_252 = &g_174;
            int i;
            (*l_248) = l_140[g_47];
            ++l_253[1][0][4];
        }
    }
    return (**l_421);
}


/* ------------------------------------------ */
/* 
 * reads : g_22 g_44 g_47 g_113
 * writes: g_97 g_47 g_44 g_113
 */
static const int16_t  func_94(struct S0 * p_95)
{ /* block id: 26 */
    int32_t *l_96 = &g_22;
    int32_t *l_98 = &g_47;
    struct S0 *l_110 = &g_26;
    int8_t *l_115 = &g_44;
    g_97 = l_96;
    (*l_98) = (*l_96);
    for (g_44 = 0; (g_44 >= 5); g_44 = safe_add_func_int16_t_s_s(g_44, 6))
    { /* block id: 31 */
        struct S0 * const *l_107 = &g_28;
        uint8_t *l_111 = (void*)0;
        uint8_t *l_112[8] = {&g_59,&g_59,&g_59,&g_59,&g_59,&g_59,&g_59,&g_59};
        struct S0 **l_114 = (void*)0;
        uint16_t l_116 = 65534UL;
        int i;
        l_98 = (((safe_rshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s((*l_96), (((*l_98) | ((safe_add_func_uint8_t_u_u((((l_107 = l_107) != ((g_113 ^= (safe_lshift_func_int16_t_s_s((l_110 != (void*)0), 4))) , l_114)) >= ((void*)0 == l_115)), l_116)) && g_22)) || 65534UL))), l_116)) || (-1L)) , l_98);
        (*l_98) = l_116;
    }
    return (*l_96);
}




/* ---------------------------------------- */
//testcase_id 1484153378
int case1484153378(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_8, "g_8", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    transparent_crc(g_22, "g_22", print_hash_value);
    transparent_crc(g_26.f0, "g_26.f0", print_hash_value);
    transparent_crc(g_29.f0, "g_29.f0", print_hash_value);
    transparent_crc(g_44, "g_44", print_hash_value);
    transparent_crc(g_47, "g_47", print_hash_value);
    transparent_crc(g_59, "g_59", print_hash_value);
    transparent_crc(g_113, "g_113", print_hash_value);
    transparent_crc(g_123, "g_123", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_131, "g_131", print_hash_value);
    transparent_crc(g_135, "g_135", print_hash_value);
    transparent_crc(g_142, "g_142", print_hash_value);
    transparent_crc(g_151, "g_151", print_hash_value);
    transparent_crc(g_174, "g_174", print_hash_value);
    transparent_crc(g_175, "g_175", print_hash_value);
    transparent_crc(g_177, "g_177", print_hash_value);
    transparent_crc(g_269, "g_269", print_hash_value);
    transparent_crc(g_298, "g_298", print_hash_value);
    transparent_crc(g_334, "g_334", print_hash_value);
    transparent_crc(g_370, "g_370", print_hash_value);
    transparent_crc(g_397, "g_397", print_hash_value);
    transparent_crc(g_500, "g_500", print_hash_value);
    transparent_crc(g_517, "g_517", print_hash_value);
    transparent_crc(g_534, "g_534", print_hash_value);
    transparent_crc(g_554, "g_554", print_hash_value);
    transparent_crc(g_598, "g_598", print_hash_value);
    transparent_crc(g_684, "g_684", print_hash_value);
    transparent_crc(g_687, "g_687", print_hash_value);
    transparent_crc(g_806, "g_806", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_866[i], "g_866[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_892[i][j], "g_892[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_907, "g_907", print_hash_value);
    transparent_crc(g_978, "g_978", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_1174[i][j], "g_1174[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 8; j++)
        {
            transparent_crc(g_1225[i][j], "g_1225[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1405[i].f0, "g_1405[i].f0", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1472[i], "g_1472[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1610, "g_1610", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1612[i][j], "g_1612[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1614, "g_1614", print_hash_value);
    transparent_crc(g_1659, "g_1659", print_hash_value);
    transparent_crc(g_1879, "g_1879", print_hash_value);
    transparent_crc(g_1884, "g_1884", print_hash_value);
    transparent_crc(g_2034, "g_2034", print_hash_value);
    transparent_crc(g_2058, "g_2058", print_hash_value);
    transparent_crc(g_2270, "g_2270", print_hash_value);
    transparent_crc(g_2433, "g_2433", print_hash_value);
    transparent_crc(g_2480.f0, "g_2480.f0", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 580
   depth: 1, occurrence: 15
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 37
breakdown:
   depth: 1, occurrence: 119
   depth: 2, occurrence: 24
   depth: 3, occurrence: 2
   depth: 8, occurrence: 1
   depth: 12, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 4
   depth: 22, occurrence: 1
   depth: 23, occurrence: 2
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 27, occurrence: 1
   depth: 30, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 37, occurrence: 1

XXX total number of pointers: 502

XXX times a variable address is taken: 1111
XXX times a pointer is dereferenced on RHS: 335
breakdown:
   depth: 1, occurrence: 266
   depth: 2, occurrence: 47
   depth: 3, occurrence: 15
   depth: 4, occurrence: 6
   depth: 5, occurrence: 1
XXX times a pointer is dereferenced on LHS: 340
breakdown:
   depth: 1, occurrence: 307
   depth: 2, occurrence: 28
   depth: 3, occurrence: 3
   depth: 4, occurrence: 2
XXX times a pointer is compared with null: 49
XXX times a pointer is compared with address of another variable: 17
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 6576

XXX max dereference level: 6
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1224
   level: 2, occurrence: 204
   level: 3, occurrence: 93
   level: 4, occurrence: 28
   level: 5, occurrence: 9
   level: 6, occurrence: 1
XXX number of pointers point to pointers: 206
XXX number of pointers point to scalars: 275
XXX number of pointers point to structs: 21
XXX percent of pointers has null in alias set: 30.3
XXX average alias set size: 1.48

XXX times a non-volatile is read: 2204
XXX times a non-volatile is write: 1069
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 4

XXX stmts: 116
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 32
   depth: 1, occurrence: 24
   depth: 2, occurrence: 17
   depth: 3, occurrence: 10
   depth: 4, occurrence: 15
   depth: 5, occurrence: 18

XXX percentage a fresh-made variable is used: 15.3
XXX percentage an existing variable is used: 84.7
********************* end of statistics **********************/

