/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      1981235322
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint16_t g_2 = 0xC692L;
static int32_t g_6 = 0x16EFA42BL;
static int32_t *g_5[1][2][6] = {{{&g_6,&g_6,&g_6,&g_6,&g_6,&g_6},{&g_6,&g_6,&g_6,&g_6,&g_6,&g_6}}};
static int8_t g_24 = (-10L);
static uint32_t g_26 = 0xA4B38B4BL;
static uint16_t g_28 = 1UL;
static int16_t g_83 = 0xB73DL;
static uint32_t g_98[3][7][6] = {{{0x884ACF1AL,0UL,0x738BD3C8L,0x90DFA932L,0xF74835F0L,0x884ACF1AL},{4294967289UL,0x99DBF4B6L,0x92F9027EL,0UL,0x52D6847AL,0x884ACF1AL},{4294967290UL,0xDA63D7FDL,0x738BD3C8L,0UL,0xED821F8EL,0x81A7339EL},{0x52D6847AL,0UL,0UL,0x4B48400EL,0UL,0UL},{0xDA63D7FDL,0x52D6847AL,4294967293UL,0x738BD3C8L,0x611122A8L,0x611122A8L},{0x884ACF1AL,0x0099732EL,0x0099732EL,0x884ACF1AL,0x8A7AA087L,4294967286UL},{4294967286UL,0xDA63D7FDL,0x5E375EFDL,4294967289UL,0x90DFA932L,0UL}},{{0x0099732EL,0x738BD3C8L,0x8E2D0685L,9UL,0x90DFA932L,0x0099732EL},{4294967295UL,0xDA63D7FDL,0UL,0xBB8780ECL,0x8A7AA087L,4294967289UL},{0x84A182F7L,4294967295UL,9UL,0x92F9027EL,0x52D6847AL,0xED821F8EL},{4294967287UL,0x216A9CAFL,0x92F9027EL,4294967286UL,8UL,4294967286UL},{0x4B48400EL,0x8A7AA087L,0x4B48400EL,0x8E2D0685L,0x92F9027EL,0x5E375EFDL},{0x0099732EL,0UL,4294967295UL,4294967288UL,0x216A9CAFL,0x738BD3C8L},{0x90DFA932L,0xDA63D7FDL,0UL,4294967288UL,0x81A7339EL,0x8E2D0685L}},{{0x0099732EL,0x90DFA932L,9UL,0x8E2D0685L,0x738BD3C8L,0x0099732EL},{0x4B48400EL,4294967289UL,0x216A9CAFL,4294967286UL,0x8A7AA087L,4294967293UL},{4294967287UL,0xD2368087L,0UL,0x92F9027EL,0x92F9027EL,0UL},{0x84A182F7L,0x84A182F7L,0x92F9027EL,0xBB8780ECL,0x884ACF1AL,0x738BD3C8L},{4294967295UL,0x8A7AA087L,0xF74835F0L,9UL,4294967290UL,0x92F9027EL},{0x0099732EL,4294967295UL,0xF74835F0L,4294967289UL,0x84A182F7L,0x738BD3C8L},{4294967286UL,4294967289UL,0x92F9027EL,4294967293UL,0x81A7339EL,0UL}}};
static int64_t g_106 = 0x40BD20A5AE12EA52LL;
static int8_t g_125 = 0xD2L;
static int16_t g_129 = 0x6D47L;
static int16_t g_130[3][4][1] = {{{0xC68BL},{0xC68BL},{0xC68BL},{0xC68BL}},{{0xC68BL},{0xC68BL},{0xC68BL},{0xC68BL}},{{0xC68BL},{0xC68BL},{0xC68BL},{0xC68BL}}};
static int32_t g_176 = 2L;
static int64_t **g_181 = (void*)0;
static uint16_t g_209 = 65534UL;
static int8_t *g_263 = &g_125;
static int8_t **g_262 = &g_263;
static uint64_t g_268[7] = {4UL,18446744073709551611UL,4UL,4UL,18446744073709551611UL,4UL,4UL};
static int32_t g_342[7][3] = {{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L},{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L},{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L},{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L},{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L},{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L},{0xBA3B6F34L,0xBA3B6F34L,0xBA3B6F34L}};
static int16_t g_346[4] = {0x4B83L,0x4B83L,0x4B83L,0x4B83L};
static int8_t g_598 = 6L;
static int32_t g_607 = 0x1C9FCD6BL;
static uint8_t g_646 = 0x91L;
static uint32_t g_652 = 0x137A539AL;
static int32_t **g_668 = (void*)0;
static int32_t ***g_667 = &g_668;
static int32_t ***g_669 = &g_668;
static int8_t ***g_713[4] = {&g_262,&g_262,&g_262,&g_262};
static int8_t ****g_712 = &g_713[2];
static int32_t g_741[7] = {(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)};
static int32_t g_775 = 0x6C678AC8L;
static int32_t g_779 = (-1L);
static uint32_t *g_790 = &g_98[1][3][2];
static uint32_t *g_958 = &g_26;
static uint32_t **g_957[3][10] = {{&g_958,&g_958,&g_958,(void*)0,&g_958,(void*)0,&g_958,&g_958,&g_958,&g_958},{&g_958,&g_958,&g_958,&g_958,(void*)0,&g_958,&g_958,(void*)0,&g_958,&g_958},{&g_958,&g_958,(void*)0,&g_958,&g_958,&g_958,&g_958,&g_958,&g_958,&g_958}};
static int64_t g_996 = 0xD419D90C0322A307LL;
static uint16_t *g_1099 = &g_28;
static uint16_t **g_1098 = &g_1099;
static int16_t *g_1112 = &g_346[0];
static int16_t **g_1111 = &g_1112;
static const int64_t g_1117 = 0xF9117CEA365D3202LL;
static const int64_t *g_1116[9][1][2] = {{{&g_1117,&g_1117}},{{(void*)0,&g_1117}},{{&g_1117,(void*)0}},{{&g_1117,&g_1117}},{{(void*)0,&g_1117}},{{&g_1117,(void*)0}},{{&g_1117,&g_1117}},{{(void*)0,(void*)0}},{{(void*)0,(void*)0}}};
static const int64_t **g_1115 = &g_1116[2][0][0];
static uint16_t g_1149 = 0xE72BL;
static const int8_t g_1158 = (-3L);
static int32_t g_1303 = 0x7BBE27AAL;
static uint8_t *g_1487 = (void*)0;
static int32_t *g_1496 = (void*)0;
static const uint16_t g_1604 = 3UL;
static int32_t g_1625 = 1L;
static uint32_t g_1664 = 0x1DB2BBBFL;
static const uint8_t g_1714[1] = {0x7CL};
static uint32_t g_1716[3][9][9] = {{{1UL,0x303D9064L,4UL,4294967288UL,4294967295UL,0x29DE0AEAL,0UL,0x0711F986L,4294967293UL},{0x31A9E696L,4UL,4294967295UL,0x303D9064L,0x80AC2C2BL,4294967293UL,0x19680336L,1UL,0UL},{1UL,0x303D9064L,0x8C12F1AFL,4294967295UL,0UL,0UL,0UL,0UL,4294967295UL},{0x8C12F1AFL,1UL,0x8C12F1AFL,0x29DE0AEAL,0x303D9064L,0x5AA092A7L,0x80AC2C2BL,0xA62FEC93L,0UL},{0x13293361L,4294967293UL,4294967295UL,0xA62FEC93L,0x1D59B732L,1UL,4294967289UL,4294967288UL,0x591D2A5CL},{0x19680336L,0UL,4UL,0x29DE0AEAL,1UL,0x8DA749CFL,0x1D59B732L,0x1D59B732L,0x8DA749CFL},{1UL,4294967295UL,0x8DA749CFL,4294967295UL,1UL,4294967295UL,0x591D2A5CL,1UL,0x8C12F1AFL},{4294967293UL,0UL,0x0711F986L,0x303D9064L,0x1D59B732L,0x13293361L,0UL,3UL,0x31A9E696L},{4294967295UL,0x591D2A5CL,1UL,4294967288UL,0x303D9064L,4294967295UL,0x0711F986L,4294967289UL,4294967289UL}},{{0x0711F986L,0x8DA749CFL,0UL,1UL,0UL,0x8DA749CFL,0x0711F986L,0x13293361L,0UL},{4294967289UL,0x8C12F1AFL,0xA62FEC93L,0UL,0x80AC2C2BL,1UL,0UL,0x303D9064L,1UL},{0UL,0x31A9E696L,4294967288UL,0x19680336L,4294967295UL,0x5AA092A7L,0x591D2A5CL,0x13293361L,0x29DE0AEAL},{0x29DE0AEAL,4294967289UL,0x1D59B732L,0UL,4294967288UL,0UL,0x1D59B732L,4294967289UL,0x29DE0AEAL},{0UL,0UL,1UL,0x80AC2C2BL,0x29DE0AEAL,4294967293UL,4294967289UL,3UL,1UL},{0x5AA092A7L,1UL,3UL,4294967289UL,4294967293UL,0x29DE0AEAL,0x80AC2C2BL,1UL,0UL},{0UL,0x29DE0AEAL,4294967289UL,0x1D59B732L,0UL,4294967288UL,0UL,0x1D59B732L,4294967289UL},{0x29DE0AEAL,0x29DE0AEAL,0x13293361L,0x591D2A5CL,0x5AA092A7L,4294967295UL,0x19680336L,4294967288UL,0x31A9E696L},{0UL,1UL,0x303D9064L,0UL,1UL,0x80AC2C2BL,0UL,0xA62FEC93L,0x8C12F1AFL}},{{4294967289UL,0UL,0x13293361L,0x0711F986L,0x8DA749CFL,0UL,1UL,0UL,0x8DA749CFL},{0x0711F986L,4294967289UL,4294967289UL,0x0711F986L,4294967295UL,0x303D9064L,4294967288UL,1UL,0x591D2A5CL},{4294967295UL,0x31A9E696L,3UL,0UL,0x13293361L,0UL,0x8C12F1AFL,0UL,4UL},{0x13293361L,0UL,3UL,0x31A9E696L,4294967295UL,1UL,0x5AA092A7L,0UL,0x5AA092A7L},{1UL,0UL,0UL,0UL,0UL,1UL,0x80AC2C2BL,0x29DE0AEAL,4294967293UL},{4294967289UL,0x31A9E696L,0x303D9064L,0UL,1UL,0UL,0x0711F986L,0x5AA092A7L,0x13293361L},{1UL,4UL,0x0711F986L,4294967288UL,1UL,0x8C12F1AFL,0x80AC2C2BL,0UL,0x1D59B732L},{0UL,0x5AA092A7L,4UL,0x591D2A5CL,0x591D2A5CL,4UL,0x5AA092A7L,0UL,0x8C12F1AFL},{3UL,4294967293UL,1UL,4294967289UL,0x13293361L,4294967288UL,0x8C12F1AFL,0x5AA092A7L,0x29DE0AEAL}}};
static uint32_t g_1728 = 0x7180D93DL;
static int32_t g_1882 = (-1L);
static const uint32_t g_1893 = 4294967286UL;
static uint8_t g_1970[8] = {1UL,1UL,1UL,1UL,1UL,1UL,1UL,1UL};
static uint64_t *g_2035[9][9] = {{&g_268[1],&g_268[0],(void*)0,&g_268[3],&g_268[3],(void*)0,&g_268[0],&g_268[1],&g_268[0]},{&g_268[0],(void*)0,&g_268[1],&g_268[0],&g_268[1],&g_268[0],(void*)0,(void*)0,&g_268[0]},{&g_268[1],&g_268[3],&g_268[1],&g_268[3],&g_268[1],&g_268[1],&g_268[3],&g_268[1],&g_268[0]},{(void*)0,&g_268[1],&g_268[6],&g_268[1],&g_268[1],&g_268[1],&g_268[1],&g_268[1],&g_268[6]},{&g_268[1],&g_268[1],&g_268[0],&g_268[1],&g_268[3],&g_268[1],&g_268[1],&g_268[3],&g_268[1]},{&g_268[0],&g_268[1],&g_268[0],(void*)0,(void*)0,&g_268[0],&g_268[1],&g_268[0],&g_268[1]},{&g_268[1],&g_268[3],&g_268[0],&g_268[1],&g_268[0],(void*)0,&g_268[3],&g_268[3],(void*)0},{&g_268[1],(void*)0,&g_268[6],(void*)0,&g_268[1],(void*)0,(void*)0,&g_268[1],&g_268[1]},{&g_268[3],&g_268[0],&g_268[1],&g_268[1],&g_268[0],&g_268[0],&g_268[0],&g_268[1],&g_268[1]}};
static uint16_t g_2048 = 0UL;
static uint32_t *g_2097 = &g_1664;
static int16_t *g_2127[7] = {&g_83,&g_83,&g_83,&g_83,&g_83,&g_83,&g_83};
static uint32_t g_2151 = 0xF849FDF0L;
static int64_t ***g_2187 = &g_181;
static int64_t ****g_2186 = &g_2187;
static int32_t ******g_2264 = (void*)0;
static int32_t g_2694 = 0xCE0F8881L;
static uint32_t ***g_2729 = (void*)0;
static int64_t g_2759 = 0xAE914239429CB304LL;
static uint32_t **g_2822 = &g_2097;
static uint32_t ***g_2821 = &g_2822;
static uint32_t ***g_2824 = &g_2822;
static uint32_t g_2985 = 18446744073709551606UL;
static int32_t g_3014 = 0x71955EAEL;
static const uint16_t *g_3029 = &g_209;
static const uint16_t **g_3028 = &g_3029;
static const uint16_t ***g_3027 = &g_3028;
static const uint16_t ****g_3026[6][1][2] = {{{&g_3027,&g_3027}},{{&g_3027,&g_3027}},{{&g_3027,&g_3027}},{{&g_3027,&g_3027}},{{&g_3027,&g_3027}},{{&g_3027,&g_3027}}};
static const uint32_t g_3037 = 0x68B608AAL;
static uint64_t g_3140 = 0x8ACB43E204F0D7A2LL;
static const int8_t *g_3180 = (void*)0;
static const int8_t **g_3179[1][3][3] = {{{&g_3180,&g_3180,&g_3180},{&g_3180,&g_3180,&g_3180},{&g_3180,&g_3180,&g_3180}}};
static const int8_t ***g_3178 = &g_3179[0][0][0];
static const int8_t ****g_3177 = &g_3178;
static uint32_t g_3194 = 18446744073709551614UL;
static uint32_t g_3234 = 0xB0E20168L;
static const uint32_t ***g_3277 = (void*)0;
static uint32_t g_3328 = 0x7B6CF4CFL;
static int32_t ****g_3504[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t **** const * const g_3503 = &g_3504[0];
static int32_t **** const * const *g_3502[3][2] = {{&g_3503,&g_3503},{&g_3503,&g_3503},{&g_3503,&g_3503}};
static uint16_t ***g_3555 = &g_1098;
static uint16_t ****g_3554 = &g_3555;
static int16_t g_3563 = 0xC7DDL;
static uint64_t g_3607 = 0x09AB83EB61943EC3LL;
static int32_t g_3669 = 0xE2E69A22L;
static int32_t g_3698 = 0x060D7348L;
static int64_t g_3791 = 0x7604BF828A1B20B8LL;
static int64_t * const g_3790 = &g_3791;
static int64_t * const *g_3789 = &g_3790;
static int64_t * const **g_3788 = &g_3789;
static uint8_t g_3851 = 255UL;
static uint32_t g_3863 = 0UL;
static int16_t g_3952 = 0xEBB5L;
static int8_t g_4019 = 0x0DL;
static int16_t g_4062 = 0x5171L;
static const uint32_t *g_4181 = &g_1893;
static const uint32_t **g_4180 = &g_4181;
static const uint32_t ***g_4179 = &g_4180;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int32_t  func_8(uint64_t  p_9, int64_t  p_10, int32_t * p_11, int32_t * p_12, int32_t  p_13);
static const int8_t  func_14(int32_t  p_15, uint32_t  p_16);
static uint32_t  func_17(uint32_t  p_18, uint8_t  p_19, int32_t  p_20);
static int32_t * func_40(int64_t  p_41, uint8_t  p_42, uint16_t * p_43);
static const int16_t  func_46(int8_t  p_47, int8_t * p_48, uint32_t  p_49);
static uint16_t  func_55(int32_t ** p_56, uint16_t  p_57, int32_t ** p_58);
static int32_t ** func_59(int16_t  p_60, uint8_t  p_61, int32_t  p_62, int32_t  p_63);
static uint8_t  func_72(int32_t  p_73);
static uint8_t  func_74(int32_t  p_75);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_5 g_6 g_24 g_26 g_263 g_125 g_598 g_1112 g_346 g_1099 g_28 g_790 g_98 g_262 g_741 g_2821 g_2822 g_2097 g_1664 g_3028 g_3029 g_209 g_996 g_1098 g_652 g_1716 g_2187 g_181 g_268 g_712 g_713 g_2264 g_3502 g_3554 g_3563 g_3177 g_3178 g_1111 g_2824 g_2186 g_3014 g_3555 g_3607 g_1149 g_3027 g_3669 g_646 g_958 g_3698 g_1970 g_1893 g_106 g_1117 g_3851 g_3863 g_3788 g_3789 g_3790 g_1158 g_3791 g_3952 g_4019 g_2694 g_4179
 * writes: g_2 g_5 g_24 g_28 g_6 g_98 g_1664 g_3140 g_996 g_652 g_268 g_125 g_1970 g_3502 g_129 g_1625 g_2985 g_3234 g_3014 g_741 g_346 g_262 g_1149 g_181 g_646 g_26 g_2186 g_106 g_3788 g_1882 g_3863 g_3791 g_1111 g_4062 g_2729 g_4179
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int16_t l_3 = (-10L);
    uint16_t l_4[2];
    const uint16_t l_31 = 0UL;
    int32_t l_3445 = 9L;
    int32_t l_3466 = 1L;
    int32_t l_3467 = 0x2E946E78L;
    int32_t l_3468[8] = {0x9648BEEEL,0x9648BEEEL,0x9648BEEEL,0x9648BEEEL,0x9648BEEEL,0x9648BEEEL,0x9648BEEEL,0x9648BEEEL};
    uint32_t l_3472 = 1UL;
    int32_t l_3478 = 5L;
    int32_t l_3479[7][10] = {{0x8B821602L,(-7L),(-2L),1L,(-7L),1L,(-2L),(-7L),0x8B821602L,0x8B821602L},{2L,0x84E3BE66L,(-1L),(-7L),(-7L),(-1L),0x84E3BE66L,2L,(-1L),2L},{(-7L),(-2L),1L,(-7L),1L,(-2L),(-7L),0x8B821602L,0x8B821602L,(-7L)},{0x8B821602L,2L,1L,1L,2L,0x8B821602L,(-2L),2L,(-2L),0x8B821602L},{0x84E3BE66L,2L,(-1L),2L,0x84E3BE66L,(-1L),(-7L),(-7L),(-1L),0x84E3BE66L},{0x84E3BE66L,(-2L),(-2L),0x84E3BE66L,1L,0x8B821602L,0x84E3BE66L,0x8B821602L,1L,0x84E3BE66L},{0x8B821602L,0x84E3BE66L,0x8B821602L,1L,0x84E3BE66L,(-2L),(-2L),0x84E3BE66L,1L,0x8B821602L}};
    int16_t l_3483 = 0x4C71L;
    uint64_t **l_3484 = &g_2035[5][7];
    int32_t l_3517 = 0L;
    int32_t *l_3518 = (void*)0;
    uint16_t ***l_3528 = &g_1098;
    int64_t l_3550 = 0xED43968B3E0A8FD7LL;
    uint32_t ****l_3612 = (void*)0;
    uint16_t l_3626 = 0xD611L;
    uint32_t ****l_3636 = &g_2729;
    int32_t *l_3645 = &l_3468[3];
    uint8_t l_3685 = 0UL;
    uint32_t l_3697 = 0xEA47A0FBL;
    uint64_t l_3703 = 0x96C7A1140AC0B497LL;
    int32_t l_3704[6][6] = {{0L,0L,0xDD21D6E2L,0xD3D7B662L,0x26C017CAL,0xCC6211D8L},{0x26C017CAL,0xD1FF4DA1L,0L,0xD3D7B662L,0xD1FF4DA1L,0xDD21D6E2L},{0L,0x26C017CAL,0L,0x26C017CAL,0L,0xCC6211D8L},{0xADDED1B2L,0x26C017CAL,0xDD21D6E2L,0xADDED1B2L,0xD1FF4DA1L,(-1L)},{0xADDED1B2L,0xD1FF4DA1L,(-1L),0x26C017CAL,0x26C017CAL,(-1L)},{0L,0L,0xDD21D6E2L,0xD3D7B662L,0x26C017CAL,0xCC6211D8L}};
    int64_t l_3705 = 1L;
    uint64_t *l_3724[8];
    int32_t *l_3743 = &g_3014;
    const int32_t l_3758 = 0xACF97953L;
    uint32_t l_3759 = 0x681958A2L;
    int8_t l_3773 = 0L;
    int32_t l_3945 = 0xCB2F2384L;
    uint32_t * const *l_3972 = (void*)0;
    int32_t l_4065[10][1];
    int32_t * const **l_4073 = (void*)0;
    uint32_t l_4080 = 0x618110A2L;
    int64_t *l_4085 = &g_2759;
    uint16_t l_4157 = 1UL;
    const int32_t *l_4184 = &g_3669;
    int i, j;
    for (i = 0; i < 2; i++)
        l_4[i] = 0x9F7FL;
    for (i = 0; i < 8; i++)
        l_3724[i] = &g_268[1];
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 1; j++)
            l_4065[i][j] = (-7L);
    }
lbl_3581:
    l_3 = g_2;
    if ((l_3 > l_3))
    { /* block id: 2 */
        int32_t l_22[9][10] = {{0x5540FFF0L,0x5540FFF0L,(-1L),0x16EA3DE8L,(-9L),0x16EA3DE8L,(-1L),0x5540FFF0L,0x5540FFF0L,(-1L)},{(-1L),0x16EA3DE8L,0x39D3CD36L,0x39D3CD36L,0x16EA3DE8L,(-1L),(-1L),(-1L),0x16EA3DE8L,0x39D3CD36L},{1L,0x5540FFF0L,1L,0x39D3CD36L,(-1L),(-1L),0x39D3CD36L,1L,0x5540FFF0L,1L},{1L,(-1L),0x5540FFF0L,0x16EA3DE8L,0x5540FFF0L,(-1L),1L,1L,(-1L),0x5540FFF0L},{(-1L),1L,1L,(-1L),0x5540FFF0L,0x16EA3DE8L,0x5540FFF0L,(-1L),1L,1L},{0x5540FFF0L,1L,0x39D3CD36L,(-1L),(-1L),0x39D3CD36L,1L,0x5540FFF0L,1L,0x39D3CD36L},{0x16EA3DE8L,(-1L),(-1L),(-1L),0x16EA3DE8L,0x39D3CD36L,0x39D3CD36L,0x16EA3DE8L,(-1L),(-1L)},{0x5540FFF0L,0x5540FFF0L,(-1L),0x16EA3DE8L,(-9L),0x16EA3DE8L,(-1L),0x5540FFF0L,0x5540FFF0L,0x5540FFF0L},{1L,0x39D3CD36L,(-1L),(-1L),0x39D3CD36L,1L,0x5540FFF0L,1L,0x39D3CD36L,(-1L)}};
        int32_t *l_3423[9] = {&l_22[1][9],&l_22[1][9],&g_779,&l_22[1][9],&l_22[1][9],&g_779,&l_22[1][9],&l_22[1][9],&g_779};
        uint32_t l_3475[9] = {18446744073709551615UL,0xADDC9B88L,18446744073709551615UL,0xADDC9B88L,18446744073709551615UL,0xADDC9B88L,18446744073709551615UL,0xADDC9B88L,18446744073709551615UL};
        uint64_t l_3480[8][3] = {{0x2594CAD4FE86DBABLL,18446744073709551606UL,0xDFB3A096F6C1A031LL},{0x2102BDBB47DB7397LL,0x2102BDBB47DB7397LL,0x4F5E4E77D3C95827LL},{0x2594CAD4FE86DBABLL,0x4F5E4E77D3C95827LL,18446744073709551606UL},{18446744073709551606UL,0x4F5E4E77D3C95827LL,0x2594CAD4FE86DBABLL},{0x4F5E4E77D3C95827LL,0x2102BDBB47DB7397LL,0x2102BDBB47DB7397LL},{0xDFB3A096F6C1A031LL,18446744073709551606UL,0x2594CAD4FE86DBABLL},{0x0D4C73224340E68BLL,0x329CCC1B97FDDB5ALL,18446744073709551606UL},{0x0D4C73224340E68BLL,18446744073709551606UL,0x4F5E4E77D3C95827LL}};
        uint64_t *l_3485 = &l_3480[5][1];
        uint8_t *l_3499 = (void*)0;
        uint8_t *l_3500 = &g_1970[7];
        int32_t ***** const *l_3501 = (void*)0;
        int32_t **** const * const **l_3505 = &g_3502[0][1];
        uint32_t l_3534 = 1UL;
        uint32_t l_3605 = 9UL;
        int32_t l_3702 = (-1L);
        int64_t *****l_3732[10] = {&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186,&g_2186};
        uint16_t ****l_3738 = &l_3528;
        int i, j;
        for (l_3 = 0; (l_3 <= 1); l_3 += 1)
        { /* block id: 5 */
            uint32_t l_37 = 1UL;
            int32_t l_3446 = 0xD76C21B2L;
            int32_t l_3447 = (-2L);
            int32_t l_3470 = 1L;
            int32_t l_3471[8][7][4] = {{{0L,(-7L),3L,0xCD991402L},{0x6FCEDDA3L,0x2711E23CL,0L,0x8EEA5E52L},{0x6C434A0BL,0x739D03E2L,0xCD991402L,0xE123B5A6L},{3L,0x1B3D50A2L,0x1635973DL,0x6C434A0BL},{7L,0x74247DDEL,0L,0xCA06669FL},{0xA05F6D98L,0xB8905301L,0xE123B5A6L,0xB8905301L},{6L,0x6C434A0BL,0xB8905301L,0x56B629E8L}},{{0x56B629E8L,0xCD991402L,0x8EEA5E52L,7L},{(-1L),1L,(-7L),1L},{(-1L),(-1L),0x8EEA5E52L,0xDF2FD5EFL},{0x56B629E8L,1L,0xB8905301L,0L},{6L,0x8EEA5E52L,0xE123B5A6L,3L},{0xA05F6D98L,0x83978545L,0L,(-1L)},{7L,0L,0x1635973DL,6L}},{{3L,(-1L),0xCD991402L,0xCE1B8E6BL},{0x6C434A0BL,0xDF2FD5EFL,0L,0L},{0x6FCEDDA3L,0x6FCEDDA3L,3L,0x49711BEBL},{0L,(-7L),0x2720A530L,(-7L)},{0xF093927BL,0xACEAA96EL,(-1L),0x2720A530L},{0x2711E23CL,0xACEAA96EL,0xDF2FD5EFL,(-7L)},{0xACEAA96EL,(-7L),0x1B3D50A2L,0x49711BEBL}},{{(-4L),0x6FCEDDA3L,0x2711E23CL,0L},{0xCD991402L,0xDF2FD5EFL,0x74247DDEL,0xCE1B8E6BL},{3L,(-1L),0x56B629E8L,6L},{0x739D03E2L,0L,(-4L),(-1L)},{0x49711BEBL,0x83978545L,(-1L),3L},{(-2L),0x8EEA5E52L,(-2L),0L},{0x74247DDEL,1L,0x6FCEDDA3L,0xDF2FD5EFL}},{{(-7L),(-1L),0xCA06669FL,1L},{0x83978545L,1L,0xCA06669FL,7L},{(-7L),0xCD991402L,0x6FCEDDA3L,0x56B629E8L},{0x74247DDEL,0x6C434A0BL,(-2L),0xB8905301L},{0xACEAA96EL,(-1L),0x6FCEDDA3L,0x1B3D50A2L},{0x03164DE5L,0L,(-7L),0xE123B5A6L},{(-1L),5L,(-4L),0x2720A530L}},{{(-3L),(-1L),0L,0x74247DDEL},{0xF093927BL,0xCD991402L,0xCD991402L,0xF093927BL},{(-7L),0xCA06669FL,5L,0L},{1L,0x83978545L,(-7L),0x56B629E8L},{0xCD991402L,0xACEAA96EL,0L,0x56B629E8L},{0L,0x83978545L,0xA05F6D98L,0L},{0xDF2FD5EFL,0xCA06669FL,(-3L),0xF093927BL}},{{3L,0xCD991402L,0xDF2FD5EFL,0x74247DDEL},{0xE123B5A6L,(-1L),0xF093927BL,0x2720A530L},{7L,5L,0xCE1B8E6BL,0xE123B5A6L},{0x83978545L,0L,(-1L),0x1B3D50A2L},{0x56B629E8L,(-1L),0x2720A530L,(-1L)},{1L,0xE123B5A6L,(-1L),(-4L)},{(-4L),0xF093927BL,0x74247DDEL,0x83978545L}},{{0L,0xB8905301L,0xCA06669FL,3L},{0L,0x1635973DL,0x74247DDEL,(-7L)},{(-4L),3L,(-1L),0x739D03E2L},{1L,0x74247DDEL,0x2720A530L,7L},{0x56B629E8L,(-1L),(-1L),0x1635973DL},{0x83978545L,(-1L),0xCE1B8E6BL,1L},{7L,(-2L),0xF093927BL,0x2711E23CL}}};
            int i, j, k;
            for (g_2 = 0; (g_2 <= 1); g_2 += 1)
            { /* block id: 8 */
                int32_t **l_7[10][10][2] = {{{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]}},{{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]}},{{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]}},{{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]}},{{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]}},{{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]}},{{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]}},{{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]}},{{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]}},{{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]},{(void*)0,&g_5[0][1][1]},{(void*)0,&g_5[0][1][5]},{&g_5[0][1][5],&g_5[0][1][1]},{&g_5[0][1][5],&g_5[0][1][5]}}};
                uint32_t l_21 = 0xE889EC00L;
                int8_t *l_23 = &g_24;
                uint16_t *l_27 = &g_28;
                int32_t *l_36 = &l_22[6][2];
                int i, j, k;
                g_5[0][0][0] = g_5[0][1][5];
                if (((*l_36) = func_8((func_14(l_4[l_3], func_17(((l_21 <= (l_22[1][9] ^ (((*l_27) = (((*l_23) &= g_6) , (!g_26))) <= (safe_mod_func_int64_t_s_s(l_31, (safe_div_func_int16_t_s_s((safe_sub_func_uint64_t_u_u(((65534UL >= ((void*)0 != l_36)) , 1UL), 0xA227283A38C8A251LL)), l_37))))))) < 0xBA2D3C5EL), l_37, (*l_36))) <= (**g_262)), l_37, l_3423[8], &g_741[6], (*l_36))))
                { /* block id: 1403 */
                    int32_t *l_3444 = &l_22[1][9];
                    int32_t l_3461 = 0x865E3561L;
                    const int8_t l_3463 = 0L;
                    int32_t l_3464[3][3] = {{1L,1L,1L},{(-4L),(-4L),(-4L)},{1L,1L,1L}};
                    int i, j;
                    for (g_652 = 0; (g_652 <= 2); g_652 += 1)
                    { /* block id: 1406 */
                        uint64_t l_3448 = 0xCD1A6861B9D3E6BFLL;
                        uint64_t *l_3462 = &g_268[5];
                        int i, j, k;
                        (*l_36) = g_1716[g_2][(l_3 + 6)][(g_652 + 1)];
                        l_3444 = (void*)0;
                        --l_3448;
                        l_3464[2][0] |= (safe_div_func_uint64_t_u_u((safe_mod_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(((*g_263) , ((****g_712) = (safe_sub_func_uint16_t_u_u(0xCB12L, ((safe_rshift_func_int16_t_s_u(((0xAE436B7812B0F541LL < ((*l_36) == 0xA026CAB087E37E90LL)) , (l_3448 , ((*g_2187) == (*g_2187)))), 15)) , (((((*l_3462) &= ((l_3461 = ((*l_36) = ((l_3448 <= 0xBC952FE9DC27DEFELL) ^ 0x03DDD0EF05AF91C9LL))) , (*l_36))) & l_3445) && 0x1CL) , l_3)))))), l_3463)), (*g_1112))), l_3447));
                    }
                    return (**g_2822);
                }
                else
                { /* block id: 1417 */
                    int32_t l_3465 = (-10L);
                    int32_t l_3469 = (-1L);
                    if (l_4[0])
                        break;
                    l_3472++;
                    if (l_3445)
                    { /* block id: 1420 */
                        return l_3466;
                    }
                    else
                    { /* block id: 1422 */
                        --l_3475[4];
                    }
                }
            }
            if (l_3466)
                break;
            l_3480[6][2]--;
            l_3447 = l_3447;
        }
        l_3479[6][4] = l_3483;
        l_3467 = ((*g_263) < (((*l_3485) = (l_3484 == &g_2035[5][7])) <= ((g_2264 != (((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((~(0x5DFECF0CL | (*g_790))), ((**g_1098)--))), 0xC135L)) < ((safe_sub_func_uint8_t_u_u(((*l_3500) = (safe_rshift_func_int16_t_s_u((7L == (safe_div_func_uint64_t_u_u(18446744073709551608UL, 18446744073709551615UL))), 14))), l_3468[1])) != 0x3030223E6D75F116LL)) , l_3501)) , 0xD38380F3976F1CB8LL)));
        if ((((((*l_3505) = g_3502[2][0]) == (void*)0) , ((*l_3485) |= ((l_3445 & l_3466) >= 0xA2868E4CL))) , ((**g_2822) < (((g_1970[3] = l_3) , (safe_rshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(0x1B375AA8L, (safe_sub_func_int8_t_s_s(l_3468[3], l_3468[0])))), 0))) , l_3472))))
        { /* block id: 1439 */
            int8_t l_3512 = (-3L);
            int32_t l_3513 = 0x230B9BE0L;
            int32_t *l_3515[10][7] = {{&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0]},{&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882},{&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0]},{&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882},{&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0]},{&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882},{&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0]},{&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882},{&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0],&l_3479[0][0]},{&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882,&g_1882}};
            int8_t l_3525 = 7L;
            uint16_t * const * const l_3530 = &g_1099;
            uint16_t * const * const *l_3529 = &l_3530;
            uint32_t **l_3531 = (void*)0;
            int32_t *l_3539 = &g_3014;
            const int8_t ***l_3570 = &g_3179[0][0][1];
            int64_t *l_3594[8][7][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_3550},{&g_2759,&l_3550,&l_3550},{&g_2759,&g_106,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_106,&g_106,&g_2759}},{{(void*)0,&l_3550,&g_2759},{&g_106,(void*)0,(void*)0},{&g_2759,&g_2759,(void*)0},{&g_106,&g_106,&l_3550},{(void*)0,&g_106,&l_3550},{&g_106,&g_2759,&g_106},{(void*)0,(void*)0,&l_3550}},{{&g_2759,&l_3550,&l_3550},{&g_2759,&g_106,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_106,&g_106,&g_2759},{(void*)0,&l_3550,&g_2759},{&g_106,(void*)0,(void*)0},{&g_2759,&g_2759,(void*)0}},{{&g_106,&g_106,&l_3550},{(void*)0,&g_106,&l_3550},{&g_106,&g_2759,&g_106},{(void*)0,(void*)0,&l_3550},{&g_2759,&l_3550,&l_3550},{&g_2759,&g_106,(void*)0},{(void*)0,(void*)0,(void*)0}},{{&g_106,&g_106,&g_2759},{(void*)0,&l_3550,&g_2759},{&g_106,(void*)0,(void*)0},{&g_2759,&g_2759,(void*)0},{&g_106,&g_106,&l_3550},{(void*)0,&g_106,&l_3550},{&g_106,&g_2759,&g_106}},{{(void*)0,(void*)0,&l_3550},{&g_2759,&l_3550,&l_3550},{&g_2759,&g_106,(void*)0},{(void*)0,(void*)0,(void*)0},{&g_106,&g_106,&g_2759},{(void*)0,&l_3550,&g_2759},{&g_106,(void*)0,(void*)0}},{{&g_2759,&g_2759,(void*)0},{&g_106,&g_106,&l_3550},{(void*)0,&g_106,&l_3550},{&g_106,&g_2759,&g_106},{(void*)0,(void*)0,&l_3550},{&g_2759,&l_3550,&l_3550},{&g_2759,&g_106,(void*)0}},{{(void*)0,(void*)0,(void*)0},{&g_106,&g_106,&g_2759},{(void*)0,&l_3550,&g_2759},{&g_106,(void*)0,(void*)0},{&g_2759,&g_2759,(void*)0},{&g_106,&g_106,&l_3550},{(void*)0,&g_106,&l_3550}}};
            int64_t **l_3593[1][6] = {{&l_3594[7][0][0],&l_3594[7][0][0],&l_3594[7][0][0],&l_3594[7][0][0],&l_3594[7][0][0],&l_3594[7][0][0]}};
            int32_t l_3606 = 1L;
            int i, j, k;
            l_3513 |= l_3512;
            for (g_28 = 0; (g_28 <= 0); g_28 += 1)
            { /* block id: 1443 */
                int32_t l_3516 = 0L;
                int32_t *l_3524 = &g_1625;
                uint16_t ***l_3527 = &g_1098;
                uint16_t ****l_3526[3][1][5] = {{{&l_3527,&l_3527,&l_3527,&l_3527,&l_3527}},{{&l_3527,&l_3527,&l_3527,&l_3527,&l_3527}},{{&l_3527,&l_3527,&l_3527,&l_3527,&l_3527}}};
                int64_t l_3535 = 9L;
                uint8_t l_3545[4] = {0UL,0UL,0UL,0UL};
                uint8_t l_3562[6][10] = {{255UL,0xC3L,0UL,249UL,6UL,0xC3L,0xC3L,6UL,0x00L,0xC0L},{6UL,6UL,0xEAL,249UL,0UL,0xC3L,255UL,0xC3L,0UL,249UL},{249UL,0x9DL,249UL,0xC3L,254UL,0xC0L,255UL,255UL,0xC0L,254UL},{0xEAL,6UL,6UL,0xEAL,249UL,0UL,0xC3L,255UL,0xC3L,0UL},{0xC0L,1UL,249UL,1UL,0xC0L,0x00L,6UL,0xC3L,0xC3L,6UL},{255UL,0x00L,0xEAL,0xEAL,0x00L,255UL,254UL,6UL,0xC0L,6UL}};
                uint32_t l_3564 = 0UL;
                int8_t * const *l_3566 = (void*)0;
                uint16_t l_3609 = 0UL;
                int i, j, k;
                for (g_129 = 0; (g_129 <= 8); g_129 += 1)
                { /* block id: 1446 */
                    uint16_t l_3514 = 0UL;
                    return l_3514;
                }
                l_3515[4][5] = l_3423[(g_28 + 6)];
                l_3518 = func_40(l_3516, l_3517, &l_4[0]);
                if ((safe_sub_func_uint32_t_u_u(((*g_790) |= (((!0xB4L) | ((safe_rshift_func_uint16_t_u_u(((((*l_3524) = 0L) , ((*l_3485) = l_3525)) ^ (((*l_3518) , ((l_3528 = &g_1098) != l_3529)) == (((void*)0 != l_3531) , (safe_add_func_uint16_t_u_u(((*l_3518) | (*l_3518)), 0UL))))), 0)) , (*l_3518))) && (**g_2822))), l_3534)))
                { /* block id: 1455 */
                    uint32_t l_3536 = 4294967295UL;
                    for (g_2985 = 1; (g_2985 <= 7); g_2985 += 1)
                    { /* block id: 1458 */
                        int i;
                        ++l_3536;
                    }
                    for (g_3234 = 0; (g_3234 <= 0); g_3234 += 1)
                    { /* block id: 1463 */
                        l_3515[4][5] = l_3539;
                    }
                }
                else
                { /* block id: 1466 */
                    uint32_t l_3542 = 0UL;
                    uint16_t *****l_3556 = &l_3526[2][0][2];
                    uint32_t l_3561 = 0x0A2DB3FEL;
                    int32_t l_3565 = (-6L);
                    (*l_3518) ^= ((*l_3539) = ((safe_sub_func_uint8_t_u_u(255UL, l_3542)) >= (safe_div_func_int64_t_s_s((l_3516 , l_3545[2]), l_3542))));
                    l_3565 |= ((l_3542 || ((safe_mod_func_int8_t_s_s(((((safe_sub_func_uint8_t_u_u((l_3545[1] == l_3550), (safe_lshift_func_uint8_t_u_u(((((+(((*l_3556) = g_3554) != (void*)0)) <= 4294967295UL) >= (((((*g_1112) = (safe_lshift_func_uint8_t_u_u(((l_3561 = (safe_sub_func_int32_t_s_s(((**g_262) , ((((((*l_3518) , (0x69F48784L ^ 0x7E7D7F2CL)) && (*l_3518)) , l_3518) == l_3518) | 1L)), (*l_3518)))) , l_3542), (*l_3518)))) | l_3562[4][1]) >= l_3542) & l_3542)) <= l_3542), (*l_3518))))) || (*l_3518)) , (**g_3028)) <= g_3563), 0xCEL)) == l_3564)) , (-1L));
                    for (l_3564 = 0; (l_3564 <= 0); l_3564 += 1)
                    { /* block id: 1475 */
                        int8_t * const **l_3567 = &l_3566;
                        (**g_712) = (**g_712);
                        (*l_3567) = l_3566;
                    }
                }
                for (g_6 = 0; (g_6 <= 0); g_6 += 1)
                { /* block id: 1482 */
                    uint32_t l_3573 = 0UL;
                    int8_t *l_3578 = &l_3525;
                    uint16_t l_3586[4][1];
                    int32_t l_3611 = 5L;
                    int i, j;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_3586[i][j] = 0x4680L;
                    }
                    (*l_3518) &= ((safe_lshift_func_int16_t_s_u(((*g_3177) != l_3570), (safe_lshift_func_int16_t_s_u((l_3573 , (0xF9209B31L ^ (((((safe_lshift_func_int8_t_s_u(((*l_3578) |= ((****g_712) &= (safe_add_func_int16_t_s_s(0x3340L, (**g_1111))))), (safe_mul_func_uint8_t_u_u(0xF2L, (&l_3515[4][5] == &l_3515[4][5]))))) & (*g_1099)) != (***g_2824)) || 1L) < l_3564))), 7)))) <= l_3573);
                    (*l_3539) = 0x00049325L;
                    if (l_3517)
                        goto lbl_3581;
                    for (g_1149 = 0; (g_1149 <= 0); g_1149 += 1)
                    { /* block id: 1490 */
                        int64_t l_3603 = (-1L);
                        int64_t l_3608 = 0L;
                        int32_t l_3610 = 9L;
                        int i, j, k;
                        g_5[g_1149][g_1149][(g_28 + 1)] = func_40((l_3610 ^= ((safe_mod_func_uint32_t_u_u((safe_add_func_uint64_t_u_u(0x103B66A0F1B7648BLL, (((*l_3518) |= (**g_3028)) < (0UL == (l_3586[0][0] != (safe_div_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((l_3609 = (((((safe_lshift_func_uint8_t_u_u((((((**g_2186) = (void*)0) != l_3593[0][2]) <= (safe_mul_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s(((((safe_add_func_int8_t_s_s(((*l_3578) = (((safe_mul_func_int16_t_s_s((l_3603 , 8L), ((((safe_unary_minus_func_int16_t_s(((*g_1112) != l_3605))) && 0x974EL) & l_3603) <= (**g_2822)))) || l_3603) , (****g_712))), l_3606)) >= (*l_3539)) > 255UL) != (*g_263)), (****g_712))) , 0x9495L), (***g_3555)))) || 18446744073709551615UL), g_3607)) ^ (**g_2822)) != l_3608) == (*g_1112)) > (***g_2821))), l_3603)), 0x96A450C197911697LL))))))), 0xE785C2F4L)) & l_3573)), l_3611, (***g_3554));
                    }
                }
            }
        }
        else
        { /* block id: 1500 */
            int32_t l_3613 = (-1L);
            int32_t l_3616 = 0x34E4A53EL;
            int32_t l_3617[8][5][4] = {{{0xBA323A35L,(-1L),(-3L),0L},{5L,0xAC691C6CL,5L,0xCF1A504AL},{0xA3E09E13L,0L,0x04D97F66L,0L},{0xAC691C6CL,(-3L),(-2L),0L},{0xCDE333B8L,0xA3E09E13L,(-2L),(-1L)}},{{0xAC691C6CL,0xBA323A35L,0x04D97F66L,0x5C02AC6AL},{0xA3E09E13L,5L,5L,0xA3E09E13L},{5L,0xA3E09E13L,(-3L),0xCDE333B8L},{0xBA323A35L,0xAC691C6CL,0xA6C34309L,0L},{0xA3E09E13L,0xCDE333B8L,0xCF1A504AL,0L}},{{(-3L),0xAC691C6CL,(-2L),0xCDE333B8L},{0L,0xA3E09E13L,0xECDCC21DL,0xA3E09E13L},{0xAC691C6CL,5L,0xCF1A504AL,0x5C02AC6AL},{(-1L),0xBA323A35L,5L,(-1L)},{0xBA323A35L,0xA3E09E13L,(-9L),0L}},{{0xBA323A35L,(-3L),5L,0L},{(-1L),0L,0xCF1A504AL,0xCF1A504AL},{0xAC691C6CL,0xAC691C6CL,0xECDCC21DL,0L},{0L,(-1L),(-2L),0xA3E09E13L},{(-3L),0xBA323A35L,0xCF1A504AL,(-2L)}},{{0xA3E09E13L,0xBA323A35L,0xA6C34309L,0xA3E09E13L},{0xBA323A35L,(-1L),(-3L),0L},{5L,0xAC691C6CL,5L,0xCF1A504AL},{0xA3E09E13L,0L,0x04D97F66L,0L},{0xAC691C6CL,(-3L),(-2L),0L}},{{0xCDE333B8L,0xA3E09E13L,(-2L),(-1L)},{0xAC691C6CL,0xBA323A35L,0x04D97F66L,0x5C02AC6AL},{0xA3E09E13L,5L,5L,0xA3E09E13L},{5L,0xA3E09E13L,(-3L),0xCDE333B8L},{0xBA323A35L,0xAC691C6CL,0xA6C34309L,0L}},{{0xA3E09E13L,0xCDE333B8L,0xCF1A504AL,0L},{(-3L),0xAC691C6CL,(-2L),0xCDE333B8L},{0L,0xA3E09E13L,0xECDCC21DL,0xA3E09E13L},{0xAC691C6CL,5L,0xECDCC21DL,(-3L)},{0xA6C34309L,0xCDE333B8L,0x498A597FL,0xA6C34309L}},{{0xCDE333B8L,5L,1L,0xCF1A504AL},{0xCDE333B8L,0x3A1BBDB8L,0x498A597FL,(-2L)},{0xA6C34309L,0xCF1A504AL,0xECDCC21DL,0xECDCC21DL},{0x08385637L,0x08385637L,0xAC691C6CL,0xCF1A504AL},{0xCF1A504AL,0xA6C34309L,(-9L),5L}}};
            int64_t l_3625 = 0x7A549A7EFCC65C41LL;
            int32_t l_3670 = 0x4B1B73CFL;
            int64_t ****l_3684 = &g_2187;
            int32_t l_3739[8];
            int i, j, k;
            for (i = 0; i < 8; i++)
                l_3739[i] = 0xADB586E1L;
            if ((l_3613 = (l_3612 == &g_2821)))
            { /* block id: 1502 */
                int16_t l_3618 = 0xC342L;
                int32_t l_3619 = 0x5FB330CFL;
                int32_t l_3620 = 0xD92BB849L;
                int32_t l_3621 = 9L;
                int32_t l_3622 = 0x9090F62EL;
                int32_t l_3623 = (-8L);
                int32_t l_3624 = 0xDB513C54L;
                for (g_3014 = (-26); (g_3014 < 24); g_3014 = safe_add_func_uint32_t_u_u(g_3014, 4))
                { /* block id: 1505 */
                    (**g_712) = (**g_712);
                }
                l_3626++;
            }
            else
            { /* block id: 1509 */
                const int8_t l_3633 = (-1L);
                int32_t *l_3644 = &g_607;
                int32_t l_3707 = 0xDBFF9D0CL;
                l_3645 = ((safe_mod_func_int64_t_s_s(((safe_mod_func_int8_t_s_s(l_3633, (((safe_lshift_func_int8_t_s_s((l_3636 != (void*)0), 1)) & (((safe_rshift_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((+l_3633), l_3617[4][2][0])), 7)) >= l_3617[2][3][2]) & ((*g_790) |= l_3613))) , l_3617[1][2][0]))) > ((safe_lshift_func_int8_t_s_s(((void*)0 != l_3644), (****g_712))) && l_3625)), l_3633)) , (void*)0);
                for (g_646 = 0; (g_646 <= 3); g_646 += 1)
                { /* block id: 1514 */
                    int16_t *l_3667 = &l_3483;
                    int32_t l_3668 = 0xA64D15AFL;
                    const uint64_t l_3671 = 1UL;
                    int16_t *l_3672[7][8][3] = {{{&g_130[1][2][0],&g_3563,&g_130[1][2][0]},{&g_346[3],(void*)0,&g_346[3]},{(void*)0,&g_3563,&g_346[3]},{&g_130[1][2][0],&g_129,&g_130[2][3][0]},{&g_346[3],&g_346[3],&g_3563},{&g_346[3],(void*)0,(void*)0},{&g_130[1][2][0],&l_3,(void*)0},{(void*)0,&g_346[3],&g_130[1][2][0]}},{{&g_346[3],&g_130[1][2][0],(void*)0},{&g_130[1][2][0],(void*)0,(void*)0},{&g_3563,&g_130[1][2][0],&g_3563},{&g_130[1][2][0],&g_130[1][2][0],&g_130[2][3][0]},{&g_130[2][3][0],(void*)0,&g_346[3]},{&g_130[1][2][0],&g_130[1][2][0],&g_346[3]},{&g_129,&g_346[3],&g_130[1][2][0]},{&g_130[1][2][0],&l_3,(void*)0}},{{&g_130[2][3][0],(void*)0,(void*)0},{&g_130[1][2][0],&g_346[3],(void*)0},{&g_3563,&g_129,(void*)0},{&g_130[1][2][0],&g_3563,&g_130[1][2][0]},{&g_346[3],(void*)0,&g_346[3]},{(void*)0,&g_3563,&g_346[3]},{&g_130[1][2][0],&g_129,&g_130[2][3][0]},{&g_346[3],&g_346[3],&g_3563}},{{&g_346[3],(void*)0,(void*)0},{&g_130[1][2][0],&l_3,(void*)0},{(void*)0,&g_346[3],&g_130[1][2][0]},{&g_346[3],&g_130[1][2][0],(void*)0},{&g_130[1][2][0],(void*)0,(void*)0},{&g_3563,&g_130[1][2][0],&g_3563},{&g_130[1][2][0],&g_130[1][2][0],&g_130[2][3][0]},{&g_130[2][3][0],(void*)0,&g_346[3]}},{{&g_130[1][2][0],&g_130[1][2][0],&g_346[3]},{&g_129,&g_346[3],&g_130[1][2][0]},{&g_130[1][2][0],&l_3,(void*)0},{&g_130[2][3][0],(void*)0,(void*)0},{&g_130[1][2][0],&g_346[3],(void*)0},{&g_3563,&g_129,(void*)0},{&g_130[1][2][0],&g_3563,&g_130[1][2][0]},{&g_346[3],(void*)0,&g_346[3]}},{{(void*)0,&g_3563,&g_346[3]},{&g_130[1][2][0],&g_129,&g_130[2][3][0]},{&g_346[3],&g_346[3],&g_3563},{&g_346[3],(void*)0,(void*)0},{&g_130[1][2][0],&l_3,(void*)0},{(void*)0,&g_346[3],&g_130[1][2][0]},{&g_346[3],&g_130[1][2][0],(void*)0},{&g_130[1][2][0],(void*)0,(void*)0}},{{&g_3563,&g_130[1][2][0],&g_3563},{&g_130[1][2][0],&g_130[1][2][0],&g_130[2][3][0]},{&g_130[2][3][0],(void*)0,&g_346[3]},{&g_130[1][2][0],&g_130[1][2][0],&g_346[3]},{&g_129,&g_346[3],&g_130[1][2][0]},{&g_130[1][2][0],&l_3,(void*)0},{&g_130[2][3][0],(void*)0,(void*)0},{&l_3,&g_346[3],&g_346[3]}}};
                    int32_t l_3673 = 0xD1B8DA62L;
                    int32_t l_3674 = 0x74413540L;
                    int i, j, k;
                    l_3674 &= (((7L >= (l_3673 &= (safe_mod_func_int8_t_s_s(((safe_rshift_func_int8_t_s_u(((safe_sub_func_uint8_t_u_u(((safe_div_func_uint16_t_u_u(7UL, ((***g_3027) , ((safe_add_func_uint64_t_u_u(((*g_1112) == 6L), ((safe_mod_func_int16_t_s_s((safe_sub_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u((((0xB028L & ((*l_3667) |= (((**g_1098) , (((safe_mod_func_uint8_t_u_u((~(safe_rshift_func_uint8_t_u_u(0xE0L, l_3617[0][2][0]))), (-1L))) && (-6L)) >= 0xBF64L)) , (**g_1111)))) == l_3613) >= (*g_790)), (**g_1098))) ^ l_3668), l_3633)), g_3669)) , l_3613))) || (-2L))))) , 0UL), 251UL)) >= 1L), l_3670)) == l_3671), l_3617[2][1][3])))) && (-1L)) <= l_3625);
                    for (g_1149 = 0; (g_1149 <= 3); g_1149 += 1)
                    { /* block id: 1520 */
                        int64_t *****l_3683[3];
                        uint64_t *l_3699[5] = {&g_3607,&g_3607,&g_3607,&g_3607,&g_3607};
                        uint32_t l_3706 = 1UL;
                        uint16_t l_3708 = 5UL;
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_3683[i] = &g_2186;
                        l_3479[(g_646 + 2)][(g_1149 + 2)] |= l_3670;
                        l_3706 ^= (safe_sub_func_int32_t_s_s((safe_div_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u(((((l_3674 = ((safe_mul_func_uint8_t_u_u(((*l_3500) |= (((l_3684 = &g_2187) != (g_2186 = (((*g_958) = l_3685) , &g_2187))) ^ ((((safe_unary_minus_func_int64_t_s((safe_lshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_s(l_3479[(g_646 + 2)][(g_1149 + 2)], (safe_mul_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((safe_div_func_uint16_t_u_u((l_3697 && (l_3673 = ((*l_3485) = g_3698))), 1L)), (safe_sub_func_uint16_t_u_u(0xD53CL, (l_3702 <= l_3613))))), (*g_3029))))), 10)))) , 0xF0DAF6FCL) >= 4294967286UL) > l_3633))), l_3670)) && l_3703)) && l_3704[5][3]) & l_3705) , (*g_790)), (***g_2821))), l_3633)), (***g_2821)));
                        l_3708++;
                        l_3617[7][4][2] = l_3670;
                    }
                }
                l_3616 = (-1L);
                l_3707 = (((&l_3480[6][2] == ((safe_mul_func_int8_t_s_s(((l_3633 < (!(((l_3633 | (l_3617[2][4][3] = (safe_sub_func_int32_t_s_s((((((*g_1112) && (((*g_263) && (safe_add_func_uint64_t_u_u(l_3707, (((*g_958) , (***g_2824)) > ((safe_lshift_func_uint8_t_u_s(((safe_mul_func_int8_t_s_s(l_3707, (*g_263))) >= g_3607), 7)) || l_3616))))) ^ (****g_3554))) || 1UL) != (**g_1111)) && l_3625), l_3613)))) < (-1L)) | l_3707))) >= l_3616), 0xB9L)) , l_3724[1])) ^ l_3633) != 0UL);
            }
            for (g_2985 = 23; (g_2985 > 37); g_2985 = safe_add_func_int64_t_s_s(g_2985, 8))
            { /* block id: 1540 */
                int32_t l_3727 = 0x94820B1DL;
                int64_t *****l_3734 = &g_2186;
                int64_t ******l_3733 = &l_3734;
                uint16_t *****l_3737[1][1];
                uint16_t *l_3740 = &l_4[0];
                int32_t *l_3741 = &l_3468[3];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_3737[i][j] = (void*)0;
                }
                l_3741 = func_40(((((l_3617[4][2][1] = (l_3727 < ((((*g_2097) ^= ((safe_lshift_func_uint8_t_u_u(g_1893, (safe_rshift_func_uint16_t_u_u((l_3732[2] != ((*l_3733) = &g_2186)), 14)))) < (safe_sub_func_int32_t_s_s((l_3727 >= (**g_1111)), (((((l_3738 = &g_3555) == ((g_106 = l_3617[2][1][3]) , &g_3027)) , l_3617[2][1][3]) ^ l_3613) == (****g_3554)))))) | 0x73FCE317L) && 0x0ABDCF25L))) || l_3727) <= l_3625) & (*g_3029)), l_3739[7], l_3740);
            }
        }
    }
    else
    { /* block id: 1549 */
        uint64_t l_3742 = 0x4301ED44951996ECLL;
        int32_t l_3761 = (-1L);
        int32_t *l_3823 = &g_741[0];
        int32_t l_3824 = 7L;
        int32_t l_3828 = 1L;
        int32_t l_3829 = (-2L);
        int32_t l_3830 = 1L;
        int32_t l_3831[8][4][8] = {{{(-1L),0xAD4B14A0L,0L,(-4L),0xE2309F4BL,0xBA5227E1L,6L,0x1C2B145EL},{0L,(-4L),5L,0L,(-9L),0x7725E164L,1L,(-2L)},{0xDA7D1E98L,0L,1L,0x07E1C385L,0L,0L,0x07E1C385L,1L},{2L,2L,1L,(-2L),(-4L),(-1L),7L,0L}},{{0x07E1C385L,0x1C2B145EL,0xDA7D1E98L,0xD2416CB1L,0L,0xF2F56DB8L,0xBF9E5652L,0L},{0x1C2B145EL,0x9663793AL,0x056B5D0FL,(-2L),8L,0x0A813304L,(-1L),1L},{0x56443E52L,(-8L),0x78A6BF29L,0x07E1C385L,0xD2416CB1L,(-2L),0x39816672L,(-2L)},{1L,0L,0x95970D47L,0L,(-6L),0x56443E52L,7L,0x1C2B145EL}},{{0x9585D140L,0L,0x9663793AL,(-4L),(-7L),1L,0xFDD72BB7L,(-6L)},{0x033A9354L,0x39816672L,0x452E4DEBL,(-6L),0xCF6BE472L,(-9L),0xE2309F4BL,0xAD4B14A0L},{0xF2F56DB8L,0L,0x73F57F95L,(-1L),(-1L),0L,0xAD4B14A0L,6L},{1L,0x056B5D0FL,0x0A813304L,0xDA7D1E98L,0L,0L,(-1L),0xD9DC9341L}},{{0L,0xB7212277L,(-6L),0x9663793AL,0x3CDA4607L,0x9663793AL,(-6L),0xB7212277L},{7L,7L,0L,0xCF6BE472L,(-8L),0xAD4B14A0L,0x056B5D0FL,(-1L)},{0L,0x82B93790L,1L,0x534BD24EL,7L,0x3CDA4607L,0x056B5D0FL,0x7725E164L},{(-2L),0x534BD24EL,0L,0xAD4B14A0L,0xFDD72BB7L,0x07E1C385L,(-6L),2L}},{{0xFDD72BB7L,0x07E1C385L,(-6L),2L,0x1C2B145EL,0xA8EE133CL,(-1L),0xBA5227E1L},{(-6L),0xD9DC9341L,0x0A813304L,0x8683771BL,0x0F067852L,0x452E4DEBL,0xAD4B14A0L,(-1L)},{0L,0xF98C77ABL,0x73F57F95L,0L,0x0A813304L,0xBF9E5652L,0xE2309F4BL,0x9663793AL},{0x82B93790L,0x73F57F95L,0x452E4DEBL,0xA8EE133CL,0x39816672L,0xA185F25AL,0xFDD72BB7L,8L}},{{0x8683771BL,0L,0x9663793AL,7L,(-3L),(-1L),7L,1L},{(-9L),1L,0x95970D47L,(-7L),(-6L),0x9585D140L,0x39816672L,0xCF6BE472L},{0L,(-1L),0x78A6BF29L,0x33FA989CL,0x33FA989CL,0x78A6BF29L,(-1L),8L},{0L,0x751C5C74L,0x39816672L,0x3CDA4607L,0xAD4B14A0L,0xA8EE133CL,(-2L),0x452E4DEBL}},{{0xE2309F4BL,0xF2F56DB8L,(-1L),7L,0x3CDA4607L,0xA8EE133CL,0L,1L},{0xA185F25AL,0x751C5C74L,1L,(-1L),0x07E1C385L,0x0F067852L,0xFDD72BB7L,8L},{1L,0xD2416CB1L,1L,0L,0xA8EE133CL,0L,0xB7212277L,0x534BD24EL},{0L,0xB7212277L,(-4L),(-6L),0x452E4DEBL,0xD2416CB1L,0xA185F25AL,0xA8EE133CL}},{{2L,0L,0x7725E164L,0xD2416CB1L,0xBF9E5652L,0x33FA989CL,(-8L),(-1L)},{6L,0x534BD24EL,7L,0xDA7D1E98L,0xA185F25AL,(-2L),0xBA5227E1L,0x39816672L},{0x033A9354L,(-6L),0x1C2B145EL,0x534BD24EL,(-1L),0x056B5D0FL,0xAD4B14A0L,(-8L)},{0L,0x033A9354L,0L,(-4L),0x9585D140L,0x73F57F95L,0x751C5C74L,0x2574A735L}}};
        int32_t l_3890 = (-10L);
        uint8_t l_3911 = 1UL;
        uint64_t l_3996 = 1UL;
        uint32_t **l_4029 = &g_958;
        uint32_t l_4032 = 0xBE042DE0L;
        uint16_t *l_4141 = &g_1149;
        int32_t *****l_4146 = &g_3504[0];
        uint32_t l_4183[1];
        int32_t *l_4186 = (void*)0;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_4183[i] = 0xC6791129L;
        if (l_3742)
        { /* block id: 1550 */
            int32_t **l_3744[5] = {&g_5[0][1][3],&g_5[0][1][3],&g_5[0][1][3],&g_5[0][1][3],&g_5[0][1][3]};
            uint64_t l_3760 = 0x137F1068F0A819F9LL;
            int64_t ***l_3792 = &g_181;
            uint8_t l_3814 = 252UL;
            uint16_t l_3832 = 0xC597L;
            uint16_t ****l_3969 = &g_3555;
            uint16_t l_4058 = 4UL;
            int32_t ***l_4072 = (void*)0;
            int64_t *l_4084 = &g_2759;
            uint8_t l_4095 = 255UL;
            uint8_t l_4151[9] = {0xDDL,0UL,0UL,0xDDL,0UL,0UL,0xDDL,0UL,0UL};
            uint16_t l_4160 = 0UL;
            const uint32_t ****l_4182 = &g_4179;
            int i;
            l_3743 = l_3743;
            (*l_3743) &= (1UL < 0xBD7B55C8L);
            if ((((l_3761 = (g_1149 || ((*g_790) && (safe_rshift_func_uint8_t_u_u((((*l_3645) == (safe_sub_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(((*l_3645) , (safe_lshift_func_int16_t_s_u(((safe_mod_func_uint64_t_u_u((((0x72992A0FD96F209ALL >= (((l_3759 = (g_106 &= ((!(((void*)0 != &g_1625) >= (safe_sub_func_int8_t_s_s(l_3758, 0x4AL)))) >= (*l_3645)))) | (*l_3645)) != 0xFFEF4691L)) || 8UL) & 255UL), l_3760)) < (*g_263)), 14))), (*l_3645))), g_1117))) | l_3742), 6))))) <= 0xDBL) < (*l_3645)))
            { /* block id: 1556 */
                int64_t * const l_3787 = &l_3705;
                int64_t * const *l_3786 = &l_3787;
                int64_t * const **l_3785 = &l_3786;
                int32_t l_3794 = (-6L);
                uint32_t *** const *l_3803 = &g_2824;
                uint64_t l_3820 = 0x8ABE574F6E834FDBLL;
                int32_t l_3825 = (-1L);
                int32_t l_3826 = 0xC876800FL;
                int32_t l_3827 = 0x8E1FBBDAL;
                uint8_t l_3850[6] = {0xE0L,253UL,253UL,0xE0L,253UL,253UL};
                uint16_t *****l_3853 = &g_3554;
                int32_t l_3862[10][7][1] = {{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}},{{(-5L)},{0x25D42D8BL},{9L},{0x25D42D8BL},{(-5L)},{0L},{0L}}};
                int16_t l_3881[4][10] = {{1L,0x387BL,0L,3L,3L,0L,0x387BL,1L,0x02F8L,5L},{0x387BL,0x58EBL,0x7491L,(-3L),1L,0x9429L,(-5L),3L,0x8F40L,3L},{(-3L),3L,0x7491L,1L,0x7491L,3L,(-3L),(-3L),0x8F40L,0x58EBL},{0x387BL,5L,0x8F40L,0x9429L,3L,(-1L),(-3L),(-3L),(-3L),(-3L)}};
                int32_t *l_3884 = &l_3468[3];
                int i, j, k;
                for (l_3 = 0; (l_3 >= 14); l_3++)
                { /* block id: 1559 */
                    uint8_t l_3774 = 0x3FL;
                    uint16_t * const * const ***l_3782 = (void*)0;
                    uint32_t l_3804[7][4][8] = {{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}},{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}},{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}},{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}},{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}},{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}},{{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL},{4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL,4294967290UL}}};
                    int32_t l_3817 = 0x73E5A522L;
                    uint32_t l_3822 = 0xA00B1F0BL;
                    int i, j, k;
                    if (((*l_3743) | (((*g_1112) = (safe_sub_func_int8_t_s_s(((((safe_rshift_func_uint16_t_u_s((7L | ((l_3761 |= (safe_mod_func_int8_t_s_s((&g_3503 == &g_3503), (safe_mod_func_uint16_t_u_u(l_3742, 0x217CL))))) || ((*g_1099) || l_3742))), 8)) == (!((***g_3027) == 65529UL))) <= l_3773) && l_3774), l_3742))) >= (****g_3554))))
                    { /* block id: 1562 */
                        int16_t *l_3781 = &l_3483;
                        int32_t l_3793 = 0L;
                        int32_t l_3802 = (-5L);
                        uint8_t *l_3819 = &l_3774;
                        int8_t *l_3821 = &l_3773;
                        l_3794 &= ((*g_1099) | ((((safe_add_func_uint32_t_u_u(((safe_lshift_func_int8_t_s_s((*l_3743), (l_3761 ^ (((*l_3787) = ((safe_mul_func_uint16_t_u_u((((**g_1111) = 0xFDC1L) ^ ((*l_3781) = 0x8A4AL)), ((void*)0 == l_3782))) >= ((safe_mod_func_uint32_t_u_u(((((g_3788 = l_3785) != (l_3761 , l_3792)) >= 0x5F0C6E62EBD1B2ABLL) , (*g_2097)), 1UL)) >= (-4L)))) ^ l_3793)))) , (*g_790)), 8UL)) > l_3793) ^ (****g_3554)) != 0x4D726B39L));
                        l_3794 = ((*l_3743) = (safe_add_func_int8_t_s_s((*l_3743), (((safe_div_func_int16_t_s_s((((((*g_958) , ((g_3014 , ((*g_1112) = (~(safe_unary_minus_func_uint16_t_u(((****g_3554) ^= (!l_3802))))))) >= l_3742)) , (*g_2097)) != (((void*)0 != l_3803) ^ ((-1L) | 0UL))) , (*g_1112)), 0x89C8L)) >= l_3802) > (-1L)))));
                        (*l_3645) = ((*l_3743) = ((l_3804[6][2][0] &= 0x6512L) == ((((((safe_lshift_func_uint8_t_u_u((((&l_3517 == ((safe_add_func_uint32_t_u_u((((*l_3821) = (safe_rshift_func_int16_t_s_s(((((**g_1111) > ((((~(safe_lshift_func_int16_t_s_u((((((void*)0 == &g_646) ^ ((l_3814 = l_3742) , ((safe_div_func_int32_t_s_s((l_3817 ^= (*l_3743)), (((*g_263) = 1L) ^ ((*l_3819) = ((~((l_3761 , (*g_2186)) != (void*)0)) != l_3774))))) , l_3802))) > l_3794) , (-5L)), 9))) == l_3820) & l_3761) , 0x48CFL)) == (****g_3554)) < l_3761), (*g_1112)))) > 0x6FL), 1L)) , (void*)0)) < 65527UL) & l_3761), l_3822)) , l_3761) == 0x512BFDCEE5B47ADFLL) == 0UL) > (-5L)) | (***g_3555))));
                    }
                    else
                    { /* block id: 1580 */
                        return (*g_2097);
                    }
                    (*l_3645) = (*l_3645);
                    l_3823 = &l_3466;
                    return l_3822;
                }
                --l_3832;
                for (g_1882 = 0; (g_1882 <= 4); g_1882 += 1)
                { /* block id: 1590 */
                    int32_t l_3845[6];
                    int8_t l_3852 = 4L;
                    uint16_t * const **l_3856 = (void*)0;
                    uint16_t * const ***l_3855 = &l_3856;
                    uint16_t * const ****l_3854 = &l_3855;
                    int32_t *l_3857 = (void*)0;
                    int32_t l_3858 = 0x93BADFD7L;
                    int32_t l_3859 = (-1L);
                    int32_t l_3860 = 0xCB60AA2DL;
                    int32_t l_3861 = (-10L);
                    int16_t **l_3872 = &g_2127[1];
                    uint64_t l_3887 = 0x2D7AB4BD69DB07DDLL;
                    int16_t l_3899 = 0xF09FL;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_3845[i] = (-1L);
                    for (l_3826 = 1; (l_3826 <= 7); l_3826 += 1)
                    { /* block id: 1593 */
                        int i;
                        l_3518 = (l_3857 = (((-9L) <= ((((safe_sub_func_uint8_t_u_u(((g_1970[l_3826] <= (((safe_mul_func_uint16_t_u_u(0UL, (safe_rshift_func_uint8_t_u_s((((((***g_3555) && (safe_mod_func_int8_t_s_s(l_3845[3], (safe_lshift_func_int8_t_s_u(l_3827, ((void*)0 == &l_3742)))))) ^ ((((safe_add_func_uint8_t_u_u((((((l_3845[3] | (*l_3743)) , 0x8B294ED0EE743CE0LL) >= l_3850[3]) == g_1970[l_3826]) && l_3845[3]), (-5L))) <= 0x7B7A5CB412A04347LL) >= (****g_712)) <= 1L)) != 0xD6L) < g_3851), l_3852)))) , l_3853) == l_3854)) < 0x4E5E61AE06442A5DLL), 0x8BL)) , g_1970[l_3826]) | g_1970[l_3826]) , (*l_3645))) , &l_3468[3]));
                        if ((*l_3857))
                            continue;
                    }
                    --g_3863;
                    if ((((***g_2821) = ((((*l_3787) = ((safe_lshift_func_uint16_t_u_s((l_3825 == ((safe_lshift_func_int8_t_s_s(((safe_sub_func_uint8_t_u_u(((void*)0 == l_3872), (l_3826 , ((*g_263) ^= ((-6L) ^ (((***g_3788) = 0x6C546F67B9C0BD30LL) < 3L)))))) , (safe_mul_func_int8_t_s_s(((*g_263) = ((safe_rshift_func_int16_t_s_s((**g_1111), 3)) | (safe_mul_func_uint16_t_u_u(((l_3861 |= 0x822800B51BC157B2LL) >= 0L), (*l_3823))))), g_1716[2][2][4]))), l_3881[3][5])) < 0L)), 0)) > (*l_3823))) != l_3794) , (*l_3823))) | (*l_3823)))
                    { /* block id: 1605 */
                        (*l_3645) |= (safe_rshift_func_uint8_t_u_s(g_3669, 3));
                        l_3884 = &l_3860;
                        return (***g_2821);
                    }
                    else
                    { /* block id: 1609 */
                        int32_t l_3885 = 0x613B4B87L;
                        int32_t l_3886 = 0xC4B2DB7AL;
                        if (l_3885)
                            break;
                        l_3887++;
                        l_3823 = func_40((l_3890 < (safe_lshift_func_int8_t_s_u(((**g_262) = (safe_add_func_int64_t_s_s(l_3885, ((***l_3785) |= ((safe_sub_func_uint32_t_u_u((safe_add_func_int16_t_s_s(((*l_3884) > (l_3899 , (safe_rshift_func_int8_t_s_s((((*l_3823) |= (safe_rshift_func_uint8_t_u_u(((!(safe_sub_func_int32_t_s_s((safe_sub_func_uint32_t_u_u(((***g_2821) = ((*g_790) ^= ((l_3911 >= (l_3885 , ((*g_1112) < (**g_1098)))) , (***g_2824)))), 0x233825F8L)), 0x4E9186E9L))) , l_3885), g_1158))) && 0xC6D1DB1E9CBB1E00LL), (*l_3645))))), (**g_1111))), 9L)) < (*l_3884)))))), (*l_3743)))), (*l_3884), &l_3832);
                    }
                    for (l_3832 = 0; l_3832 < 6; l_3832 += 1)
                    {
                        l_3845[l_3832] = 0x2E763AC1L;
                    }
                }
            }
            else
            { /* block id: 1621 */
                int32_t l_3929[4] = {(-1L),(-1L),(-1L),(-1L)};
                int32_t l_3936 = 0x39C68387L;
                int16_t ***l_3964 = &g_1111;
                const uint32_t *l_3974 = (void*)0;
                const uint32_t **l_3973 = &l_3974;
                int32_t l_3995[9][8][1] = {{{0x710D56A3L},{0xDC806B70L},{0x1CF66167L},{0xDC806B70L},{0x710D56A3L},{1L},{0x01503ACEL},{0xE63D487AL}},{{0x1CF66167L},{0x661C4854L},{0L},{1L},{0L},{0x661C4854L},{0x1CF66167L},{0xE63D487AL}},{{0x01503ACEL},{1L},{0x710D56A3L},{0xDC806B70L},{0x1CF66167L},{0xDC806B70L},{0x710D56A3L},{1L}},{{0x01503ACEL},{0xE63D487AL},{0x1CF66167L},{0x661C4854L},{0L},{1L},{0L},{0x661C4854L}},{{0x1CF66167L},{0xE63D487AL},{0x01503ACEL},{1L},{0x710D56A3L},{0xDC806B70L},{0x1CF66167L},{0xDC806B70L}},{{0x710D56A3L},{1L},{0x01503ACEL},{0xE63D487AL},{0x1CF66167L},{0x661C4854L},{0L},{1L}},{{0L},{0x661C4854L},{0x1CF66167L},{0xE63D487AL},{0x01503ACEL},{1L},{0x710D56A3L},{0xDC806B70L}},{{0x1CF66167L},{0xDC806B70L},{0x710D56A3L},{1L},{0x01503ACEL},{0xE63D487AL},{0x1CF66167L},{0x661C4854L}},{{0L},{1L},{0L},{0x661C4854L},{0x1CF66167L},{0xE63D487AL},{0x01503ACEL},{1L}}};
                uint64_t **l_4024 = &g_2035[5][7];
                uint8_t l_4056 = 0xC5L;
                uint16_t l_4068 = 65535UL;
                int32_t *l_4079 = &l_3829;
                uint16_t **l_4090 = &g_1099;
                uint32_t l_4121 = 4294967294UL;
                int8_t l_4135 = 8L;
                int16_t l_4136 = 0x4D17L;
                int32_t *l_4163 = &g_741[1];
                int i, j, k;
                for (l_3760 = (-18); (l_3760 != 2); ++l_3760)
                { /* block id: 1624 */
                    (*l_3743) ^= (safe_sub_func_uint64_t_u_u((((*g_3790) ^= (safe_div_func_int8_t_s_s(((safe_rshift_func_int16_t_s_s((~((****g_3554) = (((((***g_2824)--) == 4UL) , &g_2035[0][7]) != &g_2035[5][7]))), (safe_lshift_func_uint16_t_u_s(0x6F55L, (safe_mul_func_int16_t_s_s((*g_1112), (((safe_sub_func_uint8_t_u_u((l_3929[1] , (1L && (safe_rshift_func_uint8_t_u_u(0x8DL, (safe_mod_func_uint64_t_u_u((safe_mul_func_uint16_t_u_u((***g_3027), l_3929[1])), 0x7A1C22950938F233LL)))))), (*g_263))) | l_3936) <= (*l_3645)))))))) ^ (*l_3823)), (*l_3645)))) <= l_3929[1]), 0xA61644F10BCE4668LL));
                }
                if ((safe_mul_func_uint16_t_u_u(((*l_3743) >= (l_3936 &= 0L)), (safe_mul_func_uint16_t_u_u((*l_3823), l_3929[1])))))
                { /* block id: 1631 */
                    int16_t ***l_3947 = &g_1111;
                    int32_t l_3959 = 0xDEF4DCF3L;
                    uint32_t l_3980 = 18446744073709551606UL;
                    int16_t l_3991 = 0x9BB0L;
                    uint8_t *l_4020[10] = {(void*)0,(void*)0,&l_3911,(void*)0,(void*)0,&l_3911,(void*)0,(void*)0,&l_3911,(void*)0};
                    int64_t l_4021[4][1][4] = {{{0x60165212ACE2FDB4LL,(-8L),(-8L),0x60165212ACE2FDB4LL}},{{(-8L),0x60165212ACE2FDB4LL,(-8L),(-8L)}},{{0x60165212ACE2FDB4LL,0x60165212ACE2FDB4LL,0xE4C6745617689ACDLL,0x60165212ACE2FDB4LL}},{{0x60165212ACE2FDB4LL,(-8L),(-8L),0x60165212ACE2FDB4LL}}};
                    int64_t l_4022 = 4L;
                    int64_t l_4023 = 0xE88EBBB52815D426LL;
                    uint32_t l_4025 = 0UL;
                    uint16_t l_4026[7][5] = {{1UL,0xAF76L,1UL,0x688DL,0x688DL},{0x2566L,0x1998L,0x2566L,9UL,9UL},{1UL,0xAF76L,1UL,0x688DL,0x688DL},{0x2566L,0x1998L,0x2566L,9UL,9UL},{1UL,0xAF76L,1UL,0x688DL,0x688DL},{0x2566L,0x1998L,0x2566L,9UL,9UL},{1UL,0xAF76L,1UL,0x688DL,0x688DL}};
                    const int32_t *l_4061 = &l_3704[4][1];
                    int32_t ***l_4071 = &g_668;
                    uint32_t ***l_4074 = &l_4029;
                    int i, j, k;
lbl_4027:
                    if (((safe_sub_func_int8_t_s_s((safe_add_func_uint16_t_u_u(0xD064L, (l_3945 , (*l_3823)))), (*l_3645))) > (safe_unary_minus_func_uint32_t_u((((****g_712) = ((void*)0 != &l_3945)) >= (&g_1112 == ((*l_3947) = &g_1112)))))))
                    { /* block id: 1634 */
                        uint64_t l_3970 = 0xA2A1B8C2CB0A6C96LL;
                        int32_t *****l_3971 = &g_3504[0];
                        uint8_t *l_3975 = &l_3685;
                        int8_t *l_3976[10][5] = {{&g_598,&g_125,&g_125,&l_3773,&l_3773},{&g_125,&l_3773,&g_125,&g_125,&g_598},{&g_125,&g_24,(void*)0,&g_598,&g_598},{&l_3773,&g_598,(void*)0,&g_24,&g_125},{&g_24,&l_3773,(void*)0,&g_598,&g_24},{(void*)0,&l_3773,(void*)0,&g_125,&g_125},{(void*)0,&g_598,(void*)0,&g_125,&g_598},{&g_125,&g_24,(void*)0,&g_598,&g_598},{&l_3773,&g_598,(void*)0,&g_24,&g_125},{&g_24,&l_3773,(void*)0,&g_598,&g_24}};
                        uint16_t ****l_3979 = &g_3555;
                        int32_t l_3989 = (-1L);
                        int32_t l_3990 = 0x86EEC211L;
                        int16_t l_3992 = 0x2C2EL;
                        int32_t l_3993 = 8L;
                        int32_t l_3994[3][10] = {{0xE78DDC98L,0xE78DDC98L,0xAC6206D4L,0xE78DDC98L,0xE78DDC98L,0xAC6206D4L,0xE78DDC98L,0xE78DDC98L,0xAC6206D4L,0xE78DDC98L},{0xE78DDC98L,(-6L),(-6L),0xE78DDC98L,(-6L),(-6L),0xE78DDC98L,(-6L),(-6L),0xE78DDC98L},{(-6L),0xE78DDC98L,(-6L),(-6L),0xE78DDC98L,(-6L),(-6L),0xE78DDC98L,(-6L),(-6L)}};
                        int i, j;
                        (*l_3645) = ((**g_1111) , (safe_div_func_uint64_t_u_u(0x1657138A263075C3LL, (((safe_rshift_func_int8_t_s_s(g_3952, 1)) == (((safe_rshift_func_uint8_t_u_u((((((safe_div_func_int32_t_s_s((safe_div_func_int16_t_s_s(((((l_3959 , 0x3F55E2A5L) || (((safe_sub_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u((((*l_3975) |= (((l_3964 == &g_1111) , ((safe_lshift_func_int16_t_s_s((**g_1111), 8)) , ((((safe_lshift_func_uint16_t_u_u(((((((l_3969 == (void*)0) >= l_3970) <= (*l_3823)) , l_3971) == (void*)0) <= 0xCCL), 7)) , l_3972) == l_3973) , 0UL))) , 8UL)) >= 0xF4L), 0)), 1L)) || 65528UL) ^ (***g_2821))) ^ (***g_2824)) == 0x6C54924208B8FC1ALL), 0x3210L)), l_3970)) && l_3929[2]) , (void*)0) != &g_1112) ^ l_3929[2]), 4)) , l_3929[0]) || l_3970)) || (***g_2824)))));
                        if (g_6)
                            goto lbl_4001;
                        (*l_3823) = (((l_3976[3][1] != ((safe_mod_func_uint32_t_u_u(((***g_2824) = ((*l_3645) != ((l_3979 != (l_3980 , &g_3027)) , (safe_sub_func_int32_t_s_s((*l_3823), 4294967295UL))))), (safe_mul_func_uint16_t_u_u((safe_mod_func_int32_t_s_s(l_3959, (safe_sub_func_int32_t_s_s(((*g_1112) | 65531UL), 0xF17241A6L)))), (-2L))))) , (void*)0)) | l_3980) ^ l_3959);
                        ++l_3996;
                        if (g_1117)
                            goto lbl_4027;
                        return (***g_2821);
                    }
                    else
                    { /* block id: 1641 */
lbl_4001:
                        (*l_3743) = (l_3991 , (safe_mul_func_uint8_t_u_u((*l_3645), 0x30L)));
                        l_3995[8][0][0] = (safe_sub_func_uint8_t_u_u((((safe_mod_func_int32_t_s_s(((((safe_div_func_uint32_t_u_u((*l_3823), ((safe_mul_func_uint8_t_u_u((((safe_rshift_func_uint8_t_u_u(((*l_3823) , (l_4021[2][0][1] |= (safe_lshift_func_int8_t_s_s((safe_add_func_uint8_t_u_u((253UL > (-10L)), 255UL)), ((*g_263) = (safe_mod_func_int64_t_s_s((0x0CL ^ (((*l_3823) , (((safe_unary_minus_func_uint32_t_u((l_3995[2][6][0] != (g_4019 != (*l_3823))))) , (*l_3743)) ^ (*g_263))) != 0x5370C7C5L)), 1UL))))))), l_3959)) >= l_4022) || (****g_3554)), l_4023)) | (*l_3823)))) , &g_2035[8][8]) != l_4024) <= l_4025), (-1L))) ^ (***g_3788)) < 18446744073709551615UL), l_4026[1][1]));
                    }
                    if (((*l_3823) ^= ((~((void*)0 == l_4029)) || 0x4BE3L)))
                    { /* block id: 1650 */
                        l_3518 = (void*)0;
                    }
                    else
                    { /* block id: 1652 */
                        uint8_t **l_4037 = (void*)0;
                        uint8_t **l_4039 = &g_1487;
                        uint8_t ***l_4038 = &l_4039;
                        const int32_t l_4053 = (-1L);
                        int8_t *l_4057[9] = {&l_3773,&l_3773,&l_3773,&l_3773,&l_3773,&l_3773,&l_3773,&l_3773,&l_3773};
                        const int32_t *l_4060[6][6] = {{&l_3829,&l_3479[2][0],&l_3959,&l_3829,&l_3445,&g_3014},{&l_3829,&l_3445,&g_3014,&l_3479[2][0],&l_3479[2][0],&g_3014},{(void*)0,(void*)0,&l_3959,&l_3995[1][7][0],&l_3479[2][0],&g_176},{&l_3479[2][0],&l_3445,(void*)0,&l_3995[1][7][0],&l_3445,&l_3959},{(void*)0,&l_3479[2][0],(void*)0,&l_3479[2][0],(void*)0,&g_176},{&l_3829,&l_3479[2][0],&l_3959,&l_3829,&l_3445,&g_3014}};
                        const int32_t **l_4059[10][9][2] = {{{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]},{&l_4060[4][1],&l_4060[0][4]},{&l_4060[4][1],&l_4060[4][1]},{&l_4060[1][2],(void*)0},{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]},{(void*)0,&l_4060[0][4]},{(void*)0,&l_4060[4][1]}},{{(void*)0,&l_4060[0][4]},{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]},{(void*)0,(void*)0},{&l_4060[1][2],&l_4060[4][1]},{&l_4060[4][1],&l_4060[0][4]},{&l_4060[4][1],&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]},{(void*)0,&l_4060[2][0]}},{{&l_4060[0][4],&l_4060[4][1]},{&l_4060[1][3],&l_4060[0][4]},{&l_4060[1][2],&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]},{&l_4060[1][2],&l_4060[0][4]},{&l_4060[1][3],&l_4060[4][1]},{&l_4060[0][4],&l_4060[2][0]},{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]}},{{&l_4060[4][1],&l_4060[0][4]},{&l_4060[4][1],&l_4060[4][1]},{&l_4060[1][2],(void*)0},{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[0][4]},{(void*)0,&l_4060[0][4]},{(void*)0,&l_4060[4][1]},{(void*)0,&l_4060[0][4]},{(void*)0,&l_4060[0][4]}},{{&l_4060[0][4],&l_4060[0][4]},{&l_4060[1][4],(void*)0},{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[4][1]},{&l_4060[0][4],&l_4060[4][1]},{&l_4060[1][2],&l_4060[1][2]},{&l_4060[0][4],(void*)0},{&l_4060[0][4],&l_4060[0][4]},{&l_4060[2][0],(void*)0}},{{(void*)0,(void*)0},{&l_4060[1][2],(void*)0},{(void*)0,(void*)0},{&l_4060[2][0],&l_4060[0][4]},{&l_4060[0][4],(void*)0},{&l_4060[0][4],&l_4060[1][2]},{&l_4060[1][2],&l_4060[4][1]},{&l_4060[0][4],&l_4060[4][1]},{&l_4060[0][4],&l_4060[0][4]}},{{(void*)0,(void*)0},{&l_4060[1][4],&l_4060[0][4]},{&l_4060[1][2],&l_4060[1][3]},{&l_4060[0][4],&l_4060[1][2]},{&l_4060[0][4],&l_4060[0][4]},{&l_4060[0][4],&l_4060[1][2]},{&l_4060[0][4],&l_4060[1][3]},{&l_4060[1][2],&l_4060[0][4]},{&l_4060[1][4],(void*)0}},{{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[4][1]},{&l_4060[0][4],&l_4060[4][1]},{&l_4060[1][2],&l_4060[1][2]},{&l_4060[0][4],(void*)0},{&l_4060[0][4],&l_4060[0][4]},{&l_4060[2][0],(void*)0},{(void*)0,(void*)0},{&l_4060[1][2],(void*)0}},{{(void*)0,(void*)0},{&l_4060[2][0],&l_4060[0][4]},{&l_4060[0][4],(void*)0},{&l_4060[0][4],&l_4060[1][2]},{&l_4060[1][2],&l_4060[4][1]},{&l_4060[0][4],&l_4060[4][1]},{&l_4060[0][4],&l_4060[0][4]},{(void*)0,(void*)0},{&l_4060[1][4],&l_4060[0][4]}},{{&l_4060[1][2],&l_4060[1][3]},{&l_4060[0][4],&l_4060[1][2]},{&l_4060[0][4],&l_4060[0][4]},{&l_4060[0][4],&l_4060[1][2]},{&l_4060[0][4],&l_4060[1][3]},{&l_4060[1][2],&l_4060[0][4]},{&l_4060[1][4],(void*)0},{(void*)0,&l_4060[0][4]},{&l_4060[0][4],&l_4060[4][1]}}};
                        int i, j, k;
                        (*l_3823) ^= (safe_add_func_uint8_t_u_u(((***g_2821) <= 0x7296B1B3L), l_4032));
                        l_4061 = ((safe_mod_func_int32_t_s_s((safe_lshift_func_int8_t_s_s((l_4058 = (((((l_3995[8][0][0] , l_4037) == ((*l_4038) = &g_1487)) , ((safe_mod_func_uint16_t_u_u((safe_div_func_int16_t_s_s((safe_unary_minus_func_int8_t_s((safe_mod_func_uint8_t_u_u((((***g_3788) &= (safe_lshift_func_int8_t_s_u(((((g_2 &= ((***g_3555) = (safe_sub_func_int64_t_s_s(0x062805EB407D834ALL, l_4021[1][0][3])))) || (safe_rshift_func_uint16_t_u_s(((l_4053 | (++(***g_2824))) || 0UL), 14))) && l_3929[0]) != (((*g_263) = (255UL || (-1L))) < l_4056)), 7))) <= (*l_3823)), 1L)))), l_3980)), (**g_1111))) , (void*)0)) != (void*)0) != l_3929[1])), (*l_3823))), (-1L))) , &l_4053);
                        (*l_3823) = (g_4062 = (*l_3645));
                    }
                    if ((0xE11331BE535BFF7BLL & (safe_sub_func_uint8_t_u_u((l_4056 , (l_3959 = (0xC7L > l_4065[7][0]))), (safe_lshift_func_int8_t_s_s((((l_4068--) != (l_3936 ^= ((&l_3973 != (((l_4072 = l_4071) == l_4073) , (g_2729 = l_4074))) , (safe_lshift_func_uint8_t_u_s((safe_add_func_uint64_t_u_u(l_3995[2][4][0], (*l_3645))), 6))))) <= 0L), 2))))))
                    { /* block id: 1670 */
                        l_4079 = &l_3704[5][3];
                    }
                    else
                    { /* block id: 1672 */
                        int32_t l_4081[10][2];
                        int i, j;
                        for (i = 0; i < 10; i++)
                        {
                            for (j = 0; j < 2; j++)
                                l_4081[i][j] = 0x4905D087L;
                        }
                        l_4081[4][1] = ((*l_3645) = l_4080);
                    }
                }
                else
                { /* block id: 1676 */
                    uint8_t l_4097 = 255UL;
                    int32_t l_4105 = (-7L);
                    uint16_t *l_4122[3][1][9] = {{{&l_3832,&g_2048,&l_3832,&g_2048,&l_3832,&g_2048,&l_3832,&g_2048,&l_3832}},{{&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2,&g_2}},{{&l_3832,&g_2048,&l_3832,&g_2048,&l_3832,&g_2048,&l_3832,&g_2048,&l_3832}}};
                    uint32_t l_4139 = 0xE1DB35AFL;
                    uint32_t l_4140 = 18446744073709551615UL;
                    int32_t *l_4142 = (void*)0;
                    uint32_t l_4154 = 4294967289UL;
                    int i, j, k;
                    if ((safe_sub_func_uint8_t_u_u((l_4084 != (l_4085 = &l_3550)), (safe_mod_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(((l_4090 != (void*)0) & (((safe_sub_func_uint16_t_u_u((*l_3823), 1L)) == (*g_2097)) , (safe_rshift_func_int16_t_s_s((((246UL || 0x82L) > 0xBD939450L) | (*l_3823)), 8)))), (*l_4079))), l_4095)))))
                    { /* block id: 1678 */
                        uint16_t l_4096 = 0x9247L;
                        (*l_3743) = l_4096;
                        ++l_4097;
                    }
                    else
                    { /* block id: 1681 */
                        uint32_t l_4102 = 0x2E8735BAL;
                        int64_t *****l_4138 = (void*)0;
                        int64_t ******l_4137 = &l_4138;
                        l_3743 = func_40((l_4097 > (safe_sub_func_int32_t_s_s(l_4102, ((l_4105 = 0xB4L) > 0UL)))), (*l_3823), (***g_3554));
                        l_4079 = func_40(((safe_lshift_func_int8_t_s_s(((*g_790) , ((&g_3503 == &g_3503) != (safe_sub_func_int32_t_s_s(0x59E68A70L, (safe_lshift_func_int8_t_s_u((safe_mod_func_int16_t_s_s(((~(safe_add_func_uint32_t_u_u(0x87957716L, (--(***g_2824))))) & (l_4121 &= ((**g_3789) = ((*l_4085) &= (0x99021E48L <= ((((safe_sub_func_uint64_t_u_u(0xA6295F52C98F5F01LL, ((((&g_2822 == (void*)0) , (*l_3823)) , (-1L)) , 0x914166A7AED6EE6FLL))) , (void*)0) == (void*)0) <= (*l_3645))))))), (*l_3645))), g_2694)))))), 2)) ^ (-1L)), l_4102, l_4122[0][0][4]);
                        l_4142 = func_40((!(((safe_lshift_func_int8_t_s_u(((((safe_rshift_func_uint16_t_u_s(l_4097, 10)) , (((safe_unary_minus_func_int16_t_s(((***g_3555) , ((((0x1AL < ((&g_2264 != (void*)0) <= (safe_mul_func_uint16_t_u_u((***g_3027), (safe_div_func_uint16_t_u_u(65532UL, (****g_3554))))))) != (((safe_rshift_func_int16_t_s_s(((l_4135 , l_4097) <= (**g_1111)), l_4136)) > (*l_3645)) < (*l_4079))) >= l_4097) && g_741[4])))) < (*l_4079)) , (void*)0)) == l_4137) >= (*l_3743)), l_4097)) < l_4139) | l_4140)), l_4105, l_4141);
                    }
                    if ((~3UL))
                    { /* block id: 1691 */
                        int32_t *****l_4147 = &g_3504[2];
                        uint8_t *l_4148 = (void*)0;
                        uint8_t *l_4149 = &l_4056;
                        int32_t l_4150[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
                        int i;
                        (*l_3645) = ((safe_div_func_uint8_t_u_u((((l_4146 == l_4147) < ((***g_3555) = (***g_3027))) && (*l_4079)), (((((*l_4149) |= ((*l_3743) <= (*g_3790))) != ((((void*)0 != &g_3502[2][0]) | 8UL) & (**g_1111))) > 0x5A69L) | l_4150[3]))) == l_4151[3]);
                    }
                    else
                    { /* block id: 1695 */
                        uint16_t l_4152 = 0x4725L;
                        int32_t l_4153 = 0L;
                        l_4152 = ((*l_3645) |= (*l_4079));
                        l_4154++;
                    }
                    l_4157++;
                }
                l_4160++;
                l_3645 = l_4163;
            }
            l_3518 = func_40((**g_3789), ((0x0B32FB7CL > ((safe_div_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((((safe_div_func_uint32_t_u_u(((***g_2821) < ((safe_mul_func_uint16_t_u_u(((!((((safe_mod_func_int8_t_s_s(((((*l_3823) | (***g_2824)) && (****g_712)) == ((*l_3743) != (safe_mod_func_int32_t_s_s((l_4065[7][0] ^= (((((*l_4182) = g_4179) == &g_2822) , (**g_3028)) >= 0x16E3L)), (*l_3645))))), l_4183[0])) | (*g_3790)) , (***g_2821)) & 0x8EB68C43L)) , (*g_3029)), (**g_1098))) != (*l_3645))), 0xEA52E239L)) <= (*l_3823)) != (*l_3743)), 249UL)) & (*l_3645)), (*l_3823))) && g_1970[4])) < (*l_3645)), (*g_1098));
        }
        else
        { /* block id: 1708 */
            const int32_t **l_4185 = &l_4184;
            (*l_4185) = l_4184;
        }
        l_3518 = l_4186;
    }
    return (*g_790);
}


/* ------------------------------------------ */
/* 
 * reads : g_790 g_2821 g_2822 g_2097 g_1664 g_741 g_3028 g_3029 g_209 g_996 g_1098 g_1099
 * writes: g_98 g_1664 g_3140 g_996
 */
static int32_t  func_8(uint64_t  p_9, int64_t  p_10, int32_t * p_11, int32_t * p_12, int32_t  p_13)
{ /* block id: 1394 */
    uint8_t *l_3426 = (void*)0;
    int32_t l_3429 = 1L;
    uint64_t *l_3439 = (void*)0;
    uint64_t *l_3440 = &g_3140;
    const uint16_t l_3441[4][8][4] = {{{0xC71BL,0x1DB9L,0xE1B5L,0UL},{65535UL,0xE1B5L,0x7DBCL,0xE1B5L},{0x7DBCL,0xE1B5L,65535UL,0UL},{0xE1B5L,0x1DB9L,0xC71BL,0x7DBCL},{0UL,0UL,0UL,0UL},{0UL,0UL,0xC71BL,7UL},{0xE1B5L,0UL,65535UL,0UL},{0x7DBCL,0x2FF9L,0x7DBCL,0UL}},{{65535UL,0UL,0xE1B5L,7UL},{0xC71BL,0UL,0UL,0UL},{0UL,0UL,0UL,0x7DBCL},{0xC71BL,0x1DB9L,0xE1B5L,0UL},{65535UL,0xE1B5L,0x7DBCL,0xE1B5L},{0UL,0x2FF9L,0xC71BL,0UL},{0x2FF9L,65535UL,0x7DBCL,0UL},{0UL,0UL,0UL,0UL}},{{0UL,0UL,0x7DBCL,0x1DB9L},{0x2FF9L,0UL,0xC71BL,0xE1B5L},{0UL,7UL,0UL,0xE1B5L},{0xC71BL,0UL,0x2FF9L,0x1DB9L},{0x7DBCL,0UL,0UL,0UL},{0UL,0UL,0UL,0UL},{0x7DBCL,65535UL,0x2FF9L,0UL},{0xC71BL,0x2FF9L,0UL,0x2FF9L}},{{0UL,0x2FF9L,0xC71BL,0UL},{0x2FF9L,65535UL,0x7DBCL,0UL},{0UL,0UL,0UL,0UL},{0UL,0UL,0x7DBCL,0x1DB9L},{0x2FF9L,0UL,0xC71BL,0xE1B5L},{0UL,7UL,0UL,0xE1B5L},{0xC71BL,0UL,0x2FF9L,0x1DB9L},{0x7DBCL,0UL,0UL,0UL}}};
    int64_t *l_3442 = &g_996;
    int32_t *l_3443 = &g_779;
    int i, j, k;
    l_3443 = func_40(((*l_3442) |= (8UL || (((safe_lshift_func_int8_t_s_u(((((((void*)0 == l_3426) < (((((l_3429 = (safe_mod_func_uint32_t_u_u(((*g_790) = 1UL), ((***g_2821)--)))) && ((safe_sub_func_uint64_t_u_u(((*l_3440) = (!(safe_div_func_int64_t_s_s(((((void*)0 == &g_2035[8][1]) ^ p_9) != ((safe_div_func_uint64_t_u_u(0x1F291BC26B6347B5LL, (0xB9EE71DBL && (*p_12)))) == p_9)), p_9)))), l_3441[2][0][3])) , (**g_3028))) & p_9) , l_3441[2][0][3]) > l_3441[2][0][3])) ^ p_9) != l_3441[2][0][3]) ^ 7L), 4)) != 3L) && 0xD88B4ED0L))), l_3441[2][2][3], (*g_1098));
    return (*l_3443);
}


/* ------------------------------------------ */
/* 
 * reads : g_262 g_263 g_125 g_741
 * writes: g_28
 */
static const int8_t  func_14(int32_t  p_15, uint32_t  p_16)
{ /* block id: 947 */
    int64_t *l_2369 = &g_996;
    uint16_t *l_2375 = &g_2048;
    uint64_t *l_2378 = (void*)0;
    int32_t l_2379[4][3] = {{(-9L),0L,0L},{(-9L),0L,0L},{(-9L),0L,0L},{(-9L),0L,0L}};
    int32_t l_2384[5];
    int8_t *l_2385 = &g_598;
    int64_t *l_2386 = &g_106;
    int8_t ** const *l_2388 = &g_262;
    int8_t ** const ** const l_2387 = &l_2388;
    uint16_t l_2430[7] = {0x529FL,7UL,7UL,0x529FL,7UL,7UL,0x529FL};
    int16_t l_2528 = 0x54B0L;
    uint8_t *l_2548[3][1];
    uint8_t l_2607 = 254UL;
    uint32_t * const *l_2650 = &g_2097;
    int32_t l_2652 = 0x4B8E965DL;
    int32_t *l_2716 = &g_741[4];
    uint32_t **l_2748[1];
    int64_t l_2753 = 0x336043B11B1AF215LL;
    uint16_t l_2788 = 2UL;
    int8_t *****l_2819[3][6];
    uint32_t l_2851 = 4294967295UL;
    uint16_t l_2974 = 0xAA00L;
    uint16_t l_3071 = 0x51ACL;
    uint32_t l_3112[2];
    uint32_t l_3119 = 4294967295UL;
    uint32_t l_3128 = 0x80DA1FABL;
    uint32_t l_3356 = 1UL;
    uint8_t l_3406 = 8UL;
    int i, j;
    for (i = 0; i < 5; i++)
        l_2384[i] = 0xF3BED5FCL;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
            l_2548[i][j] = &g_1970[3];
    }
    for (i = 0; i < 1; i++)
        l_2748[i] = &g_958;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
            l_2819[i][j] = &g_712;
    }
    for (i = 0; i < 2; i++)
        l_3112[i] = 0x8C42BCD6L;
    for (g_28 = 17; (g_28 < 9); g_28 = safe_sub_func_uint16_t_u_u(g_28, 6))
    { /* block id: 950 */
        return (**g_262);
    }
    return (*l_2716);
}


/* ------------------------------------------ */
/* 
 * reads : g_6 g_263 g_125 g_598 g_1112 g_346 g_1099 g_28 g_790 g_98
 * writes: g_6
 */
static uint32_t  func_17(uint32_t  p_18, uint8_t  p_19, int32_t  p_20)
{ /* block id: 12 */
    int8_t *l_54[2];
    uint32_t l_1558[2][4][6] = {{{1UL,3UL,0x0D923704L,1UL,0x35F48C85L,1UL},{1UL,0x35F48C85L,1UL,0x0D923704L,3UL,1UL},{4294967291UL,4294967290UL,0x0D923704L,4294967295UL,3UL,4294967291UL},{0x0D923704L,0x35F48C85L,4294967295UL,4294967295UL,0x35F48C85L,0x0D923704L}},{{4294967291UL,3UL,4294967295UL,0x0D923704L,4294967290UL,4294967291UL},{1UL,3UL,0x0D923704L,1UL,0x35F48C85L,1UL},{1UL,0x35F48C85L,1UL,0x0D923704L,3UL,1UL},{4294967291UL,4294967290UL,0x0D923704L,4294967295UL,3UL,4294967291UL}}};
    int32_t l_1565 = 0x6D9518D1L;
    int32_t l_1569 = (-1L);
    int32_t l_1570 = (-6L);
    int32_t l_1571[5];
    int8_t ***l_1587[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int8_t ***l_1588 = &g_262;
    int32_t l_1589[3];
    uint16_t *l_1599 = &g_209;
    int32_t l_1725[3];
    uint32_t ** const l_1747[9][4] = {{&g_790,&g_790,(void*)0,(void*)0},{&g_790,(void*)0,&g_790,&g_790},{&g_790,&g_790,(void*)0,&g_790},{&g_790,(void*)0,&g_790,(void*)0},{&g_790,&g_790,(void*)0,&g_790},{&g_790,(void*)0,(void*)0,&g_790},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_790,&g_790,(void*)0,&g_790},{&g_790,(void*)0,&g_790,&g_790}};
    int64_t l_1760 = (-10L);
    int64_t *l_1781[1][1];
    int64_t **l_1780 = &l_1781[0][0];
    int32_t *l_1808 = &l_1725[0];
    uint8_t l_1817 = 0x51L;
    int32_t ** const *l_1823 = &g_668;
    int32_t ** const **l_1822 = &l_1823;
    int32_t l_1881 = 0x794862D3L;
    uint8_t l_1883 = 0x7BL;
    uint8_t l_1981 = 0xCDL;
    int8_t l_2012 = 0x41L;
    int64_t ***l_2024 = (void*)0;
    int64_t ****l_2023 = &l_2024;
    uint16_t l_2098[8][5] = {{65529UL,0UL,0UL,65529UL,0UL},{0x0D23L,0UL,0UL,65527UL,0UL},{65527UL,0UL,0x421CL,65535UL,0UL},{65529UL,0UL,65527UL,65527UL,0UL},{1UL,65529UL,0x0D8FL,0x7969L,0UL},{0UL,1UL,0UL,0UL,0UL},{65529UL,0UL,0x0D23L,0UL,0UL},{0UL,0x0D8FL,0UL,0x0D23L,1UL}};
    int8_t l_2134[6] = {0xC1L,0xC1L,0x3BL,0xC1L,0xC1L,0x3BL};
    uint32_t l_2143 = 0xFB3CEB98L;
    int16_t l_2172 = 0xEAF3L;
    uint16_t l_2209[4][5][10] = {{{0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L,0x011CL,1UL,0xC081L,0xAA24L,0UL},{0xC081L,1UL,7UL,0UL,0x5BC5L,0xC081L,0xBB70L,0x011CL,0xAA24L,0x5BC5L},{0x62C7L,1UL,7UL,0x5BC5L,0xAA24L,0x011CL,0xBB70L,0xC081L,0x5BC5L,0UL},{0x62C7L,1UL,0x011CL,0UL,0xAA24L,0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L},{0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L,0x011CL,1UL,0xC081L,0xAA24L,0UL}},{{0xC081L,1UL,7UL,0UL,0x5BC5L,0xC081L,0xBB70L,0x011CL,0xAA24L,0x5BC5L},{0x62C7L,1UL,7UL,0x5BC5L,0xAA24L,0x011CL,0xBB70L,0xC081L,0x5BC5L,0UL},{0x62C7L,1UL,0x011CL,0UL,0xAA24L,0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L},{0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L,0x011CL,1UL,0xC081L,0xAA24L,0UL},{0xC081L,1UL,7UL,0UL,0x5BC5L,0xC081L,0xBB70L,0x011CL,0xAA24L,0x5BC5L}},{{0x62C7L,1UL,7UL,0x5BC5L,0xAA24L,0x011CL,0xBB70L,0xC081L,0x5BC5L,0UL},{0x62C7L,1UL,0x011CL,0UL,0xAA24L,0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L},{0xC081L,1UL,0x011CL,0x5BC5L,0x5BC5L,0x011CL,1UL,0xC081L,0xAA24L,0UL},{0xC081L,1UL,7UL,0UL,0x5BC5L,0xC081L,0xBB70L,0x011CL,0xAA24L,0x5BC5L},{0x62C7L,1UL,7UL,0x5BC5L,0xAA24L,0x011CL,0xBB70L,0xC081L,0x5BC5L,0UL}},{{0x62C7L,0x7074L,0x483BL,7UL,0xC081L,0x03D1L,7UL,0x483BL,0x62C7L,0x62C7L},{0x03D1L,7UL,0x483BL,0x62C7L,0x62C7L,0x483BL,7UL,0x03D1L,0xC081L,7UL},{0x03D1L,0x7074L,0x68A2L,7UL,0x62C7L,0x03D1L,0xA2B3L,0x483BL,0xC081L,0x62C7L},{0x0335L,7UL,0x68A2L,0x62C7L,0xC081L,0x483BL,0xA2B3L,0x03D1L,0x62C7L,7UL},{0x0335L,0x7074L,0x483BL,7UL,0xC081L,0x03D1L,7UL,0x483BL,0x62C7L,0x62C7L}}};
    int16_t l_2322 = 0x4484L;
    int8_t l_2335[5][9][2] = {{{(-1L),0x72L},{0x8FL,0x84L},{0x8FL,0x72L},{(-1L),0x8FL},{0x72L,0x84L},{1L,1L},{(-1L),1L},{1L,0x84L},{0x72L,0x8FL}},{{(-1L),0x72L},{0x8FL,0x84L},{0x8FL,0x72L},{(-1L),0x8FL},{0x72L,0x84L},{1L,1L},{(-1L),1L},{1L,0x84L},{0x72L,0x8FL}},{{(-1L),0x72L},{0x8FL,0x84L},{0x8FL,0x72L},{(-1L),0x8FL},{0x72L,0x84L},{1L,1L},{(-1L),1L},{1L,0x84L},{0x72L,0x8FL}},{{(-1L),0x72L},{0x8FL,0x84L},{0x8FL,0x72L},{(-1L),0x8FL},{0x72L,0x84L},{1L,1L},{(-1L),1L},{1L,0x84L},{0x72L,0x8FL}},{{(-1L),0x72L},{0x8FL,0x84L},{0x8FL,0x72L},{(-1L),0x8FL},{0x72L,0x84L},{1L,1L},{(-1L),1L},{1L,0x84L},{0x72L,0x8FL}}};
    uint32_t l_2361 = 18446744073709551615UL;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_54[i] = &g_24;
    for (i = 0; i < 5; i++)
        l_1571[i] = 0xDC80AB4CL;
    for (i = 0; i < 3; i++)
        l_1589[i] = 0xF2712FDAL;
    for (i = 0; i < 3; i++)
        l_1725[i] = 0x0BB8B2BCL;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 1; j++)
            l_1781[i][j] = &l_1760;
    }
    for (p_20 = 0; (p_20 <= 11); p_20 = safe_add_func_uint16_t_u_u(p_20, 8))
    { /* block id: 15 */
        uint32_t l_81 = 4294967294UL;
        uint32_t l_84[4][6] = {{18446744073709551606UL,6UL,0xABF266DEL,0xEC891F7DL,0xEC891F7DL,0xABF266DEL},{8UL,8UL,0x93BB3FD4L,0xEC891F7DL,6UL,1UL},{18446744073709551606UL,0x93BB3FD4L,18446744073709551615UL,1UL,18446744073709551615UL,0x93BB3FD4L},{0xEC891F7DL,18446744073709551606UL,18446744073709551615UL,18446744073709551614UL,8UL,1UL}};
        uint16_t *l_1559 = (void*)0;
        int32_t l_1566 = 9L;
        int32_t l_1567 = 9L;
        uint32_t l_1572 = 0x936CB608L;
        int32_t *l_1593 = &g_741[3];
        uint64_t *l_1639 = &g_268[3];
        int32_t l_1656 = (-7L);
        int32_t l_1661 = 0xD19F5143L;
        int32_t l_1662 = 0xCCDA1044L;
        int32_t l_1663[7][2][8] = {{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}},{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}},{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}},{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}},{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}},{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}},{{(-2L),(-9L),1L,0x32621166L,1L,0xE674D1F9L,0x226EE408L,0x226EE408L},{0xE674D1F9L,(-9L),0x32621166L,0x32621166L,(-9L),0xE674D1F9L,(-8L),0x226EE408L}}};
        int64_t ***l_1675 = &g_181;
        int64_t ****l_1674 = &l_1675;
        int16_t l_1717 = 0xCFEBL;
        int8_t ** const l_1724 = &g_263;
        uint16_t *l_1761 = &g_1149;
        int64_t *l_1762 = (void*)0;
        int64_t *l_1763 = &g_106;
        int32_t *l_1764 = &l_1566;
        int8_t ****l_1765[9][6][4] = {{{&l_1587[4],&l_1587[2],(void*)0,&g_713[3]},{&l_1588,&l_1587[0],(void*)0,&l_1587[4]},{&l_1588,(void*)0,&l_1587[2],&l_1588},{&g_713[1],&g_713[1],&g_713[1],(void*)0},{(void*)0,&g_713[2],&l_1587[4],&l_1588},{(void*)0,&l_1587[4],&l_1588,&g_713[2]}},{{&g_713[1],&l_1587[2],&l_1588,&l_1587[4]},{(void*)0,&l_1587[1],&l_1587[4],&l_1587[1]},{(void*)0,&l_1587[5],&g_713[1],&g_713[2]},{&g_713[1],&g_713[2],&l_1587[2],&l_1587[4]},{&l_1588,&l_1587[4],(void*)0,(void*)0},{&l_1588,&l_1588,(void*)0,(void*)0}},{{&l_1587[4],&g_713[3],&l_1587[5],&g_713[2]},{(void*)0,&g_713[3],&g_713[2],&g_713[1]},{&g_713[0],&l_1587[4],&l_1588,&g_713[1]},{&g_713[2],(void*)0,&l_1587[1],(void*)0},{&g_713[2],&g_713[2],&l_1588,(void*)0},{&l_1588,&l_1588,(void*)0,&g_713[2]}},{{&g_713[3],&g_713[2],&l_1587[0],(void*)0},{(void*)0,&l_1587[4],&l_1587[4],&l_1587[4]},{&l_1587[1],&g_713[2],(void*)0,&l_1588},{&g_713[3],&l_1587[2],&g_713[2],(void*)0},{&l_1587[4],(void*)0,&l_1588,(void*)0},{&l_1587[1],&l_1588,&g_713[1],(void*)0}},{{&l_1587[2],&g_713[0],&l_1587[4],(void*)0},{&g_713[2],&l_1587[4],(void*)0,&l_1587[4]},{(void*)0,&l_1587[4],(void*)0,&g_713[0]},{&l_1587[1],&l_1587[4],(void*)0,&g_713[2]},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_713[1],&g_713[1],(void*)0,&g_713[2]}},{{&l_1588,&g_713[0],&l_1588,&l_1587[2]},{&l_1587[4],&l_1587[4],&g_713[2],&l_1588},{&g_713[1],&l_1587[4],&l_1587[4],&l_1587[2]},{&l_1587[4],&g_713[0],&l_1587[4],&g_713[2]},{(void*)0,&g_713[1],&l_1587[5],(void*)0},{&l_1588,(void*)0,&g_713[1],&g_713[2]}},{{&l_1587[4],&l_1587[4],&l_1588,&g_713[0]},{&l_1587[4],&l_1587[4],&g_713[3],&l_1588},{(void*)0,(void*)0,&l_1587[1],&l_1587[4]},{&l_1588,&l_1587[2],&l_1587[1],&g_713[1]},{&l_1587[1],&g_713[2],&l_1587[0],&g_713[2]},{&g_713[2],(void*)0,&g_713[2],&l_1587[4]}},{{&l_1587[0],&l_1588,&l_1587[4],&l_1587[4]},{&g_713[2],(void*)0,&l_1587[4],&l_1588},{&l_1587[0],&l_1588,&g_713[1],&l_1588},{&l_1588,&l_1587[4],&l_1587[4],&g_713[3]},{&l_1587[3],(void*)0,(void*)0,&l_1587[4]},{&g_713[2],&l_1588,&g_713[1],&g_713[2]}},{{&l_1587[4],&l_1588,&g_713[1],&g_713[3]},{(void*)0,&g_713[2],&l_1587[4],(void*)0},{(void*)0,&l_1587[4],&l_1588,(void*)0},{&g_713[3],&l_1588,&l_1587[4],&l_1587[4]},{(void*)0,&l_1587[4],&l_1587[0],&g_713[1]},{&g_713[2],&g_713[1],&l_1587[2],&g_713[2]}}};
        uint8_t *l_1766 = (void*)0;
        uint8_t *l_1767[5][5];
        int32_t *l_1778 = (void*)0;
        int32_t *l_1779 = &l_1725[2];
        int32_t *l_1782 = (void*)0;
        int32_t *l_1783[2];
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 5; j++)
                l_1767[i][j] = &g_646;
        }
        for (i = 0; i < 2; i++)
            l_1783[i] = (void*)0;
    }
    if ((p_20 , p_19))
    { /* block id: 701 */
        uint8_t l_1804 = 5UL;
        for (g_6 = 0; (g_6 <= 6); g_6 += 1)
        { /* block id: 704 */
            uint8_t *l_1790[4][5] = {{&g_646,&g_646,&g_646,&g_646,&g_646},{&g_646,&g_646,&g_646,&g_646,&g_646},{&g_646,(void*)0,&g_646,&g_646,&g_646},{&g_646,&g_646,&g_646,&g_646,&g_646}};
            int32_t l_1805 = (-1L);
            uint64_t l_1806 = 5UL;
            int32_t *l_1807 = &l_1569;
            int i, j;
            (*l_1807) = (((safe_div_func_int8_t_s_s((safe_sub_func_int8_t_s_s((*g_263), (p_19 = (safe_lshift_func_uint8_t_u_s(g_598, 4))))), (0x2BL || (!l_1565)))) < (*g_1112)) && (safe_rshift_func_int8_t_s_u((((((((safe_mod_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(((p_19 < 0x34050166L) != (safe_mul_func_uint8_t_u_u((p_18 , (safe_rshift_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(p_19, p_20)), 6))), l_1804))), p_20)), l_1805)) != 1UL) != (*g_1099)) | l_1806) | 0x5AD55BF4L) , 0x228FB3E9L) , 0L), 0)));
        }
        l_1808 = &p_20;
    }
    else
    { /* block id: 709 */
        int16_t l_1809 = 0x8099L;
        int32_t *l_1810 = &l_1571[3];
        int32_t *l_1811 = &l_1571[4];
        int32_t *l_1812 = &l_1565;
        int32_t *l_1813 = &l_1571[1];
        int32_t *l_1814 = (void*)0;
        int32_t *l_1815[3];
        int32_t l_1816 = (-1L);
        int i;
        for (i = 0; i < 3; i++)
            l_1815[i] = &l_1569;
        --l_1817;
    }
    for (l_1817 = 0; (l_1817 == 12); l_1817++)
    { /* block id: 714 */
        int32_t *****l_1824 = (void*)0;
        int32_t ****l_1826[6][7][6] = {{{(void*)0,&g_667,(void*)0,&g_669,&g_669,&g_667},{(void*)0,&g_667,&g_669,&g_669,&g_669,&g_667},{(void*)0,&g_669,&g_669,&g_669,&g_669,(void*)0},{(void*)0,&g_669,(void*)0,&g_669,&g_669,&g_669},{(void*)0,&g_669,(void*)0,&g_669,&g_667,(void*)0},{(void*)0,&g_667,&g_667,&g_669,&g_667,&g_669},{(void*)0,&g_669,&g_667,&g_669,&g_667,&g_669}},{{(void*)0,&g_669,(void*)0,&g_669,&g_669,(void*)0},{(void*)0,&g_667,&g_669,&g_669,&g_667,(void*)0},{(void*)0,&g_667,(void*)0,&g_669,&g_669,&g_667},{(void*)0,(void*)0,&g_667,&g_669,&g_667,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667},{&g_667,&g_669,(void*)0,&g_667,(void*)0,&g_667},{&g_669,(void*)0,&g_669,&g_669,(void*)0,&g_667}},{{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667},{&g_667,(void*)0,&g_667,&g_667,(void*)0,&g_669},{&g_669,&g_667,&g_667,&g_669,(void*)0,(void*)0},{&g_669,&g_667,&g_667,&g_667,&g_667,&g_669},{&g_667,(void*)0,&g_667,&g_667,&g_669,&g_669},{&g_669,(void*)0,&g_667,&g_669,&g_667,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667}},{{&g_667,&g_669,(void*)0,&g_667,(void*)0,&g_667},{&g_669,(void*)0,&g_669,&g_669,(void*)0,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667},{&g_667,(void*)0,&g_667,&g_667,(void*)0,&g_669},{&g_669,&g_667,&g_667,&g_669,(void*)0,(void*)0},{&g_669,&g_667,&g_667,&g_667,&g_667,&g_669},{&g_667,(void*)0,&g_667,&g_667,&g_669,&g_669}},{{&g_669,(void*)0,&g_667,&g_669,&g_667,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667},{&g_667,&g_669,(void*)0,&g_667,(void*)0,&g_667},{&g_669,(void*)0,&g_669,&g_669,(void*)0,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667},{&g_667,(void*)0,&g_667,&g_667,(void*)0,&g_669},{&g_669,&g_667,&g_667,&g_669,(void*)0,(void*)0}},{{&g_669,&g_667,&g_667,&g_667,&g_667,&g_669},{&g_667,(void*)0,&g_667,&g_667,&g_669,&g_669},{&g_669,(void*)0,&g_667,&g_669,&g_667,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667},{&g_667,&g_669,(void*)0,&g_667,(void*)0,&g_667},{&g_669,(void*)0,&g_669,&g_669,(void*)0,&g_667},{&g_669,&g_669,&g_669,&g_667,&g_669,&g_667}}};
        int32_t *****l_1825 = &l_1826[4][1][1];
        int32_t ****l_1827 = (void*)0;
        int64_t l_1874 = 4L;
        int32_t l_1876 = (-9L);
        int16_t l_1877 = (-8L);
        uint8_t *l_1968 = &g_646;
        int64_t l_2008[4][8][8] = {{{(-1L),0xAEDEE25096C6FE63LL,(-1L),0xCAF49107512484DALL,(-1L),1L,(-2L),1L},{0xDA013999D3819087LL,(-10L),0x5B3B9C48B76258BALL,(-8L),0x39E1A2263929AFECLL,4L,0L,(-1L)},{0xD61AE6CA900FE893LL,0x070EBD9503642027LL,0xE5941986A9B86FFALL,0x55D227EE7672C19DLL,0x3DF845B019ACC03ELL,0xF7794426EC2F027CLL,(-1L),0x070EBD9503642027LL},{(-1L),0xD61AE6CA900FE893LL,(-1L),(-1L),0x62CDD30DC646347ELL,1L,0x9E91593D3E56773ELL,0x5C474838F8473182LL},{(-1L),0x5E379C5A7B4FA158LL,(-1L),0x3DF845B019ACC03ELL,(-1L),0xC3DFA595A85423C1LL,0x75FFB6D1170106B0LL,1L},{(-1L),0x3DF845B019ACC03ELL,(-10L),(-7L),(-3L),(-5L),(-1L),0L},{4L,0x75FFB6D1170106B0LL,1L,0x39E1A2263929AFECLL,0x5C474838F8473182LL,0x4883A07420DEE21BLL,(-1L),0L},{0xFED5E11F597F79F5LL,(-1L),0x5B3B9C48B76258BALL,0xD61AE6CA900FE893LL,(-1L),0x942291522D0C052ALL,1L,0xCAF49107512484DALL}},{{9L,0x6DDACA1B0B9837F0LL,0L,(-3L),(-1L),0x571465EFC87DFD15LL,0L,(-1L)},{(-1L),0L,0x961F9C3E5F62A797LL,4L,0xDA013999D3819087LL,0xCA1691F69C2A56A7LL,0xED3FA88F936D6041LL,(-1L)},{1L,0x89EBE5C5FE19E61FLL,(-10L),0xD61AE6CA900FE893LL,0xFC2E89DF86A6F9EFLL,0x39E1A2263929AFECLL,(-3L),(-1L)},{1L,9L,0xF7794426EC2F027CLL,0L,0L,0x530D646BEEDF1973LL,0L,0L},{(-1L),(-1L),(-1L),1L,9L,(-1L),(-1L),0x659C021C2B8E166FLL},{1L,9L,4L,0x3DF845B019ACC03ELL,0x5E379C5A7B4FA158LL,(-2L),9L,0xCA1691F69C2A56A7LL},{1L,0L,(-1L),1L,9L,0x65221D23D8DF115ALL,(-1L),0x070EBD9503642027LL},{(-1L),4L,(-6L),1L,0L,(-1L),9L,0x6F08C4554F6FE8E1LL}},{{1L,(-6L),0x89EBE5C5FE19E61FLL,(-8L),0xFC2E89DF86A6F9EFLL,0x89EBE5C5FE19E61FLL,0x859390D4DD0B67DELL,9L},{1L,(-3L),1L,(-1L),0xDA013999D3819087LL,0x3BAA376D4718C706LL,0xBD42E03E5B345530LL,0x7C7A1D85AB170662LL},{(-1L),0x62CDD30DC646347ELL,(-4L),9L,(-1L),9L,(-1L),0xFED5E11F597F79F5LL},{9L,4L,(-9L),0x62CDD30DC646347ELL,(-1L),0x89EBE5C5FE19E61FLL,0x659C021C2B8E166FLL,(-2L)},{0xFED5E11F597F79F5LL,0x859390D4DD0B67DELL,0x87F81E8D55BE7291LL,0x24CD2A47E4BA312FLL,0x5C474838F8473182LL,0x6BD14FECF0D0D283LL,4L,1L},{4L,4L,0x942291522D0C052ALL,0x4883A07420DEE21BLL,(-3L),0x56ADFB4404694B78LL,1L,1L},{(-1L),(-1L),0xC3DFA595A85423C1LL,(-1L),(-1L),(-2L),0x7C7A1D85AB170662LL,0x4883A07420DEE21BLL},{(-1L),1L,0xDC4FCF803F4E41BFLL,0x6DDACA1B0B9837F0LL,0x62CDD30DC646347ELL,(-6L),0x5B3B9C48B76258BALL,1L}},{{1L,0x9E91593D3E56773ELL,0xCDE78CD9E7C6FAA3LL,1L,(-1L),(-5L),0xF7794426EC2F027CLL,0L},{0x56ADFB4404694B78LL,9L,0x3BAA376D4718C706LL,0x5E379C5A7B4FA158LL,0x571465EFC87DFD15LL,0x571465EFC87DFD15LL,0x5E379C5A7B4FA158LL,0x3BAA376D4718C706LL},{0x942291522D0C052ALL,0x942291522D0C052ALL,1L,(-2L),0L,(-8L),(-1L),0xCA1691F69C2A56A7LL},{(-3L),(-6L),1L,(-1L),0x961F9C3E5F62A797LL,8L,(-5L),0xCA1691F69C2A56A7LL},{(-6L),0x1F9B86921A87ACB2LL,0L,(-2L),(-1L),0xCDE78CD9E7C6FAA3LL,4L,0x3BAA376D4718C706LL},{(-9L),0x39E1A2263929AFECLL,0xFED5E11F597F79F5LL,0x5E379C5A7B4FA158LL,(-4L),0x55D227EE7672C19DLL,1L,0L},{8L,(-10L),8L,1L,4L,0x530D646BEEDF1973LL,8L,1L},{0xED3FA88F936D6041LL,(-6L),1L,0xCA1691F69C2A56A7LL,(-1L),1L,0x4883A07420DEE21BLL,0xF7794426EC2F027CLL}}};
        uint64_t *l_2037 = (void*)0;
        uint64_t **l_2036 = &l_2037;
        int8_t * const *l_2058 = &l_54[0];
        int8_t * const ** const l_2057[9][8] = {{(void*)0,(void*)0,&l_2058,(void*)0,(void*)0,&l_2058,(void*)0,(void*)0},{&l_2058,(void*)0,&l_2058,&l_2058,(void*)0,&l_2058,&l_2058,&l_2058},{(void*)0,&l_2058,&l_2058,&l_2058,&l_2058,&l_2058,(void*)0,&l_2058},{(void*)0,(void*)0,&l_2058,(void*)0,(void*)0,&l_2058,(void*)0,(void*)0},{&l_2058,(void*)0,&l_2058,&l_2058,(void*)0,&l_2058,&l_2058,&l_2058},{(void*)0,&l_2058,&l_2058,(void*)0,&l_2058,&l_2058,(void*)0,&l_2058},{(void*)0,(void*)0,&l_2058,(void*)0,(void*)0,&l_2058,(void*)0,&l_2058},{&l_2058,(void*)0,&l_2058,&l_2058,(void*)0,&l_2058,&l_2058,(void*)0},{(void*)0,&l_2058,&l_2058,(void*)0,&l_2058,&l_2058,(void*)0,&l_2058}};
        int8_t * const ** const *l_2056 = &l_2057[3][0];
        int8_t * const ** const **l_2055[4] = {&l_2056,&l_2056,&l_2056,&l_2056};
        uint16_t **l_2095 = &l_1599;
        uint16_t *l_2131 = &g_28;
        uint32_t l_2164 = 3UL;
        uint64_t l_2210 = 0xF6392D8A89BE172ALL;
        uint16_t l_2228 = 65527UL;
        uint64_t l_2258 = 18446744073709551609UL;
        uint32_t **l_2259 = &g_790;
        uint8_t *l_2268[6][8] = {{&l_1883,&l_1883,&l_1981,&g_1970[3],&l_1981,&l_1883,&l_1883,&l_1981},{(void*)0,&l_1981,&l_1981,(void*)0,&l_1817,(void*)0,&l_1981,&l_1981},{&l_1981,&l_1817,&g_1970[3],&g_1970[3],&l_1817,&l_1981,&l_1817,&g_1970[3]},{(void*)0,&l_1817,(void*)0,&l_1981,&l_1981,(void*)0,&l_1817,(void*)0},{&l_1883,&l_1981,&g_1970[3],&l_1981,&l_1883,&l_1883,&l_1981,&g_1970[3]},{&l_1883,&l_1883,&l_1981,&g_1970[3],&l_1981,&l_1883,&l_1883,&l_1981}};
        int8_t l_2288 = 6L;
        uint16_t l_2291 = 65527UL;
        int32_t l_2315 = 0L;
        int i, j, k;
    }
    return (*g_790);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_40(int64_t  p_41, uint8_t  p_42, uint16_t * p_43)
{ /* block id: 626 */
    int32_t *l_1560 = &g_741[3];
    return l_1560;
}


/* ------------------------------------------ */
/* 
 * reads : g_129 g_741 g_209 g_346 g_607 g_24 g_28 g_775 g_176 g_26 g_646 g_790 g_268 g_262 g_263 g_125 g_712 g_713 g_342 g_779 g_130 g_652 g_957 g_996 g_106 g_598 g_98 g_6 g_1099 g_1111 g_1112 g_1149
 * writes: g_741 g_209 g_268 g_176 g_106 g_779 g_790 g_24 g_346 g_125 g_28 g_26 g_646 g_5 g_652 g_6 g_263 g_130 g_1098 g_1115 g_1149 g_996
 */
static const int16_t  func_46(int8_t  p_47, int8_t * p_48, uint32_t  p_49)
{ /* block id: 327 */
    int32_t *l_747 = &g_741[6];
    uint16_t *l_754 = &g_209;
    uint64_t *l_763[8] = {&g_268[1],&g_268[1],&g_268[1],&g_268[1],&g_268[1],&g_268[1],&g_268[1],&g_268[1]};
    int32_t l_764 = 0x901451F3L;
    int32_t ****l_771 = &g_667;
    int64_t *l_772[8] = {&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106,&g_106};
    int8_t l_773[9] = {3L,3L,3L,3L,3L,3L,3L,3L,3L};
    uint64_t l_774 = 3UL;
    int32_t *l_776 = &g_176;
    const int64_t l_777[1] = {(-1L)};
    uint8_t l_778 = 0xF0L;
    int64_t l_780 = 6L;
    int32_t l_781 = 0L;
    uint32_t *l_789[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    uint32_t **l_788[2][9][6] = {{{(void*)0,&l_789[2],&l_789[2],&l_789[3],&l_789[2],&l_789[3]},{&l_789[2],(void*)0,&l_789[2],&l_789[2],(void*)0,&l_789[2]},{&l_789[2],&l_789[2],&l_789[3],&l_789[2],&l_789[2],&l_789[2]},{&l_789[2],&l_789[2],&l_789[2],&l_789[2],&l_789[3],&l_789[2]},{&l_789[2],&l_789[2],(void*)0,&l_789[2],&l_789[2],(void*)0},{&l_789[2],&l_789[3],&l_789[2],&l_789[3],&l_789[2],&l_789[2]},{(void*)0,&l_789[2],&l_789[2],&l_789[2],&l_789[3],(void*)0},{(void*)0,&l_789[2],&l_789[2],&l_789[2],&l_789[2],(void*)0},{&l_789[2],&l_789[2],&l_789[2],&l_789[2],(void*)0,&l_789[2]}},{{&l_789[2],(void*)0,&l_789[2],(void*)0,&l_789[2],(void*)0},{&l_789[2],&l_789[2],(void*)0,&l_789[2],&l_789[2],&l_789[2]},{&l_789[2],(void*)0,&l_789[2],&l_789[2],&l_789[2],&l_789[2]},{(void*)0,(void*)0,&l_789[3],&l_789[2],&l_789[2],&l_789[2]},{(void*)0,&l_789[2],&l_789[2],&l_789[3],&l_789[2],&l_789[3]},{&l_789[2],(void*)0,&l_789[2],&l_789[2],(void*)0,&l_789[2]},{&l_789[2],&l_789[2],&l_789[3],&l_789[2],&l_789[2],&l_789[2]},{&l_789[2],&l_789[2],&l_789[2],&l_789[2],&l_789[3],&l_789[2]},{&l_789[2],&l_789[2],(void*)0,&l_789[2],&l_789[2],(void*)0}}};
    const int64_t l_835 = 0x7A8B849058022D4CLL;
    int32_t l_843 = 5L;
    int64_t l_865 = 0x671CFD15A5C33C93LL;
    int16_t l_868[4][6][1] = {{{0L},{(-6L)},{(-1L)},{(-1L)},{(-6L)},{0L}},{{0x9860L},{0L},{(-6L)},{(-1L)},{(-1L)},{(-6L)}},{{0L},{0x9860L},{0L},{(-6L)},{(-1L)},{(-1L)}},{{(-6L)},{0L},{0x9860L},{0L},{(-6L)},{(-1L)}}};
    int8_t ****l_895 = &g_713[2];
    int32_t *l_918 = &g_176;
    int32_t l_924 = (-8L);
    uint16_t l_969 = 0x4235L;
    uint64_t l_978 = 0x3F05116B7591422BLL;
    int64_t ***l_1008[8][2][6] = {{{(void*)0,&g_181,&g_181,(void*)0,(void*)0,&g_181},{&g_181,&g_181,&g_181,(void*)0,&g_181,&g_181}},{{(void*)0,&g_181,(void*)0,&g_181,(void*)0,&g_181},{(void*)0,(void*)0,(void*)0,&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181,&g_181,&g_181,(void*)0},{&g_181,&g_181,(void*)0,&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181,&g_181,&g_181,&g_181},{&g_181,&g_181,&g_181,&g_181,(void*)0,(void*)0}},{{&g_181,&g_181,&g_181,&g_181,&g_181,(void*)0},{(void*)0,(void*)0,&g_181,&g_181,&g_181,&g_181}},{{&g_181,&g_181,&g_181,(void*)0,&g_181,&g_181},{&g_181,(void*)0,(void*)0,&g_181,&g_181,(void*)0}},{{&g_181,&g_181,&g_181,&g_181,(void*)0,(void*)0},{&g_181,&g_181,&g_181,(void*)0,&g_181,&g_181}},{{&g_181,&g_181,&g_181,&g_181,&g_181,(void*)0},{(void*)0,&g_181,&g_181,&g_181,&g_181,(void*)0}}};
    uint32_t l_1022 = 0xA00D1511L;
    int32_t l_1074 = (-1L);
    int32_t l_1076 = (-1L);
    int32_t l_1077 = 0xAEC9498DL;
    int32_t l_1078 = 0xD28D842AL;
    int32_t *l_1102 = &l_781;
    const uint32_t *l_1136 = (void*)0;
    const uint32_t **l_1135 = &l_1136;
    uint8_t l_1142[9][5] = {{249UL,1UL,0xB0L,249UL,0UL},{0xD4L,249UL,0xD3L,249UL,0xD4L},{0xB0L,1UL,1UL,0UL,1UL},{0xD4L,1UL,1UL,0xD4L,0UL},{249UL,0xD4L,0xD3L,1UL,1UL},{0xB0L,0xD4L,0xB0L,0UL,0xD4L},{1UL,1UL,0UL,1UL,0UL},{1UL,1UL,0xD3L,0xD4L,249UL},{0xB0L,249UL,0UL,0UL,249UL}};
    int32_t *l_1144 = &l_764;
    int32_t l_1145 = 0x37A9FDE2L;
    uint32_t l_1146 = 0x2CDC923FL;
    uint8_t l_1196 = 0x2AL;
    int32_t *l_1225 = &l_764;
    int8_t l_1418 = 0x1DL;
    int i, j, k;
    l_781 &= ((safe_sub_func_uint8_t_u_u(((g_779 = (l_778 &= (g_106 = ((safe_mod_func_uint64_t_u_u((g_129 & ((((*l_747) &= 0xB5D93203L) >= ((*l_776) |= (((safe_mul_func_uint8_t_u_u((safe_add_func_int32_t_s_s(((((g_268[2] = (((safe_rshift_func_uint16_t_u_s((--(*l_754)), ((safe_sub_func_uint64_t_u_u((p_49 <= g_346[3]), (safe_add_func_int32_t_s_s(((0xBFFAL & ((safe_mod_func_uint64_t_u_u((((((l_764 = ((void*)0 == &g_98[0][6][0])) , ((safe_div_func_int64_t_s_s((g_607 | (((safe_sub_func_int64_t_s_s((l_764 |= (safe_rshift_func_int16_t_s_s((l_771 != (void*)0), p_49))), l_773[2])) , l_773[2]) < p_47)), (-1L))) & (*p_48))) != g_28) || p_49) , 18446744073709551615UL), (-3L))) == 0L)) && p_49), p_49)))) > 0x8D9CL))) | l_774) ^ p_49)) < 0x03CD85658D76CE1FLL) || l_764) >= 0L), p_49)), g_775)) && 1UL) , (-10L)))) < 0x64CA19E7L)), l_777[0])) <= g_26)))) && 0L), 0x1AL)) ^ l_780);
    if ((safe_div_func_int64_t_s_s((((p_49 , ((safe_mul_func_int8_t_s_s((*l_747), (*l_747))) == ((((safe_rshift_func_uint16_t_u_s(g_646, ((g_790 = l_747) == &g_98[0][0][4]))) && 255UL) >= 0L) < (safe_lshift_func_uint8_t_u_s(g_28, (*p_48)))))) | (*l_776)) | (*p_48)), p_49)))
    { /* block id: 339 */
        int32_t ***l_805[10][5][2] = {{{&g_668,&g_668},{(void*)0,&g_668},{&g_668,&g_668},{(void*)0,&g_668},{&g_668,&g_668}},{{&g_668,&g_668},{&g_668,&g_668},{&g_668,&g_668},{&g_668,(void*)0},{&g_668,(void*)0}},{{&g_668,&g_668},{&g_668,&g_668},{(void*)0,&g_668},{(void*)0,&g_668},{&g_668,&g_668}},{{&g_668,(void*)0},{&g_668,(void*)0},{&g_668,&g_668},{&g_668,&g_668},{&g_668,&g_668}},{{&g_668,&g_668},{&g_668,&g_668},{(void*)0,&g_668},{&g_668,&g_668},{(void*)0,&g_668}},{{&g_668,&g_668},{(void*)0,&g_668},{&g_668,&g_668},{(void*)0,&g_668},{&g_668,&g_668}},{{&g_668,&g_668},{&g_668,&g_668},{&g_668,&g_668},{&g_668,(void*)0},{&g_668,(void*)0}},{{&g_668,&g_668},{&g_668,&g_668},{(void*)0,&g_668},{(void*)0,&g_668},{&g_668,&g_668}},{{(void*)0,(void*)0},{&g_668,&g_668},{(void*)0,&g_668},{&g_668,&g_668},{&g_668,&g_668}},{{(void*)0,&g_668},{(void*)0,&g_668},{&g_668,&g_668},{&g_668,&g_668},{&g_668,(void*)0}}};
        int32_t *l_839[4];
        uint16_t *l_873 = &g_28;
        int8_t l_883 = 0x62L;
        uint32_t l_908 = 0x19AC0266L;
        uint32_t **l_965 = &g_790;
        int32_t l_1073[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
        uint32_t l_1083[3][5] = {{1UL,18446744073709551615UL,18446744073709551615UL,1UL,18446744073709551609UL},{1UL,0x34779C5BL,0x9FF388FFL,0x9FF388FFL,0x34779C5BL},{18446744073709551609UL,18446744073709551615UL,0x9FF388FFL,0x5594AF9EL,0x5594AF9EL}};
        uint16_t l_1113[5][1][7] = {{{65529UL,5UL,5UL,65529UL,5UL,5UL,65529UL}},{{0x989CL,0xA874L,0x989CL,0x989CL,0xA874L,0x989CL,0x989CL}},{{65529UL,65529UL,0xBA7AL,65529UL,65529UL,0xBA7AL,65529UL}},{{0xA874L,0x989CL,0x989CL,0xA874L,0x989CL,0x989CL,0xA874L}},{{5UL,65529UL,5UL,5UL,65529UL,5UL,5UL}}};
        int64_t *l_1218 = &g_106;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_839[i] = (void*)0;
lbl_840:
        (*l_776) = (+p_47);
        if ((safe_add_func_uint16_t_u_u(((((((p_47 , (0xFA9CBBBDL && (((safe_rshift_func_int16_t_s_u((safe_mul_func_int8_t_s_s((~((((p_49 <= (safe_div_func_uint8_t_u_u((*l_747), ((*p_48) = ((p_47 , (safe_sub_func_uint8_t_u_u(((*l_747) > ((l_805[4][0][0] != (void*)0) , ((safe_add_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s((~((p_49 >= g_26) , 0x3584897FE5F8557CLL)), g_346[3])) >= p_49), g_741[6])) || g_28))), 1UL))) || 0x1EB9L))))) , 0x44EEC4AB33098FDBLL) , p_49) != p_49)), p_47)), 2)) | 9L) == p_49))) <= 9L) >= 18446744073709551615UL) , p_49) , (*g_790)) , p_47), g_268[0])))
        { /* block id: 342 */
            const int16_t *l_831 = (void*)0;
            int16_t *l_832 = &g_130[1][2][0];
            int16_t **l_833 = &l_832;
            int16_t *l_834 = &g_346[1];
            int32_t l_836[3][1][6] = {{{0L,(-1L),0xBDFD90A3L,(-1L),0L,0xBDFD90A3L}},{{(-1L),0L,0xBDFD90A3L,(-1L),(-1L),0xBDFD90A3L}},{{(-1L),(-1L),0xBDFD90A3L,0L,(-1L),0xBDFD90A3L}}};
            int32_t l_837[2][7] = {{(-2L),(-1L),0x8A0123ABL,(-1L),(-2L),(-2L),(-1L)},{0L,0x60C432E4L,0L,(-1L),(-1L),0L,0x60C432E4L}};
            int32_t l_838 = (-1L);
            uint32_t * const *l_907 = (void*)0;
            int32_t *l_971 = &g_6;
            int32_t **** const l_999 = &g_669;
            int32_t * const l_1001 = &g_741[6];
            int32_t **l_1002 = &l_747;
            int i, j, k;
            if (((~((-1L) != (65535UL >= (g_28 = (safe_lshift_func_int8_t_s_u(((****g_712) = ((safe_sub_func_uint32_t_u_u((safe_div_func_uint16_t_u_u((safe_add_func_uint32_t_u_u(((g_28 , &g_668) != &g_668), (safe_mod_func_uint32_t_u_u(((safe_rshift_func_int8_t_s_s((((((l_836[2][0][0] = (safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s((~p_47), ((safe_rshift_func_int8_t_s_s((((*p_48) , ((((l_837[0][3] |= ((((*g_790) = (((*l_834) = (((l_831 != ((*l_833) = l_832)) | p_47) , 0xDBCBL)) || l_835)) < l_836[2][0][0]) <= g_268[1])) , l_836[1][0][4]) | 0xD5L) , &p_48)) == (void*)0), (*p_48))) >= p_47))), 65535UL))) < (**g_262)) > 0L) , 0x8749D42EL) & p_47), (*l_776))) < (-1L)), p_47)))), g_775)), l_838)) & p_47)), 6)))))) != g_342[4][2]))
            { /* block id: 350 */
                l_839[2] = (void*)0;
                if (g_176)
                    goto lbl_840;
            }
            else
            { /* block id: 353 */
                int16_t l_841 = 0x5C7AL;
                int8_t l_842[10][6][4] = {{{0L,0x07L,8L,(-4L)},{0x45L,0x96L,3L,(-4L)},{0L,2L,(-1L),0x42L},{0x79L,0xDFL,0L,0x6FL},{0x8EL,1L,0xFDL,0x88L},{0x4EL,0x45L,0L,0xCFL}},{{0x26L,(-4L),1L,(-5L)},{0xA1L,0xABL,0xABL,0xA1L},{(-5L),(-9L),0xD7L,0x45L},{1L,0x9CL,0x96L,(-3L)},{(-5L),0x42L,0x14L,(-3L)},{(-3L),0x9CL,0xC7L,0x45L}},{{0x51L,(-9L),0x4EL,0xA1L},{2L,0xABL,0x94L,(-5L)},{(-1L),(-4L),0L,0xCFL},{0x14L,0x45L,0xA1L,0x88L},{0x94L,1L,0x9CL,0x6FL},{2L,0xDFL,4L,0x42L}},{{0x4DL,2L,0xEAL,(-4L)},{(-4L),0x96L,0x78L,0x07L},{0x51L,0x78L,4L,0x14L},{5L,0xA1L,1L,(-5L)},{0x45L,0x94L,1L,0x78L},{0L,0xB2L,0x51L,(-4L)}},{{0x26L,0x45L,0xD6L,(-8L)},{0x6FL,4L,(-5L),(-8L)},{0x9AL,0x51L,1L,(-4L)},{0xDFL,0x4DL,0xDFL,0x42L},{0xD7L,0xFDL,(-4L),1L},{0x14L,(-4L),0xCFL,0xFDL}},{{0xB2L,0x79L,0xCFL,(-5L)},{0x14L,0xC7L,(-4L),0xB2L},{0xD7L,0x26L,0xDFL,5L},{0xDFL,5L,1L,0xABL},{0x9AL,0L,(-5L),8L},{0x6FL,0L,0xD6L,(-1L)}},{{0x26L,0x52L,0x51L,0x9CL},{0L,0L,1L,1L},{0x45L,0x06L,1L,(-4L)},{5L,2L,4L,3L},{0x51L,0x8EL,0xFDL,0x79L},{0xC7L,0x42L,(-1L),4L}},{{2L,(-5L),0x14L,0x4DL},{6L,0xDFL,0L,2L},{3L,(-1L),(-4L),0x0AL},{(-1L),0x95L,0x4EL,0x51L},{0L,(-3L),3L,3L},{(-8L),(-8L),0xC7L,0xD6L}},{{0x10L,0xABL,0xBDL,0xE4L},{0x51L,0x30L,(-1L),0xBDL},{(-4L),0x30L,0x1AL,0xE4L},{0x30L,0xABL,0x52L,0xD6L},{(-7L),(-8L),0x13L,3L},{(-4L),(-3L),5L,0x51L}},{{(-5L),0x95L,3L,0x0AL},{0xC7L,(-1L),0x51L,2L},{0L,0xDFL,(-4L),0x4DL},{(-5L),(-5L),0xABL,4L},{(-4L),0x42L,0x6FL,0x79L},{0x94L,0x8EL,0xD7L,3L}}};
                uint8_t l_844 = 255UL;
                uint32_t l_866[5][6][8] = {{{1UL,0xBA1C14AFL,18446744073709551615UL,0UL,0x22699571L,0UL,7UL,6UL},{18446744073709551609UL,18446744073709551610UL,0x22699571L,18446744073709551615UL,18446744073709551615UL,0UL,18446744073709551615UL,18446744073709551615UL},{0xF1A7C781L,0xBA1C14AFL,0xF1A7C781L,0UL,18446744073709551614UL,6UL,18446744073709551615UL,18446744073709551615UL},{0x0244243DL,0UL,18446744073709551615UL,0x3324AABFL,18446744073709551609UL,0x67105625L,18446744073709551615UL,18446744073709551610UL},{18446744073709551615UL,18446744073709551610UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL,0x02D3CD2EL,0xF829A8E1L},{1UL,0x3324AABFL,0x8D8B4286L,6UL,0x57D18731L,0xBA1C14AFL,18446744073709551615UL,0x67105625L}},{{4UL,0xF829A8E1L,0UL,6UL,18446744073709551609UL,6UL,0UL,0xF829A8E1L},{18446744073709551615UL,0x42D62680L,0x22699571L,0UL,1UL,0UL,4UL,18446744073709551610UL},{18446744073709551615UL,18446744073709551615UL,0x7558ED0DL,0x33B74FCAL,18446744073709551615UL,18446744073709551615UL,4UL,0x56C5A756L},{0xF1A7C781L,0x33B74FCAL,0x22699571L,0UL,0x7558ED0DL,0x67105625L,0UL,0UL},{0x7558ED0DL,0x67105625L,0UL,0UL,1UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL},{0x02D3CD2EL,0x67105625L,0x8D8B4286L,0xBEBC8BF2L,0x8D8B4286L,0x67105625L,0x02D3CD2EL,0UL}},{{18446744073709551609UL,0x33B74FCAL,18446744073709551615UL,0x67105625L,0xF1A7C781L,18446744073709551615UL,18446744073709551615UL,0x3324AABFL},{7UL,18446744073709551615UL,0x57D18731L,18446744073709551615UL,0xF1A7C781L,0UL,1UL,0xBEBC8BF2L},{18446744073709551609UL,0x42D62680L,1UL,0x3324AABFL,0x8D8B4286L,6UL,0x57D18731L,0xBA1C14AFL},{0x02D3CD2EL,0xF829A8E1L,18446744073709551609UL,0xF3F65A1FL,1UL,0xBA1C14AFL,0x22699571L,0xBA1C14AFL},{0x7558ED0DL,0x3324AABFL,0xF1A7C781L,0x3324AABFL,0x7558ED0DL,0UL,1UL,0xBEBC8BF2L},{0xF1A7C781L,18446744073709551610UL,0x0244243DL,18446744073709551615UL,18446744073709551615UL,0xF3F65A1FL,18446744073709551615UL,0x3324AABFL}},{{18446744073709551615UL,18446744073709551615UL,0x0244243DL,0x67105625L,1UL,18446744073709551615UL,1UL,0UL},{18446744073709551615UL,6UL,0xF1A7C781L,0xBEBC8BF2L,18446744073709551609UL,18446744073709551610UL,0x22699571L,18446744073709551615UL},{4UL,18446744073709551615UL,18446744073709551609UL,0UL,0x57D18731L,18446744073709551610UL,0x57D18731L,0UL},{1UL,6UL,1UL,0UL,18446744073709551615UL,18446744073709551615UL,1UL,0x56C5A756L},{18446744073709551615UL,18446744073709551615UL,0x57D18731L,0x33B74FCAL,4UL,0xF3F65A1FL,18446744073709551615UL,18446744073709551610UL},{18446744073709551615UL,18446744073709551610UL,18446744073709551615UL,0UL,18446744073709551615UL,0UL,0x02D3CD2EL,0xF829A8E1L}},{{1UL,0x3324AABFL,0x8D8B4286L,6UL,0x57D18731L,0xBA1C14AFL,18446744073709551615UL,0x67105625L},{4UL,0xF829A8E1L,0UL,6UL,18446744073709551609UL,6UL,0UL,0xF829A8E1L},{18446744073709551615UL,0x42D62680L,0x22699571L,0UL,1UL,0UL,4UL,18446744073709551610UL},{18446744073709551615UL,18446744073709551615UL,0x7558ED0DL,0x33B74FCAL,18446744073709551615UL,0UL,18446744073709551614UL,0x67105625L},{1UL,0x42D62680L,18446744073709551609UL,18446744073709551615UL,7UL,0xF3F65A1FL,0x02D3CD2EL,0xBEBC8BF2L},{7UL,0xF3F65A1FL,0x02D3CD2EL,0xBEBC8BF2L,0UL,0x56C5A756L,1UL,0UL}}};
                int32_t l_867 = (-1L);
                uint32_t *l_869 = &g_26;
                int32_t l_870 = 8L;
                int i, j, k;
                ++l_844;
                l_870 &= ((safe_mod_func_uint16_t_u_u((safe_div_func_int16_t_s_s(((safe_unary_minus_func_int64_t_s(((((p_47 & ((safe_lshift_func_uint8_t_u_u((((*l_869) = ((p_47 , ((((0xC94063ACL != (p_47 | (((safe_mul_func_uint8_t_u_u(((((p_49 > (*l_776)) > (((p_49 < (((l_867 = ((l_838 |= (0x34L == ((safe_div_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_s(((((safe_sub_func_uint64_t_u_u((l_837[1][4] = ((((!p_47) > l_865) ^ 4L) != l_866[1][0][1])), 0x048E385B5396AF91LL)) < 0xA9D8L) | p_47) > p_47), (*l_747))), p_49)) == g_646))) | l_867)) && l_868[2][0][0]) == (*g_790))) , (void*)0) != &p_49)) != p_49) , l_836[0][0][1]), 3L)) >= 4294967292UL) ^ p_49))) , (void*)0) != (void*)0) > (*g_263))) , p_49)) , 0xD2L), 0)) == g_342[4][2])) , p_47) | l_836[2][0][0]) <= 0L))) , l_837[0][5]), g_268[1])), 0x6723L)) != 0x1BCEL);
            }
            if ((safe_rshift_func_uint8_t_u_u(((p_47 | ((g_24 , l_873) != &g_2)) != ((*g_790) , (safe_unary_minus_func_uint32_t_u((safe_add_func_uint64_t_u_u((~(safe_unary_minus_func_int32_t_s(p_47))), (l_838 = 5L))))))), (((*l_776) || 0UL) & 0x7DL))))
            { /* block id: 362 */
                int8_t l_884 = 0xA7L;
                int32_t *l_891 = &l_836[2][0][0];
                int8_t l_896 = 0xF3L;
                uint8_t *l_909 = &g_646;
                l_896 = ((*l_776) = ((safe_mul_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s(((g_28 &= ((*p_48) <= ((****g_712) = 0x22L))) , ((((*l_776) || l_837[0][3]) != (g_268[1]++)) <= ((((((safe_mul_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((((l_891 == &l_781) ^ ((((safe_mod_func_uint64_t_u_u(0xF6C12179E59361B9LL, ((+((*g_790) = (&g_713[2] != l_895))) | 0x26CCF6599B0C82FCLL))) & g_346[2]) || 0x8CL) , p_47)) || p_49), g_646)), g_26)) | g_779) < g_28) || p_47) < p_49) > p_49))), (*l_891))), 0x24L)) , 0L));
                (*l_776) |= ((((*l_909) ^= ((safe_add_func_uint64_t_u_u((safe_add_func_uint64_t_u_u((((safe_mul_func_uint8_t_u_u(((((*l_891) = ((p_49 , (l_838 > (l_837[0][0] |= (p_47 ^ (0x5EE775FDL >= ((safe_lshift_func_uint8_t_u_u((((p_47 <= ((void*)0 == &p_47)) , (void*)0) != l_907), 5)) , (*g_790))))))) > l_836[2][0][0])) > g_268[0]) == 0x8D0FL), 0x3CL)) >= p_47) != g_741[6]), l_908)), g_130[1][0][0])) == 0xDBF7L)) <= (****g_712)) > 0xDED8063BL);
                g_5[0][1][5] = (l_891 = &l_837[0][3]);
            }
            else
            { /* block id: 375 */
                int32_t *l_919 = (void*)0;
                int32_t l_922[7] = {0x15DA68E7L,0x15DA68E7L,0x15DA68E7L,0x15DA68E7L,0x15DA68E7L,0x15DA68E7L,0x15DA68E7L};
                const int32_t l_923 = 1L;
                uint32_t **l_931 = &l_789[2];
                uint32_t ***l_932 = &l_788[1][1][1];
                uint32_t **l_950 = (void*)0;
                int i;
lbl_939:
                (*l_776) = (l_924 &= (((((safe_sub_func_uint64_t_u_u(g_607, (safe_sub_func_uint64_t_u_u(((*l_747) = (safe_add_func_uint16_t_u_u((((&l_805[3][4][1] != (void*)0) >= g_130[1][2][0]) > ((l_922[2] |= ((&g_268[1] == (void*)0) ^ ((((*l_834) = ((l_919 = l_918) == &l_836[0][0][2])) != (safe_rshift_func_uint16_t_u_s(0UL, 2))) , p_47))) > l_923)), 65535UL))), g_130[1][1][0])))) & 0x52EB9C81A88C71E2LL) & (-1L)) ^ p_49) || 0x3523L));
                if ((((safe_rshift_func_int8_t_s_u((-10L), (safe_add_func_int64_t_s_s((l_919 != l_839[2]), ((*l_776) < ((safe_sub_func_uint16_t_u_u((&g_790 == ((*l_932) = (g_209 , (l_931 = &g_790)))), (((safe_div_func_int64_t_s_s((safe_lshift_func_int16_t_s_u((safe_sub_func_int32_t_s_s(p_49, p_47)), 2)), g_741[2])) && 1L) , (*l_919)))) < l_836[2][0][0])))))) && 0x592E60DCL) , (*l_919)))
                { /* block id: 384 */
                    uint16_t l_956[3][2][2] = {{{0x68AAL,0x68AAL},{0x3CFCL,0x68AAL}},{{0x68AAL,0x3CFCL},{0x68AAL,0x68AAL}},{{0x3CFCL,0x68AAL},{0x68AAL,0x3CFCL}}};
                    int i, j, k;
                    if (l_781)
                        goto lbl_939;
                    for (g_652 = 0; (g_652 <= 3); g_652 += 1)
                    { /* block id: 388 */
                        uint32_t l_940 = 6UL;
                        uint8_t *l_947 = &l_778;
                        uint32_t **l_968[3][4][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_790},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_790}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_790},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_790}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_790},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_790}}};
                        int32_t l_970 = 4L;
                        int i, j, k;
                        l_940++;
                        (*l_747) = ((safe_div_func_uint8_t_u_u((&l_907 != (void*)0), 1UL)) | (0x4DAE2AD4L < (((safe_rshift_func_uint8_t_u_s(((((((*l_947) = g_346[g_652]) || (((*l_919) ^= (((l_836[2][0][0] != p_47) | (l_950 == ((!(safe_rshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((**g_262), g_125)), l_956[2][1][1]))) , g_957[2][6]))) > 0xB1E38FC5E2A31C09LL)) > 0xD47FE7C00C6E173ALL)) && 65535UL) == 0xED9CDAD1BC8FCC1FLL) <= p_47), (*p_48))) & 1L) , 0xD0D11804L)));
                        (*l_776) = (safe_mod_func_int8_t_s_s(((((safe_add_func_uint16_t_u_u((0x0EL < p_47), (*l_918))) ^ (g_268[0]++)) ^ (p_47 < ((*g_712) == (*g_712)))) > (l_837[1][6] = (l_970 = (((l_965 == ((safe_div_func_uint32_t_u_u((0x1B7845EDL ^ 0x7434BD49L), p_49)) , l_968[1][0][1])) , l_969) >= p_49)))), (*p_48)));
                        g_5[0][1][5] = &l_922[0];
                    }
                    l_971 = &l_836[2][0][0];
                }
                else
                { /* block id: 400 */
                    int64_t l_972 = (-1L);
                    int32_t l_973 = 8L;
                    int32_t l_974 = 0xE611BAF9L;
                    int32_t l_975 = 0x0A388919L;
                    int32_t l_976 = 0x33683286L;
                    int32_t l_977 = 0xBC5CEB42L;
                    (*l_918) |= l_972;
                    --l_978;
                }
            }
            for (g_646 = 0; (g_646 >= 40); g_646 = safe_add_func_int32_t_s_s(g_646, 3))
            { /* block id: 407 */
                uint16_t **l_987 = &l_873;
                uint8_t *l_997[8];
                int32_t l_998 = 1L;
                int32_t ***l_1000[1][2];
                int i, j;
                for (i = 0; i < 8; i++)
                    l_997[i] = &l_778;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_1000[i][j] = &g_668;
                }
                (*l_918) = (~((safe_mod_func_uint16_t_u_u(((((p_49 , ((!(((*l_987) = (((*l_747) && 3L) , l_832)) == ((((((safe_lshift_func_int8_t_s_u(0x8FL, (l_998 = ((*g_790) == (safe_rshift_func_int16_t_s_s((safe_div_func_uint8_t_u_u(p_49, (*p_48))), (((*l_971) = ((safe_sub_func_uint16_t_u_u(((*l_754) ^= ((((*l_747) && (*p_48)) >= g_996) > 0xF3B00F94L)), g_652)) ^ 0UL)) >= (*l_747)))))))) < 1UL) , l_999) != (void*)0) , g_130[1][2][0]) , (void*)0))) , l_1000[0][1])) != l_1000[0][1]) >= 0L) || p_49), 65535UL)) == 0xE206B3C1C3F8A318LL));
            }
            (*l_1002) = l_1001;
        }
        else
        { /* block id: 415 */
            int8_t *l_1003 = (void*)0;
            int32_t *l_1004[1][3];
            uint8_t *l_1005 = &l_778;
            const int32_t l_1009 = 9L;
            int32_t l_1010 = 6L;
            int32_t * const *l_1011[6] = {&l_747,&l_747,&l_747,&l_747,&l_747,&l_747};
            int32_t **l_1013[3][7];
            uint64_t *l_1016 = &l_774;
            uint32_t **l_1036 = &g_958;
            uint32_t l_1080 = 0UL;
            uint64_t l_1101 = 0xC6471443E84857C2LL;
            uint32_t l_1106 = 6UL;
            uint64_t l_1143 = 18446744073709551608UL;
            int8_t ***l_1160[2][6][4] = {{{&g_262,(void*)0,&g_262,(void*)0},{&g_262,(void*)0,&g_262,(void*)0},{(void*)0,&g_262,(void*)0,&g_262},{(void*)0,&g_262,&g_262,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_262,&g_262}},{{&g_262,&g_262,&g_262,&g_262},{&g_262,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_262,&g_262,(void*)0,&g_262},{(void*)0,&g_262,(void*)0,(void*)0},{&g_262,(void*)0,&g_262,(void*)0}}};
            int64_t ***l_1201 = (void*)0;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1004[i][j] = &g_342[0][1];
            }
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 7; j++)
                    l_1013[i][j] = &g_5[0][0][3];
            }
            (*l_747) = p_47;
            if (g_24)
                goto lbl_840;
            l_1010 &= (((((*g_262) = l_1003) != (void*)0) > ((((((l_1004[0][2] == (((*l_1005) = 1UL) , (g_741[6] , l_1004[0][2]))) , ((safe_sub_func_uint64_t_u_u(((&g_181 == l_1008[5][1][3]) != p_49), g_106)) , g_346[3])) < p_47) , (*l_747)) > l_1009) == l_1009)) > (*l_918));
            if (((*l_918) = 0L))
            { /* block id: 422 */
                int32_t **l_1012[7][9] = {{&l_918,&l_747,&l_776,&l_747,&l_918,&l_839[1],&l_839[1],&l_918,&l_747},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&l_918,&g_5[0][1][5],&l_839[1],&l_776,&l_776,&l_839[1],&g_5[0][1][5],&l_918,&g_5[0][1][5]},{&l_776,(void*)0,(void*)0,(void*)0,(void*)0,&l_776,(void*)0,&l_776,(void*)0},{&l_747,&g_5[0][1][5],&g_5[0][1][5],&l_747,&l_918,&l_918,&l_918,&l_747,&g_5[0][1][5]},{(void*)0,(void*)0,(void*)0,(void*)0,&l_747,(void*)0,(void*)0,(void*)0,(void*)0},{&g_5[0][1][5],&l_747,&l_918,&l_918,&l_918,&l_747,&g_5[0][1][5],&g_5[0][1][5],&g_5[0][1][5]}};
                int i, j;
                l_1022 &= ((l_1011[5] == (l_1013[2][3] = l_1012[2][2])) < (safe_div_func_int16_t_s_s((((&g_268[1] != l_1016) || (((safe_add_func_uint32_t_u_u((!(((*p_48) , g_130[1][2][0]) <= g_607)), (*g_790))) < (safe_rshift_func_int16_t_s_u(g_598, 11))) | (*l_776))) < (*l_747)), p_49)));
                (*l_918) &= ((void*)0 != l_1003);
            }
            else
            { /* block id: 426 */
                int8_t ** const *l_1035 = &g_262;
                int8_t ** const **l_1034 = &l_1035;
                int32_t l_1038[8] = {0xA319F4EFL,0xA319F4EFL,8L,0xA319F4EFL,0xA319F4EFL,8L,0xA319F4EFL,0xA319F4EFL};
                int32_t *l_1086[9] = {&l_1077,&l_1078,&l_1077,&l_1078,&l_1077,&l_1078,&l_1077,&l_1078,&l_1077};
                uint16_t **l_1096 = &l_873;
                uint64_t l_1100[3];
                int32_t *l_1103 = &l_1077;
                const uint32_t **l_1140 = &l_1136;
                const int8_t * const **l_1154 = (void*)0;
                int32_t **l_1164 = &l_747;
                int64_t *l_1217[1];
                int i;
                for (i = 0; i < 3; i++)
                    l_1100[i] = 0xC648F0E50033B6CFLL;
                for (i = 0; i < 1; i++)
                    l_1217[i] = &g_106;
                for (g_28 = 0; g_28 < 9; g_28 += 1)
                {
                    l_773[g_28] = (-5L);
                }
                for (l_778 = 0; (l_778 > 29); l_778 = safe_add_func_int8_t_s_s(l_778, 6))
                { /* block id: 430 */
                    int8_t l_1025 = 0L;
                    int8_t ****l_1044 = &g_713[1];
                    int64_t *l_1057 = &g_106;
                    int32_t l_1070[1][2][1];
                    uint16_t ***l_1097[9];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 2; j++)
                        {
                            for (k = 0; k < 1; k++)
                                l_1070[i][j][k] = 7L;
                        }
                    }
                    for (i = 0; i < 9; i++)
                        l_1097[i] = &l_1096;
                    if ((p_47 && l_1025))
                    { /* block id: 431 */
                        int8_t l_1037[4] = {0L,0L,0L,0L};
                        int16_t *l_1055 = (void*)0;
                        int16_t *l_1056 = &g_130[1][2][0];
                        int i;
                        l_1038[3] = ((l_1003 == (((g_346[2] && (((safe_add_func_uint16_t_u_u(g_268[5], (safe_mod_func_uint16_t_u_u((p_49 , g_98[2][1][3]), (((&g_958 != (((((safe_div_func_int32_t_s_s((((*p_48) || p_47) == (safe_lshift_func_int8_t_s_s((l_1034 != &g_713[0]), 3))), 4294967287UL)) , p_47) == 4294967295UL) > p_47) , l_1036)) > (*p_48)) , g_646))))) , p_47) < l_1025)) || 0xCD40L) , (void*)0)) <= l_1037[2]);
                        g_5[0][0][0] = &l_1038[3];
                        (*l_918) = ((safe_mod_func_int32_t_s_s(((p_47 == (l_1038[3] < (~(safe_sub_func_uint64_t_u_u(18446744073709551615UL, (l_1044 == &g_713[3])))))) , ((l_1057 = ((((safe_mul_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u(1UL, 6)), (g_741[5] & ((((safe_mod_func_int64_t_s_s((safe_rshift_func_int16_t_s_u(((*l_1056) = g_130[1][2][0]), p_49)), p_47)) , (*l_776)) != g_741[6]) < 8L)))) , (*p_48)) <= 4L) , (void*)0)) == (void*)0)), 4294967295UL)) | p_49);
                        (*l_747) = ((safe_lshift_func_uint16_t_u_u(((*l_873) ^= ((*l_754) = ((((safe_sub_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u(g_646, 10)), ((*g_712) != (void*)0))) | g_652) , ((safe_mod_func_int16_t_s_s(g_6, ((((*p_48) <= (((safe_lshift_func_uint8_t_u_s((safe_add_func_int64_t_s_s((p_47 , (g_106 > (l_1025 | g_6))), l_1037[2])), (*p_48))) , l_1025) < p_47)) , (*l_776)) , g_125))) | g_209)) < p_49))), (*l_776))) ^ p_49);
                    }
                    else
                    { /* block id: 440 */
                        int16_t l_1071 = 0L;
                        int32_t l_1072 = 0L;
                        int32_t l_1075 = 3L;
                        int32_t l_1079[4][5][8] = {{{(-6L),0x8C28F2EAL,7L,0x8C28F2EAL,(-6L),0x51FC0BB0L,(-5L),1L},{0x8C28F2EAL,(-1L),0x0D84DADBL,1L,0xB3C44C44L,0xA5B7B686L,0x67B3A98FL,0x8C28F2EAL},{0xDF1ACD87L,0xB595B8A0L,0x0D84DADBL,0x29B714EBL,0xADE4D082L,0xD4FAA049L,(-5L),0x6D8F0B5DL},{0xB3C44C44L,0x5BFA40BEL,7L,1L,(-10L),(-6L),(-1L),1L},{1L,0x87C17D97L,0x1ADEB717L,0x7C7B9BB0L,0xBF27D286L,0xF78A8DC9L,0x6D8F0B5DL,0xAE1CE500L}},{{0x0D84DADBL,0x145FBBFAL,0L,1L,0xE9C29189L,0x6E5158F2L,0L,(-1L)},{1L,0x144A8D4AL,1L,0x51FC0BB0L,0xF55E7705L,0x16C57CC8L,0x4F9C78D1L,0L},{(-1L),0L,0xADE4D082L,7L,0L,(-1L),(-10L),0xA5B7B686L},{0x8C28F2EAL,0xDF1ACD87L,1L,0x16C57CC8L,(-5L),(-1L),0L,0xCA57D51AL},{1L,8L,0L,0x29B714EBL,0x660906EBL,0x660906EBL,0x29B714EBL,0L}},{{0xAE1CE500L,0xAE1CE500L,0x7C7B9BB0L,0L,0x8C5DF8A1L,1L,(-1L),0x7481CF91L},{(-5L),0L,(-1L),(-6L),0xD4FAA049L,0x51FC0BB0L,0x5F72B832L,0x7481CF91L},{0L,0x145FBBFAL,1L,0L,0xB3C44C44L,3L,(-1L),0L},{7L,1L,0xF78A8DC9L,0x29B714EBL,0xF55E7705L,0xD1F6A99EL,0xADE4D082L,0xCA57D51AL},{0L,0L,7L,0x16C57CC8L,0x6E32ED89L,1L,0xF78A8DC9L,0xA5B7B686L}},{{3L,0x87C17D97L,0xDF1ACD87L,7L,0xB595B8A0L,0xA5B7B686L,0xE9C29189L,0L},{0x0D84DADBL,0x67B3A98FL,0L,0x51FC0BB0L,5L,0xBF27D286L,0x53F23F57L,(-1L)},{0xB3C44C44L,0L,0xD4FAA049L,1L,0x8DDDF536L,1L,0x4F9C78D1L,0xAE1CE500L},{8L,0x5F72B832L,(-5L),0x7C7B9BB0L,7L,(-1L),0xA004FFB6L,1L},{0L,7L,(-5L),1L,(-5L),7L,0L,0x6D8F0B5DL}}};
                        int i, j, k;
                        --l_1080;
                        l_1083[0][4]++;
                        l_1086[1] = (void*)0;
                    }
                    if ((safe_mod_func_uint32_t_u_u(((**l_965) ^= (p_47 & ((safe_rshift_func_uint16_t_u_u(((-9L) <= (p_47 > (((safe_add_func_uint32_t_u_u((~(((**l_1035) = l_1005) != &l_883)), (safe_div_func_int32_t_s_s((((((((((**l_1044) == (void*)0) , ((*l_1016) ^= ((g_1098 = l_1096) == l_1096))) || 0xD1BE7DB3EAC47457LL) < g_607) > p_47) < (*g_1099)) | l_1100[1]) | 0x4BL), p_47)))) , g_598) != p_49))), 3)) , l_1101))), g_98[0][0][4])))
                    { /* block id: 449 */
                        l_1102 = &l_1038[0];
                    }
                    else
                    { /* block id: 451 */
                        int32_t *l_1104 = &l_1078;
                        l_776 = l_1103;
                        if (p_47)
                            break;
                        l_1086[1] = &l_1038[7];
                        l_1104 = &l_1070[0][1][0];
                    }
                    if (p_49)
                        break;
                }
                if (((~((((p_49 , 5L) ^ l_1106) <= (*l_918)) >= ((safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s((((void*)0 != g_1111) ^ (*l_747)), (-5L))), (l_1113[3][0][5] , (*g_1099)))) ^ 1L))) | p_47))
                { /* block id: 459 */
                    uint32_t **l_1134 = &l_789[3];
                    const uint32_t ***l_1137 = &l_1135;
                    const uint32_t **l_1139[6] = {&l_1136,&l_1136,&l_1136,&l_1136,&l_1136,&l_1136};
                    const uint32_t ***l_1138[4] = {&l_1139[3],&l_1139[3],&l_1139[3],&l_1139[3]};
                    int32_t l_1141 = 0xB9B64395L;
                    const int8_t * const l_1157 = &g_1158;
                    const int8_t * const *l_1156[10] = {&l_1157,&l_1157,&l_1157,&l_1157,&l_1157,&l_1157,&l_1157,&l_1157,&l_1157,&l_1157};
                    const int8_t * const **l_1155 = &l_1156[6];
                    int8_t ***l_1159 = (void*)0;
                    int32_t *l_1166 = &g_741[1];
                    int i;
                    if ((!((255UL != ((g_268[1] &= ((g_1115 = ((*g_790) , (void*)0)) != &g_1116[6][0][1])) , ((p_49 , ((safe_div_func_uint64_t_u_u(((((safe_add_func_uint64_t_u_u(((safe_mod_func_uint32_t_u_u(((safe_sub_func_int64_t_s_s((((*p_48) = (((((++(*g_790)) == ((safe_add_func_uint32_t_u_u((((safe_mul_func_int8_t_s_s(((((safe_sub_func_uint16_t_u_u(((((((g_24 , l_1134) == (l_1140 = ((*l_1137) = l_1135))) | 0xA34E5E03L) , 0UL) && l_1141) || g_268[1]), (*l_918))) == 0x7DL) == g_106) > g_106), (*p_48))) > 0UL) > p_49), 0L)) != p_47)) | 0x0C522FA4L) <= 5L) > l_1142[1][0])) | (*l_918)), 1L)) <= p_47), p_49)) > 0xC1E5913924A62BD9LL), (*l_776))) , l_1143) , 0L) == 1UL), p_47)) < 0x8C24L)) | g_996))) <= (**g_1111))))
                    { /* block id: 466 */
                        l_1144 = &l_1141;
                        (*l_1102) ^= p_47;
                        --l_1146;
                    }
                    else
                    { /* block id: 470 */
                        uint64_t l_1163[4][9][3] = {{{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL}},{{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL}},{{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL}},{{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL},{0x8EEFF3284850E0F3LL,18446744073709551611UL,18446744073709551611UL},{18446744073709551609UL,1UL,18446744073709551609UL},{0x8EEFF3284850E0F3LL,0x8EEFF3284850E0F3LL,18446744073709551611UL},{0x6BF646FFC68A4A29LL,1UL,0x6BF646FFC68A4A29LL}}};
                        const int32_t l_1165 = 0x7E25B895L;
                        int i, j, k;
                        (*l_747) &= (*l_1102);
                        g_1149--;
                        (*l_776) ^= ((safe_add_func_int8_t_s_s(((*p_48) &= (*l_1144)), ((((l_1155 = l_1154) != (l_1160[0][2][2] = l_1159)) ^ (safe_sub_func_int8_t_s_s((l_1163[3][3][0] = (0x98C2L < (&g_790 == (void*)0))), (&g_268[3] != (((p_49 <= ((void*)0 == l_1164)) , l_1165) , (void*)0))))) | (**l_1164)))) , 0xEADE12C3L);
                    }
                    l_1166 = (*l_1164);
                    l_1086[1] = ((*l_1164) = (*l_1164));
                }
                else
                { /* block id: 482 */
                    int16_t l_1184[2][7];
                    uint32_t *l_1191 = &g_98[0][0][4];
                    int64_t *l_1219 = (void*)0;
                    int i, j;
                    for (i = 0; i < 2; i++)
                    {
                        for (j = 0; j < 7; j++)
                            l_1184[i][j] = 1L;
                    }
                    for (g_779 = 0; (g_779 != 3); ++g_779)
                    { /* block id: 485 */
                        int32_t *l_1169 = &l_781;
                        int64_t ****l_1202 = &l_1008[5][1][3];
                        (*l_1164) = l_1169;
                        (*l_1169) = (safe_mod_func_uint32_t_u_u((safe_add_func_uint16_t_u_u((((safe_rshift_func_int8_t_s_u((18446744073709551611UL <= (safe_add_func_uint32_t_u_u(((safe_sub_func_int64_t_s_s(((safe_mod_func_int16_t_s_s((safe_add_func_int8_t_s_s(l_1184[1][5], (p_49 , ((safe_rshift_func_int8_t_s_s((-1L), 1)) >= (*p_48))))), (safe_rshift_func_uint8_t_u_s((safe_sub_func_int8_t_s_s((l_1191 != &g_98[0][0][4]), ((safe_add_func_uint32_t_u_u(p_47, ((((safe_add_func_int8_t_s_s((*p_48), (*p_48))) < g_130[1][2][0]) == 0x64L) , p_49))) > 0x00E159FBL))), 4)))) ^ (*l_776)), (*l_1144))) >= (*g_790)), p_47))), 4)) | l_1196) ^ p_47), 2L)), 4294967290UL));
                        (*l_1103) = (safe_rshift_func_uint8_t_u_s((g_646 = ((*l_1005) ^= g_209)), ((**l_1164) , (safe_div_func_uint8_t_u_u((0xF921L == (*g_1112)), (((((l_1201 != ((*l_1202) = (void*)0)) , ((((((((((safe_add_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(p_47, ((safe_rshift_func_uint8_t_u_u(((((safe_lshift_func_int8_t_s_u((safe_div_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((((l_1217[0] == (l_1219 = l_1218)) == p_47) <= (*l_1169)), p_47)), p_47)), p_47)) >= 0UL) > p_49) <= 251UL), g_176)) | p_49))), 1)), 0x007639F45FADFDDFLL)) || g_98[2][6][4]) <= g_130[1][2][0]) == 1L) == 0x3DL) , p_47) && (*l_1103)) >= p_47) < 0L) | g_741[6])) , (void*)0) != (void*)0) , 0x08L))))));
                    }
                }
            }
        }
        for (l_924 = 0; (l_924 > 2); ++l_924)
        { /* block id: 499 */
            uint8_t l_1222 = 3UL;
            l_1222--;
            return l_1222;
        }
    }
    else
    { /* block id: 503 */
        int32_t l_1251 = 0x87D80650L;
        uint32_t l_1264 = 0x9B5C01D8L;
        uint32_t l_1326 = 1UL;
        int32_t l_1328 = 1L;
        int32_t *l_1342 = &l_764;
        int8_t l_1357[4] = {0xE7L,0xE7L,0xE7L,0xE7L};
        int32_t l_1363 = (-1L);
        int32_t l_1364[5] = {0x40A58CF9L,0x40A58CF9L,0x40A58CF9L,0x40A58CF9L,0x40A58CF9L};
        int64_t l_1398[5][1][1];
        int32_t l_1416 = 0L;
        uint32_t **l_1459 = &g_958;
        int8_t ** const *l_1518 = &g_262;
        int8_t ** const **l_1517 = &l_1518;
        int i, j, k;
        for (i = 0; i < 5; i++)
        {
            for (j = 0; j < 1; j++)
            {
                for (k = 0; k < 1; k++)
                    l_1398[i][j][k] = 0x0346EC4DA7EC9D6FLL;
            }
        }
        l_1225 = &l_843;
        for (g_996 = (-26); (g_996 != (-4)); g_996 = safe_add_func_int16_t_s_s(g_996, 4))
        { /* block id: 507 */
            uint32_t l_1252[9][6] = {{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L},{5UL,5UL,0x9B1B4FA9L,5UL,5UL,0x9B1B4FA9L}};
            int32_t l_1253 = 0x5F6DA745L;
            int32_t ***l_1287 = (void*)0;
            const int16_t l_1327 = 0xA1E2L;
            int32_t l_1412 = (-1L);
            int32_t l_1413 = 0xC74D3FD5L;
            int32_t l_1414 = 0L;
            int32_t l_1417 = 0x7A2C42D5L;
            int32_t l_1419[9] = {9L,9L,(-9L),9L,9L,(-9L),9L,9L,(-9L)};
            uint16_t l_1420 = 65535UL;
            int32_t l_1546 = 0L;
            int32_t l_1552 = 3L;
            int i, j;
        }
    }
    return p_49;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_741
 */
static uint16_t  func_55(int32_t ** p_56, uint16_t  p_57, int32_t ** p_58)
{ /* block id: 324 */
    uint32_t l_742 = 0x779B8BD1L;
    (**p_58) = l_742;
    return l_742;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t ** func_59(int16_t  p_60, uint8_t  p_61, int32_t  p_62, int32_t  p_63)
{ /* block id: 321 */
    int32_t *l_737[7][2] = {{&g_6,&g_6},{&g_6,&g_176},{&g_6,&g_176},{&g_6,&g_6},{&g_6,&g_6},{&g_176,&g_6},{&g_176,&g_6}};
    int32_t **l_738[8] = {&l_737[5][0],&l_737[5][0],&l_737[5][0],&l_737[5][0],&l_737[5][0],&l_737[5][0],&l_737[5][0],&l_737[5][0]};
    int i, j;
    l_737[1][1] = &p_63;
    return &g_5[0][1][5];
}


/* ------------------------------------------ */
/* 
 * reads : g_652 g_646 g_106 g_26 g_262 g_263 g_125 g_24 g_176 g_346 g_83 g_130 g_129 g_598 g_268 g_607 g_712 g_6
 * writes: g_652 g_667 g_669 g_646 g_176 g_6 g_26 g_5 g_106 g_125 g_130 g_607 g_83 g_712
 */
static uint8_t  func_72(int32_t  p_73)
{ /* block id: 264 */
    int32_t *l_647 = &g_176;
    int32_t *l_648 = (void*)0;
    int32_t *l_649 = &g_6;
    int32_t *l_650[4][8][2] = {{{&g_176,(void*)0},{&g_176,&g_6},{&g_176,&g_176},{&g_6,&g_176},{&g_6,(void*)0},{&g_6,(void*)0},{&g_6,&g_176},{&g_6,&g_176}},{{&g_176,&g_6},{&g_176,(void*)0},{&g_176,&g_6},{(void*)0,&g_6},{&g_6,&g_176},{&g_6,&g_6},{(void*)0,&g_6},{&g_6,&g_6}},{{&g_6,&g_176},{&g_6,(void*)0},{&g_6,(void*)0},{(void*)0,&g_6},{&g_6,&g_6},{&g_6,&g_6},{(void*)0,(void*)0},{&g_6,(void*)0}},{{&g_6,&g_176},{&g_6,&g_6},{&g_6,&g_6},{(void*)0,&g_6},{&g_6,&g_176},{&g_6,&g_6},{(void*)0,&g_6},{&g_176,(void*)0}}};
    int32_t l_651[1][9] = {{(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L),(-10L)}};
    int32_t ***l_661 = (void*)0;
    int32_t **l_662 = &l_650[0][5][0];
    int32_t **l_666 = &g_5[0][0][2];
    int32_t ***l_665 = &l_666;
    int32_t ****l_664[2][6][2];
    uint8_t *l_670 = &g_646;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 2; k++)
                l_664[i][j][k] = &l_665;
        }
    }
lbl_673:
    --g_652;
    (*l_647) = (safe_sub_func_uint64_t_u_u(((((safe_add_func_uint64_t_u_u(1UL, (safe_rshift_func_int8_t_s_u(((((((l_662 = (void*)0) != &l_648) < ((*l_670) = (g_646 >= (safe_unary_minus_func_uint8_t_u(((p_73 , l_661) == (g_669 = ((l_650[2][0][0] != &g_26) , (g_667 = l_661))))))))) || g_106) , 0xEB4B6F7C4861B695LL) != 0x29569BE494314B79LL), 5)))) , 4294967290UL) != p_73) <= p_73), (-9L)));
    if (((*l_647) = ((*l_649) = ((void*)0 == &g_669))))
    { /* block id: 273 */
        int32_t *l_674 = &g_176;
lbl_730:
        for (g_26 = (-5); (g_26 != 59); ++g_26)
        { /* block id: 276 */
            if (g_26)
                goto lbl_673;
            return p_73;
        }
        (*l_666) = l_674;
        for (g_26 = 0; (g_26 <= 1); g_26 += 1)
        { /* block id: 283 */
            uint64_t l_683 = 0x80B1341458FF35C7LL;
            int64_t *l_684 = (void*)0;
            int64_t *l_685 = &g_106;
            int16_t *l_700 = &g_130[1][2][0];
            int32_t l_701 = 0xD3A4FF7FL;
            int8_t ****l_715[2][4];
            int i, j;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 4; j++)
                    l_715[i][j] = (void*)0;
            }
            (*l_674) = (0L >= ((**g_262) != ((safe_div_func_int32_t_s_s(((g_24 >= p_73) != ((*l_685) = (((safe_mod_func_uint8_t_u_u(0x2DL, 6L)) < (l_683 , p_73)) & (*l_674)))), p_73)) > p_73)));
            (*l_647) = (safe_unary_minus_func_int8_t_s((safe_sub_func_uint8_t_u_u((p_73 != 0x89L), (g_176 <= ((l_683 < ((safe_add_func_int16_t_s_s((safe_lshift_func_int8_t_s_u((((((*l_700) |= (((safe_unary_minus_func_uint8_t_u(g_24)) ^ ((*g_263) = (*g_263))) || (((safe_div_func_int16_t_s_s(((safe_sub_func_int8_t_s_s(((0UL >= ((((((((65529UL == 0UL) ^ g_346[3]) | p_73) >= 0UL) <= 0L) > g_83) > l_683) == l_683)) > p_73), 0xBAL)) || p_73), p_73)) , (*l_674)) , p_73))) ^ g_129) <= 0x4E9520C9A1C399E6LL) | 0x8E9D62F2L), l_701)), g_598)) <= 9L)) & g_268[1]))))));
            for (g_607 = 1; (g_607 >= 0); g_607 -= 1)
            { /* block id: 291 */
                const uint32_t l_708 = 5UL;
                for (g_83 = 6; (g_83 >= 1); g_83 -= 1)
                { /* block id: 294 */
                    int16_t *l_704 = &g_346[3];
                    int8_t *****l_714 = &g_712;
                    int32_t l_728 = 0x66B13857L;
                    int i, j, k;
                    (*l_649) |= (safe_add_func_uint16_t_u_u(((((void*)0 != l_704) > g_268[(g_607 + 5)]) < ((*l_647) = (safe_mod_func_uint32_t_u_u((+l_708), 5UL)))), (safe_sub_func_uint16_t_u_u((~(((*l_714) = g_712) == l_715[0][1])), p_73))));
                    l_728 |= ((((safe_mul_func_uint8_t_u_u(p_73, (((safe_sub_func_int8_t_s_s(p_73, ((((*l_674) = p_73) , ((safe_add_func_int8_t_s_s((safe_mod_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((safe_lshift_func_int8_t_s_u(((void*)0 == l_674), ((l_714 != &g_712) ^ (&g_342[4][2] != &g_342[4][2])))), 0x601AL)), p_73)), (*l_674))) , g_130[1][1][0])) & p_73))) | 7L) & p_73))) , 2L) == (-5L)) , 1L);
                }
                for (p_73 = 1; (p_73 >= 0); p_73 -= 1)
                { /* block id: 303 */
                    if ((8UL <= 251UL))
                    { /* block id: 304 */
                        (*l_649) ^= (~(l_708 , l_701));
                    }
                    else
                    { /* block id: 306 */
                        if (g_26)
                            goto lbl_730;
                    }
                }
            }
        }
    }
    else
    { /* block id: 312 */
        uint32_t l_733 = 0xAC0F7317L;
        int32_t *l_736 = (void*)0;
        for (g_6 = 6; (g_6 >= 11); g_6 = safe_add_func_uint32_t_u_u(g_6, 4))
        { /* block id: 315 */
            l_733--;
        }
        (**l_665) = l_736;
    }
    return p_73;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint8_t  func_74(int32_t  p_75)
{ /* block id: 21 */
    int32_t *l_88 = (void*)0;
    int32_t *l_89 = (void*)0;
    int32_t *l_90 = &g_6;
    int8_t * const l_103 = &g_24;
    int64_t *l_132 = (void*)0;
    int32_t l_149 = 0xCC579967L;
    int32_t l_152[7] = {0x4A033656L,0x4A033656L,0x4A033656L,0x4A033656L,0x4A033656L,0x4A033656L,0x4A033656L};
    uint16_t *l_208[1];
    uint8_t l_229 = 0xA4L;
    uint32_t l_252 = 0x6277879FL;
    int8_t *l_260 = &g_125;
    int32_t l_283 = 0xE0FC4583L;
    int16_t l_299 = 0x5A36L;
    uint8_t l_306 = 0xDCL;
    uint32_t l_316 = 0UL;
    int64_t l_331[6][5];
    const uint64_t l_358 = 18446744073709551615UL;
    int8_t l_412[5][4][3] = {{{0xE1L,(-1L),0x2DL},{1L,1L,4L},{(-1L),0xE1L,1L},{0x86L,1L,0x86L}},{{(-1L),(-1L),(-1L)},{0xBFL,0x86L,0x86L},{(-1L),(-1L),1L},{1L,0xBFL,4L}},{{(-1L),(-1L),0x2DL},{0xBFL,1L,0xFCL},{(-1L),(-1L),(-1L)},{0x86L,0xBFL,1L}},{{(-1L),(-1L),(-1L)},{1L,0x86L,0xFCL},{0xE1L,(-1L),0x2DL},{1L,1L,4L}},{{(-1L),0xE1L,1L},{0x86L,1L,0x86L},{(-1L),(-1L),(-1L)},{0xBFL,0x86L,0x86L}}};
    int8_t l_416 = 0L;
    uint32_t l_423[10][1][7] = {{{4294967295UL,4294967295UL,4294967295UL,0xE50F9D88L,4294967290UL,0x7AB1D79FL,0xB995AF2BL}},{{0xE50F9D88L,4294967295UL,4294967287UL,0x7AB1D79FL,0x7AB1D79FL,4294967287UL,4294967295UL}},{{4294967287UL,0x42B6D261L,4294967295UL,4294967295UL,4294967290UL,0x19C5951EL,0xE50F9D88L}},{{4294967287UL,0xB995AF2BL,1UL,4294967295UL,1UL,0xB995AF2BL,4294967287UL}},{{0x19C5951EL,0x42B6D261L,1UL,0x7AB1D79FL,4294967295UL,4294967290UL,0x8B05C5E7L}},{{4294967295UL,0x8B05C5E7L,4294967287UL,4294967287UL,0x8B05C5E7L,4294967295UL,0x19C5951EL}},{{0xE50F9D88L,4294967287UL,1UL,0x19C5951EL,0xB995AF2BL,4294967295UL,4294967295UL}},{{0x7AB1D79FL,0xB995AF2BL,0UL,0xB995AF2BL,0x7AB1D79FL,4294967290UL,0xE50F9D88L}},{{4294967295UL,4294967287UL,4294967295UL,0x42B6D261L,0x7AB1D79FL,0xE50F9D88L,0x7AB1D79FL}},{{4294967295UL,0x8B05C5E7L,0x8B05C5E7L,4294967295UL,0xB995AF2BL,0x42B6D261L,4294967295UL}}};
    int16_t l_476 = 0xA22BL;
    int8_t ** const *l_567[9];
    int32_t **l_579 = &g_5[0][0][4];
    int32_t ***l_578 = &l_579;
    int32_t l_599 = (-1L);
    uint16_t l_612 = 0x6BEFL;
    int8_t l_635 = 0L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_208[i] = &g_209;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
            l_331[i][j] = (-1L);
    }
    for (i = 0; i < 9; i++)
        l_567[i] = &g_262;
    return p_75;
}




/* ---------------------------------------- */
//testcase_id 1484153286
int case1484153286(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_2, "g_2", print_hash_value);
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_24, "g_24", print_hash_value);
    transparent_crc(g_26, "g_26", print_hash_value);
    transparent_crc(g_28, "g_28", print_hash_value);
    transparent_crc(g_83, "g_83", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_98[i][j][k], "g_98[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_106, "g_106", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_129, "g_129", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_130[i][j][k], "g_130[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_176, "g_176", print_hash_value);
    transparent_crc(g_209, "g_209", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_268[i], "g_268[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_342[i][j], "g_342[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_346[i], "g_346[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_598, "g_598", print_hash_value);
    transparent_crc(g_607, "g_607", print_hash_value);
    transparent_crc(g_646, "g_646", print_hash_value);
    transparent_crc(g_652, "g_652", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_741[i], "g_741[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_775, "g_775", print_hash_value);
    transparent_crc(g_779, "g_779", print_hash_value);
    transparent_crc(g_996, "g_996", print_hash_value);
    transparent_crc(g_1117, "g_1117", print_hash_value);
    transparent_crc(g_1149, "g_1149", print_hash_value);
    transparent_crc(g_1158, "g_1158", print_hash_value);
    transparent_crc(g_1303, "g_1303", print_hash_value);
    transparent_crc(g_1604, "g_1604", print_hash_value);
    transparent_crc(g_1625, "g_1625", print_hash_value);
    transparent_crc(g_1664, "g_1664", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1714[i], "g_1714[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_1716[i][j][k], "g_1716[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1728, "g_1728", print_hash_value);
    transparent_crc(g_1882, "g_1882", print_hash_value);
    transparent_crc(g_1893, "g_1893", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1970[i], "g_1970[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2048, "g_2048", print_hash_value);
    transparent_crc(g_2151, "g_2151", print_hash_value);
    transparent_crc(g_2694, "g_2694", print_hash_value);
    transparent_crc(g_2759, "g_2759", print_hash_value);
    transparent_crc(g_2985, "g_2985", print_hash_value);
    transparent_crc(g_3014, "g_3014", print_hash_value);
    transparent_crc(g_3037, "g_3037", print_hash_value);
    transparent_crc(g_3140, "g_3140", print_hash_value);
    transparent_crc(g_3194, "g_3194", print_hash_value);
    transparent_crc(g_3234, "g_3234", print_hash_value);
    transparent_crc(g_3328, "g_3328", print_hash_value);
    transparent_crc(g_3563, "g_3563", print_hash_value);
    transparent_crc(g_3607, "g_3607", print_hash_value);
    transparent_crc(g_3669, "g_3669", print_hash_value);
    transparent_crc(g_3698, "g_3698", print_hash_value);
    transparent_crc(g_3791, "g_3791", print_hash_value);
    transparent_crc(g_3851, "g_3851", print_hash_value);
    transparent_crc(g_3863, "g_3863", print_hash_value);
    transparent_crc(g_3952, "g_3952", print_hash_value);
    transparent_crc(g_4019, "g_4019", print_hash_value);
    transparent_crc(g_4062, "g_4062", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1060
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 49
breakdown:
   depth: 1, occurrence: 252
   depth: 2, occurrence: 48
   depth: 3, occurrence: 3
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 1
   depth: 10, occurrence: 2
   depth: 11, occurrence: 1
   depth: 12, occurrence: 2
   depth: 14, occurrence: 2
   depth: 15, occurrence: 3
   depth: 16, occurrence: 1
   depth: 17, occurrence: 4
   depth: 18, occurrence: 4
   depth: 19, occurrence: 3
   depth: 20, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 3
   depth: 23, occurrence: 5
   depth: 24, occurrence: 1
   depth: 25, occurrence: 3
   depth: 26, occurrence: 5
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 30, occurrence: 4
   depth: 31, occurrence: 6
   depth: 32, occurrence: 1
   depth: 37, occurrence: 1
   depth: 40, occurrence: 4
   depth: 44, occurrence: 1
   depth: 47, occurrence: 1
   depth: 49, occurrence: 1

XXX total number of pointers: 768

XXX times a variable address is taken: 1901
XXX times a pointer is dereferenced on RHS: 710
breakdown:
   depth: 1, occurrence: 541
   depth: 2, occurrence: 110
   depth: 3, occurrence: 34
   depth: 4, occurrence: 25
XXX times a pointer is dereferenced on LHS: 523
breakdown:
   depth: 1, occurrence: 441
   depth: 2, occurrence: 55
   depth: 3, occurrence: 14
   depth: 4, occurrence: 13
XXX times a pointer is compared with null: 97
XXX times a pointer is compared with address of another variable: 25
XXX times a pointer is compared with another pointer: 29
XXX times a pointer is qualified to be dereferenced: 17620

XXX max dereference level: 7
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2187
   level: 2, occurrence: 543
   level: 3, occurrence: 444
   level: 4, occurrence: 301
   level: 5, occurrence: 86
   level: 6, occurrence: 7
   level: 7, occurrence: 20
XXX number of pointers point to pointers: 349
XXX number of pointers point to scalars: 419
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 29
XXX average alias set size: 1.5

XXX times a non-volatile is read: 3847
XXX times a non-volatile is write: 1703
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 2
XXX backward jumps: 18

XXX stmts: 236
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 24
   depth: 1, occurrence: 20
   depth: 2, occurrence: 30
   depth: 3, occurrence: 38
   depth: 4, occurrence: 48
   depth: 5, occurrence: 76

XXX percentage a fresh-made variable is used: 13.9
XXX percentage an existing variable is used: 86.1
********************* end of statistics **********************/

