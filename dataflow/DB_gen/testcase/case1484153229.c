/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      937140674
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   const signed f0 : 15;
   unsigned f1 : 8;
   signed f2 : 10;
   const unsigned f3 : 14;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S1 {
   int8_t  f0;
   signed f1 : 21;
   uint64_t  f2;
   const int32_t  f3;
   uint16_t  f4;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static int16_t g_3 = 0xA2FAL;
static int32_t g_62 = 0x5A0349B8L;
static struct S1 g_74 = {0xDDL,-1179,18446744073709551609UL,0xBB57CF3DL,1UL};
static int32_t g_99 = 0xD53B18A5L;
static uint8_t g_101 = 0x75L;
static uint16_t g_103 = 0UL;
static int32_t *g_106 = &g_99;
static struct S0 g_120 = {-65,11,27,10};
static struct S0 g_123 = {-63,1,-15,29};
static uint64_t g_143 = 0UL;
static uint64_t g_150[10][9] = {{0xAB42EC255036077BLL,18446744073709551615UL,0xAB42EC255036077BLL,0xD843F4C8ED20401CLL,18446744073709551615UL,9UL,9UL,18446744073709551615UL,0xD843F4C8ED20401CLL},{18446744073709551615UL,18446744073709551613UL,18446744073709551615UL,0x6083B57DFA0F4C75LL,18446744073709551613UL,0xA422C1772BC21653LL,0xA422C1772BC21653LL,18446744073709551613UL,0x6083B57DFA0F4C75LL},{0xAB42EC255036077BLL,18446744073709551615UL,0xAB42EC255036077BLL,0xD843F4C8ED20401CLL,18446744073709551615UL,9UL,9UL,18446744073709551615UL,0xD843F4C8ED20401CLL},{18446744073709551615UL,18446744073709551613UL,18446744073709551615UL,0x6083B57DFA0F4C75LL,18446744073709551613UL,0xA422C1772BC21653LL,0xA422C1772BC21653LL,18446744073709551613UL,0x6083B57DFA0F4C75LL},{0xAB42EC255036077BLL,18446744073709551615UL,0xAB42EC255036077BLL,0xD843F4C8ED20401CLL,18446744073709551615UL,9UL,9UL,18446744073709551615UL,0xD843F4C8ED20401CLL},{18446744073709551615UL,0xA422C1772BC21653LL,0xC80A0586BC1F71C7LL,0x73E0F90C5DD00C07LL,0xA422C1772BC21653LL,18446744073709551607UL,18446744073709551607UL,0xA422C1772BC21653LL,0x73E0F90C5DD00C07LL},{0UL,9UL,0UL,0xF9EAB44AF1E809F8LL,9UL,0UL,0UL,9UL,0xF9EAB44AF1E809F8LL},{0xC80A0586BC1F71C7LL,0xA422C1772BC21653LL,0xC80A0586BC1F71C7LL,0x73E0F90C5DD00C07LL,0xA422C1772BC21653LL,18446744073709551607UL,18446744073709551607UL,0xA422C1772BC21653LL,0x73E0F90C5DD00C07LL},{0UL,9UL,0UL,0xF9EAB44AF1E809F8LL,9UL,0UL,0UL,9UL,0xF9EAB44AF1E809F8LL},{0xC80A0586BC1F71C7LL,0xA422C1772BC21653LL,0xC80A0586BC1F71C7LL,0x73E0F90C5DD00C07LL,0xA422C1772BC21653LL,18446744073709551607UL,18446744073709551607UL,0xA422C1772BC21653LL,0x73E0F90C5DD00C07LL}};
static int64_t g_152[4][5][2] = {{{0x99A07EF0612D7942LL,3L},{1L,1L},{1L,1L},{3L,0x99A07EF0612D7942LL},{3L,1L}},{{1L,1L},{1L,3L},{0x99A07EF0612D7942LL,3L},{1L,1L},{1L,1L}},{{3L,0x99A07EF0612D7942LL},{3L,1L},{1L,1L},{1L,3L},{0x99A07EF0612D7942LL,3L}},{{1L,1L},{1L,1L},{3L,0x99A07EF0612D7942LL},{3L,1L},{1L,1L}}};
static struct S0 g_191 = {126,1,-1,115};
static struct S0 *g_190 = &g_191;
static int8_t g_195 = (-1L);
static uint16_t g_196 = 0x0B37L;
static uint32_t g_205 = 4294967295UL;
static uint8_t *g_237 = &g_101;
static int64_t *g_259 = (void*)0;
static int16_t g_294 = 0xEE7FL;
static uint16_t g_295 = 65533UL;
static int32_t g_334 = 0x0C4FCAFDL;
static int8_t g_347 = 0x52L;
static struct S1 g_368 = {0x5DL,-956,1UL,0xD72D5A0CL,0x31FBL};
static struct S1 *g_367 = &g_368;
static int16_t g_392[2] = {4L,4L};
static uint64_t g_489 = 0x3BA7C9AF6828FB1ELL;
static int32_t *g_516 = &g_334;
static struct S0 g_535 = {-149,13,16,69};
static uint32_t g_553 = 0x67A9C4D5L;
static uint32_t g_558 = 0UL;
static uint8_t g_596 = 0UL;
static struct S1 * const *g_602 = &g_367;
static struct S1 * const **g_601 = &g_602;
static const int32_t g_662 = 1L;
static int16_t g_710 = 0x3FBEL;
static uint32_t g_711 = 0xA012AB33L;
static const uint64_t g_763[1] = {0xFD5B7A4C9589A11FLL};
static const uint64_t g_771 = 0x7BEEBEE58F8B51C9LL;
static uint32_t g_795[6] = {0UL,0UL,0UL,0UL,0UL,0UL};
static uint64_t g_801 = 18446744073709551614UL;
static uint16_t * const g_810[5] = {&g_295,&g_295,&g_295,&g_295,&g_295};
static uint16_t * const *g_809 = &g_810[0];
static const int16_t g_821 = (-3L);
static int16_t g_830[7] = {2L,2L,2L,2L,2L,2L,2L};
static int8_t g_832 = 0x5BL;
static uint8_t g_833 = 0x59L;
static int8_t g_837 = 0x9BL;
static int32_t g_885 = 9L;
static struct S0 g_932 = {-8,10,11,2};
static uint32_t g_936 = 2UL;
static uint32_t g_972 = 4294967295UL;
static uint8_t *g_1026 = &g_101;
static uint8_t g_1046[8][6][5] = {{{1UL,254UL,255UL,254UL,1UL},{0xCFL,2UL,0x3DL,0xCFL,0x77L},{0UL,2UL,255UL,254UL,255UL},{0x77L,0x77L,252UL,2UL,0x77L},{0xFFL,254UL,0xFFL,2UL,1UL},{0x77L,0x3DL,0x3DL,0x77L,0xCFL}},{{0UL,254UL,0x55L,254UL,0UL},{0xCFL,0x77L,0x3DL,0x3DL,0x77L},{1UL,2UL,0xFFL,254UL,0xFFL},{0x77L,2UL,252UL,0x77L,0x77L},{255UL,254UL,255UL,2UL,0UL},{0x77L,0xCFL,0x3DL,2UL,0xCFL}},{{1UL,254UL,255UL,254UL,1UL},{0xCFL,2UL,0x3DL,0xCFL,0x77L},{0UL,2UL,255UL,254UL,255UL},{0x77L,0x77L,252UL,2UL,0x77L},{0xFFL,254UL,0xFFL,2UL,1UL},{0x77L,252UL,252UL,0xCFL,7UL}},{{255UL,2UL,0UL,2UL,255UL},{7UL,0xCFL,252UL,252UL,0xCFL},{0xFFL,0x8AL,255UL,2UL,255UL},{0xCFL,0x3DL,2UL,0xCFL,0xCFL},{0x55L,2UL,0x55L,0x8AL,255UL},{0xCFL,7UL,252UL,0x3DL,7UL}},{{0xFFL,2UL,1UL,2UL,0xFFL},{7UL,0x3DL,252UL,7UL,0xCFL},{255UL,0x8AL,0x55L,2UL,0x55L},{0xCFL,0xCFL,2UL,0x3DL,0xCFL},{255UL,2UL,255UL,0x8AL,0xFFL},{0xCFL,252UL,252UL,0xCFL,7UL}},{{255UL,2UL,0UL,2UL,255UL},{7UL,0xCFL,252UL,252UL,0xCFL},{0xFFL,0x8AL,255UL,2UL,255UL},{0xCFL,0x3DL,2UL,0xCFL,0xCFL},{0x55L,2UL,0x55L,0x8AL,255UL},{0xCFL,7UL,252UL,0x3DL,7UL}},{{0xFFL,2UL,1UL,2UL,0xFFL},{7UL,0x3DL,252UL,7UL,0xCFL},{255UL,0x8AL,0x55L,2UL,0x55L},{0xCFL,0xCFL,2UL,0x3DL,0xCFL},{255UL,2UL,255UL,0x8AL,0xFFL},{0xCFL,252UL,252UL,0xCFL,7UL}},{{255UL,2UL,0UL,2UL,255UL},{7UL,0xCFL,252UL,252UL,0xCFL},{0xFFL,0x8AL,255UL,2UL,255UL},{0xCFL,0x3DL,2UL,0xCFL,0xCFL},{0x55L,2UL,0x55L,0x8AL,255UL},{0xCFL,7UL,252UL,0x3DL,7UL}}};
static const int32_t g_1050[5][9] = {{5L,(-1L),1L,(-1L),5L,0x09AFF512L,0x30FDF826L,5L,1L},{4L,(-1L),0xCFE1856AL,0x30FDF826L,6L,1L,0x30FDF826L,4L,0x14682315L},{(-1L),4L,0x09AFF512L,6L,6L,0x09AFF512L,4L,(-1L),0xCFE1856AL},{(-1L),5L,0x09AFF512L,0x30FDF826L,5L,1L,6L,(-1L),0L},{7L,0x7C55EB31L,5L,7L,0xD311F852L,0x30FDF826L,0xD311F852L,7L,5L}};
static uint16_t g_1133 = 0x019CL;
static struct S1 g_1144 = {0xB5L,-1143,1UL,8L,0x86EEL};
static int16_t g_1154 = (-1L);
static uint16_t *g_1191 = &g_368.f4;
static uint16_t **g_1190 = &g_1191;
static uint16_t *g_1209 = &g_1133;
static int64_t g_1270 = 8L;
static int32_t g_1274 = 5L;
static uint8_t **g_1282 = &g_1026;
static uint8_t ***g_1281 = &g_1282;
static uint8_t g_1287 = 1UL;
static struct S0 g_1289 = {-119,12,31,70};
static struct S0 **g_1290 = &g_190;
static int32_t * const *g_1366 = &g_106;
static int8_t **g_1412 = (void*)0;
static int8_t **g_1413 = (void*)0;
static int32_t **g_1516[7][7][2] = {{{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{(void*)0,&g_516},{(void*)0,&g_516},{&g_516,&g_516}},{{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516}},{{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{(void*)0,&g_516},{(void*)0,&g_516},{&g_516,&g_516},{&g_516,&g_516}},{{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516}},{{&g_516,&g_516},{&g_516,&g_516},{(void*)0,&g_516},{(void*)0,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516}},{{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516}},{{&g_516,&g_516},{(void*)0,&g_516},{(void*)0,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516},{&g_516,&g_516}}};
static int32_t ***g_1515 = &g_1516[5][2][1];
static int16_t *g_1548 = &g_830[0];
static uint16_t g_1565[5][1] = {{0x453AL},{0x453AL},{0x453AL},{0x453AL},{0x453AL}};
static const int64_t *g_1640 = (void*)0;
static const int64_t ** const g_1639 = &g_1640;
static const int64_t ** const *g_1638 = &g_1639;
static struct S1 g_1646 = {-1L,475,1UL,-1L,65532UL};
static const struct S1 *g_1645 = &g_1646;
static uint32_t g_1675[8] = {0x349A5CD8L,0x9C736B2DL,0x349A5CD8L,0x9C736B2DL,0x349A5CD8L,0x9C736B2DL,0x349A5CD8L,0x9C736B2DL};
static int64_t **g_1680 = &g_259;
static const struct S0 g_1714 = {-74,2,15,22};
static uint8_t g_1741 = 0x7AL;
static const int32_t *g_1744[8] = {&g_1274,&g_1274,&g_74.f3,&g_1274,&g_1274,&g_74.f3,&g_1274,&g_1274};
static uint16_t g_1754 = 0x4FE8L;
static int32_t g_1980 = 0x34FBEAF7L;


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static int32_t  func_7(uint32_t  p_8, struct S1  p_9, int32_t  p_10, int32_t  p_11, uint16_t  p_12);
static int8_t  func_15(uint8_t  p_16, int64_t  p_17);
static uint8_t  func_20(int64_t  p_21, const uint64_t  p_22);
static const struct S1  func_27(int32_t  p_28, uint32_t  p_29, int32_t  p_30, const uint32_t  p_31, int32_t  p_32);
static int16_t  func_40(uint64_t  p_41);
static uint64_t  func_45(uint16_t  p_46, int32_t  p_47, struct S1  p_48, int32_t  p_49);
static const int32_t  func_55(const uint32_t  p_56, int16_t  p_57, uint64_t  p_58);
static uint16_t  func_59(const int16_t  p_60, int32_t  p_61);
static int32_t  func_69(int32_t  p_70, struct S1  p_71, struct S1  p_72, struct S0  p_73);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_1366 g_106 g_763 g_1281 g_1282 g_237 g_101 g_99 g_809 g_810 g_295
 * writes: g_3 g_106 g_152 g_99
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int8_t l_42[2][4][1];
    struct S1 l_1944[5][3][7] = {{{{0xBBL,1319,0xB3D407CCDE8433B1LL,-1L,65535UL},{0x8EL,-680,0xC1A033A419329985LL,1L,0x6C94L},{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{1L,313,1UL,2L,0xD06CL},{1L,-115,18446744073709551609UL,0L,0x2BC7L},{0xA0L,-1440,0xD9BC11C42BDBFF14LL,0L,0UL},{0xDDL,950,0x566955D883B40F59LL,0xF0FD5489L,65527UL}},{{0x34L,-308,3UL,0x934F3DACL,0x1756L},{1L,-249,0x1D8BDF7FF16F1F93LL,0xD6F8CE16L,0x7AD2L},{0xF4L,1339,5UL,-1L,0xC6BCL},{5L,894,0xC1C7FE67D3785BEFLL,1L,0x27B6L},{1L,-249,0x1D8BDF7FF16F1F93LL,0xD6F8CE16L,0x7AD2L},{1L,387,0x30F5BF24E28CC65CLL,0xB305873CL,65535UL},{-7L,-314,18446744073709551611UL,0xB6845CC6L,0x1D3FL}},{{0xDEL,-1155,0x2F1E9418ED285D70LL,-1L,6UL},{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{1L,-608,0UL,0x459F2BBAL,0xE613L},{-7L,-314,18446744073709551611UL,0xB6845CC6L,0x1D3FL},{0xE0L,125,1UL,0L,65534UL},{0xE0L,125,1UL,0L,65534UL},{-7L,-314,18446744073709551611UL,0xB6845CC6L,0x1D3FL}}},{{{1L,313,1UL,2L,0xD06CL},{0x06L,-870,0x886FAD68F2AE2E8ELL,0x6356316DL,1UL},{1L,313,1UL,2L,0xD06CL},{0xA0L,-1440,0xD9BC11C42BDBFF14LL,0L,0UL},{-7L,-314,18446744073709551611UL,0xB6845CC6L,0x1D3FL},{0x75L,383,0x418AC98AE9B8AE9CLL,0x5ABEF15DL,0UL},{0xDDL,950,0x566955D883B40F59LL,0xF0FD5489L,65527UL}},{{0xA2L,-1443,18446744073709551615UL,0xB04466C0L,65528UL},{5L,894,0xC1C7FE67D3785BEFLL,1L,0x27B6L},{0L,-682,1UL,0x7FF11C6BL,0x7AF7L},{1L,443,18446744073709551615UL,-2L,1UL},{0xDEL,-1155,0x2F1E9418ED285D70LL,-1L,6UL},{0xF4L,1339,5UL,-1L,0xC6BCL},{0xA2L,-1443,18446744073709551615UL,0xB04466C0L,65528UL}},{{5L,894,0xC1C7FE67D3785BEFLL,1L,0x27B6L},{0x69L,-198,9UL,0x042F03CAL,65530UL},{0xDDL,950,0x566955D883B40F59LL,0xF0FD5489L,65527UL},{-7L,-314,18446744073709551611UL,0xB6845CC6L,0x1D3FL},{0xBBL,1319,0xB3D407CCDE8433B1LL,-1L,65535UL},{0x75L,383,0x418AC98AE9B8AE9CLL,0x5ABEF15DL,0UL},{0x8EL,-680,0xC1A033A419329985LL,1L,0x6C94L}}},{{{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{0x8EL,-680,0xC1A033A419329985LL,1L,0x6C94L},{0xBBL,1319,0xB3D407CCDE8433B1LL,-1L,65535UL},{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{-6L,-351,18446744073709551614UL,2L,0xC18EL},{0xE0L,125,1UL,0L,65534UL},{0xA0L,-1440,0xD9BC11C42BDBFF14LL,0L,0UL}},{{0xA2L,-1443,18446744073709551615UL,0xB04466C0L,65528UL},{1L,-115,18446744073709551609UL,0L,0x2BC7L},{1L,387,0x30F5BF24E28CC65CLL,0xB305873CL,65535UL},{-6L,-351,18446744073709551614UL,2L,0xC18EL},{-6L,-351,18446744073709551614UL,2L,0xC18EL},{1L,387,0x30F5BF24E28CC65CLL,0xB305873CL,65535UL},{1L,-115,18446744073709551609UL,0L,0x2BC7L}},{{-6L,-351,18446744073709551614UL,2L,0xC18EL},{1L,-608,0UL,0x459F2BBAL,0xE613L},{0x75L,383,0x418AC98AE9B8AE9CLL,0x5ABEF15DL,0UL},{1L,-249,0x1D8BDF7FF16F1F93LL,0xD6F8CE16L,0x7AD2L},{0xBBL,1319,0xB3D407CCDE8433B1LL,-1L,65535UL},{0xA0L,-1440,0xD9BC11C42BDBFF14LL,0L,0UL},{1L,443,18446744073709551615UL,-2L,1UL}}},{{{1L,387,0x30F5BF24E28CC65CLL,0xB305873CL,65535UL},{0x57L,691,18446744073709551609UL,0L,0x8182L},{1L,1246,4UL,-8L,0xBBCFL},{1L,-346,4UL,0x8C64E058L,1UL},{0xDDL,950,0x566955D883B40F59LL,0xF0FD5489L,65527UL},{0xF4L,1339,5UL,-1L,0xC6BCL},{0x39L,1157,0xB4049D46FAD1D6B5LL,0x4A9C95B9L,0x55C6L}},{{1L,313,1UL,2L,0xD06CL},{0x75L,383,0x418AC98AE9B8AE9CLL,0x5ABEF15DL,0UL},{0x06L,-870,0x886FAD68F2AE2E8ELL,0x6356316DL,1UL},{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{0xE0L,125,1UL,0L,65534UL},{5L,894,0xC1C7FE67D3785BEFLL,1L,0x27B6L},{0x57L,691,18446744073709551609UL,0L,0x8182L}},{{0xA0L,-1440,0xD9BC11C42BDBFF14LL,0L,0UL},{0x39L,1157,0xB4049D46FAD1D6B5LL,0x4A9C95B9L,0x55C6L},{0xABL,779,0x47DF3B483C342E7FLL,1L,0xE29BL},{0x69L,-198,9UL,0x042F03CAL,65530UL},{0L,-682,1UL,0x7FF11C6BL,0x7AF7L},{0x79L,-626,1UL,1L,0xFA1AL},{1L,313,1UL,2L,0xD06CL}}},{{{0L,-399,0x351B50E885DE7686LL,0xCD3D0A84L,0x83F6L},{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{0xABL,779,0x47DF3B483C342E7FLL,1L,0xE29BL},{0xABL,779,0x47DF3B483C342E7FLL,1L,0xE29BL},{-8L,-486,18446744073709551610UL,0x039BDA23L,0x3E03L},{0L,-399,0x351B50E885DE7686LL,0xCD3D0A84L,0x83F6L},{1L,-854,0xE8BC3E57BC604979LL,-6L,1UL}},{{0xDDL,950,0x566955D883B40F59LL,0xF0FD5489L,65527UL},{0xBBL,1319,0xB3D407CCDE8433B1LL,-1L,65535UL},{0x06L,-870,0x886FAD68F2AE2E8ELL,0x6356316DL,1UL},{0xE0L,125,1UL,0L,65534UL},{0x75L,383,0x418AC98AE9B8AE9CLL,0x5ABEF15DL,0UL},{0xDEL,-1155,0x2F1E9418ED285D70LL,-1L,6UL},{0xE0L,125,1UL,0L,65534UL}},{{0x69L,-198,9UL,0x042F03CAL,65530UL},{0L,-1419,0UL,1L,0UL},{1L,1246,4UL,-8L,0xBBCFL},{1L,-608,0UL,0x459F2BBAL,0xE613L},{1L,-854,0xE8BC3E57BC604979LL,-6L,1UL},{1L,387,0x30F5BF24E28CC65CLL,0xB305873CL,65535UL},{1L,313,1UL,2L,0xD06CL}}}};
    int32_t l_1984 = 0x294F62F9L;
    int32_t l_1985 = 0x25322A15L;
    int8_t l_2057 = 0x1CL;
    int32_t *l_2066[7];
    int8_t l_2067 = 0x32L;
    int32_t l_2068 = 0L;
    int32_t l_2069 = (-8L);
    uint32_t l_2070 = 18446744073709551612UL;
    int32_t **l_2073 = (void*)0;
    int32_t **l_2074 = &g_106;
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 1; k++)
                l_42[i][j][k] = 0L;
        }
    }
    for (i = 0; i < 7; i++)
        l_2066[i] = &g_885;
    if ((!(g_3 | g_3)))
    { /* block id: 1 */
        uint32_t l_1569 = 1UL;
        uint32_t l_1570 = 0x58813151L;
        int32_t l_1982 = 5L;
        int32_t l_1983 = (-1L);
        int32_t l_2008[8][3] = {{0x02CFEEC6L,0x202DCB63L,0x202DCB63L},{0x8CEB20F7L,0xA56822FCL,0x8CEB20F7L},{0x02CFEEC6L,0x02CFEEC6L,0x202DCB63L},{0xA2BB610CL,0xA56822FCL,0xA2BB610CL},{0x02CFEEC6L,0x202DCB63L,0x202DCB63L},{0x8CEB20F7L,0xA56822FCL,0x8CEB20F7L},{0x02CFEEC6L,0x02CFEEC6L,0x202DCB63L},{0xA2BB610CL,0xA56822FCL,0xA2BB610CL}};
        int32_t **l_2058 = &g_106;
        int i, j;
        for (g_3 = 0; (g_3 < 23); g_3++)
        { /* block id: 4 */
            int32_t l_6[2];
            int64_t l_1228[5][8][6] = {{{0x80D972AC35C871FDLL,0xA0A24AAB1EC2CEF8LL,0xD7E8B3F2760BC9A8LL,0L,9L,0x9A17D1E99427AD66LL},{(-10L),0x19A28478EB03E02DLL,0x53AB7DEC91DE3D67LL,(-1L),0xEACFA3EA2A2061A0LL,1L},{1L,(-9L),0L,2L,0x8EDD24A4AF0568C9LL,(-2L)},{1L,0x9184CE0D76432ED0LL,0x8EDD24A4AF0568C9LL,1L,(-1L),0xDC236C195AEB1EE1LL},{0x30AE7CC8E50ADEC2LL,(-3L),0L,0x3FB0FD4C2FBAAC54LL,0x00AB6903A0374306LL,0x6EDBC8B8F291A30FLL},{0x36B975EBD1698FDDLL,9L,0x80D972AC35C871FDLL,0x8EDD24A4AF0568C9LL,0xA58F020CC8228DBCLL,0x83539400993AF2A0LL},{(-1L),0x89A1E8508E3810CCLL,7L,0L,0L,7L},{0x8A13EAF65BD34A53LL,0x8A13EAF65BD34A53LL,0xA58F020CC8228DBCLL,0x3CE4F931DD3380CDLL,0x19A28478EB03E02DLL,(-4L)}},{{1L,0x6EDBC8B8F291A30FLL,0x7752C9617E2D48DELL,0xDF032BF90BF6D300LL,1L,0xA58F020CC8228DBCLL},{(-6L),1L,0x7752C9617E2D48DELL,0L,0x8A13EAF65BD34A53LL,(-4L)},{0xB2720303C3958545LL,0L,0xA58F020CC8228DBCLL,0xB1E2E893E7FA0049LL,(-10L),7L},{0xB1E2E893E7FA0049LL,(-10L),7L,0xFEEEED7BD1FB4E64LL,0x7752C9617E2D48DELL,0x83539400993AF2A0LL},{0xDC236C195AEB1EE1LL,(-1L),0x80D972AC35C871FDLL,0L,(-9L),0x6EDBC8B8F291A30FLL},{(-9L),1L,0L,0x72D0178F86F18A26LL,(-1L),0xDC236C195AEB1EE1LL},{0x361B2584A838A01FLL,0xB2720303C3958545LL,0x8EDD24A4AF0568C9LL,1L,0xFEEEED7BD1FB4E64LL,(-2L)},{0x1EA3632746C5EC6FLL,0x8EDD24A4AF0568C9LL,0L,0x1862097F22D41DE0LL,0x83539400993AF2A0LL,1L}},{{0x72D0178F86F18A26LL,0x361B2584A838A01FLL,0x53AB7DEC91DE3D67LL,1L,(-9L),0x9A17D1E99427AD66LL},{0x53AB7DEC91DE3D67LL,0x36B975EBD1698FDDLL,0xD7E8B3F2760BC9A8LL,0x83539400993AF2A0LL,0x69404361E7843DD0LL,0x7752C9617E2D48DELL},{0x6EDBC8B8F291A30FLL,0xB1E2E893E7FA0049LL,0L,0x6D3A244A3B951C6ELL,0xEEE0DFD5E2AD6DD0LL,0xDF032BF90BF6D300LL},{0xA0A24AAB1EC2CEF8LL,1L,0xDC236C195AEB1EE1LL,0x9184CE0D76432ED0LL,1L,0x34934E3E1EF993C3LL},{9L,0x4690602EC4282DB5LL,0xEBF95A10E7614B67LL,0xD7E8B3F2760BC9A8LL,0xEBF95A10E7614B67LL,0xB1E2E893E7FA0049LL},{0x72D0178F86F18A26LL,1L,0x36B975EBD1698FDDLL,0x1EA3632746C5EC6FLL,0L,0xDF032BF90BF6D300LL},{0x9A17D1E99427AD66LL,0xEACFA3EA2A2061A0LL,0x8EDD24A4AF0568C9LL,0xFEEEED7BD1FB4E64LL,2L,0xB2720303C3958545LL},{(-2L),0xEACFA3EA2A2061A0LL,1L,(-4L),0L,1L}},{{0x80D972AC35C871FDLL,1L,0xA8B289D3C71BD405LL,0x6D3A244A3B951C6ELL,0x4690602EC4282DB5LL,(-10L)},{0xEACFA3EA2A2061A0LL,0xB1E2E893E7FA0049LL,0x00AB6903A0374306LL,0x1AC81D9C0EE19A13LL,0x30AE7CC8E50ADEC2LL,0x53AB7DEC91DE3D67LL},{0xEBF95A10E7614B67LL,0x7752C9617E2D48DELL,0xEACFA3EA2A2061A0LL,0x72D0178F86F18A26LL,1L,0x1862097F22D41DE0LL},{0x25024AAE7895879FLL,0x1862097F22D41DE0LL,0xD56C19AEFB42F12ALL,0xB1E2E893E7FA0049LL,(-1L),(-1L)},{0x83539400993AF2A0LL,0x6EDBC8B8F291A30FLL,(-9L),0L,0x80D972AC35C871FDLL,(-1L)},{0xB2F9A45739BDE66DLL,0x8EDD24A4AF0568C9LL,0xDF032BF90BF6D300LL,(-1L),(-1L),0L},{1L,0x303E681C53C37642LL,0xEEE0DFD5E2AD6DD0LL,0xC031304BEAB4FEB7LL,0x34934E3E1EF993C3LL,0xA58F020CC8228DBCLL},{0L,0L,0x9184CE0D76432ED0LL,0x19A28478EB03E02DLL,0x6D3A244A3B951C6ELL,0L}},{{0L,0xB2F9A45739BDE66DLL,0x30AE7CC8E50ADEC2LL,0x80D972AC35C871FDLL,0x3CE4F931DD3380CDLL,0x83539400993AF2A0LL},{0x1AC81D9C0EE19A13LL,0L,0L,0xA8B289D3C71BD405LL,(-1L),0L},{0xB04AE25FB4A269C5LL,0x00AB6903A0374306LL,0x7752C9617E2D48DELL,0x7752C9617E2D48DELL,0x00AB6903A0374306LL,0xB04AE25FB4A269C5LL},{0xD56C19AEFB42F12ALL,0xEEE0DFD5E2AD6DD0LL,0L,0x89A1E8508E3810CCLL,(-9L),(-9L)},{0x9184CE0D76432ED0LL,0x36B975EBD1698FDDLL,(-10L),2L,7L,0x4C3FE19F0982B95DLL},{0x9184CE0D76432ED0LL,0xC031304BEAB4FEB7LL,2L,0x89A1E8508E3810CCLL,0x823EED9D51CC8A27LL,0xDC236C195AEB1EE1LL},{0xD56C19AEFB42F12ALL,(-9L),1L,0x7752C9617E2D48DELL,0x9A17D1E99427AD66LL,(-4L)},{0xB04AE25FB4A269C5LL,0x83539400993AF2A0LL,9L,0xA8B289D3C71BD405LL,0x361B2584A838A01FLL,1L}}};
            int32_t l_2010 = 0xC7D7CC04L;
            uint8_t * const *l_2038 = &g_237;
            uint8_t * const **l_2037[8][7][4] = {{{&l_2038,(void*)0,&l_2038,&l_2038},{(void*)0,(void*)0,&l_2038,(void*)0},{(void*)0,&l_2038,&l_2038,&l_2038},{&l_2038,(void*)0,(void*)0,&l_2038},{&l_2038,&l_2038,(void*)0,(void*)0},{(void*)0,(void*)0,&l_2038,&l_2038},{&l_2038,(void*)0,&l_2038,(void*)0}},{{&l_2038,(void*)0,&l_2038,(void*)0},{(void*)0,(void*)0,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{(void*)0,(void*)0,&l_2038,(void*)0},{&l_2038,&l_2038,(void*)0,(void*)0},{(void*)0,&l_2038,&l_2038,&l_2038},{&l_2038,(void*)0,&l_2038,&l_2038}},{{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,(void*)0,&l_2038,&l_2038},{(void*)0,&l_2038,&l_2038,(void*)0},{(void*)0,&l_2038,(void*)0,(void*)0},{&l_2038,&l_2038,(void*)0,&l_2038},{(void*)0,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038}},{{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,(void*)0},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,(void*)0,(void*)0,&l_2038},{&l_2038,(void*)0,&l_2038,&l_2038},{&l_2038,&l_2038,(void*)0,&l_2038},{(void*)0,&l_2038,&l_2038,&l_2038}},{{&l_2038,(void*)0,(void*)0,&l_2038},{(void*)0,&l_2038,(void*)0,(void*)0},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038}},{{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,(void*)0,(void*)0},{(void*)0,&l_2038,(void*)0,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{(void*)0,&l_2038,(void*)0,(void*)0},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,(void*)0,&l_2038}},{{&l_2038,(void*)0,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,(void*)0},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{(void*)0,&l_2038,&l_2038,&l_2038},{(void*)0,&l_2038,(void*)0,&l_2038}},{{&l_2038,&l_2038,(void*)0,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,&l_2038,&l_2038,(void*)0},{(void*)0,&l_2038,(void*)0,&l_2038},{&l_2038,&l_2038,&l_2038,&l_2038},{&l_2038,(void*)0,&l_2038,&l_2038}}};
            uint8_t * const ***l_2036 = &l_2037[1][1][0];
            int32_t l_2050 = 0x6DE9630EL;
            int8_t l_2052 = 3L;
            uint16_t l_2053 = 65534UL;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_6[i] = (-1L);
        }
        (*l_2058) = (*g_1366);
    }
    else
    { /* block id: 1091 */
        int32_t l_2062 = 0x8DE6EDE2L;
        uint8_t **l_2064 = &g_1026;
        uint8_t ***l_2063 = &l_2064;
        int64_t *l_2065 = &g_152[2][2][1];
        (*g_106) ^= (((8L != ((+(g_763[0] || ((*l_2065) = (safe_sub_func_uint16_t_u_u(l_2062, ((*g_1281) != ((*l_2063) = (*g_1281)))))))) != l_1944[4][0][4].f4)) >= l_42[1][1][0]) ^ (*g_237));
    }
    l_1985 = l_42[1][0][0];
    l_2070++;
    (*l_2074) = (*g_1366);
    return (**g_809);
}


/* ------------------------------------------ */
/* 
 * reads : g_809 g_810 g_295 g_601 g_602 g_367 g_1274 g_1191 g_368.f4 g_1714.f1 g_368.f1
 * writes: g_106 g_1744 g_1274
 */
static int32_t  func_7(uint32_t  p_8, struct S1  p_9, int32_t  p_10, int32_t  p_11, uint16_t  p_12)
{ /* block id: 1058 */
    int32_t **l_1945 = &g_106;
    int32_t *l_1947 = &g_1274;
    int32_t **l_1946[7][6] = {{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947},{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947},{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947},{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947},{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947},{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947},{&l_1947,&l_1947,&l_1947,&l_1947,&l_1947,&l_1947}};
    int16_t l_1966 = (-1L);
    int8_t ***l_1968 = &g_1412;
    int8_t ****l_1967 = &l_1968;
    uint32_t l_1969[5];
    int32_t l_1970 = 0L;
    int16_t l_1971 = 9L;
    uint16_t l_1972 = 0x7DEAL;
    int64_t l_1975 = 0L;
    uint32_t l_1976 = 9UL;
    int i, j;
    for (i = 0; i < 5; i++)
        l_1969[i] = 0UL;
    g_1744[0] = ((*l_1945) = (void*)0);
    l_1970 |= (safe_rshift_func_uint8_t_u_s(((safe_mul_func_uint8_t_u_u((((((**g_809) , (((*l_1947) = ((((+(&p_9 != (**g_601))) , p_9.f3) >= (safe_rshift_func_int8_t_s_s((((safe_lshift_func_int8_t_s_u((&g_1413 != ((*l_1967) = (((((((!(safe_sub_func_int64_t_s_s(((safe_mul_func_int8_t_s_s((*l_1947), (safe_mul_func_int16_t_s_s((safe_sub_func_uint32_t_u_u((0x17L != ((p_9 , ((p_9.f2 >= p_9.f0) || p_9.f3)) >= (*g_1191))), p_12)), 0x1440L)))) , 0x6515322ED0B23BE5LL), g_1714.f1))) , p_12) || (-7L)) == 0x9A4AL) && 1L) & l_1966) , &g_1412))), 7)) >= 1UL) || p_8), 5))) & p_9.f3)) ^ p_9.f4)) != p_11) & p_9.f0) <= g_368.f1), 0x24L)) >= 247UL), l_1969[4]));
    --l_1972;
    --l_1976;
    return p_8;
}


/* ------------------------------------------ */
/* 
 * reads : g_205
 * writes: g_205 g_1274
 */
static int8_t  func_15(uint8_t  p_16, int64_t  p_17)
{ /* block id: 849 */
    uint32_t *l_1578 = &g_205;
    int32_t l_1581 = (-1L);
    int32_t *l_1583 = &g_1274;
    struct S1 l_1588 = {-1L,1118,0x16F72AF2B4AC8812LL,0x3EEC90D6L,0x3FB0L};
    int8_t *l_1595 = &g_74.f0;
    int8_t *l_1596 = &g_1144.f0;
    uint16_t **l_1599[6][4] = {{(void*)0,&g_1191,&g_1191,(void*)0},{&g_1191,(void*)0,&g_1191,&g_1191},{(void*)0,(void*)0,&g_1191,(void*)0},{(void*)0,&g_1191,&g_1191,(void*)0},{&g_1191,(void*)0,&g_1191,&g_1191},{(void*)0,(void*)0,&g_1191,(void*)0}};
    uint8_t l_1614 = 0x85L;
    struct S1 **l_1617[10][9][2] = {{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}},{{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367},{&g_367,&g_367}}};
    struct S1 ***l_1616 = &l_1617[5][5][1];
    struct S0 ***l_1637[1][5][3] = {{{&g_1290,(void*)0,&g_1290},{&g_1290,&g_1290,(void*)0},{&g_1290,&g_1290,&g_1290},{(void*)0,&g_1290,&g_1290},{&g_1290,&g_1290,(void*)0}}};
    int32_t l_1674 = 0xD9A5BA7CL;
    int32_t l_1753 = 0L;
    int32_t ***l_1813 = (void*)0;
    uint64_t *l_1829 = &g_74.f2;
    int8_t ***l_1905[4][10] = {{&g_1413,(void*)0,&g_1413,&g_1413,&g_1413,(void*)0,&g_1413,&g_1412,&g_1413,&g_1412},{&g_1413,(void*)0,&g_1413,&g_1413,&g_1413,(void*)0,&g_1413,&g_1412,&g_1413,&g_1412},{&g_1413,(void*)0,&g_1413,&g_1413,&g_1413,(void*)0,&g_1413,&g_1412,&g_1413,&g_1412},{&g_1413,(void*)0,&g_1413,&g_1413,&g_1413,(void*)0,&g_1413,&g_1412,&g_1413,&g_1412}};
    int64_t ** const *l_1907 = &g_1680;
    int64_t ** const **l_1906 = &l_1907;
    int32_t l_1912[2];
    uint32_t l_1913 = 18446744073709551607UL;
    uint16_t l_1917 = 0x936FL;
    int32_t **l_1920 = &g_106;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_1912[i] = (-10L);
    l_1581 = ((++(*l_1578)) || ((*l_1583) = (l_1581 | (~(((p_16 && p_17) , &g_972) == l_1578)))));
    return p_17;
}


/* ------------------------------------------ */
/* 
 * reads : g_1274 g_1289.f2
 * writes: g_1274 g_1289.f2
 */
static uint8_t  func_20(int64_t  p_21, const uint64_t  p_22)
{ /* block id: 843 */
    struct S1 * const l_1571 = &g_368;
    int32_t l_1572 = 0x5F16F97EL;
    int32_t *l_1573 = &g_1274;
    int32_t *l_1574[4];
    int i;
    for (i = 0; i < 4; i++)
        l_1574[i] = &l_1572;
    l_1572 = ((void*)0 != l_1571);
    (*l_1573) ^= 0x5D3D1E1FL;
    g_1289.f2 ^= (*l_1573);
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_106 g_1191 g_368.f4 g_205 g_99 g_837 g_936 g_885 g_1209 g_1133 g_237 g_101 g_120.f3 g_1274 g_1154 g_392 g_295 g_1281 g_368.f1 g_1290 g_190 g_191 g_367 g_368 g_1144.f2 g_123.f0 g_1366 g_489 g_809 g_810 g_1144.f4 g_1026 g_62 g_143 g_1270 g_74.f4 g_1287 g_711 g_601 g_602 g_1282 g_516 g_334 g_74.f3 g_1190 g_1515 g_1289.f2 g_662 g_1565 g_535.f0 g_123.f2 g_103 g_801
 * writes: g_885 g_103 g_99 g_801 g_205 g_1270 g_1133 g_1274 g_74.f4 g_936 g_295 g_1154 g_1290 g_368.f2 g_101 g_74.f0 g_150 g_832 g_1144.f0 g_795 g_1366 g_489 g_143 g_106 g_711 g_334 g_516 g_1026 g_833 g_1144.f2 g_190 g_1548 g_1565 g_123.f2
 */
static const struct S1  func_27(int32_t  p_28, uint32_t  p_29, int32_t  p_30, const uint32_t  p_31, int32_t  p_32)
{ /* block id: 654 */
    uint16_t l_1254 = 0xB21AL;
    uint8_t **l_1272 = (void*)0;
    struct S0 *l_1279[4];
    struct S0 *l_1288 = &g_1289;
    const struct S1 l_1299[4] = {{1L,-1209,0xEB772D06D5CD6E12LL,0x14C6F3BCL,1UL},{1L,-1209,0xEB772D06D5CD6E12LL,0x14C6F3BCL,1UL},{1L,-1209,0xEB772D06D5CD6E12LL,0x14C6F3BCL,1UL},{1L,-1209,0xEB772D06D5CD6E12LL,0x14C6F3BCL,1UL}};
    int32_t l_1316 = 0x3FFA652FL;
    int32_t l_1317 = (-8L);
    int32_t l_1343 = 0xC955B6A2L;
    int32_t l_1350 = (-7L);
    uint16_t l_1362 = 0xF657L;
    int32_t **l_1383 = &g_106;
    struct S1 **l_1390 = (void*)0;
    int8_t *l_1410 = &g_74.f0;
    int8_t **l_1409[10][9][2] = {{{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410}},{{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410}},{{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410}},{{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410}},{{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0}},{{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,&l_1410}},{{&l_1410,&l_1410},{(void*)0,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{(void*)0,&l_1410},{&l_1410,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410}},{{&l_1410,&l_1410},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,&l_1410}},{{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410}},{{&l_1410,(void*)0},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{(void*)0,&l_1410},{&l_1410,&l_1410},{&l_1410,&l_1410},{&l_1410,(void*)0},{&l_1410,&l_1410}}};
    int64_t *l_1422 = (void*)0;
    uint8_t l_1452 = 0UL;
    int16_t *l_1455 = &g_392[1];
    int32_t l_1461 = 0x19A01BBAL;
    int32_t l_1462 = (-2L);
    int32_t l_1463 = 0x1F3410F4L;
    int32_t l_1464[7][2] = {{0x82810C61L,0x82810C61L},{0x9C3C7FF5L,0x5A951044L},{(-9L),0x5A951044L},{0x9C3C7FF5L,0x82810C61L},{0x82810C61L,0x9C3C7FF5L},{0x5A951044L,(-9L)},{0x5A951044L,0x9C3C7FF5L}};
    uint32_t l_1465[9] = {0x566AA017L,0UL,0x566AA017L,0UL,0x566AA017L,0UL,0x566AA017L,0UL,0x566AA017L};
    int32_t ** const *l_1517 = &g_1516[5][2][1];
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_1279[i] = &g_120;
lbl_1323:
    for (g_885 = (-4); (g_885 != 23); ++g_885)
    { /* block id: 657 */
        const int32_t *l_1237[1][3];
        const int32_t **l_1236 = &l_1237[0][2];
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 3; j++)
                l_1237[i][j] = &g_74.f3;
        }
        for (g_103 = 0; (g_103 == 36); g_103 = safe_add_func_int8_t_s_s(g_103, 9))
        { /* block id: 660 */
            const int32_t *l_1234 = &g_1050[2][0];
            const int32_t **l_1233 = &l_1234;
            int32_t l_1235[1][1][7];
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 7; k++)
                        l_1235[i][j][k] = (-1L);
                }
            }
            (*l_1233) = &g_1050[2][0];
            l_1235[0][0][3] = ((*g_106) = p_31);
        }
        (*l_1236) = &g_1050[0][6];
        (*l_1236) = &g_885;
    }
    for (g_801 = (-9); (g_801 > 48); g_801 = safe_add_func_int64_t_s_s(g_801, 9))
    { /* block id: 670 */
        uint32_t *l_1248 = &g_205;
        struct S1 *l_1257 = &g_1144;
        int32_t l_1258 = 0x9B77DF6EL;
        uint64_t l_1269 = 0UL;
        int32_t *l_1271 = &g_885;
        int32_t *l_1273 = &g_1274;
        struct S0 ***l_1296 = &g_1290;
        int32_t * const *l_1302 = &g_516;
        int32_t l_1318 = 1L;
        int32_t l_1346 = 0xB8F00BD8L;
        int32_t l_1347 = 0xE83F2D51L;
        int32_t l_1349 = 0x28589EE0L;
        uint16_t l_1351 = 65528UL;
        if (p_30)
            break;
        (*l_1273) &= (((((((safe_mul_func_uint16_t_u_u((((((((((safe_add_func_uint16_t_u_u((*g_1191), (((*g_1209) ^= (safe_add_func_int32_t_s_s(((*l_1271) &= (safe_sub_func_uint32_t_u_u(((*l_1248)++), (((g_1270 = (safe_unary_minus_func_int64_t_s((safe_rshift_func_int16_t_s_s((l_1254 & ((((safe_mod_func_int8_t_s_s((l_1257 == l_1257), l_1258)) == (safe_mul_func_int16_t_s_s(((*g_106) , ((safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(0UL, g_837)), (safe_div_func_int32_t_s_s((1L & l_1258), l_1258)))), l_1254)) >= p_28)), l_1269))) == g_936) == l_1254)), 4))))) >= 0x48L) > 0x39L)))), l_1254))) | l_1254))) != p_29) , l_1272) == l_1272) <= 2L) < p_28) > 0x5B68L) | (*g_237)) <= g_120.f3), 0x402AL)) & 0xFB24L) != 0x945BA6D807DAF431LL) , l_1254) >= p_29) != (*g_106)) , l_1254);
        for (g_74.f4 = 0; (g_74.f4 == 5); g_74.f4 = safe_add_func_uint32_t_u_u(g_74.f4, 1))
        { /* block id: 679 */
            struct S0 ***l_1295 = &g_1290;
            int32_t l_1298 = 0xDE93B6FCL;
            int32_t l_1315 = 1L;
            int32_t *l_1331 = &l_1316;
            int32_t *l_1335 = &l_1316;
            int32_t l_1344 = 0L;
            int32_t l_1345 = 0x39AADBFCL;
            int32_t l_1348[10] = {0xA8727F47L,(-1L),1L,(-1L),0xA8727F47L,0xA8727F47L,(-1L),1L,(-1L),0xA8727F47L};
            int8_t *l_1358 = &g_1144.f0;
            uint32_t *l_1365 = &g_795[0];
            int32_t * const **l_1367 = &g_1366;
            int i;
            for (g_936 = 0; (g_936 < 48); ++g_936)
            { /* block id: 682 */
                int8_t l_1294 = 0L;
                int8_t *l_1322 = &g_74.f0;
                for (g_295 = 0; (g_295 <= 1); g_295 += 1)
                { /* block id: 685 */
                    struct S0 **l_1280 = &l_1279[0];
                    uint8_t * const l_1286 = &g_1287;
                    uint8_t * const *l_1285[9][1][6] = {{{&l_1286,&l_1286,&l_1286,&l_1286,(void*)0,&l_1286}},{{&l_1286,(void*)0,&l_1286,(void*)0,&l_1286,&l_1286}},{{(void*)0,&l_1286,&l_1286,&l_1286,&l_1286,&l_1286}},{{&l_1286,&l_1286,&l_1286,&l_1286,(void*)0,&l_1286}},{{&l_1286,(void*)0,&l_1286,(void*)0,&l_1286,&l_1286}},{{(void*)0,&l_1286,&l_1286,&l_1286,&l_1286,&l_1286}},{{&l_1286,&l_1286,&l_1286,&l_1286,(void*)0,&l_1286}},{{&l_1286,(void*)0,&l_1286,(void*)0,&l_1286,&l_1286}},{{(void*)0,&l_1286,&l_1286,&l_1286,&l_1286,&l_1286}}};
                    uint8_t * const **l_1284[3][3] = {{&l_1285[8][0][5],&l_1285[8][0][5],&l_1285[0][0][1]},{&l_1285[8][0][5],&l_1285[8][0][5],&l_1285[0][0][1]},{&l_1285[8][0][5],&l_1285[8][0][5],&l_1285[0][0][1]}};
                    uint8_t * const ***l_1283 = &l_1284[0][0];
                    int i, j, k;
                    for (g_1154 = 0; (g_1154 <= 1); g_1154 += 1)
                    { /* block id: 688 */
                        int i;
                        (*l_1273) = 5L;
                        (*l_1271) |= g_392[g_1154];
                    }
                    (*l_1273) &= (((*l_1280) = l_1279[2]) != (l_1288 = ((g_392[g_295] , (g_1281 == ((*l_1283) = (void*)0))) , (void*)0)));
                    g_1290 = &g_190;
                }
                for (g_368.f2 = 1; (g_368.f2 <= 5); g_368.f2 += 1)
                { /* block id: 700 */
                    int64_t l_1291 = 1L;
                    struct S0 ****l_1297 = &l_1296;
                    int32_t **l_1300 = &g_516;
                    int32_t *l_1303 = &g_99;
                    int32_t l_1308 = 0x7FF21CF9L;
                    if ((((g_368.f1 , p_31) && p_29) <= (((p_29 == l_1291) ^ (safe_mul_func_uint16_t_u_u((l_1294 != ((((l_1295 != ((*l_1297) = l_1296)) || (((p_29 || l_1254) , (*g_106)) ^ l_1298)) || 0x190E6769L) == 0x3EL)), 0xA973L))) , 0L)))
                    { /* block id: 702 */
                        (*l_1273) ^= l_1298;
                        return l_1299[0];
                    }
                    else
                    { /* block id: 705 */
                        int32_t ***l_1301 = &l_1300;
                        int32_t **l_1304 = &l_1271;
                        int32_t *l_1305 = &g_885;
                        int32_t *l_1306 = (void*)0;
                        int32_t l_1307[7];
                        int32_t *l_1309 = &g_99;
                        int32_t *l_1310 = &l_1307[2];
                        int32_t *l_1311 = &l_1307[0];
                        int32_t *l_1312 = &g_1274;
                        int32_t *l_1313 = &l_1258;
                        int32_t *l_1314[4][4][3] = {{{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]},{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]}},{{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]},{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]}},{{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]},{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]}},{{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]},{(void*)0,(void*)0,(void*)0},{&l_1307[3],&l_1307[3],&l_1307[3]}}};
                        uint16_t l_1319 = 0xCF9BL;
                        int i, j, k;
                        for (i = 0; i < 7; i++)
                            l_1307[i] = 0xC941B1F4L;
                        (*g_106) = (((*l_1301) = l_1300) != l_1302);
                        (*l_1304) = l_1303;
                        l_1319++;
                        (*l_1310) = ((((**g_1290) , 6L) && (l_1322 == (void*)0)) > 0x741A32CF7955C6F4LL);
                    }
                    if ((*g_106))
                        break;
                    return (*g_367);
                }
                for (g_101 = 0; g_101 < 10; g_101 += 1)
                {
                    for (g_74.f0 = 0; g_74.f0 < 9; g_74.f0 += 1)
                    {
                        g_150[g_101][g_74.f0] = 0x729E387FB64EC652LL;
                    }
                }
                if (g_885)
                    goto lbl_1323;
            }
            (*l_1331) ^= ((g_832 = ((*l_1271) > (safe_mod_func_int64_t_s_s((~p_29), g_368.f2)))) ^ (safe_lshift_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((g_1144.f2 & ((-2L) && ((void*)0 != &g_602))), (*l_1273))), 6)));
            for (l_1316 = 7; (l_1316 < 16); l_1316++)
            { /* block id: 722 */
                int16_t l_1334 = 4L;
                int32_t *l_1341 = (void*)0;
                int32_t *l_1342[6] = {&l_1316,&l_1316,&l_1316,&l_1316,&l_1316,&l_1316};
                int i;
                (*l_1271) = ((((l_1334 , &g_885) != l_1335) ^ (p_28 , (((*l_1331) , ((0xFFL < (safe_mul_func_int8_t_s_s((((!0L) == (+g_123.f0)) ^ (~p_28)), p_32))) , 2L)) , p_28))) , (*l_1271));
                ++l_1351;
            }
            (*g_106) ^= (((((safe_rshift_func_int8_t_s_u((safe_mod_func_int8_t_s_s(((*l_1358) = (-6L)), (~((safe_lshift_func_uint8_t_u_u(p_30, (*l_1331))) >= l_1362)))), 2)) ^ p_29) && ((*l_1365) = ((*l_1248)++))) , &l_1335) == ((*l_1367) = g_1366));
        }
        for (l_1318 = (-14); (l_1318 != 18); l_1318++)
        { /* block id: 734 */
            uint64_t *l_1370 = &g_489;
            int32_t **l_1380 = &g_106;
            uint8_t l_1384 = 0x7DL;
            int8_t l_1385 = 0x2AL;
            uint64_t *l_1386 = &g_143;
            struct S1 l_1387 = {0x6DL,-449,0x40F6FEE3785160F9LL,0x839A7707L,0x390AL};
            struct S1 **l_1388 = &g_367;
            struct S1 ***l_1389 = &l_1388;
            int8_t *l_1404 = &g_195;
            struct S0 **l_1405 = &g_190;
            uint16_t **l_1431 = &g_1209;
            int32_t l_1445 = (-1L);
            int16_t *l_1456 = &g_392[1];
            (*l_1273) = (0xA3L < ((++(*l_1370)) & ((((**g_809) != (0xBFBFL > ((((*l_1386) |= (((g_1144.f4 <= (*l_1273)) > p_28) >= ((safe_mul_func_int8_t_s_s((!((safe_mul_func_uint16_t_u_u((((((safe_lshift_func_int8_t_s_u((l_1380 == (((((**g_1366) = ((safe_mod_func_int8_t_s_s((((*g_1026) ^= (((*l_1271) , &g_334) != &p_30)) && (*l_1273)), 0xC6L)) , p_31)) <= 1L) < 0xCAED203693E4EB77LL) , l_1383)), p_32)) , (void*)0) != (void*)0) > l_1384) ^ g_62), p_30)) == l_1384)), p_32)) >= l_1385))) < p_28) > g_1270))) == p_28) || (*g_1209))));
        }
    }
    if ((**l_1383))
    { /* block id: 782 */
        int32_t l_1458 = 1L;
        int32_t *l_1459 = &g_1274;
        int32_t *l_1460[1];
        int i;
        for (i = 0; i < 1; i++)
            l_1460[i] = (void*)0;
        l_1465[7]++;
        for (g_74.f4 = 0; (g_74.f4 <= 42); ++g_74.f4)
        { /* block id: 786 */
            int16_t *l_1470 = &g_294;
            int32_t l_1471[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1471[i] = 0xD7181023L;
            (*l_1383) = &l_1458;
            (**l_1383) ^= ((g_1287 < (((((void*)0 == l_1470) > 0xCF879437L) , g_368.f1) > ((*l_1459) ^= (l_1471[1] = 6L)))) && ((void*)0 == &g_830[3]));
            for (g_711 = 0; (g_711 >= 49); g_711++)
            { /* block id: 793 */
                return (*g_367);
            }
            return (***g_601);
        }
    }
    else
    { /* block id: 798 */
        uint32_t *l_1482 = &l_1465[8];
        int32_t l_1490 = 1L;
        struct S1 l_1491[1] = {{-3L,18,0x81FBB3CA2BE4941BLL,1L,1UL}};
        uint8_t *l_1507 = &g_833;
        int32_t l_1558[2];
        int32_t *l_1568[7][7] = {{&g_1274,&l_1343,&l_1464[0][0],&l_1317,&g_99,(void*)0,&l_1490},{(void*)0,(void*)0,&g_1274,&l_1464[0][0],&g_885,&l_1490,&g_885},{&g_99,&l_1343,&l_1343,&g_99,(void*)0,&l_1317,(void*)0},{&g_99,&l_1490,&g_1274,(void*)0,&l_1461,&l_1558[1],&l_1464[0][0]},{(void*)0,(void*)0,(void*)0,(void*)0,&g_1274,(void*)0,(void*)0},{&g_1274,&g_1274,(void*)0,(void*)0,&l_1558[1],(void*)0,&g_885},{&l_1464[0][0],(void*)0,&g_885,&l_1343,(void*)0,&l_1558[1],&l_1490}};
        int i, j;
        for (i = 0; i < 2; i++)
            l_1558[i] = (-1L);
        for (g_936 = (-29); (g_936 == 18); g_936++)
        { /* block id: 801 */
            struct S1 l_1483 = {0x5BL,351,0xD63B64F4CF80C2C0LL,0x0B63027FL,1UL};
            (**g_1366) = (safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(((((((**l_1383) != (safe_rshift_func_uint16_t_u_u(0x2B54L, ((l_1482 = &p_29) != &p_29)))) || (l_1483 , (safe_lshift_func_int16_t_s_u(((void*)0 != (**g_1281)), 5)))) , ((safe_lshift_func_int16_t_s_s(((safe_add_func_uint16_t_u_u(0x31E2L, ((((*g_516) = (*g_516)) , l_1490) <= p_30))) > (-9L)), g_74.f3)) ^ p_28)) , l_1491[0]) , l_1491[0].f3), (**g_1190))), p_29));
        }
        (*l_1383) = &l_1490;
        for (g_368.f2 = 21; (g_368.f2 != 6); g_368.f2 = safe_sub_func_int32_t_s_s(g_368.f2, 9))
        { /* block id: 809 */
            uint8_t l_1494 = 2UL;
            int16_t *l_1547 = &g_830[0];
            int32_t l_1549 = 1L;
            int32_t l_1550 = (-1L);
            int32_t l_1551 = 2L;
            int32_t l_1559 = 0x2D765D08L;
            int32_t l_1561 = 0x78A226F3L;
            int32_t l_1564 = 0x3FE28A9DL;
            ++l_1494;
            if ((*g_106))
                break;
            for (g_832 = 28; (g_832 != (-24)); g_832 = safe_sub_func_uint64_t_u_u(g_832, 5))
            { /* block id: 814 */
                int32_t **l_1499[8][5] = {{&g_106,&g_106,(void*)0,&g_106,&g_106},{&g_106,&g_106,&g_106,&g_106,&g_106},{(void*)0,(void*)0,(void*)0,&g_106,&g_106},{&g_106,&g_106,&g_106,&g_106,&g_106},{&g_106,&g_106,&g_106,&g_106,&g_106},{&g_106,&g_106,(void*)0,(void*)0,(void*)0},{&g_106,(void*)0,&g_106,&g_106,(void*)0},{&g_106,&g_106,(void*)0,&g_106,&g_106}};
                int32_t **l_1500[8][4] = {{&g_106,&g_106,&g_106,&g_106},{&g_106,(void*)0,(void*)0,&g_106},{(void*)0,&g_106,(void*)0,(void*)0},{&g_106,&g_106,&g_106,&g_106},{&g_106,(void*)0,(void*)0,&g_106},{(void*)0,&g_106,(void*)0,(void*)0},{&g_106,&g_106,&g_106,&g_106},{&g_106,(void*)0,(void*)0,&g_106}};
                int32_t **l_1501 = (void*)0;
                int32_t **l_1502 = (void*)0;
                int32_t **l_1503 = &g_106;
                uint64_t l_1545 = 0x221905C746BE2A9BLL;
                int16_t l_1560 = 0L;
                int i, j;
                (*g_106) = (**l_1383);
                (*l_1503) = (*g_1366);
                for (g_1270 = 0; (g_1270 == (-7)); g_1270 = safe_sub_func_uint64_t_u_u(g_1270, 7))
                { /* block id: 819 */
                    int32_t **l_1506 = &g_516;
                    uint64_t *l_1512 = (void*)0;
                    uint64_t *l_1513 = &g_1144.f2;
                    int32_t ** const **l_1518[10] = {&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517,&l_1517};
                    int32_t ** const *l_1519 = (void*)0;
                    int32_t l_1546 = (-1L);
                    int32_t l_1553 = 0x0930C42DL;
                    int32_t l_1555 = 0xDF4F2388L;
                    int32_t l_1562 = 0xD2A26D68L;
                    int32_t l_1563 = 0xF08AC3C1L;
                    int i;
                    (**l_1383) = (**g_1366);
                    (*g_106) &= (((*l_1506) = &g_334) == &g_334);
                    (**g_1366) &= 7L;
                    if ((((((**g_1281) = (void*)0) != l_1507) || ((((*l_1507) = p_32) >= ((p_29 , (**g_1190)) || (((safe_add_func_uint64_t_u_u((safe_add_func_uint64_t_u_u(((*l_1513) ^= (**l_1383)), (**l_1383))), (~(g_1515 == (l_1519 = l_1517))))) | p_30) < p_31))) ^ p_31)) < g_711))
                    { /* block id: 828 */
                        (*g_1290) = (*g_1290);
                    }
                    else
                    { /* block id: 830 */
                        uint32_t *l_1535[9] = {&l_1465[7],&l_1465[7],&l_1465[7],&l_1465[7],&l_1465[7],&l_1465[7],&l_1465[7],&l_1465[7],&l_1465[7]};
                        int32_t l_1544 = 0x24DDE20FL;
                        int32_t l_1552 = (-10L);
                        int32_t l_1554 = 1L;
                        int32_t l_1556 = 0x3AAF4D66L;
                        int32_t l_1557[7];
                        int i;
                        for (i = 0; i < 7; i++)
                            l_1557[i] = (-2L);
                        (**g_1366) = (safe_mul_func_int16_t_s_s(p_30, (safe_div_func_int16_t_s_s((safe_add_func_uint64_t_u_u((safe_add_func_uint64_t_u_u((+((*l_1513) = (safe_mod_func_int32_t_s_s((safe_mul_func_int8_t_s_s((safe_add_func_int32_t_s_s((p_31 , (l_1535[4] != &p_29)), (((safe_mod_func_int32_t_s_s((((safe_add_func_uint32_t_u_u(0x52C37855L, g_1289.f2)) == (((((**g_1366) >= (safe_add_func_uint32_t_u_u(g_1144.f4, 0xF38E322FL))) && p_29) , (**l_1383)) < (-3L))) ^ l_1544), g_662)) | l_1545) , p_31))), l_1544)), p_31)))), 1L)), l_1546)), (*g_1191)))));
                        (**l_1383) = (l_1547 != (g_1548 = &g_830[3]));
                        g_1565[1][0]++;
                    }
                }
            }
        }
        g_123.f2 ^= ((**l_1383) && g_535.f0);
    }
    return (*g_367);
}


/* ------------------------------------------ */
/* 
 * reads : g_295
 * writes:
 */
static int16_t  func_40(uint64_t  p_41)
{ /* block id: 8 */
    uint8_t l_52 = 0xE7L;
    struct S1 l_75 = {0xECL,-1423,1UL,-10L,0x6013L};
    struct S0 l_76 = {61,15,-11,52};
    uint64_t l_1212 = 0x313A96E1643874EDLL;
    for (p_41 = 22; (p_41 > 37); p_41 = safe_add_func_int32_t_s_s(p_41, 8))
    { /* block id: 11 */
        const int32_t l_199 = 0xE46C906DL;
        const uint8_t l_415 = 0x8FL;
        struct S1 l_416 = {0x5EL,1313,0x7574AEF7B459A286LL,-5L,8UL};
        int32_t *l_1211[9][10] = {{&g_885,&g_885,&g_885,&g_885,&g_885,&g_885,&g_885,&g_885,&g_885,&g_885},{&g_885,(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885},{(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0,&g_885},{(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0},{(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885,(void*)0},{&g_885,(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885},{(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0,(void*)0,&g_885,(void*)0},{(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885,(void*)0},{&g_885,(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885,(void*)0,&g_885,&g_885}};
        int i, j;
    }
    return g_295;
}


/* ------------------------------------------ */
/* 
 * reads : g_601 g_602 g_367 g_795 g_150 g_392 g_106 g_99 g_74.f1 g_516 g_334 g_74.f0 g_123.f2 g_885 g_809 g_810 g_295 g_535.f2 g_74.f2 g_932 g_120.f3 g_837 g_936 g_535.f1 g_972 g_596 g_368.f1 g_821 g_103 g_152 g_830 g_1046 g_101 g_1050 g_3 g_771 g_1026 g_833 g_368.f2 g_368 g_120.f0 g_801 g_191.f0 g_120.f1 g_120.f2 g_763 g_123.f0 g_74.f3 g_123.f1 g_143 g_347 g_832 g_489
 * writes: g_367 g_392 g_120.f2 g_106 g_74.f1 g_368.f1 g_99 g_143 g_347 g_295 g_74.f2 g_972 g_596 g_152 g_237 g_1026 g_932.f2 g_832 g_489 g_795 g_801 g_1133 g_1144.f2 g_368.f0 g_516 g_1190 g_1191 g_1209
 */
static uint64_t  func_45(uint16_t  p_46, int32_t  p_47, struct S1  p_48, int32_t  p_49)
{ /* block id: 152 */
    const int64_t *l_419 = (void*)0;
    int32_t l_430 = (-8L);
    int16_t l_439 = 0x9E31L;
    struct S0 l_448[8] = {{145,15,-5,34},{145,15,-5,34},{145,15,-5,34},{145,15,-5,34},{145,15,-5,34},{145,15,-5,34},{145,15,-5,34},{145,15,-5,34}};
    int64_t **l_449 = &g_259;
    int32_t l_500[3][6] = {{(-5L),1L,(-5L),(-5L),1L,(-5L)},{(-5L),1L,(-5L),(-5L),1L,(-5L)},{(-5L),1L,(-5L),(-5L),1L,(-5L)}};
    struct S0 *l_534 = &g_535;
    uint8_t **l_536[8][3] = {{(void*)0,&g_237,&g_237},{(void*)0,&g_237,(void*)0},{(void*)0,&g_237,&g_237},{(void*)0,&g_237,&g_237},{(void*)0,&g_237,(void*)0},{(void*)0,&g_237,&g_237},{(void*)0,&g_237,&g_237},{(void*)0,&g_237,(void*)0}};
    int32_t *l_565 = &l_500[2][4];
    int32_t *l_566 = (void*)0;
    int32_t l_595 = (-2L);
    struct S1 * const **l_603 = &g_602;
    int64_t l_629 = 1L;
    const int32_t *l_661 = &g_662;
    uint32_t l_769[5][6][3] = {{{0xA61D50BEL,4294967291UL,0UL},{0x71E8BBE8L,0xB28E485DL,4294967291UL},{0xA61D50BEL,0x8EAF7AB2L,0UL},{4294967286UL,4294967290UL,0xB28E485DL},{0xD4362F8DL,3UL,4294967286UL},{0UL,0x64744DF4L,4294967291UL}},{{0x47FD7B16L,0x62F5436BL,0UL},{0x0A0A072FL,0xD4362F8DL,0x7FF9CE34L},{0xC52D49B9L,0UL,0x7FF9CE34L},{0xA6C7F9A8L,0x3E0D177FL,0UL},{0UL,1UL,4294967291UL},{0x2C32EAFCL,4294967286UL,4294967286UL}},{{0x7FF9CE34L,0UL,0xB28E485DL},{0x5EFF2306L,0x7FF9CE34L,0UL},{0UL,0x164D3ED0L,4294967291UL},{0x3E0D177FL,4294967295UL,0UL},{4294967295UL,0x164D3ED0L,0xA6C7F9A8L},{4294967291UL,0x7FF9CE34L,0UL}},{{4294967295UL,0UL,0UL},{3UL,4294967286UL,0UL},{6UL,1UL,0xA8732FA9L},{1UL,0x3E0D177FL,0x64744DF4L},{4294967292UL,0UL,4294967286UL},{4294967292UL,0xD4362F8DL,6UL}},{{1UL,0x62F5436BL,0xC52D49B9L},{6UL,0x64744DF4L,0UL},{3UL,3UL,0x0A0A072FL},{4294967291UL,0xC52D49B9L,4294967295UL},{0xA61D50BEL,4294967295UL,0x8EAF7AB2L},{1UL,0x5EFF2306L,0xA4389031L}}};
    int8_t l_798[7] = {1L,(-9L),1L,1L,(-9L),1L,1L};
    uint8_t l_812 = 247UL;
    uint16_t l_824 = 0x7B55L;
    int32_t l_827 = 0xB21EFC6EL;
    int32_t l_836 = 1L;
    uint16_t l_838[10] = {1UL,0xC74EL,0x9D69L,0xC74EL,1UL,1UL,0xC74EL,0x9D69L,0xC74EL,1UL};
    int32_t l_971 = 0x559941DBL;
    struct S1 *l_1143 = &g_1144;
    int32_t *l_1185 = (void*)0;
    int32_t *l_1186 = &g_334;
    int i, j, k;
    if (p_49)
    { /* block id: 153 */
        int64_t *l_417 = &g_152[0][4][1];
        int64_t **l_418 = &g_259;
        uint16_t l_433 = 0x1F47L;
        const uint16_t *l_437 = (void*)0;
        const uint16_t **l_436 = &l_437;
        int8_t *l_438 = &g_347;
        struct S1 **l_478 = &g_367;
        int32_t l_488 = 0xD1537A97L;
        int32_t l_498 = (-9L);
        int32_t l_499 = 2L;
        int32_t l_502 = (-1L);
        int32_t *l_509 = &g_99;
        int32_t *l_524[3][6][10] = {{{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502},{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502},{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502}},{{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502},{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502},{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502}},{{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502},{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502},{&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502,&l_430,&l_502},{&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502,&l_499,&l_502}}};
        int8_t l_542 = 0x8FL;
        struct S0 l_604 = {-177,15,-12,20};
        int16_t *l_654 = &g_392[1];
        uint8_t *l_657 = &g_596;
        uint32_t l_670 = 4294967293UL;
        struct S0 **l_740 = &l_534;
        uint32_t l_811 = 0x63ADDC3CL;
        int i, j, k;
    }
    else
    { /* block id: 402 */
        uint32_t l_851[6] = {4294967289UL,0x76D56804L,0x76D56804L,4294967289UL,0x76D56804L,0x76D56804L};
        int32_t ** const l_872[4] = {&l_566,&l_566,&l_566,&l_566};
        uint32_t l_986 = 0UL;
        uint8_t *l_1027 = (void*)0;
        uint8_t *l_1028 = &g_101;
        uint16_t l_1063 = 0xA0ABL;
        struct S1 *l_1068 = &g_368;
        struct S0 **l_1070[10] = {&l_534,&l_534,&l_534,&l_534,&l_534,&l_534,&l_534,&l_534,&l_534,&l_534};
        uint32_t l_1107 = 0x514155C4L;
        int64_t **l_1167 = &g_259;
        uint16_t * const *l_1192[1][2][8];
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 2; j++)
            {
                for (k = 0; k < 8; k++)
                    l_1192[i][j][k] = &g_1191;
            }
        }
lbl_1148:
        if (p_48.f1)
        { /* block id: 403 */
            const uint8_t *l_846 = &g_101;
            int32_t l_863[1][9][1] = {{{0x5EDDCE03L},{0xC264EB62L},{0x5EDDCE03L},{0xC264EB62L},{0x5EDDCE03L},{0xC264EB62L},{0x5EDDCE03L},{0xC264EB62L},{0x5EDDCE03L}}};
            int32_t *l_866 = &l_430;
            int i, j, k;
            for (p_49 = 1; (p_49 <= 5); p_49 += 1)
            { /* block id: 406 */
                struct S1 **l_842 = &g_367;
                uint32_t *l_852 = &l_769[1][2][0];
                int32_t *l_853[1][7] = {{&l_827,&l_827,&l_827,&l_827,&l_827,&l_827,&l_827}};
                int i, j;
                (*l_842) = (**g_601);
                l_836 = ((((((safe_rshift_func_uint16_t_u_u(0UL, 11)) > g_795[p_49]) , 0UL) < p_48.f1) < ((*l_565) = ((+((void*)0 != l_846)) > ((*l_852) &= ((safe_mul_func_int8_t_s_s((safe_lshift_func_int16_t_s_u((*l_565), g_795[p_49])), (l_851[5] = ((-8L) & p_48.f0)))) || 0x755AL))))) || (-1L));
                for (p_48.f2 = 0; (p_48.f2 <= 8); p_48.f2 += 1)
                { /* block id: 414 */
                    int32_t l_862 = 0xA4390E15L;
                    int i, j;
                    if ((((p_48.f1 |= g_150[(p_48.f2 + 1)][p_48.f2]) > (safe_add_func_int32_t_s_s((((g_120.f2 = (safe_add_func_int32_t_s_s((safe_div_func_int32_t_s_s(g_150[(p_49 + 3)][(p_49 + 2)], (safe_lshift_func_uint8_t_u_s(((g_392[1] &= ((void*)0 != (*l_842))) >= p_49), (l_862 < p_48.f2))))), (&g_143 != (p_48.f0 , &g_150[5][8]))))) & 0x43C43410L) && p_48.f0), l_863[0][5][0]))) > p_46))
                    { /* block id: 418 */
                        int32_t **l_864 = &l_853[0][6];
                        (*l_864) = &l_500[1][0];
                    }
                    else
                    { /* block id: 420 */
                        int32_t **l_865 = &g_106;
                        (*l_865) = &g_99;
                        if ((*g_106))
                            continue;
                        (*l_865) = &p_49;
                    }
                }
            }
            (*l_866) = ((*l_565) = (p_48 , g_150[0][3]));
        }
        else
        { /* block id: 429 */
            int32_t *l_867 = (void*)0;
            int32_t *l_868 = &l_595;
            int8_t l_869 = 1L;
            int64_t l_888 = 0xA0A09C5699EE8FACLL;
            struct S0 ** const l_908 = &g_190;
            uint32_t l_913[2][4][4] = {{{0x25F650A2L,0x7DEFDA47L,0xD6D87DCDL,0x25F650A2L},{0xD6D87DCDL,0x25F650A2L,0x27C325ACL,0x27C325ACL},{0xC4D09B75L,0xC4D09B75L,0x4F266C63L,18446744073709551615UL},{0xC4D09B75L,0x7DEFDA47L,0x27C325ACL,0xC4D09B75L}},{{0xD6D87DCDL,18446744073709551615UL,0xD6D87DCDL,0x27C325ACL},{0x25F650A2L,18446744073709551615UL,0x4F266C63L,0xC4D09B75L},{18446744073709551615UL,0x7DEFDA47L,0x7DEFDA47L,18446744073709551615UL},{0xD6D87DCDL,0xC4D09B75L,0x7DEFDA47L,0x27C325ACL}}};
            int8_t l_937 = 1L;
            uint64_t l_938[2];
            int8_t *l_961 = &l_869;
            int8_t **l_960 = &l_961;
            int32_t l_962 = 8L;
            int32_t l_969 = 4L;
            int32_t l_970 = 0x93F82357L;
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_938[i] = 0xB019E063A4087628LL;
            (*l_868) = ((*l_565) = 0xB2C480DBL);
            g_74.f1 &= l_869;
            if ((g_99 = ((*l_565) = (safe_lshift_func_uint8_t_u_u(l_851[5], ((((*g_516) , l_872[2]) != (void*)0) , ((((*l_565) >= ((((((g_74.f0 && ((g_368.f1 = ((*l_868) = ((p_49 & (0x5F107DEFAD458953LL == ((0x8F17L < p_49) == 0x16L))) >= p_48.f3))) , p_48.f0)) , (-1L)) & 0xD470L) >= (*l_565)) , p_48.f1) <= p_48.f2)) , p_46) | (-2L))))))))
            { /* block id: 437 */
                int8_t l_880[6][9][3] = {{{0x3DL,(-3L),0x79L},{7L,4L,(-1L)},{0x0AL,0x79L,0xCDL},{0xAAL,0x1BL,0x31L},{0x0AL,0xA9L,0xF1L},{7L,7L,(-1L)},{0x3DL,0x55L,9L},{(-1L),(-1L),7L},{0x11L,0x35L,0x30L}},{{0x1CL,(-1L),7L},{0L,9L,9L},{0x9BL,0xAAL,(-1L)},{0xE9L,1L,0xF1L},{9L,9L,0x31L},{0xF1L,0x11L,0xCDL},{(-1L),9L,(-1L)},{0x79L,1L,0x79L},{9L,0xAAL,(-1L)}},{{0x55L,9L,0x0AL},{(-1L),(-1L),0L},{(-3L),0x35L,0x4DL},{(-1L),(-1L),0x1CL},{0x55L,0x55L,1L},{9L,7L,4L},{0x79L,0xA9L,0x55L},{(-1L),0x1BL,0x31L},{0xF1L,0x79L,0x55L}},{{9L,4L,4L},{0xE9L,(-3L),1L},{0x9BL,(-1L),0x1CL},{0L,0x3DL,0x4DL},{0x1CL,(-1L),0L},{0x11L,0x3DL,0x0AL},{(-1L),(-1L),(-1L)},{0x3DL,(-3L),0x79L},{7L,0L,(-1L)}},{{0x79L,0L,0x30L},{9L,(-1L),9L},{0x79L,0x55L,(-3L)},{0x1CL,0x1CL,0x1BL},{9L,0xF1L,0xCDL},{1L,7L,0x1CL},{0xA9L,0x11L,0x0AL},{0xAAL,1L,0x1CL},{0xE9L,0xCDL,0xCDL}},{{0x31L,9L,0x1BL},{0x4DL,0x35L,(-3L)},{0x9BL,4L,9L},{(-3L),0xA9L,0x30L},{(-1L),4L,(-1L)},{0L,0x35L,0L},{4L,9L,1L},{0xF1L,0xCDL,0x79L},{(-1L),1L,0x31L}}};
                int32_t l_881 = 0xD63E8565L;
                uint32_t l_882 = 0x9CE2CD28L;
                int i, j, k;
                for (g_143 = 24; (g_143 >= 4); g_143--)
                { /* block id: 440 */
                    if (p_46)
                    { /* block id: 441 */
                        int64_t l_875 = 0xC00BA2B4372D476BLL;
                        return l_875;
                    }
                    else
                    { /* block id: 443 */
                        return g_123.f2;
                    }
                }
                for (l_869 = 0; (l_869 == 7); l_869++)
                { /* block id: 449 */
                    (*l_565) ^= (safe_rshift_func_uint16_t_u_s(1UL, 15));
                    l_882++;
                    for (l_812 = 0; (l_812 <= 6); l_812 += 1)
                    { /* block id: 454 */
                        uint32_t l_889 = 6UL;
                        (*l_868) = (g_885 != (l_889 = (safe_mul_func_uint8_t_u_u(l_888, 0x76L))));
                        if (l_882)
                            continue;
                    }
                }
            }
            else
            { /* block id: 460 */
                struct S0 **l_894 = &g_190;
                const int32_t l_912[7] = {0x5542C803L,0x5542C803L,0x5542C803L,0x5542C803L,0x5542C803L,0x5542C803L,0x5542C803L};
                int16_t l_916 = (-3L);
                const int32_t *l_918[5][3] = {{(void*)0,&g_885,&g_885},{&l_912[3],&l_836,&l_836},{(void*)0,&g_885,&g_885},{&l_912[3],&l_836,&l_836},{(void*)0,&g_885,&g_885}};
                const int32_t **l_917 = &l_918[4][1];
                int32_t l_934 = 0x9A8E1898L;
                const struct S0 * const l_935 = &g_191;
                uint32_t l_1004 = 0UL;
                int64_t l_1014[5][7] = {{0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL},{0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL},{0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL},{0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL,0x1E869F8CE4F46170LL},{0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL,0x409DCEE13B9028B9LL,0x3CAF640F97468868LL}};
                int i, j;
                for (p_49 = (-1); (p_49 > 19); p_49 = safe_add_func_int16_t_s_s(p_49, 6))
                { /* block id: 463 */
                    struct S1 *l_911 = (void*)0;
                    for (g_347 = 1; (g_347 > (-1)); g_347 = safe_sub_func_uint16_t_u_u(g_347, 1))
                    { /* block id: 466 */
                        int8_t l_899 = 0x84L;
                        (*l_868) &= (((&g_190 == l_894) , (0x283BL ^ (--(**g_809)))) != p_48.f3);
                        (*l_565) &= (safe_mul_func_uint16_t_u_u((p_48.f4 = l_899), ((((safe_lshift_func_uint16_t_u_u(p_46, (0x67928430A39AEFE7LL & (((3UL != (p_47 | (((safe_add_func_int32_t_s_s((safe_sub_func_int64_t_s_s(p_48.f1, (l_908 != ((safe_add_func_uint16_t_u_u((p_48.f0 < (((l_911 = &g_74) != (*g_602)) == 65529UL)), p_47)) , (void*)0)))), g_535.f2)) & p_49) , 4L))) > g_885) >= p_49)))) < p_46) == l_912[3]) ^ p_47)));
                        l_913[1][3][3]++;
                    }
                    return l_916;
                }
                (*l_917) = &l_912[3];
                for (p_48.f0 = 0; (p_48.f0 <= 2); p_48.f0 += 1)
                { /* block id: 479 */
                    uint32_t l_919 = 0x06ECBF05L;
                    int32_t l_922 = 4L;
                    int8_t ***l_956 = (void*)0;
                    int8_t *l_959 = &l_869;
                    int8_t **l_958[3];
                    int8_t ***l_957 = &l_958[1];
                    int32_t l_968 = (-7L);
                    const uint16_t **l_1000 = (void*)0;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_958[i] = &l_959;
                    if (((*l_565) = ((l_919++) == 255UL)))
                    { /* block id: 482 */
                        uint16_t l_923 = 8UL;
                        uint64_t *l_924 = &g_74.f2;
                        int8_t *l_933[1][4];
                        int i, j;
                        for (i = 0; i < 1; i++)
                        {
                            for (j = 0; j < 4; j++)
                                l_933[i][j] = &l_869;
                        }
                        l_922 |= 0xE6A32A2FL;
                        g_106 = &g_99;
                        l_923 = p_49;
                        l_937 = ((0x721CL == ((((*l_924)--) , ((safe_mod_func_int32_t_s_s(((*l_868) |= (*l_565)), ((l_934 = ((+(safe_mod_func_int64_t_s_s(0L, (g_932 , g_932.f1)))) >= g_120.f3)) & p_48.f1))) <= ((&g_120 == l_935) <= p_48.f4))) ^ g_837)) ^ g_936);
                    }
                    else
                    { /* block id: 490 */
                        l_868 = &p_49;
                        g_106 = (void*)0;
                        (*l_917) = (g_106 = &g_99);
                    }
                    (*g_106) = (g_334 > (l_938[0]++));
                    if (((((safe_sub_func_uint8_t_u_u((!((l_603 != &g_602) , ((safe_sub_func_int64_t_s_s(((safe_mul_func_int8_t_s_s(((safe_mul_func_int16_t_s_s((safe_mod_func_int8_t_s_s(g_535.f1, (safe_mul_func_int16_t_s_s((safe_sub_func_int16_t_s_s(p_49, (((*l_957) = (void*)0) == l_960))), p_48.f0)))), 65535UL)) & (1UL == p_48.f4)), p_49)) ^ 0x1916L), l_962)) , 0x67F17F23L))), p_49)) , p_47) , p_49) != (*l_565)))
                    { /* block id: 499 */
                        (*g_106) |= (safe_div_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_s(p_46, 2)), (p_46 ^ 0x161BAECFCF9514C2LL)));
                    }
                    else
                    { /* block id: 501 */
                        int32_t *l_967[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                        uint32_t l_997[3][9] = {{1UL,0x38B40C9DL,0x38B40C9DL,1UL,0x38B40C9DL,0x38B40C9DL,1UL,0x38B40C9DL,0x38B40C9DL},{0xFF1362FBL,0xACB2C671L,0xACB2C671L,0xFF1362FBL,0xACB2C671L,0xACB2C671L,0xFF1362FBL,0xACB2C671L,0xACB2C671L},{1UL,0x38B40C9DL,0x38B40C9DL,1UL,0x38B40C9DL,0x38B40C9DL,1UL,0x38B40C9DL,0x38B40C9DL}};
                        const uint16_t *l_999 = &g_196;
                        const uint16_t **l_998 = &l_999;
                        int16_t *l_1001 = &g_392[0];
                        int64_t *l_1002 = &g_152[2][2][1];
                        int i, j;
                        l_967[2] = &p_49;
                        g_972++;
                        p_48.f1 = (((safe_div_func_uint8_t_u_u((safe_rshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s(((*l_1002) = (((*l_1001) = (+((*l_565) &= (((safe_sub_func_uint32_t_u_u(((p_48.f2 <= (safe_sub_func_uint32_t_u_u(((void*)0 != l_959), ((void*)0 != &g_334)))) || l_986), ((safe_sub_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((((safe_div_func_uint8_t_u_u((g_596++), ((*l_961) &= (p_46 ^ (safe_unary_minus_func_int32_t_s(((*g_106) = ((((!l_997[2][1]) == ((**g_809) != (-2L))) , &g_205) == (void*)0)))))))) , l_998) != l_1000) <= g_74.f0), p_48.f4)), p_48.f4)) & p_46))) , 0x318BDE28141F2FE4LL) != 0x7B04FE396E2A33D3LL)))) & 0x0E8BL)), 0xF04BB31F39B15CB7LL)), l_968)), g_368.f1)) <= p_48.f4) > p_48.f3);
                    }
                    for (g_972 = 0; (g_972 <= 2); g_972 += 1)
                    { /* block id: 514 */
                        int32_t l_1003[8] = {0x95360421L,0L,0x95360421L,0L,0x95360421L,0L,0x95360421L,0L};
                        int64_t *l_1013 = &g_152[2][2][1];
                        int i, j;
                        l_1004--;
                        p_49 |= (safe_lshift_func_int16_t_s_s(p_48.f1, (((**l_960) &= ((0x1B903F04L > (0x8A57768EEF3C5481LL <= (safe_add_func_int64_t_s_s((l_1003[1] >= 0x67L), (((*l_1013) ^= (g_821 <= ((p_46 , (safe_rshift_func_uint8_t_u_u((*l_565), p_48.f0))) <= g_103))) | l_1014[4][5]))))) , 0L)) , 0xD295L)));
                    }
                }
            }
            for (l_971 = 0; (l_971 != (-26)); --l_971)
            { /* block id: 524 */
                uint8_t *l_1025 = &g_596;
                int32_t l_1034 = 0x935F02F6L;
                uint32_t l_1049 = 1UL;
                g_106 = &p_49;
                for (g_99 = 0; (g_99 < 21); g_99 = safe_add_func_int16_t_s_s(g_99, 9))
                { /* block id: 528 */
                    g_106 = &g_99;
                }
                g_932.f2 |= ((*l_868) = ((((safe_mul_func_uint8_t_u_u(g_830[6], (safe_rshift_func_int8_t_s_u((safe_div_func_int32_t_s_s((((g_1026 = (l_1025 = (g_237 = &g_833))) != (l_1028 = l_1027)) & (safe_add_func_int8_t_s_s((safe_unary_minus_func_uint32_t_u((safe_mul_func_uint16_t_u_u(l_1034, (**g_809))))), ((safe_sub_func_int32_t_s_s((&l_851[4] != &g_972), (safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_s((safe_div_func_int8_t_s_s((+(safe_mul_func_int16_t_s_s((g_1046[3][3][1] != (safe_lshift_func_uint16_t_u_s((0x16DBL == g_101), (*l_868)))), p_48.f2))), l_1049)), p_48.f3)) <= (*l_565)), 14)))) && 0xE4L)))), 0x8E699D65L)), 4)))) == g_1050[2][0]) | 1UL) > g_3));
            }
        }
        for (g_832 = 23; (g_832 >= (-19)); g_832--)
        { /* block id: 541 */
            int16_t l_1062 = (-1L);
            uint64_t *l_1118 = &g_801;
            uint32_t l_1134 = 1UL;
            int64_t *l_1135 = &g_152[2][0][1];
            uint16_t *l_1136[5][1] = {{&l_838[5]},{(void*)0},{&l_838[5]},{(void*)0},{&l_838[5]}};
            int32_t l_1155 = 0xF75CFA38L;
            int32_t l_1157[4] = {0x936557F9L,0x936557F9L,0x936557F9L,0x936557F9L};
            int32_t l_1174 = 0x0AAD703BL;
            int8_t **l_1200 = (void*)0;
            struct S1 *l_1207[9];
            int i, j;
            for (i = 0; i < 9; i++)
                l_1207[i] = (void*)0;
            (*l_565) |= 0L;
            for (g_489 = 23; (g_489 == 33); ++g_489)
            { /* block id: 545 */
                int64_t l_1060 = (-7L);
                int32_t l_1061 = 0x72BA01FDL;
                const uint8_t l_1106[9][7][4] = {{{0x50L,9UL,0xC5L,249UL},{0x50L,0x1BL,0UL,1UL},{1UL,249UL,255UL,0UL},{250UL,0x31L,255UL,0xF9L},{9UL,255UL,0UL,0UL},{0UL,0UL,0x68L,0x6AL},{251UL,249UL,251UL,0xB5L}},{{0x68L,0xC2L,255UL,0x99L},{255UL,0x68L,9UL,251UL},{0x68L,249UL,0xE7L,0x1BL},{1UL,255UL,0x50L,0xBDL},{9UL,0xB5L,255UL,0xE7L},{249UL,0UL,249UL,255UL},{0xE7L,0xC2L,0xF9L,251UL}},{{255UL,255UL,0x0CL,0xC2L},{250UL,0xF9L,0x0CL,0x50L},{255UL,0x1BL,0xF9L,0xC5L},{0xE7L,249UL,249UL,255UL},{249UL,255UL,255UL,0x6AL},{9UL,0x50L,0x50L,9UL},{1UL,251UL,0xE7L,0UL}},{{0x68L,255UL,9UL,249UL},{255UL,0UL,255UL,249UL},{9UL,255UL,0x99L,0UL},{0x50L,251UL,9UL,9UL},{255UL,0x50L,0xBDL,0x6AL},{0xF9L,255UL,0xB5L,255UL},{0x31L,249UL,0UL,0xC5L}},{{251UL,0x1BL,0xC5L,0x50L},{0x6AL,0xF9L,249UL,0xC2L},{0x6AL,255UL,0xC5L,251UL},{251UL,0xC2L,0UL,255UL},{0x31L,0UL,0xB5L,0xE7L},{0xF9L,0xB5L,0xBDL,0xBDL},{255UL,255UL,9UL,0x1BL}},{{0x50L,249UL,0x99L,251UL},{9UL,0x68L,255UL,0x99L},{255UL,0x68L,9UL,251UL},{0x68L,249UL,0xE7L,0x1BL},{1UL,255UL,0x50L,0xBDL},{9UL,0xB5L,255UL,0xE7L},{249UL,0UL,249UL,255UL}},{{0xE7L,0xC2L,0xF9L,251UL},{255UL,255UL,0x0CL,0xC2L},{250UL,0xF9L,0x0CL,0x50L},{255UL,0x1BL,0xF9L,0xC5L},{0xE7L,249UL,249UL,255UL},{249UL,255UL,255UL,0x6AL},{9UL,0x50L,0x50L,9UL}},{{1UL,251UL,0xE7L,0UL},{0x68L,255UL,9UL,249UL},{255UL,0UL,255UL,249UL},{9UL,255UL,0x99L,0UL},{0x50L,251UL,9UL,9UL},{255UL,0x50L,0xBDL,0x6AL},{0xF9L,250UL,251UL,250UL}},{{0UL,249UL,255UL,249UL},{0x50L,255UL,249UL,0x6AL},{0x1BL,0xE7L,249UL,0x68L},{0x1BL,0xB5L,249UL,0x50L},{0x50L,0x68L,255UL,0UL},{0UL,0xBDL,251UL,0x0CL},{0xE7L,251UL,1UL,1UL}}};
                int i, j, k;
                if (((g_771 ^ (0x3D10770F47983A05LL && 0xBD29D7A5FD95DDC8LL)) | ((safe_unary_minus_func_uint32_t_u((safe_mul_func_int16_t_s_s((((((((void*)0 != &g_837) ^ (0UL > p_46)) & ((((*l_565) <= ((safe_sub_func_int8_t_s_s(8L, 1L)) & 0xE6364BF9C1220D09LL)) > 0x08L) < 0UL)) < l_1060) < 0xBCE2F342L) & (*g_1026)), p_48.f3)))) || g_368.f2)))
                { /* block id: 546 */
                    if (p_48.f4)
                        break;
                    l_1063--;
                }
                else
                { /* block id: 549 */
                    int8_t *l_1069 = &l_798[6];
                    uint32_t *l_1071[10] = {&l_851[5],&l_851[5],&l_851[5],&l_851[5],&l_851[5],&l_851[5],&l_851[5],&l_851[5],&l_851[5],&l_851[5]};
                    int32_t l_1077 = 0xF7AF8AECL;
                    int8_t **l_1081[10][4] = {{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069},{&l_1069,&l_1069,&l_1069,&l_1069}};
                    int8_t ** const *l_1080 = &l_1081[4][2];
                    int i, j;
                    if (((safe_sub_func_uint64_t_u_u((0xB931A92CL != (g_795[0] &= ((l_1068 == (**g_601)) <= (0xCE6DL < (((0xF6L & (p_46 && (((*l_1069) ^= 4L) < ((void*)0 == l_1070[0])))) >= g_103) != g_392[1]))))), p_48.f2)) || 1UL))
                    { /* block id: 552 */
                        uint32_t l_1090 = 0xBC784E05L;
                        g_106 = ((safe_div_func_int32_t_s_s(((safe_unary_minus_func_int8_t_s((safe_add_func_uint16_t_u_u((l_1077 == (safe_sub_func_uint32_t_u_u(l_1077, ((void*)0 == l_1080)))), (safe_div_func_int64_t_s_s(((((***g_601) , p_46) <= ((*l_565) &= (((((safe_lshift_func_uint8_t_u_u(p_48.f3, (safe_lshift_func_uint16_t_u_s(p_48.f4, ((safe_sub_func_int64_t_s_s(p_46, 5L)) < 4L))))) == (**g_809)) <= g_833) != 0xD59DL) == p_48.f3))) ^ l_1090), 0x81AFA09E69D72103LL)))))) > l_1090), g_99)) , (void*)0);
                    }
                    else
                    { /* block id: 555 */
                        uint32_t l_1091 = 0x4EFC5643L;
                        uint8_t ***l_1103 = &l_536[1][2];
                        struct S1 l_1104 = {0x3EL,-643,0UL,0x65D629E8L,0x0617L};
                        int32_t l_1105 = (-7L);
                        l_1091--;
                        (*l_565) ^= (p_48.f1 = ((safe_mul_func_uint16_t_u_u((((0x24L || (((((p_48.f0 && ((safe_div_func_int16_t_s_s(((p_46 , g_535.f2) != (0x335CL <= (l_1062 && (((safe_rshift_func_int8_t_s_s(l_1091, 2)) && (safe_unary_minus_func_uint32_t_u(((safe_add_func_int32_t_s_s(((((l_1103 != (l_1104 , (void*)0)) & l_1105) > p_48.f4) != 0x481E26E1L), 0x365C01E4L)) || (*g_1026))))) > l_1106[8][3][1])))), (**g_809))) & 0xD1L)) == p_48.f4) > l_1106[0][3][3]) <= g_120.f0) && 18446744073709551615UL)) >= 0x32B0440AL) ^ l_1106[6][0][3]), (-1L))) > 0xE15FL));
                        (*l_565) = 0x61064E54L;
                    }
                }
                g_932.f2 |= l_1107;
                (*l_565) = 0xB64EE28CL;
            }
            if ((((p_48.f4 = (safe_div_func_uint16_t_u_u(((**g_809) |= (safe_sub_func_int64_t_s_s(p_48.f1, (safe_rshift_func_uint8_t_u_s((safe_add_func_int32_t_s_s(0x077C2E23L, 0x5186E7D3L)), 4))))), (safe_sub_func_int16_t_s_s((((((((*l_1118)--) ^ ((*l_1135) = (((((l_1062 | (safe_mod_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((p_48.f4 >= (g_334 , (safe_mul_func_uint8_t_u_u((((((safe_mod_func_int8_t_s_s((1UL || ((g_1133 = ((safe_sub_func_uint8_t_u_u((((((&l_536[7][1] != (void*)0) , &l_500[1][1]) == (void*)0) | g_885) | l_1062), 255UL)) , (-1L))) != 0x8A908C76C9AB7208LL)), p_48.f2)) >= p_48.f1) , g_103) , (*l_565)) , p_47), l_1062)))), p_48.f2)), p_48.f3))) ^ p_48.f4) == 0x0FL) || p_48.f4) && l_1134))) , (*g_516)) , 255UL) == g_191.f0) | g_120.f1), p_48.f1))))) && g_120.f2) | l_1062))
            { /* block id: 570 */
                int32_t * const l_1152 = &l_500[0][4];
                int32_t l_1156 = 0xC76CA7E0L;
                int32_t l_1158 = 0x1969E597L;
                int32_t l_1159 = 0xC30B42AEL;
                int32_t l_1160 = 0x55AFB9D5L;
                int32_t l_1161[9] = {0L,0L,0L,0L,0L,0L,0L,0L,0L};
                uint16_t l_1162 = 0x27DCL;
                int i;
                for (l_971 = 28; (l_971 == 19); l_971 = safe_sub_func_uint32_t_u_u(l_971, 9))
                { /* block id: 573 */
                    for (g_801 = 0; (g_801 == 51); g_801 = safe_add_func_uint64_t_u_u(g_801, 4))
                    { /* block id: 576 */
                        if (p_49)
                            break;
                    }
                }
                for (l_1107 = (-16); (l_1107 <= 15); l_1107 = safe_add_func_uint32_t_u_u(l_1107, 2))
                { /* block id: 582 */
                    int16_t l_1145[5];
                    int i;
                    for (i = 0; i < 5; i++)
                        l_1145[i] = 0x9F14L;
                    l_1143 = (*g_602);
                    (*l_565) &= l_1145[4];
                    for (p_47 = (-3); (p_47 <= (-5)); --p_47)
                    { /* block id: 587 */
                        uint16_t l_1149 = 65535UL;
                        int32_t **l_1153 = &l_566;
                        if (g_801)
                            goto lbl_1148;
                        l_1149--;
                        (*l_1153) = l_1152;
                        p_48.f1 = p_46;
                    }
                    if ((*l_1152))
                        break;
                }
                l_1162++;
            }
            else
            { /* block id: 596 */
                uint16_t **l_1189 = (void*)0;
                struct S1 *l_1197 = &g_1144;
                if (p_48.f4)
                { /* block id: 597 */
                    for (p_47 = 8; (p_47 != 21); p_47++)
                    { /* block id: 600 */
                        int64_t **l_1168 = &l_1135;
                        l_1168 = l_1167;
                        if (p_48.f2)
                            continue;
                    }
                    if (p_48.f0)
                    { /* block id: 604 */
                        g_106 = &p_49;
                        if ((*g_106))
                            break;
                    }
                    else
                    { /* block id: 607 */
                        int32_t *l_1169 = &l_500[1][1];
                        l_1169 = &l_1157[1];
                        (*l_1169) ^= g_763[0];
                        return p_46;
                    }
                }
                else
                { /* block id: 612 */
                    uint8_t l_1180 = 246UL;
                    const int32_t *l_1181 = &l_1157[1];
                    for (l_439 = (-12); (l_439 != (-11)); ++l_439)
                    { /* block id: 615 */
                        l_1181 = (((p_49 = ((p_48.f1 < (l_1174 && ((void*)0 == &g_810[0]))) == ((g_1144.f2 = ((((~p_48.f3) , ((safe_lshift_func_int8_t_s_s((((safe_mul_func_int16_t_s_s(l_1155, (((*l_565) = (-5L)) ^ (((void*)0 == &l_534) ^ p_46)))) >= 0x55L) , p_48.f0), p_48.f0)) ^ l_1180)) <= 255UL) || p_46)) || 0xDAAB82E2D72926EALL))) >= 0x5D7919B6L) , &g_1050[4][8]);
                        g_106 = (void*)0;
                    }
                    for (g_368.f0 = 5; (g_368.f0 >= 0); g_368.f0 -= 1)
                    { /* block id: 624 */
                        int32_t **l_1184 = (void*)0;
                        const int64_t **l_1188 = &l_419;
                        const int64_t ***l_1187 = &l_1188;
                        int i;
                        (*l_565) = (((l_798[g_368.f0] >= (**g_809)) == (18446744073709551611UL > (((l_1185 = &p_47) != (g_516 = l_1186)) == 0L))) <= ((((p_48.f4 = ((((*l_1135) = (*l_1181)) || ((*l_1118) = ((((g_123.f0 != ((((*l_1187) = (void*)0) == (void*)0) < 0xA320338C89A44F1CLL)) != p_47) | l_1062) , 18446744073709551606UL))) | p_46)) <= 65535UL) > 0x5B6E716E1DCC1F3BLL) == p_48.f0));
                    }
                }
                if ((((g_1190 = l_1189) != l_1192[0][1][0]) >= (safe_lshift_func_uint16_t_u_u(((safe_rshift_func_int16_t_s_s((((l_1197 != (void*)0) < (((p_48.f3 == ((**g_809) = (g_74.f3 , ((void*)0 != l_1200)))) == p_48.f3) , 0UL)) & g_123.f1), p_47)) ^ g_123.f2), p_48.f3))))
                { /* block id: 636 */
                    return p_47;
                }
                else
                { /* block id: 638 */
                    uint16_t **l_1208[10][2][3] = {{{&l_1136[4][0],&g_1191,&l_1136[4][0]},{&l_1136[4][0],&g_1191,&g_1191}},{{&g_1191,&l_1136[4][0],&l_1136[4][0]},{&g_1191,&l_1136[4][0],&l_1136[4][0]}},{{&l_1136[1][0],&g_1191,&g_1191},{&g_1191,&g_1191,&g_1191}},{{&g_1191,&l_1136[1][0],&l_1136[4][0]},{&l_1136[4][0],&g_1191,&l_1136[4][0]}},{{&l_1136[4][0],&g_1191,&g_1191},{&g_1191,&l_1136[4][0],&l_1136[4][0]}},{{&g_1191,&l_1136[4][0],&l_1136[4][0]},{&l_1136[1][0],&g_1191,&g_1191}},{{&g_1191,&g_1191,&g_1191},{&g_1191,&l_1136[1][0],&l_1136[4][0]}},{{&l_1136[4][0],&g_1191,&l_1136[4][0]},{&l_1136[4][0],&g_1191,&g_1191}},{{&g_1191,&l_1136[4][0],&l_1136[4][0]},{&g_1191,&l_1136[4][0],&l_1136[4][0]}},{{&l_1136[1][0],&g_1191,&g_1191},{&g_1191,&g_1191,&g_1191}}};
                    uint32_t *l_1210 = &l_769[1][2][2];
                    int i, j, k;
                    (*l_565) &= (safe_sub_func_int64_t_s_s(p_48.f4, (safe_sub_func_uint32_t_u_u(((*l_1210) = ((&p_48 != l_1207[8]) != ((**g_809) = ((g_1209 = (g_1191 = &l_838[9])) == &p_46)))), 1L))));
                    return p_46;
                }
            }
        }
    }
    return g_795[5];
}


/* ------------------------------------------ */
/* 
 * reads : g_123 g_191.f0 g_205 g_99 g_74.f1 g_150 g_195 g_74 g_152 g_3 g_259 g_295 g_103 g_347 g_191.f2 g_120.f0 g_367 g_368.f4 g_120.f1 g_120.f2 g_143 g_368.f1 g_368 g_392 g_191.f1 g_191.f3 g_294
 * writes: g_237 g_103 g_123.f2 g_195 g_99 g_259 g_62 g_295 g_120.f1 g_74.f2 g_294 g_74.f0 g_347 g_191.f2 g_368.f0 g_392 g_368.f2 g_368.f1
 */
static const int32_t  func_55(const uint32_t  p_56, int16_t  p_57, uint64_t  p_58)
{ /* block id: 66 */
    const int32_t l_228[1][5][8] = {{{0x2C4CA5D1L,(-1L),(-2L),0xA3D568A4L,0xA3D568A4L,(-2L),(-1L),0x2C4CA5D1L},{(-1L),0x2C4CA5D1L,0L,1L,(-1L),(-1L),(-1L),(-6L)},{(-9L),(-1L),0L,(-1L),1L,(-1L),0L,(-1L)},{0x587681F8L,0x2C4CA5D1L,(-6L),0L,(-1L),(-2L),(-1L),0L},{(-1L),(-1L),0xA3D568A4L,(-1L),0x587681F8L,0x587681F8L,(-1L),0xA3D568A4L}}};
    uint8_t *l_232 = &g_101;
    uint8_t *l_236 = (void*)0;
    int32_t l_245 = (-1L);
    struct S0 *l_250 = &g_191;
    uint32_t l_252 = 4294967291UL;
    int32_t *l_253 = &g_99;
    int64_t *l_257 = (void*)0;
    struct S1 *l_266 = &g_74;
    uint16_t *l_305 = &g_103;
    int8_t *l_313 = &g_195;
    uint32_t *l_320 = (void*)0;
    int32_t *l_321 = &l_245;
    uint8_t l_322 = 0xADL;
    const uint8_t l_323 = 252UL;
    int32_t l_337 = (-1L);
    uint32_t l_394 = 1UL;
    int i, j, k;
    if (l_228[0][2][0])
    { /* block id: 67 */
        uint8_t **l_233 = &l_232;
        uint8_t *l_235 = &g_101;
        uint8_t **l_234 = &l_235;
        int32_t *l_238[9] = {(void*)0,(void*)0,&g_99,(void*)0,(void*)0,&g_99,(void*)0,(void*)0,&g_99};
        int i;
        g_123.f2 = (!(safe_add_func_int16_t_s_s((((*l_233) = l_232) != ((*l_234) = &g_101)), (g_103 = ((g_123 , l_236) == (g_237 = l_236))))));
    }
    else
    { /* block id: 73 */
        const uint16_t l_239[10] = {0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL,0xAD5FL};
        int8_t *l_244[3][7][3] = {{{&g_195,(void*)0,(void*)0},{(void*)0,&g_195,&g_195},{&g_195,&g_195,&g_74.f0},{&g_195,&g_195,&g_195},{&g_195,(void*)0,&g_74.f0},{&g_195,&g_74.f0,&g_195},{(void*)0,&g_195,(void*)0}},{{&g_195,&g_195,&g_195},{(void*)0,&g_74.f0,&g_74.f0},{(void*)0,(void*)0,&g_195},{&g_195,&g_195,&g_74.f0},{&g_74.f0,&g_195,&g_195},{&g_195,&g_195,(void*)0},{(void*)0,&g_195,&g_195}},{{(void*)0,(void*)0,&g_195},{&g_195,&g_195,&g_195},{(void*)0,(void*)0,&g_195},{&g_195,&g_195,&g_74.f0},{&g_195,&g_195,&g_195},{&g_195,&g_195,&g_195},{&g_195,&g_195,&g_195}}};
        uint8_t *l_246 = &g_101;
        uint8_t **l_247[6][9][4] = {{{&l_232,&l_236,&g_237,&g_237},{&l_236,(void*)0,&g_237,&g_237},{&l_246,&l_232,(void*)0,&l_236},{&g_237,&l_232,(void*)0,&g_237},{&l_232,(void*)0,&l_246,(void*)0},{&l_246,&g_237,&l_232,&l_232},{&g_237,&l_246,&l_232,&g_237},{&l_246,&l_232,&l_246,(void*)0},{&g_237,(void*)0,&l_246,&l_236}},{{&g_237,&g_237,&g_237,&l_246},{&l_232,&l_236,(void*)0,&l_246},{&l_236,&l_232,&l_232,&l_236},{(void*)0,&l_232,&l_232,&g_237},{&l_236,(void*)0,(void*)0,&g_237},{&l_232,&g_237,&g_237,&l_236},{&g_237,&l_236,&l_246,&g_237},{&g_237,&g_237,&l_246,(void*)0},{&l_246,(void*)0,&l_232,(void*)0}},{{&g_237,&g_237,&l_232,&g_237},{&l_246,&g_237,&l_246,&l_236},{&l_232,&l_246,(void*)0,&l_232},{&g_237,&l_232,(void*)0,&l_246},{&l_246,&l_232,&g_237,&l_232},{&l_236,(void*)0,&g_237,&l_246},{&l_232,(void*)0,(void*)0,&l_246},{&l_232,&l_232,&l_246,&l_246},{&g_237,&l_246,&l_236,&l_232}},{{&l_246,&l_246,(void*)0,(void*)0},{&g_237,&g_237,&g_237,&g_237},{&g_237,&l_232,(void*)0,(void*)0},{(void*)0,(void*)0,&l_232,&l_232},{&l_246,&l_232,&l_232,&l_232},{(void*)0,&l_246,&g_237,&l_232},{&g_237,&l_246,&g_237,&l_232},{&l_246,&l_232,&l_236,&l_232},{&l_236,(void*)0,&l_236,(void*)0}},{{&l_232,&l_232,(void*)0,&g_237},{(void*)0,&g_237,&g_237,(void*)0},{&g_237,&l_246,&l_232,&l_236},{&l_232,&l_236,&g_237,&l_246},{(void*)0,&l_246,(void*)0,(void*)0},{&l_236,(void*)0,&l_232,&l_232},{(void*)0,(void*)0,(void*)0,(void*)0},{&l_236,&l_232,&g_237,&g_237},{&l_236,&l_246,&l_232,&l_246}},{{(void*)0,&l_246,&g_237,(void*)0},{(void*)0,(void*)0,&l_236,&l_232},{&l_232,(void*)0,&g_237,&l_246},{&l_232,(void*)0,&g_237,(void*)0},{&l_232,&l_246,&l_232,&g_237},{&l_246,&l_232,&l_232,&l_232},{&l_246,&l_246,&l_246,&l_246},{&l_232,&l_246,&g_237,&l_232},{&g_237,&l_232,&l_232,(void*)0}}};
        struct S0 l_251 = {18,15,-18,82};
        int i, j, k;
        l_252 |= ((l_239[0] , (safe_rshift_func_uint16_t_u_u((((g_191.f0 && (safe_mul_func_int8_t_s_s((g_195 = g_205), (l_245 , p_56)))) , l_246) != (g_237 = &g_101)), 8))) , (safe_mul_func_uint8_t_u_u((l_250 != (l_251 , (void*)0)), l_251.f0)));
    }
    (*l_253) &= p_56;
    for (p_57 = 0; (p_57 == 27); ++p_57)
    { /* block id: 81 */
        int16_t l_256 = 0L;
        int64_t *l_258 = &g_152[2][2][1];
        struct S1 *l_267[5][9][5] = {{{(void*)0,&g_74,&g_74,(void*)0,&g_74},{&g_74,(void*)0,(void*)0,(void*)0,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,(void*)0,&g_74,&g_74},{(void*)0,(void*)0,(void*)0,&g_74,&g_74},{&g_74,(void*)0,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,(void*)0},{&g_74,(void*)0,&g_74,&g_74,(void*)0}},{{(void*)0,&g_74,&g_74,&g_74,&g_74},{(void*)0,&g_74,(void*)0,&g_74,(void*)0},{&g_74,&g_74,(void*)0,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,(void*)0,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,(void*)0},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,(void*)0,&g_74}},{{&g_74,(void*)0,&g_74,&g_74,(void*)0},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,(void*)0,&g_74,(void*)0,&g_74},{(void*)0,&g_74,&g_74,(void*)0,(void*)0},{&g_74,&g_74,&g_74,(void*)0,&g_74},{&g_74,&g_74,&g_74,(void*)0,&g_74},{&g_74,&g_74,&g_74,(void*)0,(void*)0},{&g_74,&g_74,&g_74,(void*)0,&g_74},{&g_74,&g_74,(void*)0,(void*)0,&g_74}},{{&g_74,(void*)0,&g_74,&g_74,(void*)0},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,(void*)0,(void*)0,&g_74},{&g_74,&g_74,&g_74,&g_74,(void*)0},{&g_74,(void*)0,&g_74,&g_74,&g_74},{&g_74,(void*)0,&g_74,(void*)0,&g_74},{&g_74,&g_74,(void*)0,&g_74,(void*)0},{&g_74,&g_74,&g_74,&g_74,&g_74},{&g_74,&g_74,(void*)0,(void*)0,&g_74}},{{&g_74,(void*)0,&g_74,&g_74,(void*)0},{(void*)0,&g_74,&g_74,&g_74,(void*)0},{&g_74,&g_74,&g_74,(void*)0,(void*)0},{&g_74,&g_74,(void*)0,&g_74,(void*)0},{&g_74,&g_74,&g_74,&g_74,(void*)0},{&g_74,&g_74,&g_74,(void*)0,(void*)0},{(void*)0,&g_74,(void*)0,&g_74,(void*)0},{(void*)0,(void*)0,&g_74,&g_74,&g_74},{&g_74,&g_74,&g_74,(void*)0,&g_74}}};
        int32_t *l_269 = &g_62;
        uint8_t l_278 = 0x46L;
        int32_t *l_279 = (void*)0;
        int32_t *l_280 = &l_245;
        int32_t *l_281 = &g_99;
        int32_t *l_282 = &l_245;
        int32_t *l_283 = (void*)0;
        int32_t *l_284 = (void*)0;
        int32_t *l_285 = &g_99;
        int32_t *l_286 = (void*)0;
        int32_t *l_287 = &l_245;
        int32_t *l_288 = &l_245;
        int32_t *l_289 = (void*)0;
        int32_t *l_290 = &l_245;
        int32_t *l_291 = &l_245;
        int32_t *l_292 = (void*)0;
        int32_t *l_293[6] = {&g_99,&g_99,&g_99,&g_99,&g_99,&g_99};
        int i, j, k;
        if (l_256)
            break;
        (*l_253) = (((((((&g_190 == (void*)0) != ((l_257 != (g_259 = l_258)) || p_58)) , ((*l_269) = (safe_sub_func_uint16_t_u_u((((g_74.f1 && (((safe_add_func_int8_t_s_s((safe_unary_minus_func_int16_t_s(((((!(l_266 != l_267[2][1][3])) | (+0x5F32L)) || 0xD5L) , p_58))), p_57)) ^ l_256) || p_56)) , g_150[5][4]) ^ l_256), g_123.f1)))) , g_195) >= (*l_253)) ^ 0UL) <= 18446744073709551614UL);
        (*l_280) = (0UL ^ (safe_sub_func_uint64_t_u_u((((*l_253) = ((g_74 , (&l_253 == (void*)0)) ^ (((safe_lshift_func_uint8_t_u_u((((((safe_add_func_int32_t_s_s(1L, ((safe_lshift_func_int8_t_s_u((((*l_266) , ((((((g_152[0][2][0] < (0x97L != l_256)) || g_3) ^ (*l_253)) <= g_74.f2) , (*l_253)) , p_58)) < l_256), 7)) >= l_278))) <= 6UL) != 0x326F7BA14FACFC06LL) | p_57) && 0x33217787L), (*l_253))) ^ 0x1976L) > 0xD345E9BEFF2D9E75LL))) & 0xBF0B190BL), (*g_259))));
        g_295++;
    }
    if ((((safe_sub_func_int8_t_s_s((safe_add_func_uint64_t_u_u((g_74.f2 = (((*l_321) = ((g_120.f1 = ((~18446744073709551608UL) == ((safe_div_func_uint16_t_u_u(((*l_305)++), ((0x2BL == (((+(((*l_253) = ((p_57 , (*l_253)) != (safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s(((l_313 = (void*)0) == (void*)0), 4)), ((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_s(1L, 10)) || p_58), (safe_add_func_uint64_t_u_u(0x67A07350B06B825BLL, (*l_253))))) != g_191.f0))))) && (*l_253))) && 3UL) ^ 1UL)) || (*l_253)))) , p_58))) == p_56)) , p_58)), 0xFDDDD462EF824573LL)), l_322)) != l_323) && (*l_253)))
    { /* block id: 96 */
        int32_t *l_324[5][2][8] = {{{&g_99,(void*)0,(void*)0,(void*)0,&g_99,(void*)0,(void*)0,&l_245},{(void*)0,&g_99,(void*)0,(void*)0,&l_245,&l_245,&g_99,(void*)0}},{{&l_245,(void*)0,&l_245,(void*)0,&g_99,(void*)0,&l_245,(void*)0},{(void*)0,&l_245,(void*)0,&g_99,(void*)0,&g_99,&l_245,&g_99}},{{&l_245,(void*)0,&l_245,&g_99,(void*)0,&l_245,&l_245,&l_245},{&g_99,&g_99,(void*)0,&l_245,&g_99,(void*)0,&l_245,&l_245}},{{&g_99,(void*)0,&l_245,&l_245,&l_245,&l_245,&l_245,&l_245},{(void*)0,(void*)0,(void*)0,(void*)0,&l_245,(void*)0,&g_99,&l_245}},{{(void*)0,&l_245,&g_99,(void*)0,&g_99,&l_245,&l_245,&g_99},{(void*)0,&l_245,&g_99,&l_245,&l_245,(void*)0,&g_99,(void*)0}}};
        int32_t **l_325[4] = {&l_324[3][0][3],&l_324[3][0][3],&l_324[3][0][3],&l_324[3][0][3]};
        int i, j, k;
        l_321 = l_324[3][1][5];
        for (p_57 = 4; (p_57 == 9); p_57++)
        { /* block id: 100 */
            uint16_t l_339 = 5UL;
            int32_t l_349 = 0x74697E7AL;
            uint8_t *l_361 = (void*)0;
            int8_t l_363 = 0x7AL;
            struct S1 *l_369 = &g_368;
            int32_t *l_383 = &g_62;
            struct S0 ** const l_405 = (void*)0;
            for (g_195 = (-24); (g_195 <= (-17)); g_195 = safe_add_func_int64_t_s_s(g_195, 4))
            { /* block id: 103 */
                int32_t l_348 = 0L;
                int32_t l_370 = 0xE5EF0ED2L;
                uint16_t * const l_393 = (void*)0;
                int32_t **l_406 = (void*)0;
                for (g_294 = 0; (g_294 <= 29); ++g_294)
                { /* block id: 106 */
                    l_321 = &g_99;
                }
                if (p_57)
                { /* block id: 109 */
                    for (g_74.f0 = 0; (g_74.f0 >= (-19)); --g_74.f0)
                    { /* block id: 112 */
                        int16_t l_335 = (-10L);
                        int32_t l_336 = 6L;
                        int32_t l_338 = 0x5114C951L;
                        l_339++;
                    }
                }
                else
                { /* block id: 115 */
                    int8_t *l_344 = (void*)0;
                    int8_t *l_345 = &g_74.f0;
                    int8_t *l_346 = &g_347;
                    uint8_t **l_362 = &l_232;
                    int32_t l_364[4][8] = {{0x030F7A79L,1L,2L,2L,1L,0x030F7A79L,1L,2L},{2L,1L,2L,0x030F7A79L,0x030F7A79L,2L,1L,2L},{(-3L),0x030F7A79L,2L,0x030F7A79L,(-3L),(-3L),0x030F7A79L,2L},{(-3L),(-3L),0x030F7A79L,2L,0x030F7A79L,(-3L),(-3L),0x030F7A79L}};
                    int8_t l_371[1][1];
                    int8_t *l_372 = &l_371[0][0];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_371[i][j] = 0x66L;
                    }
                    g_191.f2 = (l_349 = ((safe_mul_func_uint8_t_u_u(0xD1L, ((*l_346) &= ((*l_253) = ((*l_345) = 3L))))) || (l_348 | (l_339 == 0x46L))));
                    if ((p_58 , (safe_div_func_int16_t_s_s((((((safe_add_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((g_191.f2 >= ((*l_346) = (g_74.f0 = g_120.f0))), 7)), ((safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint16_t_u_u((p_57 != (((l_364[2][1] = ((((*l_362) = (g_237 = l_361)) != (void*)0) & (l_363 ^= 6UL))) && (0UL | ((*l_372) = ((safe_mod_func_int16_t_s_s((l_370 = (g_367 != l_369)), p_57)) || l_371[0][0])))) && g_191.f0)), 13)))) | g_368.f4))), l_348)) , (*l_253)) ^ g_123.f2) == g_120.f1) && g_120.f1), g_120.f2))))
                    { /* block id: 129 */
                        int16_t *l_377 = (void*)0;
                        int32_t l_380 = (-1L);
                        int32_t l_381 = (-9L);
                        int32_t *l_382[10] = {&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348,&l_348};
                        int16_t *l_391 = &g_392[1];
                        uint64_t *l_395 = &g_368.f2;
                        int i;
                        l_381 ^= (((((l_364[2][1] = (safe_lshift_func_int8_t_s_s((g_103 & (g_120.f2 ^ ((void*)0 != &g_190))), 7))) != g_143) != ((g_368.f0 = g_347) < (*l_253))) == ((((safe_sub_func_uint16_t_u_u(((((l_380 = 1UL) > l_371[0][0]) != g_368.f1) && g_150[9][0]), l_363)) , 0x98L) != l_348) ^ 1UL)) > p_57);
                        (*l_253) = ((((l_382[3] == l_383) , ((+g_123.f2) && 0xA8E03410L)) > ((void*)0 == &l_370)) & ((((((*g_367) , (safe_mul_func_int16_t_s_s(((*l_391) |= (g_294 = (safe_sub_func_int64_t_s_s(((safe_lshift_func_uint16_t_u_s(g_123.f3, (l_363 >= 0xE314L))) | g_120.f1), l_370)))), p_57))) , &g_103) != l_393) >= l_394) != 0xFC46E637L));
                        g_368.f1 ^= ((((--(*l_395)) || p_56) , ((*l_305) ^= 0x8DBAL)) == ((safe_rshift_func_uint8_t_u_s((safe_div_func_uint64_t_u_u((safe_unary_minus_func_uint8_t_u((safe_add_func_uint8_t_u_u(((((l_370 = (l_405 == &g_190)) & ((void*)0 == l_406)) || (g_191.f1 <= p_57)) & ((((*l_391) = (safe_rshift_func_uint16_t_u_s((((safe_rshift_func_int8_t_s_u((((safe_div_func_int16_t_s_s(((0x01A0L >= g_123.f1) >= 5L), g_368.f3)) > 0x8CE3L) , g_191.f3), 7)) , l_305) == &g_196), 14))) <= p_58) & g_123.f2)), l_363)))), g_294)), 3)) && g_191.f0));
                    }
                    else
                    { /* block id: 142 */
                        if (p_58)
                            break;
                    }
                }
            }
        }
    }
    else
    { /* block id: 148 */
        struct S1 **l_413 = &g_367;
        struct S1 ***l_414 = &l_413;
        (*l_414) = l_413;
    }
    return (*l_253);
}


/* ------------------------------------------ */
/* 
 * reads : g_99 g_205 g_74.f1 g_74 g_123.f2
 * writes: g_99 g_205 g_152 g_195 g_74.f0 g_106
 */
static uint16_t  func_59(const int16_t  p_60, int32_t  p_61)
{ /* block id: 57 */
    uint64_t l_200 = 0UL;
    int32_t *l_201 = &g_99;
    int32_t *l_202 = &g_99;
    int32_t *l_203 = &g_99;
    int32_t *l_204[1];
    int32_t l_208 = 0xD266F9F2L;
    struct S0 l_213 = {159,14,22,87};
    int64_t *l_226 = &g_152[2][1][0];
    int8_t *l_227 = &g_195;
    int i;
    for (i = 0; i < 1; i++)
        l_204[i] = &g_99;
    (*l_201) ^= l_200;
    g_205++;
    l_208 &= (p_61 == (*l_201));
    g_106 = ((g_74.f0 = (p_60 ^ (((*l_227) = ((p_60 | g_74.f1) < (safe_mul_func_int16_t_s_s((safe_div_func_int64_t_s_s(((*l_226) = (l_213 , ((g_74 , p_60) & (safe_rshift_func_uint16_t_u_u(((*l_203) >= (safe_sub_func_uint8_t_u_u((safe_rshift_func_uint8_t_u_u((safe_div_func_int8_t_s_s((*l_202), (safe_lshift_func_int16_t_s_u((safe_div_func_uint32_t_u_u(p_61, g_74.f3)), 11)))), 6)), p_61))), p_61))))), p_61)), (-1L))))) != p_61))) , (void*)0);
    return g_123.f2;
}


/* ------------------------------------------ */
/* 
 * reads : g_74 g_3 g_101 g_103 g_99 g_143 g_120.f0 g_120.f3 g_106 g_123 g_152 g_150 g_196
 * writes: g_101 g_103 g_106 g_74.f2 g_143 g_150 g_152 g_74.f4 g_99 g_190 g_196
 */
static int32_t  func_69(int32_t  p_70, struct S1  p_71, struct S1  p_72, struct S0  p_73)
{ /* block id: 13 */
    int8_t l_89[10];
    int32_t *l_98[2][9] = {{&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99},{&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99}};
    uint8_t *l_100[10] = {(void*)0,&g_101,&g_101,&g_101,&g_101,(void*)0,&g_101,&g_101,&g_101,&g_101};
    uint16_t *l_102 = &g_103;
    int32_t l_116 = 0x4CBE064EL;
    struct S0 *l_119 = &g_120;
    int16_t l_194[4] = {0xC1F0L,0xC1F0L,0xC1F0L,0xC1F0L};
    int i, j;
    for (i = 0; i < 10; i++)
        l_89[i] = 1L;
    if ((safe_div_func_int32_t_s_s((safe_rshift_func_int16_t_s_u((((safe_sub_func_uint8_t_u_u(0x2AL, (safe_lshift_func_uint8_t_u_s(p_72.f1, 0)))) , (safe_mod_func_uint8_t_u_u(((-7L) ^ p_73.f0), (g_74 , (safe_rshift_func_int16_t_s_u((g_74.f1 <= l_89[9]), 3)))))) ^ ((((*l_102) = ((((safe_div_func_uint32_t_u_u((((safe_mod_func_uint64_t_u_u(7UL, (((g_101 = (safe_mod_func_int32_t_s_s((p_70 = ((safe_sub_func_uint32_t_u_u((p_72 , l_89[9]), 0x8C6C4233L)) <= g_74.f1)), p_71.f1))) == g_74.f3) | (-1L)))) , p_73.f3) && g_74.f2), 7L)) > 65535UL) , g_74.f1) && 0x81FFL)) != g_74.f2) <= 0xA7320B692EDFCA59LL)), 15)), g_3)))
    { /* block id: 17 */
        int32_t l_115[1];
        struct S0 *l_122 = &g_123;
        int32_t l_184[10][9][2] = {{{0xDD16A1DDL,(-1L)},{0x057B9547L,(-1L)},{0xABEE1543L,0x2A7E1DD4L},{5L,0xE5037CFCL},{0xE5037CFCL,0x31DCF326L},{4L,(-3L)},{0xBA1A25AEL,(-1L)},{2L,0xCC87C933L},{3L,(-1L)}},{{1L,0xAF8F730DL},{8L,0L},{(-1L),(-8L)},{0x1DCDCBB9L,0x2CAAA7E9L},{0L,(-1L)},{0L,(-1L)},{0x2CAAA7E9L,0x0F0A5B65L},{0x0BFB30C4L,9L},{(-4L),0x293352AEL}},{{0x21F5EDDBL,0x378E477AL},{0xACC334F8L,5L},{6L,0x3CDDDF83L},{(-1L),(-1L)},{0xAF8F730DL,0xEEE56F2AL},{(-8L),8L},{0x293352AEL,4L},{0x3CDDDF83L,0xF5B1C491L},{0xC1009BDDL,1L}},{{1L,0xACC334F8L},{2L,(-1L)},{0L,1L},{(-1L),(-9L)},{8L,3L},{5L,0x21F5EDDBL},{(-3L),1L},{(-1L),1L},{(-3L),0x21F5EDDBL}},{{5L,3L},{8L,(-9L)},{0L,1L},{(-4L),0x165E96A0L},{(-1L),9L},{0x293352AEL,0x2CAAA7E9L},{0x9211E982L,9L},{1L,0x0BFB30C4L},{8L,4L}},{{0xABEE1543L,0xBCE19DEDL},{(-9L),(-1L)},{0xDAF1BBE7L,1L},{0L,3L},{9L,0xE5037CFCL},{(-4L),8L},{0xB982E971L,0x21F5EDDBL},{0xDD16A1DDL,8L},{1L,3L}},{{0L,0L},{(-1L),1L},{5L,0xABEE1543L},{0xACC334F8L,(-4L)},{0x31DCF326L,(-9L)},{3L,(-1L)},{0x411A6625L,1L},{0xA2CA1CAAL,0L},{6L,(-1L)}},{{0x0BFB30C4L,0x2A7E1DD4L},{0x0F0A5B65L,0x0F0A5B65L},{3L,2L},{0x3CDDDF83L,0xACC334F8L},{5L,0xDAF1BBE7L},{(-1L),5L},{(-5L),0xBA1A25AEL},{(-5L),5L},{(-1L),0xDAF1BBE7L}},{{5L,0xACC334F8L},{0x3CDDDF83L,2L},{3L,0x0F0A5B65L},{0x0F0A5B65L,0x2A7E1DD4L},{0x0BFB30C4L,(-1L)},{6L,0L},{0xA2CA1CAAL,1L},{0x411A6625L,(-1L)},{3L,(-9L)}},{{0x31DCF326L,(-4L)},{0xACC334F8L,0xABEE1543L},{5L,1L},{(-1L),0L},{0L,3L},{1L,8L},{0xDD16A1DDL,0x21F5EDDBL},{0xB982E971L,8L},{(-4L),0xE5037CFCL}}};
        struct S0 **l_189 = (void*)0;
        int32_t **l_192[4];
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_115[i] = 1L;
        for (i = 0; i < 4; i++)
            l_192[i] = &l_98[0][3];
        for (g_101 = 0; (g_101 <= 9); g_101 += 1)
        { /* block id: 20 */
            int32_t **l_104 = (void*)0;
            int32_t **l_105[7] = {&l_98[1][7],&l_98[1][7],(void*)0,&l_98[1][7],&l_98[1][7],(void*)0,&l_98[1][7]};
            uint32_t l_174 = 0UL;
            int i;
            g_106 = (void*)0;
            for (g_103 = 0; (g_103 <= 9); g_103 += 1)
            { /* block id: 24 */
                struct S1 *l_188 = &g_74;
                struct S1 **l_187 = &l_188;
                int i;
                p_72.f1 ^= ((((safe_lshift_func_int8_t_s_s((l_89[g_103] ^ g_99), 6)) <= (~(p_71.f1 = ((safe_sub_func_int32_t_s_s(l_89[g_103], ((!p_72.f3) , ((p_73.f2 || ((((safe_add_func_uint64_t_u_u(g_74.f0, (p_72.f0 < g_74.f3))) , l_115[0]) , l_115[0]) >= g_74.f4)) <= l_116)))) | g_99)))) || p_73.f3) | 0xEDL);
                for (g_74.f2 = 0; (g_74.f2 <= 3); g_74.f2++)
                { /* block id: 29 */
                    struct S0 **l_121 = (void*)0;
                    uint64_t *l_148 = &g_74.f2;
                    int32_t l_153 = 0L;
                    if (p_71.f2)
                        break;
                    l_122 = l_119;
                    p_71.f1 = 0x7C5F07B8L;
                    for (p_72.f0 = (-24); (p_72.f0 >= 28); p_72.f0++)
                    { /* block id: 35 */
                        uint64_t *l_142 = &g_143;
                        uint64_t *l_149 = &g_150[0][3];
                        int64_t *l_151 = &g_152[2][2][1];
                        uint16_t *l_154 = &g_74.f4;
                        int32_t l_155[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_155[i] = 0x1BC1FB24L;
                        g_106 = &g_99;
                        (*g_106) = (safe_rshift_func_uint8_t_u_s((safe_div_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(((*l_154) = ((safe_mul_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s((((*l_151) = (safe_mod_func_uint64_t_u_u((&l_98[0][5] != &l_98[1][3]), ((*l_149) = (((g_103 , (((safe_mod_func_uint64_t_u_u(1UL, 0xEB709D1F70EA76F4LL)) ^ 0L) , ((*l_142)--))) > 0x48FAEAFAE61C728FLL) | (safe_div_func_int8_t_s_s((l_148 == (void*)0), 0x0FL))))))) | p_73.f2), 2UL)) || g_120.f0), l_153)) ^ p_71.f4)), 1UL)), g_120.f3)), l_155[0]));
                        return p_73.f1;
                    }
                }
                (*l_187) = ((safe_lshift_func_int8_t_s_s((safe_mul_func_int8_t_s_s(((-1L) ^ (~((((safe_lshift_func_int8_t_s_u((safe_mod_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s((((safe_mod_func_int8_t_s_s(((~((g_123 , (((safe_mul_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((l_174 = l_115[0]), (safe_unary_minus_func_uint32_t_u((safe_add_func_int32_t_s_s(((g_99 == (safe_mul_func_int8_t_s_s(p_72.f1, ((safe_mod_func_int16_t_s_s((((p_71.f0 < (safe_div_func_uint64_t_u_u((l_184[1][7][1] = l_115[0]), 0xA7DBD6090396A4FFLL))) | (safe_rshift_func_int8_t_s_u(((((p_73 , (void*)0) != &g_123) , g_152[2][3][1]) , 4L), 2))) , p_71.f1), p_72.f4)) != l_115[0])))) && p_72.f3), 4294967294UL)))))), g_74.f1)) , l_184[1][7][1]) > 0x48255A9511BF24C8LL)) > p_71.f3)) | p_71.f4), l_89[g_103])) & g_150[0][3]) >= g_123.f0), 4)) , p_71.f2), p_72.f2)), 4)) , p_73.f0) >= g_3) != 0x34227FBCL))), p_71.f2)), l_115[0])) , (void*)0);
            }
        }
        g_190 = &p_73;
        l_98[0][5] = &g_99;
    }
    else
    { /* block id: 52 */
        p_72.f1 ^= (~((*g_106) , (-7L)));
    }
    g_196++;
    return p_72.f1;
}




/* ---------------------------------------- */
//testcase_id 1484153229
int case1484153229(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_74.f0, "g_74.f0", print_hash_value);
    transparent_crc(g_74.f1, "g_74.f1", print_hash_value);
    transparent_crc(g_74.f2, "g_74.f2", print_hash_value);
    transparent_crc(g_74.f3, "g_74.f3", print_hash_value);
    transparent_crc(g_74.f4, "g_74.f4", print_hash_value);
    transparent_crc(g_99, "g_99", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_120.f0, "g_120.f0", print_hash_value);
    transparent_crc(g_120.f1, "g_120.f1", print_hash_value);
    transparent_crc(g_120.f2, "g_120.f2", print_hash_value);
    transparent_crc(g_120.f3, "g_120.f3", print_hash_value);
    transparent_crc(g_123.f0, "g_123.f0", print_hash_value);
    transparent_crc(g_123.f1, "g_123.f1", print_hash_value);
    transparent_crc(g_123.f2, "g_123.f2", print_hash_value);
    transparent_crc(g_123.f3, "g_123.f3", print_hash_value);
    transparent_crc(g_143, "g_143", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_150[i][j], "g_150[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_152[i][j][k], "g_152[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_191.f0, "g_191.f0", print_hash_value);
    transparent_crc(g_191.f1, "g_191.f1", print_hash_value);
    transparent_crc(g_191.f2, "g_191.f2", print_hash_value);
    transparent_crc(g_191.f3, "g_191.f3", print_hash_value);
    transparent_crc(g_195, "g_195", print_hash_value);
    transparent_crc(g_196, "g_196", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    transparent_crc(g_294, "g_294", print_hash_value);
    transparent_crc(g_295, "g_295", print_hash_value);
    transparent_crc(g_334, "g_334", print_hash_value);
    transparent_crc(g_347, "g_347", print_hash_value);
    transparent_crc(g_368.f0, "g_368.f0", print_hash_value);
    transparent_crc(g_368.f1, "g_368.f1", print_hash_value);
    transparent_crc(g_368.f2, "g_368.f2", print_hash_value);
    transparent_crc(g_368.f3, "g_368.f3", print_hash_value);
    transparent_crc(g_368.f4, "g_368.f4", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_392[i], "g_392[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_489, "g_489", print_hash_value);
    transparent_crc(g_535.f0, "g_535.f0", print_hash_value);
    transparent_crc(g_535.f1, "g_535.f1", print_hash_value);
    transparent_crc(g_535.f2, "g_535.f2", print_hash_value);
    transparent_crc(g_535.f3, "g_535.f3", print_hash_value);
    transparent_crc(g_553, "g_553", print_hash_value);
    transparent_crc(g_558, "g_558", print_hash_value);
    transparent_crc(g_596, "g_596", print_hash_value);
    transparent_crc(g_662, "g_662", print_hash_value);
    transparent_crc(g_710, "g_710", print_hash_value);
    transparent_crc(g_711, "g_711", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_763[i], "g_763[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_771, "g_771", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_795[i], "g_795[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_801, "g_801", print_hash_value);
    transparent_crc(g_821, "g_821", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_830[i], "g_830[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_832, "g_832", print_hash_value);
    transparent_crc(g_833, "g_833", print_hash_value);
    transparent_crc(g_837, "g_837", print_hash_value);
    transparent_crc(g_885, "g_885", print_hash_value);
    transparent_crc(g_932.f0, "g_932.f0", print_hash_value);
    transparent_crc(g_932.f1, "g_932.f1", print_hash_value);
    transparent_crc(g_932.f2, "g_932.f2", print_hash_value);
    transparent_crc(g_932.f3, "g_932.f3", print_hash_value);
    transparent_crc(g_936, "g_936", print_hash_value);
    transparent_crc(g_972, "g_972", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_1046[i][j][k], "g_1046[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1050[i][j], "g_1050[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1133, "g_1133", print_hash_value);
    transparent_crc(g_1144.f0, "g_1144.f0", print_hash_value);
    transparent_crc(g_1144.f1, "g_1144.f1", print_hash_value);
    transparent_crc(g_1144.f2, "g_1144.f2", print_hash_value);
    transparent_crc(g_1144.f3, "g_1144.f3", print_hash_value);
    transparent_crc(g_1144.f4, "g_1144.f4", print_hash_value);
    transparent_crc(g_1154, "g_1154", print_hash_value);
    transparent_crc(g_1270, "g_1270", print_hash_value);
    transparent_crc(g_1274, "g_1274", print_hash_value);
    transparent_crc(g_1287, "g_1287", print_hash_value);
    transparent_crc(g_1289.f0, "g_1289.f0", print_hash_value);
    transparent_crc(g_1289.f1, "g_1289.f1", print_hash_value);
    transparent_crc(g_1289.f2, "g_1289.f2", print_hash_value);
    transparent_crc(g_1289.f3, "g_1289.f3", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_1565[i][j], "g_1565[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1646.f0, "g_1646.f0", print_hash_value);
    transparent_crc(g_1646.f1, "g_1646.f1", print_hash_value);
    transparent_crc(g_1646.f2, "g_1646.f2", print_hash_value);
    transparent_crc(g_1646.f3, "g_1646.f3", print_hash_value);
    transparent_crc(g_1646.f4, "g_1646.f4", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1675[i], "g_1675[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1714.f0, "g_1714.f0", print_hash_value);
    transparent_crc(g_1714.f1, "g_1714.f1", print_hash_value);
    transparent_crc(g_1714.f2, "g_1714.f2", print_hash_value);
    transparent_crc(g_1714.f3, "g_1714.f3", print_hash_value);
    transparent_crc(g_1741, "g_1741", print_hash_value);
    transparent_crc(g_1754, "g_1754", print_hash_value);
    transparent_crc(g_1980, "g_1980", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 503
   depth: 1, occurrence: 24
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 5
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 2
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 76
breakdown:
   indirect level: 0, occurrence: 24
   indirect level: 1, occurrence: 25
   indirect level: 2, occurrence: 16
   indirect level: 3, occurrence: 11
XXX full-bitfields structs in the program: 8
breakdown:
   indirect level: 0, occurrence: 8
XXX times a bitfields struct's address is taken: 56
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 42
XXX times a single bitfield on LHS: 29
XXX times a single bitfield on RHS: 110

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 254
   depth: 2, occurrence: 57
   depth: 3, occurrence: 5
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
   depth: 6, occurrence: 1
   depth: 8, occurrence: 2
   depth: 9, occurrence: 2
   depth: 10, occurrence: 4
   depth: 13, occurrence: 2
   depth: 14, occurrence: 1
   depth: 15, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 4
   depth: 18, occurrence: 1
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 3
   depth: 24, occurrence: 2
   depth: 27, occurrence: 2
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 1
   depth: 36, occurrence: 1
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 465

XXX times a variable address is taken: 1273
XXX times a pointer is dereferenced on RHS: 227
breakdown:
   depth: 1, occurrence: 180
   depth: 2, occurrence: 43
   depth: 3, occurrence: 4
XXX times a pointer is dereferenced on LHS: 277
breakdown:
   depth: 1, occurrence: 260
   depth: 2, occurrence: 17
XXX times a pointer is compared with null: 47
XXX times a pointer is compared with address of another variable: 13
XXX times a pointer is compared with another pointer: 12
XXX times a pointer is qualified to be dereferenced: 5174

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1046
   level: 2, occurrence: 175
   level: 3, occurrence: 18
   level: 4, occurrence: 1
XXX number of pointers point to pointers: 197
XXX number of pointers point to scalars: 241
XXX number of pointers point to structs: 27
XXX percent of pointers has null in alias set: 31.4
XXX average alias set size: 1.52

XXX times a non-volatile is read: 1668
XXX times a non-volatile is write: 902
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 237
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 37
   depth: 1, occurrence: 31
   depth: 2, occurrence: 27
   depth: 3, occurrence: 34
   depth: 4, occurrence: 45
   depth: 5, occurrence: 63

XXX percentage a fresh-made variable is used: 19.5
XXX percentage an existing variable is used: 80.5
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/

