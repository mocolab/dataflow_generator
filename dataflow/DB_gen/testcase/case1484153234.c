/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      3946178542
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   uint32_t  f0;
   const signed f1 : 2;
   uint32_t  f2;
   int32_t  f3;
};
#pragma pack(pop)

struct S1 {
   signed f0 : 30;
   unsigned f1 : 15;
   signed f2 : 31;
   const uint32_t  f3;
   const unsigned f4 : 17;
};

#pragma pack(push)
#pragma pack(1)
struct S2 {
   unsigned f0 : 15;
   const unsigned f1 : 14;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S3 {
   unsigned f0 : 26;
   unsigned : 0;
   signed f1 : 24;
   signed f2 : 19;
   signed f3 : 28;
   unsigned f4 : 30;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static const int16_t g_16 = (-1L);
static struct S0 g_21 = {18446744073709551612UL,-1,0xA28C7FFFL,-1L};
static int8_t g_36 = 6L;
static int32_t g_38 = 5L;
static uint8_t g_45 = 0xE1L;
static struct S3 g_52 = {2948,-126,-569,4982,28070};
static int32_t g_100 = 0L;
static int32_t *g_99 = &g_100;
static int16_t g_111 = 0L;
static uint32_t g_119 = 4294967291UL;
static uint64_t g_123[1][7][2] = {{{1UL,0x45F4D98BCDAFCE28LL},{0x45F4D98BCDAFCE28LL,1UL},{0x45F4D98BCDAFCE28LL,0x45F4D98BCDAFCE28LL},{1UL,0x45F4D98BCDAFCE28LL},{0x45F4D98BCDAFCE28LL,1UL},{0x45F4D98BCDAFCE28LL,0x45F4D98BCDAFCE28LL},{1UL,0x45F4D98BCDAFCE28LL}}};
static struct S1 g_130 = {22709,26,15299,4294967294UL,134};
static struct S2 g_143[3] = {{106,21},{106,21},{106,21}};
static int64_t g_148 = 0x66756846699B53D0LL;
static int32_t g_150 = (-1L);
static int32_t g_175 = 2L;
static int8_t g_221 = 0x4DL;
static int32_t g_258 = 0x67ABC463L;
static int8_t g_259 = 0x7DL;
static int16_t g_260 = (-1L);
static int32_t g_262 = 0xCBCB859EL;
static uint32_t g_265[5][5] = {{0x346CA3F7L,0x8DD2CD65L,0x8DD2CD65L,0x346CA3F7L,0x5456AB62L},{0x0D80B1D0L,0x85A30D52L,0x85A30D52L,0x0D80B1D0L,0xE503AC01L},{0x346CA3F7L,0x8DD2CD65L,0x8DD2CD65L,0x346CA3F7L,0x5456AB62L},{0x0D80B1D0L,0x85A30D52L,0x85A30D52L,0x0D80B1D0L,0xE503AC01L},{0x346CA3F7L,0x8DD2CD65L,0x8DD2CD65L,0x346CA3F7L,0x5456AB62L}};
static uint8_t g_279 = 250UL;
static int8_t g_293 = 0x03L;
static uint32_t g_294 = 4294967295UL;
static int16_t g_303 = 0x0BC7L;
static struct S3 g_314 = {5653,-3551,708,3547,827};
static struct S3 *g_313 = &g_314;
static const struct S3 *g_316 = &g_314;
static const struct S3 **g_315 = &g_316;
static uint32_t *g_335 = &g_21.f2;
static uint32_t **g_334[3][2][6] = {{{&g_335,&g_335,(void*)0,(void*)0,&g_335,&g_335},{&g_335,(void*)0,&g_335,&g_335,&g_335,&g_335}},{{&g_335,&g_335,&g_335,(void*)0,&g_335,&g_335},{&g_335,(void*)0,(void*)0,&g_335,&g_335,&g_335}},{{&g_335,&g_335,(void*)0,(void*)0,&g_335,&g_335},{&g_335,(void*)0,&g_335,&g_335,&g_335,&g_335}}};
static uint8_t g_381 = 0UL;
static uint8_t *g_401 = &g_279;
static uint8_t **g_400 = &g_401;
static int64_t g_410 = 0x72B21F51DF06D251LL;
static int64_t *g_409 = &g_410;
static uint8_t g_433 = 0UL;
static int32_t g_438 = 0x1C1A7BDDL;
static int32_t *g_440 = (void*)0;
static uint64_t g_448 = 0xE50D7E0083D8C6B9LL;
static struct S2 *g_464 = &g_143[0];
static struct S2 **g_463 = &g_464;
static uint32_t g_473 = 0xEC75B64DL;
static uint16_t g_539 = 65527UL;
static uint16_t g_540 = 0xF233L;
static struct S0 g_545 = {0x11F92433L,1,4294967295UL,2L};
static struct S0 *g_544 = &g_545;
static uint8_t g_634 = 0xD0L;
static int64_t g_652 = 0x265843A4F517A737LL;
static int64_t g_682[7] = {0xBCA88D76FE245E5FLL,0xEE395B63D813E0CELL,0xBCA88D76FE245E5FLL,0xBCA88D76FE245E5FLL,0xEE395B63D813E0CELL,0xBCA88D76FE245E5FLL,0xBCA88D76FE245E5FLL};
static uint32_t g_777 = 18446744073709551615UL;
static int64_t g_852 = (-9L);
static int32_t * const g_916 = &g_262;
static uint32_t ***g_930 = &g_334[2][1][3];
static uint32_t ****g_929 = &g_930;
static uint32_t * const ***g_931[3] = {(void*)0,(void*)0,(void*)0};
static uint32_t g_936 = 0x4274C1D0L;
static uint32_t g_937 = 0x09A99535L;
static uint32_t * const g_935[4] = {&g_936,&g_936,&g_936,&g_936};
static uint32_t * const *g_934 = &g_935[3];
static uint32_t * const **g_933 = &g_934;
static uint32_t * const ***g_932 = &g_933;
static int32_t g_959 = (-1L);
static uint8_t g_960[3] = {0x7CL,0x7CL,0x7CL};
static uint64_t * const g_980 = &g_448;
static uint64_t * const *g_979 = &g_980;
static uint8_t g_1078 = 1UL;
static int32_t g_1092 = 0x5EF0D0EFL;
static struct S1 *g_1178 = &g_130;
static struct S1 **g_1177 = &g_1178;
static struct S1 ***g_1176 = &g_1177;
static uint16_t g_1208[10] = {0x4467L,5UL,0x4467L,5UL,0x4467L,5UL,0x4467L,5UL,0x4467L,5UL};
static const struct S0 g_1335 = {0x6C0ADBB6L,1,0UL,6L};
static const struct S0 *g_1334 = &g_1335;
static int8_t *g_1417 = &g_293;
static int8_t **g_1416 = &g_1417;
static uint8_t g_1424 = 0UL;
static uint32_t g_1487 = 7UL;
static const struct S1 g_1510[6][5] = {{{-8101,105,-22172,0x06D7EADDL,179},{11618,119,15534,1UL,120},{-19805,49,43549,1UL,342},{-19805,49,43549,1UL,342},{11618,119,15534,1UL,120}},{{11618,119,15534,1UL,120},{6270,56,6485,0x9D4BA7F8L,133},{-8101,105,-22172,0x06D7EADDL,179},{23073,56,28490,4294967295UL,223},{26928,123,-20429,4294967289UL,22}},{{-6974,139,-44610,4294967286UL,270},{6270,56,6485,0x9D4BA7F8L,133},{23073,56,28490,4294967295UL,223},{-20712,29,5292,0x249A19B1L,277},{-15702,65,39315,1UL,3}},{{25740,104,28280,0x4E13332AL,357},{11618,119,15534,1UL,120},{11618,119,15534,1UL,120},{25740,104,28280,0x4E13332AL,357},{-20712,29,5292,0x249A19B1L,277}},{{-6974,139,-44610,4294967286UL,270},{-19805,49,43549,1UL,342},{26928,123,-20429,4294967289UL,22},{-23492,144,7412,0UL,350},{-20712,29,5292,0x249A19B1L,277}},{{11618,119,15534,1UL,120},{-6974,139,-44610,4294967286UL,270},{-15702,65,39315,1UL,3},{-8101,105,-22172,0x06D7EADDL,179},{-15702,65,39315,1UL,3}}};
static uint8_t g_1581 = 255UL;
static struct S3 ***g_1624 = (void*)0;
static struct S3 ****g_1623[6] = {&g_1624,&g_1624,&g_1624,&g_1624,&g_1624,&g_1624};
static struct S1 g_1866 = {-31389,13,12550,0x6D2DAB3AL,92};
static uint16_t g_1878 = 1UL;
static int32_t g_1904 = 0x54FB0275L;
static int16_t g_1907 = 1L;
static int64_t g_1914 = 0xB131B6DB35AA23E3LL;
static uint32_t g_1915 = 0x1A6DB041L;
static int32_t g_1952[8] = {(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L),(-5L)};
static uint64_t g_1954 = 0x0439B43D7579EFFALL;
static int64_t g_1974 = 0L;
static uint8_t g_1975 = 0UL;
static const uint32_t *g_1996[7] = {&g_937,&g_937,&g_937,&g_937,&g_937,&g_937,&g_937};
static const uint32_t **g_1995 = &g_1996[3];
static const uint32_t ***g_1994[8][7][4] = {{{&g_1995,&g_1995,(void*)0,(void*)0},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0},{&g_1995,&g_1995,&g_1995,&g_1995}},{{&g_1995,(void*)0,&g_1995,(void*)0},{&g_1995,&g_1995,(void*)0,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,(void*)0,&g_1995,(void*)0}},{{&g_1995,&g_1995,&g_1995,(void*)0},{(void*)0,&g_1995,(void*)0,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,(void*)0,&g_1995}},{{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0},{(void*)0,&g_1995,(void*)0,&g_1995},{&g_1995,(void*)0,&g_1995,(void*)0},{&g_1995,&g_1995,&g_1995,&g_1995}},{{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,(void*)0,&g_1995,&g_1995},{&g_1995,(void*)0,(void*)0,&g_1995},{(void*)0,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0}},{{&g_1995,(void*)0,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,(void*)0},{&g_1995,(void*)0,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{(void*)0,&g_1995,(void*)0,(void*)0},{&g_1995,&g_1995,&g_1995,(void*)0}},{{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,(void*)0,&g_1995,(void*)0},{&g_1995,&g_1995,(void*)0,&g_1995},{(void*)0,(void*)0,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995}},{{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,(void*)0,&g_1995},{&g_1995,&g_1995,(void*)0,&g_1995},{&g_1995,&g_1995,(void*)0,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995},{&g_1995,&g_1995,&g_1995,&g_1995}}};
static struct S0 g_2000[3] = {{18446744073709551607UL,-1,0x2FB44F89L,0xB6AB3BC1L},{18446744073709551607UL,-1,0x2FB44F89L,0xB6AB3BC1L},{18446744073709551607UL,-1,0x2FB44F89L,0xB6AB3BC1L}};
static uint32_t g_2116 = 18446744073709551608UL;
static uint32_t g_2173 = 0UL;
static int32_t g_2184 = 0x190C47F1L;
static int32_t g_2186[1] = {6L};
static int64_t **g_2269 = &g_409;
static int64_t ***g_2268 = &g_2269;
static int64_t g_2309 = 0x96C2AECBAE8C6C5ALL;
static struct S1 g_2338 = {-9197,45,-38043,4294967295UL,14};
static struct S1 *g_2337[8][2] = {{&g_2338,&g_2338},{&g_2338,&g_2338},{&g_2338,&g_2338},{&g_2338,&g_2338},{&g_2338,&g_2338},{&g_2338,&g_2338},{&g_2338,&g_2338},{&g_2338,&g_2338}};
static struct S1 g_2345[2] = {{-5253,21,-44386,0UL,46},{-5253,21,-44386,0UL,46}};
static const struct S0 g_2401 = {18446744073709551612UL,1,0x15B510E0L,0L};
static const struct S0 g_2403 = {0x1E182C64L,1,0x40576B59L,-2L};
static struct S0 g_2405 = {1UL,1,0xB133B268L,0x7168B76DL};
static const struct S0 *g_2404 = &g_2405;
static uint8_t g_2437[3][2] = {{255UL,255UL},{255UL,255UL},{255UL,255UL}};
static int64_t g_2449[7] = {1L,(-1L),1L,1L,(-1L),1L,1L};
static uint32_t g_2457[9][4] = {{1UL,18446744073709551615UL,1UL,18446744073709551615UL},{1UL,0x821527D4L,1UL,0x821527D4L},{1UL,18446744073709551615UL,1UL,18446744073709551615UL},{1UL,0x821527D4L,1UL,0x821527D4L},{1UL,18446744073709551615UL,1UL,18446744073709551615UL},{1UL,0x821527D4L,1UL,0x821527D4L},{1UL,18446744073709551615UL,1UL,18446744073709551615UL},{1UL,0x821527D4L,1UL,0x821527D4L},{1UL,18446744073709551615UL,1UL,18446744073709551615UL}};
static int16_t *g_2511 = (void*)0;
static int16_t **g_2510 = &g_2511;
static const uint64_t *g_2527 = &g_1954;
static const uint64_t **g_2526 = &g_2527;
static const uint64_t ***g_2525[1] = {&g_2526};
static int16_t g_2589 = (-4L);


/* --- FORWARD DECLARATIONS --- */
static struct S0  func_1(void);
static int8_t  func_13(uint32_t  p_14, uint16_t  p_15);
static uint16_t  func_17(struct S0  p_18, uint32_t  p_19, struct S2  p_20);
static uint32_t  func_22(const int8_t  p_23, int32_t  p_24);
static struct S3  func_29(struct S3  p_30, uint32_t  p_31, struct S1  p_32, uint32_t  p_33);
static struct S3  func_34(uint32_t  p_35);
static uint32_t  func_77(int16_t  p_78, uint8_t  p_79, const struct S1  p_80, struct S3 *** p_81, const struct S3 * p_82);
static int16_t  func_83(uint32_t  p_84, struct S3 * p_85, uint32_t  p_86, const struct S3 ** p_87, struct S3 * p_88);
static struct S2  func_91(uint64_t  p_92);
static int32_t  func_93(struct S3 * p_94, int32_t * p_95, int64_t  p_96, int64_t  p_97, int64_t  p_98);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_16 g_21 g_36 g_45 g_409 g_410 g_916 g_262 g_99 g_100 g_1416 g_1417 g_293 g_130.f0 g_936 g_130.f2 g_294 g_1092 g_1178 g_130 g_148 g_1623 g_464 g_143 g_440 g_401 g_279 g_934 g_935 g_937 g_175 g_314.f2 g_221 g_1424 g_313 g_979 g_980 g_400 g_448 g_111 g_545.f2 g_1176 g_1177 g_1208 g_1624 g_1487 g_314 g_540 g_652 g_1510.f3 g_959 g_1878 g_777 g_1915 g_335 g_1866.f4 g_1954 g_463 g_1866.f0 g_1510.f4 g_1975 g_123 g_1994 g_544 g_545 g_381 g_316 g_433 g_473 g_1335.f3 g_1952 g_52.f3 g_150 g_2437 g_2449 g_2000.f0 g_2457 g_960 g_315 g_2268 g_2269 g_634 g_258 g_2510 g_2173 g_1914 g_2404 g_2405 g_2525 g_438 g_933 g_2403.f2 g_539 g_1078 g_1334 g_1335
 * writes: g_36 g_38 g_45 g_52 g_262 g_1208 g_1092 g_148 g_440 g_175 g_293 g_221 g_1424 g_314 g_448 g_100 g_111 g_313 g_539 g_410 g_265 g_540 g_303 g_279 g_1878 g_464 g_777 g_1915 g_1954 g_1975 g_1994 g_936 g_1623 g_937 g_2457 g_99 g_316 g_258 g_2510 g_545.f0 g_2173 g_1914 g_2525 g_682 g_438 g_2589 g_21.f2 g_1078
 */
static struct S0  func_1(void)
{ /* block id: 0 */
    uint16_t l_2015 = 5UL;
    struct S2 l_2016 = {100,29};
    const uint32_t l_2429 = 0xEAE938D7L;
    struct S3 l_2446 = {647,1048,-13,-9390,26745};
    struct S3 **** const l_2447 = &g_1624;
    int32_t l_2448 = 0x7287BE3BL;
    int32_t l_2453 = 0x3D9858AAL;
    struct S1 l_2463 = {-6076,2,-2012,4294967295UL,209};
    struct S0 **l_2504 = &g_544;
    int32_t l_2505 = 0x396008A2L;
    struct S2 l_2573 = {160,58};
    struct S2 **l_2596 = &g_464;
    uint16_t l_2599 = 65528UL;
    int32_t *l_2607 = (void*)0;
    int32_t *l_2608 = &g_1092;
    if (((safe_lshift_func_uint16_t_u_s((+(safe_sub_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s(0x6FL, 3)), ((((**g_934) = (safe_add_func_int8_t_s_s(((safe_mod_func_uint8_t_u_u((func_13((g_16 >= func_17(g_21, (func_22((g_16 <= g_21.f2), (!(-1L))) , (safe_mod_func_int16_t_s_s(((l_2015 , g_148) || g_433), g_473))), l_2016)), g_150) | 0L), l_2429)) , l_2015), l_2015))) >= 4294967290UL) <= 0x5069C6DAL)))), 14)) < l_2016.f0))
    { /* block id: 1117 */
        uint16_t l_2432 = 65535UL;
        int32_t l_2441[1];
        struct S1 l_2444 = {13381,134,-1475,0xF6CC775CL,157};
        struct S3 l_2445 = {7184,-611,-491,-13085,9213};
        uint32_t l_2454 = 0xBDC1D625L;
        uint32_t l_2487 = 0xE02D6D32L;
        uint16_t *l_2501 = &g_539;
        struct S0 **l_2502[4] = {&g_544,&g_544,&g_544,&g_544};
        struct S0 ***l_2503 = &l_2502[0];
        int32_t *l_2509 = &g_545.f3;
        int32_t **l_2508 = &l_2509;
        int16_t ***l_2512 = (void*)0;
        int16_t ***l_2513 = &g_2510;
        int i;
        for (i = 0; i < 1; i++)
            l_2441[i] = 0xA532D318L;
        if ((0x9A687638L & (((**g_1416) = l_2015) , (safe_rshift_func_uint8_t_u_s((l_2432 , (((((safe_mul_func_int16_t_s_s((l_2441[0] = (((((safe_mod_func_int8_t_s_s(((l_2448 ^= ((((0x4E49L < g_473) & ((*g_99) = (((g_2437[2][0] != (safe_add_func_uint8_t_u_u(8UL, (safe_unary_minus_func_uint64_t_u(((l_2446 = l_2445) , 18446744073709551615UL)))))) >= l_2444.f4) , l_2016.f1))) , &g_1624) == l_2447)) , l_2446.f2), g_2449[2])) | l_2429) < 0x13L) < (-4L)) || 3UL)), (-1L))) ^ g_2000[2].f0) >= (*g_1417)) & l_2016.f1) , 0x55L)), 2)))))
        { /* block id: 1124 */
            int32_t *l_2450 = &g_2186[0];
            int32_t *l_2451 = &g_2184;
            int32_t *l_2452[4][6][5] = {{{&g_1092,&l_2441[0],&g_2186[0],&g_2184,(void*)0},{(void*)0,&g_2186[0],&g_2186[0],&g_2186[0],(void*)0},{(void*)0,&g_38,(void*)0,&l_2441[0],(void*)0},{&g_100,&l_2441[0],&g_2186[0],&l_2441[0],(void*)0},{&g_2186[0],&g_2184,&g_2186[0],&g_38,(void*)0},{&g_1092,&l_2441[0],&g_2186[0],&g_2184,(void*)0}},{{(void*)0,&g_2186[0],&g_2186[0],&g_2186[0],(void*)0},{(void*)0,&g_38,(void*)0,&l_2441[0],(void*)0},{&g_100,&l_2441[0],&g_2186[0],&l_2441[0],(void*)0},{&g_2186[0],&g_2184,&g_2186[0],&g_38,(void*)0},{&g_1092,&l_2441[0],&g_2186[0],&g_2184,(void*)0},{(void*)0,&g_2186[0],&g_2186[0],&g_2186[0],(void*)0}},{{(void*)0,&g_38,(void*)0,&l_2441[0],(void*)0},{&g_100,&l_2441[0],&g_2186[0],&l_2441[0],(void*)0},{&g_2186[0],&g_2184,&g_2186[0],&g_38,(void*)0},{&g_1092,&l_2441[0],&g_2186[0],&g_2184,(void*)0},{(void*)0,&g_2186[0],&g_2186[0],&g_2186[0],(void*)0},{(void*)0,&g_38,(void*)0,&l_2441[0],(void*)0}},{{&g_100,&l_2441[0],&g_2186[0],&l_2441[0],(void*)0},{&g_2186[0],&g_2184,&g_2186[0],&g_38,(void*)0},{&g_1092,&l_2441[0],&g_2186[0],&g_2184,(void*)0},{(void*)0,&g_2186[0],&g_2186[0],&g_2186[0],(void*)0},{(void*)0,&g_38,(void*)0,&l_2441[0],(void*)0},{&g_100,&l_2441[0],&g_2186[0],&l_2441[0],(void*)0}}};
            struct S3 l_2462 = {5306,-718,210,-5846,27944};
            int32_t **l_2464 = &g_99;
            int i, j, k;
            ++l_2454;
            g_2457[7][2]++;
            for (g_111 = 0; (g_111 <= 5); g_111++)
            { /* block id: 1129 */
                l_2462 = func_34(g_960[1]);
            }
            (*l_2464) = (l_2463 , (void*)0);
        }
        else
        { /* block id: 1133 */
            uint32_t l_2484 = 18446744073709551612UL;
            int32_t *l_2497 = &g_1092;
            int32_t **l_2498[8] = {(void*)0,&l_2497,(void*)0,(void*)0,&l_2497,(void*)0,(void*)0,&l_2497};
            int i;
            for (g_100 = 0; (g_100 >= 6); ++g_100)
            { /* block id: 1136 */
                const struct S3 *l_2467[2];
                int32_t l_2482 = 1L;
                uint16_t *l_2483 = &g_1208[8];
                int16_t *l_2488[7];
                int32_t *l_2489 = &g_175;
                int32_t **l_2490 = &g_440;
                int i;
                for (i = 0; i < 2; i++)
                    l_2467[i] = (void*)0;
                for (i = 0; i < 7; i++)
                    l_2488[i] = &g_260;
                (*g_315) = l_2467[1];
                (*l_2489) |= (safe_div_func_int16_t_s_s((l_2441[0] <= (((((((0x33L && ((((((*g_401) = ((l_2444.f2 = ((0xF05DA48023E313CBLL ^ ((safe_add_func_uint16_t_u_u((!0xBC929BC0F6F1C4D4LL), g_130.f1)) || (((safe_add_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u((safe_unary_minus_func_int64_t_s(l_2453)), (safe_sub_func_uint16_t_u_u(((*l_2483) = l_2482), (l_2484 >= (safe_add_func_uint16_t_u_u(g_1954, l_2482))))))), l_2487)) & (***g_2268)) <= (-1L)))) < g_634)) || 0xA8D1L)) == l_2482) , 0UL) <= 0xF1C2190FD333E659LL) != l_2484)) > (**g_2269)) & 1UL) < (-9L)) < g_1092) , (-3L)) >= l_2463.f2)), (-2L)));
                (*l_2490) = &l_2441[0];
                (**l_2490) = (*g_440);
            }
            for (g_258 = 0; (g_258 <= 7); g_258 = safe_add_func_int16_t_s_s(g_258, 1))
            { /* block id: 1147 */
                uint32_t l_2493[4][8][1] = {{{0x6C164280L},{5UL},{0x6C164280L},{0x3DF4C432L},{4294967295UL},{5UL},{4294967295UL},{0x3DF4C432L}},{{0x6C164280L},{5UL},{0x6C164280L},{0x3DF4C432L},{4294967295UL},{5UL},{4294967295UL},{0x3DF4C432L}},{{0x6C164280L},{5UL},{0x6C164280L},{0x3DF4C432L},{4294967295UL},{5UL},{4294967295UL},{0x3DF4C432L}},{{0x6C164280L},{5UL},{0x6C164280L},{0x3DF4C432L},{4294967295UL},{5UL},{4294967295UL},{0x3DF4C432L}}};
                struct S0 l_2496 = {0xDED620FFL,0,4294967286UL,-1L};
                int i, j, k;
                l_2493[1][7][0]--;
                return l_2496;
            }
            g_99 = l_2497;
        }
        l_2445.f2 ^= (((void*)0 != &l_2016) , (((&g_916 != ((safe_add_func_int64_t_s_s(0x14B00371FDFED864LL, (((*l_2501) = 0x5A85L) | ((l_2504 = ((*l_2503) = l_2502[3])) == ((((**g_400) >= l_2505) & (safe_div_func_uint32_t_u_u(l_2454, l_2445.f0))) , &g_2404))))) , l_2508)) != l_2432) , l_2446.f0));
        (*l_2513) = g_2510;
    }
    else
    { /* block id: 1158 */
        struct S1 l_2558 = {10159,86,627,0x03972735L,285};
        struct S3 l_2559 = {4128,-1883,300,5355,529};
        uint16_t l_2570 = 65535UL;
        for (l_2015 = 0; (l_2015 <= 2); l_2015 += 1)
        { /* block id: 1161 */
            uint32_t l_2515 = 4294967290UL;
            int32_t * const l_2554[1] = {&g_100};
            struct S3 l_2557 = {3572,-2352,143,-9939,31145};
            int64_t l_2563 = 0x3E8F09E297173F23LL;
            int i;
            for (g_545.f0 = 0; (g_545.f0 <= 2); g_545.f0 += 1)
            { /* block id: 1164 */
                uint16_t l_2536 = 65528UL;
                struct S3 *****l_2552[1];
                struct S3 l_2561 = {7391,-341,496,-12415,29172};
                int32_t l_2567 = 0x6C6C8BA5L;
                int32_t l_2568 = (-10L);
                int i;
                for (i = 0; i < 1; i++)
                    l_2552[i] = &g_1623[1];
                (*g_99) = (*g_99);
                for (g_2173 = 0; (g_2173 <= 6); g_2173 += 1)
                { /* block id: 1168 */
                    const int32_t l_2524 = (-2L);
                    const uint64_t ****l_2528 = &g_2525[0];
                    const uint8_t l_2535 = 0xA8L;
                    struct S3 l_2560[7] = {{2617,1681,-414,-7868,17592},{2617,1681,-414,-7868,17592},{2617,1681,-414,-7868,17592},{2617,1681,-414,-7868,17592},{2617,1681,-414,-7868,17592},{2617,1681,-414,-7868,17592},{2617,1681,-414,-7868,17592}};
                    int32_t l_2566 = 0xD5CD12C4L;
                    int32_t l_2569[10] = {0x62A59981L,0x62A59981L,1L,0x8C779D36L,1L,0x62A59981L,0x62A59981L,1L,0x8C779D36L,1L};
                    int i;
                    for (g_1914 = 6; (g_1914 >= 0); g_1914 -= 1)
                    { /* block id: 1171 */
                        return (*g_2404);
                    }
                    for (g_448 = 0; (g_448 <= 6); g_448 += 1)
                    { /* block id: 1176 */
                        int32_t *l_2514[7][2] = {{&g_2184,&g_2184},{&g_2184,&g_2184},{&g_2184,&g_2184},{&g_2184,&g_2184},{&g_2184,&g_2184},{&g_2184,&g_2184},{&g_2184,&g_2184}};
                        int i, j;
                        ++l_2515;
                    }
                    if ((((((safe_lshift_func_uint8_t_u_s(((**g_1416) , (0L | (safe_sub_func_int16_t_s_s(((safe_lshift_func_int8_t_s_s((*g_1417), 7)) != l_2524), (((*l_2528) = g_2525[0]) != ((*g_464) , &g_2526)))))), 3)) , (safe_sub_func_int32_t_s_s((((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int8_t_s_s((0L >= (4L == (**g_2269))), l_2535)), (*g_401))) , l_2515) || l_2535), 0x708A6B27L))) <= l_2515) == 1L) <= l_2536))
                    { /* block id: 1180 */
                        struct S2 l_2543 = {116,110};
                        const struct S0 l_2544 = {18446744073709551606UL,-1,0xF1F299F0L,0xCD76C0A5L};
                        uint16_t *l_2553 = &g_1878;
                        int32_t **l_2555 = &g_99;
                        struct S3 l_2556 = {7420,2964,172,-15849,24073};
                        int i;
                        (*g_99) = (safe_sub_func_uint32_t_u_u(l_2463.f2, ((((*g_409) | (safe_rshift_func_int16_t_s_u((+(l_2515 & (!(l_2543 , ((l_2544 , ((!(0xF03400E4L && 0x2902BD30L)) & (((*l_2553) = (l_2524 , (safe_sub_func_uint8_t_u_u((safe_div_func_int64_t_s_s((g_682[g_2173] = (safe_add_func_uint16_t_u_u((((g_123[0][6][0] != 2L) , (void*)0) == l_2552[0]), 65534UL))), l_2524)), 0x4FL)))) > l_2543.f1))) >= 254UL))))), l_2543.f1))) && 0xB1L) & 0xD0F10449L)));
                        (*l_2555) = l_2554[0];
                        l_2557 = l_2557;
                        l_2560[6] = (l_2446 = (l_2559 = (**g_315)));
                    }
                    else
                    { /* block id: 1190 */
                        int32_t l_2562[7] = {0x98175C9FL,0x98175C9FL,0x98175C9FL,0x98175C9FL,0x98175C9FL,0x98175C9FL,0x98175C9FL};
                        int8_t l_2564 = (-1L);
                        int32_t l_2565 = (-4L);
                        int i;
                        l_2561 = (*g_316);
                        --l_2570;
                    }
                }
                if ((*g_99))
                    continue;
                if (l_2559.f1)
                    break;
            }
        }
    }
    for (g_438 = 0; (g_438 <= 3); g_438 += 1)
    { /* block id: 1202 */
        int8_t l_2574 = 1L;
        uint32_t *l_2587 = (void*)0;
        uint32_t *l_2588[6][9] = {{&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4]},{&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4]},{&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4]},{&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4]},{&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4]},{&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4],&g_2116,&g_2116,&g_265[4][4]}};
        int32_t l_2590 = 2L;
        int32_t *l_2591 = &l_2505;
        int16_t ***l_2592 = &g_2510;
        int32_t **l_2593 = &g_99;
        int i, j;
        (*l_2591) = (((**g_400) = (((*g_2404) , l_2573) , (((***g_933) && l_2574) == (g_2403.f2 >= ((safe_div_func_int64_t_s_s((((safe_add_func_uint16_t_u_u(g_539, (safe_sub_func_uint32_t_u_u(((*g_335) &= ((safe_rshift_func_int8_t_s_s((l_2574 <= (safe_div_func_int8_t_s_s((g_2589 = (((g_545.f0 = (safe_mod_func_uint32_t_u_u((((*g_1417) = 0x15L) , l_2574), l_2574))) , l_2574) & l_2463.f4)), l_2590))), l_2574)) && (*g_401))), 0x601D3C9BL)))) ^ (*g_401)) , 0x4F118D7D05272729LL), (-1L))) >= l_2573.f1))))) <= 0x95L);
        (*l_2591) |= 1L;
        (*l_2591) ^= (((*l_2592) = &g_2511) == &g_2511);
        (*l_2593) = &g_175;
        for (g_1078 = 0; (g_1078 <= 3); g_1078 += 1)
        { /* block id: 1215 */
            return (*g_544);
        }
    }
    (*l_2608) &= (l_2573.f1 < (l_2446.f0 > (safe_div_func_uint64_t_u_u(((l_2596 != (void*)0) | ((**g_463) , (safe_mul_func_uint16_t_u_u((l_2599 ^ (l_2446.f4 > (!(safe_rshift_func_int16_t_s_u(l_2446.f1, (safe_add_func_int64_t_s_s((((safe_sub_func_int8_t_s_s(0xF4L, 0L)) , g_959) <= 6L), (*g_980)))))))), 0x84DBL)))), l_2016.f1))));
    return (*g_1334);
}


/* ------------------------------------------ */
/* 
 * reads : g_1417 g_293
 * writes:
 */
static int8_t  func_13(uint32_t  p_14, uint16_t  p_15)
{ /* block id: 940 */
    struct S0 *l_2048 = &g_2000[0];
    int32_t l_2055[9][6][1] = {{{(-1L)},{0xF2A91AE5L},{0xB1C3333AL},{(-3L)},{1L},{(-3L)}},{{0xB1C3333AL},{0xF2A91AE5L},{(-1L)},{(-1L)},{(-2L)},{(-1L)}},{{(-2L)},{(-1L)},{(-1L)},{0xF2A91AE5L},{0xB1C3333AL},{(-3L)}},{{1L},{(-3L)},{0xB1C3333AL},{0xF2A91AE5L},{(-1L)},{(-1L)}},{{(-2L)},{(-1L)},{(-2L)},{(-1L)},{(-1L)},{0xF2A91AE5L}},{{0xB1C3333AL},{(-3L)},{1L},{(-3L)},{0xB1C3333AL},{0xF2A91AE5L}},{{(-1L)},{(-1L)},{(-2L)},{(-1L)},{(-2L)},{(-1L)}},{{(-1L)},{0xF2A91AE5L},{0xB1C3333AL},{(-3L)},{1L},{(-3L)}},{{0xB1C3333AL},{0xF2A91AE5L},{(-1L)},{(-1L)},{(-2L)},{(-1L)}}};
    int16_t l_2056 = 1L;
    int8_t l_2084 = 1L;
    int64_t l_2088 = 9L;
    uint64_t l_2095 = 0x65758E027387FCF5LL;
    int32_t * const *l_2147 = &g_916;
    struct S3 l_2152 = {4898,2443,394,-12979,9753};
    int8_t l_2179 = (-10L);
    int16_t l_2191[4] = {0xD8C2L,0xD8C2L,0xD8C2L,0xD8C2L};
    uint64_t *l_2224 = &g_123[0][1][1];
    uint64_t **l_2223 = &l_2224;
    uint64_t ***l_2222 = &l_2223;
    struct S1 * const *l_2254 = &g_1178;
    struct S1 * const **l_2253[6][10][4] = {{{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,(void*)0,&l_2254},{&l_2254,(void*)0,(void*)0,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{(void*)0,(void*)0,&l_2254,&l_2254},{(void*)0,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254}},{{&l_2254,&l_2254,(void*)0,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,(void*)0,&l_2254},{(void*)0,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254}},{{&l_2254,(void*)0,&l_2254,&l_2254},{(void*)0,&l_2254,(void*)0,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,(void*)0,&l_2254},{(void*)0,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0}},{{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{(void*)0,&l_2254,(void*)0,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,(void*)0,&l_2254},{(void*)0,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254}},{{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{(void*)0,&l_2254,(void*)0,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,(void*)0,&l_2254},{(void*)0,&l_2254,&l_2254,&l_2254}},{{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{(void*)0,&l_2254,(void*)0,&l_2254},{&l_2254,(void*)0,&l_2254,&l_2254},{&l_2254,&l_2254,&l_2254,(void*)0},{&l_2254,&l_2254,&l_2254,&l_2254},{&l_2254,(void*)0,&l_2254,(void*)0},{&l_2254,&l_2254,(void*)0,&l_2254}}};
    struct S3 *****l_2281[7] = {&g_1623[1],&g_1623[0],&g_1623[0],&g_1623[1],&g_1623[0],&g_1623[0],&g_1623[1]};
    uint32_t l_2311 = 0xE1517482L;
    struct S1 *l_2344[10][4] = {{(void*)0,&g_2345[1],&g_130,&g_130},{&g_2345[1],&g_2345[1],(void*)0,&g_2345[1]},{&g_2345[1],&g_1866,&g_130,&g_2345[1]},{(void*)0,&g_2345[1],(void*)0,&g_130},{&g_2345[1],&g_2345[1],(void*)0,&g_2345[1]},{&g_2345[1],&g_1866,&g_1866,&g_2345[1]},{(void*)0,&g_2345[1],&g_1866,&g_130},{&g_2345[1],&g_2345[1],(void*)0,&g_2345[1]},{&g_2345[1],&g_1866,(void*)0,&g_2345[1]},{(void*)0,&g_2345[1],&g_130,&g_130}};
    const struct S0 *l_2400[6] = {&g_2401,&g_2401,&g_2401,&g_2401,&g_2401,&g_2401};
    int8_t l_2428 = 0xB4L;
    int i, j, k;
    return (*g_1417);
}


/* ------------------------------------------ */
/* 
 * reads : g_262 g_1208 g_99 g_400 g_401 g_279 g_409 g_540 g_1335.f3 g_100 g_1952 g_52.f3 g_175
 * writes: g_262 g_100 g_410 g_540 g_175
 */
static uint16_t  func_17(struct S0  p_18, uint32_t  p_19, struct S2  p_20)
{ /* block id: 930 */
    uint64_t l_2017 = 0UL;
    uint16_t *l_2030 = &g_540;
    uint8_t l_2041 = 0xF4L;
    uint32_t l_2042 = 0x9515EED2L;
    uint8_t l_2043[1][8][8] = {{{0xB4L,0x75L,0x66L,247UL,247UL,0x3EL,0x17L,0x17L},{9UL,0x38L,255UL,255UL,0x38L,9UL,0x28L,247UL},{0xB2L,255UL,0x5BL,0x12L,255UL,0x28L,0xB2L,0xB7L},{0x38L,247UL,0x66L,0x12L,0x17L,0x66L,0xB4L,247UL},{0xB7L,0x17L,255UL,255UL,0xB2L,255UL,255UL,0x17L},{0x75L,0x28L,0xB7L,247UL,255UL,0x5BL,247UL,9UL},{0x12L,0xB2L,0x3EL,0xB4L,0x75L,0x66L,247UL,247UL},{9UL,0xB4L,0xB7L,0x5BL,1UL,255UL,255UL,1UL}}};
    int32_t *l_2044 = &g_175;
    uint32_t l_2045 = 0x918FCDD7L;
    int i, j, k;
    for (g_262 = 6; (g_262 >= 2); g_262 -= 1)
    { /* block id: 933 */
        int i;
        (*g_99) = g_1208[g_262];
    }
    (*l_2044) ^= ((l_2017 <= (-5L)) & (safe_sub_func_uint32_t_u_u((((-1L) & (safe_lshift_func_uint8_t_u_u((**g_400), 0))) , ((((safe_add_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_u(((safe_sub_func_int16_t_s_s((((((l_2030 != (void*)0) <= ((((safe_mod_func_int16_t_s_s((((safe_rshift_func_int8_t_s_u(((safe_sub_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u(p_18.f0, ((*l_2030) ^= (l_2041 && ((*g_409) = (l_2042 , (-1L))))))), p_20.f0)) || 0L), (*g_401))) , 0x4AL) , g_1335.f3), 1L)) <= l_2042) ^ (*g_99)) > 0UL)) && l_2041) , p_18.f2) , 8L), g_1952[7])) , p_18.f3), p_19)), l_2043[0][5][7])), (-1L))) <= l_2043[0][0][5]) != g_52.f3) ^ (-1L))), 0xB6A41E10L)));
    return l_2045;
}


/* ------------------------------------------ */
/* 
 * reads : g_21.f2 g_16 g_21.f3 g_36 g_45 g_409 g_410 g_21 g_916 g_262 g_99 g_100 g_1416 g_1417 g_293 g_130.f0 g_936 g_130.f2 g_294 g_1092 g_1178 g_130 g_148 g_1623 g_464 g_143 g_440 g_401 g_279 g_934 g_935 g_937 g_175 g_314.f2 g_221 g_1424 g_313 g_979 g_980 g_400 g_448 g_111 g_545.f2 g_1176 g_1177 g_1208 g_1624 g_1487 g_314 g_540 g_652 g_1510.f3 g_959 g_1878 g_777 g_1915 g_335 g_1866.f4 g_1954 g_463 g_1866.f0 g_1510.f4 g_1975 g_123 g_1994 g_544 g_545 g_381 g_316 g_433
 * writes: g_36 g_38 g_45 g_52 g_262 g_1208 g_1092 g_148 g_440 g_175 g_293 g_221 g_1424 g_314 g_448 g_100 g_111 g_313 g_539 g_410 g_265 g_540 g_303 g_279 g_1878 g_464 g_777 g_1915 g_1954 g_1975 g_1994 g_936 g_1623
 */
static uint32_t  func_22(const int8_t  p_23, int32_t  p_24)
{ /* block id: 1 */
    uint32_t l_1611 = 4294967287UL;
    const struct S3 ** const l_2010 = &g_316;
    int32_t l_2011 = 0L;
    int32_t l_2012[7][2] = {{0xFB4A639FL,0xFB4A639FL},{0xFB4A639FL,0xFB4A639FL},{0xFB4A639FL,0xFB4A639FL},{0xFB4A639FL,0xFB4A639FL},{0xFB4A639FL,0xFB4A639FL},{0xFB4A639FL,0xFB4A639FL},{0xFB4A639FL,0xFB4A639FL}};
    int i, j;
    l_2012[1][1] &= (~(safe_lshift_func_uint8_t_u_u((g_21.f2 >= (g_16 == ((func_29(func_34(g_21.f2), l_1611, (*g_1178), l_1611) , ((l_2011 |= (l_2010 == (void*)0)) , l_2011)) || p_24))), p_24)));
    return l_2012[1][0];
}


/* ------------------------------------------ */
/* 
 * reads : g_148 g_1623 g_464 g_143 g_440 g_100 g_130.f1 g_401 g_279 g_1416 g_1417 g_293 g_934 g_935 g_937 g_936 g_409 g_410 g_175 g_314.f2 g_221 g_1424 g_313 g_979 g_980 g_400 g_99 g_448 g_1092 g_111 g_36 g_21.f3 g_545.f2 g_1176 g_1177 g_1178 g_130 g_1208 g_1624 g_1487 g_314 g_540 g_652 g_1510.f3 g_959 g_1878 g_777 g_1915 g_335 g_21.f2 g_1866.f4 g_1954 g_463 g_1866.f0 g_1510.f4 g_1975 g_123 g_1994 g_544 g_545 g_916 g_262 g_381 g_316 g_433 g_45 g_21 g_294
 * writes: g_148 g_440 g_175 g_293 g_221 g_1424 g_314 g_448 g_100 g_111 g_313 g_539 g_1208 g_410 g_265 g_540 g_303 g_279 g_1878 g_464 g_777 g_1915 g_1954 g_1975 g_1994 g_936 g_1623 g_36 g_38 g_45 g_52 g_262 g_1092
 */
static struct S3  func_29(struct S3  p_30, uint32_t  p_31, struct S1  p_32, uint32_t  p_33)
{ /* block id: 730 */
    uint32_t l_1613 = 0x837EACC1L;
    uint8_t l_1629 = 247UL;
    int16_t l_1636 = 9L;
    struct S0 l_1652 = {1UL,-1,0xD2690C31L,0x829CBAB6L};
    struct S3 *** const *l_1653 = &g_1624;
    int32_t l_1659 = 0xECDBDC96L;
    struct S2 **l_1692 = &g_464;
    uint32_t l_1697 = 0x3AA53626L;
    int32_t l_1710 = 0xB709EA0EL;
    uint64_t * const l_1748[6] = {&g_123[0][2][0],&g_123[0][2][0],&g_123[0][2][0],&g_123[0][2][0],&g_123[0][2][0],&g_123[0][2][0]};
    int64_t **l_1802 = &g_409;
    int64_t ***l_1801 = &l_1802;
    int8_t * const l_1806 = &g_259;
    struct S1 *l_1865 = &g_1866;
    int32_t l_1868 = (-1L);
    int32_t l_1870 = 0x97ABB7B3L;
    int32_t l_1902 = (-1L);
    int32_t l_1905 = (-6L);
    uint64_t l_1918 = 0x1C3E3E5465E0F37BLL;
    uint8_t l_1942 = 0x28L;
    struct S0 *l_1999 = &g_2000[2];
    uint16_t *l_2005 = &g_1878;
    struct S3 ****l_2006 = &g_1624;
    struct S3 *****l_2007 = &g_1623[1];
    struct S3 l_2009 = {113,2048,198,-7139,32765};
    int i;
    for (g_148 = 0; (g_148 <= 1); g_148 += 1)
    { /* block id: 733 */
        int32_t *l_1612[4];
        int32_t **l_1616 = &g_440;
        struct S3 **l_1627 = &g_313;
        struct S3 ** const *l_1626 = &l_1627;
        struct S3 ** const **l_1625[7][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
        struct S3 ** const ***l_1628 = &l_1625[0][1];
        int32_t l_1637 = 0x464C45D3L;
        uint16_t l_1657 = 65532UL;
        int32_t l_1658[2];
        uint16_t l_1660 = 0xD438L;
        uint64_t l_1689 = 1UL;
        struct S1 l_1749 = {27279,156,9992,0xD8553D09L,125};
        uint16_t l_1788 = 0xA453L;
        uint16_t l_1826 = 0x64D6L;
        struct S0 *l_1851 = &l_1652;
        int32_t l_1871[7][4][3] = {{{0xF10774CAL,0x0334FADDL,0xF10774CAL},{(-4L),0xEC8C31F8L,1L},{0x6B216CC7L,0x154B351AL,1L},{0xE6783743L,1L,(-1L)}},{{(-1L),(-7L),0x16C85292L},{0xE6783743L,0x2F7BC55BL,0L},{0x6B216CC7L,0x6B216CC7L,0x0334FADDL},{(-4L),(-7L),0xC0395467L}},{{0xF10774CAL,0x231DFFFFL,0x6B216CC7L},{0x328B43C5L,0L,0x3A402405L},{0x978D5C7FL,0xF10774CAL,0x978D5C7FL},{1L,(-1L),(-1L)}},{{0x16C85292L,0L,(-7L)},{0xF75E4FAEL,0L,0xEC8C31F8L},{0L,0x154B351AL,0L},{0xEC8C31F8L,0x2F7BC55BL,0x3A402405L}},{{0x231DFFFFL,0x154B351AL,0xF10774CAL},{0xB94FF15AL,0L,0xB94FF15AL},{0x154B351AL,0L,0x0E4E2564L},{0L,(-1L),1L}},{{0xF10774CAL,0x0E4E2564L,0L},{0x606A6B17L,0x328B43C5L,(-4L)},{0xF10774CAL,0x6B216CC7L,(-1L)},{0L,0L,0L}},{{0x154B351AL,0x978D5C7FL,0x0C3932DCL},{0xB94FF15AL,(-7L),0L},{0x231DFFFFL,0xB87CB9C8L,1L},{0xEC8C31F8L,0xB94FF15AL,0L}}};
        uint32_t ***l_1985 = (void*)0;
        int i, j, k;
        for (i = 0; i < 4; i++)
            l_1612[i] = &g_100;
        for (i = 0; i < 2; i++)
            l_1658[i] = (-1L);
        --l_1613;
        (*l_1616) = l_1612[0];
        if ((safe_sub_func_int64_t_s_s((safe_mod_func_uint64_t_u_u((((g_1623[1] == ((*l_1628) = l_1625[1][0])) ^ (((*g_464) , p_32.f3) <= (**l_1616))) > p_30.f3), (l_1629 && ((safe_mod_func_int32_t_s_s(((((safe_div_func_uint16_t_u_u(p_30.f4, (safe_add_func_uint8_t_u_u((l_1636 < g_130.f1), l_1637)))) && 0xAB5EL) , (*g_401)) == (**g_1416)), (**g_934))) && p_30.f2)))), (*g_409))))
        { /* block id: 737 */
            uint32_t l_1664[5];
            int16_t *l_1677 = &l_1636;
            int16_t **l_1676[6][7] = {{&l_1677,&l_1677,&l_1677,&l_1677,&l_1677,&l_1677,&l_1677},{&l_1677,&l_1677,&l_1677,&l_1677,&l_1677,&l_1677,(void*)0},{(void*)0,&l_1677,&l_1677,(void*)0,&l_1677,&l_1677,(void*)0},{&l_1677,(void*)0,&l_1677,&l_1677,(void*)0,&l_1677,&l_1677},{(void*)0,(void*)0,&l_1677,&l_1677,&l_1677,&l_1677,&l_1677},{&l_1677,&l_1677,&l_1677,&l_1677,&l_1677,&l_1677,&l_1677}};
            struct S2 ***l_1693 = (void*)0;
            struct S2 **l_1694 = (void*)0;
            int i, j;
            for (i = 0; i < 5; i++)
                l_1664[i] = 0x8EB69450L;
            for (g_175 = 0; (g_175 <= 1); g_175 += 1)
            { /* block id: 740 */
                struct S3 ****l_1654 = &g_1624;
                int16_t *l_1655[5][3] = {{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}};
                int32_t l_1656 = (-1L);
                int i, j;
                p_30.f2 |= (safe_div_func_uint16_t_u_u((((safe_sub_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((*g_401), (safe_lshift_func_int8_t_s_s(((**g_1416) &= l_1613), (2UL == ((l_1658[0] = ((safe_lshift_func_uint8_t_u_s((p_32.f1 ^ (safe_mod_func_int8_t_s_s(((l_1656 = ((p_30.f3 == (&g_1208[9] == (void*)0)) | ((l_1652 , l_1653) == l_1654))) , l_1657), p_32.f2))), 3)) == 0x0A8315DCL)) , l_1652.f1)))))), 0xACL)) <= 0x57E869ECF0B979ECLL) & g_314.f2), p_32.f4));
                ++l_1660;
                for (g_221 = 1; (g_221 >= 0); g_221 -= 1)
                { /* block id: 748 */
                    int32_t l_1663[1][5];
                    int i, j;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 5; j++)
                            l_1663[i][j] = 0xDEF54195L;
                    }
                    l_1664[0]++;
                    for (g_1424 = 0; (g_1424 <= 1); g_1424 += 1)
                    { /* block id: 752 */
                        const uint32_t l_1667 = 0xC3E2E45DL;
                        uint16_t *l_1675 = &l_1657;
                        int32_t l_1688 = (-2L);
                        (***l_1626) = (p_32 , p_30);
                        (*g_440) |= (l_1667 < ((~(safe_div_func_uint64_t_u_u(((((safe_mul_func_uint16_t_u_u(((*l_1675) = l_1663[0][2]), p_32.f1)) >= ((((void*)0 != l_1676[5][4]) <= (safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint8_t_u_s((p_31 , ((safe_lshift_func_uint8_t_u_u(((*g_1417) || (0x16L & (safe_unary_minus_func_uint64_t_u((((**g_979) = (safe_div_func_int64_t_s_s((((safe_mul_func_uint16_t_u_u((l_1667 , 0xD7C2L), p_30.f0)) <= p_30.f1) <= 0x5AL), p_32.f4))) <= p_30.f3))))), 4)) , 1UL)), (*g_1417)))))) != p_30.f0)) >= l_1667) > (**g_400)), 1UL))) & 0UL));
                        if ((*g_99))
                            continue;
                        --l_1689;
                    }
                    if (l_1664[0])
                        break;
                }
            }
            (*g_440) ^= (((p_32.f4 > (&g_464 == l_1692)) || (0x3A30L < ((&g_464 != (l_1694 = &g_464)) | (safe_sub_func_int32_t_s_s((p_32.f3 ^ l_1697), (safe_rshift_func_uint8_t_u_u(l_1652.f0, 7))))))) > p_32.f0);
        }
        else
        { /* block id: 765 */
            int16_t l_1706 = 0x2BBBL;
            p_30.f3 = (safe_add_func_uint16_t_u_u(((**l_1616) , (((p_32.f1 <= 0xE9L) > p_33) , (safe_mod_func_int32_t_s_s((l_1659 = ((safe_rshift_func_int16_t_s_u(((**g_979) & (0x9FF2D022L & (l_1659 | ((**g_400) && (*g_401))))), g_1092)) & p_32.f0)), l_1652.f2)))), l_1706));
        }
        if ((safe_lshift_func_int8_t_s_u(l_1613, (**g_400))))
        { /* block id: 769 */
            const uint8_t l_1709 = 0xBFL;
            if (l_1709)
                break;
        }
        else
        { /* block id: 771 */
            uint32_t l_1711 = 0xAB4C3DAAL;
            int16_t l_1753 = 0x86F1L;
            int32_t l_1760[4];
            struct S0 *l_1831[7][7] = {{&g_545,&g_21,(void*)0,&g_545,&g_545,(void*)0,(void*)0},{&g_545,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_545,&g_545,&g_21,&g_21,&g_21,(void*)0,(void*)0},{&l_1652,&g_545,&g_545,(void*)0,(void*)0,(void*)0,(void*)0},{&g_21,(void*)0,(void*)0,&g_21,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&l_1652,(void*)0,&g_545,(void*)0,&g_545},{(void*)0,&g_545,(void*)0,&g_545,(void*)0,&g_545,(void*)0}};
            int16_t l_1874 = 0xCB87L;
            int i, j;
            for (i = 0; i < 4; i++)
                l_1760[i] = 0x6DFC8D04L;
            --l_1711;
            for (l_1657 = 0; (l_1657 <= 1); l_1657 += 1)
            { /* block id: 775 */
                const uint64_t l_1721[5][1] = {{18446744073709551614UL},{18446744073709551614UL},{18446744073709551614UL},{18446744073709551614UL},{18446744073709551614UL}};
                int8_t l_1731 = (-1L);
                int32_t l_1732 = 0L;
                const uint32_t l_1752 = 18446744073709551607UL;
                uint32_t l_1763 = 0x45645C63L;
                struct S3 ***l_1792 = &l_1627;
                int32_t l_1793 = 0xE8344845L;
                uint16_t *l_1840[8] = {&g_540,&g_540,&g_540,&g_540,&g_540,&g_540,&g_540,&g_540};
                struct S0 *l_1852 = &l_1652;
                int i, j;
                for (l_1659 = 0; (l_1659 <= 1); l_1659 += 1)
                { /* block id: 778 */
                    int16_t l_1725 = 0xDAD1L;
                    uint32_t ****l_1747[10] = {&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930,&g_930};
                    int32_t l_1754 = (-1L);
                    uint32_t *l_1758 = (void*)0;
                    uint32_t **l_1757 = &l_1758;
                    struct S3 l_1759 = {2376,2787,257,-9231,20489};
                    struct S1 ***l_1768 = (void*)0;
                    struct S1 ***l_1769 = (void*)0;
                    int i;
                    for (g_111 = 1; (g_111 >= 0); g_111 -= 1)
                    { /* block id: 781 */
                        int64_t *l_1720 = &g_652;
                        int32_t l_1727 = 0x573155F0L;
                        uint32_t ****l_1746[1];
                        int32_t *l_1751 = &g_21.f3;
                        int32_t **l_1750 = &l_1751;
                        struct S3 * const l_1755 = &g_52;
                        struct S3 **l_1756 = &g_313;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_1746[i] = &g_930;
                        (*g_99) = (safe_mul_func_int8_t_s_s(((safe_mul_func_int8_t_s_s((**g_1416), (l_1720 == (void*)0))) >= (l_1721[0][0] != (((safe_add_func_uint8_t_u_u((~l_1725), (((0x6FF6AC48L <= ((~(l_1727 , (l_1732 &= (safe_lshift_func_uint8_t_u_s((((!(**g_400)) <= (((3UL >= 5UL) | l_1725) && l_1731)) != 1UL), 3))))) , 4294967295UL)) < g_36) <= l_1711))) , l_1721[1][0]) == (**l_1616)))), 9UL));
                        (*l_1756) = ((p_31 & ((safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((l_1754 |= ((safe_div_func_int64_t_s_s((((safe_unary_minus_func_uint8_t_u(((((safe_mul_func_int8_t_s_s((safe_rshift_func_int8_t_s_s((((safe_add_func_uint32_t_u_u((((*g_979) == (((l_1747[4] = l_1746[0]) != (void*)0) , l_1748[2])) == l_1721[0][0]), (0x3AL || (((*l_1750) = (((0x7CL < 0x7DL) , l_1749) , (void*)0)) != &g_150)))) == l_1731) > (**g_934)), l_1727)), (**g_400))) <= g_21.f3) & l_1752) && l_1711))) == l_1727) >= l_1753), l_1725)) && (**g_400))), 6)), g_545.f2)) | p_33)) , l_1755);
                        return p_30;
                    }
                    if ((((*l_1757) = &g_777) != &p_33))
                    { /* block id: 791 */
                        return l_1759;
                    }
                    else
                    { /* block id: 793 */
                        int8_t l_1761 = 0x93L;
                        int32_t l_1762[7] = {0xC26C2CA2L,0xA7FA7B62L,0xC26C2CA2L,0xC26C2CA2L,0xA7FA7B62L,0xC26C2CA2L,0xC26C2CA2L};
                        int32_t ***l_1784 = (void*)0;
                        int32_t *l_1787 = &g_262;
                        int32_t **l_1786 = &l_1787;
                        int32_t ***l_1785 = &l_1786;
                        uint16_t *l_1789 = &l_1788;
                        uint16_t *l_1790 = (void*)0;
                        uint16_t *l_1791 = &g_1208[8];
                        int i;
                        l_1763++;
                        (*g_99) = (0xBDL ^ (((p_31 != ((l_1768 == l_1769) < 0x6B028D59L)) | (((safe_rshift_func_int8_t_s_u(((safe_mul_func_int8_t_s_s((safe_add_func_int16_t_s_s(((-4L) > ((*l_1791) ^= (g_539 = ((*l_1789) = (safe_lshift_func_uint8_t_u_s((((safe_sub_func_int32_t_s_s((*g_440), (((***g_1176) , (safe_sub_func_uint16_t_u_u(((safe_add_func_uint8_t_u_u(((((*l_1785) = (void*)0) != (void*)0) < (**l_1616)), (**g_400))) > 0xB620L), l_1788))) < (**g_934)))) < 0xFB767756L) >= (-5L)), p_30.f2)))))), g_143[0].f1)), p_30.f0)) & p_32.f0), 7)) < 1L) == l_1761)) >= 4L));
                    }
                }
                if (((l_1792 = &l_1627) != (*l_1653)))
                { /* block id: 803 */
                    int64_t l_1794 = (-5L);
                    int32_t l_1828[4];
                    int32_t *l_1832 = &g_38;
                    int i;
                    for (i = 0; i < 4; i++)
                        l_1828[i] = 0x28E049F4L;
                    if ((l_1793 < (l_1794 || ((safe_add_func_int64_t_s_s((l_1763 <= (((safe_div_func_uint8_t_u_u(3UL, 255UL)) <= (((safe_mod_func_int16_t_s_s((((l_1801 != (void*)0) > (0L >= (safe_rshift_func_int16_t_s_s(((!((void*)0 != l_1806)) || p_32.f0), 8)))) , p_30.f4), p_33)) ^ p_32.f0) && (**l_1616))) ^ p_30.f4)), (*g_409))) <= 0x11L))))
                    { /* block id: 804 */
                        int16_t *l_1807 = (void*)0;
                        uint32_t *l_1827 = &g_265[4][3];
                        l_1828[2] |= (l_1794 > ((l_1807 != (((*l_1827) = (safe_lshift_func_uint8_t_u_u(p_30.f4, (((safe_mul_func_int16_t_s_s(l_1732, 0x8230L)) && ((safe_rshift_func_int8_t_s_s(((safe_rshift_func_uint8_t_u_s((safe_sub_func_int16_t_s_s(l_1793, 65535UL)), 1)) , ((safe_rshift_func_int8_t_s_s(((((safe_sub_func_int64_t_s_s(((*g_409) = (-1L)), (safe_add_func_int8_t_s_s(((((safe_sub_func_uint8_t_u_u(p_32.f2, 0xE5L)) , l_1826) , l_1753) <= 0x06L), 0x75L)))) ^ (**l_1616)) >= p_32.f0) ^ p_30.f2), 6)) != p_30.f2)), 1)) <= p_30.f2)) == g_221)))) , (void*)0)) && p_32.f4));
                        if ((*g_99))
                            break;
                    }
                    else
                    { /* block id: 809 */
                        (*g_440) &= (safe_lshift_func_int16_t_s_s(((void*)0 == l_1831[4][3]), g_1487));
                        l_1832 = ((*l_1616) = &g_38);
                        return (***l_1792);
                    }
                }
                else
                { /* block id: 815 */
                    return p_30;
                }
                if (((***g_1176) , ((g_303 = ((((((l_1710 ^= ((safe_add_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u(18446744073709551615UL, ((((safe_rshift_func_int8_t_s_u((+p_30.f0), (((((g_540 &= (g_1208[0] = p_30.f0)) <= ((safe_sub_func_uint32_t_u_u((safe_mul_func_uint8_t_u_u(p_32.f3, ((safe_div_func_uint16_t_u_u(((safe_sub_func_uint8_t_u_u(((((*l_1616) = (*l_1616)) == (void*)0) >= (safe_div_func_uint32_t_u_u(l_1793, (((((*g_99) <= (*g_99)) | (*g_99)) , 247UL) , l_1659)))), p_30.f2)) || (-7L)), p_32.f4)) > (-1L)))), l_1763)) >= p_30.f0)) , 0UL) || (**g_979)) > l_1731))) , p_32.f4) | p_30.f2) != p_30.f2))), p_31)) || p_30.f2)) , l_1851) != l_1852) ^ 9L) > (*g_99)) , g_130.f0)) < 0x2031L)))
                { /* block id: 823 */
                    struct S3 l_1853 = {6045,4081,452,8003,19794};
                    for (l_1637 = 2; (l_1637 >= 0); l_1637 -= 1)
                    { /* block id: 826 */
                        if (p_31)
                            break;
                    }
                    return l_1853;
                }
                else
                { /* block id: 830 */
                    struct S3 *l_1854 = &g_52;
                    int32_t l_1858 = (-9L);
                    int64_t l_1864[5][5][10] = {{{0xDC7FF8D136137FA8LL,0x709375DF5588CDA1LL,0x709375DF5588CDA1LL,0x5333C54B14838F1CLL,0x14E2EF228D6C13B7LL,(-1L),3L,1L,1L,0xCE04DA3CC970636ALL},{0x2683F06B58FF1180LL,0x22E266896DF59541LL,1L,3L,0x37047EE69A1869CFLL,0x49A25E6400AF0278LL,0x37047EE69A1869CFLL,3L,1L,0x22E266896DF59541LL},{0x17AF0E0C55906AC5LL,0xDC7FF8D136137FA8LL,8L,0x5333C54B14838F1CLL,0x709375DF5588CDA1LL,(-1L),(-1L),1L,0x14E2EF228D6C13B7LL,0x14E2EF228D6C13B7LL},{0xDC7FF8D136137FA8LL,0xCE04DA3CC970636ALL,(-1L),0x49A25E6400AF0278LL,0x49A25E6400AF0278LL,(-1L),0xCE04DA3CC970636ALL,0xDC7FF8D136137FA8LL,0x5333C54B14838F1CLL,(-1L)},{0x17AF0E0C55906AC5LL,8L,3L,0xDC7FF8D136137FA8LL,3L,0x49A25E6400AF0278LL,1L,(-1L),1L,0x49A25E6400AF0278LL}},{{0x2683F06B58FF1180LL,3L,3L,3L,0x2683F06B58FF1180LL,(-1L),0x5333C54B14838F1CLL,0xDC7FF8D136137FA8LL,0xCE04DA3CC970636ALL,(-1L)},{0x5333C54B14838F1CLL,0x709375DF5588CDA1LL,(-1L),(-1L),1L,0x14E2EF228D6C13B7LL,0x14E2EF228D6C13B7LL,1L,(-1L),(-1L)},{(-1L),(-1L),8L,(-1L),0x2683F06B58FF1180LL,0x22E266896DF59541LL,1L,3L,0x37047EE69A1869CFLL,0x49A25E6400AF0278LL},{3L,1L,1L,0xCE04DA3CC970636ALL,3L,0xCE04DA3CC970636ALL,1L,1L,3L,(-1L)},{0x709375DF5588CDA1LL,(-1L),0x37047EE69A1869CFLL,0x2683F06B58FF1180LL,0x49A25E6400AF0278LL,0x17AF0E0C55906AC5LL,0x14E2EF228D6C13B7LL,3L,3L,0x14E2EF228D6C13B7LL}},{{1L,0x709375DF5588CDA1LL,0x2683F06B58FF1180LL,0x2683F06B58FF1180LL,0x709375DF5588CDA1LL,1L,0x5333C54B14838F1CLL,0x17AF0E0C55906AC5LL,3L,0x22E266896DF59541LL},{0x22E266896DF59541LL,3L,(-1L),0xCE04DA3CC970636ALL,0x37047EE69A1869CFLL,8L,1L,8L,0x37047EE69A1869CFLL,0xCE04DA3CC970636ALL},{0x22E266896DF59541LL,8L,0x22E266896DF59541LL,(-1L),0x14E2EF228D6C13B7LL,1L,0xCE04DA3CC970636ALL,0x49A25E6400AF0278LL,(-1L),0x17AF0E0C55906AC5LL},{1L,0xCE04DA3CC970636ALL,0x49A25E6400AF0278LL,(-1L),0x17AF0E0C55906AC5LL,0x17AF0E0C55906AC5LL,(-1L),0x49A25E6400AF0278LL,0xCE04DA3CC970636ALL,1L},{0x709375DF5588CDA1LL,0xDC7FF8D136137FA8LL,0x22E266896DF59541LL,3L,(-1L),0xCE04DA3CC970636ALL,0x37047EE69A1869CFLL,8L,1L,8L}},{{3L,0x22E266896DF59541LL,(-1L),0xDC7FF8D136137FA8LL,(-1L),0x22E266896DF59541LL,3L,0x17AF0E0C55906AC5LL,0x5333C54B14838F1CLL,1L},{(-1L),0x37047EE69A1869CFLL,0x2683F06B58FF1180LL,0x49A25E6400AF0278LL,0x17AF0E0C55906AC5LL,0x14E2EF228D6C13B7LL,3L,3L,0x14E2EF228D6C13B7LL,0x17AF0E0C55906AC5LL},{0x5333C54B14838F1CLL,0x37047EE69A1869CFLL,0x37047EE69A1869CFLL,0x5333C54B14838F1CLL,0x14E2EF228D6C13B7LL,(-1L),3L,1L,1L,0xCE04DA3CC970636ALL},{0x2683F06B58FF1180LL,0x22E266896DF59541LL,1L,3L,0x37047EE69A1869CFLL,0x49A25E6400AF0278LL,0x37047EE69A1869CFLL,3L,1L,0x22E266896DF59541LL},{0x17AF0E0C55906AC5LL,0xDC7FF8D136137FA8LL,8L,0x5333C54B14838F1CLL,0x37047EE69A1869CFLL,0x709375DF5588CDA1LL,3L,0x49A25E6400AF0278LL,8L,8L}},{{0x5333C54B14838F1CLL,1L,0x709375DF5588CDA1LL,0x2683F06B58FF1180LL,0x2683F06B58FF1180LL,0x709375DF5588CDA1LL,1L,0x5333C54B14838F1CLL,0x17AF0E0C55906AC5LL,3L},{(-1L),0xCE04DA3CC970636ALL,0xDC7FF8D136137FA8LL,0x5333C54B14838F1CLL,(-1L),0x2683F06B58FF1180LL,3L,3L,3L,0x2683F06B58FF1180LL},{0x22E266896DF59541LL,(-1L),0xDC7FF8D136137FA8LL,(-1L),0x22E266896DF59541LL,3L,0x17AF0E0C55906AC5LL,0x5333C54B14838F1CLL,1L,0x709375DF5588CDA1LL},{0x17AF0E0C55906AC5LL,0x37047EE69A1869CFLL,0x709375DF5588CDA1LL,3L,0x49A25E6400AF0278LL,8L,8L,0x49A25E6400AF0278LL,3L,0x709375DF5588CDA1LL},{3L,3L,0xCE04DA3CC970636ALL,0x709375DF5588CDA1LL,0x22E266896DF59541LL,0x14E2EF228D6C13B7LL,0x49A25E6400AF0278LL,0xDC7FF8D136137FA8LL,1L,0x2683F06B58FF1180LL}}};
                    int i, j, k;
                    for (l_1788 = 0; (l_1788 <= 1); l_1788 += 1)
                    { /* block id: 833 */
                        l_1854 = &p_30;
                        if ((*g_99))
                            continue;
                    }
                    if ((p_32.f1 && (safe_rshift_func_uint16_t_u_s((p_30.f1 , (((((g_652 >= (((((+(((**g_400) |= l_1858) >= p_30.f0)) , (((p_30.f3 || ((safe_unary_minus_func_int8_t_s(((safe_mul_func_uint16_t_u_u(0xE08AL, (safe_lshift_func_uint8_t_u_u((((0xADL & ((((g_1510[0][2].f3 , l_1760[0]) , p_33) < p_30.f1) ^ p_32.f3)) ^ g_293) < l_1858), 7)))) , 0xC6L))) <= 0xB6247D0CL)) >= l_1858) | p_30.f0)) , l_1858) < l_1864[4][0][3]) < 0x69415928L)) , (*g_401)) && (**g_1416)) || 0x1E7AL) != (*g_409))), g_959))))
                    { /* block id: 838 */
                        return p_30;
                    }
                    else
                    { /* block id: 840 */
                        int32_t l_1867 = 0L;
                        int32_t l_1869 = 0x67269F31L;
                        int32_t l_1872 = 0x9CD50BECL;
                        int8_t l_1873 = 4L;
                        int16_t l_1875 = 0x8603L;
                        int32_t l_1876 = 8L;
                        int32_t l_1877 = (-8L);
                        l_1865 = &p_32;
                        --g_1878;
                        (*l_1692) = (*l_1692);
                        return p_30;
                    }
                }
            }
        }
        for (l_1826 = 0; (l_1826 <= 1); l_1826 += 1)
        { /* block id: 851 */
            uint32_t l_1881 = 0x4015E210L;
            uint16_t l_1893 = 0x5B4EL;
            int32_t l_1896 = 0x0E9609DCL;
            int32_t l_1901[1][3];
            int32_t l_1906 = (-9L);
            uint64_t *l_1929 = (void*)0;
            uint64_t **l_1928 = &l_1929;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_1901[i][j] = 0x54A93913L;
            }
            ++l_1881;
            for (l_1659 = 0; (l_1659 <= 9); l_1659 += 1)
            { /* block id: 855 */
                uint32_t l_1908 = 0xC3475BDAL;
                int32_t l_1911 = 0xA08CC1BDL;
                int32_t l_1912 = 7L;
                int32_t l_1913[3][7][4] = {{{(-4L),0x84EC5DEBL,0x4B53677EL,0x5F03875DL},{(-4L),0x4B53677EL,(-4L),(-1L)},{0x84EC5DEBL,0x5F03875DL,(-1L),(-1L)},{0x4B53677EL,0x4B53677EL,(-1L),0x5F03875DL},{0x5F03875DL,0x84EC5DEBL,(-1L),0x84EC5DEBL},{0x4B53677EL,(-4L),(-1L),(-1L)},{0x84EC5DEBL,(-4L),(-4L),0x84EC5DEBL}},{{(-4L),0x84EC5DEBL,0x4B53677EL,0x5F03875DL},{(-4L),0x4B53677EL,(-4L),(-1L)},{0x84EC5DEBL,0x5F03875DL,(-1L),(-1L)},{0x4B53677EL,0x4B53677EL,(-1L),0x5F03875DL},{0x5F03875DL,0x84EC5DEBL,(-1L),0x84EC5DEBL},{0x4B53677EL,(-4L),(-1L),(-1L)},{0x84EC5DEBL,(-4L),(-4L),0x84EC5DEBL}},{{(-4L),0x84EC5DEBL,0x4B53677EL,0x5F03875DL},{(-4L),0x4B53677EL,(-4L),(-1L)},{0x84EC5DEBL,0x5F03875DL,(-1L),(-1L)},{0x4B53677EL,0x4B53677EL,(-1L),0x5F03875DL},{0x5F03875DL,0x84EC5DEBL,(-1L),0x84EC5DEBL},{0x4B53677EL,(-4L),(-1L),(-1L)},{0x84EC5DEBL,(-4L),(-4L),0x84EC5DEBL}}};
                int i, j, k;
                for (g_777 = 0; (g_777 <= 1); g_777 += 1)
                { /* block id: 858 */
                    int32_t l_1888 = (-1L);
                    int32_t l_1889 = 0x34847365L;
                    int32_t l_1899[3][8][6] = {{{(-2L),0x42E931E1L,0L,0xDFD41115L,0L,0x42E931E1L},{0L,(-2L),0x85407A95L,3L,3L,0x85407A95L},{0L,0L,3L,0xDFD41115L,0x86BA6298L,0xDFD41115L},{(-2L),0L,(-2L),0x85407A95L,3L,3L},{0x42E931E1L,(-2L),(-2L),0x42E931E1L,0L,0xDFD41115L},{0xDFD41115L,0x42E931E1L,3L,0x42E931E1L,0xDFD41115L,0x85407A95L},{0x42E931E1L,0xDFD41115L,0x85407A95L,0x85407A95L,0xDFD41115L,0x42E931E1L},{(-2L),0x42E931E1L,0L,0xDFD41115L,0L,0x42E931E1L}},{{0L,(-2L),3L,0L,0L,3L},{0x86BA6298L,0x86BA6298L,0L,0x85407A95L,0x42E931E1L,0x85407A95L},{0xDFD41115L,0x86BA6298L,0xDFD41115L,3L,0L,0L},{(-2L),0xDFD41115L,0xDFD41115L,(-2L),0x86BA6298L,0x85407A95L},{0x85407A95L,(-2L),0L,(-2L),0x85407A95L,3L},{(-2L),0x85407A95L,3L,3L,0x85407A95L,(-2L)},{0xDFD41115L,(-2L),0x86BA6298L,0x85407A95L,0x86BA6298L,(-2L)},{0x86BA6298L,0xDFD41115L,3L,0L,0L,3L}},{{0x86BA6298L,0x86BA6298L,0L,0x85407A95L,0x42E931E1L,0x85407A95L},{0xDFD41115L,0x86BA6298L,0xDFD41115L,3L,0L,0L},{(-2L),0xDFD41115L,0xDFD41115L,(-2L),0x86BA6298L,0x85407A95L},{0x85407A95L,(-2L),0L,(-2L),0x85407A95L,3L},{(-2L),0x85407A95L,3L,3L,0x85407A95L,(-2L)},{0xDFD41115L,(-2L),0x86BA6298L,0x85407A95L,0x86BA6298L,(-2L)},{0x86BA6298L,0xDFD41115L,3L,0L,0L,3L},{0x86BA6298L,0x86BA6298L,0L,0x85407A95L,0x42E931E1L,0x85407A95L}}};
                    int i, j, k;
                    if ((safe_rshift_func_uint16_t_u_s((safe_sub_func_int16_t_s_s(g_1208[(g_777 + 4)], 65535UL)), 14)))
                    { /* block id: 859 */
                        uint32_t l_1890 = 5UL;
                        l_1890++;
                        l_1893++;
                    }
                    else
                    { /* block id: 862 */
                        int32_t l_1897 = 0L;
                        int32_t l_1898 = 0x84BC4CE8L;
                        int32_t l_1900 = 0x4AFC19A5L;
                        int32_t l_1903 = 1L;
                        int i;
                        if (g_1208[l_1659])
                            break;
                        (*g_99) |= 0xEB7D4B2DL;
                        l_1908++;
                    }
                    --g_1915;
                    l_1918--;
                    if (l_1871[(g_148 + 3)][(g_148 + 1)][l_1826])
                        continue;
                }
                (*g_440) = (~(p_30 , (-1L)));
            }
            for (l_1657 = 0; (l_1657 <= 1); l_1657 += 1)
            { /* block id: 875 */
                uint64_t *l_1927 = &g_448;
                uint64_t **l_1926 = &l_1927;
                int32_t l_1940 = 1L;
                const uint64_t l_1941 = 1UL;
                int32_t l_1949 = 0x689D968FL;
                int32_t l_1951 = 0L;
                int32_t l_1953 = 0xCE43C942L;
                struct S3 *l_1979 = &g_314;
                int16_t *l_1993 = &g_111;
                const uint32_t ****l_1997 = &g_1994[6][2][2];
                struct S0 **l_1998[5];
                int i;
                for (i = 0; i < 5; i++)
                    l_1998[i] = &g_544;
                for (l_1870 = 1; (l_1870 >= 0); l_1870 -= 1)
                { /* block id: 878 */
                    uint64_t ***l_1930 = &l_1928;
                    uint16_t *l_1939 = &l_1660;
                    const int32_t l_1943[7][10][3] = {{{0x4CE892A6L,0L,0L},{0xEFA5BE26L,0xDCE00D4CL,(-1L)},{0xD4556507L,(-1L),(-1L)},{0xE86B81CCL,0x10585727L,0L},{0x52FBF545L,(-1L),1L},{0L,0x6DE49CC4L,5L},{(-1L),0xDCE00D4CL,0L},{1L,0x6DE49CC4L,0L},{0xE86B81CCL,(-1L),(-1L)},{0x4ECFE59EL,0x10585727L,1L}},{{0x867334EFL,(-1L),(-2L)},{0x867334EFL,0xDCE00D4CL,(-7L)},{0x4ECFE59EL,0L,0x6DE49CC4L},{0xE86B81CCL,0x9C0FD97CL,0x99C48AB3L},{1L,0x5FCD9238L,1L},{(-1L),(-7L),0x99C48AB3L},{0L,0xDCE00D4CL,0x6DE49CC4L},{0x52FBF545L,0L,(-7L)},{0xE86B81CCL,(-1L),(-2L)},{0xD4556507L,(-1L),1L}},{{0xEFA5BE26L,0L,(-1L)},{0x4CE892A6L,0xDCE00D4CL,0L},{(-1L),(-7L),0L},{0xE86B81CCL,0x5FCD9238L,5L},{(-1L),0x9C0FD97CL,1L},{0x4CE892A6L,0L,0L},{0xEFA5BE26L,0xDCE00D4CL,(-1L)},{0xD4556507L,(-1L),(-1L)},{0xE86B81CCL,0x10585727L,0L},{0x52FBF545L,(-1L),1L}},{{0L,0x6DE49CC4L,5L},{(-1L),0xDCE00D4CL,0L},{1L,0x9FE50864L,0x61C56507L},{3L,0xC98DA941L,0L},{(-1L),0x8F7B7C10L,(-1L)},{(-7L),0x39C4874DL,(-8L)},{(-7L),0x11C2C00FL,0xB2280AC7L},{(-1L),0x9B409A0EL,0x9FE50864L},{3L,(-3L),0x8210E30BL},{0L,(-8L),(-1L)}},{{0x6DE49CC4L,0xB2280AC7L,0x8210E30BL},{0L,0x11C2C00FL,0x9FE50864L},{0x99C48AB3L,0x61C56507L,0xB2280AC7L},{3L,1L,(-8L)},{5L,1L,(-1L)},{0L,0x61C56507L,0L},{(-1L),0x11C2C00FL,0x61C56507L},{(-2L),0xB2280AC7L,0x9B409A0EL},{3L,(-8L),0x345EEB34L},{(-2L),(-3L),(-1L)}},{{(-1L),0x9B409A0EL,0x8E2C000EL},{0L,0x11C2C00FL,0x39C4874DL},{5L,0x39C4874DL,0x39C4874DL},{3L,0x8F7B7C10L,0x8E2C000EL},{0x99C48AB3L,0xC98DA941L,(-1L)},{0L,0x9FE50864L,0x345EEB34L},{0x6DE49CC4L,0x11C2C00FL,0x9B409A0EL},{0L,0x9FE50864L,0x61C56507L},{3L,0xC98DA941L,0L},{(-1L),0x8F7B7C10L,(-1L)}},{{(-7L),0x39C4874DL,(-8L)},{(-7L),0x11C2C00FL,0xB2280AC7L},{(-1L),0x9B409A0EL,0x9FE50864L},{3L,(-3L),0x8210E30BL},{0L,(-8L),(-1L)},{0x6DE49CC4L,0xB2280AC7L,0x8210E30BL},{0L,0x11C2C00FL,0x9FE50864L},{0x99C48AB3L,0x61C56507L,0xB2280AC7L},{3L,1L,(-8L)},{5L,1L,(-1L)}}};
                    int32_t l_1944 = (-2L);
                    int32_t l_1950[1];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                        l_1950[i] = 0x23F9801DL;
                    if (((((safe_sub_func_uint8_t_u_u((safe_mod_func_uint16_t_u_u(l_1652.f2, (p_32.f3 & p_32.f2))), ((*l_1851) , (((p_32.f4 , l_1926) == ((*l_1930) = l_1928)) > ((safe_div_func_uint32_t_u_u(((((safe_mod_func_uint32_t_u_u((safe_add_func_uint16_t_u_u(((((safe_mul_func_int16_t_s_s((((l_1939 != l_1939) >= p_30.f0) < 0x2CL), g_314.f4)) >= l_1940) , 65535UL) , l_1941), 65527UL)), l_1942)) && g_540) != (*g_335)) < g_1866.f4), l_1943[3][1][0])) | p_30.f2))))) != (**l_1616)) ^ 0x02L) != p_32.f2))
                    { /* block id: 880 */
                        int32_t l_1945 = (-1L);
                        int32_t l_1946 = 0L;
                        int32_t l_1947 = 0xB6169CE6L;
                        int32_t l_1948[5] = {0x567778D4L,0x567778D4L,0x567778D4L,0x567778D4L,0x567778D4L};
                        struct S0 l_1961[1] = {{0xE660D06DL,0,0xCE71D628L,0x29B556BDL}};
                        struct S1 *l_1963 = &g_130;
                        struct S2 *l_1971 = &g_143[0];
                        int i;
                        --g_1954;
                        (*g_99) = p_32.f1;
                        (*g_99) |= ((safe_sub_func_uint8_t_u_u((((safe_add_func_int64_t_s_s(((*g_409) = (l_1961[0] , ((safe_unary_minus_func_uint32_t_u(((**g_1176) == l_1963))) | l_1953))), ((safe_lshift_func_int8_t_s_u(((*g_1417) &= (safe_rshift_func_int16_t_s_u((safe_mul_func_uint8_t_u_u((((!(p_30.f2 >= ((l_1896 , (*g_463)) == l_1971))) >= 1L) != g_1866.f0), p_30.f4)), 11))), (*g_401))) >= g_1510[0][2].f4))) == 0x8CL) != (*g_980)), p_31)) > 2UL);
                        (*g_99) = (-2L);
                    }
                    else
                    { /* block id: 887 */
                        int32_t l_1972 = 1L;
                        int32_t l_1973 = 0xBCDEA4FCL;
                        (**l_1616) = p_32.f1;
                        ++g_1975;
                        (*g_99) |= (!((void*)0 == &g_979));
                    }
                    for (g_111 = 0; (g_111 <= 1); g_111 += 1)
                    { /* block id: 894 */
                        (*g_440) = p_32.f0;
                        if (l_1906)
                            continue;
                    }
                    if ((*g_99))
                        break;
                }
                p_32.f0 ^= ((*g_99) = (((**l_1626) = &p_30) == l_1979));
                l_1999 = ((safe_add_func_int16_t_s_s(((-1L) & ((+7UL) , ((safe_lshift_func_uint16_t_u_s((0xF5B324D64F16D9E2LL <= (g_123[0][6][0] || (l_1941 > ((l_1985 != ((*l_1997) = (((l_1881 && (safe_lshift_func_int16_t_s_s(p_32.f4, ((*l_1993) &= (safe_unary_minus_func_uint64_t_u((safe_div_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((**g_1416), p_32.f4)), 0x4F4CC68C05206352LL)))))))) <= 0x57697F4BF060A309LL) , g_1994[6][2][2]))) == 1UL)))), l_1901[0][1])) >= (**l_1616)))), p_30.f4)) , l_1851);
                return p_30;
            }
        }
    }
    (*g_99) |= (((**g_1177) , (*g_544)) , p_32.f3);
    for (g_936 = 0; (g_936 <= 4); g_936 += 1)
    { /* block id: 913 */
        return p_30;
    }
    if (((+(+(safe_sub_func_uint16_t_u_u(((*l_2005) ^= ((*g_916) , (l_1710 ^= g_381))), ((0x5CL ^ p_30.f4) , ((void*)0 != &g_1974)))))) >= ((g_123[0][5][0] , (void*)0) != ((*l_2007) = l_2006))))
    { /* block id: 919 */
        return (*g_316);
    }
    else
    { /* block id: 921 */
        int32_t **l_2008 = &g_440;
        (*g_99) = (*g_99);
        (*g_313) = func_34(g_433);
        (*l_2008) = &l_1905;
    }
    return l_2009;
}


/* ------------------------------------------ */
/* 
 * reads : g_21.f3 g_36 g_45 g_409 g_410 g_21 g_916 g_262 g_99 g_100 g_1416 g_1417 g_293 g_130.f0 g_936 g_130.f2 g_294 g_1092
 * writes: g_36 g_38 g_45 g_52 g_262 g_1208 g_1092
 */
static struct S3  func_34(uint32_t  p_35)
{ /* block id: 2 */
    int32_t *l_37 = &g_38;
    int32_t l_44 = 1L;
    struct S3 l_48[1] = {{5763,-2255,693,10631,10980}};
    uint64_t l_72 = 0x2253E45BD2804091LL;
    const struct S0 *l_1332 = &g_21;
    int32_t l_1445 = (-6L);
    uint32_t l_1461[2][8][5] = {{{4294967289UL,4294967289UL,0x07A5D4BBL,4294967295UL,1UL},{0x97409811L,4294967292UL,4294967286UL,0UL,1UL},{0x9A18A361L,4294967295UL,4294967295UL,0x9A18A361L,0x207F2530L},{1UL,0UL,0x8C8AAD77L,2UL,0UL},{4294967287UL,0x207F2530L,0xDA1E5DBFL,4294967294UL,0x7823C29AL},{0x8C8AAD77L,0xE9AC20B5L,0xCE252CD6L,2UL,2UL},{4294967289UL,4294967287UL,4294967289UL,0x9A18A361L,0x548E3282L},{0xE9AC20B5L,0UL,0x5860EDA3L,0UL,0x67A31338L}},{{4294967295UL,0x06BB4A17L,0x207F2530L,4294967295UL,0xA41BFD17L},{0UL,2UL,0x5860EDA3L,0x67A31338L,0x5860EDA3L},{0xDA1E5DBFL,0xDA1E5DBFL,4294967289UL,0x20DCAEFAL,4294967294UL},{0UL,6UL,0xCE252CD6L,0x3C14E8E4L,0xA1121350L},{1UL,0x7823C29AL,0xDA1E5DBFL,4294967289UL,0x07A5D4BBL},{4294967287UL,6UL,0x8C8AAD77L,0x8C8AAD77L,6UL},{0xA41BFD17L,1UL,0xDA1E5DBFL,0x9A18A361L,4294967294UL},{0UL,0x5860EDA3L,0UL,0x67A31338L,4294967286UL}}};
    struct S1 l_1471 = {11567,34,12926,0xD87CB27EL,337};
    uint32_t **l_1554[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int64_t l_1607[4] = {0x582F4B3DB4ED4942LL,0x582F4B3DB4ED4942LL,0x582F4B3DB4ED4942LL,0x582F4B3DB4ED4942LL};
    uint16_t *l_1608 = &g_1208[8];
    int32_t *l_1609 = &g_1092;
    struct S3 l_1610[9] = {{2791,3547,662,-3208,15459},{2791,3547,662,-3208,15459},{6017,2260,-182,12310,5260},{2791,3547,662,-3208,15459},{2791,3547,662,-3208,15459},{6017,2260,-182,12310,5260},{2791,3547,662,-3208,15459},{2791,3547,662,-3208,15459},{6017,2260,-182,12310,5260}};
    int i, j, k;
    if (((*l_37) = (g_36 ^= g_21.f3)))
    { /* block id: 5 */
        int32_t *l_39 = &g_38;
        int32_t *l_40 = &g_38;
        int32_t *l_41 = &g_38;
        int32_t *l_42 = &g_38;
        int32_t *l_43[1][4][10] = {{{(void*)0,(void*)0,&g_38,(void*)0,(void*)0,&g_38,&g_38,&g_38,&g_38,(void*)0},{(void*)0,&g_38,&g_38,(void*)0,&g_38,&g_38,(void*)0,&g_38,&g_38,(void*)0},{&g_38,(void*)0,&g_38,&g_38,(void*)0,&g_38,&g_38,(void*)0,&g_38,&g_38},{&g_38,&g_38,&g_38,(void*)0,(void*)0,&g_38,(void*)0,(void*)0,&g_38,&g_38}}};
        struct S3 *l_49 = (void*)0;
        struct S3 *l_50 = &l_48[0];
        struct S3 *l_51 = &g_52;
        int i, j, k;
        g_45--;
        (*l_51) = ((*l_50) = l_48[0]);
    }
    else
    { /* block id: 9 */
        struct S3 *l_54 = &g_52;
        struct S3 **l_53 = &l_54;
        struct S3 ***l_55 = &l_53;
        int32_t *l_56 = &g_38;
        int32_t *l_57 = &g_38;
        int32_t l_58 = 0x6E3F992CL;
        int32_t *l_59 = &l_44;
        int32_t *l_60 = &l_44;
        int32_t l_61 = 0x94A7464CL;
        int32_t *l_62 = &g_38;
        int32_t *l_63 = &l_61;
        int32_t l_64 = 1L;
        int32_t *l_65 = &l_58;
        int32_t *l_66 = &l_58;
        int32_t *l_67 = &l_64;
        int32_t *l_68 = &l_64;
        int32_t *l_69 = &l_44;
        int32_t *l_70 = (void*)0;
        int32_t *l_71[6] = {&g_38,&g_38,&g_38,&g_38,&g_38,&g_38};
        const struct S1 l_921 = {-14488,82,18788,0xE39C0CDAL,253};
        uint64_t *l_1371 = (void*)0;
        uint64_t ** const l_1370 = &l_1371;
        uint64_t ** const *l_1369 = &l_1370;
        uint64_t l_1401 = 0xFAB89F16AED24436LL;
        struct S2 l_1481[2][1][7] = {{{{19,79},{19,79},{19,79},{19,79},{19,79},{19,79},{19,79}}},{{{19,79},{19,79},{19,79},{19,79},{19,79},{19,79},{19,79}}}};
        uint32_t l_1485 = 0xFEA37A71L;
        const int32_t l_1518 = 0xEED1AC93L;
        int i, j, k;
    }
    l_37 = &l_44;
    (*l_1609) |= ((safe_mod_func_int64_t_s_s((((((*g_409) , ((*l_1608) = ((safe_add_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(((((*l_1332) , ((*g_916) = (*g_916))) , (*g_99)) | (safe_lshift_func_int16_t_s_u(((~(safe_lshift_func_int8_t_s_u((**g_1416), 0))) ^ ((safe_sub_func_uint16_t_u_u(((safe_sub_func_uint16_t_u_u((*l_37), ((safe_div_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_s((safe_rshift_func_uint16_t_u_u(((((safe_add_func_uint16_t_u_u((*l_37), 65529UL)) || (*l_37)) , 0x265234A36213EEC4LL) & (*g_409)), 4)), 7)), p_35)) >= l_1607[3]))) != 0x0CD7L), g_130.f0)) < 0x8CL)), g_936))), p_35)), g_130.f2)) , (*l_37)), p_35)) , 0UL))) & g_294) , 0x0463L) && 0xF7C3L), 0x214B0F4DC161F7F4LL)) | (*g_409));
    return l_1610[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_929 g_931 g_933 g_934 g_409 g_148 g_410 g_433 g_400 g_401 g_279 g_175 g_960 g_143 g_315 g_316 g_438 g_979 g_935 g_937 g_936 g_980 g_448 g_463 g_314.f2 g_294 g_130.f0 g_473 g_130 g_258 g_916 g_262 g_52 g_260 g_1078 g_545.f2 g_1092 g_99 g_38 g_464 g_652 g_545.f3 g_45 g_959 g_293 g_123 g_1208 g_1177 g_1178 g_852 g_682 g_259 g_314 g_381
 * writes: g_38 g_931 g_932 g_293 g_175 g_410 g_960 g_464 g_279 g_259 g_262 g_381 g_148 g_52.f1 g_440 g_1078 g_294 g_1092 g_99 g_45 g_539 g_652 g_260 g_937 g_936 g_1176 g_1208 g_111 g_682 g_448 g_545.f2 g_52
 */
static uint32_t  func_77(int16_t  p_78, uint8_t  p_79, const struct S1  p_80, struct S3 *** p_81, const struct S3 * p_82)
{ /* block id: 419 */
    int32_t l_924 = 0x6519CE6AL;
    uint32_t **l_945 = &g_335;
    int8_t *l_946 = (void*)0;
    int8_t *l_947 = &g_293;
    int32_t l_948 = 0x6EDC3D59L;
    int8_t *l_949[8][5][6] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}}};
    uint32_t l_950 = 18446744073709551613UL;
    int32_t *l_951 = &g_175;
    int32_t l_955 = (-8L);
    int32_t l_956 = (-7L);
    int32_t l_957[1][5];
    int64_t **l_981[6][6][6] = {{{&g_409,(void*)0,(void*)0,&g_409,(void*)0,&g_409},{&g_409,&g_409,(void*)0,&g_409,(void*)0,&g_409},{&g_409,(void*)0,&g_409,(void*)0,&g_409,&g_409},{&g_409,(void*)0,&g_409,(void*)0,(void*)0,&g_409},{(void*)0,&g_409,&g_409,(void*)0,&g_409,(void*)0},{&g_409,(void*)0,&g_409,(void*)0,(void*)0,&g_409}},{{&g_409,&g_409,&g_409,&g_409,(void*)0,&g_409},{&g_409,(void*)0,&g_409,&g_409,&g_409,&g_409},{&g_409,&g_409,&g_409,&g_409,(void*)0,(void*)0},{&g_409,&g_409,&g_409,&g_409,&g_409,&g_409},{&g_409,&g_409,(void*)0,&g_409,&g_409,&g_409},{(void*)0,&g_409,&g_409,(void*)0,&g_409,&g_409}},{{(void*)0,&g_409,(void*)0,&g_409,&g_409,&g_409},{&g_409,&g_409,&g_409,(void*)0,&g_409,&g_409},{&g_409,&g_409,(void*)0,(void*)0,&g_409,&g_409},{&g_409,&g_409,&g_409,(void*)0,&g_409,(void*)0},{&g_409,&g_409,&g_409,(void*)0,&g_409,&g_409},{&g_409,&g_409,(void*)0,(void*)0,&g_409,&g_409}},{{&g_409,&g_409,&g_409,(void*)0,&g_409,(void*)0},{&g_409,&g_409,&g_409,(void*)0,&g_409,&g_409},{&g_409,(void*)0,(void*)0,&g_409,&g_409,&g_409},{(void*)0,(void*)0,&g_409,(void*)0,&g_409,(void*)0},{(void*)0,&g_409,(void*)0,&g_409,&g_409,(void*)0},{&g_409,&g_409,&g_409,&g_409,&g_409,&g_409}},{{&g_409,&g_409,(void*)0,&g_409,&g_409,&g_409},{&g_409,&g_409,(void*)0,&g_409,&g_409,&g_409},{&g_409,&g_409,&g_409,(void*)0,&g_409,(void*)0},{(void*)0,&g_409,(void*)0,&g_409,&g_409,(void*)0},{(void*)0,&g_409,&g_409,&g_409,&g_409,&g_409},{&g_409,&g_409,(void*)0,&g_409,&g_409,&g_409}},{{(void*)0,&g_409,&g_409,&g_409,&g_409,(void*)0},{(void*)0,&g_409,&g_409,(void*)0,&g_409,&g_409},{&g_409,&g_409,(void*)0,&g_409,(void*)0,&g_409},{&g_409,&g_409,&g_409,&g_409,(void*)0,(void*)0},{&g_409,&g_409,&g_409,&g_409,&g_409,&g_409},{&g_409,&g_409,(void*)0,&g_409,&g_409,&g_409}}};
    int32_t l_982[7][2][10] = {{{(-1L),0xE083EA82L,1L,0xD3FE2C42L,1L,1L,0xD3FE2C42L,1L,0xE083EA82L,(-1L)},{0xE083EA82L,(-6L),0x1DCAA412L,0xD3FE2C42L,(-9L),(-1L),(-9L),0xD3FE2C42L,0x1DCAA412L,(-6L)}},{{5L,1L,(-1L),(-6L),1L,1L,1L,1L,0xE083EA82L,0x1DCAA412L},{1L,1L,(-9L),(-6L),(-1L),1L,5L,1L,(-1L),(-6L)}},{{(-6L),0xD3FE2C42L,(-6L),1L,1L,0x1DCAA412L,5L,5L,0x1DCAA412L,1L},{(-9L),1L,1L,(-9L),(-6L),(-1L),1L,5L,1L,(-1L)}},{{0x1DCAA412L,0xB1DD07A4L,(-6L),0xB1DD07A4L,0x1DCAA412L,0xE083EA82L,1L,1L,1L,1L},{5L,0xE083EA82L,(-9L),(-9L),0xE083EA82L,5L,1L,1L,0x1DCAA412L,1L}},{{0xB1DD07A4L,(-9L),0x1DCAA412L,1L,0x1DCAA412L,(-9L),0xB1DD07A4L,1L,(-1L),(-1L)},{0xB1DD07A4L,(-1L),5L,(-6L),(-6L),5L,(-1L),0xB1DD07A4L,0xE083EA82L,1L}},{{5L,(-1L),0xB1DD07A4L,0xE083EA82L,1L,0xE083EA82L,0xB1DD07A4L,(-1L),5L,(-6L)},{0x1DCAA412L,(-9L),0xB1DD07A4L,1L,(-1L),(-1L),1L,0xB1DD07A4L,(-9L),0x1DCAA412L}},{{(-9L),0xE083EA82L,5L,1L,1L,0x1DCAA412L,1L,1L,5L,0xE083EA82L},{(-6L),0xB1DD07A4L,0x1DCAA412L,0xE083EA82L,1L,1L,1L,1L,0xE083EA82L,0x1DCAA412L}}};
    struct S2 *l_985 = &g_143[1];
    uint8_t **l_1059 = &g_401;
    int32_t l_1144[6][1] = {{0xECE1AE0CL},{2L},{0xECE1AE0CL},{2L},{0xECE1AE0CL},{2L}};
    uint64_t *l_1154 = &g_123[0][1][0];
    uint64_t **l_1153[3][4] = {{&l_1154,(void*)0,&l_1154,&l_1154},{(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,&l_1154,&l_1154,(void*)0}};
    struct S1 *l_1175 = &g_130;
    struct S1 **l_1174 = &l_1175;
    struct S1 ***l_1173[10][2] = {{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174},{&l_1174,&l_1174}};
    int16_t l_1269 = (-4L);
    int i, j, k;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 5; j++)
            l_957[i][j] = (-1L);
    }
lbl_1215:
    for (g_38 = 0; (g_38 <= 9); g_38 = safe_add_func_int64_t_s_s(g_38, 7))
    { /* block id: 422 */
        uint32_t l_925 = 0x9ABF074BL;
        l_925 ^= (l_924 && (-1L));
    }
    (*l_951) &= ((((~(0x62C2L == 1L)) ^ (g_929 == (g_932 = (g_931[0] = g_931[0])))) == (p_79 , (((((l_950 = (((~l_924) <= ((safe_mul_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((safe_div_func_uint8_t_u_u((((l_945 == (*g_933)) | ((*l_947) = ((*g_409) , g_433))) >= 8L), 0x85L)) != l_948), p_80.f2)), 1UL)) != l_948)) | p_80.f4)) == l_948) != p_80.f2) > l_948) ^ (**g_400)))) , l_950);
    for (g_410 = 29; (g_410 <= (-12)); g_410 = safe_sub_func_int32_t_s_s(g_410, 6))
    { /* block id: 432 */
        int32_t *l_954[1];
        int64_t l_958 = 0L;
        const int16_t l_1028 = 0x27B1L;
        const struct S1 l_1102 = {16259,43,-1454,0UL,29};
        struct S0 l_1112[5][6][1] = {{{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}}},{{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}}},{{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}}},{{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}}},{{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}},{{0xF0F13F7FL,1,5UL,5L}},{{0xF8ECFE99L,-0,0xA2688315L,-6L}}}};
        struct S1 *l_1171 = &g_130;
        struct S1 **l_1170 = &l_1171;
        struct S1 ***l_1169[4] = {&l_1170,&l_1170,&l_1170,&l_1170};
        const int32_t *l_1202 = &l_956;
        struct S3 *l_1238 = &g_314;
        struct S0 *l_1266 = (void*)0;
        uint32_t l_1268 = 18446744073709551614UL;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_954[i] = &g_38;
        g_960[2]--;
        if ((safe_mod_func_uint32_t_u_u(((safe_sub_func_uint16_t_u_u((+(!(safe_div_func_int8_t_s_s(p_80.f3, (((safe_lshift_func_uint8_t_u_u(((((~(((*p_82) , ((((((((**p_81) != (g_143[0] , (*g_315))) , (+(((safe_div_func_uint16_t_u_u((g_438 >= (safe_rshift_func_uint8_t_u_s(250UL, (g_979 != (void*)0)))), 0xBAAAL)) , (void*)0) == l_981[1][2][3]))) | p_80.f3) , 0x4A240D44L) , (*g_409)) , (*l_951)) > p_78)) , (-1L))) >= g_410) <= (-2L)) > 1UL), (*l_951))) < 0x21EFL) || 1UL))))), (*l_951))) == p_78), (***g_933))))
        { /* block id: 434 */
            struct S2 *l_987 = &g_143[0];
            struct S2 **l_986 = &l_987;
            int32_t l_1005 = 0x821DE3CEL;
            struct S1 l_1008[1] = {{-27114,22,-26860,0x2C55FDD8L,81}};
            int16_t l_1009 = 0x5434L;
            int32_t l_1033 = 0L;
            int32_t l_1036 = (-2L);
            int32_t l_1037[10][6][4] = {{{0xB0E1E491L,8L,0L,(-6L)},{7L,0x48244850L,0L,0xD559E16CL},{4L,0x9598FCB1L,4L,0x5092D3E1L},{(-6L),0xD559E16CL,(-1L),0x992687E4L},{0x9598FCB1L,0L,0x2320659AL,0xD559E16CL},{0x5CBEB641L,(-6L),0x2320659AL,0x48244850L}},{{0x9598FCB1L,7L,(-1L),0L},{(-6L),4L,4L,(-6L)},{4L,(-6L),0L,0x5CBEB641L},{7L,0x9598FCB1L,0L,0x992687E4L},{(-6L),0x5CBEB641L,0x5092D3E1L,0x992687E4L},{0L,0x9598FCB1L,0x2320659AL,0x5CBEB641L}},{{0xD559E16CL,(-6L),0x2AA8877AL,(-6L)},{0x9598FCB1L,4L,0x5092D3E1L,0L},{0x48244850L,7L,4L,0x48244850L},{7L,(-6L),0x94595262L,0xD559E16CL},{7L,0L,4L,0x992687E4L},{0x48244850L,0xD559E16CL,0x5092D3E1L,0x5092D3E1L}},{{0x9598FCB1L,0x9598FCB1L,0x2AA8877AL,0xD559E16CL},{0xD559E16CL,0x48244850L,0x2320659AL,(-6L)},{0L,7L,0x5092D3E1L,0x2320659AL},{(-6L),7L,0L,(-6L)},{7L,0x48244850L,0L,0xD559E16CL},{4L,0x9598FCB1L,4L,0x5092D3E1L}},{{(-6L),0xD559E16CL,(-1L),0x992687E4L},{0x9598FCB1L,0L,0x2320659AL,0xD559E16CL},{0x5CBEB641L,(-6L),0x2320659AL,0x48244850L},{0x9598FCB1L,7L,(-1L),0L},{(-6L),4L,4L,(-6L)},{4L,(-6L),0L,0x5CBEB641L}},{{7L,0x9598FCB1L,0L,0x992687E4L},{(-6L),0x5CBEB641L,0x5092D3E1L,0x992687E4L},{0L,0x9598FCB1L,0x2320659AL,0x5CBEB641L},{0xD559E16CL,(-6L),0x2AA8877AL,(-6L)},{0x9598FCB1L,4L,0x5092D3E1L,0L},{0x48244850L,7L,4L,0x48244850L}},{{7L,(-6L),0x94595262L,0xD559E16CL},{7L,0L,4L,0x992687E4L},{0x48244850L,0xD559E16CL,0x5092D3E1L,0x5092D3E1L},{0x9598FCB1L,0x9598FCB1L,0x2AA8877AL,0xD559E16CL},{0xD559E16CL,0x48244850L,0x2320659AL,(-6L)},{0L,7L,0x5092D3E1L,0x2320659AL}},{{(-6L),7L,0L,(-6L)},{7L,0x48244850L,0L,0xD559E16CL},{4L,0x9598FCB1L,4L,0x5092D3E1L},{(-6L),0xD559E16CL,(-1L),0x992687E4L},{0x9598FCB1L,0L,0x2320659AL,0xD559E16CL},{0x5CBEB641L,(-6L),0x2320659AL,0x48244850L}},{{(-1L),0x5CBEB641L,0L,0L},{4L,0x8CAF21D2L,0x8CAF21D2L,4L},{0x8CAF21D2L,4L,0xB0E1E491L,(-1L)},{0x5CBEB641L,(-1L),0xD559E16CL,0x2320659AL},{4L,(-1L),0x2AA8877AL,0x2320659AL},{0xB0E1E491L,(-1L),0x94595262L,(-1L)}},{{0x5092D3E1L,4L,0x9598FCB1L,4L},{(-1L),0x8CAF21D2L,0x2AA8877AL,0L},{0L,0x5CBEB641L,0x8CAF21D2L,0L},{0x5CBEB641L,4L,0L,0x5092D3E1L},{0x5CBEB641L,0xB0E1E491L,0x8CAF21D2L,0x2320659AL},{0L,0x5092D3E1L,0x2AA8877AL,0x2AA8877AL}}};
            uint16_t l_1039 = 0xEB25L;
            struct S1 * const l_1110 = &l_1008[0];
            struct S1 * const *l_1109[5] = {&l_1110,&l_1110,&l_1110,&l_1110,&l_1110};
            int64_t * const *l_1124 = &g_409;
            int8_t **l_1125 = &l_949[0][1][0];
            uint64_t l_1126 = 0x22DEA5F6A49C71E9LL;
            const int32_t *l_1185 = (void*)0;
            const int32_t **l_1184 = &l_1185;
            struct S0 l_1195 = {0xD2AD289EL,-1,0x3C8E2D33L,1L};
            struct S3 * const l_1233 = &g_314;
            int32_t *l_1236 = &l_1036;
            int i, j, k;
            if ((((l_982[0][0][1] == (**g_979)) > ((safe_add_func_int64_t_s_s(((p_78 , ((((l_1008[0].f0 = (g_259 = (((((*g_463) = l_985) != ((*l_986) = l_985)) ^ (safe_add_func_uint32_t_u_u((safe_mul_func_uint16_t_u_u(((*l_951) = ((((safe_div_func_uint8_t_u_u((--(*g_401)), (safe_mul_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((0xEE7DL || (~((0x41L < ((*l_947) = (safe_sub_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_u((l_1005 & ((safe_div_func_uint64_t_u_u(((((l_1005 , l_1008[0]) , (void*)0) == &g_544) <= l_1008[0].f1), 0x83510C7D955F10DDLL)) , p_80.f4)), l_1009)) <= p_80.f2), g_314.f2)))) , p_78))), g_294)), p_79)))) & (-1L)) != l_1005) & g_130.f0)), p_80.f1)), g_473))) != p_80.f2))) == l_1009) | p_80.f2) == 0x0A28L)) | l_1009), 0xD211D566C443B874LL)) , 1L)) & (-1L)))
            { /* block id: 442 */
                int64_t **l_1012 = (void*)0;
                int64_t ***l_1013 = &l_1012;
                int32_t l_1029 = 0x6389C3D9L;
                (*l_1013) = ((*l_987) , l_1012);
                l_955 ^= (l_1008[0].f2 = ((safe_add_func_int64_t_s_s(p_80.f3, ((**g_934) <= (g_130 , (safe_sub_func_uint32_t_u_u(0UL, (((safe_div_func_int32_t_s_s((((g_258 <= ((p_80.f2 | (((*g_916) ^= 0x98466093L) , p_80.f0)) && ((safe_add_func_uint32_t_u_u((safe_rshift_func_int16_t_s_s(((safe_rshift_func_int16_t_s_s((safe_mod_func_int32_t_s_s((l_1028 > p_80.f4), l_1008[0].f2)), g_175)) ^ 0xCA1809485B9FF09CLL), 3)), p_80.f4)) == 1L))) <= 1L) <= 1L), (*l_951))) <= l_1008[0].f4) , l_1029))))))) > 18446744073709551615UL));
            }
            else
            { /* block id: 447 */
                int32_t l_1030 = 0xB8D27DA9L;
                int32_t l_1031 = 0x62EBC489L;
                int32_t l_1032 = 1L;
                int32_t l_1034 = (-8L);
                int32_t l_1035[7] = {0x469FE93CL,7L,0x469FE93CL,0x469FE93CL,7L,0x469FE93CL,0x469FE93CL};
                int32_t l_1038 = 9L;
                const uint64_t ***l_1103 = (void*)0;
                int32_t *l_1104 = &g_175;
                int i;
                l_1039--;
                for (g_381 = (-5); (g_381 < 57); g_381 = safe_add_func_int8_t_s_s(g_381, 9))
                { /* block id: 451 */
                    uint32_t l_1044 = 2UL;
                    int32_t **l_1070 = (void*)0;
                    int32_t **l_1071 = &g_440;
                    int32_t l_1072 = 0x17F56B06L;
                    int32_t l_1073 = (-1L);
                    int32_t l_1074 = (-4L);
                    int32_t l_1075 = 0xDEAE12F9L;
                    int32_t l_1076 = 0xD8BD37BBL;
                    int32_t l_1077[6][8][2] = {{{9L,0x40250A6DL},{0x43D0A9C8L,2L},{0x43D0A9C8L,0x40250A6DL},{9L,0x8E21B551L},{0x40250A6DL,1L},{0xECD08D2EL,9L},{1L,0xB664CB68L},{0xB664CB68L,0x9EC06619L}},{{1L,0x572F806BL},{0L,0xECD08D2EL},{0xE58D7903L,(-1L)},{1L,(-1L)},{0xE58D7903L,0xECD08D2EL},{0L,0x572F806BL},{1L,0x9EC06619L},{0xB664CB68L,0xB664CB68L}},{{1L,9L},{0xECD08D2EL,1L},{0x40250A6DL,0x8E21B551L},{9L,0x40250A6DL},{0x43D0A9C8L,2L},{0x43D0A9C8L,0x40250A6DL},{9L,0x8E21B551L},{0x40250A6DL,1L}},{{0xECD08D2EL,9L},{1L,0xB664CB68L},{0xB664CB68L,0x9EC06619L},{1L,0x572F806BL},{0L,0xECD08D2EL},{0xE58D7903L,(-1L)},{1L,(-1L)},{0xE58D7903L,0xECD08D2EL}},{{0L,0x572F806BL},{1L,0x9EC06619L},{0xB664CB68L,0xB664CB68L},{1L,9L},{0xECD08D2EL,1L},{0x40250A6DL,0x8E21B551L},{9L,0x40250A6DL},{0x43D0A9C8L,2L}},{{0x43D0A9C8L,0x40250A6DL},{9L,0x8E21B551L},{0x40250A6DL,1L},{0xECD08D2EL,9L},{1L,0xB664CB68L},{0xB664CB68L,0x9EC06619L},{1L,0x572F806BL},{0L,0xECD08D2EL}}};
                    int i, j, k;
                    (*l_951) ^= ((0UL ^ 0xAE93484EL) ^ ((p_79 != l_1044) != l_1035[0]));
                    for (g_148 = 0; (g_148 == 14); g_148 = safe_add_func_int16_t_s_s(g_148, 2))
                    { /* block id: 455 */
                        l_1033 = (safe_mod_func_uint64_t_u_u((safe_mod_func_int64_t_s_s(((((safe_div_func_int16_t_s_s(g_130.f1, p_79)) ^ (safe_mul_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_s((safe_sub_func_uint64_t_u_u((&g_401 == l_1059), (safe_sub_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u((*g_401), ((***p_81) , (safe_lshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((g_52.f1 = p_80.f2), ((((safe_lshift_func_int16_t_s_s(0xE08CL, 1)) & l_1035[0]) != 0x57D3L) == 0UL))), (*g_401)))))), 0x960AL)))), g_260)) != l_1038), (*l_951)))) <= (-3L)) ^ 65526UL), p_80.f4)), p_80.f1));
                    }
                    (*l_1071) = &l_1031;
                    ++g_1078;
                }
                for (g_279 = 0; (g_279 <= 1); g_279 += 1)
                { /* block id: 464 */
                    int32_t *l_1091[3];
                    struct S0 l_1101 = {0UL,-0,0UL,0L};
                    int32_t l_1105 = (-5L);
                    uint16_t l_1106 = 0x20E0L;
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1091[i] = (void*)0;
                    for (g_294 = 0; (g_294 <= 1); g_294 += 1)
                    { /* block id: 467 */
                        struct S2 **l_1085[5];
                        int32_t **l_1093 = &g_99;
                        uint8_t *l_1098[5];
                        int i, j, k;
                        for (i = 0; i < 5; i++)
                            l_1085[i] = &g_464;
                        for (i = 0; i < 5; i++)
                            l_1098[i] = &g_960[2];
                        g_1092 |= (safe_lshift_func_int8_t_s_s((((*l_951) ^= (0x93F562C4L || ((((((l_982[(g_294 + 5)][g_294][(g_279 + 5)] > ((((g_130 , (l_1085[3] != &g_464)) || (((!(p_79 | p_80.f3)) && (safe_rshift_func_int8_t_s_u(((*l_947) = (safe_div_func_uint8_t_u_u((l_1091[2] == ((((9UL <= (*g_980)) , l_1035[6]) >= 1UL) , &g_175)), g_545.f2))), (*g_401)))) >= l_1036)) && p_80.f2) <= l_982[(g_294 + 5)][g_294][(g_279 + 5)])) , 1L) , p_80.f0) , (**g_979)) < (*g_409)) >= 0xB322C295L))) , p_80.f4), 6));
                        (*l_1093) = &g_1092;
                        (*l_951) |= ((safe_sub_func_uint8_t_u_u(0x8BL, (g_45 = (p_79++)))) <= (**l_1093));
                    }
                    if ((safe_add_func_int64_t_s_s(p_78, (*g_980))))
                    { /* block id: 476 */
                        l_1104 = ((l_1101 , ((l_1102 , l_1103) == (p_78 , &g_979))) , &g_38);
                    }
                    else
                    { /* block id: 478 */
                        return l_1105;
                    }
                    for (p_78 = 4; (p_78 >= 2); p_78 -= 1)
                    { /* block id: 483 */
                        int i, j, k;
                        l_1035[(g_279 + 1)] |= l_982[(g_279 + 5)][g_279][(p_78 + 2)];
                    }
                    for (g_539 = 0; (g_539 <= 0); g_539 += 1)
                    { /* block id: 488 */
                        struct S1 * const **l_1111 = &l_1109[4];
                        ++l_1106;
                        if (p_80.f0)
                            continue;
                        if ((*l_1104))
                            continue;
                        (*l_1111) = l_1109[4];
                    }
                }
            }
            if ((((l_1112[0][2][0] , ((((safe_mod_func_uint32_t_u_u((safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u((p_79 = ((**l_1059) = (safe_mod_func_int8_t_s_s(((((**g_463) , (safe_mul_func_int16_t_s_s(((g_652 |= ((((+((void*)0 != l_1124)) ^ ((*l_951) >= 0xD940D2F2B9F6919ELL)) != ((((-6L) && p_80.f1) , l_1125) == (void*)0)) || 0xEFL)) || l_1033), g_545.f3))) < p_80.f0) , 9L), (**g_400))))), l_1126)), p_80.f4)), p_80.f3)) > p_80.f2) && (*g_409)) != p_80.f2)) <= 1L) || 0x7662L))
            { /* block id: 499 */
                uint32_t l_1131 = 18446744073709551610UL;
                int16_t *l_1132 = (void*)0;
                int16_t *l_1133 = &g_260;
                uint8_t *l_1145 = &g_45;
                uint8_t *l_1146 = &g_960[2];
                int32_t *l_1164 = &l_957[0][3];
                int32_t l_1204 = 0x86FB0C2CL;
                int32_t l_1206 = 1L;
                int32_t l_1207 = 0x5ED03974L;
                uint64_t l_1213[7][1][9] = {{{1UL,0xC55426A198A198BELL,0x136F60BEBB6096CALL,0x136F60BEBB6096CALL,0xC55426A198A198BELL,1UL,0x2A37AA17A00D9ACDLL,1UL,0xC55426A198A198BELL}},{{0x925213DC1DBC9B9FLL,0x17CAF4EFF15B3F37LL,0x17CAF4EFF15B3F37LL,0x925213DC1DBC9B9FLL,0UL,18446744073709551615UL,0UL,0x925213DC1DBC9B9FLL,0x17CAF4EFF15B3F37LL}},{{18446744073709551612UL,18446744073709551612UL,0x2A37AA17A00D9ACDLL,0xC55426A198A198BELL,0UL,0xC55426A198A198BELL,0x2A37AA17A00D9ACDLL,18446744073709551612UL,18446744073709551612UL}},{{0x17CAF4EFF15B3F37LL,0x925213DC1DBC9B9FLL,0UL,18446744073709551615UL,0UL,0x925213DC1DBC9B9FLL,0x17CAF4EFF15B3F37LL,0x17CAF4EFF15B3F37LL,0x17CAF4EFF15B3F37LL}},{{0x136F60BEBB6096CALL,0x57C683BD45725F8BLL,18446744073709551612UL,0x57C683BD45725F8BLL,0x136F60BEBB6096CALL,0x2A37AA17A00D9ACDLL,0x2A37AA17A00D9ACDLL,0x136F60BEBB6096CALL,0x57C683BD45725F8BLL}},{{18446744073709551615UL,0xB2009ABE2C92A710LL,18446744073709551615UL,18446744073709551613UL,0UL,0UL,18446744073709551613UL,18446744073709551615UL,0xB2009ABE2C92A710LL}},{{0UL,0xC55426A198A198BELL,0x2A37AA17A00D9ACDLL,18446744073709551612UL,18446744073709551612UL,0x2A37AA17A00D9ACDLL,0xC55426A198A198BELL,0UL,0xC55426A198A198BELL}}};
                struct S0 l_1214 = {0x7B997824L,0,0x5B1C24DBL,0L};
                int i, j, k;
                if (((safe_rshift_func_uint16_t_u_s(((safe_rshift_func_int8_t_s_s(l_1131, 0)) > ((***g_933) = ((((*l_1133) = (-8L)) , ((*l_1146) ^= ((*l_1145) = ((((*l_951) = (safe_rshift_func_uint16_t_u_s(((g_45 || (safe_mul_func_uint8_t_u_u((6L && ((*l_947) = l_1131)), 0xBBL))) ^ p_80.f2), (safe_rshift_func_uint8_t_u_u(((**l_1059) = (safe_mod_func_uint16_t_u_u(((g_130.f4 <= ((safe_mod_func_uint8_t_u_u((((g_130.f0 < g_438) || l_1009) != (-1L)), p_79)) >= 0xDFL)) >= 0x0FD77654B4508339LL), (*l_951)))), l_1144[5][0]))))) | l_1039) <= 0x7CB7C93811C150CFLL)))) , 0xB3AC5498L))), p_80.f4)) > p_80.f1))
                { /* block id: 507 */
                    int8_t l_1162 = 0x59L;
                    int32_t *l_1166[4] = {&g_1092,&g_1092,&g_1092,&g_1092};
                    int8_t l_1203 = 0xF6L;
                    int i;
                    if (((void*)0 != (*g_400)))
                    { /* block id: 508 */
                        uint64_t l_1161 = 0xCDD7F1C3B5577F54LL;
                        uint16_t *l_1163[7][6] = {{&g_539,&g_539,&g_540,&g_540,&g_539,&g_540},{&g_540,&g_539,&g_540,(void*)0,&g_540,&g_540},{&g_540,&g_540,&g_540,&g_540,&g_539,&g_540},{&g_540,&g_540,&g_540,&g_540,&g_540,(void*)0},{&g_540,&g_540,(void*)0,(void*)0,&g_540,&g_540},{&g_540,&g_540,&g_539,&g_540,&g_539,&g_540},{&g_539,&g_540,(void*)0,&g_540,&g_540,(void*)0}};
                        int32_t **l_1165[9] = {&g_440,&g_440,&g_440,&g_440,&g_440,&g_440,&g_440,&g_440,&g_440};
                        int i, j;
                        l_1036 |= ((g_959 , ((*l_947) |= g_45)) && (safe_lshift_func_uint16_t_u_u((safe_div_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((&g_980 != l_1153[0][3]), ((safe_rshift_func_uint16_t_u_u((0UL || 0x1FL), (l_1037[7][2][0] = ((*l_985) , ((((safe_rshift_func_int8_t_s_s((l_1005 < (safe_mul_func_int16_t_s_s((((void*)0 == &g_545) | 0L), 0L))), 1)) <= l_1161) ^ l_1161) , l_1162))))) != 0x9EL))), 0x88L)), 5)));
                        l_1166[1] = l_1164;
                    }
                    else
                    { /* block id: 513 */
                        struct S1 ****l_1172[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_1172[i] = &l_1169[0];
                        l_1008[0].f0 = (safe_rshift_func_uint8_t_u_u(((g_1176 = (l_1169[1] = (l_1173[0][0] = l_1169[0]))) != &g_1177), 3));
                        if (p_80.f2)
                            continue;
                    }
                    for (g_937 = 0; (g_937 <= 0); g_937 += 1)
                    { /* block id: 522 */
                        int32_t **l_1183 = &g_440;
                        int32_t l_1205[10][8] = {{0x5476A8C0L,2L,0x5701811BL,1L,0L,0L,1L,0x5701811BL},{4L,4L,5L,0x986BF721L,0x19626F8AL,0L,0x16F8BB65L,0x901196E7L},{0x986BF721L,0x20733101L,1L,9L,2L,3L,1L,0x901196E7L},{0x20733101L,0x16F8BB65L,1L,0x986BF721L,1L,(-6L),5L,0x5701811BL},{1L,1L,0x19626F8AL,1L,0L,1L,0x19626F8AL,1L},{9L,0x986BF721L,1L,0x20733101L,(-6L),0x16F8BB65L,0x3C723F1BL,0x6C1BDE1AL},{2L,1L,0L,(-6L),9L,1L,0x3C723F1BL,4L},{3L,(-6L),1L,0x16F8BB65L,1L,5L,0x19626F8AL,0x3C723F1BL},{1L,5L,0x19626F8AL,0x3C723F1BL,0x3C723F1BL,0x19626F8AL,5L,1L},{0x901196E7L,0x5701811BL,1L,0x6C1BDE1AL,4L,0x3C723F1BL,1L,9L}};
                        int i, j;
                        l_1202 = ((safe_mul_func_int8_t_s_s((safe_lshift_func_int8_t_s_u((l_1183 == l_1184), ((*l_1146) &= ((((!(safe_rshift_func_uint16_t_u_s(l_1144[(g_937 + 3)][g_937], 14))) , (void*)0) == &p_80) , (1UL <= (((safe_mul_func_uint8_t_u_u((((&g_315 != (((*g_916) = (safe_add_func_int8_t_s_s((safe_mod_func_int8_t_s_s(((l_1195 , (safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u((safe_sub_func_int32_t_s_s(0L, p_80.f0)), 9)), p_78))) | (*l_951)), (*l_951))), (*l_951)))) , (void*)0)) && 0x2B08168CL) < g_123[0][6][0]), 0xC0L)) < 0xD777L) <= (**g_400))))))), 1UL)) , (*l_1184));
                        g_1208[8]++;
                    }
                }
                else
                { /* block id: 528 */
                    int32_t *l_1211 = &l_1144[4][0];
                    (*l_1184) = l_1211;
                    l_1213[5][0][1] |= (~p_80.f3);
                    (*l_986) = (g_279 , (l_1214 , (*g_463)));
                }
                for (g_111 = 0; (g_111 <= 3); g_111 += 1)
                { /* block id: 535 */
                    uint32_t l_1228 = 1UL;
                    if (g_438)
                        goto lbl_1215;
                    for (g_148 = 3; (g_148 >= 0); g_148 -= 1)
                    { /* block id: 539 */
                        int i;
                        (*l_1164) &= (((*l_1133) = ((**g_1177) , (safe_sub_func_uint8_t_u_u(((**g_400) = ((&l_1185 == (void*)0) == (g_682[3] |= (safe_add_func_int8_t_s_s((((safe_mod_func_uint64_t_u_u((((**g_934) &= 0xEF81D744L) <= ((safe_rshift_func_int16_t_s_u((safe_add_func_int8_t_s_s(g_652, ((*l_1146) ^= (safe_add_func_uint16_t_u_u((l_1228 < (p_78 <= ((((((l_1037[2][1][2] = (safe_rshift_func_int8_t_s_s((l_1228 == ((safe_div_func_uint64_t_u_u((l_1233 != (**p_81)), (**g_979))) , 0x9BC5L)), 4))) > 0xC5420C396AD4C4C3LL) == p_78) == (*g_401)) && p_80.f4) != p_80.f0))), 0UL))))), 15)) > g_852)), (*g_980))) || p_80.f2) & 1UL), 0L))))), g_38)))) , 0x20DB5E90L);
                    }
                }
            }
            else
            { /* block id: 549 */
                const struct S1 *l_1235 = &l_1008[0];
                const struct S1 **l_1234 = &l_1235;
                struct S3 *l_1239[4];
                int32_t l_1240[3];
                int i;
                for (i = 0; i < 4; i++)
                    l_1239[i] = &g_52;
                for (i = 0; i < 3; i++)
                    l_1240[i] = 0x7833CA77L;
                (*l_951) = (-9L);
                (*l_1234) = &p_80;
                for (g_259 = 2; (g_259 >= 0); g_259 -= 1)
                { /* block id: 554 */
                    uint16_t l_1241 = 0x1AF8L;
                    for (g_448 = 0; (g_448 <= 1); g_448 += 1)
                    { /* block id: 557 */
                        int32_t **l_1237 = &l_954[0];
                        int i;
                        (*l_1184) = ((*l_1237) = (l_1236 = &g_38));
                        if (g_682[g_259])
                            continue;
                    }
                    l_1239[1] = l_1238;
                    --l_1241;
                    for (g_545.f2 = 0; (g_545.f2 <= 6); g_545.f2 += 1)
                    { /* block id: 567 */
                        uint64_t l_1244 = 0x7A0D834CC7A0EAF0LL;
                        l_1244--;
                        (*l_1184) = l_1202;
                    }
                    for (p_78 = 0; (p_78 <= 2); p_78 += 1)
                    { /* block id: 573 */
                        (***p_81) = (**g_315);
                        (*l_1184) = &l_957[0][2];
                    }
                }
            }
            (*l_1236) = p_80.f2;
        }
        else
        { /* block id: 580 */
            uint16_t l_1247[1][5][5] = {{{1UL,65535UL,65535UL,1UL,65535UL},{1UL,0xA8B1L,65533UL,65533UL,0xA8B1L},{65535UL,65535UL,65533UL,0x4854L,0x4854L},{65535UL,65535UL,65535UL,65533UL,0x4854L},{0xA8B1L,1UL,0x4854L,1UL,0xA8B1L}}};
            int32_t l_1267[3];
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1267[i] = (-2L);
            l_1247[0][2][3]--;
            l_1269 |= ((**g_1177) , (((((safe_mul_func_uint16_t_u_u(((safe_mod_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s(((((p_80.f0 & (*l_951)) <= (safe_sub_func_int32_t_s_s((safe_rshift_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((&g_1176 == &l_1169[0]), (*l_951))), 6)), 0x4C06D993L))) < (++(*g_980))) & (safe_add_func_int8_t_s_s((((l_1267[1] = (((*l_951) < (l_1266 != &g_21)) >= l_1247[0][3][3])) || 0xF2AA7B9FL) && p_80.f3), l_1268))), g_652)) || l_1267[1]), 9L)) < g_314.f2), 1UL)) ^ (*l_951)) > p_80.f1) , g_381) == (**g_400)));
            (***p_81) = (*g_316);
        }
        (*l_951) &= 0xB0646DEAL;
    }
    (*l_951) = (safe_mod_func_uint16_t_u_u(((safe_div_func_uint8_t_u_u((*l_951), (((**g_979) || ((safe_mul_func_uint16_t_u_u((p_80.f0 >= 0x3DCCFA5CL), (p_80.f3 , 0x78BEL))) ^ (p_80.f2 >= (((safe_sub_func_uint32_t_u_u((safe_mul_func_int8_t_s_s(((*g_409) & ((p_80.f2 | p_80.f3) || 1UL)), p_80.f4)), p_80.f4)) > p_80.f0) || (-7L))))) , (*l_951)))) | p_80.f1), g_52.f3));
    return p_80.f1;
}


/* ------------------------------------------ */
/* 
 * reads : g_45 g_334 g_259 g_265 g_21 g_314.f1 g_221 g_313 g_314 g_52.f3 g_258 g_99 g_38 g_100 g_52.f2 g_303 g_400 g_316 g_36 g_401 g_279 g_52.f4 g_409 g_148 g_438 g_16 g_130.f1 g_315 g_111 g_463 g_473 g_293 g_130.f3 g_335 g_175 g_381 g_440 g_410 g_130 g_260 g_540 g_545.f2 g_294 g_634 g_464 g_652 g_52 g_777 g_143.f0 g_539 g_545.f3 g_852 g_123 g_544 g_545 g_916
 * writes: g_45 g_99 g_148 g_52 g_381 g_38 g_314 g_36 g_409 g_294 g_433 g_123 g_438 g_440 g_448 g_111 g_259 g_293 g_279 g_260 g_410 g_175 g_464 g_100 g_539 g_544 g_316 g_545.f2 g_473 g_334 g_634 g_545.f3 g_652 g_335 g_303 g_540
 */
static int16_t  func_83(uint32_t  p_84, struct S3 * p_85, uint32_t  p_86, const struct S3 ** p_87, struct S3 * p_88)
{ /* block id: 124 */
    struct S3 l_317[3][8] = {{{3209,-2919,496,15581,31930},{4134,1574,-535,1330,23355},{4134,1574,-535,1330,23355},{4134,1574,-535,1330,23355},{8149,3523,-603,-12275,23369},{8149,3523,-603,-12275,23369},{4134,1574,-535,1330,23355},{8149,3523,-603,-12275,23369}},{{4134,1574,-535,1330,23355},{4134,1574,-535,1330,23355},{3209,-2919,496,15581,31930},{4134,1574,-535,1330,23355},{4134,1574,-535,1330,23355},{3209,-2919,496,15581,31930},{4134,1574,-535,1330,23355},{4134,1574,-535,1330,23355}},{{8149,3523,-603,-12275,23369},{4134,1574,-535,1330,23355},{8149,3523,-603,-12275,23369},{8149,3523,-603,-12275,23369},{4134,1574,-535,1330,23355},{8149,3523,-603,-12275,23369},{8149,3523,-603,-12275,23369},{4134,1574,-535,1330,23355}}};
    uint8_t *l_326 = &g_45;
    uint32_t **l_346 = &g_335;
    int64_t * const l_347[2][6] = {{(void*)0,&g_148,&g_148,(void*)0,&g_148,&g_148},{(void*)0,&g_148,&g_148,(void*)0,&g_148,&g_148}};
    struct S2 *l_389[5][1] = {{&g_143[0]},{&g_143[0]},{&g_143[0]},{&g_143[0]},{&g_143[0]}};
    int32_t **l_449 = &g_440;
    struct S1 l_472[3] = {{-11683,59,43619,6UL,329},{-11683,59,43619,6UL,329},{-11683,59,43619,6UL,329}};
    uint32_t l_496 = 5UL;
    int8_t *l_537[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint16_t *l_538 = &g_539;
    uint32_t l_541 = 1UL;
    struct S2 l_569 = {138,101};
    uint32_t l_594 = 18446744073709551615UL;
    uint16_t l_798 = 0x1964L;
    int16_t l_853 = 0x23C1L;
    int32_t *l_919 = (void*)0;
    int32_t *l_920 = &g_38;
    int i, j;
    if ((l_317[0][1] , (((((safe_add_func_uint32_t_u_u((p_84 & (safe_mul_func_int8_t_s_s(0x24L, ((safe_add_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u((((*l_326)++) >= p_84), (+(safe_sub_func_int16_t_s_s((((2L || (g_334[2][1][3] != ((safe_div_func_uint8_t_u_u((safe_div_func_int8_t_s_s(0L, (safe_mod_func_uint32_t_u_u(((0x77DB1310L >= ((((safe_rshift_func_int16_t_s_s(((safe_div_func_int16_t_s_s(l_317[0][1].f4, p_86)) | g_259), 9)) & p_84) , 6L) != p_86)) >= g_265[1][1]), l_317[0][1].f3)))), l_317[0][1].f2)) , l_346))) <= 1UL) ^ p_84), l_317[0][1].f2))))), l_317[0][1].f3)) ^ 4UL)))), 0L)) != 0x5705L) , l_347[0][5]) != (void*)0) ^ l_317[0][1].f3)))
    { /* block id: 126 */
        uint64_t *l_360 = &g_123[0][6][0];
        int32_t l_361[6][1][7] = {{{0x289F59E0L,4L,4L,0x289F59E0L,4L,4L,0x289F59E0L}},{{0x0FBD0330L,0L,0x0FBD0330L,0x0FBD0330L,0L,0x0FBD0330L,0x0FBD0330L}},{{0x289F59E0L,0x289F59E0L,0xA0C366FAL,0x289F59E0L,0x289F59E0L,0xA0C366FAL,0x289F59E0L}},{{0L,0x0FBD0330L,0x0FBD0330L,0L,0x0FBD0330L,0x0FBD0330L,0L}},{{4L,0x289F59E0L,4L,4L,0x289F59E0L,4L,4L}},{{0L,0L,0x923B7875L,0L,0L,0x923B7875L,0L}}};
        int32_t ** const l_388[2] = {&g_99,&g_99};
        struct S2 *l_391 = &g_143[1];
        struct S1 *l_442 = &g_130;
        int i, j, k;
        if ((((safe_sub_func_int64_t_s_s((safe_mod_func_int8_t_s_s(((((void*)0 == &p_84) || ((safe_rshift_func_int8_t_s_s((safe_rshift_func_uint8_t_u_u(((*l_326) &= (safe_rshift_func_uint16_t_u_s((safe_lshift_func_int8_t_s_s((p_86 == (l_317[0][1].f1 == (((((g_21 , p_84) , &g_123[0][6][1]) != (l_317[0][1] , l_360)) <= (-1L)) >= 65535UL))), 0)), 10))), p_84)), 0)) != l_317[0][1].f3)) , g_21.f0), g_314.f1)), l_361[2][0][6])) && p_86) , g_221))
        { /* block id: 128 */
            int32_t **l_362 = &g_99;
            uint16_t l_397[3][7][8] = {{{65535UL,0UL,0x66EEL,0x6CA5L,0x6CA5L,0x66EEL,0UL,65535UL},{65526UL,0UL,0xA56BL,1UL,1UL,65535UL,65528UL,65531UL},{0x6CA5L,65535UL,1UL,8UL,1UL,65535UL,0UL,0xA56BL},{0UL,0UL,8UL,0UL,0UL,0x66EEL,65531UL,1UL},{0xA56BL,0UL,0x2767L,65535UL,65531UL,65535UL,0x2767L,0UL},{0x6CA5L,0x2767L,65535UL,0UL,65528UL,65526UL,8UL,65528UL},{0UL,1UL,0x9863L,0UL,0x6CA5L,0x281BL,8UL,0UL}},{{0x2767L,0UL,65535UL,65526UL,0xB209L,65526UL,0x2767L,65531UL},{0xB209L,65526UL,0x2767L,65531UL,0x7B48L,0x7B48L,65531UL,0x2767L},{1UL,1UL,8UL,0x6CA5L,0UL,0x67ADL,0UL,8UL},{65535UL,65535UL,1UL,0x2767L,0xB209L,65535UL,65528UL,8UL},{65535UL,0x7B48L,0xA56BL,0x6CA5L,1UL,0xA56BL,0UL,0x5ECDL},{65528UL,65526UL,0x6CA5L,0x7B48L,65526UL,0UL,65526UL,0x7B48L},{0xA3F8L,1UL,0xA3F8L,65535UL,0x7B48L,0x66EEL,0x2A88L,1UL}},{{1UL,8UL,0x9863L,65528UL,8UL,0x2A88L,0x7B48L,0x67ADL},{1UL,65535UL,0xD811L,65526UL,0x7B48L,0UL,65535UL,65535UL},{0xA3F8L,65535UL,0x66EEL,8UL,65526UL,65535UL,65535UL,65526UL},{65528UL,0x67ADL,0x67ADL,65528UL,0xA3F8L,0x710CL,0x2767L,0x2A88L},{65535UL,65528UL,0UL,0x2767L,0xA56BL,1UL,65535UL,0x7B48L},{0x66EEL,65528UL,0x5ECDL,0x281BL,65535UL,0x710CL,0x281BL,65535UL},{65526UL,0x67ADL,0x9863L,65535UL,0x710CL,65535UL,1UL,65535UL}}};
            uint32_t ** const *l_407[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
            int64_t *l_413[9][6][4] = {{{&g_148,&g_410,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148},{(void*)0,(void*)0,&g_148,&g_148},{&g_148,&g_148,(void*)0,&g_148},{&g_410,(void*)0,&g_148,(void*)0},{&g_410,(void*)0,&g_148,&g_148}},{{(void*)0,&g_148,&g_148,&g_148},{&g_148,(void*)0,(void*)0,&g_148},{&g_410,&g_148,&g_410,&g_148},{&g_410,&g_410,&g_148,&g_148},{&g_410,&g_148,&g_148,&g_148},{&g_410,&g_148,&g_148,&g_148}},{{&g_410,&g_148,(void*)0,&g_148},{&g_410,&g_148,&g_410,&g_148},{&g_410,&g_410,(void*)0,&g_410},{&g_410,&g_148,&g_148,&g_148},{&g_410,&g_148,&g_148,&g_148},{&g_410,&g_410,&g_148,&g_148}},{{&g_410,&g_148,&g_410,&g_148},{&g_410,(void*)0,(void*)0,&g_410},{&g_148,&g_148,&g_148,&g_148},{(void*)0,&g_148,&g_148,&g_148},{&g_410,&g_410,&g_148,&g_148},{&g_410,&g_148,(void*)0,&g_148}},{{&g_148,&g_148,&g_148,&g_410},{(void*)0,(void*)0,&g_148,&g_148},{&g_148,&g_148,&g_148,&g_148},{&g_148,&g_410,&g_410,&g_148},{&g_410,&g_148,&g_148,(void*)0},{&g_148,&g_410,(void*)0,&g_148}},{{&g_410,&g_148,&g_148,&g_410},{&g_410,&g_148,&g_148,&g_148},{&g_410,&g_148,(void*)0,&g_148},{&g_148,&g_410,&g_148,&g_410},{&g_148,&g_410,&g_148,&g_148},{&g_148,&g_148,&g_148,(void*)0}},{{&g_410,(void*)0,(void*)0,(void*)0},{&g_410,&g_410,&g_410,&g_410},{&g_148,&g_148,&g_148,&g_410},{(void*)0,&g_148,(void*)0,&g_148},{(void*)0,&g_148,&g_148,&g_410},{&g_148,&g_148,&g_410,&g_410}},{{&g_148,&g_410,&g_148,(void*)0},{&g_148,(void*)0,&g_148,(void*)0},{(void*)0,&g_148,&g_410,&g_148},{&g_148,&g_410,&g_148,&g_410},{&g_148,&g_410,(void*)0,&g_148},{(void*)0,&g_148,&g_148,&g_148}},{{&g_148,&g_148,(void*)0,&g_410},{&g_148,&g_148,&g_148,&g_148},{(void*)0,&g_410,(void*)0,(void*)0},{&g_148,&g_148,&g_148,&g_410},{&g_148,(void*)0,&g_410,&g_148},{(void*)0,&g_410,&g_148,&g_410}}};
            struct S1 *l_450[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i, j, k;
lbl_441:
            (*l_362) = &g_38;
            for (g_148 = 0; (g_148 > (-10)); --g_148)
            { /* block id: 132 */
                uint32_t l_380 = 0xB4996438L;
                int32_t l_382 = 0L;
                int32_t ** const l_386 = &g_99;
                struct S1 l_387 = {22826,137,-29155,4294967295UL,158};
                uint8_t **l_402 = &g_401;
                uint32_t ** const **l_408 = &l_407[1];
                int64_t *l_412 = &g_148;
                int64_t **l_411[5][2][3] = {{{&l_412,&l_412,&l_412},{&l_412,&l_412,&l_412}},{{&l_412,&l_412,&l_412},{&l_412,&l_412,&l_412}},{{&l_412,&l_412,&l_412},{&l_412,&l_412,&l_412}},{{&l_412,&l_412,&l_412},{&l_412,&l_412,&l_412}},{{&l_412,&l_412,&l_412},{&l_412,&l_412,&l_412}}};
                uint32_t *l_414 = (void*)0;
                uint32_t *l_415 = &g_294;
                int i, j, k;
                (*p_85) = (*g_313);
                for (p_84 = 0; (p_84 <= 0); p_84 += 1)
                { /* block id: 136 */
                    int32_t *l_383 = &l_382;
                    struct S2 **l_390 = &l_389[0][0];
                    int32_t **l_392 = &l_383;
                    int32_t l_396[5][10][5] = {{{1L,4L,(-8L),0xFCF3309EL,6L},{(-1L),5L,6L,1L,0xE65E2219L},{0xD8F2B562L,7L,0x9F4E4425L,(-3L),0x9F4E4425L},{0xD0329CD6L,0xD0329CD6L,0xC1ADC8FDL,0L,5L},{1L,0xFCF3309EL,0L,1L,(-3L)},{0x0EDAA46BL,7L,0xC97F00F2L,(-1L),(-3L)},{7L,0xFCF3309EL,0xA0F3FFA1L,0xD8F2B562L,(-8L)},{0x43CEBE81L,0xD0329CD6L,(-1L),0xD0329CD6L,0x43CEBE81L},{0L,7L,8L,1L,0x94130D6FL},{0L,5L,1L,0x0EDAA46BL,(-1L)}},{{8L,4L,(-4L),0x2A39CCFAL,0x36928767L},{0x0EDAA46BL,0x43CEBE81L,(-1L),0xCE793188L,0xCE793188L},{0x36928767L,5L,0x36928767L,0xFCF3309EL,0x9F4E4425L},{6L,0xC97F00F2L,(-1L),(-1L),0x0EDAA46BL},{5L,0x9F4E4425L,1L,0L,0L},{(-1L),1L,(-1L),0x0EDAA46BL,0L},{0x94130D6FL,0x1C9231ABL,0x36928767L,0x36928767L,0x1C9231ABL},{0L,7L,(-1L),6L,0xD077DB20L},{1L,4L,(-1L),5L,7L},{(-1L),0xC1ADC8FDL,(-3L),(-1L),1L}},{{1L,(-8L),0L,0x94130D6FL,0x2A39CCFAL},{0L,1L,1L,0L,(-1L)},{0x94130D6FL,0L,(-8L),1L,(-3L)},{(-1L),(-3L),0xC1ADC8FDL,(-1L),0L},{5L,(-1L),4L,1L,6L},{6L,(-1L),7L,0L,7L},{0x36928767L,0x36928767L,0x1C9231ABL,0x94130D6FL,(-1L)},{0x0EDAA46BL,(-1L),1L,(-1L),0L},{0L,1L,0x9F4E4425L,5L,8L},{(-1L),(-1L),0xC97F00F2L,6L,0xC1ADC8FDL}},{{0xFCF3309EL,0x36928767L,5L,0x36928767L,0xFCF3309EL},{0xCE793188L,(-1L),0x43CEBE81L,0x0EDAA46BL,0xE65E2219L},{0x2A39CCFAL,(-1L),1L,0L,5L},{0x43CEBE81L,(-3L),0L,(-1L),0xE65E2219L},{8L,0L,0x2A39CCFAL,0xFCF3309EL,0xFCF3309EL},{0xE65E2219L,1L,0xE65E2219L,0xCE793188L,0xC1ADC8FDL},{0L,(-8L),0xA0F3FFA1L,0x2A39CCFAL,8L},{1L,0xC1ADC8FDL,0x8A15FCD9L,0x43CEBE81L,0L},{0L,4L,0xA0F3FFA1L,8L,(-1L)},{0xD0329CD6L,7L,0xE65E2219L,0xE65E2219L,7L}},{{0L,0x1C9231ABL,0x2A39CCFAL,0L,6L},{(-3L),1L,0L,1L,0L},{0xA0F3FFA1L,0x9F4E4425L,1L,0L,(-3L)},{(-3L),0xC97F00F2L,0x43CEBE81L,0xD0329CD6L,(-1L)},{0L,5L,5L,0L,0x2A39CCFAL},{0xD0329CD6L,0x43CEBE81L,0xC97F00F2L,(-3L),1L},{0L,1L,0x9F4E4425L,0xA0F3FFA1L,7L},{1L,0L,1L,(-3L),0xD077DB20L},{0L,0x2A39CCFAL,0x1C9231ABL,(-4L),1L},{0xD077DB20L,0xD077DB20L,0x8A15FCD9L,0xE65E2219L,0xD0329CD6L}}};
                    int i, j, k;
                    (*g_99) = ((safe_mod_func_int16_t_s_s((((18446744073709551615UL < (safe_sub_func_int16_t_s_s(0x48EEL, (0L > (g_381 = (l_380 = (!((safe_add_func_uint64_t_u_u(((safe_rshift_func_int16_t_s_s(0x7F5AL, 9)) < g_52.f3), (safe_lshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((p_84 <= 0x3428C04C170D7808LL) < (safe_div_func_int32_t_s_s(((void*)0 != &g_260), (-5L)))), 4)), g_258)))) >= l_361[5][0][1])))))))) < 0xBF6DL) , 0x9E67L), l_382)) , (**l_362));
                    l_383 = &g_100;
                    if ((safe_rshift_func_uint16_t_u_s((((l_386 == (l_387 , l_388[1])) ^ ((((*l_390) = l_389[2][0]) != l_391) < ((void*)0 == l_392))) , (((((*l_326) = (**l_392)) <= (((((safe_mod_func_int8_t_s_s(g_52.f2, 247UL)) == 65532UL) , p_86) >= g_303) , (**l_386))) > 0xFD88F71A0FEE5A66LL) || l_317[0][1].f1)), 5)))
                    { /* block id: 143 */
                        int32_t l_395[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_395[i] = 0x595F58C2L;
                        (*l_392) = ((*l_386) = (*l_362));
                        --l_397[2][0][2];
                        l_402 = g_400;
                    }
                    else
                    { /* block id: 148 */
                        (*g_313) = (*g_316);
                    }
                    for (g_36 = 0; (g_36 <= 4); g_36 += 1)
                    { /* block id: 153 */
                        int i, j, k;
                        (*l_392) = (*l_362);
                        if (l_317[0][1].f4)
                            continue;
                        l_361[(p_84 + 1)][p_84][(g_36 + 1)] ^= p_84;
                    }
                }
                if ((safe_lshift_func_uint8_t_u_u(((((((&g_401 == (void*)0) , 0L) ^ (-4L)) >= (safe_div_func_uint8_t_u_u((((*l_415) = ((((((**l_386) &= (((*l_408) = l_407[2]) != &g_334[0][1][5])) , l_360) == (l_413[3][4][0] = (g_409 = &g_148))) , (g_314.f4 < g_21.f0)) != p_84)) > p_86), (*g_401)))) == p_86) <= 0x8ABC2B3C5BA28970LL), 2)))
                { /* block id: 164 */
                    struct S1 *l_429 = &l_387;
                    struct S1 **l_428 = &l_429;
                    uint16_t *l_432[10] = {&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2],&l_397[2][0][2]};
                    int32_t l_439 = 0xDB28FE34L;
                    int i;
                    if ((safe_mul_func_int64_t_s_s(p_86, (((safe_mul_func_uint16_t_u_u(g_303, (l_439 |= ((g_314.f3 > ((safe_lshift_func_uint16_t_u_s((g_438 &= (g_52.f4 <= (safe_mod_func_int16_t_s_s(l_317[0][1].f4, (safe_mod_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_s((((*l_428) = &l_387) != (void*)0), 7)) , (safe_mul_func_uint16_t_u_u((g_433 = l_317[0][1].f1), ((g_123[0][6][0] = (safe_sub_func_int16_t_s_s(((safe_rshift_func_int8_t_s_u((((**p_87) , (*g_99)) ^ 0x6F75F420L), (**g_400))) != p_84), 65533UL))) != (-1L))))), (*g_409))))))), 5)) >= 0xFD69753BL)) >= 252UL)))) & g_16) == g_21.f3))))
                    { /* block id: 170 */
                        g_440 = ((*l_386) = (*l_386));
                        if (p_86)
                            goto lbl_441;
                    }
                    else
                    { /* block id: 174 */
                        uint64_t *l_447 = &g_448;
                        if ((**l_386))
                            break;
                        (*g_99) &= p_84;
                        l_317[0][1].f3 &= (((((g_21 , l_442) != (void*)0) , (safe_mul_func_int8_t_s_s(g_221, ((g_130.f1 | (((*l_447) = (g_123[0][3][0] = 1UL)) , ((((**l_386) , p_86) , l_449) == &g_99))) | 0xA8B86CE1C01A14A0LL)))) && p_86) && 0xBF19587DL);
                        l_439 ^= ((**l_386) |= (1UL | (((**g_315) , l_442) == &g_130)));
                    }
                }
                else
                { /* block id: 183 */
                    struct S1 **l_451[8] = {&l_442,&l_442,&l_442,&l_442,&l_442,&l_442,&l_442,&l_442};
                    int i;
                    l_442 = l_450[1];
                }
                if ((*g_99))
                    continue;
            }
        }
        else
        { /* block id: 188 */
            int16_t *l_454[4];
            int32_t l_455[7][6][6] = {{{(-7L),0x29905270L,0x82225183L,0x3C404A4CL,(-1L),(-6L)},{(-6L),0x3C404A4CL,0x1C18EDC4L,0x5D5689A9L,(-1L),(-1L)},{(-1L),0x29905270L,0x7F6E0C2EL,0x82225183L,(-7L),0x1B809683L},{0x29905270L,0xCA5A9F7FL,0x3C404A4CL,0x5D5689A9L,(-6L),(-1L)},{0x1B809683L,(-1L),0x3C404A4CL,0x3C404A4CL,(-1L),0x1B809683L},{(-4L),0x3C404A4CL,0x7F6E0C2EL,(-1L),0x29905270L,(-1L)}},{{0x5D5689A9L,(-1L),0x1C18EDC4L,0x82225183L,0x1B809683L,(-6L)},{0x5D5689A9L,0xCA5A9F7FL,0x82225183L,(-1L),(-4L),(-1L)},{(-4L),0x29905270L,0xCA5A9F7FL,0x3C404A4CL,0x5D5689A9L,(-6L)},{0x1B809683L,0x3C404A4CL,(-1L),0x5D5689A9L,0x5D5689A9L,(-1L)},{0x29905270L,0x29905270L,(-1L),0x82225183L,(-4L),0x1B809683L},{(-1L),0xCA5A9F7FL,0x076EA888L,0x5D5689A9L,0x1B809683L,(-1L)}},{{(-6L),(-1L),0x076EA888L,0x3C404A4CL,0x29905270L,0x1B809683L},{(-7L),0x3C404A4CL,(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),(-1L),0x82225183L,(-6L),(-6L)},{(-1L),0xCA5A9F7FL,0xCA5A9F7FL,(-1L),(-7L),(-1L)},{(-7L),0x29905270L,0x82225183L,0x3C404A4CL,(-1L),(-6L)},{(-6L),0x3C404A4CL,0x1C18EDC4L,0x5D5689A9L,(-1L),(-1L)}},{{(-1L),0x29905270L,0x7F6E0C2EL,0x82225183L,(-7L),0x1B809683L},{0x29905270L,0xCA5A9F7FL,0x3C404A4CL,0x5D5689A9L,(-6L),(-1L)},{0x1B809683L,(-1L),0x3C404A4CL,0x3C404A4CL,(-1L),0x1B809683L},{(-4L),0x3C404A4CL,0x7F6E0C2EL,(-1L),0x29905270L,(-1L)},{0x5D5689A9L,(-1L),0x1C18EDC4L,0x82225183L,0x1B809683L,(-6L)},{0x5D5689A9L,0xCA5A9F7FL,0x82225183L,(-1L),(-4L),(-1L)}},{{(-4L),0x29905270L,0xCA5A9F7FL,0x3C404A4CL,0x5D5689A9L,(-6L)},{0x1B809683L,0x3C404A4CL,(-1L),0x5D5689A9L,0x5D5689A9L,(-1L)},{0x29905270L,0x29905270L,(-1L),0x82225183L,(-4L),0x1B809683L},{(-1L),0xCA5A9F7FL,0x076EA888L,0x5D5689A9L,0x1B809683L,(-1L)},{(-6L),(-1L),0x076EA888L,0x3C404A4CL,0x29905270L,0x1B809683L},{(-7L),0x3C404A4CL,(-1L),(-1L),(-1L),(-1L)}},{{(-1L),(-1L),(-1L),0x82225183L,(-6L),(-6L)},{(-1L),0xCA5A9F7FL,0xCA5A9F7FL,(-1L),(-7L),(-1L)},{(-7L),0x29905270L,0x82225183L,0x3C404A4CL,(-1L),(-6L)},{(-6L),0x3C404A4CL,0x1C18EDC4L,0x5D5689A9L,(-1L),(-1L)},{(-1L),0x29905270L,0x7F6E0C2EL,0x82225183L,(-7L),0x1B809683L},{0x29905270L,0xCA5A9F7FL,0x3C404A4CL,0x5D5689A9L,(-6L),(-1L)}},{{0x1B809683L,(-1L),0x3C404A4CL,0x3C404A4CL,(-1L),0x1B809683L},{(-4L),0x3C404A4CL,0x7F6E0C2EL,(-1L),0x29905270L,(-1L)},{0x5D5689A9L,(-1L),0x1C18EDC4L,0x82225183L,0x1B809683L,(-6L)},{0x5D5689A9L,0xCA5A9F7FL,0x82225183L,(-1L),(-4L),(-1L)},{(-4L),0x29905270L,0xCA5A9F7FL,0x3C404A4CL,0x5D5689A9L,(-6L)},{0x1B809683L,0x3C404A4CL,(-1L),0x5D5689A9L,0x5D5689A9L,(-1L)}}};
            int i, j, k;
            for (i = 0; i < 4; i++)
                l_454[i] = &g_260;
            l_455[6][4][5] |= (safe_lshift_func_int16_t_s_s((g_111 &= 0x6D5EL), g_52.f2));
            return g_45;
        }
    }
    else
    { /* block id: 193 */
        uint8_t *l_456 = &g_45;
        uint64_t *l_459 = &g_123[0][6][0];
        int32_t l_460 = (-5L);
        int8_t *l_465[3][9] = {{&g_293,&g_293,&g_36,&g_36,&g_293,&g_293,&g_36,&g_36,&g_293},{&g_36,&g_259,&g_36,&g_259,&g_36,&g_259,&g_36,&g_259,&g_36},{&g_293,&g_36,&g_36,&g_293,&g_293,&g_36,&g_36,&g_293,&g_293}};
        int32_t l_474[7] = {1L,1L,1L,1L,1L,1L,1L};
        uint32_t ***l_517 = &g_334[2][1][3];
        int i, j;
        l_474[5] |= (((void*)0 != l_456) <= ((((*l_459) = (safe_div_func_int16_t_s_s(p_84, p_84))) != l_460) , (safe_add_func_uint8_t_u_u((**g_400), ((((((((g_259 = (g_463 != (void*)0)) >= ((((safe_lshift_func_int8_t_s_s((g_293 |= (safe_add_func_uint16_t_u_u(((((safe_div_func_uint64_t_u_u((l_472[0] , 0x815BF7E9FE5C63CDLL), g_314.f4)) || g_473) , g_52.f4) <= p_86), 65532UL))), 7)) >= 1L) == l_460) && p_86)) , l_460) , p_86) & p_86) || p_84) && p_86) & g_130.f3)))));
        if ((1L < (safe_add_func_uint16_t_u_u(l_474[0], (safe_rshift_func_int8_t_s_u(p_84, 2))))))
        { /* block id: 198 */
            uint32_t **l_492 = (void*)0;
            struct S1 l_500 = {-2978,106,15671,0x6E8F03F4L,49};
            int8_t l_512 = 0x01L;
            for (g_293 = 21; (g_293 > 25); g_293++)
            { /* block id: 201 */
                int64_t l_484 = 0x19D62B9EE0C3FDE4LL;
                int32_t l_487 = (-1L);
                int32_t *l_495 = &l_460;
                (*l_495) |= ((**g_400) ^ (((+(safe_sub_func_uint8_t_u_u(((*g_335) <= (l_484 > l_474[5])), (l_487 = ((*l_326)--))))) || g_175) & (safe_rshift_func_int8_t_s_s((safe_mod_func_uint64_t_u_u((l_492 != l_492), g_381)), ((safe_sub_func_uint16_t_u_u(p_86, p_84)) >= p_84)))));
                (*l_495) = 0x9BD3302BL;
                l_496 ^= (*l_495);
                for (g_448 = 0; (g_448 <= 2); g_448 += 1)
                { /* block id: 209 */
                    uint64_t l_504[3][7] = {{0x41CD933EFBF38546LL,0x41CD933EFBF38546LL,18446744073709551615UL,0UL,0UL,0UL,18446744073709551615UL},{0x41CD933EFBF38546LL,0x41CD933EFBF38546LL,18446744073709551615UL,0UL,0UL,0UL,18446744073709551615UL},{0x41CD933EFBF38546LL,0x41CD933EFBF38546LL,18446744073709551615UL,0UL,0UL,0UL,18446744073709551615UL}};
                    int32_t *l_507 = &g_38;
                    int i, j;
                    for (g_279 = 0; (g_279 <= 2); g_279 += 1)
                    { /* block id: 212 */
                        int16_t *l_499 = &g_260;
                        int32_t *l_502 = (void*)0;
                        int32_t *l_503 = &g_175;
                        (*l_503) = ((((*l_326) = (safe_mod_func_int8_t_s_s((-8L), (-7L)))) ^ (p_84 < 0xA13A7113L)) || ((*g_409) = (((((*l_495) = (((((*l_499) = p_84) , (l_500 , &g_410)) == ((+p_86) , l_347[0][5])) , p_84)) ^ g_259) | 0xB5L) || 1UL)));
                        (*g_463) = (void*)0;
                        (*l_449) = &l_474[3];
                    }
                    ++l_504[0][3];
                    for (g_45 = 0; (g_45 <= 2); g_45 += 1)
                    { /* block id: 224 */
                        l_507 = (void*)0;
                    }
                }
            }
            (*l_449) = &g_38;
            l_472[0].f0 ^= (safe_mul_func_uint8_t_u_u(((((**l_449) && (((**l_449) ^= (-1L)) && (*g_440))) > ((safe_div_func_int32_t_s_s(p_84, (*g_335))) ^ (0x53L && l_512))) , (((safe_sub_func_int32_t_s_s((safe_unary_minus_func_int64_t_s((&g_401 == (void*)0))), (-8L))) , p_86) , (*g_401))), 0xF6L));
        }
        else
        { /* block id: 232 */
            uint32_t *l_516[4];
            int32_t *l_518 = (void*)0;
            int32_t *l_519 = &g_100;
            int32_t *l_520 = &g_38;
            int i;
            for (i = 0; i < 4; i++)
                l_516[i] = (void*)0;
            (*l_520) ^= ((*l_519) = ((g_21 , (p_84 |= (*g_335))) != (((((**g_400) || ((void*)0 == l_517)) > (-1L)) == (((p_86 > (**g_400)) > ((*g_409) |= (((**g_315) , &g_111) != &g_111))) ^ 18446744073709551615UL)) > g_259)));
            for (g_148 = (-30); (g_148 <= 17); g_148 = safe_add_func_int16_t_s_s(g_148, 7))
            { /* block id: 239 */
                const uint32_t l_523 = 0x8CFBE9B8L;
                if (l_523)
                    break;
            }
        }
    }
    if ((p_84 , ((safe_div_func_int64_t_s_s((((((safe_add_func_uint32_t_u_u(1UL, (safe_mod_func_int16_t_s_s((((*l_538) = (((((void*)0 == &p_85) == (((((safe_unary_minus_func_int8_t_s(p_84)) , g_130) , (safe_lshift_func_int16_t_s_s((((((safe_div_func_int8_t_s_s((l_317[0][1].f1 = (0x831C16027D64C9FELL || (safe_sub_func_int32_t_s_s(g_260, (&g_123[0][6][0] == &g_448))))), 248UL)) | 0xCEAAL) != (*g_409)) , p_86) , g_38), 5))) , 8L) , p_84)) == 0xEEBCFCB3L) != 0x99808B2AL)) | 0xC0E6L), p_86)))) , 1L) & p_86) , p_86) == g_540), 0x3BEE14C134F6D5F6LL)) >= l_541)))
    { /* block id: 246 */
        struct S0 *l_543 = &g_21;
        struct S0 **l_542[3];
        int i;
        for (i = 0; i < 3; i++)
            l_542[i] = &l_543;
        g_544 = (void*)0;
    }
    else
    { /* block id: 248 */
        uint32_t l_546 = 0x8E7296D8L;
        int32_t l_589 = 0x02168895L;
        struct S2 *l_606 = &g_143[2];
        int32_t l_615 = 7L;
        uint64_t l_616[9][2][6] = {{{1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL},{0xB79115FF9086ACC2LL,0x929233747352B2B6LL,0x26FF2E95A49290F9LL,0x929233747352B2B6LL,0xB79115FF9086ACC2LL,0x929233747352B2B6LL}},{{1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL},{0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL,0x26FF2E95A49290F9LL,0x6D6A9EB457653D48LL,0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL}},{{1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL},{0xB79115FF9086ACC2LL,0x929233747352B2B6LL,0x26FF2E95A49290F9LL,0x929233747352B2B6LL,0xB79115FF9086ACC2LL,0x929233747352B2B6LL}},{{1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL},{0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL,0x26FF2E95A49290F9LL,0x6D6A9EB457653D48LL,0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL}},{{1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL},{0xB79115FF9086ACC2LL,0x929233747352B2B6LL,0x26FF2E95A49290F9LL,0x929233747352B2B6LL,0xB79115FF9086ACC2LL,0x929233747352B2B6LL}},{{1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL},{0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL,0x26FF2E95A49290F9LL,0x6D6A9EB457653D48LL,0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL}},{{1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL},{0xB79115FF9086ACC2LL,0x929233747352B2B6LL,0x26FF2E95A49290F9LL,0x929233747352B2B6LL,0xB79115FF9086ACC2LL,0x929233747352B2B6LL}},{{1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL},{0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL,0x26FF2E95A49290F9LL,0x6D6A9EB457653D48LL,0xB79115FF9086ACC2LL,0x6D6A9EB457653D48LL}},{{1UL,0x929233747352B2B6LL,1UL,0x6D6A9EB457653D48LL,1UL,0x929233747352B2B6LL},{0xB79115FF9086ACC2LL,0x929233747352B2B6LL,0x26FF2E95A49290F9LL,0x929233747352B2B6LL,0xB79115FF9086ACC2LL,0x929233747352B2B6LL}}};
        int16_t l_630 = 0xA21DL;
        int32_t l_632[4][5][4] = {{{(-3L),1L,0x398D1BF7L,1L},{(-3L),0L,8L,1L},{8L,1L,8L,0L},{(-3L),1L,0x398D1BF7L,1L},{(-3L),0L,8L,1L}},{{8L,1L,8L,0L},{(-3L),1L,0x398D1BF7L,1L},{(-3L),0L,8L,1L},{8L,1L,8L,0L},{(-3L),1L,0x398D1BF7L,1L}},{{(-3L),0L,8L,1L},{8L,1L,8L,0L},{(-3L),1L,0x398D1BF7L,1L},{(-3L),0L,8L,1L},{8L,1L,8L,0L}},{{(-3L),1L,0x398D1BF7L,1L},{(-3L),0L,8L,1L},{8L,1L,8L,0L},{(-3L),1L,0x398D1BF7L,1L},{(-3L),0L,8L,1L}}};
        int32_t l_633[1][9] = {{6L,0xABFDB49CL,6L,6L,0xABFDB49CL,6L,6L,0xABFDB49CL,6L}};
        uint32_t ***l_653 = &g_334[2][1][3];
        uint16_t l_664 = 1UL;
        uint16_t l_706 = 0x31FEL;
        uint16_t l_741[8] = {65526UL,0x035EL,65526UL,65526UL,0x035EL,65526UL,65526UL,0x035EL};
        int32_t l_808 = 0x7D28606DL;
        uint32_t l_862 = 0xA13D84FFL;
        int8_t *l_896 = &g_36;
        int32_t *l_918 = (void*)0;
        int32_t **l_917 = &l_918;
        int i, j, k;
        (*l_449) = &g_175;
        if (l_546)
        { /* block id: 250 */
            uint16_t l_568[7][8][4] = {{{0x4D3CL,0x311EL,0UL,0UL},{0x1579L,0UL,0x1579L,0x94D4L},{0x0428L,0x1579L,1UL,65530UL},{0UL,0xAC78L,0UL,0x1579L},{0UL,0x7DDCL,0UL,0xFC19L},{0UL,0UL,1UL,1UL},{0x0428L,0UL,0x1579L,65530UL},{0x1579L,65530UL,0UL,1UL}},{{0x4D3CL,0x3271L,1UL,0UL},{1UL,0UL,7UL,0x7183L},{0x3271L,65535UL,65535UL,0x3271L},{0UL,0xFC19L,6UL,0x311EL},{0UL,0xAC78L,0UL,0x6DCFL},{0UL,0x3271L,4UL,0x6DCFL},{0x94D4L,0xAC78L,0xD997L,0x311EL},{65530UL,0xFC19L,1UL,0x3271L}},{{7UL,65535UL,0xDA29L,0x7183L},{0x1579L,0UL,0xAC78L,0UL},{0xDA29L,1UL,0x4D3CL,0UL},{0xFC19L,1UL,0x1579L,1UL},{0x11C6L,0xDA29L,0x0428L,0UL},{0x6DCFL,0x94D4L,0UL,0x4D3CL},{0x4D3CL,5UL,0UL,0x30CDL},{0x4D3CL,1UL,0UL,3UL}},{{0x6DCFL,0x30CDL,0x0428L,0xDE0AL},{0x11C6L,0x311EL,0x1579L,0UL},{0xFC19L,1UL,0x4D3CL,0UL},{0xDA29L,65530UL,0xAC78L,0x7DDCL},{0x1579L,0xD997L,0xDA29L,0xDA29L},{7UL,7UL,1UL,5UL},{65530UL,3UL,0xD997L,0xFC19L},{0x94D4L,0UL,4UL,0xD997L}},{{0UL,0UL,0UL,0xFC19L},{0UL,3UL,6UL,5UL},{0UL,7UL,65535UL,0xDA29L},{0x3271L,0xD997L,7UL,0x7DDCL},{1UL,65530UL,0UL,0UL},{0x0428L,1UL,0UL,0UL},{0x30CDL,0x311EL,0x30CDL,0xDE0AL},{65530UL,0x30CDL,0x7183L,3UL}},{{0UL,1UL,0xFC19L,0x30CDL},{0xAC78L,5UL,0xFC19L,0x4D3CL},{0UL,0x94D4L,0x7183L,0UL},{65530UL,0xDA29L,0x30CDL,1UL},{0x30CDL,1UL,0UL,0UL},{0x0428L,1UL,0UL,0UL},{1UL,0UL,7UL,0x7183L},{0x3271L,65535UL,65535UL,0x3271L}},{{0UL,0xFC19L,6UL,0x311EL},{0UL,0xAC78L,0UL,0x6DCFL},{0UL,0x3271L,4UL,0x6DCFL},{0x94D4L,0xAC78L,0xD997L,0x311EL},{65530UL,0xFC19L,1UL,0x3271L},{7UL,65535UL,0xDA29L,0x7183L},{0x1579L,0UL,0xAC78L,0UL},{0xDA29L,1UL,0x4D3CL,0UL}}};
            int32_t l_590 = 0x13C84A35L;
            int32_t l_591 = 0x6F7D0EC3L;
            int32_t l_592[6];
            uint32_t ***l_650[5][7][7] = {{{&g_334[2][1][3],&l_346,&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3],(void*)0},{(void*)0,&g_334[0][0][4],&l_346,&g_334[1][0][1],&l_346,&g_334[2][1][3],&l_346},{&g_334[2][1][3],&l_346,&l_346,&g_334[2][1][3],&g_334[2][1][5],(void*)0,&l_346},{&g_334[2][1][3],&l_346,(void*)0,&g_334[2][1][3],&l_346,&g_334[2][1][3],&l_346},{&l_346,&l_346,(void*)0,(void*)0,(void*)0,&l_346,&l_346},{&g_334[2][1][3],&l_346,&g_334[0][0][4],&l_346,&g_334[2][1][3],&l_346,&l_346},{&l_346,&g_334[2][1][3],(void*)0,(void*)0,&g_334[0][1][1],&g_334[0][1][1],(void*)0}},{{&g_334[0][0][4],&g_334[2][1][3],&g_334[0][0][4],&g_334[1][0][0],&g_334[2][1][3],&g_334[2][1][3],&l_346},{&l_346,&g_334[0][1][3],(void*)0,&g_334[2][1][3],&l_346,&g_334[2][1][3],&l_346},{&g_334[2][1][3],&g_334[2][1][3],(void*)0,(void*)0,&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3]},{&g_334[2][1][3],(void*)0,&l_346,&l_346,(void*)0,&g_334[0][1][1],&g_334[2][1][5]},{&g_334[1][0][1],&g_334[2][1][3],&l_346,&l_346,&l_346,&l_346,&l_346},{(void*)0,(void*)0,&g_334[2][1][3],&g_334[2][1][3],&l_346,&l_346,&l_346},{&g_334[2][1][3],&g_334[2][1][3],&g_334[1][0][0],&g_334[2][1][3],&l_346,&g_334[2][1][3],&g_334[2][1][3]}},{{&l_346,&g_334[0][1][3],&l_346,&g_334[0][1][3],&l_346,(void*)0,&g_334[2][1][3]},{&l_346,&g_334[2][1][3],&g_334[1][0][1],&l_346,&l_346,&g_334[2][1][3],&g_334[2][1][3]},{(void*)0,&g_334[2][1][3],&g_334[2][1][3],(void*)0,(void*)0,&g_334[2][1][3],&g_334[2][1][3]},{&l_346,&l_346,&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3],&g_334[1][0][0],&g_334[2][1][3]},{&l_346,&l_346,&l_346,&l_346,&l_346,(void*)0,&l_346},{&g_334[2][1][3],&l_346,&l_346,&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3],&g_334[1][0][1]},{(void*)0,&l_346,&l_346,(void*)0,&g_334[0][1][1],&g_334[2][1][5],&g_334[0][1][3]}},{{&g_334[1][0][1],&g_334[0][0][4],&g_334[2][1][3],&l_346,&g_334[2][1][3],&g_334[0][0][4],&g_334[1][0][1]},{&g_334[2][1][3],(void*)0,&l_346,&g_334[0][1][3],(void*)0,&g_334[2][1][3],&l_346},{&g_334[2][1][3],&l_346,&g_334[2][1][3],&g_334[2][1][3],&l_346,&l_346,&g_334[2][1][3]},{&l_346,&g_334[2][1][3],&l_346,&g_334[2][1][3],&g_334[2][1][5],&l_346,&g_334[2][1][3]},{&g_334[0][0][4],&l_346,&g_334[2][1][3],&l_346,&l_346,&g_334[2][1][3],&g_334[2][1][3]},{&l_346,&g_334[2][1][3],&l_346,&l_346,&g_334[2][1][3],&l_346,&g_334[2][1][3]},{&g_334[2][1][3],&g_334[2][1][3],&l_346,&g_334[0][0][4],(void*)0,&g_334[1][0][0],&g_334[2][1][3]}},{{(void*)0,&g_334[0][1][3],(void*)0,&l_346,(void*)0,&l_346,(void*)0},{&g_334[1][0][1],&g_334[1][0][1],&l_346,&g_334[2][1][3],&g_334[0][0][4],&g_334[2][1][3],&g_334[2][1][3]},{&g_334[0][1][1],&l_346,&g_334[2][1][3],&l_346,(void*)0,&g_334[2][1][3],&g_334[2][1][3]},{&g_334[0][0][4],&g_334[2][1][3],&l_346,&g_334[2][1][3],&g_334[0][0][4],&g_334[1][0][1],&l_346},{&g_334[2][1][3],&g_334[2][1][3],(void*)0,&g_334[2][1][3],(void*)0,&l_346,&g_334[0][1][3]},{&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3],(void*)0,(void*)0,&g_334[2][1][3],&g_334[2][1][3]},{&g_334[2][1][3],&g_334[2][1][3],&g_334[2][1][3],&g_334[0][1][1],&l_346,&g_334[2][1][3],&l_346}}};
            uint32_t ****l_649[10] = {(void*)0,&l_650[1][1][3],(void*)0,&l_650[1][1][3],(void*)0,&l_650[1][1][3],(void*)0,&l_650[1][1][3],(void*)0,&l_650[1][1][3]};
            struct S1 l_699[8] = {{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118},{-28637,125,26050,0xD509EC46L,118}};
            uint8_t *l_757 = (void*)0;
            int i, j, k;
            for (i = 0; i < 6; i++)
                l_592[i] = 0x09D6454BL;
            (*p_87) = (*g_315);
            for (g_545.f2 = 12; (g_545.f2 != 48); ++g_545.f2)
            { /* block id: 254 */
                uint32_t l_561 = 1UL;
                uint32_t l_578[6][3];
                int32_t l_593 = 1L;
                struct S3 ***l_603 = (void*)0;
                struct S3 **l_605 = &g_313;
                struct S3 ***l_604 = &l_605;
                uint32_t *l_611 = &l_546;
                int8_t *l_612 = &g_293;
                int i, j;
                for (i = 0; i < 6; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_578[i][j] = 0xE0083D33L;
                }
                for (g_410 = 3; (g_410 <= 9); g_410 += 1)
                { /* block id: 257 */
                    uint16_t l_567 = 0xE7E7L;
                    struct S2 l_570 = {66,56};
                    const int16_t l_585 = 0x8ECEL;
                    (*l_449) = ((safe_lshift_func_int8_t_s_s(p_84, (((((**l_449) > (0x311576D3139D1EA6LL < (safe_div_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(0L, (safe_mul_func_int8_t_s_s(((**l_449) && ((((safe_rshift_func_int8_t_s_u((((l_561 = (&g_409 != &g_409)) & ((safe_add_func_uint8_t_u_u(((((0x6E6B2F18L != (+((*l_326)++))) == (((((-8L) < l_567) != (-8L)) , &g_440) == (void*)0)) || p_86) && (**l_449)), 0xF4L)) ^ l_546)) > l_568[4][3][0]), 6)) < p_86) != p_86) , (*g_401))), 0x7DL)))), (*g_401))))) <= g_148) , l_569) , 0L))) , &g_38);
                    for (g_438 = 2; (g_438 >= 0); g_438 -= 1)
                    { /* block id: 263 */
                        int32_t *l_586 = &g_100;
                        int32_t *l_587 = &g_38;
                        int32_t *l_588[10][9][2] = {{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}},{{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175},{(void*)0,&g_175}}};
                        int i, j, k;
                        if ((*g_440))
                            break;
                        (**l_449) = 0L;
                        (*g_440) ^= (l_570 , (safe_mul_func_uint16_t_u_u(p_84, (l_546 , (safe_mod_func_uint64_t_u_u((safe_unary_minus_func_uint32_t_u((safe_add_func_int16_t_s_s(((l_578[2][2] || 0x55A1L) , (((((1UL & 0x79L) == (safe_sub_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(((((safe_div_func_int16_t_s_s(((p_84 , l_317[g_438][(g_438 + 2)]) , l_546), 0x9C39L)) || 0x865E2A93B19B3352LL) > l_568[4][3][0]) > 4294967290UL), p_86)), (*g_401)))) < l_585) > (-4L)) ^ 0x15E6L)), p_86)))), (*g_409)))))));
                        l_594--;
                    }
                    (**l_449) |= p_84;
                    for (g_45 = 0; (g_45 <= 0); g_45 += 1)
                    { /* block id: 272 */
                        struct S1 *l_598 = (void*)0;
                        struct S1 **l_597 = &l_598;
                        (*l_597) = &g_130;
                    }
                }
                (*g_440) = 0x07FCE07CL;
                l_616[1][0][2] |= ((safe_mod_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((((((*l_604) = &g_313) == &p_85) & (l_606 != l_606)) == (safe_rshift_func_int16_t_s_u((safe_lshift_func_uint8_t_u_s((((0xB2L <= ((p_86 != (((*l_611) |= 0xB9B7E344L) <= ((((((l_612 != ((safe_div_func_int64_t_s_s(((**p_87) , l_615), p_84)) , l_537[3])) , (void*)0) == (void*)0) || (*g_440)) > p_86) , (**l_449)))) && 8L)) != (**l_449)) <= l_591), l_578[0][1])), 5))), (**l_449))), g_52.f3)) && 65534UL);
            }
            if ((**l_449))
            { /* block id: 281 */
                int32_t l_631[7] = {0xBF40001EL,(-4L),0xBF40001EL,0xBF40001EL,(-4L),0xBF40001EL,0xBF40001EL};
                struct S3 *l_642 = &l_317[0][1];
                struct S0 *l_675[8][1];
                const uint64_t *l_746 = &l_616[4][1][3];
                const uint64_t **l_745 = &l_746;
                int i, j;
                for (i = 0; i < 8; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_675[i][j] = &g_21;
                }
                for (g_473 = 0; (g_473 <= 3); g_473 += 1)
                { /* block id: 284 */
                    uint32_t l_619 = 3UL;
                    struct S1 l_622 = {4480,18,37097,0x781504FAL,239};
                    uint32_t ***l_623 = &g_334[0][1][3];
                    int32_t *l_626 = &g_175;
                    int32_t *l_627 = &g_38;
                    int32_t *l_628 = (void*)0;
                    int32_t *l_629[9][1][2] = {{{&l_592[2],&l_592[2]}},{{&l_615,&l_592[2]}},{{&l_592[2],&l_615}},{{&l_592[2],&l_592[2]}},{{&l_615,&l_592[2]}},{{&l_592[2],&l_615}},{{&l_592[2],&l_592[2]}},{{&l_615,&l_592[2]}},{{&l_592[2],&l_615}}};
                    struct S3 *l_645 = &g_314;
                    int i, j, k;
                    for (g_294 = 0; (g_294 <= 9); g_294 += 1)
                    { /* block id: 287 */
                        int32_t *l_617 = &l_615;
                        int32_t *l_618[3][1];
                        int i, j;
                        for (i = 0; i < 3; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_618[i][j] = &l_591;
                        }
                        l_592[g_473] &= 0x769F3E77L;
                        --l_619;
                    }
                    (*g_440) ^= (((l_622 , ((*l_623) = (void*)0)) != (void*)0) == ((*g_401)++));
                    g_634--;
                    for (g_438 = 9; (g_438 >= 0); g_438 -= 1)
                    { /* block id: 297 */
                        struct S0 l_641 = {0x98D283F3L,-1,0xC0527E5FL,-8L};
                        struct S3 **l_643 = &l_642;
                        struct S3 **l_644[10][4] = {{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313},{&g_313,&g_313,&g_313,&g_313}};
                        int32_t *l_651[3];
                        int i, j;
                        for (i = 0; i < 3; i++)
                            l_651[i] = &g_150;
                        g_314.f3 &= (safe_add_func_uint8_t_u_u(((((g_652 ^= (safe_div_func_uint64_t_u_u((l_641 , (p_84 | (l_592[4] > (((l_645 = ((*l_643) = l_642)) == (*p_87)) >= ((g_545.f3 = ((~(safe_mod_func_int8_t_s_s(((**g_400) == (l_649[9] != (void*)0)), (((void*)0 == (*g_463)) ^ p_84)))) ^ l_632[2][1][0])) , (**l_449)))))), (*g_409)))) , l_653) == (void*)0) > l_630), p_86));
                    }
                }
lbl_742:
                (*l_449) = &g_38;
                for (l_615 = (-12); (l_615 > (-22)); l_615--)
                { /* block id: 308 */
                    int32_t *l_656 = &l_590;
                    int32_t *l_657 = &l_591;
                    int32_t *l_658 = &l_592[4];
                    int32_t *l_659 = &l_590;
                    int32_t *l_660 = &l_631[5];
                    int32_t *l_661 = &l_633[0][4];
                    int32_t *l_662[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int32_t l_663 = 0xB89483BFL;
                    struct S1 *l_667[1][9] = {{&l_472[0],&l_472[0],&l_472[0],&l_472[0],&l_472[0],&l_472[0],&l_472[0],&l_472[0],&l_472[0]}};
                    struct S1 **l_668 = (void*)0;
                    struct S1 **l_669 = (void*)0;
                    struct S1 **l_670 = &l_667[0][1];
                    struct S2 **l_692 = (void*)0;
                    int i, j;
                    l_664--;
                    (*l_660) = (((*l_670) = l_667[0][4]) != &g_130);
                    if ((((safe_sub_func_uint8_t_u_u(l_631[6], (safe_add_func_int64_t_s_s(((void*)0 != l_675[4][0]), (-1L))))) ^ (safe_add_func_int64_t_s_s((safe_add_func_int8_t_s_s((((void*)0 == &g_334[2][0][0]) ^ p_86), 1L)), (safe_div_func_uint64_t_u_u((((*g_400) != (void*)0) > l_592[4]), (**l_449)))))) , (*g_440)))
                    { /* block id: 312 */
                        uint8_t l_683 = 0xAEL;
                        uint16_t *l_701 = (void*)0;
                        uint16_t *l_702[3];
                        uint32_t *l_711 = (void*)0;
                        int16_t *l_718 = &g_303;
                        uint64_t *l_736 = &g_123[0][6][0];
                        uint64_t **l_735 = &l_736;
                        int i;
                        for (i = 0; i < 3; i++)
                            l_702[i] = &l_568[4][3][0];
                        ++l_683;
                        (*l_659) |= ((safe_mul_func_uint8_t_u_u((((safe_add_func_uint64_t_u_u((g_294 ^ (l_692 == (void*)0)), (safe_lshift_func_int8_t_s_s((safe_mod_func_uint32_t_u_u(0xD55BC2D2L, (safe_add_func_uint64_t_u_u(((0x6194L != (l_589 = (l_699[6].f0 = (l_699[6] , ((*l_538) = (~0xDBL)))))) == (p_86 | (safe_rshift_func_uint8_t_u_s((safe_unary_minus_func_uint32_t_u(0x9D30E2EBL)), 6)))), 1L)))), 3)))) , l_706) , 1UL), 0L)) >= 0L);
                        (*l_657) |= (safe_sub_func_uint64_t_u_u(((safe_mod_func_int64_t_s_s(((l_631[0] >= (((*l_346) = l_711) == &p_84)) >= ((((*l_326) = (safe_mod_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(7UL, (((safe_mul_func_int16_t_s_s(l_590, ((*l_718) = l_683))) , 248UL) ^ (safe_mul_func_uint16_t_u_u(g_52.f3, ((safe_mul_func_uint8_t_u_u(p_84, 0x9AL)) < (**l_449))))))), p_86))) & p_84) != 0L)), (*g_409))) ^ 7UL), 0UL));
                        (*l_659) ^= (safe_add_func_uint32_t_u_u(p_84, (l_699[6].f3 > ((**l_449) = (safe_mul_func_uint16_t_u_u(l_592[4], ((g_652 &= ((safe_mul_func_int8_t_s_s(((*l_660) = (safe_add_func_int32_t_s_s(((safe_rshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_s(((((3UL & (&l_616[4][1][0] == ((*l_735) = &g_448))) < (safe_sub_func_int32_t_s_s(p_84, p_84))) | (safe_rshift_func_uint8_t_u_s(0xAEL, 6))) != 0x2804L), 0)), l_741[7])) <= p_86), (-4L)))), 0x2CL)) >= (*g_409))) == 8UL)))))));
                    }
                    else
                    { /* block id: 327 */
                        if (g_21.f3)
                            goto lbl_742;
                    }
                    for (l_663 = 0; (l_663 > (-28)); l_663--)
                    { /* block id: 332 */
                        const uint64_t ***l_747 = &l_745;
                        (*l_747) = l_745;
                        if ((*g_440))
                            break;
                    }
                }
            }
            else
            { /* block id: 337 */
                int32_t *l_748 = &g_100;
                uint8_t *l_768 = &g_381;
                int8_t l_776 = (-7L);
                (*l_748) = ((**l_449) = p_84);
                for (g_100 = 0; (g_100 < (-16)); g_100 = safe_sub_func_uint64_t_u_u(g_100, 8))
                { /* block id: 342 */
                    uint8_t l_758 = 0x43L;
                    (*p_85) = ((*g_313) = (*p_85));
                    g_99 = &g_100;
                    for (g_540 = 0; (g_540 < 55); g_540 = safe_add_func_int16_t_s_s(g_540, 4))
                    { /* block id: 348 */
                        uint16_t *l_759[7] = {(void*)0,&l_741[7],(void*)0,(void*)0,&l_741[7],(void*)0,(void*)0};
                        int32_t l_775 = 0L;
                        int i;
                        (*g_440) = ((((*l_538) = (safe_lshift_func_int16_t_s_s((0UL == 0x4E0E57E5L), 7))) | (l_633[0][4] = (((*g_400) != l_757) <= ((*p_85) , (p_84 <= l_758))))) , 0L);
                        (*g_440) = l_758;
                        if (p_86)
                            break;
                        (*p_85) = ((safe_rshift_func_int16_t_s_u(((((*l_748) ^ (((safe_lshift_func_uint16_t_u_u((0xF6L || ((*l_768) &= ((safe_sub_func_int16_t_s_s((safe_add_func_uint64_t_u_u((g_130.f2 <= ((((void*)0 == l_768) , (((safe_sub_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u(((-9L) <= (safe_rshift_func_uint8_t_u_s(p_84, l_775))), l_775)) ^ (*g_401)), g_130.f2)) <= p_84) > l_568[1][2][3])) >= 0x9B0CL)), (*g_409))), 6L)) > l_546))), 5)) & 4L) | 0xBD93L)) && (**l_449)) < g_52.f4), l_776)) , (*p_88));
                    }
                    (*g_440) = 0x0FD6AEADL;
                }
            }
        }
        else
        { /* block id: 360 */
            int32_t *l_779 = &l_633[0][4];
            struct S2 **l_910 = &l_389[2][0];
            (*l_449) = &l_589;
            if (g_777)
            { /* block id: 362 */
                int32_t *l_778 = (void*)0;
                l_778 = l_778;
                (*l_449) = l_779;
            }
            else
            { /* block id: 365 */
                int64_t l_809 = 0x4083A771A0439DFFLL;
                uint32_t l_810 = 1UL;
                int32_t l_833 = 0x5462F367L;
                uint64_t *l_885 = (void*)0;
                uint64_t **l_884 = &l_885;
                uint64_t ***l_883 = &l_884;
                struct S0 l_890 = {0x963DC3EFL,1,0x2A2D4A09L,0x4CF53A02L};
                if ((((l_810 &= (g_293 = (0xA8F52462DB80A985LL == (l_809 = (((*l_538) = ((safe_rshift_func_int8_t_s_s((safe_add_func_int64_t_s_s((4L && g_294), (safe_div_func_int16_t_s_s((safe_sub_func_int8_t_s_s((safe_div_func_int64_t_s_s((safe_mod_func_uint64_t_u_u(6UL, 3L)), (safe_add_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u((l_798 | ((safe_sub_func_int32_t_s_s(0x590383A2L, (0xCEL == ((safe_mod_func_uint8_t_u_u((+(safe_add_func_uint32_t_u_u(((safe_mod_func_uint16_t_u_u(p_84, g_221)) , p_84), p_86))), l_632[2][2][2])) | 255UL)))) || l_808)), g_148)) && g_540), p_84)))), p_84)), (-1L))))), (*l_779))) || p_84)) <= g_143[0].f0))))) , (void*)0) == &l_326))
                { /* block id: 370 */
                    (*l_449) = (*l_449);
                    (*l_449) = &g_38;
                }
                else
                { /* block id: 373 */
                    int16_t *l_831 = &l_630;
                    int16_t l_832 = 0x77FDL;
                    int32_t l_865 = 1L;
                    struct S2 l_911 = {67,63};
                    l_833 ^= (safe_sub_func_uint8_t_u_u((safe_mod_func_uint32_t_u_u(0xCD5DDE6CL, (((((safe_lshift_func_int8_t_s_s((((((((((*l_831) |= (((safe_rshift_func_int8_t_s_s((((p_84 <= p_84) ^ (safe_rshift_func_uint16_t_u_s((safe_sub_func_uint32_t_u_u((g_16 > (((safe_mul_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_s(((*l_538)--), (safe_rshift_func_uint16_t_u_u((p_86 , 1UL), 1)))) < 0x6EL), (0x0EL && g_294))) == 1L) || (*l_779))), 1UL)), (*l_779)))) >= 0UL), p_84)) >= 0xABD42669L) && (*g_335))) ^ l_809) & p_86) , &g_293) == (void*)0) , &l_317[1][1]) == (void*)0) , g_545.f3), l_809)) <= 0x9BCA201DL) || l_832) >= p_86) && g_540))), l_741[4]));
                    for (g_652 = 7; (g_652 >= 0); g_652 -= 1)
                    { /* block id: 379 */
                        uint64_t *l_874 = &g_123[0][5][0];
                        struct S0 l_877[4][8] = {{{0xC35F48A2L,0,0xC867BAA2L,9L},{0x860FB8F0L,1,0x1F782C73L,-1L},{0xD918B22FL,0,0xF2A38997L,-3L},{0x860FB8F0L,1,0x1F782C73L,-1L},{0xC35F48A2L,0,0xC867BAA2L,9L},{2UL,-1,0x3F0494ACL,0x7A4CA2C3L},{0xC35F48A2L,0,0xC867BAA2L,9L},{0x860FB8F0L,1,0x1F782C73L,-1L}},{{1UL,-0,0x025579BEL,0x16CAF76DL},{0x860FB8F0L,1,0x1F782C73L,-1L},{1UL,-0,0x025579BEL,0x16CAF76DL},{7UL,-0,0x1332725AL,0x2ED38C47L},{0xC35F48A2L,0,0xC867BAA2L,9L},{7UL,-0,0x1332725AL,0x2ED38C47L},{1UL,-0,0x025579BEL,0x16CAF76DL},{0x860FB8F0L,1,0x1F782C73L,-1L}},{{0xC35F48A2L,0,0xC867BAA2L,9L},{7UL,-0,0x1332725AL,0x2ED38C47L},{1UL,-0,0x025579BEL,0x16CAF76DL},{0x860FB8F0L,1,0x1F782C73L,-1L},{1UL,-0,0x025579BEL,0x16CAF76DL},{7UL,-0,0x1332725AL,0x2ED38C47L},{0xC35F48A2L,0,0xC867BAA2L,9L},{7UL,-0,0x1332725AL,0x2ED38C47L}},{{0xC35F48A2L,0,0xC867BAA2L,9L},{0x860FB8F0L,1,0x1F782C73L,-1L},{0xD918B22FL,0,0xF2A38997L,-3L},{0x860FB8F0L,1,0x1F782C73L,-1L},{0xC35F48A2L,0,0xC867BAA2L,9L},{2UL,-1,0x3F0494ACL,0x7A4CA2C3L},{0xC35F48A2L,0,0xC867BAA2L,9L},{0x860FB8F0L,1,0x1F782C73L,-1L}}};
                        int32_t *l_891 = &g_38;
                        int i, j;
                        (*g_440) ^= (safe_rshift_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s(((!(1UL > l_741[g_652])) && (((((safe_lshift_func_uint8_t_u_s((safe_div_func_int16_t_s_s((safe_sub_func_int8_t_s_s((safe_div_func_uint32_t_u_u(((((*g_401) = (((void*)0 == &g_401) , ((safe_sub_func_int64_t_s_s((((*g_409) ^ (safe_div_func_int64_t_s_s((p_84 , (+((((p_84 , (p_84 , (void*)0)) == &g_401) != l_832) <= (*l_779)))), g_852))) == l_833), l_832)) == g_21.f0))) && 1L) | g_175), l_853)), p_84)), l_741[g_652])), g_294)) ^ l_741[g_652]) | 65533UL) | 4294967291UL) , l_809)), l_741[g_652])) <= 18446744073709551615UL), 2));
                        if ((**l_449))
                            break;
                        l_472[0].f0 &= ((((*g_440) ^= 0x50DD5225L) | ((p_86 && ((safe_mod_func_uint32_t_u_u(1UL, p_84)) , ((*l_779) = ((safe_rshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((safe_mod_func_int8_t_s_s(l_862, 0x45L)) , (**g_400)), 1)), 7)) < ((safe_lshift_func_int8_t_s_s((l_865 &= g_303), 2)) | (safe_div_func_int64_t_s_s((!l_832), p_86))))))) > 0xDC0FL)) , l_832);
                        l_865 &= ((*g_440) = ((*l_891) &= (safe_add_func_uint16_t_u_u((safe_sub_func_int16_t_s_s((&g_123[0][0][0] != &g_448), (+((--(*l_874)) == (l_877[3][7] , ((safe_sub_func_int8_t_s_s((!l_589), (safe_lshift_func_int16_t_s_u(((void*)0 != l_883), 4)))) > (safe_sub_func_uint16_t_u_u(((safe_mod_func_uint16_t_u_u(g_21.f2, (l_741[g_652] = ((0x5932DFBEEC5342B6LL > ((l_890 , (*g_409)) , 0L)) && 0x2D40AA2A8B595AB7LL)))) | l_832), (*l_779))))))))), p_86))));
                    }
                    for (g_36 = 5; (g_36 > 2); g_36 = safe_sub_func_int16_t_s_s(g_36, 7))
                    { /* block id: 395 */
                        int8_t **l_897 = &l_896;
                        int32_t l_900 = (-5L);
                        (*l_449) = ((safe_mul_func_uint8_t_u_u((((*g_544) , ((*l_831) = (((((*l_897) = l_896) != (void*)0) , (safe_rshift_func_int16_t_s_u(l_900, 7))) , (0L != 0x9F4C0A4AL)))) && (safe_lshift_func_uint8_t_u_s(((**g_400) = ((((*l_779) ^ ((((safe_mod_func_uint32_t_u_u((!p_86), 0x3926A2B8L)) ^ 0UL) < 0x3D210872E87CC903LL) >= 0x33C6620C91AC15B3LL)) == 0UL) >= g_130.f3)), 4))), 0xD7L)) , &l_833);
                    }
                    for (g_652 = 0; (g_652 <= 0); g_652 += 1)
                    { /* block id: 403 */
                        int32_t l_912 = 0x1FE4FB6DL;
                        int32_t l_913 = 1L;
                        (*g_440) = (((0x1539L < ((*l_831) &= ((safe_mod_func_uint64_t_u_u(p_86, (((safe_add_func_int32_t_s_s((*g_440), ((g_130 , (void*)0) == (void*)0))) && ((void*)0 == l_910)) | (l_912 = ((l_911 , (*g_409)) == p_84))))) , g_294))) ^ (**l_449)) & l_913);
                    }
                }
            }
            g_440 = &l_633[0][4];
        }
        (*g_440) |= (safe_lshift_func_uint16_t_u_u(9UL, ((*l_538) = (0x4AC148B04E3C34B3LL <= ((0x85B6214CL || p_86) && (g_916 == ((*l_917) = &g_258)))))));
    }
    (*l_920) = (g_314.f4 ^ 0x2D20L);
    (*g_313) = (*p_88);
    return p_84;
}


/* ------------------------------------------ */
/* 
 * reads : g_99 g_111 g_100 g_119 g_21.f3 g_123 g_21.f1 g_21.f2 g_52 g_130 g_143 g_36 g_175 g_45 g_16 g_221 g_265 g_279 g_294 g_303 g_21.f0
 * writes: g_111 g_119 g_123 g_52 g_99 g_100 g_148 g_150 g_175 g_45 g_265 g_279 g_294 g_36
 */
static struct S2  func_91(uint64_t  p_92)
{ /* block id: 13 */
    uint16_t l_103 = 0xF478L;
    int16_t *l_110 = &g_111;
    uint32_t *l_118 = &g_119;
    uint8_t l_121 = 0x22L;
    uint64_t *l_122 = &g_123[0][6][0];
    int64_t l_124 = 0xCD949914032C2DE0LL;
    int64_t *l_125[2][3][7];
    int32_t l_126 = (-2L);
    struct S1 *l_129 = &g_130;
    int8_t l_139[7] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    struct S2 l_170[4][6] = {{{177,31},{91,99},{91,99},{177,31},{168,64},{149,30}},{{149,30},{168,64},{149,30},{177,31},{177,31},{149,30}},{{91,99},{91,99},{177,31},{123,80},{177,31},{91,99}},{{177,31},{168,64},{123,80},{123,80},{168,64},{177,31}}};
    struct S0 l_184[6][3] = {{{8UL,1,4294967295UL,-1L},{0xFD144FB7L,0,0UL,0xD52605C4L},{0x685E78B0L,1,0x5DDE06F2L,0x8DD3C908L}},{{8UL,1,4294967295UL,-1L},{8UL,1,4294967295UL,-1L},{0xFD144FB7L,0,0UL,0xD52605C4L}},{{1UL,-1,0x0E80B645L,0x29C22F73L},{0xFD144FB7L,0,0UL,0xD52605C4L},{0xFD144FB7L,0,0UL,0xD52605C4L}},{{0xFD144FB7L,0,0UL,0xD52605C4L},{0x2602EF28L,-0,0xCB55262EL,1L},{0x685E78B0L,1,0x5DDE06F2L,0x8DD3C908L}},{{1UL,-1,0x0E80B645L,0x29C22F73L},{0x2602EF28L,-0,0xCB55262EL,1L},{1UL,-1,0x0E80B645L,0x29C22F73L}},{{8UL,1,4294967295UL,-1L},{0xFD144FB7L,0,0UL,0xD52605C4L},{0x685E78B0L,1,0x5DDE06F2L,0x8DD3C908L}}};
    int8_t l_200[5] = {1L,1L,1L,1L,1L};
    struct S3 l_226[5] = {{4232,-212,-166,-3184,5552},{4232,-212,-166,-3184,5552},{4232,-212,-166,-3184,5552},{4232,-212,-166,-3184,5552},{4232,-212,-166,-3184,5552}};
    int i, j, k;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 7; k++)
                l_125[i][j][k] = &l_124;
        }
    }
    if (func_93(&g_52, g_99, (l_126 = ((safe_add_func_int8_t_s_s(l_103, (((safe_rshift_func_int8_t_s_s((safe_mod_func_uint16_t_u_u((safe_div_func_uint32_t_u_u(((l_103 == (((((*l_110) |= p_92) && g_100) >= (safe_sub_func_uint64_t_u_u((safe_lshift_func_int16_t_s_s((((*l_118) |= (safe_lshift_func_uint16_t_u_s(p_92, 2))) , (!l_103)), 1)), ((*l_122) = (((((4294967295UL ^ 5UL) == 0x50L) || p_92) <= l_121) , 0xAA494CAA1C64AC22LL))))) < g_21.f3)) , 1UL), p_92)), (-4L))), l_124)) || p_92) , g_123[0][6][0]))) & p_92)), g_21.f1, p_92))
    { /* block id: 22 */
        struct S1 **l_131 = &l_129;
        int32_t l_132[10][1][5] = {{{0x56F341A2L,4L,4L,0x56F341A2L,0x56F341A2L}},{{1L,0x5B119D2BL,1L,0x5B119D2BL,1L}},{{0x56F341A2L,0x56F341A2L,4L,4L,0x56F341A2L}},{{0x50AD34F8L,0x5B119D2BL,0x50AD34F8L,0x5B119D2BL,0x50AD34F8L}},{{0x56F341A2L,4L,4L,0x56F341A2L,0x56F341A2L}},{{1L,0x5B119D2BL,1L,0x5B119D2BL,1L}},{{0x56F341A2L,0x56F341A2L,4L,4L,0x56F341A2L}},{{0x50AD34F8L,0x5B119D2BL,0x50AD34F8L,0x5B119D2BL,0x50AD34F8L}},{{0x56F341A2L,4L,4L,0x56F341A2L,0x56F341A2L}},{{1L,0x5B119D2BL,1L,0x5B119D2BL,1L}}};
        struct S2 l_140 = {114,120};
        struct S0 l_156 = {18446744073709551615UL,1,1UL,0xB4E2EDBCL};
        int32_t *l_166 = &l_132[7][0][3];
        uint32_t **l_167 = (void*)0;
        int i, j, k;
        (*l_131) = l_129;
        if (g_52.f4)
            goto lbl_133;
lbl_133:
        l_132[8][0][4] = 0x868CE927L;
        if (p_92)
        { /* block id: 26 */
            int32_t **l_134[2][9] = {{&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99},{&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99,&g_99}};
            int i, j;
            g_99 = &g_100;
        }
        else
        { /* block id: 28 */
            const struct S3 l_169 = {118,-635,-369,10036,19135};
            int32_t *l_176 = &l_132[3][0][1];
            for (g_111 = 0; (g_111 == (-16)); g_111 = safe_sub_func_uint32_t_u_u(g_111, 6))
            { /* block id: 31 */
                int32_t **l_137 = &g_99;
                const int32_t l_165[8][10][2] = {{{0x6B179DCFL,1L},{0xEF93B096L,(-4L)},{0xDAD02808L,0x0746DC9FL},{0x1DA5A7FFL,0L},{0x8866DC53L,0L},{0x04C0B57BL,(-3L)},{0x28449A31L,0x4B7B0D79L},{0x9A3B3DE1L,0xDAD02808L},{0L,(-1L)},{0xFC347C86L,0x04C0B57BL}},{{6L,(-6L)},{0L,7L},{0xD7CC4F2FL,2L},{(-6L),0L},{0x17DE5E03L,(-1L)},{9L,(-1L)},{0x17DE5E03L,0L},{(-6L),2L},{0xD7CC4F2FL,7L},{0L,(-6L)}},{{6L,0x04C0B57BL},{0xFC347C86L,(-1L)},{0L,0xDAD02808L},{0x9A3B3DE1L,0x4B7B0D79L},{0x28449A31L,(-3L)},{0x04C0B57BL,0L},{0x8866DC53L,0L},{0x1DA5A7FFL,0x0746DC9FL},{0xDAD02808L,(-4L)},{0xEF93B096L,1L}},{{0x6B179DCFL,0xD7CC4F2FL},{0x576304A0L,0x576304A0L},{(-3L),0x0F193884L},{0x5EB5DA64L,6L},{(-1L),0xA434C43CL},{0x4B7B0D79L,(-1L)},{0x0746DC9FL,0L},{0xF22FF882L,0x4B7B0D79L},{0xD7CC4F2FL,0L},{0x4B7B0D79L,0x9A3B3DE1L}},{{1L,(-5L)},{(-1L),(-4L)},{(-4L),0x3B740F3BL},{0L,0x4F3BA64CL},{0xA434C43CL,2L},{0xFC347C86L,0xF22FF882L},{0x0746DC9FL,(-1L)},{(-3L),(-6L)},{0x6B179DCFL,(-1L)},{0x080E1D11L,0xD7CC4F2FL}},{{(-1L),0xFC347C86L},{(-1L),1L},{0x0F193884L,0x6B179DCFL},{0x9A3B3DE1L,9L},{(-3L),0x8866DC53L},{0x3B740F3BL,0L},{9L,6L},{0xDAD02808L,0x5EB5DA64L},{7L,0x5EB5DA64L},{0xDAD02808L,6L}},{{9L,0L},{0x3B740F3BL,0x8866DC53L},{(-3L),9L},{0x9A3B3DE1L,0x6B179DCFL},{0x0F193884L,1L},{(-1L),0xFC347C86L},{(-1L),0xD7CC4F2FL},{0x080E1D11L,(-1L)},{0x6B179DCFL,(-6L)},{(-3L),(-1L)}},{{0x0746DC9FL,0xF22FF882L},{0xFC347C86L,2L},{0xA434C43CL,0x4F3BA64CL},{0L,0x3B740F3BL},{(-4L),(-4L)},{(-1L),(-5L)},{1L,0x9A3B3DE1L},{0x4B7B0D79L,0L},{0xD7CC4F2FL,0x4B7B0D79L},{0xF22FF882L,(-3L)}}};
                int i, j, k;
                (*l_137) = &g_100;
                if ((!(0x3DL != l_139[6])))
                { /* block id: 33 */
                    return l_140;
                }
                else
                { /* block id: 35 */
                    int32_t l_149 = 0x3E2DD196L;
                    uint16_t l_153 = 0xD697L;
                    uint32_t ***l_168 = &l_167;
                    if ((p_92 & (((*l_137) = (g_130 , (*l_137))) != (void*)0)))
                    { /* block id: 37 */
                        int32_t *l_151 = (void*)0;
                        int32_t *l_152[5][7] = {{&l_132[8][0][4],&l_132[2][0][1],&l_132[7][0][4],&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4]},{(void*)0,&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4],&l_132[4][0][3],&l_132[8][0][4]},{&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4],(void*)0,&l_132[8][0][4],&l_132[8][0][4]},{&l_132[8][0][4],&l_132[8][0][4],(void*)0,&l_132[4][0][3],&l_132[8][0][4],(void*)0,&l_132[7][0][0]},{&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4],&l_132[2][0][1],&l_132[8][0][4],&l_132[8][0][4],&l_132[8][0][4]}};
                        int i, j;
                        l_153 |= (g_52.f1 = (p_92 | (g_150 = (safe_sub_func_int16_t_s_s(0x88D0L, (l_149 = (((g_143[0] , 0x36A15226L) , p_92) <= (safe_add_func_int32_t_s_s((((*g_99) = (*g_99)) && (safe_sub_func_int64_t_s_s((g_148 = 1L), p_92))), ((((g_130.f4 , p_92) & 0xF2876425F5104884LL) < (-5L)) | l_132[8][0][4]))))))))));
                        (**l_137) = (*g_99);
                        if ((*g_99))
                            break;
                    }
                    else
                    { /* block id: 46 */
                        uint16_t *l_157 = &l_103;
                        int32_t l_164 = (-1L);
                        (**l_137) = ((((g_36 , ((l_156 , (0x68E7L | ((*l_157) = 0xC8FCL))) || ((safe_mul_func_int8_t_s_s((l_139[2] | ((safe_add_func_int8_t_s_s(0xF9L, (safe_lshift_func_uint8_t_u_u(0x5CL, p_92)))) , 3L)), g_52.f1)) != l_164))) && l_165[1][1][1]) < 0x46L) ^ (-4L));
                        l_166 = ((*l_137) = &g_100);
                    }
                    (*l_168) = l_167;
                }
                (*l_137) = l_118;
                (*l_137) = &g_100;
            }
            for (g_100 = 0; (g_100 <= 0); g_100 += 1)
            { /* block id: 59 */
                int32_t **l_171 = &l_166;
                g_99 = (l_169 , (l_170[1][1] , ((*l_171) = &l_132[8][0][4])));
                for (g_119 = 0; (g_119 <= 0); g_119 += 1)
                { /* block id: 64 */
                    int32_t *l_174 = &g_175;
                    (*l_174) &= ((safe_add_func_int16_t_s_s(((1L > p_92) >= (*l_166)), (**l_171))) == (0x86458D21332A2132LL | p_92));
                    if (l_169.f4)
                        continue;
                    for (l_103 = 0; (l_103 <= 0); l_103 += 1)
                    { /* block id: 69 */
                        int i, j, k;
                        (*g_99) ^= g_123[l_103][(l_103 + 3)][g_100];
                        return l_140;
                    }
                }
            }
            l_176 = l_118;
            (*l_176) ^= (*g_99);
        }
    }
    else
    { /* block id: 78 */
        struct S2 * const l_177 = &l_170[1][1];
        struct S2 *l_179 = &g_143[0];
        struct S2 **l_178 = &l_179;
        uint32_t l_195 = 0xE6F33552L;
        struct S1 l_218 = {15331,144,-37392,0x75043125L,319};
        int32_t l_250 = 0x604FD5BAL;
        int32_t l_263 = (-2L);
        (*l_178) = l_177;
        for (g_175 = 0; (g_175 > 14); g_175 = safe_add_func_uint32_t_u_u(g_175, 9))
        { /* block id: 82 */
            uint32_t l_223 = 0xB4F5B66DL;
            int32_t l_246 = 0x55B6F9C3L;
            int32_t l_247 = 0x19848999L;
            int32_t l_253 = (-5L);
            int32_t l_254 = 4L;
            int32_t l_257 = 1L;
            int32_t l_261 = 0x43BC25FBL;
            struct S2 l_312 = {38,50};
            for (g_45 = 0; (g_45 == 46); ++g_45)
            { /* block id: 85 */
                const uint8_t l_208[6] = {0x9FL,0x9FL,0x9FL,0x9FL,0x9FL,0x9FL};
                uint16_t *l_222 = &l_103;
                int32_t l_249 = 0x41819897L;
                int32_t l_251 = 6L;
                int32_t l_252 = 0xBBE56635L;
                int32_t l_256 = (-1L);
                int32_t l_264 = 9L;
                int i;
                if ((0x55L & (g_175 ^ (l_184[1][1] , (l_184[1][1].f2 <= ((safe_mod_func_uint64_t_u_u(p_92, (safe_lshift_func_uint16_t_u_u(0x06E0L, 10)))) || g_119))))))
                { /* block id: 86 */
                    uint16_t l_199[6] = {0x761CL,0x761CL,0x761CL,0x761CL,0x761CL,0x761CL};
                    int32_t *l_203 = &g_100;
                    int32_t **l_204 = &l_203;
                    int32_t **l_205 = &g_99;
                    struct S2 **l_209 = &l_179;
                    uint16_t *l_210 = &l_199[1];
                    int32_t *l_211 = &g_100;
                    int i;
                    (*l_211) = (safe_lshift_func_int16_t_s_s((!(((safe_lshift_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u(0x7BF7F2EFA27BBEFFLL)), p_92)) && (l_195 >= (safe_add_func_int64_t_s_s(p_92, ((~l_199[1]) || (((l_200[3] , ((*l_210) = (((((safe_mod_func_uint8_t_u_u((((((&g_100 != ((*l_205) = ((*l_204) = l_203))) , (safe_add_func_int32_t_s_s(((0x5CCE37DA2B038524LL >= 0x62863D54A8836995LL) & g_130.f1), (*g_99)))) , 0xE2F412CFL) || l_208[0]) , 0x6BL), g_100)) ^ p_92) , (void*)0) != l_209) && p_92))) & l_195) , (*l_203))))))) , g_21.f2)), l_195));
                    (*g_99) ^= ((safe_rshift_func_uint8_t_u_s(l_184[1][1].f2, 2)) ^ p_92);
                    if ((g_143[2] , (((1L ^ (-1L)) , ((l_208[0] , (g_130.f2 >= (safe_mod_func_int32_t_s_s((g_130 , (((safe_div_func_int16_t_s_s((l_218 , (safe_mod_func_uint64_t_u_u(g_16, 0xCDB2E776E5D696A6LL))), g_221)) , l_222) == (void*)0)), 0x47AD6A65L)))) , l_208[0])) , (*g_99))))
                    { /* block id: 92 */
                        if (l_223)
                            break;
                        if ((*g_99))
                            break;
                    }
                    else
                    { /* block id: 95 */
                        (*l_203) |= (((void*)0 != &g_123[0][4][0]) < g_52.f3);
                        return g_143[0];
                    }
                }
                else
                { /* block id: 99 */
                    int8_t *l_228 = &l_139[6];
                    int32_t l_248 = 0x1036F43DL;
                    int32_t l_255[1][1][1];
                    int32_t *l_282 = &l_261;
                    int32_t *l_283 = &l_249;
                    int32_t *l_284 = &l_251;
                    int32_t *l_285 = (void*)0;
                    int32_t *l_286 = &l_261;
                    int32_t *l_287 = &l_249;
                    int32_t *l_288 = &l_253;
                    int32_t *l_289 = &l_252;
                    int32_t *l_290 = &l_255[0][0][0];
                    int32_t *l_291 = (void*)0;
                    int32_t *l_292[4][10][5] = {{{&l_261,&l_255[0][0][0],&l_255[0][0][0],&l_261,&l_247},{&l_264,&l_251,&l_256,&l_249,(void*)0},{(void*)0,&l_255[0][0][0],&l_257,&l_256,&l_247},{(void*)0,(void*)0,&l_248,&l_249,&l_255[0][0][0]},{&l_255[0][0][0],&l_249,&l_252,&l_261,&g_175},{&g_175,(void*)0,&l_249,(void*)0,&l_248},{&l_251,&g_175,&l_251,&l_247,&l_255[0][0][0]},{&g_100,(void*)0,&l_252,&l_257,&l_256},{&l_249,(void*)0,(void*)0,(void*)0,&l_254},{(void*)0,&l_263,&l_246,&g_175,&l_256}},{{&l_256,&l_256,&l_246,(void*)0,&l_255[0][0][0]},{&l_249,&l_257,(void*)0,(void*)0,(void*)0},{&l_248,(void*)0,(void*)0,&l_256,&l_257},{(void*)0,&l_247,&g_175,&l_256,&l_255[0][0][0]},{&l_256,&l_249,(void*)0,&l_248,(void*)0},{&l_248,&l_248,(void*)0,&g_175,&l_251},{(void*)0,(void*)0,&g_175,&g_100,&l_249},{&l_256,(void*)0,&l_246,&l_251,&l_257},{&l_257,(void*)0,&l_254,&l_246,&l_256},{&l_255[0][0][0],&l_248,&l_248,&l_254,(void*)0}},{{(void*)0,&l_249,&l_255[0][0][0],&l_251,&l_261},{&l_257,&l_247,&l_263,(void*)0,(void*)0},{&l_251,(void*)0,&l_247,&l_252,&l_257},{&g_100,&l_246,&l_247,(void*)0,&l_263},{&l_253,&g_100,(void*)0,&l_255[0][0][0],&l_254},{&l_253,&l_255[0][0][0],(void*)0,&l_255[0][0][0],&l_263},{&g_100,&l_253,&l_254,&l_255[0][0][0],&l_246},{&l_251,&l_256,&l_263,&l_251,&l_255[0][0][0]},{&l_257,&l_249,(void*)0,&l_248,&l_251},{(void*)0,&l_249,(void*)0,&l_249,(void*)0}},{{&l_255[0][0][0],&g_175,&l_255[0][0][0],&l_257,&l_248},{&l_257,&g_175,&l_248,&l_255[0][0][0],&l_264},{&l_256,&l_257,&l_249,&g_175,&l_248},{(void*)0,&l_255[0][0][0],(void*)0,&l_257,(void*)0},{&l_248,&l_248,&l_254,(void*)0,&l_251},{&l_256,&l_255[0][0][0],(void*)0,&l_251,&l_255[0][0][0]},{(void*)0,&l_254,(void*)0,&l_253,&l_246},{&l_248,&l_255[0][0][0],&l_257,(void*)0,&l_263},{&g_175,&l_264,&g_100,&l_255[0][0][0],&l_254},{&l_261,(void*)0,&l_256,&l_255[0][0][0],&l_263}}};
                    struct S3 ***l_304 = (void*)0;
                    int8_t *l_311[7][7] = {{&g_293,&g_36,&g_293,(void*)0,&l_200[1],&g_293,&g_293},{&g_36,&l_200[1],(void*)0,&l_200[1],&g_36,&l_200[3],&l_200[1]},{&l_200[0],&l_200[0],&l_200[3],&l_200[1],&g_293,&l_200[3],&g_221},{&g_293,&g_221,(void*)0,(void*)0,&g_221,&g_293,&l_200[0]},{&l_200[0],&l_200[1],(void*)0,&l_200[0],&g_221,&l_200[3],&l_200[1]},{&g_36,&g_293,&g_293,&g_259,&g_293,&g_293,&g_36},{&g_293,&l_200[1],(void*)0,&g_293,&g_36,&g_293,&g_293}};
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 1; j++)
                        {
                            for (k = 0; k < 1; k++)
                                l_255[i][j][k] = 0L;
                        }
                    }
                    if ((safe_sub_func_uint8_t_u_u(l_200[1], ((*l_228) = ((l_226[2] , g_119) > (safe_unary_minus_func_int32_t_s(0xD425E7FCL)))))))
                    { /* block id: 101 */
                        int32_t *l_229 = &g_100;
                        int32_t *l_230 = (void*)0;
                        int32_t *l_231 = &g_100;
                        int32_t *l_232 = &g_100;
                        int32_t *l_233 = &g_100;
                        int32_t *l_234 = &g_100;
                        int32_t *l_235 = &g_100;
                        int32_t *l_236 = (void*)0;
                        int32_t *l_237 = &g_100;
                        int32_t *l_238 = &g_100;
                        int32_t *l_239 = &g_100;
                        int32_t *l_240 = (void*)0;
                        int32_t *l_241 = &g_100;
                        int32_t *l_242 = &g_100;
                        int32_t *l_243 = &g_100;
                        int32_t *l_244 = (void*)0;
                        int32_t *l_245[6][8] = {{&g_175,&g_100,&g_100,&g_175,&g_175,&g_100,&g_100,&g_175},{&g_175,&g_100,&g_100,&g_175,&g_175,&g_100,&g_100,&g_175},{&g_175,&g_100,&g_100,&g_175,&g_175,&g_100,&g_100,&g_175},{&g_175,&g_100,&g_100,&g_175,&g_175,&g_100,&g_100,&g_175},{&g_175,&g_100,&g_100,&g_175,&g_175,&g_100,&g_100,&g_175},{&g_175,&g_100,&g_100,&g_175,&g_175,&g_100,&g_100,&g_175}};
                        int i, j;
                        --g_265[0][4];
                    }
                    else
                    { /* block id: 103 */
                        int32_t *l_268 = (void*)0;
                        int32_t *l_269 = &l_253;
                        int32_t *l_270 = &l_247;
                        int32_t *l_271 = &l_253;
                        int32_t *l_272 = &l_252;
                        int32_t *l_273 = &l_247;
                        int32_t *l_274 = &g_100;
                        int32_t *l_275 = &l_246;
                        int32_t *l_276 = &l_246;
                        int32_t *l_277 = &l_250;
                        int32_t *l_278[9][5] = {{&l_253,&l_253,&l_261,&l_253,&l_253},{&l_255[0][0][0],&l_255[0][0][0],&l_255[0][0][0],&l_252,(void*)0},{&l_253,&l_247,&l_247,&l_253,&l_247},{(void*)0,&l_255[0][0][0],(void*)0,&l_255[0][0][0],(void*)0},{&l_247,&l_253,&l_247,&l_247,&l_253},{(void*)0,&l_252,&l_255[0][0][0],&l_255[0][0][0],&l_255[0][0][0]},{&l_253,&l_253,&l_261,&l_253,&l_253},{&l_255[0][0][0],&l_255[0][0][0],&l_255[0][0][0],&l_252,(void*)0},{&l_253,&l_247,&l_247,&l_253,&l_247}};
                        int i, j;
                        (*l_270) ^= ((*l_269) |= (*g_99));
                        ++g_279;
                    }
                    --g_294;
                    for (g_36 = 0; (g_36 < 8); g_36 = safe_add_func_uint16_t_u_u(g_36, 2))
                    { /* block id: 111 */
                        (*l_286) ^= ((*g_99) = (-6L));
                    }
                    (*l_289) = (((safe_rshift_func_int8_t_s_u(((safe_sub_func_int8_t_s_s(g_303, (l_247 = ((((void*)0 == l_304) < ((*l_228) ^= ((p_92 >= (((safe_sub_func_uint32_t_u_u(p_92, g_21.f0)) && (safe_mul_func_int8_t_s_s(g_52.f4, (safe_rshift_func_int8_t_s_u(l_218.f0, (((-1L) <= 0xE6L) == p_92)))))) , g_123[0][6][0])) <= l_246))) ^ l_253)))) && 0UL), g_52.f4)) | p_92) <= p_92);
                }
                return l_312;
            }
        }
    }
    return g_143[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_21.f2 g_52 g_100 l_2113
 * writes: g_52
 */
static int32_t  func_93(struct S3 * p_94, int32_t * p_95, int64_t  p_96, int64_t  p_97, int64_t  p_98)
{ /* block id: 18 */
    int32_t *l_127 = &g_100;
    int32_t **l_128 = &l_127;
    (*l_128) = l_127;
    g_52 = (g_21.f2 , (*p_94));
    return (*p_95);
}




/* ---------------------------------------- */
//testcase_id 1484153234
int case1484153234(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_21.f0, "g_21.f0", print_hash_value);
    transparent_crc(g_21.f1, "g_21.f1", print_hash_value);
    transparent_crc(g_21.f2, "g_21.f2", print_hash_value);
    transparent_crc(g_21.f3, "g_21.f3", print_hash_value);
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_38, "g_38", print_hash_value);
    transparent_crc(g_45, "g_45", print_hash_value);
    transparent_crc(g_52.f0, "g_52.f0", print_hash_value);
    transparent_crc(g_52.f1, "g_52.f1", print_hash_value);
    transparent_crc(g_52.f2, "g_52.f2", print_hash_value);
    transparent_crc(g_52.f3, "g_52.f3", print_hash_value);
    transparent_crc(g_52.f4, "g_52.f4", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_111, "g_111", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_123[i][j][k], "g_123[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_130.f0, "g_130.f0", print_hash_value);
    transparent_crc(g_130.f1, "g_130.f1", print_hash_value);
    transparent_crc(g_130.f2, "g_130.f2", print_hash_value);
    transparent_crc(g_130.f3, "g_130.f3", print_hash_value);
    transparent_crc(g_130.f4, "g_130.f4", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_143[i].f0, "g_143[i].f0", print_hash_value);
        transparent_crc(g_143[i].f1, "g_143[i].f1", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_148, "g_148", print_hash_value);
    transparent_crc(g_150, "g_150", print_hash_value);
    transparent_crc(g_175, "g_175", print_hash_value);
    transparent_crc(g_221, "g_221", print_hash_value);
    transparent_crc(g_258, "g_258", print_hash_value);
    transparent_crc(g_259, "g_259", print_hash_value);
    transparent_crc(g_260, "g_260", print_hash_value);
    transparent_crc(g_262, "g_262", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_265[i][j], "g_265[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_279, "g_279", print_hash_value);
    transparent_crc(g_293, "g_293", print_hash_value);
    transparent_crc(g_294, "g_294", print_hash_value);
    transparent_crc(g_303, "g_303", print_hash_value);
    transparent_crc(g_314.f0, "g_314.f0", print_hash_value);
    transparent_crc(g_314.f1, "g_314.f1", print_hash_value);
    transparent_crc(g_314.f2, "g_314.f2", print_hash_value);
    transparent_crc(g_314.f3, "g_314.f3", print_hash_value);
    transparent_crc(g_314.f4, "g_314.f4", print_hash_value);
    transparent_crc(g_381, "g_381", print_hash_value);
    transparent_crc(g_410, "g_410", print_hash_value);
    transparent_crc(g_433, "g_433", print_hash_value);
    transparent_crc(g_438, "g_438", print_hash_value);
    transparent_crc(g_448, "g_448", print_hash_value);
    transparent_crc(g_473, "g_473", print_hash_value);
    transparent_crc(g_539, "g_539", print_hash_value);
    transparent_crc(g_540, "g_540", print_hash_value);
    transparent_crc(g_545.f0, "g_545.f0", print_hash_value);
    transparent_crc(g_545.f1, "g_545.f1", print_hash_value);
    transparent_crc(g_545.f2, "g_545.f2", print_hash_value);
    transparent_crc(g_545.f3, "g_545.f3", print_hash_value);
    transparent_crc(g_634, "g_634", print_hash_value);
    transparent_crc(g_652, "g_652", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_682[i], "g_682[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_777, "g_777", print_hash_value);
    transparent_crc(g_852, "g_852", print_hash_value);
    transparent_crc(g_936, "g_936", print_hash_value);
    transparent_crc(g_937, "g_937", print_hash_value);
    transparent_crc(g_959, "g_959", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_960[i], "g_960[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1078, "g_1078", print_hash_value);
    transparent_crc(g_1092, "g_1092", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1208[i], "g_1208[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1335.f0, "g_1335.f0", print_hash_value);
    transparent_crc(g_1335.f1, "g_1335.f1", print_hash_value);
    transparent_crc(g_1335.f2, "g_1335.f2", print_hash_value);
    transparent_crc(g_1335.f3, "g_1335.f3", print_hash_value);
    transparent_crc(g_1424, "g_1424", print_hash_value);
    transparent_crc(g_1487, "g_1487", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_1510[i][j].f0, "g_1510[i][j].f0", print_hash_value);
            transparent_crc(g_1510[i][j].f1, "g_1510[i][j].f1", print_hash_value);
            transparent_crc(g_1510[i][j].f2, "g_1510[i][j].f2", print_hash_value);
            transparent_crc(g_1510[i][j].f3, "g_1510[i][j].f3", print_hash_value);
            transparent_crc(g_1510[i][j].f4, "g_1510[i][j].f4", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1581, "g_1581", print_hash_value);
    transparent_crc(g_1866.f0, "g_1866.f0", print_hash_value);
    transparent_crc(g_1866.f1, "g_1866.f1", print_hash_value);
    transparent_crc(g_1866.f2, "g_1866.f2", print_hash_value);
    transparent_crc(g_1866.f3, "g_1866.f3", print_hash_value);
    transparent_crc(g_1866.f4, "g_1866.f4", print_hash_value);
    transparent_crc(g_1878, "g_1878", print_hash_value);
    transparent_crc(g_1904, "g_1904", print_hash_value);
    transparent_crc(g_1907, "g_1907", print_hash_value);
    transparent_crc(g_1914, "g_1914", print_hash_value);
    transparent_crc(g_1915, "g_1915", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_1952[i], "g_1952[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1954, "g_1954", print_hash_value);
    transparent_crc(g_1974, "g_1974", print_hash_value);
    transparent_crc(g_1975, "g_1975", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_2000[i].f0, "g_2000[i].f0", print_hash_value);
        transparent_crc(g_2000[i].f1, "g_2000[i].f1", print_hash_value);
        transparent_crc(g_2000[i].f2, "g_2000[i].f2", print_hash_value);
        transparent_crc(g_2000[i].f3, "g_2000[i].f3", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2116, "g_2116", print_hash_value);
    transparent_crc(g_2173, "g_2173", print_hash_value);
    transparent_crc(g_2184, "g_2184", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2186[i], "g_2186[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2309, "g_2309", print_hash_value);
    transparent_crc(g_2338.f0, "g_2338.f0", print_hash_value);
    transparent_crc(g_2338.f1, "g_2338.f1", print_hash_value);
    transparent_crc(g_2338.f2, "g_2338.f2", print_hash_value);
    transparent_crc(g_2338.f3, "g_2338.f3", print_hash_value);
    transparent_crc(g_2338.f4, "g_2338.f4", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_2345[i].f0, "g_2345[i].f0", print_hash_value);
        transparent_crc(g_2345[i].f1, "g_2345[i].f1", print_hash_value);
        transparent_crc(g_2345[i].f2, "g_2345[i].f2", print_hash_value);
        transparent_crc(g_2345[i].f3, "g_2345[i].f3", print_hash_value);
        transparent_crc(g_2345[i].f4, "g_2345[i].f4", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2401.f0, "g_2401.f0", print_hash_value);
    transparent_crc(g_2401.f1, "g_2401.f1", print_hash_value);
    transparent_crc(g_2401.f2, "g_2401.f2", print_hash_value);
    transparent_crc(g_2401.f3, "g_2401.f3", print_hash_value);
    transparent_crc(g_2403.f0, "g_2403.f0", print_hash_value);
    transparent_crc(g_2403.f1, "g_2403.f1", print_hash_value);
    transparent_crc(g_2403.f2, "g_2403.f2", print_hash_value);
    transparent_crc(g_2403.f3, "g_2403.f3", print_hash_value);
    transparent_crc(g_2405.f0, "g_2405.f0", print_hash_value);
    transparent_crc(g_2405.f1, "g_2405.f1", print_hash_value);
    transparent_crc(g_2405.f2, "g_2405.f2", print_hash_value);
    transparent_crc(g_2405.f3, "g_2405.f3", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_2437[i][j], "g_2437[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_2449[i], "g_2449[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2457[i][j], "g_2457[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2589, "g_2589", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 602
   depth: 1, occurrence: 69
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 13
XXX zero bitfields defined in structs: 1
XXX const bitfields defined in structs: 3
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 147
breakdown:
   indirect level: 0, occurrence: 69
   indirect level: 1, occurrence: 41
   indirect level: 2, occurrence: 18
   indirect level: 3, occurrence: 10
   indirect level: 4, occurrence: 6
   indirect level: 5, occurrence: 3
XXX full-bitfields structs in the program: 31
breakdown:
   indirect level: 0, occurrence: 31
XXX times a bitfields struct's address is taken: 96
XXX times a bitfields struct on LHS: 9
XXX times a bitfields struct on RHS: 111
XXX times a single bitfield on LHS: 19
XXX times a single bitfield on RHS: 205

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 411
   depth: 2, occurrence: 94
   depth: 3, occurrence: 14
   depth: 4, occurrence: 3
   depth: 5, occurrence: 5
   depth: 6, occurrence: 4
   depth: 8, occurrence: 2
   depth: 11, occurrence: 1
   depth: 12, occurrence: 2
   depth: 13, occurrence: 1
   depth: 14, occurrence: 4
   depth: 15, occurrence: 4
   depth: 16, occurrence: 2
   depth: 17, occurrence: 2
   depth: 18, occurrence: 4
   depth: 19, occurrence: 3
   depth: 20, occurrence: 5
   depth: 21, occurrence: 7
   depth: 22, occurrence: 6
   depth: 24, occurrence: 1
   depth: 25, occurrence: 3
   depth: 26, occurrence: 4
   depth: 27, occurrence: 8
   depth: 28, occurrence: 1
   depth: 29, occurrence: 4
   depth: 30, occurrence: 5
   depth: 31, occurrence: 3
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 34, occurrence: 2
   depth: 38, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 529

XXX times a variable address is taken: 1225
XXX times a pointer is dereferenced on RHS: 355
breakdown:
   depth: 1, occurrence: 247
   depth: 2, occurrence: 100
   depth: 3, occurrence: 7
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 365
breakdown:
   depth: 1, occurrence: 319
   depth: 2, occurrence: 39
   depth: 3, occurrence: 6
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 53
XXX times a pointer is compared with address of another variable: 15
XXX times a pointer is compared with another pointer: 11
XXX times a pointer is qualified to be dereferenced: 8277

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1286
   level: 2, occurrence: 332
   level: 3, occurrence: 35
   level: 4, occurrence: 18
XXX number of pointers point to pointers: 204
XXX number of pointers point to scalars: 262
XXX number of pointers point to structs: 63
XXX percent of pointers has null in alias set: 26.3
XXX average alias set size: 1.42

XXX times a non-volatile is read: 2203
XXX times a non-volatile is write: 1105
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 2
XXX backward jumps: 6

XXX stmts: 390
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 34
   depth: 1, occurrence: 39
   depth: 2, occurrence: 45
   depth: 3, occurrence: 61
   depth: 4, occurrence: 85
   depth: 5, occurrence: 126

XXX percentage a fresh-made variable is used: 18.9
XXX percentage an existing variable is used: 81.1
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/

