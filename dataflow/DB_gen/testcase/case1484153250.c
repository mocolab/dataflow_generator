/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      1950793084
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_4[6][9] = {{4L,0x24F84B81L,0L,0L,0x24F84B81L,4L,(-1L),0L,0L},{0xC25BFC88L,0xEA2A3B3FL,4L,0L,(-1L),0xC25BFC88L,0xC25BFC88L,(-1L),0L},{0L,0x24F84B81L,0L,4L,(-1L),0L,(-1L),0x080C170CL,4L},{0L,(-1L),4L,0L,0x24F84B81L,0L,4L,(-1L),0L},{0xC25BFC88L,(-1L),0L,4L,0xEA2A3B3FL,0xC25BFC88L,4L,0L,4L},{4L,0xC25BFC88L,(-5L),(-5L),0xC25BFC88L,0x71C1EC2DL,0x4768C0AAL,(-1L),(-5L)}};
static int32_t g_26[5] = {0x1A504A86L,0x1A504A86L,0x1A504A86L,0x1A504A86L,0x1A504A86L};
static int32_t g_34[5][9][5] = {{{0x708C9934L,5L,1L,0L,0x31FD024CL},{0L,1L,0xE77332E7L,0L,0xB650F839L},{1L,0x9A92ED16L,4L,1L,0x708C9934L},{1L,1L,(-4L),0xB650F839L,1L},{1L,9L,(-9L),9L,1L},{1L,0L,(-5L),0x31FD024CL,0x9A92ED16L},{0L,0x53CC61BBL,1L,0x3E8932A2L,0x53CC61BBL},{0x708C9934L,9L,0x4D52A103L,0L,0x9A92ED16L},{(-4L),0x3E8932A2L,0x95BD6605L,1L,1L}},{{0x9A92ED16L,4L,1L,0x708C9934L,1L},{9L,1L,(-10L),0x708C9934L,0x708C9934L},{1L,0L,1L,1L,0xB650F839L},{0L,1L,(-4L),0L,0x31FD024CL},{0x3E8932A2L,0x9A92ED16L,1L,0x3E8932A2L,0x708C9934L},{0x95BD6605L,0x3E8932A2L,(-4L),0x31FD024CL,1L},{1L,(-4L),1L,9L,0x95BD6605L},{0x3E8932A2L,0L,(-10L),0xB650F839L,0x9A92ED16L},{(-5L),0L,1L,1L,0L}},{{0x708C9934L,(-4L),0x95BD6605L,0L,4L},{9L,0x3E8932A2L,0x4D52A103L,0L,1L},{4L,0x9A92ED16L,1L,1L,1L},{9L,1L,(-5L),0x708C9934L,1L},{0x708C9934L,0L,(-9L),0L,0xB650F839L},{(-5L),1L,(-4L),0x53CC61BBL,0xB650F839L},{0x3E8932A2L,4L,4L,0x3E8932A2L,1L},{1L,0x3E8932A2L,0xE77332E7L,0xB650F839L,1L},{0x95BD6605L,9L,1L,(-4L),1L}},{{0x3E8932A2L,0x53CC61BBL,(-5L),0xB650F839L,4L},{0L,0L,(-4L),0x3E8932A2L,0L},{1L,9L,0x95BD6605L,0x53CC61BBL,0x9A92ED16L},{9L,1L,0x95BD6605L,0L,0x95BD6605L},{0x9A92ED16L,0x9A92ED16L,(-4L),0x708C9934L,1L},{(-4L),1L,(-5L),1L,0x708C9934L},{0x708C9934L,5L,1L,0L,0x31FD024CL},{0L,1L,0xE77332E7L,0L,0xB650F839L},{1L,0x9A92ED16L,4L,1L,0x53CC61BBL}},{{(-5L),1L,0x95EEC0DBL,1L,0x4D52A103L},{(-5L),5L,0L,5L,(-5L)},{1L,1L,(-9L),0x42895483L,1L},{1L,(-4L),1L,4L,(-4L)},{0x53CC61BBL,5L,0L,1L,1L},{0x95EEC0DBL,4L,(-10L),0xE77332E7L,(-5L)},{1L,7L,1L,0x53CC61BBL,0x4D52A103L},{5L,0x95BD6605L,0x708C9934L,0x53CC61BBL,0x53CC61BBL},{(-10L),0x31FD024CL,(-10L),0xE77332E7L,1L}}};
static uint8_t g_63 = 0UL;
static int8_t g_73 = (-1L);
static int32_t g_98 = (-1L);
static int8_t g_100 = (-1L);
static uint16_t g_101 = 4UL;
static int32_t **g_107 = (void*)0;
static int32_t g_114 = 0x3D763EE6L;
static uint8_t g_119 = 1UL;
static int32_t g_133 = (-3L);
static int16_t g_173 = 1L;
static uint8_t * const g_177 = (void*)0;
static uint32_t g_179 = 0x7A930F33L;
static uint8_t g_181 = 0x99L;
static int64_t g_183 = 0x89D617F3B7B1D02BLL;
static uint32_t g_194 = 1UL;
static uint16_t g_267 = 65535UL;
static uint64_t g_275[10] = {0x7354803B48D6DF37LL,18446744073709551615UL,18446744073709551615UL,0x7354803B48D6DF37LL,18446744073709551615UL,18446744073709551615UL,0x7354803B48D6DF37LL,18446744073709551615UL,18446744073709551615UL,0x7354803B48D6DF37LL};
static uint16_t *g_303 = (void*)0;
static uint32_t g_326 = 0UL;
static uint32_t g_336 = 18446744073709551606UL;
static uint8_t *g_348 = &g_119;
static const int32_t g_410 = 0x96BD690EL;
static int64_t g_430[7][10] = {{0x80F6240D1436571BLL,0x4764F954569E3898LL,(-3L),(-10L),(-7L),1L,7L,0L,0xFCE58D607E727781LL,0xFCE58D607E727781LL},{0x6220A6EDD08027E6LL,(-10L),0x80F6240D1436571BLL,0L,0L,0x80F6240D1436571BLL,(-10L),0x6220A6EDD08027E6LL,(-1L),0L},{0L,1L,8L,0x80F6240D1436571BLL,0x6220A6EDD08027E6LL,0L,0x266CC68DD8CB413FLL,5L,(-3L),0L},{0x26B8B0E4CA7BC208LL,0L,8L,0x6220A6EDD08027E6LL,0x4764F954569E3898LL,5L,0x4764F954569E3898LL,0x6220A6EDD08027E6LL,8L,0L},{0x6BDDD89E2EAA588ALL,0xFCE58D607E727781LL,0x80F6240D1436571BLL,0L,0x7A29E6B39C141DDELL,0L,1L,0L,0x1A81D8E0298DE64FLL,0x26B8B0E4CA7BC208LL},{8L,7L,(-3L),0x26B8B0E4CA7BC208LL,5L,0L,0x6BDDD89E2EAA588ALL,0x6BDDD89E2EAA588ALL,0L,5L},{0x6BDDD89E2EAA588ALL,(-7L),(-7L),0x6BDDD89E2EAA588ALL,0x80F6240D1436571BLL,5L,0x1A81D8E0298DE64FLL,(-3L),0x266CC68DD8CB413FLL,(-10L)}};
static uint8_t g_432 = 0x9DL;
static uint32_t g_454[2][8][4] = {{{0xD84E16D6L,0xD84E16D6L,0x99CF55E9L,0xD84E16D6L},{0xD84E16D6L,0UL,0UL,0xD84E16D6L},{0UL,0xD84E16D6L,0UL,0UL},{0xD84E16D6L,0xD84E16D6L,0x99CF55E9L,0xD84E16D6L},{0xD84E16D6L,0UL,0UL,0xD84E16D6L},{0UL,0xD84E16D6L,0UL,0UL},{0xD84E16D6L,0xD84E16D6L,0x99CF55E9L,0xD84E16D6L},{0xD84E16D6L,0UL,0UL,0xD84E16D6L}},{{0UL,0xD84E16D6L,0UL,0UL},{0xD84E16D6L,0xD84E16D6L,0x99CF55E9L,0xD84E16D6L},{0xD84E16D6L,0UL,0UL,0xD84E16D6L},{0UL,0xD84E16D6L,0UL,0UL},{0xD84E16D6L,0xD84E16D6L,0x99CF55E9L,0xD84E16D6L},{0xD84E16D6L,0UL,0UL,0xD84E16D6L},{0UL,0xD84E16D6L,0UL,0UL},{0xD84E16D6L,0xD84E16D6L,0x99CF55E9L,0UL}}};
static uint32_t g_486[9] = {0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L,0x7CCCB5D9L};
static int32_t g_549[5][2] = {{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)},{(-8L),(-8L)}};
static int16_t g_550[1] = {(-6L)};
static uint64_t g_551 = 2UL;
static uint8_t **g_613 = &g_348;
static uint8_t ***g_612 = &g_613;
static uint64_t g_617 = 1UL;
static uint16_t g_623 = 9UL;
static int16_t g_636[1] = {(-1L)};
static uint16_t g_637 = 0xDDCBL;
static const int32_t g_642 = 0x6CC57A27L;
static int64_t *g_664[10] = {&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9],&g_430[1][9]};
static int64_t **g_663[8] = {&g_664[8],&g_664[8],&g_664[8],&g_664[8],&g_664[8],&g_664[8],&g_664[8],&g_664[8]};
static int8_t *g_680 = &g_73;
static int8_t ** const g_679 = &g_680;
static uint64_t g_696 = 0x59ED372D56FFD2D0LL;
static uint16_t **g_706 = &g_303;
static uint16_t ***g_705 = &g_706;
static uint32_t g_722 = 4294967292UL;
static uint64_t g_743 = 18446744073709551615UL;
static uint32_t g_807 = 8UL;
static uint32_t *g_844 = &g_454[1][7][0];
static int16_t *g_894 = &g_636[0];
static int16_t *g_908 = &g_550[0];
static int16_t ** const g_907[1] = {&g_908};
static int32_t *g_912 = &g_98;
static int32_t g_921 = 0x4B8271FAL;
static int16_t g_922[7] = {7L,(-4L),7L,7L,(-4L),7L,7L};
static uint64_t g_924 = 0x20A25B0683E577C4LL;
static uint8_t g_1013 = 0x21L;
static uint8_t ****g_1015 = &g_612;
static uint16_t g_1030 = 0x5A0CL;
static uint8_t g_1070 = 2UL;
static uint64_t g_1088[10] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static int64_t ***g_1090 = &g_663[4];
static int64_t ****g_1089 = &g_1090;
static uint32_t g_1103 = 0UL;
static int16_t g_1195 = (-3L);
static int8_t g_1196 = 0x29L;
static int64_t g_1197 = (-1L);
static uint32_t g_1199 = 18446744073709551615UL;
static int64_t g_1273 = 0x2D7ECE2AFF562ECELL;
static uint64_t g_1274 = 0x7AFEB4C24635E5F7LL;
static uint32_t g_1277[2] = {0xCAD6976EL,0xCAD6976EL};
static uint8_t *g_1326 = &g_181;
static uint64_t *g_1423 = (void*)0;
static uint64_t **g_1422 = &g_1423;
static const int32_t *g_1425 = (void*)0;
static int32_t g_1439 = 0x7A234362L;
static int64_t g_1553 = 0x04A305B599210076LL;
static const int8_t g_1621 = (-10L);
static const int8_t *g_1620 = &g_1621;
static const uint8_t **g_1706 = (void*)0;
static const uint8_t ***g_1705 = &g_1706;
static const uint8_t **** const g_1704[9] = {&g_1705,&g_1705,&g_1705,&g_1705,&g_1705,&g_1705,&g_1705,&g_1705,&g_1705};
static const uint8_t **** const *g_1703 = &g_1704[6];
static int16_t g_1743[5] = {0x18EAL,0x18EAL,0x18EAL,0x18EAL,0x18EAL};
static const int32_t * const g_1805 = &g_549[0][1];
static const int32_t * const *g_1804 = &g_1805;
static int32_t *g_1808[9] = {&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0]};
static int32_t **g_1807 = &g_1808[8];
static int32_t *g_1813 = &g_98;


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static const int32_t * func_13(int32_t * p_14, uint64_t  p_15, uint32_t  p_16);
static uint32_t  func_17(uint32_t  p_18);
static int32_t  func_41(int32_t * p_42, int32_t  p_43, uint64_t  p_44);
static int32_t * func_45(int32_t * p_46, int32_t * p_47, uint64_t  p_48, int32_t  p_49, int32_t * p_50);
static int32_t * func_51(uint64_t  p_52, int32_t * p_53, int32_t * p_54);
static int32_t  func_65(uint8_t * p_66, int32_t  p_67, int32_t  p_68);
static uint8_t * func_69(int8_t  p_70, int16_t  p_71);
static int8_t  func_78(uint16_t  p_79, int32_t * p_80, int32_t * p_81, int32_t * p_82, int8_t * const  p_83);
static uint16_t  func_84(const int8_t * p_85, uint32_t  p_86, uint64_t  p_87, const int32_t  p_88);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_1804 g_1807 g_1326 g_181 g_1813 g_4 g_908 g_550 g_844 g_454 g_680 g_73 g_98
 * writes: g_1804 g_1807 g_181 g_912 g_73 g_98
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = (void*)0;
    int32_t *l_3 = &g_4[2][8];
    int32_t *l_5 = &g_4[2][8];
    int32_t *l_6[5];
    int8_t l_7 = 0x3EL;
    uint16_t l_8 = 0UL;
    int64_t l_1331 = (-7L);
    uint8_t l_1445 = 255UL;
    uint8_t l_1462 = 0xFCL;
    const int8_t l_1470 = 0x0EL;
    int16_t l_1519 = 1L;
    int64_t l_1557 = 3L;
    int8_t l_1587[9] = {1L,1L,1L,1L,1L,1L,1L,1L,1L};
    uint32_t l_1625 = 0x4AC586F3L;
    uint64_t * const l_1655[8] = {&g_275[9],&g_275[9],&g_275[9],&g_275[9],&g_275[9],&g_275[9],&g_275[9],&g_275[9]};
    int32_t l_1659 = 3L;
    const uint16_t l_1683 = 0UL;
    uint16_t ***l_1759 = &g_706;
    uint8_t l_1769[7][4][1] = {{{0x76L},{0xECL},{0x23L},{0x23L}},{{0xECL},{0x76L},{0x57L},{0xECL}},{{0x57L},{0x76L},{0xECL},{0x23L}},{{0x23L},{0xECL},{0x76L},{0x57L}},{{0xECL},{0x57L},{0x76L},{0xECL}},{{0x23L},{0x23L},{0xECL},{0x76L}},{{0x57L},{0xECL},{0x57L},{0x76L}}};
    const int32_t * const **l_1806 = &g_1804;
    int32_t ***l_1809 = (void*)0;
    int32_t ***l_1810 = (void*)0;
    int32_t ***l_1811 = &g_1807;
    int32_t **l_1812[3][8][10] = {{{&l_6[0],&l_6[0],&l_6[3],&l_6[0],(void*)0,&g_912,&l_6[3],&g_912,(void*)0,&l_6[0]},{&g_912,&l_6[1],&g_912,&g_912,&l_3,&l_5,&l_6[3],&l_3,&l_5,&l_5},{&l_6[3],&l_6[0],&l_6[0],&l_2,&g_912,&l_2,&l_2,&l_3,&l_2,&l_2},{&l_5,&l_3,&g_912,&l_3,&l_5,(void*)0,&l_6[0],&g_912,&l_2,&l_6[0]},{&l_6[3],(void*)0,&l_6[3],&l_2,(void*)0,&l_3,&l_3,&l_6[0],&l_5,&l_6[0]},{&g_912,&l_2,&l_5,&g_912,&l_5,&l_2,&g_912,&l_5,(void*)0,&l_2},{&g_912,&l_2,&l_6[3],(void*)0,(void*)0,&l_6[0],&l_5,&l_6[1],&l_6[3],&l_2},{&g_912,&l_5,&l_6[4],&l_2,(void*)0,&l_2,&l_6[4],&l_5,&g_912,(void*)0}},{{&l_6[3],&l_6[0],&l_6[4],&l_2,&l_5,&l_5,(void*)0,&l_6[1],&l_6[0],&l_3},{&l_6[0],&l_2,&g_912,&l_2,&l_2,&l_3,&l_2,&l_2,&g_912,&l_2},{(void*)0,&l_6[1],&l_6[3],&l_2,&l_2,&l_3,&g_912,&g_912,&l_6[3],&l_3},{&l_2,&g_912,&l_6[0],(void*)0,&l_5,&l_3,&g_912,&l_3,&l_5,(void*)0},{(void*)0,&l_5,(void*)0,&l_3,(void*)0,&l_3,&g_912,&l_6[0],&l_6[3],&l_2},{&l_6[0],&g_912,&l_2,&l_6[0],(void*)0,&l_5,&g_912,&l_6[0],&g_912,&l_5},{&l_6[3],&l_6[1],(void*)0,&l_6[1],&l_6[3],&l_2,&l_2,&l_3,&g_912,&g_912},{&g_912,&l_2,&l_6[0],&l_6[0],&l_6[3],&l_6[0],(void*)0,&g_912,&l_6[3],&g_912}},{{&l_6[4],&l_6[0],&l_6[3],&l_3,&l_6[3],&l_6[0],&l_6[4],&l_2,&l_5,&l_5},{&l_6[4],&l_5,&g_912,(void*)0,(void*)0,&l_6[0],&l_5,&l_6[1],&l_6[3],&l_2},{&g_912,&l_5,&l_6[4],&l_2,(void*)0,&l_2,&l_6[4],&l_5,&g_912,(void*)0},{&l_6[3],&l_6[0],&l_6[4],&l_2,&l_5,&l_5,(void*)0,&l_6[1],&l_6[0],&l_3},{&l_6[0],&l_2,&g_912,&l_2,&l_2,&l_3,&l_2,&l_2,&g_912,&l_2},{(void*)0,&l_6[1],&l_6[3],&l_2,&l_2,&l_3,&g_912,&g_912,&l_6[3],&l_3},{&l_2,&g_912,&l_6[0],(void*)0,&l_5,&l_3,&g_912,&l_3,&l_5,(void*)0},{(void*)0,&l_5,(void*)0,&l_3,(void*)0,&l_3,&g_912,&l_6[0],&l_6[3],&l_2}}};
    const uint64_t l_1818 = 18446744073709551608UL;
    int i, j, k;
    for (i = 0; i < 5; i++)
        l_6[i] = &g_4[2][8];
    ++l_8;
    for (l_7 = 12; (l_7 >= 3); l_7--)
    { /* block id: 4 */
        int32_t l_23 = 0x23A35E06L;
        int32_t *l_1327[3][8][6] = {{{(void*)0,&g_549[4][1],&g_921,&g_549[4][1],&g_921,&g_549[4][0]},{&l_23,&g_549[2][0],(void*)0,(void*)0,(void*)0,&g_921},{&l_23,&g_921,&g_549[4][0],&g_921,&l_23,&g_549[4][1]},{(void*)0,&g_921,&l_23,&g_921,(void*)0,(void*)0},{&g_921,(void*)0,(void*)0,&g_549[4][1],&g_549[4][0],&l_23},{&l_23,(void*)0,&l_23,(void*)0,&g_549[4][0],&l_23},{&g_549[1][0],&g_549[4][0],(void*)0,&g_921,(void*)0,(void*)0},{&g_549[4][0],&l_23,&l_23,&g_549[4][0],&g_549[1][0],&g_549[4][1]}},{{&g_921,(void*)0,&g_549[4][0],&g_549[1][0],&g_921,&g_549[2][0]},{(void*)0,&l_23,(void*)0,&l_23,&g_921,&g_921},{&g_549[4][1],(void*)0,(void*)0,&g_921,&g_549[1][0],&g_921},{&g_921,&l_23,&g_921,(void*)0,(void*)0,&g_921},{&g_921,&g_549[4][0],&g_549[2][0],&g_921,&g_549[4][0],(void*)0},{&g_549[4][0],(void*)0,&g_549[4][1],&g_921,&g_549[4][0],(void*)0},{&g_921,(void*)0,&g_549[1][0],(void*)0,(void*)0,&g_549[1][0]},{&g_921,&g_921,&g_921,&g_921,&l_23,&g_549[4][0]}},{{&g_549[4][1],&g_549[2][0],&l_23,&l_23,&g_549[4][1],&g_921},{(void*)0,&g_549[4][1],&l_23,&g_549[1][0],&g_921,&g_549[4][0]},{&g_921,&g_549[1][0],&g_921,&g_549[4][0],&g_921,&g_549[1][0]},{&g_549[4][0],&g_921,&g_549[1][0],&g_921,(void*)0,(void*)0},{&g_549[1][0],&l_23,&g_549[4][1],(void*)0,&l_23,(void*)0},{&l_23,&l_23,&g_549[2][0],&g_549[4][1],(void*)0,&g_921},{&g_921,&g_921,&g_921,&g_921,&g_921,&g_921},{(void*)0,&g_549[1][0],(void*)0,&g_921,&g_921,&g_921}}};
        int32_t l_1328 = 0x09671E5BL;
        int32_t l_1329 = 0x0B6F1CBBL;
        uint32_t *l_1330 = &g_194;
        int8_t *l_1438[1][4][4] = {{{(void*)0,&g_1196,(void*)0,&g_1196},{(void*)0,&g_1196,(void*)0,&g_1196},{(void*)0,&g_1196,(void*)0,&g_1196},{(void*)0,&g_1196,(void*)0,&g_1196}}};
        int32_t l_1499 = 0x56DFB19CL;
        int32_t l_1501 = 0x91A479ACL;
        uint16_t l_1511 = 0UL;
        const uint16_t *l_1609 = &g_623;
        const uint16_t **l_1608[7] = {&l_1609,&l_1609,(void*)0,&l_1609,&l_1609,(void*)0,&l_1609};
        int16_t l_1652[3][5] = {{1L,1L,1L,1L,1L},{0L,1L,0L,1L,0L},{1L,1L,1L,1L,1L}};
        int16_t l_1708 = 1L;
        int16_t l_1762 = 0L;
        const int8_t l_1771 = 6L;
        uint16_t l_1773 = 65528UL;
        int32_t *l_1776 = &g_34[1][8][0];
        int i, j, k;
    }
    (*g_1813) ^= ((safe_mul_func_int8_t_s_s((&l_1655[1] == &g_1423), ((*g_1326) &= (((*l_1806) = g_1804) != ((*l_1811) = g_1807))))) | (((g_912 = (void*)0) != g_1813) & ((*g_680) ^= ((*l_5) , (safe_rshift_func_int16_t_s_s((*g_908), (safe_div_func_int32_t_s_s(((((0xFBB8L != l_1818) > (*l_5)) && (*g_844)) != (*l_5)), (*l_3)))))))));
    return (*l_5);
}


/* ------------------------------------------ */
/* 
 * reads : g_722 g_844 g_1088 g_894 g_636 g_1089 g_1090 g_663 g_664 g_430 g_348 g_4 g_173 g_181 g_680 g_73 g_26 g_1015 g_612 g_1030 g_613 g_98 g_617 g_454 g_908 g_550 g_549 g_1274 g_912
 * writes: g_722 g_454 g_636 g_119 g_4 g_26 g_173 g_181 g_617 g_549 g_912
 */
static const int32_t * func_13(int32_t * p_14, uint64_t  p_15, uint32_t  p_16)
{ /* block id: 612 */
    int32_t l_1344 = 1L;
    int32_t *l_1346 = &g_26[3];
    int16_t **l_1366 = &g_894;
    uint8_t ***l_1371[9][8] = {{(void*)0,&g_613,(void*)0,&g_613,&g_613,&g_613,&g_613,&g_613},{&g_613,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613,(void*)0},{&g_613,&g_613,&g_613,(void*)0,&g_613,(void*)0,&g_613,(void*)0},{&g_613,(void*)0,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613},{(void*)0,&g_613,&g_613,&g_613,&g_613,&g_613,(void*)0,&g_613},{(void*)0,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613},{(void*)0,&g_613,&g_613,&g_613,&g_613,(void*)0,&g_613,&g_613},{(void*)0,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613,(void*)0},{(void*)0,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613,&g_613}};
    uint16_t l_1373 = 0x5DE8L;
    int32_t l_1402 = 0x56F5E6C7L;
    int32_t l_1403 = 0x4D5E9807L;
    int32_t l_1404 = 0x54BEB4B9L;
    int32_t l_1406 = 0x35A5D3A4L;
    int32_t l_1407 = 0x376592DAL;
    int32_t l_1408[4][8] = {{1L,(-1L),(-1L),1L,(-1L),(-1L),1L,(-1L)},{1L,1L,0x4DED9D86L,1L,1L,0x4DED9D86L,1L,1L},{(-1L),1L,(-1L),(-1L),1L,(-1L),(-1L),1L},{1L,(-1L),(-1L),1L,(-1L),(-1L),1L,(-1L)}};
    uint64_t *l_1421 = &g_275[9];
    uint64_t **l_1420 = &l_1421;
    int32_t **l_1424 = &g_912;
    int i, j;
    for (g_722 = 20; (g_722 <= 44); g_722 = safe_add_func_uint32_t_u_u(g_722, 8))
    { /* block id: 615 */
        uint64_t *l_1336[4][4] = {{&g_275[4],&g_275[4],(void*)0,&g_275[4]},{&g_275[4],&g_696,&g_696,&g_275[4]},{&g_696,&g_275[4],&g_696,&g_696},{&g_275[4],&g_275[4],(void*)0,&g_275[4]}};
        int32_t l_1337 = (-1L);
        const int32_t l_1339 = (-1L);
        int32_t *l_1345 = &g_4[4][0];
        int i, j;
        (*l_1345) &= ((safe_sub_func_int8_t_s_s(1L, (((*g_844) = 0x90A65C7DL) ^ ((((0x91L >= ((((((((l_1337 = g_1088[7]) | (((*g_348) = (!(l_1339 & ((*g_894) = ((safe_lshift_func_uint16_t_u_s(l_1339, (l_1339 != ((0xC8A50604L <= (((safe_rshift_func_int8_t_s_u((255UL && (-1L)), p_16)) , p_16) <= (*g_894))) , l_1339)))) != (****g_1089)))))) != p_16)) ^ p_15) , l_1344) == l_1339) > p_16) != p_16) < l_1339)) && p_16) || l_1337) >= p_16)))) , 0xFFE31AD5L);
    }
    (*l_1346) = l_1344;
    for (g_173 = (-14); (g_173 >= (-28)); g_173 = safe_sub_func_uint64_t_u_u(g_173, 1))
    { /* block id: 625 */
        int16_t l_1351 = 6L;
        int16_t **l_1368[5];
        int32_t l_1390 = 0x51388566L;
        int32_t l_1391 = (-10L);
        int32_t l_1392 = 8L;
        int32_t l_1393 = 5L;
        int32_t l_1394 = 1L;
        int32_t l_1395 = 1L;
        int32_t l_1396 = 0L;
        int32_t l_1397 = 1L;
        int32_t l_1399 = (-5L);
        int32_t l_1400 = 0L;
        int32_t l_1401[4] = {0x08A0CEB2L,0x08A0CEB2L,0x08A0CEB2L,0x08A0CEB2L};
        uint16_t l_1409 = 0xD88CL;
        int i;
        for (i = 0; i < 5; i++)
            l_1368[i] = (void*)0;
        for (p_16 = 25; (p_16 == 10); p_16 = safe_sub_func_int64_t_s_s(p_16, 2))
        { /* block id: 628 */
            if (l_1351)
                break;
        }
        for (g_181 = 0; (g_181 < 48); g_181++)
        { /* block id: 633 */
            int16_t **l_1367 = (void*)0;
            int32_t l_1372 = 0x053EDED6L;
            uint64_t *l_1374 = (void*)0;
            uint64_t *l_1375 = &g_617;
            int32_t l_1376 = (-1L);
            int32_t l_1398[2];
            int32_t *l_1412 = &g_133;
            int32_t *l_1413 = (void*)0;
            int32_t *l_1414[5] = {&l_1401[2],&l_1401[2],&l_1401[2],&l_1401[2],&l_1401[2]};
            uint32_t l_1415 = 18446744073709551615UL;
            int i;
            for (i = 0; i < 2; i++)
                l_1398[i] = 0x063DB6DBL;
            if ((p_16 , (safe_lshift_func_uint16_t_u_s((((l_1376 ^= (&g_486[1] != (((((((*l_1375) ^= (safe_mod_func_uint64_t_u_u(((*g_680) < ((**g_613) = ((safe_mul_func_uint16_t_u_u(((safe_mod_func_uint8_t_u_u((*l_1346), (safe_rshift_func_uint8_t_u_s((safe_mod_func_int64_t_s_s(((l_1367 = l_1366) != (p_16 , l_1368[4])), ((safe_mul_func_uint16_t_u_u(((*g_1015) == l_1371[2][5]), l_1372)) , g_1030))), l_1373)))) && l_1351), (*g_894))) & 0xD3BBFDE3L))), g_98))) , (*l_1367)) == (*l_1366)) < l_1372) , (*g_844)) , &g_486[1]))) == l_1372) , p_16), p_15))))
            { /* block id: 638 */
                int32_t *l_1383 = (void*)0;
                int32_t *l_1384 = &g_549[0][1];
                uint64_t *l_1387 = &g_275[9];
                int32_t *l_1388 = &g_4[3][8];
                (*l_1388) |= ((safe_sub_func_uint32_t_u_u(0x471F409FL, (((safe_add_func_int16_t_s_s((p_15 == (p_16 == (*l_1346))), (((((safe_lshift_func_int16_t_s_s((*g_908), (((*l_1384) &= p_15) , (l_1351 || l_1351)))) && (((((safe_add_func_int32_t_s_s(((void*)0 == l_1387), (*l_1346))) , l_1351) > p_15) & (-3L)) > (*g_844))) | g_1274) != p_15) <= (*l_1346)))) , (*l_1346)) > l_1351))) <= (*l_1346));
            }
            else
            { /* block id: 641 */
                int32_t **l_1389[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                int64_t l_1405 = (-2L);
                int i;
                g_912 = &l_1376;
                if ((*g_912))
                    break;
                l_1409++;
            }
            --l_1415;
        }
    }
    (*l_1424) = p_14;
    return (*l_1424);
}


/* ------------------------------------------ */
/* 
 * reads : g_26 g_4 g_844 g_454 g_706 g_680 g_73 g_1090 g_663 g_664 g_430 g_114 g_637
 * writes: g_26 g_4 g_34 g_1070 g_912 g_303 g_114 g_133
 */
static uint32_t  func_17(uint32_t  p_18)
{ /* block id: 5 */
    int16_t l_64 = 0L;
    int32_t *l_1279[7] = {&g_26[0],&g_26[0],&g_26[0],&g_26[0],&g_26[0],&g_26[0],&g_26[0]};
    const uint16_t *l_1305[9][10] = {{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_637,&g_637,&g_1030},{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_637,&g_637,&g_1030},{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_637,&g_637,&g_1030},{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_637,&g_637,&g_1030},{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_637,&g_637,&g_1030},{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_637,&g_637,&g_1030},{&g_623,&g_637,&g_623,&g_1030,&g_637,&g_637,&g_637,&g_623,&g_623,&g_637},{&g_101,(void*)0,&g_101,&g_637,&g_623,&g_623,&g_623,&g_623,&g_623,&g_637},{&g_101,(void*)0,&g_101,&g_637,&g_623,&g_623,&g_623,&g_623,&g_623,&g_637}};
    uint64_t l_1307 = 0x0BF895825340D4EFLL;
    uint32_t l_1317 = 9UL;
    int i, j;
    for (p_18 = 8; (p_18 < 15); p_18 = safe_add_func_uint8_t_u_u(p_18, 5))
    { /* block id: 8 */
        uint64_t l_31 = 0x1FD00955DF93C581LL;
        int32_t l_136 = 0x53C73CE1L;
        int32_t l_197 = 0x6BD755E5L;
        int32_t *l_198 = &g_4[2][8];
        int64_t l_746[10][4][5] = {{{0x820011D095AFDBACLL,0L,0xC0154FB51D710750LL,0xA0BD590DFFF54561LL,0x820011D095AFDBACLL},{0x1F9FC27957CCA152LL,0xE96B3EF9D1A22BE7LL,0L,0L,0xE96B3EF9D1A22BE7LL},{0xE6557AD874D95B1ALL,0xFDD8FDE7180C38BELL,(-4L),0xA0BD590DFFF54561LL,0L},{0xE96B3EF9D1A22BE7LL,8L,1L,0xE96B3EF9D1A22BE7LL,1L}},{{(-5L),0xA0BD590DFFF54561LL,(-5L),2L,0x820011D095AFDBACLL},{0xE96B3EF9D1A22BE7LL,0x6302F01519CBF16DLL,0x5BCD57058F489DD7LL,8L,0x1F9FC27957CCA152LL},{0xE6557AD874D95B1ALL,0L,(-7L),0L,0xE6557AD874D95B1ALL},{0x1F9FC27957CCA152LL,8L,0x5BCD57058F489DD7LL,0x6302F01519CBF16DLL,0xE96B3EF9D1A22BE7LL}},{{0x820011D095AFDBACLL,2L,(-5L),0xA0BD590DFFF54561LL,(-5L)},{1L,0xE96B3EF9D1A22BE7LL,1L,8L,0xE96B3EF9D1A22BE7LL},{0L,0xA0BD590DFFF54561LL,(-4L),0xFDD8FDE7180C38BELL,0xE6557AD874D95B1ALL},{0xE96B3EF9D1A22BE7LL,0L,0L,0xE96B3EF9D1A22BE7LL,0x1F9FC27957CCA152LL}},{{0x820011D095AFDBACLL,0xA0BD590DFFF54561LL,0xC0154FB51D710750LL,0L,0x820011D095AFDBACLL},{0x6302F01519CBF16DLL,0xE96B3EF9D1A22BE7LL,0x5BCD57058F489DD7LL,0L,1L},{0xE6557AD874D95B1ALL,2L,(-4L),0L,0L},{1L,8L,(-10L),0xE96B3EF9D1A22BE7LL,0xE96B3EF9D1A22BE7LL}},{{(-5L),0L,(-5L),0xFDD8FDE7180C38BELL,0x820011D095AFDBACLL},{1L,0x6302F01519CBF16DLL,0L,8L,0x6302F01519CBF16DLL},{0xE6557AD874D95B1ALL,0xA0BD590DFFF54561LL,(-7L),0xA0BD590DFFF54561LL,0xE6557AD874D95B1ALL},{0x6302F01519CBF16DLL,8L,0L,0x6302F01519CBF16DLL,1L}},{{0x820011D095AFDBACLL,0xFDD8FDE7180C38BELL,(-5L),0L,(-5L)},{0xE96B3EF9D1A22BE7LL,0xE96B3EF9D1A22BE7LL,(-10L),8L,1L},{0L,0L,(-4L),2L,0xE6557AD874D95B1ALL},{1L,0L,0x5BCD57058F489DD7LL,0xE96B3EF9D1A22BE7LL,0x6302F01519CBF16DLL}},{{0x820011D095AFDBACLL,0L,0xC0154FB51D710750LL,0xA0BD590DFFF54561LL,0x820011D095AFDBACLL},{0x1F9FC27957CCA152LL,0xE96B3EF9D1A22BE7LL,0L,0L,0xE96B3EF9D1A22BE7LL},{0xE6557AD874D95B1ALL,0xFDD8FDE7180C38BELL,(-4L),0xA0BD590DFFF54561LL,0L},{0xE96B3EF9D1A22BE7LL,8L,1L,0xE96B3EF9D1A22BE7LL,1L}},{{(-5L),0xA0BD590DFFF54561LL,(-5L),2L,0x820011D095AFDBACLL},{0xE96B3EF9D1A22BE7LL,0x6302F01519CBF16DLL,0x5BCD57058F489DD7LL,8L,0x1F9FC27957CCA152LL},{0xE6557AD874D95B1ALL,0L,(-7L),0L,0xE6557AD874D95B1ALL},{0x1F9FC27957CCA152LL,8L,0x5BCD57058F489DD7LL,0x6302F01519CBF16DLL,0xE96B3EF9D1A22BE7LL}},{{0x820011D095AFDBACLL,2L,(-5L),0xA0BD590DFFF54561LL,(-5L)},{1L,0xE96B3EF9D1A22BE7LL,1L,0L,0x6302F01519CBF16DLL},{0xEB36C9597AE69B86LL,0xFDD8FDE7180C38BELL,(-7L),0x3DFC03CBD36F2CEFLL,0L},{0x6302F01519CBF16DLL,1L,1L,0x6302F01519CBF16DLL,0L}},{{(-5L),0xFDD8FDE7180C38BELL,0x820011D095AFDBACLL,2L,(-5L)},{0x7C353E383D47B3D5LL,0x6302F01519CBF16DLL,(-10L),1L,0x1F9FC27957CCA152LL},{0L,(-1L),(-7L),2L,0xEB36C9597AE69B86LL},{0x1F9FC27957CCA152LL,0L,1L,0x6302F01519CBF16DLL,0x6302F01519CBF16DLL}}};
        int64_t *** const l_1302 = &g_663[1];
        uint16_t *l_1306 = (void*)0;
        int i, j, k;
        for (g_26[0] = 0; (g_26[0] <= 5); g_26[0] += 1)
        { /* block id: 11 */
            int32_t *l_27 = &g_4[2][8];
            int32_t *l_28 = (void*)0;
            int32_t *l_29 = &g_4[2][8];
            int32_t *l_30[1][9];
            uint8_t l_1296 = 0x45L;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 9; j++)
                    l_30[i][j] = &g_4[g_26[0]][g_26[0]];
            }
            g_4[g_26[0]][g_26[0]] |= p_18;
            l_31--;
            for (l_31 = 0; (l_31 <= 5); l_31 += 1)
            { /* block id: 16 */
                int64_t l_36 = (-5L);
                int32_t l_37 = 0xE4AA2297L;
                int32_t *l_1299 = &g_549[4][0];
                int i, j;
                for (g_34[1][6][1] = 0; (g_34[1][6][1] <= 5); g_34[1][6][1] += 1)
                { /* block id: 19 */
                    int32_t l_35[6] = {(-1L),1L,(-1L),(-1L),1L,(-1L)};
                    uint64_t l_38[10][9] = {{0xD4FCBE7CA3003AF4LL,0xBB9F80B5C327FA81LL,18446744073709551615UL,0UL,0UL,0xD4FCBE7CA3003AF4LL,0UL,0x8686667EE9F729FALL,0x889C39DB43F150FDLL},{0x0064457BBF0BED92LL,0UL,18446744073709551615UL,0x8686667EE9F729FALL,0x4A04EC100FABB65FLL,0xCA3B757A549BDA14LL,18446744073709551610UL,0x0064457BBF0BED92LL,0UL},{0xF9042C8AC77275A2LL,0x4A04EC100FABB65FLL,0x5DC4542491522484LL,0xCA3B757A549BDA14LL,0xD8E95A6083B6D913LL,0xCA3B757A549BDA14LL,0x5DC4542491522484LL,0x4A04EC100FABB65FLL,0xF9042C8AC77275A2LL},{18446744073709551613UL,0x8686667EE9F729FALL,0xD93D5F64BAD57AD3LL,0UL,0x0E34D29090F62EB7LL,0xD4FCBE7CA3003AF4LL,0UL,18446744073709551612UL,0x8686667EE9F729FALL},{0UL,0x19870F1E569BB213LL,1UL,0x7FC81C9D718AB875LL,0x30A6D4616B4C8AB2LL,18446744073709551615UL,0x841160C006E74240LL,0x889C39DB43F150FDLL,0x7FC81C9D718AB875LL},{18446744073709551613UL,0x0E34D29090F62EB7LL,0UL,0UL,0x5DC4542491522484LL,0UL,18446744073709551606UL,0UL,0xD93D5F64BAD57AD3LL},{0xF9042C8AC77275A2LL,0UL,0UL,0x5DC4542491522484LL,0x5DC4542491522484LL,0UL,0UL,0xF9042C8AC77275A2LL,0UL},{0x0064457BBF0BED92LL,0UL,18446744073709551615UL,0UL,0x30A6D4616B4C8AB2LL,0x0064457BBF0BED92LL,0x5DC4542491522484LL,0x8686667EE9F729FALL,0xCA3B757A549BDA14LL},{0xD4FCBE7CA3003AF4LL,0UL,18446744073709551612UL,0x8686667EE9F729FALL,0x0E34D29090F62EB7LL,0xBB9F80B5C327FA81LL,0x4A04EC100FABB65FLL,0x7FC81C9D718AB875LL,0UL},{0x4A04EC100FABB65FLL,18446744073709551610UL,1UL,0x889C39DB43F150FDLL,0xD8E95A6083B6D913LL,0UL,18446744073709551613UL,0xD93D5F64BAD57AD3LL,0xD93D5F64BAD57AD3LL}};
                    int32_t *l_1298 = &g_549[4][0];
                    int32_t **l_1297 = &l_1298;
                    int i, j;
                    l_38[5][8]--;
                    for (l_37 = 0; (l_37 >= 0); l_37 -= 1)
                    { /* block id: 23 */
                        uint8_t *l_62 = &g_63;
                        int8_t *l_72 = &g_73;
                        int i, j;
                    }
                    for (g_1070 = 0; (g_1070 <= 0); g_1070 += 1)
                    { /* block id: 581 */
                        int8_t l_1278[4][3][3] = {{{(-7L),(-1L),0x36L},{(-7L),0xFBL,(-7L)},{(-7L),0x99L,0x7FL}},{{(-7L),(-1L),0x36L},{(-7L),0xFBL,(-7L)},{(-7L),0x99L,0x7FL}},{{(-7L),(-1L),0x36L},{(-7L),0xFBL,(-7L)},{(-7L),0x99L,0x7FL}},{{(-7L),(-1L),0x36L},{(-7L),0xFBL,(-7L)},{(-7L),0x99L,0x7FL}}};
                        int i, j, k;
                        if (l_1278[3][1][0])
                            break;
                    }
                    g_912 = &l_37;
                }
                if (g_4[g_26[0]][(l_31 + 1)])
                    break;
            }
        }
        g_114 &= ((((*l_198) || (((*l_198) <= ((*g_844) | p_18)) > ((((((0x7BE4E9C3L != ((void*)0 != l_1302)) & ((safe_sub_func_uint8_t_u_u((l_1305[2][9] == ((*g_706) = l_1306)), p_18)) && (*l_198))) <= (*g_680)) >= 0x848F10CBL) & (***g_1090)) , (*g_844)))) != p_18) < 6UL);
    }
    ++l_1307;
    if (p_18)
    { /* block id: 598 */
        g_133 = (safe_rshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u(p_18, g_637)), 4));
        return (*g_844);
    }
    else
    { /* block id: 601 */
        uint16_t l_1314 = 0UL;
        ++l_1314;
        l_1317 ^= p_18;
    }
    return (*g_844);
}


/* ------------------------------------------ */
/* 
 * reads : g_100 g_612 g_613 g_348 g_549 g_26 g_34 g_119 g_430 g_807 g_133 g_114 g_680 g_73 g_275 g_183 g_326 g_844 g_454 g_637 g_679 g_636 g_907 g_173 g_432 g_98 g_63 g_550 g_617 g_623 g_194 g_663 g_410 g_642 g_4 g_696 g_705 g_722 g_743 g_924 g_894 g_908 g_912 g_706 g_303 g_664 g_1013 g_1030 g_1015 g_1070 g_1088 g_1089 g_1103 g_1090 g_1199 g_267 g_486 g_1274 g_181 g_551
 * writes: g_100 g_98 g_119 g_807 g_133 g_114 g_183 g_844 g_454 g_722 g_637 g_894 g_430 g_73 g_432 g_326 g_101 g_275 g_617 g_623 g_194 g_663 g_550 g_743 g_912 g_924 g_636 g_173 g_664 g_551 g_1015 g_181 g_1030 g_1013 g_1070 g_1103 g_348 g_1199 g_1090 g_1274
 */
static int32_t  func_41(int32_t * p_42, int32_t  p_43, uint64_t  p_44)
{ /* block id: 355 */
    uint32_t l_770[8] = {0x20FFE26CL,0x752FAE71L,0x20FFE26CL,0x752FAE71L,0x20FFE26CL,0x752FAE71L,0x20FFE26CL,0x752FAE71L};
    uint8_t ***l_791 = &g_613;
    uint8_t ****l_792 = (void*)0;
    uint8_t ****l_793 = &l_791;
    uint16_t *l_800[3];
    int32_t l_801 = 0xA1FBD972L;
    const int32_t l_802 = (-8L);
    int64_t l_803 = 0x02B91D17BB413306LL;
    int32_t l_804[6][9][1] = {{{0L},{0x4C0829FEL},{1L},{1L},{0x43C6137AL},{0xA6EB8C19L},{0x6FCD0391L},{0x4C0829FEL},{0x6FCD0391L}},{{0xA6EB8C19L},{0x43C6137AL},{1L},{1L},{0x4C0829FEL},{0L},{0xBBB3B572L},{0x43C6137AL},{0xBBB3B572L}},{{0L},{0L},{0x35D724F2L},{0xD9824FB5L},{0xE86DC89EL},{0x4C0829FEL},{8L},{0L},{8L}},{{0x4C0829FEL},{0xE86DC89EL},{0xD9824FB5L},{0x35D724F2L},{0L},{0x43C6137AL},{(-5L)},{0xE86DC89EL},{(-5L)}},{{0x43C6137AL},{0L},{0x35D724F2L},{0xD9824FB5L},{0xE86DC89EL},{0x4C0829FEL},{8L},{0L},{8L}},{{0x4C0829FEL},{0xE86DC89EL},{0xD9824FB5L},{0x35D724F2L},{0L},{0x43C6137AL},{(-5L)},{0xE86DC89EL},{(-5L)}}};
    int32_t *l_805 = (void*)0;
    int32_t *l_806[5];
    int8_t **l_859 = &g_680;
    int8_t l_863 = 1L;
    int32_t l_1011 = 4L;
    int64_t l_1017 = (-1L);
    int64_t ***l_1035 = (void*)0;
    int64_t ***l_1036[2][10] = {{(void*)0,&g_663[3],&g_663[3],(void*)0,&g_663[3],(void*)0,&g_663[3],&g_663[3],(void*)0,&g_663[3]},{(void*)0,&g_663[3],&g_663[3],(void*)0,&g_663[3],(void*)0,&g_663[3],&g_663[3],(void*)0,&g_663[3]}};
    uint8_t l_1124[1];
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_800[i] = &g_623;
    for (i = 0; i < 5; i++)
        l_806[i] = &g_114;
    for (i = 0; i < 1; i++)
        l_1124[i] = 255UL;
    for (g_100 = 0; (g_100 <= 20); g_100 = safe_add_func_int16_t_s_s(g_100, 2))
    { /* block id: 358 */
        int32_t *l_756 = &g_114;
        int32_t *l_757 = &g_98;
        int32_t l_758 = 0x51FCBDBAL;
        int32_t *l_759 = &g_98;
        int32_t l_760 = (-3L);
        int32_t *l_761 = &g_114;
        int32_t *l_762 = &l_758;
        int32_t *l_763 = &l_760;
        int32_t l_764 = 2L;
        int32_t *l_765 = &l_758;
        int32_t *l_766 = &g_114;
        int32_t *l_767 = &g_98;
        int32_t *l_768 = &g_98;
        int32_t *l_769[1];
        uint8_t *l_778[8][5] = {{&g_181,&g_181,&g_63,&g_181,&g_181},{&g_432,&g_181,&g_432,&g_432,&g_181},{&g_181,&g_432,&g_432,&g_181,&g_432},{&g_181,&g_181,&g_63,&g_181,&g_181},{&g_432,&g_181,&g_432,&g_432,&g_181},{&g_181,&g_432,&g_432,&g_181,&g_432},{&g_181,&g_181,&g_63,&g_181,&g_181},{&g_432,&g_181,&g_432,&g_432,&g_181}};
        int i, j;
        for (i = 0; i < 1; i++)
            l_769[i] = (void*)0;
        l_770[3]--;
        (*l_768) = (safe_add_func_uint16_t_u_u((p_43 | (safe_add_func_int32_t_s_s((!g_100), (l_778[2][0] == (**g_612))))), 0x8ACEL));
    }
    l_804[4][8][0] = (safe_div_func_int8_t_s_s((safe_add_func_int32_t_s_s((((p_44 & (safe_mod_func_uint8_t_u_u((((safe_mul_func_uint8_t_u_u(((***g_612) = 0x44L), ((((safe_mod_func_int16_t_s_s((g_549[1][0] , (safe_rshift_func_int8_t_s_s((((*l_793) = l_791) != &g_613), 2))), ((safe_mod_func_uint8_t_u_u(p_43, ((safe_sub_func_uint16_t_u_u(p_44, ((safe_rshift_func_uint8_t_u_s(0xC2L, 7)) >= (l_801 = (p_44 == p_44))))) , g_26[4]))) , 3L))) , l_802) , l_803) != 0x2B3F6B75L))) | (*p_42)) , (***g_612)), l_770[0]))) ^ l_770[3]) | 0x6DL), g_430[3][8])), p_43));
    ++g_807;
    if ((*p_42))
    { /* block id: 367 */
        uint64_t l_813 = 0xD286479B4372FB38LL;
        int32_t l_816[2][4][1] = {{{1L},{1L},{1L},{1L}},{{1L},{1L},{1L},{1L}}};
        int32_t l_825 = 0x6B600E8EL;
        uint8_t l_843 = 0x18L;
        uint32_t *l_860 = &g_722;
        uint64_t *l_867 = (void*)0;
        uint64_t *l_868[7];
        int16_t *l_881 = &g_550[0];
        int16_t **l_906 = &l_881;
        int i, j, k;
        for (i = 0; i < 7; i++)
            l_868[i] = (void*)0;
        for (g_133 = 0; (g_133 <= 7); g_133 += 1)
        { /* block id: 370 */
            uint8_t ***l_812 = &g_613;
            uint32_t l_824 = 7UL;
            int64_t *l_833 = &g_183;
            uint32_t *l_834 = (void*)0;
            int32_t l_835 = (-1L);
            int32_t l_836 = (-8L);
            int32_t l_837 = 1L;
            int i;
            l_813 = (safe_lshift_func_uint16_t_u_u((l_812 != l_812), 11));
            l_837 ^= (safe_add_func_int32_t_s_s((l_816[0][2][0] ^= 0x90FAB5D7L), (((g_98 = (safe_unary_minus_func_uint32_t_u((safe_div_func_int32_t_s_s((((safe_lshift_func_uint16_t_u_u((((safe_sub_func_uint32_t_u_u(l_770[g_133], (l_835 = (((**g_613) = (l_770[g_133] != ((l_825 = l_824) || 65526UL))) , (safe_div_func_int32_t_s_s(((g_114 |= (*p_42)) == ((-4L) <= ((!(*g_680)) | ((safe_sub_func_int64_t_s_s(((*l_833) &= (safe_mod_func_int32_t_s_s((g_275[9] || 4294967295UL), 0x2AF29377L))), 0UL)) , 0xE4L)))), g_26[1])))))) ^ l_824) ^ (-3L)), p_43)) , p_44) & l_836), p_44))))) && 0L) ^ l_813)));
            l_836 = (((~((*l_833) = (safe_sub_func_int32_t_s_s((*p_42), (((***g_612) & (l_843 = (safe_mul_func_int16_t_s_s(1L, g_326)))) >= 1L))))) , l_816[0][2][0]) == (p_42 != (g_844 = &g_454[0][6][3])));
        }
        if (((safe_sub_func_int64_t_s_s((safe_rshift_func_int8_t_s_s((((*l_860) = (g_430[3][2] , (((((*g_844) &= (safe_div_func_int32_t_s_s((safe_rshift_func_int16_t_s_u((safe_lshift_func_uint16_t_u_u(0xE971L, (p_43 == (l_843 && (safe_lshift_func_uint16_t_u_s(p_43, 12)))))), l_816[1][2][0])), 0x2742AE5CL))) | (safe_sub_func_uint16_t_u_u((l_859 != (void*)0), l_843))) || 0x8C9E42D02FC32521LL) == 0x00BC07F59ACFE5ACLL))) > g_183), 2)), p_44)) <= p_43))
        { /* block id: 387 */
            l_816[0][2][0] &= (*p_42);
        }
        else
        { /* block id: 389 */
            for (p_44 = 0; p_44 < 8; p_44 += 1)
            {
                l_770[p_44] = 6UL;
            }
        }
        if ((safe_add_func_uint8_t_u_u(l_863, ((((((safe_rshift_func_int16_t_s_u(((l_816[0][3][0] = (!(((**g_613) ^= 0UL) | p_44))) == ((~((0x0BCC332995EB342DLL ^ ((safe_mod_func_int8_t_s_s((+(safe_mul_func_uint8_t_u_u(p_43, l_843))), ((p_44 == (safe_mul_func_uint16_t_u_u((--g_637), (safe_sub_func_uint32_t_u_u(((l_825 ^= ((p_44 <= (&g_636[0] == l_881)) | 0x51129060L)) , 0x3A79A2A6L), 0xADDDEB3BL))))) && p_44))) > p_43)) ^ 7UL)) ^ p_43)), 12)) < p_43) ^ (**g_679)) >= (*p_42)) <= p_44) >= 4UL))))
        { /* block id: 396 */
            int16_t **l_893[9][4] = {{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881},{&l_881,&l_881,&l_881,&l_881}};
            uint8_t l_903[9][4] = {{0x37L,0x37L,1UL,0x65L},{0x37L,255UL,0UL,0x37L},{0UL,0x65L,0UL,0UL},{0xA4L,0x65L,1UL,0x37L},{0x65L,255UL,255UL,0x65L},{0UL,0x37L,255UL,0UL},{0x65L,0xA4L,1UL,0xA4L},{0xA4L,255UL,0UL,0xA4L},{0UL,0xA4L,0UL,0UL}};
            int32_t l_904[8][6] = {{0xEE4C76C4L,(-5L),0x8E4664D6L,4L,(-9L),4L},{4L,(-1L),4L,0x7BC78407L,(-9L),0x7C0818D9L},{0x8E4664D6L,(-5L),0xEE4C76C4L,4L,4L,4L},{0x7BC78407L,0x7BC78407L,0xCDDF62F5L,4L,0x7C0818D9L,0x8E4664D6L},{(-1L),0xCDDF62F5L,0xEE4C76C4L,0x8E4664D6L,0xEE4C76C4L,0xCDDF62F5L},{4L,(-1L),0xEE4C76C4L,1L,0x7BC78407L,0x8E4664D6L},{4L,1L,0xCDDF62F5L,0xCDDF62F5L,1L,4L},{0xCDDF62F5L,1L,4L,(-5L),0x7BC78407L,0xEE4C76C4L}};
            int64_t *l_905 = &g_430[0][1];
            int32_t *l_910 = &g_549[4][0];
            int32_t **l_909 = &l_910;
            int32_t **l_911 = (void*)0;
            int i, j;
            g_912 = func_51((((safe_sub_func_uint32_t_u_u(1UL, (((&p_43 != ((*l_909) = func_51((((p_43 | (safe_add_func_uint16_t_u_u((p_44 & g_326), (((safe_mod_func_uint8_t_u_u((((~(safe_mul_func_uint8_t_u_u((--(***l_791)), 246UL))) , (g_894 = &g_550[0])) == l_800[1]), (((((((*l_905) = (((((l_904[1][0] &= (((safe_lshift_func_int8_t_s_s(((((safe_add_func_uint64_t_u_u(((safe_rshift_func_int8_t_s_u(l_903[5][0], p_43)) < p_44), l_903[5][0])) || 8UL) > (-3L)) & l_903[5][0]), 5)) >= l_903[5][0]) || 0x65L)) <= p_43) || 254UL) | p_44) & 1L)) || 0UL) != (-9L)) >= g_636[0]) , 0xD654L) & p_43))) ^ p_43) != p_43)))) , l_906) == g_907[0]), p_42, p_42))) , 0x014CA70BL) > (*p_42)))) > 0xA105L) == p_44), &l_804[4][8][0], &l_816[0][2][0]);
        }
        else
        { /* block id: 403 */
            uint32_t l_913 = 18446744073709551615UL;
            ++l_913;
        }
    }
    else
    { /* block id: 406 */
        int64_t l_916[4] = {6L,6L,6L,6L};
        int32_t l_917 = 0xC44F1B38L;
        int32_t l_918 = 0xB5D9EF96L;
        int32_t l_919 = 0xBB44C106L;
        int32_t l_920 = 0x0C0C2694L;
        int32_t l_923[6];
        int32_t l_937 = 0xC879A299L;
        uint64_t *l_940 = &g_924;
        uint64_t *l_942 = (void*)0;
        uint64_t **l_941 = &l_942;
        int32_t *l_945 = (void*)0;
        int32_t *l_946[4] = {&g_549[4][0],&g_549[4][0],&g_549[4][0],&g_549[4][0]};
        int16_t *l_953 = &g_636[0];
        int64_t ***l_986 = &g_663[3];
        uint16_t ***l_995 = (void*)0;
        uint16_t l_1069 = 0UL;
        int32_t *l_1093 = &l_919;
        uint8_t *l_1173 = (void*)0;
        int16_t l_1194 = 0xA925L;
        uint32_t l_1247 = 4294967295UL;
        int32_t l_1271 = 0x9EDBF29CL;
        int i;
        for (i = 0; i < 6; i++)
            l_923[i] = (-5L);
lbl_1248:
        --g_924;
        if ((((safe_add_func_int64_t_s_s((&l_803 != &l_916[3]), ((((safe_add_func_uint64_t_u_u((safe_lshift_func_uint16_t_u_u((1L | (((l_919 = (l_937 = (l_918 |= (safe_lshift_func_int16_t_s_s((safe_mod_func_int64_t_s_s((((*g_894) = (((l_937 | l_923[1]) && (safe_mod_func_int32_t_s_s((l_919 ^ (&p_44 != ((*l_941) = (l_940 = &g_551)))), (safe_mod_func_int8_t_s_s((*g_680), l_920))))) || p_43)) | p_44), 0xEF8AAD1DF5B73F85LL)), 0))))) , (void*)0) == l_946[1])), 9)), 0x0CE1FC791D1EF6C7LL)) && 0x98E6ED13L) , p_44) != p_43))) ^ (-10L)) > (**g_679)))
        { /* block id: 414 */
            int32_t ***l_962 = &g_107;
            int32_t l_1021 = 0xAF8C6A47L;
            int32_t l_1028[7][3][1] = {{{0xFB5ED261L},{0L},{0xFB5ED261L}},{{0L},{0xFB5ED261L},{0L}},{{0xFB5ED261L},{0L},{0xFB5ED261L}},{{0L},{0xFB5ED261L},{0L}},{{0xFB5ED261L},{0L},{0xFB5ED261L}},{{0L},{0xFB5ED261L},{0L}},{{0xFB5ED261L},{0L},{0xFB5ED261L}}};
            int64_t ***l_1038 = (void*)0;
            uint64_t l_1048 = 0UL;
            int16_t l_1051 = 0x69CEL;
            int8_t l_1068[1][4][6] = {{{0x6AL,1L,0x8CL,0x8CL,1L,0x6AL},{0x6AL,0xF3L,0xF2L,0x8CL,0xF3L,0x8CL},{0x6AL,0L,0x6AL,0x8CL,0L,0xF2L},{0x6AL,1L,0x8CL,0x8CL,1L,0x6AL}}};
            uint8_t ****l_1086 = (void*)0;
            int16_t **l_1117 = &l_953;
            uint16_t **l_1187[10][9];
            int32_t l_1192[10];
            uint16_t **l_1193 = (void*)0;
            int i, j, k;
            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 9; j++)
                    l_1187[i][j] = &l_800[1];
            }
            for (i = 0; i < 10; i++)
                l_1192[i] = (-3L);
lbl_1077:
            for (l_801 = (-4); (l_801 != (-5)); --l_801)
            { /* block id: 417 */
                int32_t **l_949 = &g_912;
                int16_t *l_952 = (void*)0;
                int8_t *l_963 = &g_100;
                int32_t **l_964 = &l_805;
                const uint16_t *l_969 = &g_101;
                const uint16_t **l_968[1][8];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 8; j++)
                        l_968[i][j] = &l_969;
                }
                (*l_964) = ((*l_949) = func_45(((*l_949) = &l_920), (((safe_sub_func_int16_t_s_s(((p_43 | (l_952 == l_953)) > p_43), ((((((((*g_680) ^ (((*l_963) = (safe_add_func_uint64_t_u_u(g_722, (((safe_div_func_uint16_t_u_u(((safe_add_func_uint32_t_u_u((safe_mod_func_uint32_t_u_u((*g_844), ((p_44 || (*g_908)) , (*g_844)))), 1L)) != 0xECB5238311A9C50ALL), p_44)) , l_962) == (void*)0)))) && (**g_679))) > 0xCE67AB099477D1FCLL) | 1UL) >= l_923[1]) != p_44) ^ p_43) <= p_44))) & l_917) , (void*)0), p_44, l_919, p_42));
                for (g_73 = 0; (g_73 <= 1); g_73 += 1)
                { /* block id: 424 */
                    uint16_t **l_967[1];
                    int32_t l_981 = (-9L);
                    int i;
                    for (i = 0; i < 1; i++)
                        l_967[i] = &g_303;
                    for (g_119 = 0; (g_119 <= 1); g_119 += 1)
                    { /* block id: 427 */
                        if ((*g_912))
                            break;
                        return (*p_42);
                    }
                    for (g_173 = 0; (g_173 <= 1); g_173 += 1)
                    { /* block id: 433 */
                        int64_t l_980 = 0L;
                        g_133 ^= (safe_lshift_func_uint16_t_u_s((((((l_967[0] != l_968[0][6]) & ((p_44 , (safe_lshift_func_uint16_t_u_s((safe_div_func_uint16_t_u_u(((*g_844) != 1L), (((safe_mul_func_uint16_t_u_u((((((safe_mod_func_int64_t_s_s((**l_964), p_43)) ^ 0x67092CD020D68D22LL) , ((safe_mod_func_uint8_t_u_u((0x6D84L & p_43), p_43)) & 0xEFDDL)) , l_980) != g_114), 65535UL)) & p_43) ^ 0x07CCDC19L))), (*g_894)))) ^ (***g_612))) , l_981) , l_800[1]) != (*g_706)), p_44));
                    }
                }
            }
            if (((((g_696 & ((**g_613) != (l_918 != ((*g_844) = 0x0B182542L)))) >= (safe_div_func_uint64_t_u_u((((safe_sub_func_uint64_t_u_u(0x24ABA7635AB4EB1DLL, 2L)) , l_986) == (void*)0), 0x28558482CCECEE1CLL))) >= 3L) && p_43))
            { /* block id: 439 */
                return (*p_42);
            }
            else
            { /* block id: 441 */
                int32_t **l_1012 = (void*)0;
                int32_t l_1016[10][1];
                int32_t ***l_1050 = &g_107;
                int i, j;
                for (i = 0; i < 10; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_1016[i][j] = 0x7BA66B68L;
                }
lbl_1018:
                if ((safe_rshift_func_int16_t_s_u(((((*g_844) = l_917) > (l_923[3] = (((*l_940) = ((safe_add_func_int32_t_s_s(1L, (g_133 ^= ((((**l_986) = (**l_986)) == (void*)0) & ((18446744073709551609UL > (((g_722++) == ((p_44 , (g_114 = (*g_912))) == (safe_div_func_uint32_t_u_u((l_995 == (void*)0), 0xF2D7E3BBL)))) < 0x03L)) || g_617))))) , g_98)) & l_937))) | 0x1AL), p_44)))
                { /* block id: 449 */
                    uint8_t l_996 = 0x51L;
                    uint8_t *****l_1014[9][2][6] = {{{&l_792,&l_792,&l_792,&l_792,(void*)0,&l_792},{&l_792,&l_792,(void*)0,&l_792,&l_792,&l_793}},{{&l_792,&l_792,&l_793,&l_793,&l_792,&l_792},{&l_792,&l_792,(void*)0,&l_792,(void*)0,&l_792}},{{(void*)0,&l_792,&l_793,(void*)0,(void*)0,&l_793},{(void*)0,(void*)0,(void*)0,&l_792,&l_793,&l_792}},{{&l_792,(void*)0,&l_792,&l_793,(void*)0,(void*)0},{&l_792,&l_792,&l_792,&l_792,(void*)0,&l_792}},{{&l_792,&l_792,(void*)0,&l_792,&l_792,&l_793},{&l_792,&l_792,&l_793,&l_793,&l_792,&l_792}},{{&l_792,&l_792,(void*)0,&l_792,(void*)0,&l_792},{(void*)0,&l_792,&l_793,(void*)0,(void*)0,&l_793}},{{(void*)0,(void*)0,(void*)0,&l_792,&l_793,&l_792},{&l_792,(void*)0,&l_792,&l_793,(void*)0,(void*)0}},{{&l_792,&l_792,&l_792,&l_792,(void*)0,&l_792},{&l_792,&l_792,(void*)0,&l_792,&l_792,&l_793}},{{&l_792,&l_792,&l_793,&l_793,&l_792,&l_792},{&l_792,&l_792,(void*)0,&l_793,&l_793,&l_792}}};
                    int i, j, k;
                    ++l_996;
                    if (g_326)
                        goto lbl_1018;
                    g_114 = (l_1016[1][0] = ((g_1015 = ((((p_44 > ((safe_lshift_func_uint8_t_u_s(((4UL >= ((((((safe_mul_func_uint16_t_u_u(4UL, ((((*l_940) = l_996) && (!(safe_add_func_uint8_t_u_u(((safe_add_func_int32_t_s_s(((g_637 ^= ((!(safe_sub_func_uint16_t_u_u(p_44, (0xBA5CC3E2L | (*g_844))))) != (0xE9F9L > ((*l_793) != &g_613)))) , l_923[1]), l_923[4])) , (*g_348)), 0x5FL)))) , p_44))) == (-10L)) > l_1011) ^ (*g_908)) , l_1012) != (void*)0)) && g_1013), 7)) , p_43)) != (*p_42)) , 8L) , &g_612)) == &l_791));
                }
                else
                { /* block id: 456 */
                    return l_1017;
                }
                for (g_181 = 0; (g_181 != 18); g_181++)
                { /* block id: 462 */
                    int16_t l_1022 = 1L;
                    int32_t l_1027[2][3][2] = {{{0xA852FA19L,0x0EE488B3L},{0x0EE488B3L,0xA852FA19L},{0x0EE488B3L,0x0EE488B3L}},{{0xA852FA19L,0x0EE488B3L},{0x0EE488B3L,0xA852FA19L},{0x0EE488B3L,0x0EE488B3L}}};
                    int64_t ****l_1037[5] = {&l_986,&l_986,&l_986,&l_986,&l_986};
                    int32_t ***l_1049 = &g_107;
                    int i, j, k;
                    for (l_937 = 0; (l_937 <= 9); l_937 += 1)
                    { /* block id: 465 */
                        uint16_t l_1023 = 65529UL;
                        int32_t l_1026 = 0x242366CBL;
                        int32_t l_1029 = (-9L);
                        ++l_1023;
                        g_1030++;
                    }
                    l_1021 = (l_1051 ^= (((((****g_1015) <= (((safe_lshift_func_uint8_t_u_u(p_43, (g_1013 &= ((l_1035 != (l_1038 = l_1036[1][9])) > (safe_lshift_func_int16_t_s_s(((((((((safe_div_func_uint16_t_u_u(((safe_sub_func_int16_t_s_s(1L, ((p_44 & (((!(p_43 ^ p_44)) | ((*g_908) == l_1048)) , 0x40EE8DDB75B18CC1LL)) | 0x1BL))) ^ 0xDBB7F3A3L), (-1L))) != g_98) , l_1049) != l_1050) , p_44) , (*g_894)) | l_923[1]) > (-1L)), 14)))))) , (void*)0) == &g_194)) & 18446744073709551610UL) <= p_43) < (*g_348)));
                    l_1069 = (safe_mul_func_int8_t_s_s((safe_add_func_uint16_t_u_u((~((((g_101 = ((+(safe_lshift_func_int8_t_s_s(l_937, 7))) , (l_1028[5][1][0] ^= ((safe_mul_func_int16_t_s_s((((safe_mul_func_int16_t_s_s(((*g_908) = 0x2666L), p_44)) & p_43) | (g_637 = p_43)), p_44)) > ((safe_mul_func_uint16_t_u_u((p_44 , p_44), (safe_mod_func_uint32_t_u_u((g_326 &= (*g_844)), l_1068[0][1][3])))) != p_44))))) && p_43) & (*g_894)) > l_916[1])), p_44)), p_43));
                    for (l_1021 = 5; (l_1021 >= 2); l_1021 -= 1)
                    { /* block id: 481 */
                        return (*g_912);
                    }
                }
                g_1070++;
                l_1016[5][0] = (*g_912);
            }
            for (g_551 = 0; (g_551 <= 21); ++g_551)
            { /* block id: 490 */
                int32_t **l_1095 = &l_806[2];
                int8_t *l_1096 = &l_1068[0][1][3];
                int32_t l_1125 = 0L;
                int32_t l_1126 = 0xA87339DCL;
                int32_t l_1198[7][3][3] = {{{(-1L),0L,0L},{0x76289FEEL,(-1L),0xCEB5E2A7L},{0x76289FEEL,0xCEB5E2A7L,0xB60190B7L}},{{(-1L),0x1C94189EL,1L},{0xB60190B7L,0L,0x6F7BCC1DL},{0L,0x1C94189EL,0x246DA4F0L}},{{0xE3067627L,0xCEB5E2A7L,0xE3067627L},{0x1C94189EL,(-1L),0xE3067627L},{0xC7DFD6A7L,0L,0x246DA4F0L}},{{0x246DA4F0L,0xE3067627L,0x6F7BCC1DL},{(-1L),1L,1L},{0x246DA4F0L,1L,0xB60190B7L}},{{0xC7DFD6A7L,0xC7DFD6A7L,0xCEB5E2A7L},{0x1C94189EL,0xC7DFD6A7L,0L},{0xE3067627L,1L,0xC7DFD6A7L}},{{0L,1L,0x3C13D321L},{0xB60190B7L,0xE3067627L,0xC7DFD6A7L},{(-1L),0L,0L}},{{0x76289FEEL,(-1L),0xCEB5E2A7L},{0x76289FEEL,0xCEB5E2A7L,0xB60190B7L},{(-1L),0x1C94189EL,1L}}};
                int32_t *l_1212 = &l_1192[5];
                uint8_t *****l_1221 = &l_792;
                uint16_t l_1235[3][5][7] = {{{0x3D78L,1UL,0UL,0xCFACL,0x375AL,65527UL,65527UL},{0x6058L,1UL,0UL,1UL,0x6058L,65527UL,0x9109L},{1UL,0xBFBEL,0x3D78L,0UL,0xD765L,0x1F71L,0UL},{1UL,0xE157L,0xBB0DL,0xD54DL,0x83FDL,0xBFBEL,0x6372L},{1UL,0UL,65531UL,0UL,1UL,0xCFACL,0UL}},{{0x6058L,0x8020L,0xCFACL,0xBB0DL,0xBB0DL,0xCFACL,0x8020L},{0x3D78L,65527UL,0UL,0x8089L,0x9769L,0xBFBEL,0xBB0DL},{65527UL,0x8089L,0UL,0x3D78L,1UL,0x1F71L,0x6058L},{0xD54DL,0UL,1UL,0x8089L,0x1F71L,65527UL,0x9769L},{65535UL,0x6058L,1UL,0xBB0DL,2UL,65527UL,2UL}},{{0UL,0x6058L,0x6058L,0UL,0xE157L,0x9109L,0x83FDL},{0x1F71L,0UL,0x8020L,0xD54DL,0x3D78L,0UL,65531UL},{65531UL,0x8089L,0xE157L,0UL,65535UL,0x6372L,0x83FDL},{0UL,65527UL,65527UL,1UL,0UL,0UL,2UL},{0x9769L,0x8020L,0UL,0xCFACL,0UL,0x8020L,0x9769L}}};
                const int8_t l_1246[8][8][2] = {{{0L,1L},{4L,0xDDL},{1L,0xE6L},{0xE6L,(-3L)},{(-1L),1L},{(-8L),(-1L)},{1L,0x31L},{0x05L,1L}},{{(-8L),1L},{1L,(-3L)},{0xDDL,0xFFL},{1L,0xFFL},{0xDDL,(-3L)},{1L,1L},{(-8L),1L},{0x05L,0x31L}},{{1L,(-1L)},{(-8L),1L},{(-1L),(-3L)},{0xE6L,0xE6L},{1L,0xDDL},{0xFFL,(-3L)},{1L,0x05L},{(-8L),1L}},{{1L,0x31L},{1L,1L},{(-8L),0x05L},{1L,(-3L)},{0xFFL,0xDDL},{1L,0xE6L},{0xE6L,(-3L)},{(-1L),1L}},{{(-8L),(-1L)},{1L,0x31L},{0x05L,1L},{(-8L),1L},{1L,(-3L)},{0xDDL,0xFFL},{1L,0xFFL},{0xDDL,(-3L)}},{{1L,1L},{(-8L),1L},{0x05L,0x31L},{1L,(-1L)},{(-8L),1L},{(-1L),(-3L)},{0xE6L,0xE6L},{1L,0xDDL}},{{0xFFL,(-3L)},{1L,0x05L},{(-8L),1L},{1L,0x31L},{1L,1L},{(-8L),0x05L},{1L,(-3L)},{0xFFL,0xDDL}},{{1L,0xE6L},{0xE6L,(-3L)},{(-1L),1L},{(-8L),(-1L)},{1L,0x31L},{0x05L,1L},{(-8L),1L},{1L,(-3L)}}};
                int i, j, k;
                for (g_73 = 0; (g_73 > 7); g_73++)
                { /* block id: 493 */
                    uint8_t ****l_1085 = &g_612;
                    uint8_t *****l_1087 = &l_1086;
                    int32_t *l_1091 = &g_4[3][5];
                    int32_t **l_1092[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    if (g_194)
                        goto lbl_1077;
                    for (g_119 = 0; (g_119 == 39); ++g_119)
                    { /* block id: 497 */
                        uint32_t l_1082 = 4294967291UL;
                        int32_t *l_1083 = &g_34[1][6][1];
                        int32_t **l_1084[3][4][9] = {{{(void*)0,(void*)0,&l_805,&g_912,&l_805,&l_806[2],&l_805,&l_806[2],&l_806[2]},{(void*)0,&l_805,&l_805,(void*)0,&l_806[4],&l_806[2],&g_912,&l_806[2],&l_806[2]},{&l_806[2],&l_805,&l_806[0],&g_912,(void*)0,&g_912,&g_912,&l_1083,&l_806[2]},{&g_912,(void*)0,&l_806[0],(void*)0,&l_806[4],&l_805,&l_805,&l_1083,&l_1083}},{{&l_806[2],&g_912,&l_805,(void*)0,&l_805,&g_912,&l_806[2],&l_806[2],&l_1083},{(void*)0,(void*)0,&l_805,&g_912,&l_805,&l_806[2],&l_805,&l_806[2],&l_806[2]},{(void*)0,&l_805,&l_805,(void*)0,&l_806[4],&l_806[2],&g_912,&l_805,&l_806[2]},{&l_806[2],&g_912,&l_806[2],&l_806[2],(void*)0,&l_806[2],&l_806[2],&g_912,&l_805}},{{&l_806[2],&l_806[3],&l_806[2],(void*)0,&l_806[1],&g_912,&g_912,&g_912,&g_912},{&l_806[2],&l_806[2],&l_805,(void*)0,&l_805,&l_806[2],&l_806[2],&l_805,&g_912},{&l_1083,&l_806[3],&g_912,&l_806[2],&l_805,&l_1083,&g_912,&l_806[2],&l_805},{&l_1083,&g_912,&l_805,&l_1083,&l_806[1],&l_1083,&l_806[2],&l_805,&l_806[2]}}};
                        int i, j, k;
                        g_114 = (safe_lshift_func_int16_t_s_s((*g_908), p_43));
                        if ((*p_42))
                            break;
                        if (l_1082)
                            continue;
                        p_42 = l_1083;
                    }
                    l_1021 = (l_1085 != ((*l_1087) = l_1086));
                    l_1093 = ((((l_995 == (void*)0) || (g_1088[8] | 0xFFCE22AD7B024056LL)) , (g_1089 == &g_1090)) , (g_912 = func_45(p_42, l_1091, p_43, (*p_42), l_1091)));
                }
                if ((((+(((*l_1095) = p_42) == p_42)) >= (p_44 , ((((((((*l_1096) &= (*g_680)) != ((((l_923[2] |= 0UL) > 0xA8F8L) , ((*g_844) = ((safe_sub_func_uint8_t_u_u((***g_612), (safe_rshift_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((*l_1093), 65526UL)), 9)))) , (*l_1093)))) != p_44)) && 255UL) != g_133) >= 0UL) || (**g_679)) > 0x5A625B6EL))) , (*p_42)))
                { /* block id: 512 */
                    int32_t ***l_1118 = &l_1095;
                    uint64_t *l_1121 = &l_1048;
                    if ((*g_912))
                        break;
                    ++g_1103;
                    g_133 &= (l_1126 = (safe_div_func_uint8_t_u_u(((safe_div_func_uint32_t_u_u(((~(((safe_sub_func_uint32_t_u_u(p_43, (*g_912))) > (safe_add_func_uint16_t_u_u(((((0xB33125FC72D6062FLL > ((safe_add_func_uint64_t_u_u((((g_1103 , l_1117) != &g_908) , ((l_1125 = (l_1118 == (((safe_div_func_uint64_t_u_u((--(*l_1121)), l_1124[0])) <= p_43) , &g_107))) >= (**l_1095))), g_549[4][0])) , 0x6CD09677829E0A11LL)) <= (**g_679)) & p_44) != 1UL), 0xFED9L))) >= 6UL)) & 7UL), 0x006408D1L)) < 6UL), 5UL)));
                }
                else
                { /* block id: 519 */
                    const int64_t *l_1138[10] = {&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183,&g_183};
                    const int64_t **l_1137 = &l_1138[7];
                    const int32_t l_1149[4][2] = {{0x7BCAD190L,0x7BCAD190L},{0x7BCAD190L,0x7BCAD190L},{0x7BCAD190L,0x7BCAD190L},{0x7BCAD190L,0x7BCAD190L}};
                    uint64_t *l_1150 = &l_1048;
                    uint32_t l_1169 = 0xB7BABB57L;
                    uint8_t *l_1172 = &g_119;
                    int32_t l_1186 = 0xECF88AF0L;
                    int32_t **l_1209 = (void*)0;
                    int i, j;
                    if ((safe_rshift_func_int16_t_s_s(((**l_1117) &= (p_44 || ((safe_add_func_uint16_t_u_u(1UL, ((++(*g_844)) , (((((((safe_mul_func_uint16_t_u_u(p_43, (safe_lshift_func_uint16_t_u_s(p_44, ((void*)0 == l_1137))))) <= (safe_sub_func_int64_t_s_s((**l_1095), ((*l_1150) = ((((safe_div_func_int64_t_s_s((safe_div_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(0x31A9C5620C49C247LL, (safe_sub_func_int32_t_s_s((l_1149[1][0] , (*p_42)), l_1149[0][0])))), p_43)), p_44)) >= (*l_1093)) != 0xEC63L) <= 18446744073709551611UL))))) , (**l_1095)) & g_807) ^ (**l_1095)) != (*g_844)) | p_43)))) <= 1L))), (**l_1095))))
                    { /* block id: 523 */
                        uint64_t l_1151 = 18446744073709551614UL;
                        int32_t l_1168[9][1];
                        int32_t *l_1170 = &l_917;
                        uint64_t l_1171 = 0UL;
                        int i, j;
                        for (i = 0; i < 9; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1168[i][j] = 8L;
                        }
                        g_912 = func_51(((*l_1150) = (p_44 > (l_1151 , (safe_lshift_func_uint8_t_u_u(0xE9L, (safe_add_func_uint32_t_u_u(0xED3BADC0L, (safe_add_func_int32_t_s_s((-8L), (+0xC15A019DL)))))))))), func_45(((*l_1095) = func_45(func_45(func_51(((((((((safe_sub_func_int8_t_s_s(((safe_div_func_int8_t_s_s(((((p_44 == (safe_mul_func_int8_t_s_s(((safe_mul_func_int16_t_s_s(((((((!(l_1168[3][0] = p_44)) >= 4294967290UL) , (*l_1093)) || l_1169) ^ (*g_894)) != 0x17L), p_43)) & p_43), 0xDBL))) & g_617) & g_743) , p_43), (*g_348))) || (**l_1095)), (****g_1015))) && (**l_1095)) ^ 3UL) ^ 0xAA9C4359L) | p_43) >= p_43) , &p_43) == &g_549[2][0]), &l_804[4][8][0], l_1170), p_42, g_454[1][7][0], l_1171, p_42), p_42, p_43, (*l_1093), p_42)), p_42, p_44, (*l_1093), p_42), p_42);
                        p_42 = ((*l_1095) = func_45(p_42, func_45(p_42, (p_43 , &g_4[2][8]), p_44, (g_98 = ((p_44 , l_1172) != ((***l_793) = (**g_612)))), &l_920), p_44, (*g_912), p_42));
                        (*l_1095) = &l_801;
                    }
                    else
                    { /* block id: 533 */
                        if ((*p_42))
                            break;
                        (*l_1095) = p_42;
                        l_1186 = (((*l_1096) = 0x98L) > ((l_1173 == (***g_1015)) >= (safe_lshift_func_uint8_t_u_s((safe_mul_func_uint16_t_u_u((0x78048C0FL == (*g_844)), ((*g_908) = ((((p_43 , (safe_mul_func_uint16_t_u_u(l_1149[1][0], ((**l_1095) == (((((((safe_div_func_int32_t_s_s((safe_div_func_int16_t_s_s(((safe_mul_func_int8_t_s_s((((p_43 != 1UL) , 0xA8AE3E548BA28484LL) , (-3L)), 0xF9L)) , p_43), 0xCCCCL)), (*p_42))) | 0x3EE0L) == p_44) , g_696) , 0x45542AD3L) >= (*g_912)) || p_43))))) >= (*g_348)) != (**l_1095)) || p_44)))), 0))));
                        p_42 = func_45(&l_804[5][8][0], &l_1186, (((((253UL ^ (****g_1015)) || (**l_1095)) >= (((((((*l_1096) = ((*g_912) , (0x5F1CL > (l_1187[6][6] != ((safe_lshift_func_int8_t_s_u((safe_mod_func_uint64_t_u_u(((***g_1090) >= 0xCFA37671CD817279LL), l_1192[5])), 3)) , l_1187[6][6]))))) , l_1193) != (void*)0) && (*g_912)) ^ l_1194) & l_1186)) , p_43) ^ (*g_894)), (*p_42), &l_937);
                    }
                    ++g_1199;
                    for (g_173 = 0; (g_173 != (-20)); --g_173)
                    { /* block id: 545 */
                        int32_t **l_1206 = &l_945;
                        int32_t l_1229 = 0x1126CDDFL;
                        g_133 ^= ((p_44 | 6UL) & (safe_mod_func_int8_t_s_s(l_1186, ((((*l_1206) = &g_549[4][0]) == ((((((*g_894) &= (safe_mul_func_int8_t_s_s(l_1169, ((void*)0 != l_1209)))) , (l_1186 > (((safe_rshift_func_uint8_t_u_s((((**g_679) < 0x9FL) && p_43), 6)) || 0xE030535CL) > 255UL))) | g_34[1][6][1]) & 1UL) , l_1212)) , 248UL))));
                    }
                    l_923[3] &= ((void*)0 == &p_43);
                }
                if ((*p_42))
                    continue;
                l_1028[5][0][0] = ((***g_612) , (safe_add_func_int64_t_s_s((safe_div_func_int16_t_s_s((safe_unary_minus_func_uint32_t_u((l_1235[0][4][1] < ((safe_mod_func_int64_t_s_s(((*l_1093) >= (safe_rshift_func_int8_t_s_u(p_43, 3))), p_44)) , (safe_sub_func_uint16_t_u_u((**l_1095), (safe_add_func_int16_t_s_s((p_43 & (g_267 >= ((safe_lshift_func_uint16_t_u_u(((0UL >= 0xC4C8AD3BL) && (**l_1095)), (**l_1095))) & l_1246[5][5][1]))), 0xDA29L)))))))), p_43)), (***g_1090))));
            }
            l_1247 = (((*g_680) >= (((*g_1089) = (((***g_612) ^ (*l_1093)) , l_1036[1][9])) != (void*)0)) < (*l_1093));
        }
        else
        { /* block id: 561 */
            int32_t * const *l_1266 = &l_945;
            int32_t l_1267 = (-1L);
            int32_t l_1268[3][7] = {{0x1D6740D4L,1L,0x1D6740D4L,1L,0x1D6740D4L,1L,0x1D6740D4L},{0xF29D02BCL,0xF29D02BCL,0L,0L,0xF29D02BCL,0xF29D02BCL,0L},{0x520E6DACL,1L,0x520E6DACL,1L,0x520E6DACL,1L,0x520E6DACL}};
            int32_t l_1269[1];
            int32_t l_1270[8];
            int i, j;
            for (i = 0; i < 1; i++)
                l_1269[i] = 0x24397330L;
            for (i = 0; i < 8; i++)
                l_1270[i] = (-1L);
            if (g_1070)
                goto lbl_1248;
            if ((safe_add_func_int16_t_s_s(((safe_sub_func_int8_t_s_s((safe_add_func_int32_t_s_s((safe_unary_minus_func_uint32_t_u(p_43)), (safe_lshift_func_uint8_t_u_s(((((g_623 = 0xC8E9L) & (l_1269[0] = (l_1268[1][1] = (safe_div_func_uint64_t_u_u((safe_sub_func_uint8_t_u_u((safe_sub_func_int16_t_s_s(p_43, ((p_43 , (((l_1267 = (safe_add_func_uint64_t_u_u(p_43, (((void*)0 == l_1266) > (((void*)0 == (**l_791)) <= p_44))))) == 0xB95CL) ^ p_44)) == (*p_42)))), (*g_680))), 18446744073709551615UL))))) == (*l_1093)) && g_486[0]), 1)))), l_1270[1])) || 255UL), 7UL)))
            { /* block id: 567 */
                return l_1271;
            }
            else
            { /* block id: 569 */
                int16_t l_1272 = (-4L);
                (*g_912) = (-2L);
                ++g_1274;
            }
        }
    }
    return (*p_42);
}


/* ------------------------------------------ */
/* 
 * reads : g_642 g_637 g_34 g_98
 * writes: g_637 g_623 g_98
 */
static int32_t * func_45(int32_t * p_46, int32_t * p_47, uint64_t  p_48, int32_t  p_49, int32_t * p_50)
{ /* block id: 349 */
    uint32_t l_747 = 0xD0AC7146L;
    uint16_t *l_748 = (void*)0;
    uint16_t *l_749[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    uint64_t *l_752 = &g_617;
    int32_t *l_753 = &g_98;
    int i;
    l_747 = (-8L);
    (*l_753) |= (g_642 < (((0xEAA7L > (g_623 = (&p_48 != ((++g_637) , l_752)))) < 0x36L) || g_34[1][6][1]));
    return &g_34[4][5][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_173 g_4 g_114 g_98 g_432 g_326 g_63 g_454 g_133 g_34 g_100 g_550 g_612 g_348 g_617 g_623 g_194 g_637 g_549 g_663 g_410 g_642 g_679 g_613 g_119 g_696 g_705 g_722 g_743
 * writes: g_73 g_114 g_98 g_432 g_326 g_101 g_275 g_133 g_119 g_617 g_623 g_194 g_637 g_663 g_430 g_100 g_550 g_722 g_743
 */
static int32_t * func_51(uint64_t  p_52, int32_t * p_53, int32_t * p_54)
{ /* block id: 76 */
    int32_t l_216 = 0xB88609A6L;
    int8_t l_234 = 8L;
    int32_t l_280[8][9][3] = {{{8L,0xFD188BAEL,0L},{0xDF44F759L,0x0C6BC6A4L,(-6L)},{(-1L),0x4CF9F92AL,0L},{0x3B964475L,(-7L),1L},{0L,0x4CF9F92AL,0xD9593F4FL},{0L,0x0C6BC6A4L,0x8E7C1B22L},{0xFD188BAEL,0xFD188BAEL,0x4CF9F92AL},{1L,0x4E70C977L,(-9L)},{(-2L),0xA80F4721L,0x007509DAL}},{{(-9L),(-1L),0L},{0x22A489C8L,(-2L),0x007509DAL},{(-3L),(-2L),(-9L)},{0x4CF9F92AL,0xF51D0D96L,0x4CF9F92AL},{(-1L),0L,0x8E7C1B22L},{0L,0L,0xD9593F4FL},{0L,0L,1L},{0x2DC099E8L,0xD9593F4FL,0L},{0L,0x9ED5F9CCL,(-6L)}},{{0L,0L,0L},{(-1L),1L,0x4E70C977L},{0x4CF9F92AL,0x2DC099E8L,0x92A305A8L},{(-3L),0xDF44F759L,(-1L)},{0x22A489C8L,0L,0xFD188BAEL},{(-9L),0xDF44F759L,(-1L)},{(-2L),0x2DC099E8L,0L},{1L,1L,0xDF44F759L},{0xFD188BAEL,0L,0L}},{{0L,0x9ED5F9CCL,1L},{0L,0xD9593F4FL,(-2L)},{0x3B964475L,0L,1L},{(-1L),0L,0L},{0xDF44F759L,0L,0xDF44F759L},{8L,0xD9593F4FL,0x4CF9F92AL},{0xDF44F759L,(-7L),(-3L)},{0xD9593F4FL,0x2DC099E8L,0x22A489C8L},{0x4E70C977L,(-3L),(-9L)}},{{0xD9593F4FL,0x92A305A8L,(-2L)},{0xDF44F759L,1L,1L},{0xA80F4721L,0x22A489C8L,0xFD188BAEL},{(-1L),0x4E70C977L,0L},{0L,8L,0L},{0x697F749FL,1L,0x3B964475L},{0L,8L,(-1L)},{0x0C6BC6A4L,0x4E70C977L,0xDF44F759L},{0x22A489C8L,0x22A489C8L,8L}},{{0x3B964475L,1L,0x8E7C1B22L},{0x2DC099E8L,0x92A305A8L,0xF51D0D96L},{0x8E7C1B22L,(-3L),0x0C6BC6A4L},{0xED61A26EL,0x2DC099E8L,0xF51D0D96L},{(-1L),(-7L),0x8E7C1B22L},{8L,0xD9593F4FL,8L},{0x9ED5F9CCL,(-1L),0xDF44F759L},{0x4CF9F92AL,0L,(-1L)},{(-1L),0x0C6BC6A4L,0x3B964475L}},{{0L,(-1L),0L},{(-1L),(-6L),0L},{0x4CF9F92AL,0xFD188BAEL,0xFD188BAEL},{0x9ED5F9CCL,0x3B964475L,1L},{8L,0L,(-2L)},{(-1L),(-1L),(-9L)},{0xED61A26EL,0x007509DAL,0x22A489C8L},{0x8E7C1B22L,(-1L),(-3L)},{0x2DC099E8L,0L,0x4CF9F92AL}},{{0x3B964475L,0x3B964475L,(-1L)},{0x22A489C8L,0xFD188BAEL,0L},{0x0C6BC6A4L,(-6L),0L},{0L,(-1L),0x2DC099E8L},{0x697F749FL,0x0C6BC6A4L,0L},{0L,0L,0L},{(-1L),(-1L),(-1L)},{0xA80F4721L,0xD9593F4FL,0x4CF9F92AL},{0xDF44F759L,(-7L),(-3L)}}};
    uint8_t *l_292 = (void*)0;
    uint16_t *l_299 = &g_101;
    int64_t l_314[7][3] = {{1L,1L,(-4L)},{1L,0xE8EA873BF1479107LL,0xE8EA873BF1479107LL},{(-4L),0x1BA9FA5B34151BE3LL,(-7L)},{1L,5L,1L},{1L,(-4L),(-7L)},{5L,5L,0xE8EA873BF1479107LL},{0x2C4D35416227AC34LL,(-4L),(-4L)}};
    uint16_t l_413 = 0UL;
    uint64_t *l_427[2];
    uint8_t l_574 = 0x52L;
    uint16_t **l_582 = &g_303;
    uint8_t l_607[1];
    uint8_t **l_611 = &g_348;
    uint8_t ***l_610[8][5][6] = {{{&l_611,&l_611,(void*)0,&l_611,&l_611,(void*)0},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611,(void*)0},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{(void*)0,&l_611,&l_611,&l_611,&l_611,&l_611}},{{&l_611,(void*)0,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,(void*)0,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611,(void*)0},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611,(void*)0},{&l_611,&l_611,&l_611,(void*)0,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,(void*)0,&l_611},{&l_611,&l_611,(void*)0,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,(void*)0},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,(void*)0},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611}},{{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,(void*)0,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,(void*)0,(void*)0,&l_611},{&l_611,(void*)0,&l_611,(void*)0,&l_611,&l_611}},{{(void*)0,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611},{&l_611,&l_611,&l_611,(void*)0,&l_611,&l_611},{&l_611,&l_611,&l_611,&l_611,&l_611,&l_611}}};
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_427[i] = &g_275[9];
    for (i = 0; i < 1; i++)
        l_607[i] = 253UL;
    for (g_73 = 0; (g_73 > 21); ++g_73)
    { /* block id: 79 */
        int32_t *l_201 = &g_114;
        int32_t **l_202 = &l_201;
        uint32_t l_209[4][9][6] = {{{4294967295UL,1UL,0x778B0BB3L,0x778B0BB3L,0xB52015B9L,1UL},{7UL,0x586E15FCL,0x17D50ABDL,0x9FE9B06FL,0x778B0BB3L,0x43CAFA4DL},{1UL,4294967294UL,1UL,4294967295UL,0xB164AEADL,9UL},{1UL,9UL,4294967295UL,0x9FE9B06FL,4294967295UL,0xB164AEADL},{7UL,0x778B0BB3L,1UL,0x778B0BB3L,7UL,1UL},{1UL,0x17D50ABDL,0UL,4294967294UL,4294967293UL,5UL},{1UL,1UL,0x5AA5DF87L,0x17D50ABDL,4294967295UL,5UL},{0x586E15FCL,4294967295UL,0UL,0x43CAFA4DL,1UL,1UL},{4294967295UL,1UL,1UL,4294967295UL,0x586E15FCL,0xB164AEADL}},{{0x43CAFA4DL,0UL,4294967295UL,0x586E15FCL,0x9FE9B06FL,9UL},{0x17D50ABDL,0x5AA5DF87L,1UL,1UL,0x9FE9B06FL,0x43CAFA4DL},{4294967294UL,0UL,0x17D50ABDL,1UL,0x586E15FCL,1UL},{0x778B0BB3L,1UL,0x778B0BB3L,7UL,1UL,0x9FE9B06FL},{0x9FE9B06FL,4294967295UL,9UL,1UL,4294967295UL,0UL},{4294967295UL,1UL,4294967294UL,1UL,4294967293UL,7UL},{0x9FE9B06FL,0x17D50ABDL,0x586E15FCL,7UL,7UL,0x586E15FCL},{0x778B0BB3L,0x778B0BB3L,0xB52015B9L,1UL,4294967295UL,4294967295UL},{4294967294UL,9UL,4294967295UL,1UL,0xB164AEADL,0xB52015B9L}},{{0x17D50ABDL,4294967294UL,4294967295UL,0x586E15FCL,0x778B0BB3L,4294967295UL},{0x43CAFA4DL,0x586E15FCL,0xB52015B9L,4294967295UL,0xB52015B9L,0x586E15FCL},{4294967295UL,0xB52015B9L,0x586E15FCL,0x43CAFA4DL,0x35D945D5L,7UL},{0x586E15FCL,4294967295UL,4294967294UL,0x17D50ABDL,1UL,0UL},{1UL,4294967295UL,9UL,4294967294UL,0x35D945D5L,0x9FE9B06FL},{1UL,0xB52015B9L,0x778B0BB3L,0x778B0BB3L,0xB52015B9L,1UL},{7UL,0x586E15FCL,0x17D50ABDL,0x9FE9B06FL,0x778B0BB3L,0x43CAFA4DL},{1UL,4294967294UL,1UL,4294967295UL,0xB164AEADL,9UL},{1UL,9UL,4294967295UL,0x9FE9B06FL,4294967295UL,0xB164AEADL}},{{7UL,0x778B0BB3L,1UL,0x778B0BB3L,7UL,1UL},{1UL,0x17D50ABDL,0UL,4294967294UL,4294967293UL,5UL},{1UL,1UL,0x5AA5DF87L,0x17D50ABDL,4294967295UL,5UL},{0x586E15FCL,4294967295UL,0UL,0x43CAFA4DL,1UL,1UL},{4294967295UL,1UL,1UL,4294967295UL,0x586E15FCL,0xB164AEADL},{9UL,1UL,0UL,1UL,0x17D50ABDL,4294967294UL},{7UL,1UL,0xB52015B9L,4294967294UL,0x17D50ABDL,9UL},{0x9FE9B06FL,1UL,7UL,0x778B0BB3L,1UL,0x778B0BB3L},{0x35D945D5L,4294967294UL,0x35D945D5L,4294967295UL,0xB52015B9L,0x17D50ABDL}}};
        const int8_t l_212 = 0xC3L;
        int8_t *l_231 = &g_73;
        const uint8_t l_269[3] = {0x7CL,0x7CL,0x7CL};
        int32_t l_273 = 0L;
        int16_t l_278 = 0L;
        int32_t l_307 = 1L;
        int32_t l_324 = 0x26967131L;
        int32_t l_325 = 0xD55AAA7FL;
        uint16_t **l_371 = &l_299;
        int32_t l_431 = 1L;
        int8_t l_453 = 0x47L;
        int64_t *l_673 = (void*)0;
        int64_t *l_674 = &g_430[1][7];
        int32_t *l_697 = &g_4[4][4];
        int64_t ** const *l_708 = &g_663[0];
        int i, j, k;
        (*l_202) = l_201;
        if ((safe_add_func_int64_t_s_s(((((void*)0 != &g_173) & ((-1L) && (g_173 | (safe_mod_func_int8_t_s_s(((safe_sub_func_int16_t_s_s(((((*p_54) ^ l_209[3][4][1]) , (safe_lshift_func_uint16_t_u_s(l_212, 11))) | g_73), (((safe_unary_minus_func_int8_t_s((safe_div_func_int32_t_s_s(((**l_202) ^= l_216), p_52)))) | (-7L)) > 254UL))) & p_52), p_52))))) , p_52), p_52)))
        { /* block id: 82 */
            const int64_t *l_219 = (void*)0;
            int32_t l_228 = (-3L);
            int32_t l_277 = 0x332EF712L;
            int32_t **l_297 = &l_201;
            uint16_t *l_300[10][9] = {{&g_101,&g_101,&g_267,&g_101,&g_267,(void*)0,&g_101,&g_101,(void*)0},{&g_101,&g_267,(void*)0,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_267,&g_267,&g_267,&g_101,&g_267,&g_101,&g_101,&g_101},{&g_101,&g_101,&g_101,&g_101,&g_101,&g_267,&g_101,&g_101,(void*)0},{&g_101,&g_101,&g_267,&g_101,&g_101,&g_267,&g_101,&g_101,&g_101},{&g_267,&g_101,&g_267,&g_101,&g_101,&g_101,&g_101,&g_267,(void*)0},{&g_267,&g_101,&g_101,&g_267,&g_101,(void*)0,&g_101,&g_267,&g_101},{&g_101,&g_101,&g_267,&g_101,&g_267,(void*)0,&g_101,&g_101,(void*)0},{&g_101,&g_267,(void*)0,&g_101,&g_101,&g_101,&g_101,&g_101,&g_101},{&g_101,&g_267,&g_267,&g_267,&g_101,&g_267,&g_101,&g_101,&g_101}};
            uint16_t *l_306 = &g_101;
            int64_t *l_366 = &l_314[4][0];
            const int32_t *l_416[6][8][5] = {{{&g_410,&g_410,&g_410,&l_273,&g_410},{&g_410,&g_410,&l_273,&l_273,&g_410},{&g_410,&g_410,&g_410,&g_410,&l_273},{&l_273,&g_410,&g_410,&g_410,&l_273},{&g_410,&g_410,&g_410,&l_273,&g_410},{&l_273,&g_410,&g_410,&l_273,&l_273},{&g_410,&l_273,&g_410,&g_410,&g_410},{&g_410,&l_273,&g_410,&l_273,&l_273}},{{&g_410,&g_410,&l_273,&g_410,&l_273},{&g_410,&g_410,&g_410,&l_273,&g_410},{&g_410,&g_410,&l_273,&l_273,&g_410},{&g_410,&g_410,&g_410,&g_410,&l_273},{&l_273,&g_410,&g_410,&g_410,&l_273},{&g_410,&g_410,&g_410,&l_273,&g_410},{&l_273,&g_410,&g_410,&l_273,&g_410},{&l_273,&g_410,&g_410,&g_410,&g_410}},{{&l_273,&g_410,&l_273,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&l_273},{&l_273,&l_273,&g_410,&g_410,&l_273},{&l_273,&g_410,&l_273,&l_273,&g_410},{&g_410,&l_273,&g_410,&l_273,&g_410},{&l_273,&g_410,&g_410,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&g_410}},{{&l_273,&g_410,&g_410,&g_410,&g_410},{&l_273,&g_410,&l_273,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&l_273},{&l_273,&l_273,&g_410,&g_410,&l_273},{&l_273,&g_410,&l_273,&l_273,&g_410},{&g_410,&l_273,&g_410,&l_273,&g_410},{&l_273,&g_410,&g_410,&g_410,&g_410}},{{&g_410,&g_410,&g_410,&g_410,&g_410},{&l_273,&g_410,&g_410,&g_410,&g_410},{&l_273,&g_410,&l_273,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&l_273},{&l_273,&l_273,&g_410,&g_410,&l_273},{&l_273,&g_410,&l_273,&l_273,&g_410},{&g_410,&l_273,&g_410,&l_273,&g_410}},{{&l_273,&g_410,&g_410,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&g_410},{&l_273,&g_410,&g_410,&g_410,&g_410},{&l_273,&g_410,&l_273,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&g_410},{&g_410,&g_410,&g_410,&g_410,&l_273},{&l_273,&l_273,&g_410,&g_410,&l_273},{&l_273,&g_410,&l_273,&l_273,&g_410}}};
            uint16_t **l_426 = &l_306;
            int i, j, k;
            for (g_98 = 0; (g_98 <= 3); g_98 += 1)
            { /* block id: 85 */
                int8_t *l_230[2];
                int8_t **l_232 = &l_231;
                uint32_t *l_233[3];
                int32_t l_235[2];
                int32_t **l_298 = (void*)0;
                int32_t *l_316 = &l_280[0][4][1];
                int64_t *l_344 = (void*)0;
                int64_t l_368 = 0L;
                const int16_t l_369 = 0x755BL;
                uint16_t **l_370 = &l_300[5][6];
                uint64_t l_383 = 18446744073709551613UL;
                int32_t ***l_403 = &l_297;
                int i;
                for (i = 0; i < 2; i++)
                    l_230[i] = &g_73;
                for (i = 0; i < 3; i++)
                    l_233[i] = &g_179;
                for (i = 0; i < 2; i++)
                    l_235[i] = 0x30DACF97L;
            }
            if ((*p_54))
                break;
        }
        else
        { /* block id: 184 */
            (*l_202) = p_54;
        }
        if ((4294967294UL | ((l_280[6][5][2] |= (**l_202)) <= 1UL)))
        { /* block id: 188 */
            int32_t *l_429[9][4];
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 4; j++)
                    l_429[i][j] = &l_216;
            }
            ++g_432;
        }
        else
        { /* block id: 190 */
            int8_t l_435 = 0L;
            int16_t * const l_440 = (void*)0;
            int32_t l_450 = (-5L);
            int64_t *l_459 = &g_430[2][4];
            uint64_t **l_474 = &l_427[0];
            int32_t l_479 = (-7L);
            int32_t l_481 = 0xD06BB92DL;
            int32_t l_483 = (-1L);
            int32_t l_484 = (-4L);
            int32_t l_485[4];
            int16_t l_502[4][4] = {{(-1L),(-1L),0L,(-1L)},{(-1L),(-9L),(-9L),(-1L)},{(-9L),(-1L),(-9L),(-9L)},{(-1L),(-1L),0L,(-1L)}};
            int64_t l_548 = 0xC847EC321BDA1F4FLL;
            uint32_t *l_554[7][6] = {{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]},{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]},{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]},{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]},{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]},{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]},{&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1],&l_209[3][4][1]}};
            int32_t * const *l_575 = (void*)0;
            int i, j;
            for (i = 0; i < 4; i++)
                l_485[i] = 0x5A360165L;
            for (g_432 = 0; (g_432 <= 2); g_432 += 1)
            { /* block id: 193 */
                const int8_t l_442 = (-1L);
                int32_t *l_443 = &g_98;
                uint16_t ***l_449 = &l_371;
                uint8_t **l_476[2];
                int32_t l_480[9] = {1L,(-1L),(-1L),1L,(-1L),(-1L),1L,(-1L),(-1L)};
                uint16_t l_499 = 0x1F71L;
                uint32_t *l_501 = &g_194;
                int i;
                for (i = 0; i < 2; i++)
                    l_476[i] = &l_292;
                (*l_443) ^= (l_435 | (safe_rshift_func_uint8_t_u_s((p_52 , (safe_div_func_int64_t_s_s(((l_440 == l_299) < p_52), (0x841E4F36C541260ELL & ((!(l_442 | 0xDE98L)) , ((void*)0 != &l_413)))))), 0)));
                (*l_202) = (*l_202);
            }
            if (((((((((l_485[2] , (g_326++)) , (safe_add_func_uint16_t_u_u((safe_mod_func_uint64_t_u_u((((l_280[2][4][1] = 247UL) < (safe_sub_func_uint8_t_u_u(((safe_sub_func_uint16_t_u_u(65535UL, (p_52 & ((&l_278 != ((safe_rshift_func_int16_t_s_u(((((safe_sub_func_int8_t_s_s(((void*)0 != &l_427[0]), (~(g_63 , ((((safe_sub_func_int8_t_s_s(((((*l_299) = ((((safe_lshift_func_int16_t_s_s(0x9E68L, l_502[1][0])) > g_454[1][7][0]) , (**l_202)) > l_574)) , l_314[5][1]) || (*l_201)), 0x72L)) & (*l_201)) > l_314[5][1]) <= 0x02L))))) > l_485[2]) | g_133) ^ 0xDEL), 7)) , (void*)0)) < 1UL)))) && 0x6295L), 0xF5L))) , p_52), l_479)), 65527UL))) , (void*)0) != l_575) && 65531UL) , p_52) <= p_52) , l_280[0][4][1]))
            { /* block id: 252 */
                int16_t *l_588 = &l_278;
                int16_t **l_587 = &l_588;
                uint32_t l_589 = 0xFA9CAE84L;
                int32_t l_622 = 0x45B7448BL;
                if (((safe_lshift_func_uint16_t_u_s(((safe_div_func_uint64_t_u_u(((safe_mod_func_uint32_t_u_u(0xB203A109L, g_34[1][6][1])) , (((l_582 = (void*)0) != &g_303) <= (((p_52 ^ ((safe_add_func_int8_t_s_s((((void*)0 == p_53) >= (safe_div_func_uint64_t_u_u(((**l_474) = ((((((*l_587) = (l_314[5][1] , &g_550[0])) != &l_502[2][0]) || g_100) , g_114) != g_550[0])), p_52))), 0x46L)) != p_52)) >= p_52) != 1UL))), 0x85B25380C8B93140LL)) != 0L), 7)) >= l_589))
                { /* block id: 256 */
                    const uint64_t l_609 = 0xD508D49B6499A6A2LL;
                    if ((safe_rshift_func_uint16_t_u_s(65534UL, 0)))
                    { /* block id: 257 */
                        int64_t l_608 = 0xA29CA75C89A46040LL;
                        int32_t l_614 = 7L;
                        int32_t *l_615 = &l_325;
                        int32_t *l_616[8];
                        int i;
                        for (i = 0; i < 8; i++)
                            l_616[i] = &g_98;
                        p_53 = (void*)0;
                        g_133 &= (p_52 > (+p_52));
                        l_614 |= ((safe_add_func_int8_t_s_s(l_450, 0x85L)) == (safe_rshift_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u(((**l_611) = (l_484 & ((((0xEE5861C3DED09E91LL || (0x98E1DB35AF4FE7F8LL > (((p_52 < ((safe_sub_func_uint8_t_u_u((((safe_div_func_uint16_t_u_u(((safe_sub_func_uint16_t_u_u(7UL, 0x4630L)) || 0L), (-1L))) | 4294967295UL) ^ l_607[0]), l_608)) >= l_609)) || (*p_54)) && p_52))) , l_610[1][1][1]) != g_612) == 0x56C2743D2316A527LL))), 0xB6L)) != 0xB7FD15C1L), p_52)));
                        ++g_617;
                    }
                    else
                    { /* block id: 263 */
                        int32_t *l_620 = &g_133;
                        int32_t *l_621[2][3];
                        int i, j;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 3; j++)
                                l_621[i][j] = &g_114;
                        }
                        ++g_623;
                        (*l_620) = (-8L);
                    }
                }
                else
                { /* block id: 267 */
                    int32_t *l_626 = &l_484;
                    return &g_114;
                }
                (*l_202) = p_54;
            }
            else
            { /* block id: 271 */
                int8_t l_634[1];
                int32_t l_635 = 1L;
                const int32_t *l_643 = &l_481;
                int64_t **l_660 = &l_459;
                int i;
                for (i = 0; i < 1; i++)
                    l_634[i] = (-1L);
                for (l_483 = (-16); (l_483 >= (-5)); ++l_483)
                { /* block id: 274 */
                    (*l_202) = &l_450;
                }
                for (g_194 = 0; (g_194 <= 0); g_194 += 1)
                { /* block id: 279 */
                    int32_t *l_629 = &l_216;
                    int32_t *l_630 = &l_485[1];
                    int32_t *l_631 = &l_280[0][4][1];
                    int32_t *l_632 = &l_325;
                    int32_t *l_633[5][1][6] = {{{&l_484,&g_34[2][5][4],&l_216,&g_34[2][5][4],&l_484,&l_484}},{{&g_114,&g_34[2][5][4],&g_34[2][5][4],&g_114,&l_325,&g_114}},{{&g_114,&l_325,&g_114,&g_34[2][5][4],&g_34[2][5][4],&g_114}},{{&l_484,&l_484,&g_34[2][5][4],&l_216,&g_34[2][5][4],&l_484}},{{&g_34[2][5][4],&l_325,&l_216,&l_216,&l_325,&g_34[2][5][4]}}};
                    int i, j, k;
                    g_98 = g_550[g_194];
                    (*l_629) = ((0xD9L ^ 0UL) && p_52);
                    ++g_637;
                    for (l_453 = 0; (l_453 <= 0); l_453 += 1)
                    { /* block id: 285 */
                        const int32_t *l_641[3];
                        const int32_t **l_640 = &l_641[1];
                        int64_t **l_661 = &l_459;
                        int32_t l_662[3];
                        int64_t ***l_665 = &g_663[3];
                        int i, j, k;
                        for (i = 0; i < 3; i++)
                            l_641[i] = &g_642;
                        for (i = 0; i < 3; i++)
                            l_662[i] = (-6L);
                        l_643 = ((*l_640) = p_53);
                        l_662[1] = ((safe_div_func_uint8_t_u_u(7UL, (((safe_div_func_int64_t_s_s((safe_mul_func_uint8_t_u_u((l_209[(l_453 + 3)][(l_453 + 5)][(g_194 + 2)] || l_209[(g_194 + 3)][(g_194 + 1)][(g_194 + 1)]), (-7L))), p_52)) == (safe_lshift_func_uint8_t_u_s((((-9L) & (((safe_rshift_func_uint16_t_u_s((safe_add_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(((safe_lshift_func_int16_t_s_s((((((p_52 , l_660) != ((*l_201) , l_661)) , p_52) != g_549[3][0]) != p_52), 15)) > 4L), 0xD5L)), p_52)), 14)) ^ l_502[1][0]) && l_548)) , 246UL), (*l_630)))) && (*l_201)))) <= p_52);
                        if ((**l_202))
                            continue;
                        (*l_665) = g_663[3];
                    }
                }
                return &g_34[1][6][1];
            }
            return p_54;
        }
        if (((safe_rshift_func_int8_t_s_u(((*p_53) == ((**l_202) , (1UL != (safe_unary_minus_func_int64_t_s(((*l_674) = (~(safe_unary_minus_func_int8_t_s((1L && ((**l_202) , (g_410 & p_52)))))))))))), 1)) == (safe_lshift_func_int16_t_s_u(g_642, (safe_add_func_int64_t_s_s(((((g_679 != &g_680) >= g_4[0][5]) < (-1L)) ^ 0x64L), p_52))))))
        { /* block id: 298 */
            uint16_t l_681 = 5UL;
            int32_t *l_682 = &l_280[0][4][1];
            l_682 = (l_681 , &g_34[1][6][1]);
            return p_53;
        }
        else
        { /* block id: 301 */
            uint32_t l_700[5][7][4] = {{{4294967294UL,6UL,0x1794A50CL,0x81312DFDL},{0x1794A50CL,0x81312DFDL,4294967295UL,0x79E16248L},{0xEE45F4BDL,0x35B2D357L,0xC0C9EAFDL,4294967294UL},{0x43901657L,1UL,3UL,0xA12135EAL},{0xB50A5BB2L,0x8BB91633L,0xA12135EAL,0x35B2D357L},{0x1D9DF6D4L,0xDD4A49ABL,4294967295UL,1UL},{0x00899FFCL,0xEE45F4BDL,6UL,0x2A8176FFL}},{{0x15DAAC7AL,0x00899FFCL,0x47C47FABL,1UL},{4294967287UL,0x831BE22DL,0x55E80177L,0xB50A5BB2L},{0x7E8A093FL,4UL,1UL,0x2995FA70L},{7UL,6UL,6UL,7UL},{5UL,0x47C47FABL,4294967294UL,1UL},{1UL,6UL,0x463D9F51L,0xEE45F4BDL},{0x8D1FAE48L,0x2995FA70L,0x7E8A093FL,0xEE45F4BDL}},{{1UL,6UL,0xDD4A49ABL,1UL},{0xA12135EAL,0x47C47FABL,4294967294UL,7UL},{0x4E7D89DBL,6UL,2UL,0x2995FA70L},{0x15503E63L,4UL,0x9D580449L,0xB50A5BB2L},{9UL,0x831BE22DL,0x9014DEB2L,1UL},{6UL,0x00899FFCL,0x45676220L,0x2A8176FFL},{0x9014DEB2L,0xEE45F4BDL,0UL,1UL}},{{0xECFE81E3L,0xDD4A49ABL,1UL,0x35B2D357L},{0x35B2D357L,0x8BB91633L,4294967295UL,0xA12135EAL},{4294967295UL,1UL,8UL,4294967294UL},{8UL,0x35B2D357L,9UL,0x79E16248L},{0x2A8176FFL,0x81312DFDL,0x35B2D357L,0x81312DFDL},{0x55E80177L,6UL,7UL,4294967295UL},{0x8BB91633L,0xECFE81E3L,0x4E7D89DBL,8UL}},{{1UL,0x1794A50CL,1UL,1UL},{1UL,2UL,4294967294UL,6UL},{3UL,0x2A8176FFL,4294967293UL,4294967294UL},{6UL,0x47C47FABL,1UL,0x45676220L},{8UL,4294967288UL,0x4E7D89DBL,4294967295UL},{0UL,5UL,0x2995FA70L,0UL},{0x808D53B8L,0x35B2D357L,4294967287UL,0xB50A5BB2L}}};
            int32_t l_717 = 0x101422ACL;
            int32_t l_719 = (-9L);
            int32_t l_740 = 0x1944424BL;
            int32_t l_741 = 0x08721B57L;
            int i, j, k;
            for (l_574 = 0; (l_574 <= 9); l_574 += 1)
            { /* block id: 304 */
                uint64_t l_691 = 0x8F05B2E998C8F00ALL;
                int64_t *l_693 = &g_430[6][1];
                for (l_431 = 1; (l_431 <= 4); l_431 += 1)
                { /* block id: 307 */
                    int64_t *l_692[8] = {&g_430[1][7],&g_430[1][7],&g_430[6][7],&g_430[1][7],&g_430[1][7],&g_430[6][7],&g_430[1][7],&g_430[1][7]};
                    int32_t l_694 = 0x1ABBFE67L;
                    int32_t *l_695 = &l_324;
                    int i, j, k;
                    l_216 |= (safe_add_func_uint16_t_u_u(p_52, (safe_mod_func_int32_t_s_s(((*l_695) |= (safe_add_func_uint64_t_u_u(l_280[0][4][1], ((**g_613) <= (g_100 |= (0x2CB7L & (p_52 <= ((safe_mul_func_int16_t_s_s(p_52, (l_691 ^ g_623))) , (l_694 &= (l_692[0] == l_693)))))))))), g_696))));
                    (*l_695) = 1L;
                }
                for (g_133 = 9; (g_133 >= 3); g_133 -= 1)
                { /* block id: 316 */
                    if ((*p_54))
                        break;
                    return p_54;
                }
                for (g_617 = 1; (g_617 <= 7); g_617 += 1)
                { /* block id: 322 */
                    return l_697;
                }
            }
            if ((*p_53))
                continue;
            if ((safe_sub_func_uint64_t_u_u(p_52, 0x9931891D9A1DD73BLL)))
            { /* block id: 327 */
                ++l_700[3][5][3];
            }
            else
            { /* block id: 329 */
                int8_t l_720 = 1L;
                int32_t l_721 = 0x6F5655CBL;
                int32_t l_742[5] = {0L,0L,0L,0L,0L};
                int i;
                for (g_617 = 0; (g_617 <= 1); g_617 += 1)
                { /* block id: 332 */
                    int16_t *l_707 = &g_550[0];
                    int32_t *l_709 = &l_280[0][4][1];
                    int32_t l_716 = 0x556A0C08L;
                    int32_t l_718[9][1][2] = {{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}},{{(-8L),(-8L)}}};
                    int32_t *l_731 = &l_719;
                    int32_t *l_732 = &g_114;
                    int32_t *l_733 = (void*)0;
                    int32_t *l_734 = &l_307;
                    int32_t *l_735 = &g_133;
                    int32_t *l_736 = &l_280[7][2][0];
                    int32_t *l_737 = &l_716;
                    int32_t *l_738 = &l_721;
                    int32_t *l_739[1][3];
                    int i, j, k;
                    for (i = 0; i < 1; i++)
                    {
                        for (j = 0; j < 3; j++)
                            l_739[i][j] = &l_280[0][4][1];
                    }
                    if ((safe_mul_func_int16_t_s_s(((*l_707) &= ((void*)0 == g_705)), (l_708 == (void*)0))))
                    { /* block id: 334 */
                        return p_54;
                    }
                    else
                    { /* block id: 336 */
                        int32_t *l_712 = &g_133;
                        int32_t *l_713 = &l_307;
                        int32_t *l_714 = (void*)0;
                        int32_t *l_715[5] = {&l_307,&l_307,&l_307,&l_307,&l_307};
                        int i;
                        (*l_202) = ((safe_div_func_int8_t_s_s(p_52, ((*l_201) & l_280[6][3][1]))) , p_53);
                        ++g_722;
                    }
                    (*l_731) = ((*l_709) = (safe_sub_func_uint8_t_u_u((safe_sub_func_int64_t_s_s(p_52, ((void*)0 == &l_202))), (safe_rshift_func_int16_t_s_s(p_52, 3)))));
                    ++g_743;
                }
            }
            (*l_202) = (void*)0;
        }
    }
    return p_53;
}


/* ------------------------------------------ */
/* 
 * reads : g_34 g_114 g_101 g_100 g_4 g_119 g_177 g_179 g_181 g_98 g_194
 * writes: g_114 g_101 g_100 g_119 g_179 g_181 g_183 g_98 g_194
 */
static int32_t  func_65(uint8_t * p_66, int32_t  p_67, int32_t  p_68)
{ /* block id: 48 */
    uint32_t l_138 = 0x2EEB9F0DL;
    int32_t *l_139 = &g_114;
    int32_t *l_143[7];
    int32_t l_186[9] = {0x9E7E5C91L,0x9E7E5C91L,1L,0x9E7E5C91L,0x9E7E5C91L,1L,0x9E7E5C91L,0x9E7E5C91L,1L};
    int i;
    for (i = 0; i < 7; i++)
        l_143[i] = &g_4[2][8];
    (*l_139) ^= ((!(g_34[0][0][3] ^ 0x76L)) > l_138);
    for (g_101 = (-5); (g_101 != 15); g_101 = safe_add_func_uint64_t_u_u(g_101, 3))
    { /* block id: 52 */
        int16_t l_142[1][7] = {{0xBD8FL,0xBD8FL,0xBD8FL,0xBD8FL,0xBD8FL,0xBD8FL,0xBD8FL}};
        int32_t *l_144 = &g_133;
        int32_t l_185 = 0x51C85CD5L;
        int32_t l_187 = (-8L);
        int32_t l_190 = 0xAEE7E948L;
        int32_t l_191 = 6L;
        int32_t l_193 = 0xB20226B5L;
        int i, j;
        for (g_100 = 0; (g_100 >= 0); g_100 -= 1)
        { /* block id: 55 */
            int32_t **l_145 = &l_143[3];
            int32_t l_184 = 1L;
            int32_t l_188[3][8][4] = {{{1L,0x693F941DL,0xE7A130ACL,5L},{2L,4L,0x68258FFCL,5L},{(-7L),0x693F941DL,0x4BFEC47BL,0xEE84A3AFL},{0L,0L,9L,0xED3EF12DL},{0xAF0DCE50L,5L,0xAF0DCE50L,0xDF005D3DL},{(-4L),2L,1L,1L},{0L,0xE7A130ACL,1L,2L},{0x4BFEC47BL,0x68258FFCL,1L,(-7L)}},{{0L,0x4BFEC47BL,1L,0L},{(-4L),9L,0xAF0DCE50L,0xAF0DCE50L},{0xAF0DCE50L,0xAF0DCE50L,9L,(-4L)},{0L,1L,0x4BFEC47BL,0L},{(-7L),1L,0x68258FFCL,0x4BFEC47BL},{2L,1L,0xE7A130ACL,0L},{1L,1L,2L,(-4L)},{0xDF005D3DL,0xAF0DCE50L,5L,0xAF0DCE50L}},{{0xED3EF12DL,9L,0L,0L},{0xEE84A3AFL,0x4BFEC47BL,0x693F941DL,(-7L)},{5L,0x68258FFCL,4L,2L},{5L,0xE7A130ACL,0x693F941DL,1L},{0xEE84A3AFL,2L,0L,0xDF005D3DL},{0xED3EF12DL,5L,5L,0xED3EF12DL},{0xDF005D3DL,0L,2L,0xEE84A3AFL},{1L,0x693F941DL,0xE7A130ACL,5L}}};
            int i, j, k;
            (*l_145) = (l_144 = l_143[4]);
            for (p_68 = 0; (p_68 >= 0); p_68 -= 1)
            { /* block id: 60 */
                int16_t *l_172[9][10][2] = {{{&g_173,(void*)0},{(void*)0,&g_173},{&g_173,(void*)0},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{(void*)0,(void*)0},{&g_173,&g_173},{&g_173,(void*)0}},{{&g_173,&g_173},{&g_173,(void*)0},{(void*)0,&g_173},{(void*)0,(void*)0},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{&g_173,(void*)0}},{{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,(void*)0}},{{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,(void*)0},{(void*)0,&g_173}},{{(void*)0,(void*)0},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0}},{{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0}},{{&g_173,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,(void*)0},{(void*)0,&g_173},{(void*)0,(void*)0},{&g_173,&g_173},{(void*)0,&g_173}},{{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,&g_173}},{{(void*)0,&g_173},{&g_173,&g_173},{(void*)0,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{&g_173,(void*)0},{&g_173,&g_173},{&g_173,&g_173},{(void*)0,&g_173}}};
                int32_t l_174 = 0xAC62AC5CL;
                uint32_t *l_178 = &g_179;
                uint8_t *l_180 = &g_181;
                int64_t *l_182 = &g_183;
                int32_t l_189 = 0x5BDB795EL;
                int32_t l_192 = (-1L);
                int i, j, k;
                (*l_145) = &p_67;
                (*l_139) = l_142[p_68][g_100];
                g_98 |= ((*l_139) = (((p_67 | ((safe_mod_func_int64_t_s_s(((*l_182) = (safe_mod_func_uint8_t_u_u(((*l_180) &= (safe_lshift_func_uint8_t_u_u(((((*l_144) , ((safe_lshift_func_uint8_t_u_u(((*p_66) |= (safe_rshift_func_int16_t_s_s((l_142[p_68][(p_68 + 1)] = l_142[p_68][g_100]), 12))), 3)) & ((*l_178) ^= (safe_sub_func_uint8_t_u_u(246UL, (safe_add_func_int32_t_s_s(((safe_mod_func_uint32_t_u_u(((safe_lshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s((+(safe_mul_func_int8_t_s_s((safe_sub_func_uint64_t_u_u(((l_174 = (+0xCF1A2F3BL)) || p_67), (safe_sub_func_int32_t_s_s((0L <= (0x991EL < (((void*)0 == g_177) && (-6L)))), g_100)))), g_4[0][3]))), p_67)), 15)) && (-8L)), p_68)) || (*l_144)), p_68))))))) > (**l_145)) , (*p_66)), 7))), 0x23L))), (*l_144))) && 0x8B1C4E8FL)) | 1UL) == g_34[1][6][1]));
                g_194++;
            }
        }
    }
    return (*l_139);
}


/* ------------------------------------------ */
/* 
 * reads : g_73 g_34 g_26 g_4 g_100 g_101 g_107 g_98 g_114 g_133
 * writes: g_73 g_98 g_100 g_107 g_114 g_119 g_133
 */
static uint8_t * func_69(int8_t  p_70, int16_t  p_71)
{ /* block id: 26 */
    int64_t l_74 = 0x34BBF6D4BD0D1FC1LL;
    int8_t *l_77 = &g_73;
    uint32_t l_89 = 0UL;
    uint8_t *l_118 = &g_119;
    int32_t *l_132 = &g_133;
    int32_t **l_134 = &l_132;
    uint8_t *l_135 = &g_119;
    (*l_132) ^= ((l_74 , l_74) && ((safe_sub_func_int8_t_s_s(((*l_77) |= (-9L)), (g_34[1][6][1] == (1UL ^ ((((func_78((((((*l_118) = (p_70 == func_84(((((l_89 > (6L == ((9UL >= g_26[2]) , l_89))) , p_71) & p_71) , l_77), g_34[1][6][1], l_74, p_71))) & 0UL) | 9UL) || g_34[1][6][1]), &g_34[1][6][1], &g_26[3], &g_4[4][0], l_118) & 0x17L) && l_74) | l_89) , l_74))))) > l_74));
    (*l_134) = l_132;
    return l_135;
}


/* ------------------------------------------ */
/* 
 * reads : g_4 g_101 g_98 g_34 g_114
 * writes: g_114
 */
static int8_t  func_78(uint16_t  p_79, int32_t * p_80, int32_t * p_81, int32_t * p_82, int8_t * const  p_83)
{ /* block id: 41 */
    int32_t *l_130 = &g_114;
    int32_t l_131 = 0L;
    l_131 ^= ((*l_130) |= (safe_mul_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(g_4[0][6], 10)), ((p_79 & g_4[0][8]) > ((p_82 == (void*)0) , (safe_lshift_func_int8_t_s_u((&p_82 != (void*)0), ((safe_sub_func_int64_t_s_s((g_101 <= ((safe_rshift_func_int8_t_s_u((-9L), 5)) | g_98)), g_34[0][3][0])) & g_34[1][3][2]))))))));
    return g_98;
}


/* ------------------------------------------ */
/* 
 * reads : g_34 g_4 g_100 g_101 g_107 g_26
 * writes: g_98 g_100 g_107 g_114
 */
static uint16_t  func_84(const int8_t * p_85, uint32_t  p_86, uint64_t  p_87, const int32_t  p_88)
{ /* block id: 28 */
    int32_t *l_97[6];
    int8_t *l_99 = &g_100;
    uint32_t l_117 = 0xE3EA3A5BL;
    int i;
    for (i = 0; i < 6; i++)
        l_97[i] = &g_98;
    if ((((safe_unary_minus_func_uint16_t_u(g_34[2][3][1])) , (((g_4[0][8] < (((safe_lshift_func_int16_t_s_u((g_4[2][8] == ((*l_99) ^= (p_88 == (safe_rshift_func_uint16_t_u_s((safe_sub_func_uint32_t_u_u(0x6C9A4CFDL, (9L >= ((g_98 = g_4[3][4]) <= (g_34[1][6][1] || p_87))))), p_86))))), 13)) > 0x1BL) & p_86)) <= 0xFCB09B1BL) ^ g_101)) <= 0x99L))
    { /* block id: 31 */
        const uint32_t l_102 = 0xEC888E09L;
        int32_t **l_104 = (void*)0;
        int32_t ***l_103 = &l_104;
        int32_t **l_106 = &l_97[2];
        int32_t ***l_105[9][10][2] = {{{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{(void*)0,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106}},{{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0}},{{&l_106,&l_106},{&l_106,&l_106},{(void*)0,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{(void*)0,&l_106},{&l_106,(void*)0},{&l_106,&l_106}},{{(void*)0,(void*)0},{(void*)0,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{(void*)0,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{(void*)0,&l_106}},{{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{(void*)0,(void*)0},{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0}},{{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,(void*)0},{(void*)0,&l_106},{(void*)0,&l_106},{&l_106,&l_106}},{{(void*)0,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{(void*)0,&l_106},{&l_106,&l_106},{(void*)0,&l_106},{(void*)0,(void*)0},{&l_106,(void*)0},{&l_106,&l_106}},{{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0},{(void*)0,&l_106},{&l_106,(void*)0}},{{&l_106,&l_106},{&l_106,&l_106},{&l_106,(void*)0},{&l_106,&l_106},{&l_106,&l_106},{(void*)0,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106},{&l_106,&l_106}}};
        int i, j, k;
        g_107 = ((*l_103) = (l_102 , &l_97[2]));
    }
    else
    { /* block id: 34 */
        int32_t l_108 = 0L;
        int32_t **l_109[1][3];
        int32_t * const l_113 = &g_114;
        int32_t * const *l_112 = &l_113;
        uint16_t *l_115 = (void*)0;
        uint16_t *l_116[3];
        int i, j;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 3; j++)
                l_109[i][j] = &l_97[2];
        }
        for (i = 0; i < 3; i++)
            l_116[i] = (void*)0;
        g_98 = (((((l_108 , &l_97[2]) != l_109[0][0]) & ((safe_sub_func_uint16_t_u_u(((*l_113) = ((l_112 = g_107) == &l_97[1])), (g_4[2][8] <= (((2UL || (-1L)) || ((0xCC7AA637DEE9CF0CLL | p_86) & p_86)) == g_26[1])))) && g_101)) & 0xFFB87322L) , g_34[3][1][0]);
    }
    return l_117;
}




/* ---------------------------------------- */
//testcase_id 1484153250
int case1484153250(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_4[i][j], "g_4[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_26[i], "g_26[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 5; k++)
            {
                transparent_crc(g_34[i][j][k], "g_34[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_63, "g_63", print_hash_value);
    transparent_crc(g_73, "g_73", print_hash_value);
    transparent_crc(g_98, "g_98", print_hash_value);
    transparent_crc(g_100, "g_100", print_hash_value);
    transparent_crc(g_101, "g_101", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_133, "g_133", print_hash_value);
    transparent_crc(g_173, "g_173", print_hash_value);
    transparent_crc(g_179, "g_179", print_hash_value);
    transparent_crc(g_181, "g_181", print_hash_value);
    transparent_crc(g_183, "g_183", print_hash_value);
    transparent_crc(g_194, "g_194", print_hash_value);
    transparent_crc(g_267, "g_267", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_275[i], "g_275[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_326, "g_326", print_hash_value);
    transparent_crc(g_336, "g_336", print_hash_value);
    transparent_crc(g_410, "g_410", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_430[i][j], "g_430[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_432, "g_432", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_454[i][j][k], "g_454[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_486[i], "g_486[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_549[i][j], "g_549[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_550[i], "g_550[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_551, "g_551", print_hash_value);
    transparent_crc(g_617, "g_617", print_hash_value);
    transparent_crc(g_623, "g_623", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_636[i], "g_636[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_637, "g_637", print_hash_value);
    transparent_crc(g_642, "g_642", print_hash_value);
    transparent_crc(g_696, "g_696", print_hash_value);
    transparent_crc(g_722, "g_722", print_hash_value);
    transparent_crc(g_743, "g_743", print_hash_value);
    transparent_crc(g_807, "g_807", print_hash_value);
    transparent_crc(g_921, "g_921", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_922[i], "g_922[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_924, "g_924", print_hash_value);
    transparent_crc(g_1013, "g_1013", print_hash_value);
    transparent_crc(g_1030, "g_1030", print_hash_value);
    transparent_crc(g_1070, "g_1070", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_1088[i], "g_1088[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1103, "g_1103", print_hash_value);
    transparent_crc(g_1195, "g_1195", print_hash_value);
    transparent_crc(g_1196, "g_1196", print_hash_value);
    transparent_crc(g_1197, "g_1197", print_hash_value);
    transparent_crc(g_1199, "g_1199", print_hash_value);
    transparent_crc(g_1273, "g_1273", print_hash_value);
    transparent_crc(g_1274, "g_1274", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1277[i], "g_1277[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1439, "g_1439", print_hash_value);
    transparent_crc(g_1553, "g_1553", print_hash_value);
    transparent_crc(g_1621, "g_1621", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1743[i], "g_1743[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 456
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 52
breakdown:
   depth: 1, occurrence: 195
   depth: 2, occurrence: 46
   depth: 3, occurrence: 6
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 9, occurrence: 1
   depth: 11, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 17, occurrence: 3
   depth: 18, occurrence: 3
   depth: 19, occurrence: 2
   depth: 20, occurrence: 1
   depth: 22, occurrence: 3
   depth: 23, occurrence: 2
   depth: 24, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 27, occurrence: 2
   depth: 28, occurrence: 3
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 1
   depth: 36, occurrence: 1
   depth: 40, occurrence: 1
   depth: 47, occurrence: 1
   depth: 52, occurrence: 1

XXX total number of pointers: 385

XXX times a variable address is taken: 879
XXX times a pointer is dereferenced on RHS: 310
breakdown:
   depth: 1, occurrence: 220
   depth: 2, occurrence: 64
   depth: 3, occurrence: 18
   depth: 4, occurrence: 8
XXX times a pointer is dereferenced on LHS: 218
breakdown:
   depth: 1, occurrence: 184
   depth: 2, occurrence: 21
   depth: 3, occurrence: 9
   depth: 4, occurrence: 4
XXX times a pointer is compared with null: 32
XXX times a pointer is compared with address of another variable: 8
XXX times a pointer is compared with another pointer: 13
XXX times a pointer is qualified to be dereferenced: 4521

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1542
   level: 2, occurrence: 408
   level: 3, occurrence: 151
   level: 4, occurrence: 43
   level: 5, occurrence: 3
XXX number of pointers point to pointers: 146
XXX number of pointers point to scalars: 239
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 26.8
XXX average alias set size: 1.51

XXX times a non-volatile is read: 1792
XXX times a non-volatile is write: 759
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 1
XXX backward jumps: 4

XXX stmts: 198
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 23
   depth: 2, occurrence: 34
   depth: 3, occurrence: 36
   depth: 4, occurrence: 40
   depth: 5, occurrence: 32

XXX percentage a fresh-made variable is used: 17.3
XXX percentage an existing variable is used: 82.7
********************* end of statistics **********************/

