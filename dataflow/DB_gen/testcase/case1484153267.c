/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      73732517
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static const uint32_t g_5 = 4294967294UL;
static uint64_t g_23[1] = {0xEF906D8BC0D133F2LL};
static int32_t g_38 = 0xAD015B01L;
static uint64_t g_59 = 0UL;
static int32_t g_64 = (-1L);
static int8_t g_66 = 0x75L;
static int32_t g_71 = 0x149F7FAEL;
static uint64_t g_80 = 6UL;
static uint64_t g_103 = 4UL;
static uint64_t *g_102[10][5] = {{&g_103,&g_103,&g_59,&g_59,(void*)0},{&g_59,&g_103,&g_103,&g_59,&g_103},{&g_59,&g_59,&g_103,&g_59,&g_103},{&g_103,&g_59,&g_59,&g_59,&g_59},{&g_103,&g_59,&g_103,&g_103,&g_103},{&g_103,&g_59,&g_103,&g_59,&g_103},{&g_59,&g_59,&g_103,&g_59,&g_59},{&g_59,&g_103,&g_103,&g_103,&g_59},{&g_103,&g_103,&g_59,&g_103,&g_103},{&g_59,&g_103,&g_59,&g_103,(void*)0}};
static uint16_t g_115 = 65535UL;
static int32_t *g_127 = &g_71;
static uint32_t g_140 = 0UL;
static int16_t g_153 = 3L;
static uint8_t g_166 = 0UL;
static int8_t g_178 = 0x9AL;
static uint32_t g_208[10][7][3] = {{{0x302F6452L,0x06E73B9EL,18446744073709551615UL},{1UL,0xCEF9BD45L,1UL},{0UL,0x83C10CCCL,0x06E73B9EL},{1UL,0x028EE803L,0xD7E8CB10L},{4UL,1UL,4UL},{18446744073709551615UL,0UL,0x01D1818CL},{0x9CDA5918L,0x18173DE0L,0x83C10CCCL}},{{0x5A9A4678L,1UL,18446744073709551615UL},{0x06E73B9EL,0x7DBE0CD2L,0x30922173L},{0x5A9A4678L,18446744073709551614UL,1UL},{0x9CDA5918L,0x5051AD09L,9UL},{18446744073709551615UL,0xCEF9BD45L,0x5825647FL},{4UL,18446744073709551611UL,18446744073709551611UL},{1UL,0xAD2CF816L,0x952B729EL}},{{0UL,0xADBD7DA9L,4UL},{1UL,1UL,18446744073709551608UL},{0x302F6452L,0x18173DE0L,0x5051AD09L},{18446744073709551608UL,1UL,0UL},{0x06E73B9EL,0xADBD7DA9L,8UL},{18446744073709551615UL,0xAD2CF816L,1UL},{0x18173DE0L,18446744073709551611UL,0x7D319A83L}},{{0xCEF9BD45L,0xCEF9BD45L,1UL},{0x3784CA32L,0x5051AD09L,18446744073709551615UL},{1UL,18446744073709551614UL,18446744073709551607UL},{18446744073709551606UL,0x7DBE0CD2L,4UL},{0x00C1A7E2L,1UL,18446744073709551607UL},{0xD96D95FFL,0x18173DE0L,18446744073709551615UL},{18446744073709551614UL,0UL,1UL}},{{0x06E73B9EL,1UL,0x7D319A83L},{1UL,0x028EE803L,1UL},{0UL,0x83C10CCCL,8UL},{1UL,0xCEF9BD45L,0UL},{0x67E9A630L,0x06E73B9EL,0x5051AD09L},{1UL,18446744073709551610UL,18446744073709551608UL},{0x67E9A630L,0x5A8641CAL,4UL}},{{1UL,18446744073709551615UL,0x952B729EL},{0UL,0x18173DE0L,18446744073709551611UL},{1UL,0x5825647FL,0x5825647FL},{0x06E73B9EL,18446744073709551615UL,9UL},{18446744073709551614UL,18446744073709551612UL,1UL},{0xD96D95FFL,18446744073709551615UL,0x30922173L},{0x00C1A7E2L,0xCEF9BD45L,18446744073709551615UL}},{{18446744073709551606UL,18446744073709551615UL,0x83C10CCCL},{1UL,18446744073709551612UL,0x01D1818CL},{0x3784CA32L,18446744073709551615UL,4UL},{0xCEF9BD45L,0x5825647FL,0xD7E8CB10L},{0x18173DE0L,0x18173DE0L,0x06E73B9EL},{18446744073709551615UL,18446744073709551615UL,1UL},{0x06E73B9EL,0x5A8641CAL,18446744073709551615UL}},{{18446744073709551608UL,18446744073709551610UL,1UL},{0x302F6452L,0x06E73B9EL,18446744073709551615UL},{1UL,0xCEF9BD45L,1UL},{0UL,0x83C10CCCL,0x06E73B9EL},{1UL,0x028EE803L,0xD7E8CB10L},{4UL,1UL,4UL},{18446744073709551615UL,0UL,0x01D1818CL}},{{0x9CDA5918L,0x18173DE0L,0x83C10CCCL},{0x5A9A4678L,1UL,18446744073709551615UL},{0x06E73B9EL,0x7DBE0CD2L,0x30922173L},{0x5A9A4678L,18446744073709551614UL,1UL},{0x9CDA5918L,0x5051AD09L,9UL},{18446744073709551615UL,0xCEF9BD45L,0x5825647FL},{0x30922173L,4UL,4UL}},{{18446744073709551608UL,18446744073709551615UL,18446744073709551614UL},{18446744073709551615UL,0x9CDA5918L,0x30922173L},{18446744073709551615UL,0x5A9A4678L,0xAD2CF816L},{18446744073709551615UL,0x06E73B9EL,0UL},{0x952B729EL,0x5A9A4678L,18446744073709551615UL},{18446744073709551606UL,0x9CDA5918L,0x7DBE0CD2L},{0x01D1818CL,18446744073709551615UL,18446744073709551608UL}}};
static int64_t g_210 = 1L;
static uint16_t g_212[4][9] = {{0x70E8L,0x5402L,0x6C6DL,0x5402L,0x70E8L,0x70E8L,0x5402L,0x6C6DL,0x5402L},{65530UL,0x66F5L,0UL,0UL,0x66F5L,65530UL,0x66F5L,0UL,0UL},{0x70E8L,0x70E8L,0x5402L,0x6C6DL,0x5402L,0x70E8L,0x70E8L,0x5402L,0x6C6DL},{0x8992L,0x66F5L,0x8992L,65530UL,65530UL,0x8992L,0x66F5L,0x8992L,65530UL}};
static uint32_t g_235 = 0UL;
static int32_t g_237[4] = {0xEA080918L,0xEA080918L,0xEA080918L,0xEA080918L};
static int64_t g_327 = 0x7FB2059F015E4749LL;
static int32_t **g_396 = (void*)0;
static uint64_t *g_557 = (void*)0;
static uint64_t **g_556[9] = {&g_557,&g_557,&g_557,&g_557,&g_557,&g_557,&g_557,&g_557,&g_557};
static uint64_t ***g_555 = &g_556[1];
static int32_t g_571 = 0x8533DBDFL;
static uint8_t g_606 = 255UL;
static int16_t *g_703 = &g_153;
static int16_t **g_702 = &g_703;
static int16_t g_745 = 0L;
static int32_t g_783 = (-10L);
static uint32_t g_940 = 0x352D6F82L;
static uint32_t *g_944 = &g_208[2][2][2];
static uint32_t **g_943 = &g_944;
static uint8_t *g_986 = (void*)0;
static uint8_t **g_985 = &g_986;
static int8_t g_1012 = (-9L);
static int32_t g_1024 = 0xBAD21EC1L;
static const uint32_t g_1060 = 0x333A771FL;
static int32_t **g_1098 = &g_127;
static int32_t *g_1242 = &g_237[0];
static int8_t g_1268 = 0x29L;
static const int32_t g_1292 = 0x0601AD9FL;
static const int32_t *g_1293 = (void*)0;
static int32_t ** const *g_1346 = &g_396;
static int32_t ** const **g_1345[7] = {&g_1346,&g_1346,&g_1346,&g_1346,&g_1346,&g_1346,&g_1346};
static int32_t ** const ***g_1344 = &g_1345[2];
static int32_t *** const **g_1348 = (void*)0;
static uint16_t *g_1358 = &g_212[1][6];
static uint16_t **g_1357[10] = {&g_1358,&g_1358,&g_1358,&g_1358,&g_1358,&g_1358,&g_1358,&g_1358,&g_1358,&g_1358};
static const uint64_t *g_1399 = &g_103;
static const uint64_t **g_1398 = &g_1399;
static const uint64_t ***g_1397 = &g_1398;
static const uint64_t ****g_1396 = &g_1397;
static uint32_t g_1422 = 6UL;
static uint8_t g_1492 = 0x4CL;
static int8_t g_1604 = 0xB2L;
static int32_t g_1610 = 0xFDC97D57L;
static int32_t g_1614 = 0x720B1658L;
static int32_t g_1615 = 4L;
static int32_t g_1616 = (-10L);
static uint16_t g_1617 = 1UL;
static int16_t ***g_1638 = &g_702;
static uint32_t *g_1809 = (void*)0;
static uint32_t **g_1808[8] = {&g_1809,&g_1809,&g_1809,&g_1809,&g_1809,&g_1809,&g_1809,&g_1809};
static const uint32_t g_1931[10][9][2] = {{{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L}},{{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L}},{{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L}},{{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L}},{{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L}},{{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L}},{{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L}},{{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L}},{{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L}},{{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L},{0xBB8F67A3L,0x1A7E3A97L},{7UL,0x1A7E3A97L}}};
static const uint32_t *g_1930 = &g_1931[6][5][1];
static uint64_t ****g_1934 = &g_555;
static uint64_t *****g_1933 = &g_1934;
static const int32_t *g_1985 = &g_1024;
static const int32_t **g_1984 = &g_1985;
static int8_t *g_2031 = &g_1604;
static int8_t **g_2030 = &g_2031;
static int32_t g_2041 = 0x4237EF81L;
static const int16_t **g_2072 = (void*)0;
static uint32_t g_2082 = 1UL;
static const int32_t **g_2154[10] = {&g_1293,&g_1293,&g_1293,&g_1293,&g_1293,&g_1293,&g_1293,&g_1293,&g_1293,&g_1293};
static const int32_t **g_2155 = &g_1293;
static int16_t ****g_2198 = (void*)0;
static int16_t ****g_2199 = &g_1638;
static uint16_t g_2209 = 0x69BFL;
static int8_t g_2243 = (-1L);
static int32_t *****g_2276 = (void*)0;
static int64_t g_2310 = 1L;
static const uint64_t g_2393 = 0x2D785ABE16947F70LL;
static uint32_t g_2439 = 0xB0204598L;
static const uint32_t *g_2482 = &g_235;
static const uint32_t * const *g_2481 = &g_2482;
static const uint32_t * const **g_2480[5][7][1] = {{{&g_2481},{&g_2481},{(void*)0},{&g_2481},{(void*)0},{&g_2481},{&g_2481}},{{&g_2481},{(void*)0},{&g_2481},{(void*)0},{&g_2481},{&g_2481},{&g_2481}},{{(void*)0},{&g_2481},{(void*)0},{&g_2481},{&g_2481},{&g_2481},{(void*)0}},{{&g_2481},{(void*)0},{&g_2481},{&g_2481},{&g_2481},{(void*)0},{&g_2481}},{{(void*)0},{&g_2481},{&g_2481},{&g_2481},{(void*)0},{&g_2481},{(void*)0}}};
static const uint32_t * const ***g_2479 = &g_2480[4][3][0];
static const uint8_t *g_2498 = &g_1492;
static const uint8_t **g_2497 = &g_2498;
static const uint8_t ***g_2496 = &g_2497;
static uint8_t g_2513[1][2][4] = {{{1UL,1UL,1UL,1UL},{1UL,1UL,1UL,1UL}}};
static int32_t g_2536[5] = {0x27B1E6F4L,0x27B1E6F4L,0x27B1E6F4L,0x27B1E6F4L,0x27B1E6F4L};
static int16_t ** const * const g_2563 = (void*)0;
static int16_t ** const * const *g_2562 = &g_2563;
static int16_t ** const * const **g_2561 = &g_2562;
static uint8_t g_2700 = 0xA1L;
static int32_t g_2735 = 7L;
static uint64_t g_2806[9][4] = {{1UL,18446744073709551607UL,1UL,1UL},{18446744073709551607UL,18446744073709551607UL,0xEF783AC169D53364LL,18446744073709551607UL},{18446744073709551607UL,1UL,1UL,18446744073709551607UL},{1UL,18446744073709551607UL,1UL,1UL},{18446744073709551607UL,18446744073709551607UL,0xEF783AC169D53364LL,18446744073709551607UL},{18446744073709551607UL,1UL,1UL,18446744073709551607UL},{1UL,18446744073709551607UL,1UL,1UL},{18446744073709551607UL,18446744073709551607UL,0xEF783AC169D53364LL,18446744073709551607UL},{18446744073709551607UL,1UL,1UL,18446744073709551607UL}};
static uint32_t g_2884 = 4294967294UL;
static uint32_t g_2887 = 0xAE22E54BL;
static uint64_t g_2925 = 0xDB8EFC5A44C9385FLL;
static int16_t g_2952[5] = {0xA1C5L,0xA1C5L,0xA1C5L,0xA1C5L,0xA1C5L};
static int64_t *g_2956 = &g_2310;
static uint32_t g_3075[6][1] = {{0xA0887FCBL},{1UL},{0xA0887FCBL},{0xA0887FCBL},{0x0E297089L},{1UL}};
static int32_t *g_3114[2][9] = {{&g_38,&g_2041,&g_2041,&g_38,&g_237[0],&g_783,&g_38,&g_783,&g_237[0]},{&g_38,&g_2041,&g_2041,&g_38,&g_237[0],&g_783,&g_38,&g_783,&g_237[0]}};
static int16_t ****g_3189 = &g_1638;
static uint16_t g_3251[4][9] = {{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{0x4BAAL,1UL,0x4BAAL,1UL,0x4BAAL,1UL,0x4BAAL,1UL,0x4BAAL},{65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL,65535UL},{0x4BAAL,1UL,0x4BAAL,1UL,0x4BAAL,1UL,0x4BAAL,1UL,0x4BAAL}};
static uint32_t g_3270 = 0x3610CAE3L;
static int64_t g_3292 = (-5L);
static int64_t g_3302 = 0L;
static int32_t g_3311 = 0x9BA5E3D5L;


/* --- FORWARD DECLARATIONS --- */
static int8_t  func_1(void);
static int64_t  func_6(int16_t  p_7);
static uint64_t  func_26(const uint8_t  p_27, int64_t  p_28);
static uint32_t  func_30(int16_t  p_31, const uint64_t  p_32, uint64_t * p_33, uint64_t * p_34);
static int32_t  func_36(const uint16_t  p_37);
static int64_t  func_51(uint64_t * p_52, int64_t  p_53, uint8_t  p_54);
static uint64_t * func_55(uint64_t * p_56, uint64_t * p_57);
static uint16_t  func_74(uint64_t  p_75, const int32_t  p_76, uint32_t  p_77);
static uint16_t  func_83(uint64_t  p_84, int64_t  p_85, int8_t * p_86, int64_t  p_87, uint16_t  p_88);
static int16_t  func_104(const int32_t  p_105, int32_t  p_106);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_5 g_23 g_166 g_1060 g_703 g_153 g_1242 g_237 g_1933 g_1024 g_1098 g_127 g_71 g_1638 g_702 g_1617 g_1358 g_212 g_1984 g_64 g_1616 g_944 g_208 g_210 g_327 g_1930 g_140 g_2030 g_2031 g_2041 g_1012 g_571 g_1604 g_2072 g_2082 g_1398 g_1399 g_103 g_1397 g_1344 g_1345 g_1346 g_396 g_80 g_2310 g_1934 g_66 g_2199 g_2479 g_2480 g_2513 g_2700 g_2209 g_2243 g_606 g_2806 g_2496 g_2497 g_2498 g_1492 g_1396 g_985 g_986 g_2884 g_2887 g_1985 g_2925 g_1610 g_2952 g_555 g_1422 g_745 g_2155 g_2735 g_3075 g_178 g_2393 g_556 g_557 g_3114 g_783 g_38 g_3189 g_2956 g_3251 g_3270 g_2198 g_3292 g_1268 g_943 g_2481 g_2482
 * writes: g_23 g_166 g_80 g_1930 g_1933 g_1615 g_1614 g_1024 g_210 g_212 g_571 g_153 g_140 g_327 g_237 g_1604 g_115 g_71 g_1012 g_64 g_1617 g_66 g_2030 g_127 g_2700 g_2209 g_606 g_2806 g_2243 g_986 g_59 g_2884 g_2887 g_1616 g_555 g_2310 g_1268 g_2956 g_745 g_1293 g_2735 g_178 g_783 g_38 g_2041 g_2198 g_702 g_2925 g_1422 g_3251 g_3270 g_944
 */
static int8_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_2 = 1UL;
    uint16_t *l_2765 = &g_2209;
    int32_t l_2772 = 9L;
    int64_t l_2775 = (-1L);
    uint8_t *l_2776 = &g_606;
    int32_t l_2777 = 0xAA4BC75BL;
    uint32_t l_2778 = 0x8EE78878L;
    int32_t l_2795 = 0L;
    int32_t l_2796 = 5L;
    int32_t l_2797 = (-1L);
    int32_t l_2801 = 0x0EEF31F8L;
    int32_t l_2802 = 4L;
    int32_t l_2804 = 0x46C96662L;
    int32_t *l_2830 = &g_1610;
    int32_t **l_2829 = &l_2830;
    uint64_t ***l_2913 = &g_556[7];
    uint32_t ***l_2945 = &g_943;
    uint32_t ****l_2944[2];
    uint32_t *****l_2943 = &l_2944[0];
    int64_t *l_2958 = (void*)0;
    const int32_t l_3084 = 0L;
    uint32_t l_3086 = 4294967295UL;
    int32_t l_3087 = 5L;
    const uint32_t **l_3165 = (void*)0;
    const uint32_t ** const *l_3164 = &l_3165;
    uint64_t l_3213 = 18446744073709551607UL;
    uint16_t *l_3255 = &g_3251[2][1];
    uint64_t l_3313 = 0x64160BC7323C51B8LL;
    int16_t l_3319 = 0xDF09L;
    uint64_t l_3320 = 9UL;
    int16_t l_3343 = (-2L);
    int i;
    for (i = 0; i < 2; i++)
        l_2944[i] = &l_2945;
    if ((l_2 < (safe_sub_func_uint16_t_u_u(g_5, (func_6(g_5) >= (((l_2777 = (((*l_2765) ^= l_2) < (safe_sub_func_int16_t_s_s((l_2772 = (safe_mul_func_int8_t_s_s((safe_lshift_func_uint16_t_u_s(0x7EF6L, 6)), g_2243))), (0x3D8D18D8L || (safe_mul_func_uint8_t_u_u(((*l_2776) = (4UL == l_2775)), 255UL))))))) ^ l_2778) , g_2513[0][1][1]))))))
    { /* block id: 1405 */
        int32_t l_2787[10] = {0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL,0x026F69EAL};
        int32_t l_2792 = 0x7DC15F3BL;
        int32_t l_2794 = 0L;
        int32_t l_2800 = 0xDC010C32L;
        int32_t l_2805 = 0x6C737D32L;
        const uint32_t l_2809 = 0x40FBAD00L;
        int32_t l_2810 = (-1L);
        int8_t **l_2846 = (void*)0;
        int32_t l_2848 = 0x3B003181L;
        int8_t l_2881[6] = {0x59L,0x59L,0x59L,0x59L,0x59L,0x59L};
        int i;
        for (g_606 = (-10); (g_606 == 32); g_606++)
        { /* block id: 1408 */
            int8_t l_2788 = 0x57L;
            uint32_t *l_2791[2];
            int32_t l_2793 = (-7L);
            int32_t l_2798 = 1L;
            int32_t l_2799 = 0L;
            int32_t l_2803 = 0x1AB3BE6CL;
            int64_t *l_2817 = &g_327;
            uint8_t *l_2818[7] = {&g_1492,&g_1492,&g_1492,&g_1492,&g_1492,&g_1492,&g_1492};
            uint8_t ***l_2835 = &g_985;
            int8_t l_2904[3][5] = {{0xF3L,0x3DL,0xF3L,0x3DL,0xF3L},{0x56L,0x56L,0x56L,0x56L,0x56L},{0xF3L,0x3DL,0xF3L,0x3DL,0xF3L}};
            int i, j;
            for (i = 0; i < 2; i++)
                l_2791[i] = &l_2;
            if (((l_2805 = (safe_div_func_int8_t_s_s(((safe_sub_func_int16_t_s_s((**g_702), (safe_rshift_func_int16_t_s_s(l_2787[7], l_2788)))) | ((((safe_rshift_func_uint16_t_u_u(((g_2806[6][2]--) & (((((((-1L) != 0x6372CA7BL) || (l_2809 && (l_2810 != (((((safe_mul_func_uint8_t_u_u((***g_2496), ((safe_rshift_func_uint16_t_u_s(((*l_2765) = ((((*l_2817) = (((((l_2799 ^= (safe_mul_func_int16_t_s_s(l_2803, l_2787[7]))) < l_2798) || (*g_1358)) >= l_2803) | (-1L))) , l_2798) || (*g_2031))), 9)) || 1UL))) & (*g_1399)) <= (-1L)) || (-1L)) > 0x4BB1423CL)))) , (void*)0) != (void*)0) , l_2792) & g_5)), 2)) | 8L) < l_2775) ^ (*g_703))), l_2777))) != (**g_2030)))
            { /* block id: 1414 */
                uint32_t l_2822 = 0x64553C5EL;
                int32_t l_2836 = (-10L);
                int16_t l_2837 = 0L;
                uint8_t *l_2847 = &g_2700;
                int32_t l_2877 = 0xF38C9D63L;
                int32_t l_2878 = 0xD600AB03L;
                int32_t l_2879 = (-1L);
                int32_t l_2880 = 0xC175959BL;
                int32_t l_2882 = (-8L);
                int32_t l_2883 = 0xBF424B64L;
                l_2798 &= ((~(l_2837 |= (((g_2243 &= ((((****g_1396) , (safe_rshift_func_uint8_t_u_u(l_2822, ((safe_mod_func_uint32_t_u_u((((safe_mul_func_int8_t_s_s((((safe_mul_func_uint16_t_u_u(0UL, l_2787[6])) != (&g_1985 != (l_2822 , l_2829))) < (safe_lshift_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(((((***g_1638) = (9UL != ((((void*)0 != l_2835) , 0xF99CL) <= l_2836))) & (*g_1358)) < 0xC482L), l_2803)), (***g_2496)))), l_2799)) >= g_212[1][6]) != l_2795), l_2822)) ^ 0x04L)))) >= (*g_2031)) >= l_2793)) & l_2822) >= l_2822))) == l_2836);
                if ((0x4E017BDB3083931ELL > ((safe_sub_func_int16_t_s_s((***g_1638), (safe_div_func_int16_t_s_s((((((&g_1985 != (((safe_div_func_uint16_t_u_u((l_2787[7] > ((void*)0 != l_2846)), l_2836)) == 0xC6A83A4996BF92A6LL) , (((((**l_2835) = l_2847) != (void*)0) , l_2778) , (void*)0))) , 0xF5CC6BCAL) || 0UL) && l_2848) | (**g_2030)), (**g_702))))) ^ 0x35A2037C66D92419LL)))
                { /* block id: 1420 */
                    const int8_t l_2859 = 6L;
                    (*g_1242) = ((safe_sub_func_uint8_t_u_u((--(*g_986)), (((safe_rshift_func_int8_t_s_s((*g_2031), 2)) , (**g_2030)) | (safe_add_func_uint8_t_u_u((safe_add_func_uint32_t_u_u(4294967292UL, (((l_2859 , (***g_1344)) == (void*)0) && (**g_2030)))), (((**g_2497) , (*g_2497)) != (void*)0)))))) && 0L);
                }
                else
                { /* block id: 1423 */
                    const int32_t *l_2862 = &l_2805;
                    int32_t l_2875[6][10] = {{8L,9L,0x5B8CE52BL,0x481F68FFL,0x481F68FFL,0x5B8CE52BL,9L,8L,8L,9L},{8L,0x481F68FFL,9L,9L,0x481F68FFL,8L,0x5B8CE52BL,0x5B8CE52BL,8L,0x481F68FFL},{0x481F68FFL,9L,9L,0x481F68FFL,8L,0x5B8CE52BL,0x5B8CE52BL,8L,0x481F68FFL,9L},{0x481F68FFL,0x481F68FFL,0x5B8CE52BL,9L,8L,8L,9L,0x5B8CE52BL,0x481F68FFL,0x481F68FFL},{8L,9L,0x5B8CE52BL,0x481F68FFL,0x481F68FFL,0x5B8CE52BL,9L,8L,8L,9L},{8L,0x481F68FFL,9L,9L,0x481F68FFL,8L,0x5B8CE52BL,0x5B8CE52BL,8L,0x481F68FFL}};
                    int i, j;
                    if (l_2772)
                    { /* block id: 1424 */
                        int64_t l_2865 = 2L;
                        int32_t l_2870 = (-5L);
                        uint64_t *l_2871 = &g_2806[6][2];
                        uint64_t *l_2872 = &g_59;
                        l_2862 = ((safe_lshift_func_uint16_t_u_s(0x503DL, 14)) , (void*)0);
                        (*g_1242) &= ((l_2802 > ((*l_2872) = (((safe_add_func_int64_t_s_s(g_2243, (0x031C91981A8DB2BBLL >= (l_2865 | (0xBFL && (l_2822 , ((safe_mod_func_uint64_t_u_u(((*l_2871) &= (safe_mul_func_int16_t_s_s(l_2865, (l_2870 &= (****g_2199))))), (l_2796 ^= (&g_1808[3] == &g_1808[3])))) , 0x9EL))))))) & l_2794) <= (*g_2031)))) <= l_2787[7]);
                    }
                    else
                    { /* block id: 1431 */
                        int32_t *l_2873 = &l_2800;
                        int32_t *l_2874 = &g_1616;
                        int32_t *l_2876[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        g_2884--;
                        g_2887--;
                        (*l_2873) = ((*g_1242) = 5L);
                        l_2904[2][0] &= (((safe_rshift_func_uint16_t_u_s(((****g_2199) , (safe_rshift_func_uint16_t_u_s(((l_2875[5][1] = (safe_unary_minus_func_uint32_t_u((safe_mul_func_int8_t_s_s(((!g_210) ^ (((*g_1242) = ((((((l_2800 , ((*l_2862) < (((*l_2874) = (safe_lshift_func_uint8_t_u_s(((65532UL == 0xE102L) & (((**g_2030) = (safe_div_func_uint16_t_u_u((6UL != (*l_2862)), (safe_div_func_uint32_t_u_u((((0xC8B0F517L > l_2878) , 0xFA2F7B4EB87D0100LL) , (*l_2862)), 1L))))) > 0xA5L)), 7))) > l_2881[1]))) , l_2800) ^ 1L) || l_2800) , l_2795) > 0xB4E86E628A88D0BDLL)) ^ (*g_1930))), (*l_2862)))))) <= l_2788), 15))), (*g_703))) , (*l_2862)) | 5L);
                    }
                }
            }
            else
            { /* block id: 1443 */
                (*g_1242) = (((safe_lshift_func_uint16_t_u_u(0UL, l_2798)) , 0UL) >= (-7L));
            }
        }
    }
    else
    { /* block id: 1447 */
        int8_t l_2918[10];
        int8_t *l_2919 = &l_2918[2];
        int32_t l_2924 = 0x019112A0L;
        int64_t *l_2926 = &g_2310;
        int8_t *l_2927[3];
        uint32_t ***l_2942[3][6][8] = {{{&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943,(void*)0,&g_943},{&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943},{&g_943,&g_943,(void*)0,&g_943,&g_943,(void*)0,&g_943,&g_943},{&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943},{&g_943,(void*)0,&g_943,&g_943,(void*)0,&g_943,&g_943,&g_943},{(void*)0,&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943,(void*)0}},{{&g_943,&g_943,&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943},{&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943},{&g_943,&g_943,&g_943,(void*)0,&g_943,(void*)0,&g_943,&g_943},{&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943},{&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943,&g_943,&g_943},{&g_943,(void*)0,&g_943,&g_943,(void*)0,&g_943,&g_943,&g_943}},{{(void*)0,&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943,(void*)0},{&g_943,&g_943,&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943},{&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943},{&g_943,&g_943,&g_943,(void*)0,&g_943,(void*)0,&g_943,&g_943},{&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943,&g_943},{&g_943,&g_943,&g_943,(void*)0,&g_943,&g_943,&g_943,&g_943}}};
        uint32_t ****l_2941 = &l_2942[0][5][7];
        uint32_t *****l_2940 = &l_2941;
        uint8_t l_2972 = 1UL;
        int16_t **l_2976 = &g_703;
        uint16_t l_3001[10] = {0x401EL,0x401EL,0x401EL,0x401EL,0x401EL,0x401EL,0x401EL,0x401EL,0x401EL,0x401EL};
        int16_t l_3055 = (-1L);
        uint64_t l_3100 = 9UL;
        const uint16_t l_3180 = 1UL;
        uint8_t l_3212[8][10] = {{254UL,0xDEL,0x3DL,254UL,0x3DL,0xDEL,254UL,0x2CL,0x2CL,254UL},{0x2CL,1UL,0x3DL,0x3DL,1UL,0x2CL,0xDEL,1UL,0xDEL,0x2CL},{0UL,1UL,0x57L,1UL,0UL,0x57L,254UL,254UL,0x57L,0UL},{0UL,0xDEL,0xDEL,0UL,0x3DL,0x2CL,0UL,0x2CL,0x3DL,0UL},{0x2CL,0UL,0x2CL,0x3DL,0UL,0xDEL,0xDEL,0UL,0x3DL,0x2CL},{254UL,254UL,0x57L,0UL,1UL,0x57L,1UL,0UL,0x57L,254UL},{1UL,0xDEL,0x2CL,1UL,0x3DL,0x3DL,1UL,0x2CL,0x57L,0x3DL},{252UL,0x2CL,0x57L,0x7FL,0x2CL,0x7FL,0x57L,0x2CL,252UL,252UL}};
        int32_t l_3232 = 0x1D1154FEL;
        int32_t l_3303 = 0xF1E44DF3L;
        int32_t l_3304[8][8] = {{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),0x51C48E19L,0x51C48E19L,(-1L),(-1L),(-1L),0x51C48E19L},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{0x7ACE2F8AL,(-1L),0x51C48E19L,(-1L),0x7ACE2F8AL,0x7ACE2F8AL,(-1L),0x51C48E19L},{0x7ACE2F8AL,0x7ACE2F8AL,(-1L),0x51C48E19L,(-1L),0x7ACE2F8AL,0x7ACE2F8AL,(-1L)},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)},{(-1L),(-1L),0x51C48E19L,0x51C48E19L,(-1L),(-1L),(-1L),0x51C48E19L},{(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)}};
        int i, j, k;
        for (i = 0; i < 10; i++)
            l_2918[i] = (-9L);
        for (i = 0; i < 3; i++)
            l_2927[i] = &g_1268;
        if ((!(safe_rshift_func_int8_t_s_s((g_1268 = (safe_add_func_int64_t_s_s(((0x62E8CA5863DDAE0ELL | (safe_unary_minus_func_int64_t_s(((*g_1396) == ((*g_1934) = l_2913))))) | (((*g_1985) , ((safe_sub_func_int8_t_s_s(((*l_2919) = ((**g_2030) = (safe_rshift_func_uint8_t_u_u((*g_2498), (l_2918[2] & (*g_1930)))))), ((((*l_2926) = (safe_add_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_u((l_2924 >= l_2924), l_2924)), g_2925))) >= 18446744073709551607UL) > 0x840B28437FD53912LL))) || 0x6EL)) , 0x6692E733FA0DA033LL)), l_2924))), g_1610))))
        { /* block id: 1453 */
            uint32_t ******l_2946 = &l_2940;
            int32_t l_2947 = 0x65F58009L;
            int64_t **l_2957 = (void*)0;
            int64_t **l_2959 = &l_2926;
            int64_t l_2963 = (-3L);
            uint64_t *****l_3017 = &g_1934;
            int32_t l_3060 = 0x9B7DA330L;
            int32_t l_3061 = 0x5CF7AF5EL;
            const uint64_t l_3085[7][6] = {{0xED61DA654D886C75LL,1UL,3UL,1UL,8UL,0x44911ACBE74D7175LL},{0x4D0A7B510557668ALL,3UL,18446744073709551610UL,1UL,0x3528442B177FB36BLL,0xB6EB6BE5B3200CA5LL},{0x4D0A7B510557668ALL,1UL,1UL,1UL,0x4D0A7B510557668ALL,8UL},{0xED61DA654D886C75LL,8UL,0x3528442B177FB36BLL,0x4D0A7B510557668ALL,0xB6EB6BE5B3200CA5LL,18446744073709551614UL},{18446744073709551615UL,0x44911ACBE74D7175LL,0xB6EB6BE5B3200CA5LL,8UL,18446744073709551614UL,18446744073709551614UL},{18446744073709551606UL,0x3528442B177FB36BLL,0x3528442B177FB36BLL,18446744073709551606UL,0x2CDD0E5C4D8C9FD3LL,8UL},{18446744073709551614UL,3UL,1UL,1UL,1UL,0xB6EB6BE5B3200CA5LL}};
            uint8_t l_3127 = 250UL;
            uint16_t l_3144 = 0xC3F7L;
            int i, j;
            (*g_1242) &= (safe_div_func_uint16_t_u_u(((l_2924 = (((((*g_1358) > (((safe_div_func_uint64_t_u_u((safe_add_func_int32_t_s_s((((((****g_2199) &= ((safe_mod_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_s((((((*l_2946) = (l_2943 = l_2940)) != (void*)0) >= ((((*g_1358) , l_2947) , 1UL) > ((*g_2031) != (safe_mod_func_uint32_t_u_u(((safe_sub_func_uint32_t_u_u(((void*)0 == &g_2497), 0x4DDC1CACL)) & l_2947), l_2947))))) < l_2775), l_2777)), 4)), 0x536BL)) > (*g_1358))) <= 65535UL) , l_2776) != (*g_2030)), (*g_1930))), g_2952[3])) , 4294967289UL) >= 4294967295UL)) != 1UL) || l_2947) | l_2947)) , l_2924), (*g_1358)));
            if (((*g_1242) = (safe_add_func_uint64_t_u_u(((~((((l_2958 = (g_2956 = (l_2918[9] , (void*)0))) != ((*l_2959) = &l_2775)) >= 0x4B32L) , l_2924)) , l_2796), 0x5BC0DA3BA49B938FLL))))
            { /* block id: 1463 */
                uint64_t l_2960 = 1UL;
                uint8_t l_2971 = 7UL;
                int32_t l_2977[8][8][4] = {{{9L,(-5L),0x647348D8L,0x7FB2F866L},{0x8D88F804L,(-1L),0xF06593C2L,0xE208F227L},{0L,0x6ECF9D9EL,0x5B37099AL,0x3DBE72AEL},{0x38E270F2L,0x96546A3FL,(-1L),0x7FB2F866L},{0xEBC48F7EL,0x38E270F2L,0xEBC48F7EL,0x647348D8L},{0xB899E1EBL,(-1L),(-1L),(-1L)},{0x3DBE72AEL,0xEBC48F7EL,1L,(-1L)},{1L,(-8L),1L,1L}},{{0x3DBE72AEL,(-4L),(-1L),0x8B42F104L},{0xB899E1EBL,0xECA43FC3L,0xEBC48F7EL,9L},{0xEBC48F7EL,9L,(-1L),(-5L)},{0x38E270F2L,3L,0x5B37099AL,0xFB0F5D13L},{0L,0x8D88F804L,0xF06593C2L,0x38E270F2L},{0x8D88F804L,9L,0x647348D8L,1L},{9L,0xB899E1EBL,0xBC690261L,0x8B42F104L},{0x1746E679L,0xE6688492L,(-1L),0x6ECF9D9EL}},{{1L,(-8L),0x8D88F804L,0xD3D1D5CFL},{0x8B42F104L,0x3DA71072L,0L,(-1L)},{0xEBC48F7EL,0x5B37099AL,0x5B37099AL,0xEBC48F7EL},{0x6D795A37L,(-1L),1L,(-1L)},{1L,(-1L),0x38E270F2L,1L},{0x613F2209L,0x53BC60B7L,0x99BDF245L,1L},{0L,(-1L),1L,(-1L)},{0x9E9B7832L,(-1L),9L,0xEBC48F7EL}},{{1L,0x5B37099AL,0x683D1A29L,(-1L)},{4L,0x9E9B7832L,(-1L),0x5B37099AL},{0xD3D1D5CFL,0xE208F227L,0x8B42F104L,0xF06593C2L},{1L,7L,0x683D1A29L,0x647348D8L},{0x99BDF245L,1L,1L,0xBC690261L},{0x9E9B7832L,0xD3D1D5CFL,3L,(-1L)},{(-1L),(-7L),0x99BDF245L,0x8D88F804L},{0x515B7AD8L,1L,0xF65FC6EBL,0L}},{{1L,0xD3D1D5CFL,0x3DA71072L,0xD3D1D5CFL},{0x96546A3FL,0x99BDF245L,0x5B37099AL,0x647348D8L},{1L,1L,0x76B69EA6L,0x53BC60B7L},{0x647348D8L,0xE208F227L,9L,0xECA43FC3L},{0x647348D8L,1L,0x76B69EA6L,(-1L)},{1L,0xECA43FC3L,0x5B37099AL,1L},{0x96546A3FL,(-1L),0x3DA71072L,1L},{1L,0x73337C3BL,0xF65FC6EBL,1L}},{{0x515B7AD8L,0xF06593C2L,0x99BDF245L,4L},{(-1L),(-1L),3L,1L},{0x9E9B7832L,0L,1L,0xEBC48F7EL},{0x99BDF245L,0xECA43FC3L,0x683D1A29L,0x73337C3BL},{1L,0x9E9B7832L,0x8B42F104L,0xECA43FC3L},{0xD3D1D5CFL,0x76B69EA6L,(-1L),0xF06593C2L},{4L,1L,0x683D1A29L,0x683D1A29L},{1L,1L,9L,0xD3D1D5CFL}},{{0x9E9B7832L,0xBC690261L,1L,(-1L)},{0L,1L,0x99BDF245L,1L},{0x613F2209L,1L,0x38E270F2L,(-1L)},{1L,0xBC690261L,1L,0xD3D1D5CFL},{0x6D795A37L,1L,0x5B37099AL,0x683D1A29L},{0xEBC48F7EL,1L,0x3DBE72AEL,0xF06593C2L},{0x647348D8L,0x76B69EA6L,(-7L),0xECA43FC3L},{0x683D1A29L,0x9E9B7832L,0x76B69EA6L,0x73337C3BL}},{{0xEBC48F7EL,0xECA43FC3L,0xB899E1EBL,0xEBC48F7EL},{0x96546A3FL,0L,1L,1L},{(-7L),(-1L),0xF65FC6EBL,4L},{0x613F2209L,0xF06593C2L,0L,1L},{(-1L),0x73337C3BL,1L,1L},{1L,(-1L),1L,1L},{1L,0xECA43FC3L,0x1746E679L,(-1L)},{1L,1L,(-1L),0xECA43FC3L}}};
                uint32_t *l_2978 = &l_2;
                int32_t *l_2980[9][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                uint16_t *l_3000 = &g_1617;
                const uint64_t *****l_3016 = &g_1396;
                int i, j, k;
                l_2960 = 2L;
                if ((((*l_2978) = ((safe_add_func_uint16_t_u_u((l_2795 & l_2963), (((*l_2776) = (**g_2497)) , (!((((safe_mod_func_int64_t_s_s((safe_mod_func_int32_t_s_s((-2L), (((safe_lshift_func_uint8_t_u_s((((*l_2765) = (l_2971 & 1L)) <= ((l_2972 <= ((safe_add_func_int32_t_s_s((+(l_2976 == l_2976)), l_2795)) , (-5L))) , (****g_2199))), l_2960)) == (*g_2031)) || (*g_2498)))), l_2963)) , l_2971) <= (***g_2496)) > (**g_702)))))) > l_2977[0][6][0])) , 0L))
                { /* block id: 1468 */
                    int32_t *l_2979 = &g_237[0];
                    const uint16_t *l_2999 = (void*)0;
                    uint64_t * const *l_3003 = (void*)0;
                    uint64_t * const **l_3002 = &l_3003;
                    int16_t *l_3004 = &g_745;
                    const uint64_t *****l_3015[3][2] = {{&g_1396,&g_1396},{&g_1396,&g_1396},{&g_1396,&g_1396}};
                    uint64_t ******l_3018 = &g_1933;
                    int i, j;
                    (*g_1242) = l_2977[0][4][1];
                    l_2980[1][0] = l_2979;
                    l_2947 = ((((*l_3004) ^= (((safe_sub_func_int32_t_s_s((safe_add_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s((0x0FL & ((*l_2919) = ((((((0x94C7B26346358F91LL ^ (safe_sub_func_int8_t_s_s((l_2795 >= ((*g_703) = ((((safe_add_func_int32_t_s_s(((safe_mod_func_int16_t_s_s(l_2802, (safe_sub_func_int64_t_s_s(((((*l_2978) &= (*l_2979)) && (*g_1930)) | ((**g_2030) > (safe_lshift_func_int16_t_s_u(((safe_rshift_func_int8_t_s_s((((l_2999 == l_3000) > 3L) != l_2963), 1)) & l_2777), l_2795)))), 0UL)))) == l_3001[6]), l_2804)) , (*g_2498)) , (*g_1934)) == l_3002))), 0x23L))) >= l_3001[6]) , l_2802) || g_1422) || l_2918[4]) | (*l_2979)))), (*l_2979))), 0x69L)), l_2972)) , l_2797) < 18446744073709551615UL)) , (**g_1398)) && 1UL);
                    (*g_1242) = (l_2947 = (((safe_sub_func_int16_t_s_s(((**g_702) = (safe_sub_func_int16_t_s_s((safe_mod_func_uint16_t_u_u((((safe_sub_func_int8_t_s_s(((safe_div_func_uint16_t_u_u(((l_3016 = l_3015[1][1]) == ((*l_3018) = l_3017)), l_2947)) & 4UL), ((*g_2031) , ((*l_2776) = l_2972)))) != ((safe_mul_func_int8_t_s_s(((*l_2919) = (((safe_rshift_func_uint16_t_u_s((l_2926 == (void*)0), 1)) > (safe_unary_minus_func_int8_t_s((safe_sub_func_uint64_t_u_u((safe_div_func_uint64_t_u_u(0x8CACA6EBFA346E56LL, g_2884)), (*l_2979)))))) == 0xBBADB5BEL)), (-1L))) || (*l_2979))) , 0x7A22L), l_3001[2])), 0xFEE5L))), 0x9B6DL)) || 1L) <= l_3001[6]));
                }
                else
                { /* block id: 1483 */
                    uint64_t *l_3053 = &g_2925;
                    int32_t l_3054[8][9][3] = {{{0x909E07CBL,0L,0L},{0x91B240F7L,(-8L),(-6L)},{8L,0x91B240F7L,(-6L)},{0xA5147F65L,1L,0x99D256E7L},{0xFD910B4BL,1L,0x732CF50EL},{6L,0x1ED4FC6BL,0x4F49FDDBL},{0xE6841889L,(-5L),0xFD910B4BL},{0L,7L,1L},{0x69AE1EECL,0x0C70BFCDL,0L}},{{0x919C2BDCL,1L,0x0C70BFCDL},{(-1L),(-1L),0x50B113A8L},{1L,0xD694EB5CL,(-1L)},{0xE1DA647FL,0x80316C74L,6L},{1L,0x69AE1EECL,0L},{(-9L),0xE1DA647FL,6L},{0x91B487D9L,1L,(-1L)},{1L,0xA526C5D0L,0x50B113A8L},{0x798F244AL,6L,0x0C70BFCDL}},{{(-1L),0x684CAB97L,0L},{0x4F156F24L,1L,1L},{(-6L),(-6L),0xFD910B4BL},{0x684CAB97L,0x91B487D9L,0x4F49FDDBL},{0L,(-4L),0x732CF50EL},{0x9914296CL,0x732CF50EL,0x99D256E7L},{0x2D1F817CL,5L,(-6L)},{0xFC787A1FL,0xC56798D3L,(-6L)},{2L,(-6L),0L}},{{0x6595CA04L,0x798F244AL,0x6B61013FL},{0x93E2E7DDL,0xFC787A1FL,0x69AE1EECL},{0x93E2E7DDL,(-6L),2L},{0x6595CA04L,(-1L),1L},{2L,0x2D1F817CL,0x1ED4FC6BL},{0xFC787A1FL,0x7973D27EL,0xC56798D3L},{0x2D1F817CL,0xF52613C1L,0xE6841889L},{0x9914296CL,(-1L),0L},{0L,(-1L),0x684CAB97L}},{{0x684CAB97L,0xF0C60F90L,0xFC787A1FL},{(-6L),0x4F156F24L,5L},{0x4F156F24L,8L,(-4L)},{(-1L),0L,0L},{0x798F244AL,3L,0L},{1L,0x356B3DAEL,0x93E2E7DDL},{0x91B487D9L,0x7DB23885L,0xD2FD4267L},{(-9L),(-7L),0xA5147F65L},{1L,0x7DB23885L,(-5L)}},{{0xE1DA647FL,0x356B3DAEL,1L},{1L,3L,(-8L)},{(-1L),0L,2L},{0x919C2BDCL,8L,(-1L)},{0x69AE1EECL,0x4F156F24L,1L},{0L,0xF0C60F90L,0x80E05C35L},{0xE6841889L,(-1L),1L},{6L,(-1L),(-3L)},{0xFD910B4BL,0xF52613C1L,7L}},{{0xA5147F65L,0x7973D27EL,1L},{8L,0x2D1F817CL,0x4F156F24L},{0x91B240F7L,(-1L),0x7973D27EL},{0x909E07CBL,(-6L),0L},{0x69AE1EECL,1L,8L},{0x6B61013FL,(-8L),0x4F49FDDBL},{0L,0xA526C5D0L,(-1L)},{(-6L),0L,0x6595CA04L},{(-6L),(-7L),0xD694EB5CL}},{{0x99D256E7L,0x91B487D9L,0x2D1F817CL},{0x732CF50EL,(-1L),(-9L)},{0x4F49FDDBL,(-5L),(-1L)},{0xFD910B4BL,0x91B240F7L,1L},{1L,0x6595CA04L,1L},{0L,0x80E05C35L,0x909E07CBL},{0x0C70BFCDL,0L,0x0C70BFCDL},{0x50B113A8L,0xFD910B4BL,0xD2FD4267L},{(-1L),(-9L),(-1L)}}};
                    int16_t l_3062 = 0x222EL;
                    int i, j, k;
                    if (l_2924)
                    { /* block id: 1484 */
                        uint16_t l_3028 = 0x58AFL;
                        int32_t l_3031[2];
                        uint64_t *l_3032 = (void*)0;
                        uint64_t *l_3033 = &g_2806[8][2];
                        uint16_t l_3050 = 65527UL;
                        int i;
                        for (i = 0; i < 2; i++)
                            l_3031[i] = 0L;
                        (*g_1242) = l_3028;
                        (*g_1242) = 1L;
                        (*g_2155) = ((((((*l_2765) = ((0xA9C6CF90727AB60DLL > (safe_mod_func_uint64_t_u_u((l_3031[1] = (*g_1399)), ((*l_3033) &= 0x8D00383165D57E11LL)))) <= (safe_mod_func_uint16_t_u_u(0x6FE8L, ((safe_mod_func_uint8_t_u_u(((safe_add_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_s(l_3028, (safe_mod_func_int8_t_s_s(((safe_lshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((l_3050 & 1UL), 1)), (safe_div_func_int16_t_s_s((((*g_1242) |= (((((l_3053 = &l_2960) == (***g_1396)) == g_153) == 0x2004BE5CL) < 65526UL)) | 1L), 0x6334L)))) == l_2918[0]), (**g_2030))))), 3)) <= l_3054[5][4][1]), 0xBE83L)) , 1UL), l_2963)) , l_2924))))) < l_3054[5][4][1]) >= l_2947) , 0x26086849L) , (void*)0);
                        return l_3055;
                    }
                    else
                    { /* block id: 1494 */
                        uint64_t l_3057 = 18446744073709551615UL;
                        int32_t l_3063[9][5][1] = {{{3L},{0x8BD40616L},{3L},{0x507AE2A1L},{0x54117804L}},{{0x664BCC37L},{0x4514FA17L},{0L},{0L},{0x4514FA17L}},{{0x664BCC37L},{0x54117804L},{0x507AE2A1L},{3L},{0x8BD40616L}},{{3L},{0x507AE2A1L},{0x54117804L},{0x664BCC37L},{0x4514FA17L}},{{0L},{0L},{0x4514FA17L},{0x664BCC37L},{0x54117804L}},{{0x507AE2A1L},{3L},{0x8BD40616L},{3L},{0x507AE2A1L}},{{0x54117804L},{0x664BCC37L},{0x4514FA17L},{0L},{0L}},{{0x4514FA17L},{0x664BCC37L},{0x54117804L},{0x507AE2A1L},{3L}},{{0x8BD40616L},{3L},{0x507AE2A1L},{0x54117804L},{0x664BCC37L}}};
                        uint32_t l_3064 = 18446744073709551615UL;
                        int i, j, k;
                        (*g_1242) = (+9UL);
                        l_3057++;
                        --l_3064;
                        l_3063[8][3][0] = l_3063[8][3][0];
                    }
                    for (g_2735 = 0; (g_2735 <= 8); ++g_2735)
                    { /* block id: 1502 */
                        uint16_t l_3080 = 0UL;
                        int32_t l_3081 = 3L;
                        (*g_1242) ^= ((g_178 &= ((safe_mul_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(((*g_1399) > (l_2797 = (***g_1397))), ((safe_mul_func_uint16_t_u_u(g_3075[1][0], l_2963)) <= ((l_3080 &= (safe_lshift_func_uint8_t_u_s((safe_lshift_func_int8_t_s_u((**g_2030), 3)), 7))) <= (-1L))))) , (l_2777 &= l_3080)), ((((l_3081 ^ (safe_lshift_func_uint16_t_u_s((((((((0xFB61731ADFB97B87LL || l_3084) , l_3085[4][1]) != l_3086) , l_2801) || l_3087) >= 1UL) , l_3084), (***g_1638)))) == 0xDE8A97A4L) < 8UL) , 1UL))) ^ (**g_2030))) != (***g_2496));
                        if (l_2778)
                            continue;
                        l_2947 = (0x1302L >= ((((safe_sub_func_int64_t_s_s((0xA62F08CDL ^ l_2777), ((safe_div_func_uint16_t_u_u(((safe_add_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u(l_2918[2], ((*l_2765) |= l_3081))), 0x995CL)) && ((*g_1399) , ((safe_mod_func_int8_t_s_s(((0UL > l_3054[5][4][1]) , 0xBBL), (-9L))) >= 0x8B4C57A5L))), l_3100)) | (**g_2030)))) , (*g_2031)) , (-4L)) , l_3061));
                    }
                    l_3054[7][7][0] = ((((**g_1984) , ((**g_2497) > (l_3001[6] , ((((((**l_2959) ^= (safe_lshift_func_int16_t_s_u(((void*)0 == (*l_2943)), (((((*l_2765) |= l_2801) < ((safe_div_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_u(((*g_1242) || (safe_add_func_int8_t_s_s((((((safe_lshift_func_int16_t_s_s((safe_div_func_uint8_t_u_u((l_2924 = (*g_2498)), ((*l_2919) &= ((((***g_1638) >= l_2804) == l_3054[0][2][0]) , (*g_2031))))), 12)) | l_3086) > 0x28D5L) == 0x6A56L) || l_3055), (**g_2030)))), l_3001[9])), (****g_2199))) , (**g_702))) < 65535UL) > (****g_1396))))) & g_2393) , 4L) , (void*)0) == (****g_1933))))) == (-1L)) > (***g_2496));
                }
            }
            else
            { /* block id: 1518 */
                int16_t l_3135 = 0x3710L;
                for (g_1012 = 9; (g_1012 >= 2); g_1012 -= 1)
                { /* block id: 1521 */
                    int32_t l_3113[1][9][9] = {{{0x0B76FAF5L,1L,0xC7B05523L,2L,0xA00CB89DL,(-1L),(-1L),0xA00CB89DL,2L},{0x19F52165L,(-6L),0x19F52165L,1L,0L,(-1L),0x6CE8A421L,0xBCB73F87L,0xBCB73F87L},{0xFDE3E2D7L,0x0B76FAF5L,(-6L),(-1L),(-6L),0x0B76FAF5L,0xFDE3E2D7L,(-3L),0xC7B05523L},{0x6CE8A421L,(-1L),0L,1L,0x19F52165L,(-6L),0x19F52165L,1L,0L},{(-1L),(-1L),0xA00CB89DL,2L,0xC7B05523L,1L,0x0B76FAF5L,(-3L),0x0B76FAF5L},{0L,0x581BEF16L,(-1L),(-1L),0x581BEF16L,0L,2L,0xBCB73F87L,0x5BF792DCL},{(-3L),(-1L),0xA00CB89DL,1L,0xFDE3E2D7L,0xFDE3E2D7L,1L,0xA00CB89DL,(-1L)},{0x581BEF16L,0x3476636AL,0L,0x19F52165L,(-1L),1L,2L,2L,1L},{1L,0xA00CB89DL,(-6L),0xA00CB89DL,1L,0x1C6C5056L,0x0B76FAF5L,(-1L),1L}}};
                    int i, j, k;
                    (*g_2155) = (l_3113[0][4][0] , &l_3061);
                    for (g_2887 = 1; (g_2887 <= 9); g_2887 += 1)
                    { /* block id: 1525 */
                        int32_t l_3134 = 1L;
                        int32_t l_3136[10][4][1] = {{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}},{{0L},{0x3270C9F9L},{0L},{0x3270C9F9L}}};
                        int i, j, k;
                        (*g_1098) = g_3114[0][1];
                        if ((**g_1098))
                            break;
                        l_3061 |= (safe_rshift_func_int16_t_s_u(((safe_add_func_uint64_t_u_u((65532UL != ((safe_rshift_func_int8_t_s_s(((((**g_1098) = l_2801) && (0x5EL == (safe_add_func_uint16_t_u_u((((((safe_rshift_func_uint16_t_u_s(l_3127, 15)) && (safe_rshift_func_uint8_t_u_s((**g_2497), ((safe_div_func_int16_t_s_s(l_3001[6], (safe_lshift_func_int16_t_s_s(((l_3134 | 0L) | l_3135), l_3134)))) , (**g_2030))))) , (**g_2030)) <= (*g_2031)) | l_3001[6]), l_3135)))) | l_3136[8][0][0]), (*g_2031))) , 65534UL)), 0xA1712405338FB70CLL)) != 65535UL), l_2972));
                    }
                }
                for (l_2801 = 0; (l_2801 > (-14)); l_2801 = safe_sub_func_uint8_t_u_u(l_2801, 7))
                { /* block id: 1534 */
                    int32_t *l_3139 = &g_571;
                    int32_t *l_3140 = &g_783;
                    int32_t *l_3141 = &g_71;
                    int32_t *l_3142 = &l_2947;
                    int32_t *l_3143[7];
                    int i;
                    for (i = 0; i < 7; i++)
                        l_3143[i] = &l_2804;
                    ++l_3144;
                }
            }
            return (*g_2031);
        }
        else
        { /* block id: 1539 */
            uint8_t l_3147 = 0UL;
            int64_t l_3181 = 1L;
            uint8_t l_3184 = 250UL;
            uint8_t ***l_3196 = &g_985;
            int16_t ***l_3286[6];
            int32_t l_3298[8] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
            int32_t l_3299 = 0x3D0362A7L;
            int64_t l_3308 = 0x1D9FD8920C979CB4LL;
            uint32_t *l_3342 = &g_2439;
            int i;
            for (i = 0; i < 6; i++)
                l_3286[i] = &l_2976;
            l_3147++;
            if (((((((safe_lshift_func_int8_t_s_u(0xFAL, 3)) && l_3147) , (safe_lshift_func_int16_t_s_u(((0xA5L || ((*g_1358) & ((l_2918[2] ^ (safe_mod_func_uint16_t_u_u((((((safe_mul_func_uint16_t_u_u((safe_div_func_int32_t_s_s((safe_div_func_uint64_t_u_u(((safe_rshift_func_uint16_t_u_u(((((void*)0 == l_3164) , (safe_mul_func_uint16_t_u_u((*g_1358), (((((safe_lshift_func_uint8_t_u_s(((safe_lshift_func_int16_t_s_u((l_2924 |= (safe_sub_func_int32_t_s_s((safe_mod_func_int16_t_s_s((((safe_div_func_uint64_t_u_u((safe_div_func_uint16_t_u_u(0x0F41L, l_2795)), l_3147)) < (*g_1358)) && 3UL), l_3087)), l_3100))), 7)) , 0x86L), (**g_2030))) , l_3001[6]) <= l_2972) , l_2778) | l_3147)))) != l_3001[3]), l_2795)) & (*g_2031)), l_3180)), l_2918[2])), (*g_1358))) , &g_2479) == (void*)0) , l_2) | l_3055), (*g_1358)))) >= (**g_2030)))) , l_3084), (*g_1358)))) || l_3181) || (*g_2031)) > l_2801))
            { /* block id: 1542 */
                int16_t *****l_3190 = &g_2198;
                int32_t l_3193 = (-1L);
                uint16_t *l_3254 = &g_3251[3][4];
                int16_t l_3300 = 0x806EL;
                int32_t l_3301 = 0L;
                int32_t l_3306 = 0xBE998379L;
                int32_t l_3312[10][9][2] = {{{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL}},{{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L}},{{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)}},{{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)}},{{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L}},{{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL}},{{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L}},{{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)}},{{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)},{0x7A17E5B5L,(-3L)},{0xB4C31865L,0x7A17E5B5L},{0xA649CADBL,0xA649CADBL},{0xA649CADBL,0x7A17E5B5L},{0xB4C31865L,(-3L)}},{{0x7A17E5B5L,(-3L)},{0xB4C31865L,0xB4C31865L},{0x7A17E5B5L,0x7A17E5B5L},{0x7A17E5B5L,0xB4C31865L},{0x2C9BD703L,0xA649CADBL},{0xB4C31865L,0xA649CADBL},{0x2C9BD703L,0xB4C31865L},{0x7A17E5B5L,0x7A17E5B5L},{0x7A17E5B5L,0xB4C31865L}}};
                int64_t l_3318[3];
                int64_t l_3332 = 0x6BF829754DE869BDLL;
                uint32_t l_3346 = 0UL;
                int i, j, k;
                for (i = 0; i < 3; i++)
                    l_3318[i] = (-9L);
                if ((((safe_lshift_func_uint8_t_u_u(((l_3184 > (safe_mod_func_uint8_t_u_u(((*l_2776) |= ((****g_1396) >= (safe_mul_func_int16_t_s_s(0xAABCL, (((*l_3190) = g_3189) != (void*)0))))), 0xEEL))) , (safe_mod_func_int16_t_s_s(0x7722L, l_3055))), 2)) > (l_2775 == l_3193)) | 0x4F06L))
                { /* block id: 1545 */
                    int8_t ***l_3205 = &g_2030;
                    int8_t ****l_3204 = &l_3205;
                    uint64_t *l_3210 = &g_2806[6][1];
                    int32_t l_3211[10];
                    int64_t l_3214 = 0x5E4A2173C9E797C9LL;
                    int8_t l_3231 = 0x45L;
                    uint32_t l_3252 = 0UL;
                    uint32_t l_3253 = 0x614D967FL;
                    int i;
                    for (i = 0; i < 10; i++)
                        l_3211[i] = 0x6238400BL;
                    l_3214 |= ((safe_add_func_uint32_t_u_u((l_2802 & (l_3196 != (void*)0)), (((((safe_unary_minus_func_uint32_t_u(l_3193)) == ((safe_add_func_uint32_t_u_u(((((((((((safe_mul_func_int8_t_s_s((((((*l_3210) = ((l_2924 = (safe_add_func_int32_t_s_s((((*l_3204) = &g_2030) != &g_2030), ((safe_rshift_func_int16_t_s_s((safe_sub_func_int8_t_s_s(0x1AL, ((*l_2919) |= ((((l_3084 == ((0x4497L < 5UL) < 0x0469F90004EAF235LL)) && (*g_2956)) , 1UL) , 0x04L)))), 13)) , (*g_1930))))) < 1UL)) | l_3193) || (***g_1397)) , l_3211[5]), (***g_2496))) , l_3212[0][7]) <= l_3211[9]) , (***g_2496)) && (**g_2030)) < 0xCD95B377L) ^ (*g_2031)) <= (*g_1242)) , (*g_2956)) >= l_2778), 0x56D6DDE6L)) >= l_2972)) > 0xFA31EEE1L) < l_3193) != (*g_2956)))) , l_3213);
                    l_3232 = (+((!(l_3193 , ((**g_2497) < (l_2924 = 0xD8L)))) ^ ((safe_mod_func_int8_t_s_s((safe_rshift_func_uint8_t_u_s((***g_2496), (*g_2031))), 0x34L)) == (((safe_mod_func_int8_t_s_s(((safe_mul_func_int8_t_s_s(((*l_2919) = ((l_3181 ^ l_3193) > (safe_mul_func_uint8_t_u_u((++(*l_2776)), (l_3184 < l_3100))))), 0UL)) ^ l_3231), (***g_2496))) >= 7UL) > 5L))));
                    l_3253 = (((safe_add_func_uint64_t_u_u(l_2801, ((0xFD3AD5D2L != l_3213) != 0UL))) , (safe_lshift_func_int8_t_s_u((+(safe_unary_minus_func_uint8_t_u((0xFE74L && l_3193)))), 7))) , (safe_lshift_func_uint16_t_u_u(0x005EL, (((safe_add_func_uint8_t_u_u(((safe_div_func_uint16_t_u_u(l_3147, (safe_sub_func_uint64_t_u_u(((*l_3210)--), (safe_mul_func_uint16_t_u_u(((****g_2199) != g_3251[2][4]), l_2924)))))) , l_3181), l_3181)) , 4L) ^ l_3252))));
                    if (((***g_2496) > (l_3254 != l_3255)))
                    { /* block id: 1557 */
                        int64_t *l_3259 = (void*)0;
                        int32_t l_3260 = 0xF1857D5CL;
                        const int32_t ***l_3269 = &g_2154[4];
                        const int32_t ****l_3268 = &l_3269;
                        const int32_t *****l_3267 = &l_3268;
                        const int32_t ******l_3266 = &l_3267;
                        int64_t *l_3271 = &g_327;
                        l_2924 ^= (safe_rshift_func_uint8_t_u_u(((*g_2956) & ((+((((*g_1358) >= ((((l_2801 = (*g_2956)) , ((*l_3271) = (((l_3260 == ((*g_2956) , (0x8F6B13B4L < (l_3001[3] > (~((safe_add_func_uint16_t_u_u((safe_div_func_uint64_t_u_u(((*g_2031) || (l_3266 != &g_2276)), (*g_2956))), (*g_1358))) <= l_3214)))))) ^ 0xF7E098DCL) > g_3270))) , (*g_2031)) || (*g_2498))) >= l_2797) && l_2775)) >= 0L)), (***g_2496)));
                    }
                    else
                    { /* block id: 1561 */
                        (*g_1242) = (1UL | l_3252);
                        (*g_2155) = &l_2801;
                        (***l_3190) = (**g_2199);
                    }
                }
                else
                { /* block id: 1566 */
                    uint64_t l_3274 = 1UL;
                    for (l_2802 = 28; (l_2802 > 8); l_2802 = safe_sub_func_uint16_t_u_u(l_2802, 9))
                    { /* block id: 1569 */
                        return l_3274;
                    }
                }
                for (l_2775 = 7; (l_2775 < 14); l_2775++)
                { /* block id: 1575 */
                    uint8_t *l_3289[7][6] = {{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972},{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972},{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972},{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972},{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972},{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972},{&l_2972,&l_2972,&l_2972,&l_2972,&l_2972,&l_2972}};
                    int32_t l_3293 = 0L;
                    int8_t l_3305 = 0xE3L;
                    int32_t l_3307 = (-8L);
                    int16_t l_3309 = (-2L);
                    int32_t l_3310[7];
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_3310[i] = 0L;
                    for (g_2925 = 0; (g_2925 <= 2); g_2925 += 1)
                    { /* block id: 1578 */
                        int32_t l_3294 = 1L;
                        int32_t *l_3295 = &l_2796;
                        int32_t *l_3296 = &l_2797;
                        int32_t *l_3297[2][1];
                        int i, j;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_3297[i][j] = &g_1616;
                        }
                        (*g_1242) ^= (g_3251[g_2925][g_2925] | (+0L));
                        (*g_1242) = (safe_div_func_uint8_t_u_u(((g_3251[g_2925][(g_2925 + 2)] == (((safe_div_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(((*l_2765) = (safe_mul_func_uint8_t_u_u((**g_2497), ((((l_3286[5] != (*g_3189)) || (((safe_rshift_func_int16_t_s_s(((**l_2976) = (((*g_985) = l_3289[3][4]) != (**g_2496))), ((safe_mul_func_uint8_t_u_u((g_3251[g_2925][(g_2925 + 2)] != ((0L > (l_3193 && (***g_2496))) , l_3087)), 0xB3L)) , g_3292))) , l_3193) == l_2801)) >= (***g_1397)) , (**g_2030))))), l_2797)), l_3293)) | g_3251[g_2925][g_2925]) ^ (**g_2030))) && 0xB68C7611L), 0x5AL));
                        ++l_3313;
                    }
                    for (g_1422 = 0; (g_1422 <= 5); g_1422 += 1)
                    { /* block id: 1588 */
                        int32_t *l_3316 = &l_3193;
                        int32_t *l_3317[6] = {&l_3312[9][5][1],&l_3312[9][5][1],&l_3312[9][5][1],&l_3312[9][5][1],&l_3312[9][5][1],&l_3312[9][5][1]};
                        int i;
                        ++l_3320;
                    }
                    for (l_3100 = 0; (l_3100 <= 30); ++l_3100)
                    { /* block id: 1593 */
                        int32_t *l_3335 = &l_3304[1][3];
                        (*g_1098) = &l_3306;
                        (*g_1242) ^= 0x401E6A87L;
                        (*g_2155) = (((*g_1242) = ((safe_rshift_func_uint16_t_u_s(l_3309, 11)) , (safe_lshift_func_int16_t_s_u(((((+65528UL) <= (++(*l_3255))) != l_3332) && (safe_lshift_func_int8_t_s_s(((*g_944) , ((*g_1933) == (*g_1933))), 7))), 4)))) , l_3335);
                    }
                }
                for (g_327 = 1; (g_327 <= 4); g_327 += 1)
                { /* block id: 1603 */
                    int32_t *l_3336[4] = {&l_2801,&l_2801,&l_2801,&l_2801};
                    int i;
                    (*g_2155) = l_3336[3];
                    (*g_2155) = &l_2797;
                    for (g_1268 = 6; (g_1268 >= 2); g_1268 -= 1)
                    { /* block id: 1608 */
                        int i, j;
                        return l_3212[(g_327 + 1)][(g_1268 + 3)];
                    }
                    for (l_3300 = 0; (l_3300 >= 0); l_3300 -= 1)
                    { /* block id: 1613 */
                        int32_t l_3337[4][3][4] = {{{(-1L),0xF69CFA31L,0L,0x721A4AE6L},{(-1L),(-3L),0x0C79DECCL,0x0A47FB04L},{0xB0BE2E50L,0x721A4AE6L,9L,9L}},{{1L,1L,0xC7800BE4L,0x721A4AE6L},{0x721A4AE6L,0xB0BE2E50L,0xCAE92052L,0xF69CFA31L},{(-3L),(-1L),9L,0xCAE92052L}},{{0xF69CFA31L,(-1L),0xC42F5E0AL,0xF69CFA31L},{(-1L),0xB0BE2E50L,(-3L),0x721A4AE6L},{0x0C79DECCL,1L,0x0C79DECCL,9L}},{{0xF69CFA31L,0x721A4AE6L,0xC62D3D2EL,0x0A47FB04L},{1L,(-3L),0xCAE92052L,0x721A4AE6L},{1L,0xF69CFA31L,0xCAE92052L,0xB0BE2E50L}}};
                        int i, j, k;
                        l_2795 |= g_3075[(g_327 + 1)][l_3300];
                        return l_3337[0][1][0];
                    }
                }
                for (l_2777 = 13; (l_2777 >= 10); --l_2777)
                { /* block id: 1620 */
                    for (g_115 = 16; (g_115 != 46); g_115++)
                    { /* block id: 1623 */
                        return l_3147;
                    }
                    for (g_3270 = 0; (g_3270 <= 3); g_3270 += 1)
                    { /* block id: 1628 */
                        int32_t *l_3344 = &g_237[1];
                        int32_t *l_3345[1];
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_3345[i] = &g_71;
                        l_3301 ^= g_2806[g_3270][g_3270];
                        l_3343 &= (((*g_943) = l_3342) == (*g_2481));
                        l_3346--;
                    }
                }
            }
            else
            { /* block id: 1635 */
                int32_t l_3352 = 0xA1DD78C8L;
                for (l_3055 = 0; (l_3055 <= 9); l_3055 = safe_add_func_int32_t_s_s(l_3055, 5))
                { /* block id: 1638 */
                    int32_t *l_3353 = &l_2801;
                    l_3352 ^= (safe_unary_minus_func_uint16_t_u(((-6L) > (*g_2956))));
                    l_3353 = (void*)0;
                    if (l_3298[4])
                        break;
                }
            }
        }
    }
    for (g_571 = 26; (g_571 < (-11)); g_571 = safe_sub_func_uint32_t_u_u(g_571, 1))
    { /* block id: 1648 */
        (*g_1242) ^= 0xBA59A270L;
        if (l_2777)
            break;
    }
    return (*g_2031);
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_23 g_166 g_1060 g_703 g_153 g_1242 g_237 g_1933 g_1024 g_1098 g_127 g_71 g_1638 g_702 g_1617 g_1358 g_212 g_1984 g_64 g_1616 g_944 g_208 g_210 g_327 g_1930 g_140 g_2030 g_2031 g_2041 g_1012 g_571 g_1604 g_2072 g_2082 g_1398 g_1399 g_103 g_1397 g_1344 g_1345 g_1346 g_396 g_80 g_2310 g_1934 g_66 g_2199 g_2479 g_2480 g_2513 g_2700
 * writes: g_23 g_166 g_80 g_1930 g_1933 g_1615 g_1614 g_1024 g_210 g_212 g_571 g_153 g_140 g_327 g_237 g_1604 g_115 g_71 g_1012 g_64 g_1617 g_66 g_2030 g_127 g_2700
 */
static int64_t  func_6(int16_t  p_7)
{ /* block id: 1 */
    int8_t l_10 = 0x08L;
    uint64_t *l_22 = &g_23[0];
    int32_t l_2084[5][6] = {{0x9080B938L,4L,0x9080B938L,0x82C01129L,0x82C01129L,0x9080B938L},{4L,4L,0x82C01129L,0x775D47A2L,0x82C01129L,4L},{0x82C01129L,4L,0x775D47A2L,0x775D47A2L,4L,0x82C01129L},{4L,0x82C01129L,0x775D47A2L,0x82C01129L,4L,4L},{0x9080B938L,0x82C01129L,0x82C01129L,0x9080B938L,4L,0x9080B938L}};
    int32_t *l_2085 = &g_1614;
    int32_t *l_2087 = &g_64;
    uint64_t l_2097 = 1UL;
    uint32_t **l_2106 = &g_944;
    int32_t l_2258 = 0x0ECADD57L;
    uint32_t l_2286[2];
    uint64_t ****l_2300 = &g_555;
    uint16_t l_2359 = 0x50D2L;
    uint16_t **l_2425 = &g_1358;
    int16_t l_2600 = 0L;
    int16_t l_2649[10] = {0L,0L,6L,0L,0L,6L,0L,0L,6L,0L};
    uint8_t *l_2659 = &g_1492;
    int32_t l_2699 = 0xEB2BF605L;
    int16_t l_2723 = 1L;
    int32_t ****l_2743 = (void*)0;
    const int16_t l_2764 = 0x5045L;
    int i, j;
    for (i = 0; i < 2; i++)
        l_2286[i] = 0UL;
    if (((safe_mul_func_uint8_t_u_u(l_10, (l_10 & ((safe_div_func_uint16_t_u_u((((*g_703) = (safe_lshift_func_uint16_t_u_s((safe_mul_func_int8_t_s_s((!(((((*l_2085) = ((safe_rshift_func_uint16_t_u_s((g_5 != ((((safe_mod_func_uint64_t_u_u((((++(*l_22)) ^ p_7) < func_26(g_5, l_10)), ((l_2084[0][0] ^= ((*g_2030) != (void*)0)) & l_10))) || (**g_1098)) == 0x2BE331059B2A8B96LL) ^ 0x268B3D88B435E988LL)), 1)) & l_10)) , l_2084[0][3]) <= p_7) >= (***g_1397))), l_10)), 2))) & 0xE1CCL), p_7)) ^ p_7)))) , p_7))
    { /* block id: 1058 */
        int32_t *l_2086[9] = {&g_1616,&g_1616,&g_1616,&g_1616,&g_1616,&g_1616,&g_1616,&g_1616,&g_1616};
        int32_t **l_2100 = &l_2087;
        int32_t ****l_2128 = (void*)0;
        int32_t *****l_2127[5];
        const int64_t *l_2176[3][3];
        int16_t ****l_2200 = &g_1638;
        int32_t l_2202 = 1L;
        const int32_t *l_2208 = &g_783;
        uint16_t *l_2250 = &g_1617;
        uint64_t l_2259 = 1UL;
        uint64_t l_2260 = 18446744073709551613UL;
        int32_t l_2285 = 0x438D37E8L;
        uint16_t l_2311 = 0x455DL;
        uint64_t l_2313[5] = {0x5FD253D0FD4F0585LL,0x5FD253D0FD4F0585LL,0x5FD253D0FD4F0585LL,0x5FD253D0FD4F0585LL,0x5FD253D0FD4F0585LL};
        uint16_t l_2347[10][5] = {{0UL,65527UL,0xA866L,0UL,0UL},{0UL,0xC10BL,0UL,65530UL,3UL},{0xA866L,65527UL,0UL,65526UL,3UL},{0x2B8FL,0UL,0UL,0x2B8FL,0UL},{65535UL,0x2B8FL,0xC10BL,65527UL,65535UL},{0xA866L,0xC10BL,65526UL,0xC10BL,0xA866L},{0UL,0x2B8FL,0UL,65527UL,65530UL},{0UL,0x2B8FL,0UL,0UL,0x2B8FL},{65526UL,0xC10BL,0xA866L,0x2B8FL,65530UL},{0xC10BL,0UL,0xA866L,0UL,0xA866L}};
        int32_t l_2350 = 1L;
        uint64_t **l_2364[2][6];
        uint32_t **l_2376 = &g_944;
        int32_t l_2394[7][10] = {{1L,0x01D8DD58L,0x4FC06BE8L,(-10L),(-1L),(-10L),0x4FC06BE8L,0x01D8DD58L,1L,2L},{0x649E9ABEL,2L,0x1DCF01E3L,(-8L),(-2L),1L,4L,(-1L),(-1L),4L},{0x01D8DD58L,0x649E9ABEL,(-8L),(-8L),0x649E9ABEL,0x01D8DD58L,0x26523906L,1L,1L,(-5L)},{(-5L),(-1L),2L,(-10L),0x1DCF01E3L,(-8L),0x01D8DD58L,(-8L),0x1DCF01E3L,(-10L)},{(-5L),(-8L),(-5L),0x26CBAAD5L,4L,0x01D8DD58L,(-10L),(-2L),2L,1L},{0x01D8DD58L,(-10L),(-2L),2L,1L,1L,2L,(-2L),(-10L),0x01D8DD58L},{0x649E9ABEL,1L,(-5L),(-1L),2L,(-10L),0x1DCF01E3L,(-8L),0x01D8DD58L,(-8L)}};
        int32_t *l_2395 = &g_237[0];
        uint8_t l_2396 = 9UL;
        int i, j;
        for (i = 0; i < 5; i++)
            l_2127[i] = &l_2128;
        for (i = 0; i < 3; i++)
        {
            for (j = 0; j < 3; j++)
                l_2176[i][j] = &g_327;
        }
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 6; j++)
                l_2364[i][j] = &l_22;
        }
        l_2087 = l_2086[4];
        if ((safe_rshift_func_uint16_t_u_s((((safe_sub_func_uint32_t_u_u((~((~(((((((p_7 || 0x6C391DE97350BF00LL) & (((p_7 <= ((safe_rshift_func_uint16_t_u_u(((0x6298613E397F0D57LL > (!(p_7 , l_2097))) || (safe_mod_func_uint16_t_u_u(((***g_1344) == (l_2100 = &l_2087)), (safe_add_func_uint64_t_u_u((((+((safe_sub_func_int32_t_s_s(((((*g_127) = 1L) >= 0L) | 3UL), p_7)) || p_7)) , p_7) >= (*g_1930)), g_80))))), p_7)) && p_7)) > (***g_1397)) , 0xC3CE789E6D4E7BADLL)) && 0xE0A2C900D38A0F43LL) , (void*)0) == l_2106) , l_2086[4]) != l_2086[4])) , p_7)), 7L)) == 0x97L) <= 0xCA5B8E80L), 7)))
        { /* block id: 1062 */
            uint64_t l_2109[1][7] = {{2UL,2UL,2UL,2UL,2UL,2UL,2UL}};
            int32_t l_2133 = 0x58FDF108L;
            int32_t l_2135 = 0x9144DE0EL;
            int32_t l_2136 = 0x3DE6E5CDL;
            int32_t l_2139 = (-5L);
            int32_t l_2143[3];
            int32_t *l_2152 = &g_237[2];
            const int32_t **l_2153 = (void*)0;
            int64_t *l_2159 = &g_210;
            uint32_t ***l_2167 = &l_2106;
            uint32_t ****l_2166 = &l_2167;
            int8_t *l_2179 = (void*)0;
            int16_t ****l_2196 = &g_1638;
            uint64_t l_2201 = 0xC56DB99B43D70EB4LL;
            uint64_t **l_2210 = (void*)0;
            int i, j;
            for (i = 0; i < 3; i++)
                l_2143[i] = 0xEC5D528AL;
            for (g_1615 = 0; (g_1615 <= 3); g_1615 += 1)
            { /* block id: 1065 */
                int8_t *l_2117 = &g_178;
                uint8_t *l_2118 = &g_606;
                const int32_t l_2119 = 0xB29A7C48L;
                uint8_t l_2120 = 0x57L;
                int32_t ** const ***l_2129 = &g_1345[3];
                int32_t l_2134 = 0xE4B54A62L;
                int32_t l_2138 = (-1L);
                int32_t l_2140 = 0xF30CFAF5L;
                int32_t l_2145 = (-5L);
                int32_t l_2147 = (-3L);
                int32_t l_2148 = (-1L);
                int i;
            }
        }
        else
        { /* block id: 1170 */
            (*g_127) |= 1L;
        }
        for (g_1617 = 0; (g_1617 < 43); ++g_1617)
        { /* block id: 1175 */
            uint16_t l_2307 = 0x9428L;
            int64_t *l_2312 = &g_210;
            (*g_127) = (safe_rshift_func_uint16_t_u_s((p_7 , ((safe_div_func_uint8_t_u_u(((p_7 < p_7) >= (((safe_div_func_uint8_t_u_u(((1L ^ (((*l_2312) = (safe_add_func_int32_t_s_s((((!(l_2300 == ((((safe_mod_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((!((safe_unary_minus_func_int8_t_s(l_2307)) <= p_7)) && (safe_mod_func_int16_t_s_s(g_2310, p_7))), g_571)), l_2311)) ^ p_7) == p_7) , (*g_1933)))) , 0x84F960E7L) || (*g_127)), 4294967295UL))) <= l_2307)) , 0x3CL), l_2307)) < 0x166D9184L) , p_7)), 0x37L)) , l_2313[0])), 11));
        }
        for (p_7 = 3; (p_7 >= 0); p_7 -= 1)
        { /* block id: 1181 */
            int32_t *l_2314 = &g_237[2];
            uint64_t **l_2363 = &g_557;
            uint16_t *l_2378 = &g_2209;
            (*l_2100) = l_2314;
            for (g_1604 = 0; (g_1604 <= 3); g_1604 += 1)
            { /* block id: 1185 */
                int16_t l_2349 = (-1L);
                int32_t l_2358 = 0xF79C94D2L;
                uint16_t *l_2365 = &g_115;
                uint32_t *l_2428 = &g_235;
            }
        }
    }
    else
    { /* block id: 1223 */
        int32_t *l_2440 = (void*)0;
        const uint8_t *l_2495 = (void*)0;
        const uint8_t **l_2494 = &l_2495;
        const uint8_t ***l_2493 = &l_2494;
        const uint8_t ***l_2499 = (void*)0;
        uint64_t **** const *l_2511 = &g_1934;
        int32_t l_2545[3][5][10] = {{{0x6CFFB03AL,0xB8084EC3L,0L,0xF2D10044L,4L,6L,0L,0xF8131424L,0xDC8CDDD8L,9L},{0x90432EA9L,0xF2D10044L,0xD2A347AFL,0x789590A6L,0x4AEDA4F9L,0x571FBC6AL,6L,6L,0x571FBC6AL,0x4AEDA4F9L},{0xF8131424L,0xD070E6CDL,0xD070E6CDL,0xF8131424L,(-2L),0L,0x44C72F99L,0x4AEDA4F9L,0L,0x6CFFB03AL},{4L,9L,0x6CFFB03AL,2L,6L,0x05A60EC3L,0xD070E6CDL,0x44C72F99L,0L,0x90432EA9L},{0xD070E6CDL,0xFA42621BL,4L,0xF8131424L,0x571FBC6AL,0xDC8CDDD8L,0x96D48F99L,0xDC8CDDD8L,0x571FBC6AL,0xF8131424L}},{{0xFA18239AL,3L,0xFA18239AL,0x789590A6L,0L,0xA1068222L,0xA22E7C7CL,2L,0xDC8CDDD8L,4L},{(-5L),0x4AEDA4F9L,0L,0xF2D10044L,0x05A60EC3L,0xA22E7C7CL,0xFA42621BL,2L,0xFA18239AL,0xD070E6CDL},{0x96D48F99L,0L,0xFA18239AL,0x6CFFB03AL,0xDC8CDDD8L,(-5L),(-5L),0xDC8CDDD8L,0x6CFFB03AL,0xFA18239AL},{0x789590A6L,0x789590A6L,4L,0L,0xA1068222L,0xA1068222L,0xD070E6CDL,0L,0x571FBC6AL,0L},{0x571FBC6AL,0x789590A6L,0xA1068222L,0xFA18239AL,0x44C72F99L,4L,0xD070E6CDL,0x96D48F99L,9L,0L}},{{0x90432EA9L,0xA22E7C7CL,(-5L),0x9956A483L,0L,0L,0L,0x9956A483L,(-5L),0xA22E7C7CL},{3L,0xFA18239AL,0x789590A6L,0L,0xA1068222L,0xA22E7C7CL,2L,0xDC8CDDD8L,4L,0x571FBC6AL},{0x05A60EC3L,0x96D48F99L,(-2L),(-5L),4L,0xA22E7C7CL,0x44C72F99L,0L,2L,0x90432EA9L},{3L,6L,0xFA42621BL,0L,0L,0L,0L,0xFA42621BL,6L,3L},{0x90432EA9L,2L,0L,0x44C72F99L,0xA22E7C7CL,4L,(-5L),(-2L),0x96D48F99L,0x05A60EC3L}}};
        int16_t l_2641 = 0L;
        int32_t l_2651 = 0xBE1CA866L;
        int64_t l_2652 = 0x1C1FD8AE5C08FB90LL;
        int16_t l_2654 = 0x2A7EL;
        int8_t **l_2686[2];
        uint64_t l_2694 = 0x1B0D18BC3AC4F9E2LL;
        uint32_t l_2728 = 0x4B9E5647L;
        int16_t l_2732 = (-1L);
        uint8_t l_2738[1];
        int i, j, k;
        for (i = 0; i < 2; i++)
            l_2686[i] = &g_2031;
        for (i = 0; i < 1; i++)
            l_2738[i] = 255UL;
        for (g_166 = 0; (g_166 == 36); g_166 = safe_add_func_int64_t_s_s(g_166, 7))
        { /* block id: 1226 */
            int32_t *l_2441 = &g_71;
            uint32_t * const *l_2477 = &g_944;
            uint32_t * const ** const l_2476 = &l_2477;
            uint32_t * const ** const *l_2475 = &l_2476;
            int32_t l_2544 = 0L;
            int32_t l_2546 = 0x88E54941L;
            int32_t l_2547[10] = {0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL,0x50B9248AL};
            int16_t ** const * const ***l_2564 = &g_2561;
            uint32_t *l_2565 = (void*)0;
            uint32_t *l_2566 = &l_2286[0];
            int32_t *l_2638 = &l_2546;
            int i;
        }
        for (g_66 = (-20); (g_66 != (-23)); g_66 = safe_sub_func_uint32_t_u_u(g_66, 9))
        { /* block id: 1323 */
            uint32_t ***l_2685 = &g_943;
            int32_t l_2692[5] = {0x80FBD601L,0x80FBD601L,0x80FBD601L,0x80FBD601L,0x80FBD601L};
            int i;
            for (g_1024 = 0; (g_1024 >= 0); g_1024 -= 1)
            { /* block id: 1326 */
                int8_t ***l_2684 = &g_2030;
                int32_t l_2687 = 9L;
                int64_t *l_2688 = &g_327;
                int32_t l_2693 = 0L;
                int32_t l_2698[3][2][6] = {{{0x3AC82599L,0xBECA207BL,0xBECA207BL,0x3AC82599L,0xBECA207BL,0xBECA207BL},{0x3AC82599L,0xBECA207BL,0xBECA207BL,0x3AC82599L,0xBECA207BL,0xBECA207BL}},{{0x3AC82599L,0xBECA207BL,0xBECA207BL,0x3AC82599L,0xBECA207BL,0xBECA207BL},{0x3AC82599L,0xBECA207BL,0xBECA207BL,0x3AC82599L,0xBECA207BL,0xBECA207BL}},{{0x3AC82599L,0xBECA207BL,0xBECA207BL,0x3AC82599L,0xBECA207BL,0xBECA207BL},{0x3AC82599L,0xBECA207BL,0xBECA207BL,0x3AC82599L,0xBECA207BL,0xBECA207BL}}};
                int i, j, k;
                if ((((safe_lshift_func_int8_t_s_u(((!p_7) >= (((****g_2199) |= p_7) != (safe_mul_func_int16_t_s_s(((((~((*l_2688) &= (0xE94AL <= (safe_mod_func_uint16_t_u_u((((*l_2684) = &g_2031) == ((((5UL && (*g_2031)) , l_2685) == (*g_2479)) , l_2686[0])), l_2687))))) , (void*)0) != &l_2545[2][3][0]) || 0xD377L), 65531UL)))), 0)) == 0xE47CL) && p_7))
                { /* block id: 1330 */
                    for (p_7 = 0; (p_7 >= 0); p_7 -= 1)
                    { /* block id: 1333 */
                        int i, j, k;
                        (*g_1098) = (void*)0;
                        return g_2513[g_1024][(g_1024 + 1)][(p_7 + 3)];
                    }
                    if (l_2687)
                        break;
                    return p_7;
                }
                else
                { /* block id: 1339 */
                    int32_t *l_2689 = &g_237[1];
                    int32_t *l_2690 = &l_2545[2][3][0];
                    int32_t *l_2691[7][1] = {{&l_2687},{&l_2687},{&l_2687},{&l_2687},{&l_2687},{&l_2687},{&l_2687}};
                    int32_t l_2697 = 0x92FF5F38L;
                    int i, j;
                    l_2687 = ((((void*)0 == (*g_2479)) & (**g_2030)) , ((*g_1242) |= p_7));
                    l_2694--;
                    --g_2700;
                }
            }
            (*l_2087) |= (l_2692[1] = p_7);
        }
        (*g_127) ^= p_7;
    }
    (*g_1098) = &l_2084[0][0];
    (*g_127) = (((safe_mod_func_int32_t_s_s((((+((safe_rshift_func_int8_t_s_s(((**g_2030) != (safe_rshift_func_uint8_t_u_s(0xD0L, 5))), 5)) > p_7)) == (253UL & ((safe_mul_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((void*)0 == &g_2479), (safe_mul_func_uint8_t_u_u(p_7, (**g_2030))))), ((~(p_7 , 1UL)) != 0UL))) != l_2764))) <= p_7), p_7)) , p_7) > (*g_2031));
    return p_7;
}


/* ------------------------------------------ */
/* 
 * reads : g_5 g_166 g_1060 g_703 g_153 g_1242 g_237 g_1933 g_1024 g_1098 g_127 g_71 g_1638 g_702 g_1617 g_1358 g_212 g_1984 g_64 g_1616 g_944 g_208 g_210 g_327 g_1930 g_140 g_2030 g_2031 g_2041 g_1012 g_571 g_1604 g_2072 g_2082 g_1398 g_1399 g_103
 * writes: g_166 g_80 g_1930 g_1933 g_1615 g_1614 g_1024 g_210 g_212 g_571 g_153 g_140 g_327 g_237 g_1604 g_115 g_71 g_1012 g_64
 */
static uint64_t  func_26(const uint8_t  p_27, int64_t  p_28)
{ /* block id: 3 */
    uint64_t l_35 = 0x585950CFDFE924F6LL;
    int32_t l_1909 = 6L;
    uint64_t l_1914[1];
    uint32_t ** const *l_1915 = &g_1808[3];
    uint32_t ** const **l_1916[7] = {&l_1915,&l_1915,&l_1915,&l_1915,&l_1915,&l_1915,&l_1915};
    uint32_t *l_1919 = &g_140;
    uint32_t ** const l_1918 = &l_1919;
    uint32_t ** const *l_1917 = &l_1918;
    uint64_t *l_1920 = &g_80;
    uint8_t ***l_1921 = &g_985;
    int32_t l_1922 = 9L;
    const uint32_t **l_1925 = (void*)0;
    const uint32_t *l_1927 = &g_5;
    const uint32_t **l_1926 = &l_1927;
    const uint32_t *l_1929 = &g_5;
    const uint32_t **l_1928[2];
    const uint64_t *****l_1932 = &g_1396;
    int32_t *l_1935 = &g_1615;
    int32_t *l_1936 = (void*)0;
    int32_t *l_1937 = &g_1614;
    int8_t l_1940 = 2L;
    int64_t *l_1941 = (void*)0;
    int64_t *l_1942 = &g_210;
    int16_t * const *l_1943[1];
    int32_t l_1944 = 0xEC4824DAL;
    int32_t *l_1945 = &l_1944;
    int32_t *l_1946 = (void*)0;
    int8_t l_1947 = 0L;
    int32_t *l_1948 = &g_571;
    int32_t *l_1949 = &g_237[0];
    int32_t l_1950[7][6][6] = {{{0xA9920220L,(-1L),0x4A758EB7L,0xA766F0EBL,0xA766F0EBL,0x4A758EB7L},{0x77F39FB4L,0x77F39FB4L,0x7E6B316DL,0xAF48C537L,0xD133D716L,0x8C6ADF9FL},{0xEE6F5DA1L,(-6L),0xC281B2C5L,0x17385442L,0xE6265213L,0x7E6B316DL},{1L,0xEE6F5DA1L,0xC281B2C5L,0xF8AB52AAL,0x77F39FB4L,0x8C6ADF9FL},{(-6L),0xF8AB52AAL,0x7E6B316DL,0xCB24B648L,0x0B9153E9L,0x4A758EB7L},{0xCB24B648L,0x0B9153E9L,0x4A758EB7L,7L,0xEB8A37C0L,0x30820A25L}},{{0x03469A8DL,0xBBFBD705L,(-3L),0xDB153051L,6L,0L},{6L,0x7F9D3C5CL,0xA631A0E7L,0x4A758EB7L,2L,1L},{0xF8AB52AAL,0xCB24B648L,(-4L),0x66F5EBFCL,1L,0L},{0xE674DF8EL,1L,0x2FB00C2BL,1L,0xEE6F5DA1L,0x4C723CCCL},{0xE6265213L,0x6D8607DEL,1L,0x3A2296E6L,0xB74D953AL,2L},{7L,1L,(-1L),0x7F9D3C5CL,1L,(-5L)}},{{0x17385442L,1L,(-6L),0xA631A0E7L,6L,0xA631A0E7L},{0xBDB6D96EL,(-6L),0xBDB6D96EL,9L,0xE674DF8EL,0x03469A8DL},{0x1F15EB20L,0xDE2DE90AL,0L,0xA766F0EBL,0xA5B48EFBL,(-5L)},{6L,1L,(-5L),0xA766F0EBL,1L,9L},{0x1F15EB20L,(-4L),1L,9L,1L,0xA5B48EFBL},{0xBDB6D96EL,0xF8AB52AAL,(-1L),0xA631A0E7L,0L,(-3L)}},{{0x17385442L,(-1L),(-3L),0x7F9D3C5CL,(-6L),(-4L)},{7L,(-1L),0x5EA0C21BL,0x3A2296E6L,9L,(-1L)},{0xE781F23FL,1L,0xDE2DE90AL,0x5EA0C21BL,0x7E6B316DL,0x1F15EB20L},{0xCB24B648L,0L,0x03469A8DL,0xEE6F5DA1L,(-1L),0x4AF4CEE3L},{0xC281B2C5L,0x53F807F2L,0x4A758EB7L,1L,1L,0xBDB6D96EL},{0L,0x54D2C8A8L,(-1L),0xB74D953AL,0x8ABF89B9L,0xE674DF8EL}},{{(-6L),(-5L),0L,1L,0xA9920220L,0xA9920220L},{0x4C723CCCL,0xAF48C537L,0xAF48C537L,0x4C723CCCL,0xD133D716L,0x30820A25L},{(-3L),0xEE6F5DA1L,(-6L),0xC281B2C5L,0x17385442L,0xE6265213L},{0xDB153051L,(-1L),0x53F807F2L,6L,0x17385442L,0x3A2296E6L},{1L,0xEE6F5DA1L,2L,0L,0xD133D716L,(-3L)},{(-5L),0xAF48C537L,(-4L),0xDE2DE90AL,0xA9920220L,0x66F5EBFCL}},{{(-4L),(-5L),(-6L),0xD133D716L,0x8ABF89B9L,1L},{1L,0x54D2C8A8L,0xC281B2C5L,0xBBFBD705L,1L,1L},{0x5A361982L,0x53F807F2L,0xE6265213L,0xBDB6D96EL,(-1L),(-6L)},{0xEE6F5DA1L,0L,0x8ABF89B9L,(-5L),0x7E6B316DL,0xDE2DE90AL},{0xA631A0E7L,1L,0x3A2296E6L,0x9DE9CE1DL,9L,(-1L)},{0xAF48C537L,(-1L),0x1F15EB20L,0xE674DF8EL,(-6L),0xCB24B648L}},{{1L,(-1L),0L,0L,0L,1L},{1L,0xF8AB52AAL,0xB74D953AL,0xF8AB52AAL,1L,0L},{0x2FB00C2BL,(-4L),0x77F39FB4L,(-1L),1L,8L},{9L,1L,0xF8AB52AAL,(-4L),0xA5B48EFBL,8L},{0x66F5EBFCL,0xDE2DE90AL,0x77F39FB4L,0x53F807F2L,0xE674DF8EL,0L},{0xA5B48EFBL,(-6L),0xB74D953AL,0xAF48C537L,6L,1L}}};
    int32_t *l_1951 = &g_64;
    int32_t *l_1952 = &g_237[2];
    int32_t *l_1953 = &g_1616;
    int32_t *l_1954[10] = {&g_64,&g_64,&g_237[0],&g_64,&g_64,&g_237[0],&g_64,&g_64,&g_237[0],&g_64};
    uint32_t l_1955 = 0x1D0B5EA6L;
    int32_t l_1995 = 0x24D0E91DL;
    uint32_t **l_2000 = &g_944;
    uint32_t l_2060 = 4294967295UL;
    int64_t l_2064 = 0xB320FF7C6584B2E2LL;
    const int16_t *l_2074 = (void*)0;
    const int16_t **l_2073 = &l_2074;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1914[i] = 18446744073709551615UL;
    for (i = 0; i < 2; i++)
        l_1928[i] = &l_1929;
    for (i = 0; i < 1; i++)
        l_1943[i] = &g_703;
lbl_2066:
    l_1922 |= ((+(func_30(l_35, g_5, &l_35, &l_35) | ((safe_mul_func_int16_t_s_s((safe_mod_func_int16_t_s_s(((((safe_mul_func_uint8_t_u_u((g_166--), (((*l_1920) = (safe_mul_func_uint16_t_u_u(p_28, (g_1060 ^ (l_1914[0] < ((l_1917 = l_1915) == &g_1808[3])))))) && 0xA2DBC255BA9C29ACLL))) , &g_985) == l_1921) > l_1914[0]), l_35)), (*g_703))) , l_1909))) , (*g_1242));
    l_1944 &= (((safe_div_func_uint64_t_u_u((((((g_1024 &= ((*l_1937) = ((*l_1935) = ((l_1909 &= (l_1922 &= ((*l_1918) != (g_1930 = ((*l_1926) = (*l_1918)))))) & (l_1932 != (g_1933 = g_1933)))))) , ((((*g_703) , (safe_mul_func_uint8_t_u_u(l_1914[0], ((l_1940 > ((*l_1942) = ((((l_1936 == l_1927) < p_28) != 0x679EE6982A190A58LL) && (**g_1098)))) != 0x4FE1L)))) , (*g_1638)) != l_1943[0])) , l_1932) == l_1932) != p_28), l_1914[0])) ^ l_1940) , 0x4A39472DL);
    l_1955--;
    if (((*l_1948) = (safe_div_func_int16_t_s_s(((safe_mul_func_int16_t_s_s((-7L), p_27)) > (safe_lshift_func_uint16_t_u_s(((*g_1358) &= (safe_mul_func_int16_t_s_s(((safe_sub_func_int32_t_s_s(((*l_1945) | 0x9F470BB2F7639295LL), (p_27 , ((18446744073709551609UL < ((*l_1942) = ((l_1935 == (void*)0) <= ((safe_add_func_uint8_t_u_u(0x01L, p_28)) && 18446744073709551608UL)))) <= g_1617)))) , 0xBEAEL), (**g_702)))), 14))), (**g_702)))))
    { /* block id: 1004 */
        int32_t **l_1983 = (void*)0;
        int32_t l_1992 = 9L;
        const uint8_t l_1993 = 0UL;
        uint32_t **l_1994 = &g_1809;
        uint32_t **l_2001 = &g_944;
        uint64_t **l_2022 = &l_1920;
        if ((safe_rshift_func_uint16_t_u_s(((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_s((18446744073709551615UL != (~(((safe_div_func_int16_t_s_s((9L < ((safe_lshift_func_uint16_t_u_u(((l_1983 == g_1984) > (((((((**g_702) = (safe_div_func_uint16_t_u_u((*g_1358), (safe_add_func_uint16_t_u_u(p_27, ((((*g_1358) < (safe_lshift_func_uint8_t_u_u(p_28, 4))) <= (p_27 != p_28)) | p_28)))))) == (*l_1951)) || (*l_1953)) == l_1992) , (*g_1638)) != (void*)0)), (*g_1358))) & p_28)), l_1993)) , l_1994) != l_1994))), 15)), l_1995)) , p_28), l_1993)))
        { /* block id: 1006 */
            uint32_t ***l_2002 = (void*)0;
            uint32_t ***l_2003 = &l_2001;
            int32_t **l_2008 = &l_1936;
            uint64_t **l_2021 = &g_102[8][3];
            const int16_t *l_2024 = (void*)0;
            const int16_t **l_2023 = &l_2024;
            int32_t l_2025 = 0xB6D0C9C9L;
            (*l_1952) = ((safe_sub_func_int32_t_s_s(p_28, ((safe_mul_func_int16_t_s_s((l_2000 != ((*l_2003) = l_2001)), ((*l_1951) ^ (g_327 ^= ((*l_1942) ^= ((safe_div_func_uint32_t_u_u((((safe_mod_func_uint32_t_u_u((l_2008 != l_2008), (safe_mul_func_int8_t_s_s(0x70L, (safe_rshift_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((safe_sub_func_uint64_t_u_u((safe_mod_func_uint8_t_u_u((g_166 = ((((((safe_mul_func_int16_t_s_s(((((*l_1945) , ((*l_1920) = (((*l_1919) = ((l_2021 == l_2022) != (**g_1098))) | 0x6EA56614L))) , l_2023) == &l_2024), (***g_1638))) > 255UL) == (*l_1949)) , (**g_1638)) != (*g_702)) , p_28)), 253UL)), (-1L))), p_27)) >= 0x44DB1D96L), 6)))))) , (*g_944)) , p_28), (*l_1952))) , l_2025)))))) != p_28))) < (*l_1951));
        }
        else
        { /* block id: 1014 */
            uint64_t l_2034[1][3][5] = {{{0UL,0UL,0UL,0UL,0UL},{0x4D5DD4BCF40987B4LL,18446744073709551613UL,0x4D5DD4BCF40987B4LL,18446744073709551613UL,0x4D5DD4BCF40987B4LL},{0UL,0UL,0UL,0UL,0UL}}};
            uint8_t *l_2037[6][7][6] = {{{&g_166,&g_606,(void*)0,&g_166,&g_166,&g_606},{&g_1492,&g_606,&g_166,&g_1492,&g_166,&g_606},{(void*)0,&g_606,&g_1492,&g_166,&g_1492,&g_166},{&g_606,&g_1492,&g_1492,&g_606,&g_1492,(void*)0},{&g_1492,&g_1492,&g_606,&g_606,&g_606,&g_166},{(void*)0,&g_166,&g_606,&g_1492,&g_1492,&g_166},{&g_1492,&g_606,(void*)0,(void*)0,(void*)0,&g_166}},{{(void*)0,&g_606,&g_1492,&g_606,(void*)0,&g_166},{&g_606,&g_166,(void*)0,&g_1492,&g_1492,(void*)0},{(void*)0,&g_606,&g_606,&g_606,&g_166,&g_1492},{&g_1492,&g_606,&g_166,&g_1492,&g_166,&g_1492},{&g_166,&g_166,&g_606,&g_1492,&g_1492,&g_166},{&g_166,&g_606,&g_1492,&g_166,&g_166,&g_606},{&g_606,&g_1492,&g_1492,&g_1492,&g_166,&g_166}},{{&g_166,&g_1492,&g_166,&g_1492,&g_1492,&g_606},{&g_606,&g_1492,&g_606,&g_166,&g_1492,&g_166},{&g_1492,(void*)0,&g_166,(void*)0,&g_1492,&g_606},{(void*)0,&g_166,&g_1492,&g_166,&g_606,&g_606},{&g_166,&g_606,&g_166,&g_166,(void*)0,&g_606},{&g_166,(void*)0,&g_1492,(void*)0,&g_606,&g_606},{(void*)0,(void*)0,&g_166,&g_166,&g_1492,&g_166}},{{&g_1492,&g_1492,&g_606,&g_606,&g_166,&g_606},{(void*)0,&g_166,&g_166,&g_606,&g_606,&g_166},{(void*)0,&g_606,&g_1492,&g_606,&g_1492,&g_606},{&g_1492,&g_606,&g_1492,&g_166,&g_1492,&g_166},{&g_1492,(void*)0,&g_606,(void*)0,&g_166,&g_1492},{&g_166,&g_166,&g_166,&g_166,&g_606,&g_1492},{&g_606,&g_166,&g_606,&g_166,(void*)0,(void*)0}},{{&g_606,&g_606,(void*)0,&g_166,&g_606,(void*)0},{&g_166,(void*)0,&g_1492,&g_606,&g_606,&g_166},{&g_166,&g_1492,(void*)0,&g_166,&g_1492,&g_606},{&g_1492,&g_166,&g_606,&g_166,&g_606,&g_606},{(void*)0,&g_166,&g_606,&g_606,(void*)0,&g_606},{&g_606,&g_606,&g_606,&g_606,&g_606,&g_606},{&g_606,&g_606,&g_166,&g_606,&g_166,(void*)0}},{{&g_166,&g_1492,&g_166,&g_606,&g_166,&g_606},{&g_166,&g_166,&g_606,&g_606,&g_166,&g_1492},{&g_606,&g_166,&g_1492,&g_606,&g_166,(void*)0},{&g_606,&g_606,(void*)0,&g_606,&g_166,&g_1492},{(void*)0,&g_166,&g_1492,&g_166,&g_606,&g_1492},{&g_1492,&g_606,&g_166,&g_166,&g_166,&g_166},{&g_166,&g_166,&g_1492,&g_606,(void*)0,(void*)0}}};
            int32_t l_2038 = (-1L);
            uint16_t *l_2042 = &g_115;
            int i, j, k;
            (*l_1948) = ((*g_127) = ((safe_mod_func_int32_t_s_s((((*g_1930) == 0x165DB470L) < (*g_1358)), (safe_sub_func_uint8_t_u_u((g_2030 != (void*)0), ((*l_1952) = (18446744073709551611UL == ((((*l_2042) = (safe_rshift_func_uint16_t_u_s((l_2034[0][0][0] || (((safe_lshift_func_uint8_t_u_u((((*g_2031) = 0x5BL) & (++g_166)), ((g_2041 == (***g_1638)) <= l_2034[0][0][0]))) , (*l_2000)) == (void*)0)), p_27))) & (*g_1358)) , 0x4E7A1C58849762B6LL))))))) | l_2034[0][1][3]));
            for (l_35 = 0; (l_35 <= 0); l_35 += 1)
            { /* block id: 1023 */
                int i;
                return l_1914[l_35];
            }
        }
    }
    else
    { /* block id: 1027 */
        int16_t ***l_2045[2][6][10] = {{{&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702},{&g_702,(void*)0,&g_702,(void*)0,&g_702,&g_702,(void*)0,&g_702,&g_702,(void*)0},{&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,(void*)0},{(void*)0,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702},{&g_702,(void*)0,&g_702,&g_702,&g_702,&g_702,(void*)0,&g_702,&g_702,&g_702},{&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702}},{{&g_702,&g_702,(void*)0,&g_702,&g_702,(void*)0,&g_702,&g_702,(void*)0,&g_702},{&g_702,&g_702,&g_702,&g_702,&g_702,(void*)0,&g_702,&g_702,(void*)0,&g_702},{&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,(void*)0,&g_702},{&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,(void*)0,&g_702},{&g_702,(void*)0,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702,&g_702},{&g_702,&g_702,&g_702,&g_702,&g_702,(void*)0,&g_702,&g_702,&g_702,&g_702}}};
        int32_t l_2057 = 0x2867810FL;
        int32_t l_2061 = 1L;
        uint8_t l_2083 = 255UL;
        int i, j, k;
        for (g_1012 = 6; (g_1012 >= 0); g_1012 -= 1)
        { /* block id: 1030 */
            int i;
            if (((l_2064 = (safe_mod_func_uint32_t_u_u(((((void*)0 == l_2045[0][5][1]) == (safe_div_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(((safe_add_func_uint8_t_u_u(((safe_lshift_func_uint8_t_u_u((safe_add_func_uint8_t_u_u(((+l_2057) , ((safe_mod_func_int32_t_s_s((*l_1948), (*g_127))) && (p_28 & ((*g_2031) &= l_2060)))), l_2061)), 4)) <= (safe_div_func_uint8_t_u_u((*l_1945), (*l_1951)))), 0x1BL)) > p_28), (***g_1638))) & p_27), p_27))) ^ 0xECL), p_28))) || 0x715D5FEFL))
            { /* block id: 1033 */
                for (g_140 = 0; (g_140 <= 0); g_140 += 1)
                { /* block id: 1036 */
                    int i;
                    (*l_1951) |= l_1914[g_140];
                    for (g_571 = 0; (g_571 <= 2); g_571 += 1)
                    { /* block id: 1040 */
                        int32_t *l_2065 = &l_1950[3][2][1];
                        int i, j, k;
                        l_2065 = &l_2057;
                        if (g_212[(g_571 + 1)][(g_571 + 5)])
                            break;
                        if (g_1060)
                            goto lbl_2066;
                        (*l_1945) ^= ((0xA316L | g_208[(g_140 + 5)][(g_571 + 3)][g_571]) ^ 0xFA1AL);
                    }
                }
            }
            else
            { /* block id: 1047 */
                return l_2061;
            }
        }
        (*l_1945) ^= (safe_lshift_func_uint8_t_u_u((((safe_lshift_func_uint8_t_u_s(0x55L, 4)) ^ (0xD2L && (p_28 , ((!((p_27 <= (((l_2073 = g_2072) == &l_2074) > (!p_27))) || (((safe_mul_func_int8_t_s_s(((p_27 , (((safe_sub_func_uint32_t_u_u(((safe_div_func_int16_t_s_s(l_2061, (*g_1358))) && 1L), 0xB9341F71L)) , 0x9E6B053EDCC3C2E3LL) && g_1616)) == 1UL), p_28)) && 18446744073709551612UL) , (*l_1952)))) || g_2082)))) ^ l_2083), p_28));
    }
    return (**g_1398);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static uint32_t  func_30(int16_t  p_31, const uint64_t  p_32, uint64_t * p_33, uint64_t * p_34)
{ /* block id: 4 */
    uint32_t l_690 = 0x93B832A0L;
    uint64_t ***l_764 = (void*)0;
    int32_t l_788 = 0x70C2E6ACL;
    uint64_t l_789 = 18446744073709551615UL;
    int32_t **l_806 = (void*)0;
    int32_t l_827 = 0x45B62B83L;
    uint8_t *l_853 = (void*)0;
    uint64_t * const **l_873 = (void*)0;
    int32_t l_905 = 0L;
    int32_t l_935[8][6] = {{0x97BEF500L,0xD1310F6FL,0x97BEF500L,0x43105DD5L,(-1L),3L},{1L,(-1L),(-1L),0L,(-1L),(-1L)},{(-1L),0xD1310F6FL,0L,0L,(-1L),0x43105DD5L},{1L,0x43105DD5L,0L,0x43105DD5L,1L,(-1L)},{0x97BEF500L,0x43105DD5L,(-1L),3L,(-1L),3L},{0x97BEF500L,0xD1310F6FL,0x97BEF500L,0x43105DD5L,(-1L),3L},{1L,(-1L),(-1L),0L,(-1L),(-1L)},{(-1L),0xD1310F6FL,0L,0L,(-1L),0x43105DD5L}};
    uint8_t **l_984 = &l_853;
    int64_t l_989 = 0x9E0A7DAEE27BDC1BLL;
    int16_t l_1019 = 0x4F05L;
    int8_t *l_1064 = &g_1012;
    int64_t l_1120 = (-1L);
    int16_t ***l_1136 = &g_702;
    int32_t ***l_1167 = &g_1098;
    int32_t ****l_1166 = &l_1167;
    int64_t l_1210 = 0x0070F8EC5C830AF5LL;
    int16_t l_1229 = 6L;
    int8_t l_1232 = 0x74L;
    uint16_t l_1255[9][9][3] = {{{65530UL,0UL,0xE358L},{5UL,5UL,0UL},{5UL,1UL,0x57AFL},{6UL,0x5DBDL,0x5712L},{65528UL,0xD841L,0UL},{65530UL,6UL,0x5712L},{0x99BDL,0x9206L,0x57AFL},{5UL,65533UL,0UL},{0UL,0xFE06L,0xE358L}},{{0UL,0xD841L,5UL},{0xAEDFL,5UL,0UL},{0x30FBL,0x0F44L,65535UL},{0UL,65535UL,0UL},{0xFC33L,0x24DBL,0xE358L},{65528UL,0x0CCAL,0x73ADL},{0xA5AAL,1UL,65528UL},{0xA5AAL,0x0D2BL,0xFC33L},{65528UL,0UL,1UL}},{{0xFC33L,0xFE06L,0x5712L},{0UL,0UL,65532UL},{0x30FBL,65533UL,65535UL},{0xAEDFL,0xA5AAL,0xA928L},{0UL,0UL,0UL},{0UL,0x5DBDL,0UL},{5UL,0x6CCFL,65528UL},{0x99BDL,65535UL,0x5F5AL},{65530UL,0x9CBBL,0xA928L}},{{65528UL,65535UL,0UL},{6UL,0x6CCFL,65528UL},{5UL,0x5DBDL,65535UL},{5UL,0UL,0UL},{65530UL,0xA5AAL,0xFC33L},{0UL,65533UL,0x57AFL},{5UL,0UL,65535UL},{5UL,0xFE06L,8UL},{5UL,0UL,0x5F5AL}},{{0xAEDFL,0x0D2BL,65535UL},{5UL,1UL,65535UL},{65535UL,0x0CCAL,0x5F5AL},{65535UL,0x24DBL,8UL},{5UL,65535UL,65535UL},{0xA5AAL,0x0F44L,0x57AFL},{5UL,65532UL,0UL},{0x73ADL,5UL,0xD841L},{0UL,0x99BDL,0UL}},{{65533UL,0x8FF5L,65535UL},{0xFC33L,5UL,5UL},{0xF1C7L,0UL,5UL},{65528UL,5UL,0xF160L},{0xF1C7L,0UL,65535UL},{0xFC33L,5UL,0x24DBL},{65533UL,0UL,0UL},{0UL,5UL,5UL},{0x73ADL,0x9CBBL,0xFE06L}},{{0x347EL,0xAEDFL,0xE358L},{65535UL,65532UL,0x6CCFL},{0UL,4UL,5UL},{0UL,0UL,0UL},{65529UL,0x0D2BL,65535UL},{0x5712L,0x0D2BL,1UL},{4UL,0UL,0x01D3L},{65535UL,4UL,0UL},{0xF1C7L,65532UL,0x73ADL}},{{65535UL,0xAEDFL,0x24DBL},{0x2DEAL,0x9CBBL,0xF1C7L},{1UL,5UL,0x01D3L},{0UL,0UL,5UL},{0x347EL,5UL,8UL},{0UL,0UL,0x6CCFL},{65535UL,5UL,0x5F5AL},{1UL,0UL,0x6CCFL},{65533UL,5UL,8UL}},{{0x5712L,0x8FF5L,5UL},{0UL,0x99BDL,0x01D3L},{0UL,5UL,0xF1C7L},{4UL,65532UL,0x24DBL},{0xFC33L,0UL,0x73ADL},{0x2DEAL,0x73ADL,0UL},{0UL,0xA5AAL,0x01D3L},{65535UL,0x9CBBL,1UL},{65535UL,5UL,65535UL}}};
    uint32_t l_1262 = 0x1B0C7A1FL;
    int32_t *l_1263 = &g_1024;
    int64_t *l_1264 = &g_210;
    uint16_t *l_1265 = (void*)0;
    uint16_t *l_1266 = &l_1255[6][5][2];
    uint32_t l_1267 = 0x0F9111BAL;
    const int32_t *l_1294 = (void*)0;
    uint64_t l_1299 = 0x6A9155AA08312429LL;
    uint64_t l_1323 = 1UL;
    uint32_t **l_1341 = &g_944;
    uint32_t l_1440 = 4294967295UL;
    uint64_t l_1500 = 18446744073709551615UL;
    uint64_t l_1565 = 18446744073709551615UL;
    const int64_t l_1639 = 1L;
    uint8_t **l_1717[1];
    uint64_t l_1770[9] = {0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL,0x3353EFE8AB5A1081LL};
    uint16_t l_1833 = 0x3D49L;
    int32_t l_1835[6][7][6] = {{{0x02771F12L,0L,0x2BED725AL,0x0EF5140AL,0x17ACB21EL,0x3C20E9F3L},{0x2472564CL,0x425E7452L,0L,1L,2L,0xC9820094L},{0x17ACB21EL,0x77DF896AL,1L,0L,0xF3D3446CL,0x0EF5140AL},{0x425E7452L,0x17ACB21EL,1L,0L,1L,1L},{0x3C20E9F3L,(-1L),(-1L),0x3C20E9F3L,0x77DF896AL,0xAB042516L},{0xAB042516L,0L,0xE462892DL,0x1285245CL,0x0EF5140AL,(-7L)},{0xDA83332AL,(-7L),0L,9L,0x0EF5140AL,0xDA83332AL}},{{(-1L),0L,5L,0L,0x77DF896AL,0x1285245CL},{0x2472564CL,(-1L),9L,2L,1L,0xC9820094L},{0x2BED725AL,0x17ACB21EL,2L,0xAB042516L,0xF3D3446CL,0xAB042516L},{0x02771F12L,0x77DF896AL,0x02771F12L,0L,2L,0xE462892DL},{(-1L),0x0EF5140AL,0xAB042516L,0xABF772A0L,1L,0L},{(-7L),0x425E7452L,0x77DF896AL,0xABF772A0L,0xC9820094L,0xE462892DL},{(-1L),(-7L),(-1L),0xE462892DL,0L,(-1L)}},{{0L,0x02771F12L,1L,9L,(-4L),1L},{(-8L),(-7L),1L,0x2BED725AL,0x2BED725AL,1L},{0x8621B9FFL,0x8621B9FFL,0x2BED725AL,4L,0x3C20E9F3L,0L},{0xAB042516L,(-4L),0x0B13DC31L,(-1L),0x2472564CL,0x2BED725AL},{(-1L),0xAB042516L,0x0B13DC31L,0x02771F12L,0x8621B9FFL,0L},{9L,0x02771F12L,0x2BED725AL,1L,0xC9820094L,1L},{1L,0xC9820094L,1L,0xE462892DL,(-7L),1L}},{{0x0EF5140AL,(-1L),1L,4L,1L,(-1L)},{1L,(-7L),(-1L),0x77DF896AL,0x2472564CL,0xE462892DL},{0x8621B9FFL,1L,0x77DF896AL,(-7L),0x1285245CL,0L},{0L,1L,0xAB042516L,1L,0x2472564CL,5L},{1L,(-7L),0L,0x02771F12L,1L,9L},{0L,(-1L),0x2BED725AL,0xABF772A0L,(-7L),2L},{0x02771F12L,0xC9820094L,(-1L),(-1L),0xC9820094L,0x02771F12L}},{{0x0EF5140AL,0x02771F12L,0xF3D3446CL,(-7L),0x8621B9FFL,(-1L)},{(-8L),0xAB042516L,0xE462892DL,5L,0x2472564CL,1L},{(-8L),(-4L),5L,(-7L),0x3C20E9F3L,9L},{0x0EF5140AL,0x8621B9FFL,0xAB042516L,(-1L),0x2BED725AL,0x17ACB21EL},{0x02771F12L,(-7L),0x0B13DC31L,0xABF772A0L,(-4L),(-7L)},{0L,0x02771F12L,0x77DF896AL,0x02771F12L,0L,2L},{1L,(-7L),0xE462892DL,1L,0xC9820094L,1L}},{{0L,0x425E7452L,(-8L),(-7L),1L,1L},{0x8621B9FFL,0x0EF5140AL,0xE462892DL,0x77DF896AL,0x2BED725AL,2L},{1L,(-4L),0x77DF896AL,4L,0xDA83332AL,(-7L)},{0x0EF5140AL,1L,0x0B13DC31L,0xE462892DL,0x17ACB21EL,0x17ACB21EL},{1L,0xAB042516L,0xAB042516L,1L,(-4L),9L},{9L,0x425E7452L,5L,0x02771F12L,(-7L),1L},{(-1L),0L,0xE462892DL,(-1L),(-7L),(-1L)}}};
    const int64_t l_1837 = 0xA1B5393096AE7FB6LL;
    uint32_t l_1884 = 0x69FF74F8L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1717[i] = &l_853;
    return p_31;
}


/* ------------------------------------------ */
/* 
 * reads : g_38 g_64 g_59 g_5 g_66 g_115 g_71 g_140 g_166 g_153 g_178 g_103 g_127 g_210 g_212 g_208 g_235 g_237 g_327 g_555 g_571 g_606 g_80
 * writes: g_38 g_64 g_71 g_59 g_80 g_102 g_115 g_127 g_103 g_140 g_66 g_166 g_153 g_178 g_208 g_210 g_212 g_235 g_237 g_327 g_396 g_555 g_571 g_606
 */
static int32_t  func_36(const uint16_t  p_37)
{ /* block id: 5 */
    uint64_t *l_60 = &g_59;
    uint64_t ***l_559 = &g_556[1];
    const uint32_t *l_569 = &g_5;
    int32_t l_572 = 0x9254B591L;
    int32_t l_674 = 1L;
    for (g_38 = (-29); (g_38 == (-3)); g_38++)
    { /* block id: 8 */
        uint64_t *l_58 = &g_59;
        uint64_t ****l_558[10][4][5] = {{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,(void*)0,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,(void*)0,&g_555,(void*)0,&g_555}},{{(void*)0,&g_555,&g_555,&g_555,&g_555},{&g_555,(void*)0,&g_555,(void*)0,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555},{(void*)0,&g_555,(void*)0,&g_555,&g_555}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,&g_555,(void*)0}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,(void*)0,&g_555,&g_555,(void*)0},{&g_555,&g_555,(void*)0,&g_555,&g_555},{&g_555,(void*)0,&g_555,&g_555,&g_555}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{(void*)0,&g_555,&g_555,&g_555,&g_555},{(void*)0,&g_555,&g_555,&g_555,&g_555},{(void*)0,&g_555,&g_555,&g_555,&g_555}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,(void*)0,&g_555,&g_555,&g_555},{(void*)0,&g_555,&g_555,&g_555,(void*)0},{&g_555,&g_555,&g_555,&g_555,&g_555}},{{&g_555,&g_555,(void*)0,&g_555,(void*)0},{(void*)0,&g_555,(void*)0,&g_555,&g_555},{(void*)0,&g_555,&g_555,&g_555,&g_555},{(void*)0,&g_555,(void*)0,(void*)0,&g_555}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,&g_555,(void*)0,&g_555},{&g_555,(void*)0,&g_555,&g_555,&g_555},{&g_555,&g_555,(void*)0,&g_555,&g_555}},{{&g_555,&g_555,&g_555,&g_555,&g_555},{&g_555,&g_555,(void*)0,&g_555,&g_555},{&g_555,&g_555,(void*)0,&g_555,&g_555},{(void*)0,&g_555,&g_555,&g_555,(void*)0}}};
        int32_t *l_570 = &g_571;
        uint16_t l_573 = 65531UL;
        int16_t *l_672 = &g_153;
        uint32_t *l_673[1][1];
        int32_t *l_675 = &g_237[3];
        int i, j, k;
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 1; j++)
                l_673[i][j] = &g_235;
        }
        (*l_675) = (((*l_570) = (safe_mod_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u((safe_mul_func_int16_t_s_s(((*l_672) = (g_38 || (p_37 | (safe_lshift_func_int8_t_s_u(((safe_div_func_int64_t_s_s(func_51(func_55(l_58, l_60), (safe_add_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_div_func_uint64_t_u_u(((g_555 = g_555) != l_559), (((safe_mod_func_uint16_t_u_u((safe_lshift_func_uint16_t_u_u(((~((((((safe_add_func_uint32_t_u_u((safe_add_func_int32_t_s_s(((*l_570) |= (l_569 != (void*)0)), l_572)), p_37)) , (*l_570)) >= g_38) >= 0x805E2ECF681C85FBLL) < p_37) == p_37)) >= 0xB206AE4D5B689FBCLL), g_38)), l_572)) < 0x90L) || 18446744073709551615UL))), 6UL)), l_573)), l_572), 0xE6A0AB16DCCA87AELL)) >= p_37), g_5))))), g_38)), p_37)), 3UL))) , l_674);
    }
    for (g_166 = 0; (g_166 == 44); g_166 = safe_add_func_uint32_t_u_u(g_166, 5))
    { /* block id: 329 */
        if (l_674)
        { /* block id: 330 */
            uint32_t l_681 = 6UL;
            uint64_t ***l_684 = &g_556[8];
            for (g_38 = 0; (g_38 == 10); g_38 = safe_add_func_int64_t_s_s(g_38, 1))
            { /* block id: 333 */
                int32_t *l_680 = (void*)0;
                int32_t *l_685[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_685[i] = &l_572;
                ++l_681;
                g_237[0] = ((void*)0 != l_684);
            }
        }
        else
        { /* block id: 337 */
            return p_37;
        }
    }
    return p_37;
}


/* ------------------------------------------ */
/* 
 * reads : g_140 g_64 g_5 g_153 g_212 g_166 g_606 g_237 g_571 g_80 g_59 g_235 g_178 g_103
 * writes: g_178 g_166 g_66 g_210 g_606 g_237 g_64 g_571 g_140 g_153
 */
static int64_t  func_51(uint64_t * p_52, int64_t  p_53, uint8_t  p_54)
{ /* block id: 277 */
    const int32_t l_599 = 7L;
    int32_t l_612[10][10] = {{2L,0x45046C54L,2L,(-7L),(-1L),0xE2BEEC09L,3L,(-7L),(-7L),3L},{(-1L),0x691516D2L,0xE2BEEC09L,0xE2BEEC09L,0x691516D2L,(-1L),0x9743EE81L,(-7L),0xE926B2A1L,0x5D16A322L},{0x45046C54L,0x5D16A322L,2L,1L,(-7L),1L,2L,0x5D16A322L,0x45046C54L,(-1L)},{0x45046C54L,0xE2BEEC09L,(-7L),0x9743EE81L,1L,(-1L),(-1L),1L,0x9743EE81L,(-7L)},{(-1L),(-1L),1L,0x9743EE81L,(-7L),0xE2BEEC09L,0x45046C54L,3L,0x45046C54L,0xE2BEEC09L},{2L,1L,(-7L),1L,2L,0x5D16A322L,0x45046C54L,(-1L),0xE926B2A1L,0xE926B2A1L},{0x9743EE81L,(-1L),0x691516D2L,0xE2BEEC09L,0xE2BEEC09L,0x691516D2L,(-1L),0x9743EE81L,(-7L),0xE926B2A1L},{3L,0xE2BEEC09L,(-1L),(-7L),2L,0x45046C54L,2L,(-7L),(-1L),0xE2BEEC09L},{0x691516D2L,0x5D16A322L,(-1L),2L,(-7L),(-7L),0x9743EE81L,0x9743EE81L,(-7L),(-7L)},{0xE926B2A1L,0x691516D2L,0x691516D2L,0xE926B2A1L,1L,(-7L),3L,(-1L),(-7L),(-1L)}};
    uint16_t l_620 = 0xF8CBL;
    int8_t l_641 = 0x47L;
    uint32_t l_653 = 0xFA9C1591L;
    uint64_t ***l_656[8][1] = {{&g_556[1]},{&g_556[1]},{&g_556[1]},{&g_556[1]},{&g_556[1]},{&g_556[1]},{&g_556[1]},{&g_556[1]}};
    uint64_t ***l_657[9] = {&g_556[0],&g_556[0],&g_556[1],&g_556[0],&g_556[0],&g_556[1],&g_556[0],&g_556[0],&g_556[1]};
    int8_t *l_658 = &g_178;
    int16_t *l_659 = (void*)0;
    int16_t *l_660[9][9][3] = {{{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,(void*)0,(void*)0},{&g_153,(void*)0,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153}},{{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,&g_153,&g_153}},{{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,(void*)0,&g_153},{&g_153,(void*)0,&g_153},{&g_153,(void*)0,&g_153},{&g_153,&g_153,(void*)0},{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153}},{{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153}},{{&g_153,&g_153,&g_153},{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,(void*)0},{&g_153,(void*)0,&g_153}},{{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,(void*)0,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,&g_153,&g_153}},{{&g_153,&g_153,&g_153},{&g_153,&g_153,(void*)0},{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153},{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,(void*)0,&g_153},{&g_153,&g_153,&g_153}},{{&g_153,&g_153,(void*)0},{&g_153,(void*)0,&g_153},{&g_153,&g_153,(void*)0},{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153}},{{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153},{(void*)0,&g_153,&g_153},{&g_153,&g_153,&g_153},{&g_153,&g_153,(void*)0},{&g_153,&g_153,&g_153},{&g_153,&g_153,&g_153}}};
    int32_t *l_661 = (void*)0;
    int32_t *l_662 = &l_612[9][4];
    int32_t l_663 = 0xC24C1485L;
    int32_t *l_664 = &g_237[0];
    int32_t *l_665 = &g_237[2];
    int32_t *l_666 = (void*)0;
    int32_t *l_667[1][4][4] = {{{(void*)0,&l_612[0][1],(void*)0,&l_612[0][1]},{(void*)0,&l_612[0][1],(void*)0,&l_612[0][1]},{(void*)0,&l_612[0][1],(void*)0,&l_612[0][1]},{(void*)0,&l_612[0][1],(void*)0,&l_612[0][1]}}};
    int32_t l_668 = 0L;
    uint16_t l_669 = 0x1C11L;
    int i, j, k;
    for (p_54 = 0; (p_54 == 54); p_54 = safe_add_func_uint32_t_u_u(p_54, 4))
    { /* block id: 280 */
        const int32_t l_582 = 4L;
        int8_t *l_583 = (void*)0;
        int8_t *l_584[8][10] = {{&g_66,&g_66,&g_178,&g_66,&g_178,&g_66,&g_66,&g_66,&g_178,&g_66},{&g_178,&g_66,&g_178,&g_66,&g_66,&g_66,&g_178,&g_66,&g_178,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66,(void*)0,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_178,&g_66,&g_178,&g_66,&g_66,&g_66,&g_178,&g_66},{&g_178,&g_66,&g_178,&g_66,&g_66,&g_66,&g_178,&g_66,&g_178,&g_66},{&g_66,&g_66,&g_66,&g_66,&g_66,(void*)0,&g_66,&g_66,&g_66,&g_66},{&g_66,&g_66,&g_178,&g_66,&g_178,&g_66,&g_66,&g_66,&g_178,&g_66},{&g_178,&g_66,&g_178,&g_66,&g_66,&g_66,&g_178,&g_66,&g_178,&g_66}};
        int32_t l_585[7][6][6] = {{{0xDB3445CEL,0xDB3445CEL,0x57D17F23L,(-7L),0x1279A759L,0x1CF7DC32L},{0xB0A17151L,0xA3F0EE69L,0x920A235CL,5L,0x9EBC1412L,0x57D17F23L},{0x100B603BL,0xB0A17151L,0x920A235CL,0x9CE8B7ACL,0xDB3445CEL,0x1CF7DC32L},{0x33BA33E0L,0x9CE8B7ACL,0x57D17F23L,0xDFC325C0L,0L,(-7L)},{0xDFC325C0L,0L,(-7L),0x0C6450C7L,0xC510C92EL,(-4L)},{7L,(-1L),0L,0x3F198DC5L,0xB2D4348CL,1L}},{{0xB2D4348CL,0x552E7C1CL,(-7L),(-1L),1L,5L},{0L,(-1L),0x842C4DEBL,(-6L),1L,0L},{1L,0xA3F0EE69L,0x062F9F1FL,(-1L),1L,1L},{(-1L),0x920A235CL,(-1L),(-7L),0xCFE2A1E0L,0x33BA33E0L},{6L,(-1L),1L,(-1L),0x33BA33E0L,0x7B953AD2L},{0xDFC325C0L,7L,1L,1L,1L,(-1L)}},{{0x08325ED2L,9L,(-1L),0xE1F256A3L,0xA3F0EE69L,0x8BD843FDL},{(-1L),7L,2L,1L,(-1L),5L},{(-1L),0x3444D775L,7L,(-7L),0x9959F675L,6L},{0x920A235CL,0xEFBAF48BL,1L,0xDCE9E878L,0x43B90F0BL,6L},{0x9CE8B7ACL,1L,2L,0x8BD843FDL,0xCFE2A1E0L,(-7L)},{0x521E7C35L,(-1L),0x57D17F23L,0xE77C23D7L,0x3F198DC5L,(-7L)}},{{0x8D43EC58L,1L,0x0C6450C7L,(-1L),2L,1L},{0xE1F256A3L,0L,0x9B76ED0DL,1L,0x9B76ED0DL,0L},{(-10L),0xD93A882FL,1L,(-4L),0xEFBAF48BL,(-9L)},{0x2511D667L,1L,0xB2D4348CL,0xD9226A75L,0x8BD843FDL,9L},{1L,1L,0x1CF7DC32L,7L,0xEFBAF48BL,0xA3F0EE69L},{(-1L),0xD93A882FL,1L,0x920A235CL,0x9B76ED0DL,1L}},{{(-7L),0L,0xCFE2A1E0L,0x552E7C1CL,2L,1L},{0xB0A17151L,0x0C6450C7L,1L,1L,(-7L),0x062F9F1FL},{7L,0x062F9F1FL,0x94842205L,(-7L),0x1279A759L,2L},{(-9L),6L,1L,(-1L),0x6BEB3631L,0x29F1086AL},{0x2511D667L,0x94842205L,(-1L),0x7B953AD2L,9L,0xD9226A75L},{(-7L),0x8F81571BL,0xE1F256A3L,0xC472CE8FL,0xDB3445CEL,0L}},{{0x552E7C1CL,1L,0L,7L,0x33F3A96EL,1L},{(-10L),1L,0x7A02613EL,2L,9L,(-1L)},{0x062F9F1FL,2L,7L,1L,(-7L),(-1L)},{0xD93A882FL,7L,0xEEC44BEDL,1L,0x1279A759L,2L},{0x9B76ED0DL,(-6L),1L,0xB2D4348CL,(-1L),6L},{0x9959F675L,0x33F3A96EL,(-1L),1L,(-4L),5L}},{{0xE1F256A3L,0x9959F675L,(-6L),0xC472CE8FL,(-1L),0x7A02613EL},{2L,0xD93A882FL,0xEF22322DL,0x08325ED2L,1L,0L},{0x7B953AD2L,(-1L),0xB2D4348CL,0x062F9F1FL,0xDCE9E878L,0x57D17F23L},{0x727EA700L,1L,7L,7L,1L,0x727EA700L},{7L,0x50D01EACL,0x9959F675L,0x0C6450C7L,0x9B76ED0DL,7L},{0xCC41EAECL,1L,1L,(-9L),(-1L),1L}}};
        uint8_t *l_598 = &g_166;
        int32_t *l_600 = &g_237[3];
        uint16_t l_642 = 3UL;
        int i, j, k;
        if ((safe_add_func_int8_t_s_s(((g_66 = ((((((safe_sub_func_uint64_t_u_u((safe_rshift_func_uint8_t_u_u((p_53 == (l_585[3][5][0] = (0x1B0269BFL < l_582))), p_54)), ((safe_sub_func_uint64_t_u_u((((((((safe_sub_func_uint32_t_u_u(0UL, l_582)) || (-1L)) <= ((*l_598) = (safe_sub_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((safe_mod_func_int16_t_s_s((((g_178 = (p_54 , p_54)) || 9L) == 0x0506BF24L), g_140)), p_54)), 0xF7F2L)))) == l_582) || 0xEB9623361F96C25ELL) || 0L) && p_53), p_53)) < 1L))) & 0x6D89BC37C76D20A8LL) , g_64) > l_599) , g_5) , l_599)) , g_153), g_212[2][1])))
        { /* block id: 285 */
            l_600 = &g_571;
        }
        else
        { /* block id: 287 */
            int32_t l_603 = 1L;
            int32_t l_619 = 0L;
            uint8_t * const l_625[9][2] = {{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166},{&g_166,&g_166}};
            int32_t **l_629 = &g_127;
            int i, j;
            if (l_599)
            { /* block id: 288 */
                int64_t *l_604 = &g_210;
                uint8_t *l_605 = &g_606;
                int32_t l_609 = 0x4DAE9847L;
                int32_t *l_613 = &l_612[5][5];
                int32_t *l_614 = &g_71;
                int32_t *l_615 = (void*)0;
                int32_t *l_616 = &g_571;
                int32_t *l_617 = &g_237[3];
                int32_t *l_618[5][4];
                const int32_t l_628 = 0L;
                int i, j;
                for (i = 0; i < 5; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_618[i][j] = &l_585[3][5][0];
                }
                (*l_600) &= (safe_mul_func_int8_t_s_s((((((*l_598) &= ((l_603 , ((*l_604) = 0x775D9F8CE65F8A81LL)) && 0UL)) == ((*l_605)++)) , (l_609 || 2L)) , (safe_lshift_func_uint8_t_u_s(9UL, 1))), p_54));
                l_620++;
                (*l_600) = ((*l_613) = (((p_54 >= (0x21AEL & (g_571 & g_153))) != (safe_sub_func_uint8_t_u_u(((*l_605) = 4UL), (((*p_52) , &g_166) == l_625[1][0])))) , (((safe_mod_func_uint32_t_u_u(((((p_53 , g_235) , g_178) && l_628) != (*p_52)), (*l_616))) > 0x288D6D0DACD21FACLL) | (*l_616))));
            }
            else
            { /* block id: 297 */
                (*l_600) = (0x29L || ((l_619 = g_103) >= (l_629 == (void*)0)));
                for (g_64 = (-24); (g_64 > 0); g_64++)
                { /* block id: 302 */
                    int32_t *l_632 = &g_571;
                    (*l_632) = ((*l_600) = 0xFA738BCAL);
                }
                for (g_140 = (-2); (g_140 > 27); ++g_140)
                { /* block id: 308 */
                    int32_t *l_635 = &l_603;
                    int32_t *l_636 = (void*)0;
                    int32_t *l_637 = &l_585[1][4][4];
                    int32_t *l_638 = &l_585[6][5][0];
                    int32_t *l_639 = (void*)0;
                    int32_t *l_640[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    l_642--;
                    return l_641;
                }
            }
            if (p_54)
                continue;
            if (p_53)
                continue;
        }
    }
    (*l_662) = (safe_sub_func_int16_t_s_s((g_153 |= ((((g_235 , (-1L)) > ((*p_52) != ((safe_sub_func_uint64_t_u_u((((safe_mul_func_uint16_t_u_u((4294967295UL != (safe_mul_func_int8_t_s_s(l_653, l_612[9][2]))), ((((*l_658) &= (((safe_add_func_int64_t_s_s(((((l_656[5][0] = l_656[5][0]) != l_657[5]) == (0x47L < p_53)) > p_53), g_606)) & g_103) == g_64)) && 0xD8L) , p_53))) <= p_54) >= (-10L)), 18446744073709551610UL)) & l_599))) != 1UL) >= l_641)), p_54));
    ++l_669;
    return (*l_665);
}


/* ------------------------------------------ */
/* 
 * reads : g_64 g_59 g_5 g_66 g_115 g_71 g_140 g_166 g_153 g_178 g_103 g_127 g_38 g_210 g_212 g_208 g_235 g_237 g_327 g_80
 * writes: g_64 g_71 g_59 g_80 g_102 g_115 g_127 g_103 g_140 g_66 g_166 g_153 g_178 g_208 g_210 g_212 g_235 g_237 g_327 g_396
 */
static uint64_t * func_55(uint64_t * p_56, uint64_t * p_57)
{ /* block id: 9 */
    uint32_t l_61[9] = {0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L,0x24D8F3B1L};
    int32_t *l_62 = (void*)0;
    int32_t *l_63 = &g_64;
    uint8_t l_494 = 0x06L;
    int16_t l_495 = 0x5538L;
    uint64_t *l_548 = &g_80;
    int i;
    (*l_63) |= l_61[4];
    for (g_64 = 8; (g_64 >= 0); g_64 -= 1)
    { /* block id: 13 */
        int8_t *l_65[10] = {&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66,&g_66};
        int32_t l_67 = (-5L);
        int32_t *l_68 = (void*)0;
        int32_t *l_69 = (void*)0;
        int32_t *l_70 = &g_71;
        int i;
        (*l_70) = (l_61[g_64] != (l_67 ^= (-1L)));
        for (g_59 = 3; (g_59 <= 8); g_59 += 1)
        { /* block id: 18 */
            uint64_t *l_78 = (void*)0;
            uint64_t *l_79 = &g_80;
            int32_t l_89 = 0x5EF1C8D2L;
            uint32_t l_93 = 0xD14A140CL;
            (*l_70) = ((safe_add_func_uint16_t_u_u(func_74(((*l_79) = (*p_56)), ((safe_mul_func_uint16_t_u_u(func_83(l_89, ((((!(*l_63)) , ((safe_sub_func_int64_t_s_s(g_5, (&l_67 != &g_71))) > g_59)) < 0xA6EDB5B292197989LL) | l_93), &g_66, (*l_63), l_89), l_494)) >= 0xF9L), l_495), l_93)) || (*l_63));
        }
        return l_548;
    }
    return p_57;
}


/* ------------------------------------------ */
/* 
 * reads : g_80 g_212 g_71 g_115 g_140 g_208 g_153 g_237 g_59 g_38 g_327 g_166
 * writes: g_80 g_71 g_237 g_140 g_210 g_153 g_66 g_212
 */
static uint16_t  func_74(uint64_t  p_75, const int32_t  p_76, uint32_t  p_77)
{ /* block id: 242 */
    const uint64_t *l_503 = &g_80;
    const uint64_t **l_502[8] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t **l_506 = &g_127;
    int32_t *l_508 = &g_237[0];
    int32_t *l_509[2][7][5] = {{{&g_64,&g_64,&g_64,&g_64,&g_64},{&g_71,(void*)0,&g_38,&g_64,&g_71},{(void*)0,&g_71,&g_64,&g_71,(void*)0},{&g_64,&g_38,&g_71,(void*)0,&g_71},{&g_71,&g_71,&g_71,&g_64,&g_64},{&g_71,&g_38,&g_64,&g_64,&g_71},{&g_64,&g_38,(void*)0,&g_71,&g_64}},{{(void*)0,&g_38,&g_38,(void*)0,&g_38},{&g_71,&g_71,&g_38,&g_64,&g_64},{&g_64,&g_38,(void*)0,&g_64,&g_38},{&g_38,&g_71,&g_64,&g_64,&g_71},{&g_71,(void*)0,&g_71,(void*)0,&g_71},{&g_38,&g_64,&g_71,&g_71,&g_38},{(void*)0,&g_71,&g_64,&g_64,&g_64}}};
    int16_t *l_519[3];
    int16_t **l_518 = &l_519[2];
    uint64_t l_530 = 0xD1D1D9E5E4C15ED0LL;
    uint32_t *l_533 = &g_140;
    uint32_t *l_536[6][1];
    int8_t *l_543[6][8] = {{(void*)0,&g_178,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{&g_66,&g_178,&g_66,&g_66,&g_66,(void*)0,&g_178,(void*)0},{&g_66,(void*)0,&g_178,&g_66,&g_178,&g_66,&g_66,&g_178},{&g_66,(void*)0,&g_66,(void*)0,&g_66,&g_66,(void*)0,&g_66},{&g_66,&g_66,&g_178,&g_66,(void*)0,(void*)0,&g_178,(void*)0},{(void*)0,&g_178,&g_66,(void*)0,(void*)0,&g_178,&g_178,(void*)0}};
    uint64_t l_544[6][1][10] = {{{18446744073709551615UL,0xAC502771DB6809A4LL,0x42D97CE08067BE08LL,0UL,0x1EEB7D162F49A33ELL,0x42D97CE08067BE08LL,0xE091EBE8FB2D6D63LL,0xEBF70F9A3BEB3094LL,0xAC502771DB6809A4LL,18446744073709551615UL}},{{18446744073709551615UL,0xD97ADD422439332ALL,0x1EEB7D162F49A33ELL,0x86F09D2878E20FFFLL,0x7AFD5BAC74D19237LL,0x42D97CE08067BE08LL,0x42D97CE08067BE08LL,0x7AFD5BAC74D19237LL,0x86F09D2878E20FFFLL,0x1EEB7D162F49A33ELL}},{{18446744073709551615UL,18446744073709551615UL,1UL,0x7AFD5BAC74D19237LL,0UL,1UL,0UL,18446744073709551615UL,0x6C67ADB3B678F5A3LL,0UL}},{{0xD97ADD422439332ALL,18446744073709551615UL,0x42D97CE08067BE08LL,18446744073709551615UL,18446744073709551615UL,0x86F09D2878E20FFFLL,0UL,0x86F09D2878E20FFFLL,18446744073709551615UL,18446744073709551615UL}},{{0xAC502771DB6809A4LL,18446744073709551615UL,0xAC502771DB6809A4LL,0x42D97CE08067BE08LL,0UL,0x1EEB7D162F49A33ELL,0x42D97CE08067BE08LL,0xE091EBE8FB2D6D63LL,0xEBF70F9A3BEB3094LL,0xAC502771DB6809A4LL}},{{0xE091EBE8FB2D6D63LL,0xD97ADD422439332ALL,0x6C67ADB3B678F5A3LL,0x7AFD5BAC74D19237LL,18446744073709551615UL,0x3891C05EF8C64C08LL,0xE091EBE8FB2D6D63LL,0xE091EBE8FB2D6D63LL,0x3891C05EF8C64C08LL,18446744073709551615UL}}};
    int32_t l_545 = 0x5FE45AD9L;
    uint16_t *l_546 = &g_212[1][6];
    uint16_t l_547[10][8][3] = {{{0x26D5L,0UL,0UL},{2UL,0xB2EFL,0x7627L},{65535UL,0x1204L,65534UL},{4UL,2UL,0x651EL},{0UL,65535UL,65534UL},{1UL,0xACBEL,65535UL},{65535UL,0UL,65528UL},{65529UL,0x5BF8L,1UL}},{{65535UL,65535UL,0xC539L},{65535UL,0UL,65535UL},{0x1A3DL,0xACBEL,65535UL},{0xB3B4L,65535UL,0xC539L},{1UL,0x3629L,1UL},{0xD51FL,0xB3B4L,65528UL},{0UL,0xD51FL,65535UL},{0x1A3DL,65535UL,65534UL}},{{65529UL,0UL,0x651EL},{0x1A3DL,0x3629L,65535UL},{0UL,65529UL,0xC539L},{0xD51FL,65535UL,65535UL},{1UL,0xB3B4L,0xACBEL},{0xB3B4L,0x5BF8L,0x602AL},{0x1A3DL,0x5BF8L,65529UL},{65535UL,0xB3B4L,0x651EL}},{{65535UL,65535UL,65535UL},{65529UL,65529UL,65534UL},{65535UL,0x3629L,0x602AL},{1UL,0UL,0x1A3DL},{0UL,65535UL,0x602AL},{65535UL,0xD51FL,65534UL},{0x7627L,0xB3B4L,65535UL},{0xACBEL,0x3629L,0x651EL}},{{65529UL,65535UL,65529UL},{0xD51FL,0xACBEL,0x602AL},{0xD51FL,0UL,0xACBEL},{65529UL,65535UL,65535UL},{0xACBEL,0x5BF8L,0xC539L},{0x7627L,0UL,65535UL},{65535UL,0xACBEL,0x651EL},{0UL,65535UL,65534UL}},{{1UL,0xACBEL,65535UL},{65535UL,0UL,65528UL},{65529UL,0x5BF8L,1UL},{65535UL,65535UL,0xC539L},{65535UL,0UL,65535UL},{0x1A3DL,0xACBEL,65535UL},{0xB3B4L,65535UL,0xC539L},{1UL,0x3629L,1UL}},{{0xD51FL,0xB3B4L,65528UL},{0UL,0xD51FL,65535UL},{0x1A3DL,65535UL,65534UL},{65529UL,0UL,0x651EL},{0x1A3DL,0x3629L,65535UL},{0UL,65529UL,0xC539L},{0xD51FL,65535UL,65535UL},{1UL,0xB3B4L,0xACBEL}},{{0xB3B4L,0x5BF8L,0x602AL},{0x1A3DL,0x5BF8L,65529UL},{65535UL,0xB3B4L,0x651EL},{65535UL,65535UL,65535UL},{65529UL,65529UL,65534UL},{65535UL,0x3629L,0x602AL},{1UL,0UL,0x1A3DL},{0UL,65535UL,0x602AL}},{{65535UL,0xD51FL,65534UL},{0x7627L,0xB3B4L,65535UL},{0xACBEL,0x3629L,0x651EL},{65529UL,0xE9D2L,0x651EL},{65529UL,0x5CC6L,65535UL},{65529UL,65535UL,0x5CC6L},{0x602AL,0x7627L,0x7627L},{0x5CC6L,65535UL,4UL}},{{0xC539L,0xD51FL,65528UL},{0x26D5L,0x5CC6L,0x3629L},{65535UL,65534UL,65535UL},{2UL,0x5CC6L,0x7627L},{0x7627L,0xD51FL,0x2F4BL},{0x602AL,65535UL,2UL},{0x26D5L,0x7627L,4UL},{65534UL,65535UL,0x1A3DL}}};
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_519[i] = &g_153;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
            l_536[i][j] = (void*)0;
    }
    for (g_80 = 2; (g_80 >= 60); g_80 = safe_add_func_int8_t_s_s(g_80, 3))
    { /* block id: 245 */
        uint8_t l_498 = 1UL;
        int32_t **l_501 = &g_127;
        const uint64_t ***l_504 = (void*)0;
        const uint64_t ***l_505[4];
        int32_t *l_507 = &g_71;
        int i;
        for (i = 0; i < 4; i++)
            l_505[i] = &l_502[7];
        if (l_498)
            break;
        (*l_507) |= (((safe_rshift_func_int8_t_s_s(0xFBL, 7)) != (l_501 == ((((l_502[2] = l_502[2]) == &g_102[4][4]) != g_212[0][8]) , l_506))) | p_76);
        return g_71;
    }
    g_71 &= ((*l_508) = g_115);
    for (g_140 = (-4); (g_140 <= 41); g_140 = safe_add_func_uint16_t_u_u(g_140, 1))
    { /* block id: 255 */
        int16_t *l_517 = &g_153;
        int16_t * const * const l_516 = &l_517;
        uint64_t ** const l_520 = &g_102[8][3];
        uint64_t **l_521[3];
        uint64_t ***l_522 = (void*)0;
        uint64_t ***l_523 = &l_521[1];
        int32_t l_524 = 9L;
        int64_t *l_525 = &g_210;
        int32_t l_526 = 0xDC1C8557L;
        int i;
        for (i = 0; i < 3; i++)
            l_521[i] = &g_102[8][3];
        l_526 &= ((((safe_rshift_func_uint16_t_u_s(g_208[0][4][1], ((safe_add_func_int32_t_s_s((l_516 == (p_77 , l_518)), (((*l_508) |= ((((l_520 == ((*l_523) = l_521[0])) | (((*l_525) = (p_77 | (p_77 , (p_77 != l_524)))) & 2L)) && (-6L)) , g_153)) < g_212[1][6]))) == 0xCCL))) > 0x5A35DB8D120CC5D3LL) | 0UL) , p_76);
        return g_212[1][6];
    }
    g_71 = (safe_rshift_func_int16_t_s_u(3L, ((!(*l_508)) , (((((((**l_518) ^= l_530) | (((((*l_546) = (((safe_lshift_func_uint16_t_u_s(((((p_77 = ((*l_533)++)) && ((4L > p_75) | (safe_lshift_func_int16_t_s_s((p_76 && (safe_add_func_int32_t_s_s((safe_add_func_uint64_t_u_u((g_59 != p_75), (((*l_508) = (g_66 = 1L)) , l_544[0][0][9]))), l_545))), 9)))) , p_75) , g_38), g_208[2][2][2])) == g_327) < p_76)) > (-5L)) > 65535UL) < 1UL)) && l_547[1][0][0]) , p_75) , g_166) <= p_76))));
    return p_75;
}


/* ------------------------------------------ */
/* 
 * reads : g_66 g_64 g_115 g_5 g_71 g_140 g_166 g_153 g_178 g_103 g_127 g_38 g_210 g_212 g_208 g_235 g_237 g_327
 * writes: g_102 g_115 g_71 g_127 g_103 g_140 g_66 g_166 g_153 g_178 g_208 g_210 g_212 g_235 g_237 g_327 g_396
 */
static uint16_t  func_83(uint64_t  p_84, int64_t  p_85, int8_t * p_86, int64_t  p_87, uint16_t  p_88)
{ /* block id: 20 */
    uint32_t l_96[7][9][4] = {{{18446744073709551607UL,0x4763CD51L,0x84F79AABL,8UL},{1UL,0x5B2DFF54L,18446744073709551615UL,8UL},{0x787D1A38L,0x4763CD51L,0x2CFEAA3CL,1UL},{6UL,0x97376B24L,18446744073709551608UL,18446744073709551611UL},{0UL,1UL,6UL,18446744073709551606UL},{0xC99B0BF1L,1UL,18446744073709551611UL,0UL},{18446744073709551609UL,0x9D1BBA4BL,0UL,0xC99B0BF1L},{0x4763CD51L,1UL,0xF7C4FBB7L,18446744073709551608UL},{8UL,0x3FFCD553L,0UL,0x3FFCD553L}},{{5UL,0xC99B0BF1L,0x3FFCD553L,1UL},{1UL,18446744073709551611UL,18446744073709551606UL,0x4763CD51L},{18446744073709551615UL,18446744073709551615UL,0x97376B24L,1UL},{18446744073709551615UL,0xF359A124L,18446744073709551606UL,0x84F79AABL},{1UL,1UL,0x3FFCD553L,18446744073709551613UL},{5UL,18446744073709551606UL,0UL,18446744073709551609UL},{8UL,18446744073709551609UL,0xF7C4FBB7L,0xF359A124L},{0x4763CD51L,0xF7C4FBB7L,0UL,5UL},{18446744073709551609UL,0xEDA29731L,18446744073709551611UL,0x085588CBL}},{{0xC99B0BF1L,0x84F79AABL,6UL,6UL},{0UL,0UL,18446744073709551608UL,0x427032CDL},{6UL,18446744073709551615UL,0x2CFEAA3CL,0x97376B24L},{0x787D1A38L,18446744073709551607UL,18446744073709551615UL,0x2CFEAA3CL},{1UL,18446744073709551607UL,0x84F79AABL,0x97376B24L},{18446744073709551607UL,18446744073709551615UL,0x9D1BBA4BL,0x427032CDL},{0UL,0UL,1UL,6UL},{18446744073709551611UL,0x84F79AABL,1UL,0x085588CBL},{18446744073709551608UL,0xEDA29731L,1UL,5UL}},{{1UL,0xF7C4FBB7L,0UL,0xF359A124L},{0x427032CDL,18446744073709551609UL,1UL,18446744073709551609UL},{0x5B2DFF54L,18446744073709551606UL,0x5B2DFF54L,18446744073709551613UL},{1UL,1UL,0UL,0x84F79AABL},{18446744073709551615UL,0xF359A124L,18446744073709551608UL,1UL},{18446744073709551609UL,18446744073709551615UL,18446744073709551608UL,0x4763CD51L},{18446744073709551615UL,18446744073709551611UL,0UL,1UL},{1UL,0xC99B0BF1L,0x5B2DFF54L,0x3FFCD553L},{0x5B2DFF54L,0x3FFCD553L,1UL,18446744073709551608UL}},{{0x427032CDL,1UL,0UL,0xC99B0BF1L},{1UL,0x9D1BBA4BL,1UL,0UL},{0UL,1UL,18446744073709551613UL,1UL},{0x787D1A38L,18446744073709551611UL,18446744073709551611UL,0x787D1A38L},{0x97376B24L,18446744073709551608UL,0UL,18446744073709551613UL},{1UL,18446744073709551609UL,18446744073709551615UL,1UL},{18446744073709551611UL,18446744073709551607UL,6UL,1UL},{0xF7C4FBB7L,18446744073709551609UL,8UL,18446744073709551613UL},{0x84F79AABL,18446744073709551608UL,0UL,0x787D1A38L}},{{18446744073709551608UL,18446744073709551611UL,0x84F79AABL,1UL},{0UL,1UL,0x787D1A38L,0x2CFEAA3CL},{0x4763CD51L,0UL,0x085588CBL,0UL},{18446744073709551609UL,18446744073709551613UL,0xEDA29731L,0x9D1BBA4BL},{1UL,0xF359A124L,0x2CFEAA3CL,0xF359A124L},{18446744073709551615UL,0UL,0xF359A124L,0UL},{0UL,0x787D1A38L,1UL,18446744073709551609UL},{6UL,0x3FFCD553L,18446744073709551608UL,18446744073709551609UL},{6UL,0UL,1UL,18446744073709551615UL}},{{0UL,18446744073709551609UL,0xF359A124L,1UL},{18446744073709551615UL,1UL,0x2CFEAA3CL,0x4763CD51L},{1UL,18446744073709551615UL,0xEDA29731L,0UL},{18446744073709551609UL,0xEDA29731L,0x085588CBL,18446744073709551615UL},{0x4763CD51L,0x5B2DFF54L,0x787D1A38L,1UL},{0UL,18446744073709551615UL,0x84F79AABL,0x84F79AABL},{18446744073709551608UL,18446744073709551608UL,0UL,1UL},{0x84F79AABL,0xC99B0BF1L,8UL,18446744073709551608UL},{0xF7C4FBB7L,1UL,6UL,8UL}}};
    uint64_t *l_112[2][7][5] = {{{&g_103,&g_103,&g_103,&g_103,(void*)0},{&g_103,&g_103,(void*)0,&g_59,&g_103},{&g_59,&g_59,(void*)0,&g_103,&g_103},{&g_103,(void*)0,&g_103,&g_59,&g_103},{(void*)0,&g_103,&g_59,&g_103,&g_103},{(void*)0,&g_59,&g_59,(void*)0,&g_59},{&g_59,&g_59,&g_59,&g_103,&g_59}},{{&g_59,&g_59,&g_59,&g_103,&g_103},{&g_103,&g_103,&g_103,(void*)0,&g_103},{&g_103,&g_59,(void*)0,&g_103,&g_103},{(void*)0,&g_103,(void*)0,&g_103,&g_59},{&g_59,&g_59,&g_103,&g_59,&g_103},{&g_103,&g_59,&g_103,&g_59,&g_59},{&g_103,&g_59,&g_103,&g_59,&g_103}}};
    uint64_t **l_111[3];
    int32_t l_123 = 0x02EA2108L;
    int32_t *l_145 = &g_71;
    int32_t l_150 = 0L;
    int32_t l_152 = 0xBF5F1DF6L;
    int32_t l_197 = 1L;
    int32_t l_280 = 0x493D74D1L;
    int32_t l_290 = 0xA0B34B94L;
    int32_t l_292 = 1L;
    int32_t l_294 = 7L;
    int32_t l_296 = 0x4C9D8C37L;
    int32_t l_298 = 0x64EA76DCL;
    int8_t *l_385[4][6][4] = {{{&g_178,&g_66,(void*)0,&g_178},{&g_178,&g_178,&g_66,&g_178},{&g_66,&g_66,&g_178,&g_66},{&g_178,&g_66,(void*)0,&g_66},{(void*)0,(void*)0,&g_66,&g_178},{&g_178,&g_178,&g_66,&g_178}},{{&g_66,(void*)0,&g_178,&g_178},{&g_178,&g_178,&g_178,&g_66},{&g_178,&g_178,&g_66,&g_66},{&g_178,&g_66,&g_178,&g_66},{&g_66,&g_66,&g_178,&g_66},{&g_66,&g_178,&g_66,&g_66}},{{(void*)0,&g_178,&g_66,&g_178},{&g_178,(void*)0,&g_66,&g_178},{&g_66,&g_178,&g_66,&g_66},{&g_178,&g_66,&g_66,&g_178},{(void*)0,&g_66,&g_66,(void*)0},{&g_66,(void*)0,&g_178,&g_178}},{{&g_66,&g_66,&g_178,&g_178},{&g_178,(void*)0,&g_66,(void*)0},{&g_178,&g_66,&g_178,&g_178},{&g_178,&g_66,&g_178,&g_66},{&g_66,&g_178,&g_66,&g_178},{&g_66,(void*)0,&g_178,&g_178}}};
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_111[i] = &l_112[0][6][4];
lbl_403:
    for (p_85 = 0; (p_85 <= 13); ++p_85)
    { /* block id: 23 */
        const int64_t l_98 = (-1L);
        uint64_t * const *l_109 = (void*)0;
        int32_t *l_119 = &g_64;
        uint16_t l_133 = 1UL;
        int32_t l_147[9] = {7L,7L,7L,7L,7L,7L,7L,7L,7L};
        int16_t l_155 = (-4L);
        int32_t l_236 = 0x57C9E646L;
        uint8_t *l_360 = &g_166;
        uint32_t *l_372 = &g_140;
        int32_t *l_375 = &l_292;
        int32_t **l_376 = &g_127;
        int i;
        for (p_84 = 0; (p_84 <= 3); p_84 += 1)
        { /* block id: 26 */
            uint64_t *l_101 = &g_59;
            uint64_t **l_100[5][2] = {{&l_101,&l_101},{&l_101,&l_101},{&l_101,&l_101},{&l_101,&l_101},{&l_101,&l_101}};
            int32_t l_110 = 9L;
            uint16_t *l_114 = &g_115;
            int32_t l_116 = 0xA51D28A6L;
            int i, j;
            if (((((void*)0 != &g_66) == (((!((l_98 , ((~((*l_114) |= (((l_98 , 9L) != ((g_102[8][3] = (void*)0) == ((p_85 == (l_110 = (func_104(((((safe_div_func_int8_t_s_s((l_109 != (l_110 , l_111[1])), l_98)) && p_85) >= 0x0D01L) && g_66), l_98) > 0UL))) , &p_84))) && 1UL))) == l_116)) & p_85)) , g_5) , 5UL)) ^ 0x0AL))
            { /* block id: 33 */
                int32_t *l_117 = (void*)0;
                int32_t *l_118 = &l_116;
                (*l_118) &= 0x53116D36L;
            }
            else
            { /* block id: 35 */
                int32_t **l_120 = &l_119;
                (*l_120) = l_119;
                l_123 = (safe_lshift_func_uint8_t_u_u(6UL, g_115));
                for (l_123 = 1; (l_123 >= 0); l_123 -= 1)
                { /* block id: 40 */
                    int i, j, k;
                    for (g_115 = 0; (g_115 <= 3); g_115 += 1)
                    { /* block id: 43 */
                        int32_t *l_124[4][2] = {{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0},{(void*)0,(void*)0}};
                        int i, j, k;
                        g_71 ^= l_96[(p_84 + 2)][(p_84 + 3)][p_84];
                    }
                    return l_96[l_123][(l_123 + 1)][p_84];
                }
            }
            for (p_87 = 0; (p_87 <= 3); p_87 += 1)
            { /* block id: 51 */
                for (g_115 = 0; (g_115 <= 3); g_115 += 1)
                { /* block id: 54 */
                    int32_t *l_125 = &l_110;
                    int32_t *l_128[5][8][1] = {{{&g_71},{&l_123},{&g_64},{&l_116},{&g_71},{&l_123},{&g_71},{&l_123}},{{&g_71},{&l_116},{&g_64},{&l_123},{&g_71},{&l_123},{&l_116},{&l_116}},{{&l_116},{&l_123},{&g_71},{&l_123},{&g_64},{&l_116},{&g_71},{&l_123}},{{&g_71},{&l_123},{&g_71},{&l_116},{&g_64},{&l_123},{&g_71},{&l_123}},{{&l_116},{&l_116},{&l_116},{&l_123},{&g_71},{&l_123},{&g_64},{&l_116}}};
                    int i, j, k;
                    for (p_88 = 0; (p_88 <= 3); p_88 += 1)
                    { /* block id: 57 */
                        int32_t **l_126[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                        int i;
                        l_128[0][7][0] = (g_127 = l_125);
                    }
                }
                for (g_103 = 0; (g_103 <= 3); g_103 += 1)
                { /* block id: 64 */
                    const int32_t *l_130 = &g_38;
                    const int32_t **l_129 = &l_130;
                    int32_t *l_131 = &l_116;
                    int32_t *l_132[6][5][3] = {{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0}}};
                    int i, j, k;
                    (*l_129) = (void*)0;
                    --l_133;
                    (*l_129) = &g_38;
                }
                for (g_115 = 0; (g_115 <= 3); g_115 += 1)
                { /* block id: 71 */
                    int32_t *l_136 = &l_123;
                    int32_t *l_137 = &l_123;
                    int32_t *l_138 = &l_123;
                    int32_t *l_139[6][8][2] = {{{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110},{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110}},{{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110},{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110}},{{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110},{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110}},{{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110},{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110}},{{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110},{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110}},{{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110},{&l_116,&g_71},{&l_116,&l_110},{&l_110,&g_71},{&l_110,&l_110}}};
                    int i, j, k;
                    g_140++;
                    return l_96[(p_84 + 1)][(p_84 + 4)][g_115];
                }
            }
        }
        for (l_133 = 0; (l_133 <= 1); l_133 += 1)
        { /* block id: 79 */
            uint32_t l_143 = 7UL;
            int32_t **l_144[5] = {&l_119,&l_119,&l_119,&l_119,&l_119};
            uint16_t *l_170[9][2];
            int32_t l_171[3];
            uint64_t *l_201 = &g_59;
            uint32_t *l_279 = &g_140;
            int32_t l_347[1];
            int i, j;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 2; j++)
                    l_170[i][j] = (void*)0;
            }
            for (i = 0; i < 3; i++)
                l_171[i] = 0xF86D33B1L;
            for (i = 0; i < 1; i++)
                l_347[i] = (-1L);
            if (l_143)
                break;
            l_145 = &g_64;
            for (g_115 = 0; (g_115 <= 1); g_115 += 1)
            { /* block id: 84 */
                int64_t l_146 = 0L;
                int32_t l_176 = (-9L);
                const uint8_t l_186 = 0x7BL;
                for (g_71 = 0; (g_71 <= 1); g_71 += 1)
                { /* block id: 87 */
                    return p_84;
                }
                l_147[3] ^= l_146;
                if (((safe_rshift_func_int16_t_s_u(g_115, 1)) != g_115))
                { /* block id: 91 */
                    int8_t l_151 = 0x87L;
                    int32_t l_154 = 7L;
                    int32_t l_156 = 9L;
                    for (g_66 = 0; (g_66 <= 1); g_66 += 1)
                    { /* block id: 94 */
                        uint64_t l_157 = 0x0E55F7E0F154065CLL;
                        l_157--;
                    }
                }
                else
                { /* block id: 97 */
                    uint8_t *l_164 = (void*)0;
                    uint8_t *l_165 = &g_166;
                    int16_t *l_169 = &g_153;
                    int32_t l_179[5][8] = {{0x609C6297L,0x80768EFDL,0x4B92E75AL,0x4B92E75AL,0x80768EFDL,0x609C6297L,0x80768EFDL,0x4B92E75AL},{0xF769B8DEL,0x80768EFDL,0xF769B8DEL,0x609C6297L,0x609C6297L,0xF769B8DEL,0x80768EFDL,0xF769B8DEL},{0xEB9ED40CL,0x609C6297L,0x4B92E75AL,0x609C6297L,0xEB9ED40CL,0xEB9ED40CL,0x609C6297L,0x4B92E75AL},{0xEB9ED40CL,0xEB9ED40CL,0x609C6297L,0x4B92E75AL,0x609C6297L,0xEB9ED40CL,0xEB9ED40CL,0x609C6297L},{0xF769B8DEL,0x609C6297L,0x609C6297L,0xF769B8DEL,0x80768EFDL,0xF769B8DEL,0x609C6297L,0x609C6297L}};
                    int i, j;
                    l_171[1] = (safe_rshift_func_int16_t_s_s(((*l_169) ^= ((safe_mul_func_uint8_t_u_u(((*l_165)++), 0xE9L)) | 0xC1L)), (((void*)0 == l_170[6][0]) || l_146)));
                    for (p_88 = 0; (p_88 <= 1); p_88 += 1)
                    { /* block id: 103 */
                        const int16_t *l_172 = &l_155;
                        int8_t *l_177 = &g_178;
                        int32_t l_187 = 0xAD064898L;
                        int i, j, k;
                        l_187 |= ((l_172 == &g_153) && (((!(((((safe_rshift_func_uint8_t_u_u((p_85 ^ (l_179[4][6] = ((*l_119) == (((*l_177) ^= ((*p_86) = l_176)) || ((*l_165) = 0x94L))))), 4)) | ((*l_169) |= (safe_sub_func_uint32_t_u_u((*l_145), ((safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u(((-5L) <= p_85), (*l_145))), 18446744073709551611UL)) , (-1L)))))) < 0x318FL) , l_186) <= g_71)) > 0x31FEL) > p_88));
                    }
                    g_127 = &g_38;
                    g_71 = (l_186 ^ ((safe_unary_minus_func_uint32_t_u((safe_mul_func_uint16_t_u_u(((void*)0 != &p_84), g_103)))) >= (((l_144[0] != ((((safe_lshift_func_int16_t_s_s((g_166 , ((p_85 && ((*l_169) |= (safe_lshift_func_uint16_t_u_u((((((safe_rshift_func_uint8_t_u_u(p_88, p_84)) != g_66) , g_178) <= (*g_127)) , p_88), g_166)))) || (*p_86))), p_87)) >= (*l_145)) ^ (*l_145)) , l_144[2])) ^ 1L) && (*g_127))));
                }
                l_145 = &l_147[3];
            }
            for (l_143 = 0; (l_143 <= 1); l_143 += 1)
            { /* block id: 119 */
                uint16_t l_198[1];
                int i;
                for (i = 0; i < 1; i++)
                    l_198[i] = 0x8820L;
                l_198[0]++;
                return l_198[0];
            }
            for (g_71 = 0; (g_71 <= 1); g_71 += 1)
            { /* block id: 125 */
                uint8_t l_209[6][5] = {{0xC5L,255UL,252UL,248UL,5UL},{0xE0L,255UL,248UL,0xBFL,0xDBL},{0xA5L,255UL,255UL,0xA5L,0xBFL},{0xE0L,0xC5L,5UL,0x04L,0xBFL},{255UL,0xE0L,0xDBL,252UL,0xDBL},{252UL,252UL,0xBFL,0x04L,5UL}};
                int64_t l_211 = 0L;
                int16_t l_246 = 0xE831L;
                int32_t l_247 = (-8L);
                int32_t l_284 = 0xA2D12A1FL;
                int32_t l_289 = (-4L);
                int32_t l_291 = 8L;
                int32_t l_293 = 0L;
                int32_t l_295[4] = {0x5C2C58BBL,0x5C2C58BBL,0x5C2C58BBL,0x5C2C58BBL};
                int i, j, k;
                for (l_152 = 0; (l_152 <= 1); l_152 += 1)
                { /* block id: 128 */
                    uint32_t l_204[9] = {0UL,0x20666721L,0UL,0x20666721L,0UL,0x20666721L,0UL,0x20666721L,0UL};
                    int8_t *l_234 = (void*)0;
                    int16_t *l_283[9][6] = {{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155},{&l_155,&g_153,&l_155,&l_155,&g_153,&l_155}};
                    uint8_t *l_285 = &g_166;
                    int32_t l_286 = 0L;
                    int32_t l_287 = 0x94C9F594L;
                    int32_t l_297[1][9][9] = {{{(-3L),0xA71C66B8L,0xA71C66B8L,(-3L),1L,(-3L),0xA71C66B8L,0xA71C66B8L,(-3L)},{0L,0xA71C66B8L,0x00648590L,0xA71C66B8L,0L,0L,0xA71C66B8L,0x00648590L,0xA71C66B8L},{0xA71C66B8L,1L,0x00648590L,0x00648590L,1L,0xA71C66B8L,1L,0x00648590L,0x00648590L},{0L,0L,0xA71C66B8L,0x00648590L,0xA71C66B8L,0L,0L,0xA71C66B8L,0x00648590L},{(-3L),1L,(-3L),0xA71C66B8L,0xA71C66B8L,(-3L),1L,(-3L),0xA71C66B8L},{(-3L),0xA71C66B8L,0xA71C66B8L,(-3L),1L,(-3L),0xA71C66B8L,0xA71C66B8L,(-3L)},{0L,0xA71C66B8L,0x00648590L,0xA71C66B8L,0L,0L,0xA71C66B8L,0x00648590L,0xA71C66B8L},{0xA71C66B8L,1L,0x00648590L,0x00648590L,1L,0xA71C66B8L,1L,0x00648590L,0x00648590L},{0L,0L,0xA71C66B8L,0x00648590L,0xA71C66B8L,0L,0L,0xA71C66B8L,0x00648590L}}};
                    uint32_t l_299 = 1UL;
                    int32_t ** const l_329 = &l_119;
                    int i, j, k;
                    for (p_88 = 0; (p_88 <= 8); p_88 += 1)
                    { /* block id: 131 */
                        int64_t *l_207 = (void*)0;
                        int i, j, k;
                        if (l_96[(l_133 + 3)][(l_152 + 7)][g_71])
                            break;
                        g_210 |= (l_147[p_88] = ((((void*)0 != l_201) , (((-1L) | (((-1L) & (p_84 || (safe_mod_func_uint16_t_u_u(((((p_85 , ((l_204[7] < ((safe_rshift_func_int16_t_s_s(((g_208[2][2][2] = ((l_112[l_133][(l_152 + 1)][(l_133 + 2)] = &g_103) != &g_103)) , 0x53D1L), (*l_145))) >= p_85)) ^ g_38)) >= (-1L)) | p_85) | g_66), g_64)))) , p_85)) | l_209[4][0])) ^ 255UL));
                        if (l_147[p_88])
                            continue;
                        g_212[1][6] = l_211;
                    }
                    if ((safe_add_func_int32_t_s_s((g_237[0] = (safe_div_func_uint8_t_u_u(((((void*)0 != p_86) > (((((((safe_mul_func_uint16_t_u_u((((safe_rshift_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(0x2AL, (safe_div_func_uint8_t_u_u((0x7C92A14779510EEALL < (l_236 &= ((((safe_mul_func_int8_t_s_s((!((*l_119) & 0x0B449B74578E65A7LL)), (!(g_235 = ((safe_add_func_uint32_t_u_u(((safe_unary_minus_func_int16_t_s(p_88)) > p_88), (((safe_div_func_uint32_t_u_u((((p_87 &= (&g_178 == l_234)) || (*l_145)) != g_212[1][6]), p_85)) & (*l_119)) , g_212[1][6]))) , l_209[4][0]))))) ^ (*p_86)) != g_208[4][6][0]) < 0x6DF01C17L))), 255UL)))), p_84)) == l_204[4]) && p_88), 0x97C5L)) ^ (*l_145)) & g_208[2][2][2]) , l_204[7]) <= g_66) , g_71) | 247UL)) & 4294967293UL), g_71))), 0xC44B1F67L)))
                    { /* block id: 144 */
                        uint32_t *l_242 = (void*)0;
                        uint32_t *l_243 = (void*)0;
                        uint32_t *l_244[2];
                        int i;
                        for (i = 0; i < 2; i++)
                            l_244[i] = &g_140;
                        l_123 = (safe_rshift_func_uint16_t_u_s((((g_140 = (safe_mul_func_int16_t_s_s(p_87, l_211))) != p_85) >= ((g_115 &= (~p_87)) == g_235)), 2));
                    }
                    else
                    { /* block id: 148 */
                        int8_t l_251 = 0x18L;
                        int16_t *l_268 = &g_153;
                        uint64_t l_269 = 0x53953203D1BC8D61LL;
                        int32_t l_270 = 0xFF59F603L;
                        l_247 = ((-2L) <= l_246);
                        l_270 ^= (safe_mul_func_uint8_t_u_u(((((p_84 = (!(((l_251 | (p_88 < (safe_lshift_func_int8_t_s_u((+(p_84 , (((+(safe_mul_func_int8_t_s_s((safe_mod_func_uint16_t_u_u(((safe_rshift_func_uint8_t_u_u(((~((*l_268) = (((safe_rshift_func_int16_t_s_s((((p_87 && p_84) , (p_87 = ((*l_145) , (!g_66)))) < 0x12B37BC6C639F2A2LL), 4)) < ((safe_add_func_uint32_t_u_u(g_166, l_204[7])) && 0xE8L)) < l_209[5][0]))) < g_140), 0)) < l_204[7]), l_269)), g_140))) > g_237[2]) , 7UL))), 2)))) , (*p_86)) , p_88))) && g_153) , &l_145) != (void*)0), g_178));
                    }
                    if ((safe_lshift_func_uint8_t_u_u(((((*l_145) && ((*l_285) = (safe_sub_func_int64_t_s_s((safe_rshift_func_int8_t_s_s((p_86 != &g_66), (safe_mod_func_uint32_t_u_u((l_279 != l_279), (((*l_119) > (l_284 = ((g_140 < l_280) < (l_247 = ((safe_sub_func_int16_t_s_s((g_153 = (p_88 ^ g_64)), g_166)) , p_84))))) ^ (*p_86)))))), p_84)))) > g_71) >= g_212[1][6]), g_5)))
                    { /* block id: 159 */
                        int32_t l_288[2][10][7] = {{{0x876024A5L,1L,0xC8C0BE4AL,(-10L),0x6C9A34EFL,9L,1L},{1L,2L,(-8L),0x3F771F14L,9L,1L,0x4A8FA5A9L},{0xE5EE3B36L,(-2L),9L,0L,(-2L),0L,9L},{1L,1L,0L,9L,0x7265DA1EL,0x2AD69AD4L,8L},{(-1L),0x327A7D23L,0x81858CDBL,0x876024A5L,(-1L),0x6C9A34EFL,(-2L)},{7L,0L,(-8L),0L,0x7265DA1EL,0x55292B81L,0L},{9L,(-2L),0xC322C723L,0xC322C723L,(-2L),9L,0x36E7A10DL},{0x55292B81L,0x7265DA1EL,0L,(-8L),0L,7L,0xEC37B445L},{0x6C9A34EFL,(-1L),0x876024A5L,0x81858CDBL,0x327A7D23L,(-1L),(-10L)},{0x2AD69AD4L,0x7265DA1EL,9L,0L,1L,1L,0L}},{{0L,(-2L),0L,9L,(-2L),0xE5EE3B36L,(-2L)},{0L,0L,0L,0xE48A2C21L,5L,0L,0x1D25D300L},{(-1L),0x327A7D23L,(-2L),0L,0xFDA7F21EL,0xE5EE3B36L,0x876024A5L},{8L,1L,0xCF1FFD86L,0x4A8FA5A9L,0xCF1FFD86L,1L,8L},{(-10L),(-2L),7L,0x327A7D23L,(-1L),(-1L),0xC8C0BE4AL},{0x3EFC6D46L,5L,(-8L),0x7265DA1EL,0L,7L,5L},{9L,0xFDA7F21EL,7L,0x516C3827L,6L,9L,6L},{(-8L),0xCF1FFD86L,0xCF1FFD86L,(-8L),0x2E8EBB75L,0x55292B81L,0L},{0xAB43CCFAL,(-1L),(-2L),1L,9L,0x6C9A34EFL,(-10L)},{0x765BA502L,0L,0L,0xEC37B445L,1L,0x2AD69AD4L,0L}}};
                        uint16_t *l_302 = &g_212[1][6];
                        uint32_t *l_306[8] = {&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140,&g_140};
                        int16_t **l_321 = (void*)0;
                        int16_t **l_322 = (void*)0;
                        int16_t **l_323 = &l_283[7][3];
                        int32_t l_328 = 0x33847FF0L;
                        int i, j, k;
                        l_299--;
                        l_298 = ((((l_302 != &g_115) || (~p_88)) > (((p_84 & g_140) < ((safe_mod_func_uint32_t_u_u(((g_237[0] , l_306[3]) == (void*)0), (safe_add_func_uint32_t_u_u((l_288[0][1][2] ^= (safe_rshift_func_uint16_t_u_u(0x107DL, 1))), 0xA5F03214L)))) == (*l_145))) < g_71)) != 0x84BE0C722BE9EF24LL);
                        l_288[0][1][2] = (safe_mod_func_uint32_t_u_u((((l_328 |= (safe_rshift_func_uint16_t_u_u((p_87 == (g_212[0][0] >= ((((*l_145) , ((g_327 = ((*p_86) & ((((safe_rshift_func_int16_t_s_s(0xFC6AL, (safe_lshift_func_int16_t_s_s((p_88 , (((*l_323) = &g_153) == &g_153)), 7)))) , (((safe_mul_func_uint16_t_u_u((+((p_85 , p_88) | 0x7FE1CBAAL)), 0x6299L)) || (-1L)) ^ p_85)) , p_85) , l_288[0][3][1]))) , g_208[7][1][2])) && g_210) == p_87))), 9))) , l_329) == &g_127), 4294967295UL));
                    }
                    else
                    { /* block id: 167 */
                        uint16_t l_330 = 0xBE60L;
                        --l_330;
                    }
                }
                l_145 = &g_38;
                l_291 = (l_298 = (1UL & (safe_div_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((((l_201 != (l_112[l_133][l_133][(g_71 + 1)] = l_201)) & (safe_rshift_func_int8_t_s_s((((0x65L <= (safe_add_func_uint16_t_u_u((p_88 = (((void*)0 == &g_153) >= ((p_87 = (p_85 || (((((p_84 = (((safe_add_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((((*p_86) ^= 0x55L) | (&g_127 == &g_127)), g_153)), (*l_145))) && l_347[0]), g_140)) | p_88) || 250UL)) > g_237[0]) , p_87) || 0x75D0AAF9E480F206LL) >= l_289))) ^ (*l_145)))), (*l_119)))) , p_87) == (*l_145)), 1))) > p_85) >= p_85), 0xCD97L)), 0x43L))));
            }
        }
    }
    if ((safe_rshift_func_int16_t_s_u(g_210, (&p_84 != (g_102[8][3] = &p_84)))))
    { /* block id: 188 */
        int32_t **l_379 = &l_145;
        uint8_t *l_382 = &g_166;
        int32_t l_383 = (-1L);
        uint16_t *l_384[6] = {&g_212[1][6],&g_212[1][6],&g_212[1][6],&g_212[1][6],&g_212[1][6],&g_212[1][6]};
        int32_t l_487 = 0xCE9B650AL;
        int i;
        (*l_379) = (void*)0;
        if ((((safe_lshift_func_int16_t_s_s(((g_212[1][7] , l_382) != l_382), (l_383 <= ((g_115 |= 0xD1F3L) && g_212[3][8])))) && p_87) > ((&g_66 != l_385[3][0][3]) > p_85)))
        { /* block id: 191 */
            int32_t ***l_395[5];
            int16_t l_448 = 0xD963L;
            int32_t l_481[6];
            int i;
            for (i = 0; i < 5; i++)
                l_395[i] = (void*)0;
            for (i = 0; i < 6; i++)
                l_481[i] = 0x2E58FA03L;
            if ((safe_sub_func_uint16_t_u_u((((-1L) || (safe_sub_func_int8_t_s_s(((*p_86) = ((safe_sub_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u(((((&g_38 != ((*l_379) = (*l_379))) && ((+((g_396 = &l_145) == &l_145)) <= (p_84 | (safe_mod_func_uint32_t_u_u((((*l_379) = (((safe_add_func_int64_t_s_s(((void*)0 != &p_85), (safe_rshift_func_int8_t_s_s(((void*)0 != &g_210), g_5)))) & g_103) , (void*)0)) != &l_298), p_87))))) , g_210) >= p_87), 12)) , g_38), 0xD9L)) < g_208[2][2][2])), (-3L)))) & l_298), g_235)))
            { /* block id: 196 */
                if (g_71)
                    goto lbl_403;
            }
            else
            { /* block id: 198 */
                const uint16_t l_422 = 65528UL;
                int32_t l_427 = 0x96EF45B0L;
                uint32_t l_438 = 0x032137EBL;
                int32_t **l_464 = &g_127;
                for (l_280 = 0; (l_280 < (-20)); --l_280)
                { /* block id: 201 */
                    int64_t l_425 = 0L;
                    int32_t **l_426 = &g_127;
                    int32_t l_428 = (-1L);
                    for (l_123 = 15; (l_123 < (-26)); l_123 = safe_sub_func_int16_t_s_s(l_123, 7))
                    { /* block id: 204 */
                        uint32_t *l_429 = &g_140;
                        int32_t l_447 = (-1L);
                        l_292 = (safe_mul_func_uint16_t_u_u(((safe_add_func_uint32_t_u_u(0x5EE46925L, ((*l_429) &= (safe_mod_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((safe_lshift_func_int16_t_s_u((safe_add_func_int32_t_s_s((safe_add_func_int16_t_s_s((((l_422 == 8UL) < ((l_427 = (safe_add_func_int32_t_s_s(p_85, (((((p_87 ^ ((l_425 , l_426) != (g_396 = &g_127))) < 5UL) , p_85) || p_84) <= p_85)))) , g_327)) != 0xC2L), 9UL)), l_428)), 6)), p_84)), (-1L)))))) ^ p_85), 2L));
                        g_71 |= (safe_rshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((((safe_sub_func_int32_t_s_s(l_438, p_88)) > (0UL & (g_178 != 1L))) < 0xC451L), ((safe_div_func_uint8_t_u_u((g_64 || 0x26L), ((safe_rshift_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(((safe_add_func_int64_t_s_s((0x4131CA756EBC41ADLL < 18446744073709551613UL), l_447)) , 9UL), 4UL)), l_448)) & 4294967295UL))) < 0x3BL))), g_237[0]));
                        l_447 ^= (-1L);
                        g_71 = p_85;
                    }
                }
                for (g_210 = 0; (g_210 < (-3)); --g_210)
                { /* block id: 216 */
                    int32_t **l_465 = &g_127;
                    int32_t l_486 = 0L;
                    g_71 |= (!(safe_mod_func_uint64_t_u_u((((safe_lshift_func_int16_t_s_s(((safe_mul_func_int16_t_s_s((g_208[2][2][2] & ((((safe_lshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((safe_mul_func_int16_t_s_s((((l_464 != l_465) && ((safe_sub_func_uint64_t_u_u(0x06916F802E40A817LL, ((((!((((safe_lshift_func_uint8_t_u_u(((safe_div_func_int64_t_s_s((((*p_86) = (safe_lshift_func_uint8_t_u_u((g_178 , (safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_s((safe_sub_func_int8_t_s_s((g_153 , (&l_465 != &g_396)), (g_166 = 2UL))), 6)), 0UL))), 0))) != 248UL), 0xF49A396AC79A0483LL)) | p_87), 5)) >= p_88) ^ p_85) || 0xDAL)) == 0x8EL) , 4294967293UL) || p_85))) == p_87)) | p_84), 4UL)), p_88)), 4)) <= 0UL) > g_237[3]) == 0UL)), p_88)) & p_87), g_327)) , (void*)0) == &g_237[1]), p_87)));
                    if (l_481[1])
                        break;
                    l_487 = (l_383 = (safe_mul_func_uint16_t_u_u(((safe_lshift_func_uint16_t_u_u((g_5 != p_84), 5)) || (l_486 ^= p_84)), (0xB9087CD5L > (254UL > 0xBDL)))));
                }
            }
            for (g_235 = 0; (g_235 > 23); ++g_235)
            { /* block id: 228 */
                for (l_150 = 0; (l_150 == 2); l_150++)
                { /* block id: 231 */
                    if (p_87)
                        break;
                }
            }
        }
        else
        { /* block id: 235 */
            int32_t l_492 = (-10L);
            return l_492;
        }
    }
    else
    { /* block id: 238 */
        int32_t l_493 = 1L;
        return l_493;
    }
    return p_87;
}


/* ------------------------------------------ */
/* 
 * reads : g_66 g_64
 * writes:
 */
static int16_t  func_104(const int32_t  p_105, int32_t  p_106)
{ /* block id: 28 */
    uint32_t l_113 = 4294967295UL;
    l_113 = g_66;
    return g_64;
}




/* ---------------------------------------- */
//testcase_id 1484153267
int case1484153267(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_5, "g_5", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_23[i], "g_23[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_38, "g_38", print_hash_value);
    transparent_crc(g_59, "g_59", print_hash_value);
    transparent_crc(g_64, "g_64", print_hash_value);
    transparent_crc(g_66, "g_66", print_hash_value);
    transparent_crc(g_71, "g_71", print_hash_value);
    transparent_crc(g_80, "g_80", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_140, "g_140", print_hash_value);
    transparent_crc(g_153, "g_153", print_hash_value);
    transparent_crc(g_166, "g_166", print_hash_value);
    transparent_crc(g_178, "g_178", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_208[i][j][k], "g_208[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_210, "g_210", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_212[i][j], "g_212[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_235, "g_235", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_237[i], "g_237[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_327, "g_327", print_hash_value);
    transparent_crc(g_571, "g_571", print_hash_value);
    transparent_crc(g_606, "g_606", print_hash_value);
    transparent_crc(g_745, "g_745", print_hash_value);
    transparent_crc(g_783, "g_783", print_hash_value);
    transparent_crc(g_940, "g_940", print_hash_value);
    transparent_crc(g_1012, "g_1012", print_hash_value);
    transparent_crc(g_1024, "g_1024", print_hash_value);
    transparent_crc(g_1060, "g_1060", print_hash_value);
    transparent_crc(g_1268, "g_1268", print_hash_value);
    transparent_crc(g_1292, "g_1292", print_hash_value);
    transparent_crc(g_1422, "g_1422", print_hash_value);
    transparent_crc(g_1492, "g_1492", print_hash_value);
    transparent_crc(g_1604, "g_1604", print_hash_value);
    transparent_crc(g_1610, "g_1610", print_hash_value);
    transparent_crc(g_1614, "g_1614", print_hash_value);
    transparent_crc(g_1615, "g_1615", print_hash_value);
    transparent_crc(g_1616, "g_1616", print_hash_value);
    transparent_crc(g_1617, "g_1617", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1931[i][j][k], "g_1931[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2041, "g_2041", print_hash_value);
    transparent_crc(g_2082, "g_2082", print_hash_value);
    transparent_crc(g_2209, "g_2209", print_hash_value);
    transparent_crc(g_2243, "g_2243", print_hash_value);
    transparent_crc(g_2310, "g_2310", print_hash_value);
    transparent_crc(g_2393, "g_2393", print_hash_value);
    transparent_crc(g_2439, "g_2439", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_2513[i][j][k], "g_2513[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2536[i], "g_2536[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2700, "g_2700", print_hash_value);
    transparent_crc(g_2735, "g_2735", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_2806[i][j], "g_2806[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2884, "g_2884", print_hash_value);
    transparent_crc(g_2887, "g_2887", print_hash_value);
    transparent_crc(g_2925, "g_2925", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2952[i], "g_2952[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_3075[i][j], "g_3075[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_3251[i][j], "g_3251[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_3270, "g_3270", print_hash_value);
    transparent_crc(g_3292, "g_3292", print_hash_value);
    transparent_crc(g_3302, "g_3302", print_hash_value);
    transparent_crc(g_3311, "g_3311", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 824
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 246
   depth: 2, occurrence: 75
   depth: 3, occurrence: 6
   depth: 4, occurrence: 2
   depth: 5, occurrence: 2
   depth: 8, occurrence: 2
   depth: 9, occurrence: 2
   depth: 11, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 2
   depth: 15, occurrence: 1
   depth: 17, occurrence: 1
   depth: 18, occurrence: 6
   depth: 19, occurrence: 3
   depth: 20, occurrence: 2
   depth: 21, occurrence: 3
   depth: 22, occurrence: 3
   depth: 23, occurrence: 6
   depth: 24, occurrence: 2
   depth: 26, occurrence: 3
   depth: 27, occurrence: 4
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 2
   depth: 31, occurrence: 2
   depth: 32, occurrence: 3
   depth: 33, occurrence: 2
   depth: 35, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 3
   depth: 38, occurrence: 2
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1

XXX total number of pointers: 658

XXX times a variable address is taken: 1560
XXX times a pointer is dereferenced on RHS: 463
breakdown:
   depth: 1, occurrence: 329
   depth: 2, occurrence: 82
   depth: 3, occurrence: 33
   depth: 4, occurrence: 19
XXX times a pointer is dereferenced on LHS: 451
breakdown:
   depth: 1, occurrence: 402
   depth: 2, occurrence: 31
   depth: 3, occurrence: 13
   depth: 4, occurrence: 5
XXX times a pointer is compared with null: 67
XXX times a pointer is compared with address of another variable: 17
XXX times a pointer is compared with another pointer: 38
XXX times a pointer is qualified to be dereferenced: 10629

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2153
   level: 2, occurrence: 420
   level: 3, occurrence: 201
   level: 4, occurrence: 130
   level: 5, occurrence: 65
XXX number of pointers point to pointers: 261
XXX number of pointers point to scalars: 397
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 30.7
XXX average alias set size: 1.48

XXX times a non-volatile is read: 2998
XXX times a non-volatile is write: 1469
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 2
XXX backward jumps: 12

XXX stmts: 267
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 33
   depth: 1, occurrence: 30
   depth: 2, occurrence: 34
   depth: 3, occurrence: 44
   depth: 4, occurrence: 62
   depth: 5, occurrence: 64

XXX percentage a fresh-made variable is used: 16.3
XXX percentage an existing variable is used: 83.7
********************* end of statistics **********************/

