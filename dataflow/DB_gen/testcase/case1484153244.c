/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      2034850856
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
struct S0 {
   int32_t  f0;
   unsigned f1 : 15;
   unsigned f2 : 7;
   signed f3 : 13;
   uint8_t  f4;
   unsigned f5 : 15;
   const unsigned f6 : 24;
   signed f7 : 15;
   unsigned f8 : 26;
};

/* --- GLOBAL VARIABLES --- */
static int32_t g_2[5] = {0x801FC3EEL,0x801FC3EEL,0x801FC3EEL,0x801FC3EEL,0x801FC3EEL};
static struct S0 g_7 = {0x4DBBE568L,158,5,58,0x40L,4,1191,-70,5106};
static int32_t g_60 = 0x27BC4A6BL;
static uint32_t g_72 = 0x665F1F47L;
static int64_t g_74 = 0x0880F5C685B35A07LL;
static int32_t g_76 = 0x3FE27FCEL;
static int32_t g_78 = 0x5669BA90L;
static uint8_t g_96 = 0x7AL;
static int32_t g_103[8][2] = {{0xBBD83FE2L,6L},{0xBBD83FE2L,0xE610DA34L},{(-5L),(-5L)},{0xE610DA34L,0xBBD83FE2L},{6L,0xBBD83FE2L},{0xE610DA34L,(-5L)},{(-5L),0xE610DA34L},{0xBBD83FE2L,6L}};
static int32_t g_116 = 0x32C37782L;
static uint64_t g_132 = 0xDB6BE6058BBDFDC5LL;
static int16_t g_134 = (-7L);
static int16_t g_136 = 0xD7A3L;
static uint16_t g_149 = 65535UL;
static int32_t *g_155[1] = {(void*)0};
static int16_t g_296 = 0x7577L;
static uint32_t g_371 = 0xA7614259L;
static uint8_t g_396 = 255UL;
static int64_t *g_421 = &g_74;
static int64_t **g_420[2][8][7] = {{{(void*)0,&g_421,&g_421,(void*)0,&g_421,(void*)0,&g_421},{&g_421,&g_421,&g_421,&g_421,&g_421,&g_421,&g_421},{&g_421,&g_421,&g_421,&g_421,&g_421,&g_421,&g_421},{(void*)0,&g_421,(void*)0,&g_421,(void*)0,&g_421,&g_421},{(void*)0,&g_421,&g_421,&g_421,&g_421,(void*)0,&g_421},{&g_421,&g_421,(void*)0,&g_421,&g_421,&g_421,(void*)0},{&g_421,&g_421,(void*)0,&g_421,(void*)0,&g_421,&g_421},{&g_421,&g_421,&g_421,&g_421,&g_421,(void*)0,(void*)0}},{{&g_421,&g_421,&g_421,(void*)0,(void*)0,&g_421,&g_421},{(void*)0,&g_421,&g_421,&g_421,&g_421,&g_421,&g_421},{&g_421,(void*)0,(void*)0,&g_421,&g_421,&g_421,(void*)0},{&g_421,(void*)0,(void*)0,&g_421,(void*)0,(void*)0,&g_421},{&g_421,(void*)0,&g_421,(void*)0,&g_421,&g_421,(void*)0},{&g_421,&g_421,&g_421,&g_421,(void*)0,&g_421,&g_421},{(void*)0,&g_421,&g_421,&g_421,&g_421,(void*)0,&g_421},{&g_421,&g_421,(void*)0,&g_421,&g_421,&g_421,(void*)0}}};
static int64_t **g_427 = (void*)0;
static int16_t g_447 = 0xA252L;
static uint8_t g_488 = 1UL;
static int32_t g_493 = 0x34BA7087L;
static uint8_t g_526 = 0x89L;
static uint32_t *g_594 = (void*)0;
static const uint32_t g_611 = 0xBE0A0AC9L;
static uint32_t g_662 = 0x645EE446L;
static int64_t g_673 = 1L;
static uint64_t *g_720[1] = {(void*)0};
static int32_t g_752 = 0x2032EA7EL;
static int64_t g_776[3] = {0xAEDF42D676D59312LL,0xAEDF42D676D59312LL,0xAEDF42D676D59312LL};
static int64_t g_808[6] = {(-5L),1L,(-5L),(-5L),1L,(-5L)};
static int16_t *g_845 = &g_134;
static int16_t **g_844 = &g_845;
static uint32_t g_876 = 1UL;
static int64_t * const *g_892 = (void*)0;
static uint32_t *g_902 = &g_662;
static uint32_t ** const g_901 = &g_902;
static uint32_t g_926 = 0x3144A19AL;
static const int32_t g_942[7][4] = {{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L},{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L},{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L},{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L},{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L},{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L},{0x9B659FB6L,0x508A2FCCL,0x508A2FCCL,0x9B659FB6L}};
static uint32_t g_955 = 3UL;
static int8_t g_983 = 0x51L;
static int8_t *g_982 = &g_983;
static uint32_t **g_984 = &g_902;
static uint64_t g_1107 = 0UL;
static const int64_t *g_1121 = &g_74;
static const int64_t ** const g_1120 = &g_1121;
static const int64_t ** const *g_1119 = &g_1120;
static int32_t **g_1189 = &g_155[0];
static const uint16_t g_1209 = 0x1636L;
static const uint32_t g_1277 = 1UL;
static uint32_t g_1289[3] = {0x387C61D8L,0x387C61D8L,0x387C61D8L};
static int64_t g_1410 = 0x7D0F45B14626A372LL;
static uint16_t g_1473[9][7][2] = {{{0x6909L,0xFE32L},{65528UL,0x6F43L},{0UL,0x3D3FL},{0x3D3FL,0x3D3FL},{0UL,0x6F43L},{65528UL,0xFE32L},{0x6909L,0UL}},{{4UL,0x6909L},{0x6F7EL,0x34BCL},{0x6F7EL,0x6909L},{4UL,0UL},{0x6909L,0xFE32L},{65528UL,0x6F43L},{0UL,0x3D3FL}},{{0x3D3FL,0x3D3FL},{0UL,0x6F43L},{65528UL,0xFE32L},{0x6909L,0UL},{4UL,0x6909L},{0x6F7EL,0x34BCL},{0x6F7EL,0x6909L}},{{4UL,0UL},{0x6909L,0xFE32L},{65528UL,0x6F43L},{0UL,0x3D3FL},{0x3D3FL,0x3D3FL},{0UL,0x6F43L},{65528UL,0xFE32L}},{{0x6909L,0UL},{4UL,0x6909L},{0x6F7EL,0x34BCL},{0x6F43L,0x34BCL},{65528UL,0x3D3FL},{0x34BCL,4UL},{0x6F7EL,0UL}},{{0x3D3FL,8UL},{8UL,8UL},{0x3D3FL,0UL},{0x6F7EL,4UL},{0x34BCL,0x3D3FL},{65528UL,0x34BCL},{0x6F43L,0UL}},{{0x6F43L,0x34BCL},{65528UL,0x3D3FL},{0x34BCL,4UL},{0x6F7EL,0UL},{0x3D3FL,8UL},{8UL,8UL},{0x3D3FL,0UL}},{{0x6F7EL,4UL},{0x34BCL,0x3D3FL},{65528UL,0x34BCL},{0x6F43L,0UL},{0x6F43L,0x34BCL},{65528UL,0x3D3FL},{0x34BCL,4UL}},{{0x6F7EL,0UL},{0x3D3FL,8UL},{8UL,8UL},{0x3D3FL,0UL},{0x6F7EL,4UL},{0x34BCL,0x3D3FL},{65528UL,0x34BCL}}};
static int16_t g_1567 = 1L;
static struct S0 **g_1687 = (void*)0;
static struct S0 ***g_1686 = &g_1687;
static struct S0 ****g_1685 = &g_1686;
static uint16_t ***g_1806 = (void*)0;
static uint64_t g_1814 = 18446744073709551607UL;
static uint16_t g_1817 = 65534UL;
static const uint32_t g_1830 = 0x61804222L;
static uint32_t *g_1833 = &g_926;
static int8_t g_1849 = 0x16L;
static uint32_t g_1909 = 0x78F47B1BL;
static int32_t *g_1965 = &g_103[1][1];
static int32_t **g_1964 = &g_1965;
static int16_t ** const ** const g_1999 = (void*)0;
static uint64_t g_2029 = 0x6CCE06A636CB38D4LL;
static int8_t g_2030 = 0L;
static struct S0 g_2106 = {-4L,39,10,9,0xA4L,65,3087,-55,1146};
static struct S0 *g_2105 = &g_2106;
static int16_t ****g_2231 = (void*)0;
static const int16_t g_2267 = 0xBC10L;
static uint64_t g_2308[1] = {0x77F0AD5FBB033FFBLL};
static int16_t g_2357 = 0x8E5BL;
static struct S0 g_2390[8] = {{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787},{1L,9,10,56,0x81L,72,4026,78,5787}};
static int64_t ***g_2559 = &g_420[1][0][6];
static int64_t ****g_2558 = &g_2559;
static int8_t **g_2565 = &g_982;
static int64_t g_2690 = 9L;
static const int16_t *g_2828 = (void*)0;
static const int16_t **g_2827 = &g_2828;
static const int16_t ***g_2826 = &g_2827;
static const int16_t ****g_2825[10] = {(void*)0,&g_2826,&g_2826,&g_2826,&g_2826,(void*)0,&g_2826,&g_2826,&g_2826,&g_2826};
static int16_t g_2905 = 5L;
static uint16_t g_2907 = 3UL;
static uint32_t g_2919 = 9UL;
static uint16_t g_2945 = 0xCAADL;
static int32_t g_2963 = 0x9B591EC5L;
static int32_t g_3003[9][10][2] = {{{6L,0x687DB618L},{0L,6L},{0x475B5259L,(-4L)},{0x53E9DFF0L,0xB1A7AA2CL},{0L,0x8B5FC350L},{7L,(-1L)},{(-1L),(-1L)},{7L,0x8B5FC350L},{0L,0xB1A7AA2CL},{0x53E9DFF0L,(-4L)}},{{0x475B5259L,6L},{0L,0x687DB618L},{6L,2L},{0xB872E20BL,(-1L)},{(-8L),0x475B5259L},{(-2L),0x7EE9A4DDL},{0xC067EAB9L,0L},{0L,(-6L)},{0x15D999C9L,0xC067EAB9L},{6L,0xF3566B6BL}},{{0xB1A7AA2CL,4L},{0x1C3D3735L,0x1C3D3735L},{1L,(-5L)},{0xC8EE3D72L,0L},{(-1L),(-2L)},{0xD7FB3D31L,(-1L)},{4L,6L},{4L,(-1L)},{0xD7FB3D31L,(-2L)},{(-1L),0L}},{{0xC8EE3D72L,(-5L)},{1L,0x1C3D3735L},{0x1C3D3735L,4L},{0xB1A7AA2CL,0xF3566B6BL},{6L,0xC067EAB9L},{0x15D999C9L,(-6L)},{0L,0L},{0xC067EAB9L,0x7EE9A4DDL},{(-2L),0x475B5259L},{(-8L),(-1L)}},{{0xB872E20BL,2L},{6L,0x687DB618L},{0L,6L},{0x475B5259L,(-4L)},{0x53E9DFF0L,0xB1A7AA2CL},{0L,0x8B5FC350L},{7L,(-1L)},{(-1L),(-1L)},{7L,0x8B5FC350L},{0L,0xB1A7AA2CL}},{{0x53E9DFF0L,(-4L)},{0x475B5259L,6L},{0L,0x687DB618L},{6L,0x8B5FC350L},{0x53E9DFF0L,7L},{(-1L),(-7L)},{(-1L),(-6L)},{0xCD7CF012L,0x935938A6L},{(-2L),6L},{0L,0xCD7CF012L}},{{0x0C9EC944L,0L},{0xD7FB3D31L,0xB1A7AA2CL},{0x15D999C9L,0x15D999C9L},{(-5L),0x81B859EEL},{0xF3566B6BL,6L},{(-4L),(-1L)},{0x7EE9A4DDL,(-4L)},{0xB1A7AA2CL,(-1L)},{0xB1A7AA2CL,(-4L)},{0x7EE9A4DDL,(-1L)}},{{(-4L),6L},{0xF3566B6BL,0x81B859EEL},{(-5L),0x15D999C9L},{0x15D999C9L,0xB1A7AA2CL},{0xD7FB3D31L,0L},{0x0C9EC944L,0xCD7CF012L},{0L,6L},{(-2L),0x935938A6L},{0xCD7CF012L,(-6L)},{(-1L),(-7L)}},{{(-1L),7L},{0x53E9DFF0L,0x8B5FC350L},{(-1L),0x475B5259L},{(-1L),0x0C9EC944L},{(-7L),0L},{1L,0xD7FB3D31L},{6L,0x37F8DF14L},{0xC8EE3D72L,2L},{7L,2L},{0xC8EE3D72L,0x37F8DF14L}}};
static uint64_t **g_3024 = (void*)0;
static uint64_t ***g_3023 = &g_3024;


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);
static int32_t  func_5(struct S0  p_6);
static const int8_t  func_19(uint32_t  p_20, uint16_t  p_21, uint32_t  p_22, uint16_t  p_23);
static int16_t  func_26(uint64_t  p_27, int32_t  p_28, uint64_t  p_29, uint16_t  p_30);
static int16_t  func_33(const uint16_t  p_34, uint8_t  p_35, uint32_t  p_36);
static const struct S0  func_45(uint8_t  p_46, uint32_t  p_47, struct S0  p_48, uint32_t  p_49, const int32_t  p_50);
static uint64_t  func_51(uint64_t  p_52, uint64_t  p_53, int32_t  p_54);
static uint32_t  func_63(int64_t  p_64, int32_t * p_65, uint32_t  p_66);
static int32_t ** func_84(uint16_t  p_85);
static struct S0  func_91(uint8_t  p_92, uint32_t  p_93, int64_t * p_94);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_2 g_7 g_76
 * writes: g_2 g_60 g_76 g_2963
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int32_t l_3062 = 0L;
    for (g_2[1] = 0; (g_2[1] <= (-28)); g_2[1] = safe_sub_func_int8_t_s_s(g_2[1], 9))
    { /* block id: 3 */
        int32_t *l_3057 = &g_60;
        (*l_3057) = func_5(g_7);
    }
    for (g_76 = 0; (g_76 <= (-12)); --g_76)
    { /* block id: 1528 */
        int32_t *l_3060 = &g_2963;
        int8_t **l_3061 = &g_982;
        (*l_3060) = 0x4201D0C2L;
        l_3061 = &g_982;
    }
    return l_3062;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_5(struct S0  p_6)
{ /* block id: 4 */
    const uint8_t l_37 = 0x7DL;
    int8_t l_2069 = (-1L);
    uint32_t l_2204 = 4294967295UL;
    int32_t *l_2212 = (void*)0;
    int32_t l_2216 = 8L;
    const int64_t ***l_2225 = (void*)0;
    int16_t **l_2226 = (void*)0;
    uint8_t l_2259 = 0x4AL;
    const int16_t *l_2266 = &g_2267;
    int8_t **l_2360 = &g_982;
    const struct S0 *l_2362 = &g_2106;
    int32_t l_2365[3];
    uint16_t l_2366[10] = {65534UL,0UL,5UL,5UL,0UL,65534UL,0UL,5UL,5UL,0UL};
    const int16_t l_2384[1][5][6] = {{{0L,0x38D9L,0x38D9L,0L,1L,1L},{(-1L),(-2L),(-8L),1L,5L,0x3A0EL},{5L,1L,0xBF12L,1L,5L,0x1342L},{0x3A0EL,(-2L),0L,0x5481L,1L,(-8L)},{(-8L),0x38D9L,(-2L),(-2L),0x38D9L,(-8L)}}};
    const int32_t *l_2387 = &g_752;
    uint16_t l_2404 = 0x4F73L;
    int32_t l_2424 = 1L;
    uint32_t ***l_2433 = &g_984;
    uint64_t l_2436 = 0x47E2C166499E2A42LL;
    int32_t ***l_2511 = &g_1964;
    const uint8_t **l_2521 = (void*)0;
    int16_t * const *l_2592 = &g_845;
    int16_t * const **l_2591 = &l_2592;
    int16_t * const ***l_2590 = &l_2591;
    int16_t * const ****l_2589 = &l_2590;
    int8_t l_2625 = 0xA7L;
    int16_t l_2820 = 0x3F8BL;
    uint32_t l_2948[10] = {0x6F29ABFAL,4294967295UL,0xAA793699L,0xAA793699L,4294967295UL,0x6F29ABFAL,4294967295UL,0xAA793699L,0xAA793699L,4294967295UL};
    int64_t l_3002 = 1L;
    const uint64_t *l_3019 = &g_1107;
    const uint64_t **l_3018 = &l_3019;
    const uint64_t ***l_3017[6] = {(void*)0,(void*)0,&l_3018,(void*)0,(void*)0,&l_3018};
    int16_t l_3049 = (-5L);
    int64_t ****l_3050[4][10][6] = {{{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_2559,&g_2559,(void*)0},{&g_2559,&g_2559,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_2559,&g_2559,(void*)0},{&g_2559,&g_2559,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_2559,&g_2559,(void*)0},{&g_2559,&g_2559,(void*)0,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559}},{{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559}},{{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559},{&g_2559,&g_2559,&g_2559,&g_2559,&g_2559,&g_2559}}};
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_2365[i] = 1L;
    return p_6.f5;
}


/* ------------------------------------------ */
/* 
 * reads : g_982 g_983 g_1119 g_1120 g_1121 g_74 g_1830 g_1410 g_116 g_1833 g_926 g_844 g_845 g_134 g_76 g_421 g_673 g_2106 g_1107
 * writes: g_1410 g_155 g_116 g_2105 g_673 g_1814 g_955 g_983 g_1107
 */
static const int8_t  func_19(uint32_t  p_20, uint16_t  p_21, uint32_t  p_22, uint16_t  p_23)
{ /* block id: 1006 */
    uint8_t l_2072 = 0xBEL;
    int32_t **l_2079 = &g_1965;
    int32_t l_2092 = 3L;
    struct S0 *** const *l_2093 = &g_1686;
    int64_t *l_2094 = &g_1410;
    int32_t l_2152[3][3];
    uint32_t l_2177 = 0xB8E47B43L;
    int i, j;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            l_2152[i][j] = 0xAAB93B57L;
    }
    if ((((*l_2094) |= (safe_add_func_uint16_t_u_u((0x75L ^ l_2072), (safe_div_func_uint8_t_u_u((l_2072 > ((safe_sub_func_int16_t_s_s((((((((l_2092 = (((1L && ((safe_div_func_uint8_t_u_u(((&g_1965 == l_2079) , (safe_mul_func_int16_t_s_s((safe_mod_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(l_2072, 0x73F1L)), (safe_rshift_func_uint8_t_u_u((((safe_sub_func_int8_t_s_s((safe_sub_func_int32_t_s_s(0x19E6A340L, l_2072)), (*g_982))) <= l_2092) > 65531UL), 6)))), l_2092))), (*g_982))) && (***g_1119))) == (*g_982)) <= 0x683F3235L)) , l_2093) == &g_1686) , 0x3459F525L) > p_21) | 0x9F30L) , p_22), 1UL)) | g_1830)), l_2072))))) , 0x777A4FD0L))
    { /* block id: 1009 */
        int32_t l_2096 = 0x48CB18A6L;
        int32_t l_2097 = 1L;
        int32_t **l_2098 = (void*)0;
        int32_t **l_2099 = &g_155[0];
        l_2097 = ((l_2092 |= (!p_21)) || l_2096);
        (*l_2099) = &l_2092;
    }
    else
    { /* block id: 1013 */
        int32_t *l_2100 = &g_116;
        int32_t *l_2101 = &l_2092;
        struct S0 *l_2104 = (void*)0;
        uint16_t ***l_2151 = (void*)0;
        uint32_t l_2154 = 0xC85C7EEBL;
        int16_t ** const *l_2191 = &g_844;
        int16_t ** const **l_2190[9] = {&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191,&l_2191};
        int16_t ***l_2196 = &g_844;
        int16_t ****l_2195[4][3];
        int64_t *l_2199 = &g_808[4];
        int i, j;
        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 3; j++)
                l_2195[i][j] = &l_2196;
        }
        (*l_2101) = ((*l_2100) &= ((void*)0 == l_2079));
        if (((*l_2100) = 0x26CEBCA1L))
        { /* block id: 1017 */
            int8_t l_2119 = 0x5AL;
            int32_t *l_2162 = (void*)0;
            (*l_2100) = (((safe_mul_func_int32_t_s_s(p_22, (l_2104 != (g_2105 = l_2104)))) & (safe_mul_func_int64_t_s_s(p_23, ((void*)0 == &p_21)))) | (l_2092 > (safe_add_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u((safe_mod_func_int8_t_s_s(((safe_add_func_uint16_t_u_u((safe_div_func_int8_t_s_s((((l_2092 , p_23) >= 1L) <= (*g_1833)), 0x4FL)), (**g_844))) && l_2119), 1L)), g_76)), (*g_421)))));
            for (g_673 = (-7); (g_673 == (-2)); ++g_673)
            { /* block id: 1022 */
                int16_t l_2148[2][3] = {{0x642DL,0x642DL,0L},{0x642DL,0xC75DL,0x642DL}};
                struct S0 l_2149 = {5L,128,9,2,0x89L,175,78,168,1750};
                int32_t **l_2161 = &l_2101;
                int i, j;
            }
            (*l_2101) ^= p_20;
        }
        else
        { /* block id: 1057 */
            return (*g_982);
        }
        for (p_21 = 0; (p_21 <= 1); p_21 += 1)
        { /* block id: 1062 */
            struct S0 *l_2163 = &g_2106;
            int64_t *l_2174 = &g_808[0];
            if (p_23)
                break;
            for (g_1814 = 0; (g_1814 <= 1); g_1814 += 1)
            { /* block id: 1066 */
                struct S0 **l_2164 = &l_2163;
                int32_t l_2169 = 0x857B2633L;
                uint32_t l_2201 = 1UL;
                (*l_2164) = l_2163;
                for (g_955 = 0; (g_955 <= 1); g_955 += 1)
                { /* block id: 1070 */
                    int16_t l_2173[10][9][2] = {{{(-7L),0xBFCAL},{4L,1L},{8L,(-2L)},{0xB13CL,0L},{0xDBCFL,1L},{8L,0xAF46L},{(-4L),0xBFCAL},{0L,0xBFCAL},{(-4L),0xAF46L}},{{8L,1L},{0xDBCFL,0L},{0xB13CL,(-2L)},{8L,1L},{4L,0xBFCAL},{(-7L),(-1L)},{(-4L),1L},{1L,1L},{0xB13CL,0L}},{{0xB13CL,1L},{1L,1L},{(-4L),(-1L)},{(-7L),0xBFCAL},{4L,1L},{8L,(-2L)},{0xB13CL,0L},{0xDBCFL,1L},{8L,0xAF46L}},{{(-4L),0xBFCAL},{0L,0xBFCAL},{(-4L),0xAF46L},{8L,1L},{0xDBCFL,0L},{0xB13CL,(-2L)},{8L,1L},{4L,0xBFCAL},{(-7L),(-1L)}},{{(-4L),1L},{1L,1L},{0xB13CL,0L},{0xB13CL,1L},{1L,1L},{(-4L),(-1L)},{(-7L),0xBFCAL},{4L,1L},{8L,(-2L)}},{{0xB13CL,0L},{0xDBCFL,1L},{8L,0xAF46L},{(-4L),0xBFCAL},{0L,0xBFCAL},{(-4L),0xAF46L},{8L,1L},{0xDBCFL,0L},{0xB13CL,(-2L)}},{{8L,1L},{4L,0xBFCAL},{(-7L),(-1L)},{(-4L),1L},{1L,1L},{0xB13CL,0L},{0xB13CL,1L},{1L,1L},{(-4L),(-1L)}},{{(-7L),0xBFCAL},{4L,1L},{8L,(-2L)},{0xB13CL,0L},{0xDBCFL,1L},{8L,0xAF46L},{(-4L),0xBFCAL},{0L,0xBFCAL},{(-4L),0xAF46L}},{{8L,1L},{0xDBCFL,0L},{0xB13CL,(-2L)},{8L,1L},{4L,0xBFCAL},{(-7L),(-1L)},{(-4L),1L},{1L,1L},{0xB13CL,0L}},{{0xB13CL,1L},{1L,1L},{(-4L),(-1L)},{(-7L),0xBFCAL},{4L,1L},{8L,(-2L)},{0xB13CL,0L},{0xDBCFL,1L},{8L,0xAF46L}}};
                    int16_t ***l_2193 = &g_844;
                    int16_t ****l_2192[10] = {(void*)0,&l_2193,(void*)0,&l_2193,(void*)0,(void*)0,&l_2193,(void*)0,&l_2193,&l_2193};
                    int16_t *****l_2194[8];
                    int64_t **l_2200[9][8][3] = {{{&l_2199,&g_421,&l_2174},{&l_2174,&g_421,&l_2199},{&l_2094,&l_2094,&g_421},{&g_421,&l_2174,(void*)0},{&g_421,&l_2199,&g_421},{(void*)0,&g_421,&l_2174},{&l_2174,&l_2094,&l_2094},{&l_2094,&g_421,&l_2094}},{{&g_421,&l_2199,&g_421},{&l_2094,&l_2094,&l_2174},{&l_2174,&l_2174,(void*)0},{(void*)0,&g_421,&l_2174},{&g_421,&l_2094,&l_2094},{&g_421,&g_421,&g_421},{&l_2094,&l_2174,&g_421},{&l_2174,&l_2199,&l_2094}},{{&l_2199,&l_2094,&l_2174},{(void*)0,&l_2199,&g_421},{&g_421,&l_2174,&l_2094},{&l_2094,&g_421,&l_2174},{&l_2094,&l_2094,&g_421},{&g_421,&g_421,&g_421},{&g_421,&l_2174,&l_2094},{&g_421,&l_2094,&g_421}},{{&l_2174,&l_2199,&g_421},{(void*)0,&g_421,&g_421},{&l_2199,&l_2094,&l_2094},{&g_421,&g_421,&g_421},{&g_421,&l_2199,&g_421},{&l_2174,&l_2174,&l_2174},{&l_2199,&l_2094,&l_2094},{(void*)0,&g_421,&g_421}},{{&l_2199,&g_421,&l_2174},{&g_421,&g_421,&l_2094},{&l_2199,&g_421,&g_421},{(void*)0,&l_2094,&g_421},{&l_2199,&l_2094,&l_2094},{&l_2174,&l_2174,&l_2174},{&g_421,(void*)0,(void*)0},{&g_421,&l_2174,&l_2174}},{{&l_2199,&l_2094,&g_421},{(void*)0,&g_421,&l_2094},{&l_2174,&l_2094,&l_2094},{&g_421,&l_2174,&l_2174},{&g_421,(void*)0,&g_421},{&g_421,&l_2174,(void*)0},{&l_2094,&l_2094,&g_421},{&l_2094,&l_2094,&l_2199}},{{&g_421,&g_421,&l_2174},{(void*)0,&g_421,&l_2174},{&l_2199,&g_421,&l_2174},{&l_2174,&g_421,&l_2199},{&l_2094,&l_2094,&g_421},{&g_421,&l_2174,(void*)0},{&g_421,&l_2199,&g_421},{(void*)0,&g_421,&l_2174}},{{&l_2174,&l_2094,&l_2094},{&l_2094,&g_421,&l_2094},{&g_421,&l_2199,&g_421},{&l_2094,&l_2094,&l_2174},{&l_2174,&l_2174,(void*)0},{(void*)0,&g_421,&l_2174},{&g_421,&l_2094,&l_2094},{(void*)0,(void*)0,&l_2094}},{{&l_2174,&l_2174,&g_421},{&l_2094,&l_2094,&g_421},{&l_2094,&g_421,&l_2094},{&g_421,&l_2094,&l_2094},{&l_2174,&l_2174,&l_2199},{&l_2174,(void*)0,&l_2094},{&g_421,&g_421,&l_2199},{&g_421,&g_421,(void*)0}}};
                    int i, j, k;
                    for (i = 0; i < 8; i++)
                        l_2194[i] = &l_2192[6];
                    (*l_2100) &= ((safe_add_func_int64_t_s_s((safe_rshift_func_uint8_t_u_s(0x10L, ((l_2169 > (((safe_unary_minus_func_uint8_t_u(((safe_mod_func_int32_t_s_s(((((((*g_982) = l_2173[8][8][1]) & (-1L)) , (void*)0) != ((*l_2101) , ((*l_2163) , l_2174))) >= ((void*)0 != &g_2030)), 0x135D2512L)) , 0x5BL))) & 0xE3BABBD5L) > 0x23L)) || 0xDCL))), p_23)) > 0xF170B72F399B0BCFLL);
                    (*l_2164) = l_2163;
                    (*l_2101) = (safe_div_func_uint32_t_u_u(((-2L) == (l_2177 , (safe_sub_func_uint32_t_u_u(((safe_mod_func_uint16_t_u_u(((safe_mul_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(((safe_sub_func_uint64_t_u_u((((((l_2169 ^ ((p_22 <= (safe_mod_func_int16_t_s_s((((l_2190[0] != (l_2195[2][0] = l_2192[6])) >= (((p_20 < ((l_2094 = l_2199) != (void*)0)) < (**g_844)) < 4294967295UL)) , l_2177), 5UL))) & (*g_1833))) > (*g_982)) > p_20) && p_20) <= (**g_844)), p_23)) <= l_2173[3][1][0]), 0xF39239E70C8CA251LL)), l_2201)) == 0xAFAE5797L), l_2173[2][4][0])) , 4294967286UL), l_2173[5][8][1])))), l_2169));
                    if (p_20)
                        break;
                }
            }
        }
        (*l_2101) = l_2152[1][1];
    }
    for (g_1107 = 24; (g_1107 <= 51); g_1107++)
    { /* block id: 1085 */
        return p_20;
    }
    return l_2152[1][1];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int16_t  func_26(uint64_t  p_27, int32_t  p_28, uint64_t  p_29, uint16_t  p_30)
{ /* block id: 1003 */
    return p_28;
}


/* ------------------------------------------ */
/* 
 * reads : g_7.f1 g_7.f4 g_76 g_78 g_7.f8 g_2 g_103 g_7.f6 g_7.f7 g_132 g_136 g_96 g_7.f2 g_7.f3 g_116 g_7.f0 g_7 g_134 g_149 g_72 g_155 g_371 g_396 g_74 g_421 g_296 g_447 g_982 g_983 g_1209 g_845 g_808 g_488 g_1410 g_942 g_1119 g_1120 g_1121
 * writes: g_60 g_72 g_74 g_76 g_78 g_7.f0 g_96 g_116 g_7.f7 g_132 g_134 g_136 g_149 g_155 g_7.f8 g_296 g_371 g_420 g_7.f5 g_427 g_983 g_1189 g_662 g_1410
 */
static int16_t  func_33(const uint16_t  p_34, uint8_t  p_35, uint32_t  p_36)
{ /* block id: 5 */
    uint16_t l_55 = 0x603FL;
    int64_t l_61 = 0x9045039A24791A13LL;
    int32_t l_2050 = 0L;
    int32_t *l_2051 = &l_2050;
    int32_t *l_2052[9];
    uint32_t l_2053 = 1UL;
    int16_t l_2056 = 0x0A0FL;
    uint32_t l_2057[7] = {6UL,0x83F7A4CCL,0x83F7A4CCL,6UL,0x83F7A4CCL,0x83F7A4CCL,6UL};
    int16_t ***l_2063[2][4][3] = {{{&g_844,&g_844,&g_844},{&g_844,(void*)0,&g_844},{&g_844,(void*)0,&g_844},{&g_844,&g_844,&g_844}},{{&g_844,(void*)0,&g_844},{&g_844,(void*)0,&g_844},{&g_844,&g_844,&g_844},{&g_844,(void*)0,&g_844}}};
    int16_t **** const l_2062[8] = {&l_2063[1][2][1],&l_2063[1][2][1],&l_2063[1][2][1],&l_2063[1][2][1],&l_2063[1][2][1],&l_2063[1][2][1],&l_2063[1][2][1],&l_2063[1][2][1]};
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_2052[i] = &g_116;
    for (p_36 = 0; (p_36 == 52); p_36++)
    { /* block id: 8 */
        uint32_t l_40 = 7UL;
        struct S0 l_62 = {0x739C36DEL,40,0,54,0UL,74,3430,-115,2267};
        int32_t *l_67 = &g_2[1];
        l_40--;
        l_2050 = ((g_7.f1 ^ (safe_sub_func_uint32_t_u_u((func_45((func_51(l_40, l_55, l_40) & g_7.f4), l_61, l_62, func_63((((-2L) || p_35) , l_40), l_67, p_35), p_36) , p_34), (*l_67)))) | l_61);
    }
    --l_2053;
    l_2057[1]++;
    for (g_1410 = 0; (g_1410 > 16); ++g_1410)
    { /* block id: 998 */
        int32_t l_2068 = 0x557E8281L;
        (*l_2051) = (9L < ((l_2062[0] != (void*)0) || ((((0xC9L || g_942[3][0]) == (***g_1119)) == ((((-6L) < (safe_mul_func_uint16_t_u_u((safe_rshift_func_int16_t_s_s((&l_2063[1][0][2] == &l_2063[1][1][0]), (*g_845))), 1UL))) , p_35) & 0x54L)) > p_35)));
        l_2068 |= 1L;
    }
    return p_34;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const struct S0  func_45(uint8_t  p_46, uint32_t  p_47, struct S0  p_48, uint32_t  p_49, const int32_t  p_50)
{ /* block id: 602 */
    uint64_t l_1241[6] = {0xE9442502D184851DLL,0xC21F716735920288LL,0xC21F716735920288LL,0xE9442502D184851DLL,0xC21F716735920288LL,0xC21F716735920288LL};
    int32_t l_1261[5][6];
    uint16_t *l_1269 = &g_149;
    uint16_t **l_1268[3];
    int64_t l_1287[9];
    const uint32_t l_1309 = 0x8A5E8F38L;
    const struct S0 l_1314 = {-4L,128,0,-53,0UL,46,1132,-97,8003};
    uint32_t l_1331 = 0xCED16D16L;
    uint64_t *l_1332[1][4][7] = {{{&g_132,&g_1107,&g_1107,&l_1241[1],&l_1241[2],&l_1241[2],&l_1241[1]},{&g_1107,(void*)0,&g_1107,&l_1241[2],&l_1241[1],&g_132,&l_1241[2]},{&g_1107,&g_1107,&g_132,&l_1241[2],&g_132,&g_1107,&g_1107},{&g_1107,(void*)0,&l_1241[2],&l_1241[1],&l_1241[1],&g_132,&l_1241[1]}}};
    uint64_t l_1333 = 0UL;
    int8_t l_1338 = 0x4AL;
    uint16_t l_1377 = 5UL;
    int32_t l_1409 = (-1L);
    const int32_t *l_1422[1];
    uint64_t *l_1427 = &g_132;
    struct S0 *l_1435 = &g_7;
    struct S0 **l_1434 = &l_1435;
    uint32_t *l_1708 = (void*)0;
    int64_t ** const l_1732 = &g_421;
    uint8_t *l_1799 = &g_396;
    uint8_t **l_1798 = &l_1799;
    int16_t l_1813[4];
    uint32_t l_1840 = 0UL;
    int64_t l_1874 = 9L;
    uint8_t l_1888 = 0xABL;
    int16_t ***l_1974 = &g_844;
    uint64_t l_2011 = 0UL;
    int64_t l_2018 = (-10L);
    int i, j, k;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 6; j++)
            l_1261[i][j] = (-1L);
    }
    for (i = 0; i < 3; i++)
        l_1268[i] = &l_1269;
    for (i = 0; i < 9; i++)
        l_1287[i] = 3L;
    for (i = 0; i < 1; i++)
        l_1422[i] = &g_76;
    for (i = 0; i < 4; i++)
        l_1813[i] = 0xCD08L;
    return p_48;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_60
 */
static uint64_t  func_51(uint64_t  p_52, uint64_t  p_53, int32_t  p_54)
{ /* block id: 10 */
    const int64_t l_58[6][9] = {{7L,0L,0L,7L,(-10L),(-2L),0x06EE4F120B252307LL,8L,0x06EE4F120B252307LL},{0xBEA8E4340421A167LL,(-4L),6L,6L,(-4L),0xBEA8E4340421A167LL,0xC77ACAEC2B2A8F30LL,0xCF68020C2888A0E7LL,1L},{0x80EABF554C62B419LL,(-2L),8L,(-10L),(-10L),8L,(-2L),0x80EABF554C62B419LL,0xC0E5C5CC99CF48B0LL},{1L,6L,0x7A065BBD97C2923FLL,0xC77ACAEC2B2A8F30LL,(-1L),(-1L),0xC77ACAEC2B2A8F30LL,0x7A065BBD97C2923FLL,6L},{(-10L),0x80EABF554C62B419LL,(-1L),0xC0E5C5CC99CF48B0LL,0L,0x06EE4F120B252307LL,0x06EE4F120B252307LL,0L,0xC0E5C5CC99CF48B0LL},{0x3E67B33DF123EFBBLL,0xD86E6AF5756627F1LL,0x3E67B33DF123EFBBLL,0x8101BC642EE1E3C1LL,0xC77ACAEC2B2A8F30LL,1L,0xBEA8E4340421A167LL,0xBEA8E4340421A167LL,1L}};
    int32_t *l_59 = &g_60;
    int i, j;
    (*l_59) = (safe_lshift_func_uint16_t_u_u(l_58[2][5], 1));
    return p_53;
}


/* ------------------------------------------ */
/* 
 * reads : g_7.f4 g_76 g_78 g_7.f8 g_2 g_103 g_7.f6 g_7.f7 g_132 g_136 g_96 g_7.f2 g_7.f3 g_7.f1 g_116 g_7.f0 g_7 g_134 g_149 g_72 g_155 g_371 g_396 g_74 g_421 g_296 g_447 g_982 g_983 g_1209 g_845 g_808 g_662 g_488
 * writes: g_72 g_74 g_76 g_78 g_7.f0 g_96 g_116 g_7.f7 g_132 g_134 g_136 g_149 g_155 g_7.f8 g_296 g_371 g_420 g_7.f5 g_427 g_983 g_1189 g_662
 */
static uint32_t  func_63(int64_t  p_64, int32_t * p_65, uint32_t  p_66)
{ /* block id: 13 */
    uint8_t l_70 = 1UL;
    uint32_t *l_71 = &g_72;
    int64_t *l_73 = &g_74;
    int32_t *l_75 = &g_76;
    int32_t *l_77[10] = {&g_78,&g_2[2],&g_78,&g_78,&g_2[2],&g_78,&g_78,&g_2[2],&g_78,&g_78};
    uint16_t l_1207 = 0x0C8AL;
    uint16_t *l_1229 = &l_1207;
    uint16_t **l_1228 = &l_1229;
    uint32_t l_1230 = 0x33A86253L;
    int8_t l_1234 = 0L;
    uint64_t l_1235[7][10][3] = {{{0x69835EEB9A7AECE2LL,5UL,0x316E3D690225D4DFLL},{1UL,0x8F17622134F1210FLL,1UL},{0x316E3D690225D4DFLL,5UL,0x69835EEB9A7AECE2LL},{0x9F1DF28B32E27C47LL,0xFE68E3107111122FLL,0xE55DEEF134DF18B0LL},{18446744073709551612UL,18446744073709551615UL,0x54CACD8DB50D7A3ALL},{0x6A1929DF99FEC84FLL,0x6A1929DF99FEC84FLL,0UL},{18446744073709551612UL,0x316E3D690225D4DFLL,18446744073709551615UL},{0x9F1DF28B32E27C47LL,0UL,1UL},{0x316E3D690225D4DFLL,18446744073709551615UL,18446744073709551615UL},{18446744073709551615UL,0x8F17622134F1210FLL,0xE55DEEF134DF18B0LL}},{{0x54CACD8DB50D7A3ALL,18446744073709551612UL,7UL},{1UL,0UL,0x6A1929DF99FEC84FLL},{5UL,1UL,5UL},{0x6A1929DF99FEC84FLL,0UL,1UL},{7UL,18446744073709551612UL,0x54CACD8DB50D7A3ALL},{0xE55DEEF134DF18B0LL,0x8F17622134F1210FLL,18446744073709551615UL},{0x0774FA1C7029F7DELL,0x0774FA1C7029F7DELL,18446744073709551615UL},{0xE55DEEF134DF18B0LL,0x6A1929DF99FEC84FLL,0x8F17622134F1210FLL},{7UL,18446744073709551615UL,0x69835EEB9A7AECE2LL},{0x6A1929DF99FEC84FLL,0xA3A0F479527C6113LL,0xA3A0F479527C6113LL}},{{5UL,7UL,0x69835EEB9A7AECE2LL},{1UL,1UL,0x8F17622134F1210FLL},{0x54CACD8DB50D7A3ALL,0x316E3D690225D4DFLL,18446744073709551615UL},{18446744073709551615UL,0UL,18446744073709551615UL},{18446744073709551615UL,0x316E3D690225D4DFLL,0x54CACD8DB50D7A3ALL},{0x8F17622134F1210FLL,1UL,1UL},{0x69835EEB9A7AECE2LL,7UL,5UL},{0xA3A0F479527C6113LL,0xA3A0F479527C6113LL,0x6A1929DF99FEC84FLL},{0x69835EEB9A7AECE2LL,18446744073709551615UL,7UL},{0x8F17622134F1210FLL,0x6A1929DF99FEC84FLL,0xE55DEEF134DF18B0LL}},{{18446744073709551615UL,0x0774FA1C7029F7DELL,0x0774FA1C7029F7DELL},{18446744073709551615UL,0x8F17622134F1210FLL,0xE55DEEF134DF18B0LL},{0x54CACD8DB50D7A3ALL,18446744073709551612UL,7UL},{1UL,0UL,0x6A1929DF99FEC84FLL},{5UL,1UL,5UL},{0x6A1929DF99FEC84FLL,0UL,1UL},{7UL,18446744073709551612UL,0x54CACD8DB50D7A3ALL},{0xE55DEEF134DF18B0LL,0x8F17622134F1210FLL,18446744073709551615UL},{0x0774FA1C7029F7DELL,0x0774FA1C7029F7DELL,18446744073709551615UL},{0xE55DEEF134DF18B0LL,0x6A1929DF99FEC84FLL,0x8F17622134F1210FLL}},{{7UL,18446744073709551615UL,0x69835EEB9A7AECE2LL},{0x6A1929DF99FEC84FLL,0xA3A0F479527C6113LL,0xA3A0F479527C6113LL},{5UL,7UL,0x69835EEB9A7AECE2LL},{1UL,1UL,0x8F17622134F1210FLL},{0x54CACD8DB50D7A3ALL,0x316E3D690225D4DFLL,18446744073709551615UL},{18446744073709551615UL,0UL,18446744073709551615UL},{18446744073709551615UL,0x316E3D690225D4DFLL,0x54CACD8DB50D7A3ALL},{0x8F17622134F1210FLL,1UL,1UL},{0x69835EEB9A7AECE2LL,7UL,5UL},{0xA3A0F479527C6113LL,0xA3A0F479527C6113LL,0x6A1929DF99FEC84FLL}},{{0x69835EEB9A7AECE2LL,18446744073709551615UL,7UL},{0x8F17622134F1210FLL,0x6A1929DF99FEC84FLL,0xE55DEEF134DF18B0LL},{18446744073709551615UL,0x0774FA1C7029F7DELL,0x0774FA1C7029F7DELL},{18446744073709551615UL,0x8F17622134F1210FLL,0xE55DEEF134DF18B0LL},{0x54CACD8DB50D7A3ALL,18446744073709551612UL,7UL},{1UL,0UL,0x6A1929DF99FEC84FLL},{5UL,1UL,5UL},{0x6A1929DF99FEC84FLL,0UL,1UL},{7UL,18446744073709551612UL,0x54CACD8DB50D7A3ALL},{0xE55DEEF134DF18B0LL,0x8F17622134F1210FLL,18446744073709551615UL}},{{0x0774FA1C7029F7DELL,0x0774FA1C7029F7DELL,18446744073709551615UL},{0xE55DEEF134DF18B0LL,0x6A1929DF99FEC84FLL,0x8F17622134F1210FLL},{7UL,18446744073709551615UL,0x69835EEB9A7AECE2LL},{0x6A1929DF99FEC84FLL,0xA3A0F479527C6113LL,0xA3A0F479527C6113LL},{5UL,7UL,0x69835EEB9A7AECE2LL},{1UL,1UL,0x8F17622134F1210FLL},{0x54CACD8DB50D7A3ALL,0x316E3D690225D4DFLL,18446744073709551615UL},{18446744073709551615UL,0UL,18446744073709551615UL},{18446744073709551615UL,0x316E3D690225D4DFLL,0x54CACD8DB50D7A3ALL},{0x8F17622134F1210FLL,1UL,1UL}}};
    int i, j, k;
    (*l_75) = ((safe_sub_func_uint64_t_u_u(0x82577C8A60B10F65LL, ((*l_73) = ((((void*)0 == &g_2[0]) & ((*l_71) = l_70)) & 0x72BB1342F2784030LL)))) >= g_7.f4);
    g_78 |= (*l_75);
    for (p_64 = 13; (p_64 >= (-18)); p_64 = safe_sub_func_int64_t_s_s(p_64, 8))
    { /* block id: 20 */
        uint32_t **l_1227 = (void*)0;
        int32_t l_1231[1][8][7] = {{{0xD375386BL,0x6A012D4AL,0xC9E9B731L,0x3CFDA061L,1L,1L,0x3CFDA061L},{(-1L),0x6A012D4AL,(-1L),(-1L),0x3CFDA061L,(-7L),0xC10F7165L},{0xD375386BL,0x47A37AE7L,0L,0xC9E9B731L,0xC10F7165L,(-1L),(-1L)},{1L,(-1L),(-7L),0x47A37AE7L,0x47A37AE7L,(-7L),(-1L)},{(-1L),0x48513EE2L,(-5L),1L,0x47A37AE7L,(-1L),0L},{0x6A012D4AL,0xD375386BL,0x48513EE2L,0L,0xC10F7165L,0L,(-5L)},{0xC9E9B731L,0x3CFDA061L,1L,1L,0x3CFDA061L,0xC9E9B731L,0x6A012D4AL},{0x48513EE2L,(-5L),1L,0x47A37AE7L,(-1L),0L,0x3CFDA061L}}};
        int64_t l_1233 = 0x741771859BBAB38ELL;
        int i, j, k;
        for (g_78 = 0; (g_78 < (-4)); --g_78)
        { /* block id: 23 */
            int32_t **l_83 = &l_77[6];
            (*l_83) = (void*)0;
            g_1189 = func_84(g_7.f8);
        }
        for (g_296 = 0; (g_296 > 29); g_296 = safe_add_func_int16_t_s_s(g_296, 8))
        { /* block id: 579 */
            int32_t l_1192 = 0x8FD591A6L;
            const int16_t *l_1215 = &g_296;
            const int16_t **l_1214 = &l_1215;
            uint8_t *l_1232 = &g_96;
            l_1192 = (-1L);
            l_1231[0][3][2] = (safe_lshift_func_int16_t_s_s((~((!(safe_mod_func_int8_t_s_s((l_1192 || ((safe_sub_func_int8_t_s_s(((((l_1192 , (safe_div_func_uint64_t_u_u((safe_lshift_func_uint8_t_u_s(((*l_1232) = (((((safe_add_func_int32_t_s_s(((l_1207 , (((!((((((((*g_845) = (g_1209 && (safe_lshift_func_int8_t_s_u(l_1192, (safe_lshift_func_int8_t_s_s((((*g_982) , l_1214) != (void*)0), 7)))))) >= ((safe_sub_func_uint16_t_u_u((safe_mod_func_int8_t_s_s((((safe_mul_func_uint8_t_u_u(((((safe_div_func_uint64_t_u_u((((+((safe_lshift_func_uint16_t_u_u(((l_1227 != l_1227) > g_2[4]), 14)) <= g_7.f4)) | 0x514851D357775F1FLL) | 0x05L), p_66)) , (void*)0) != l_1228) < (*l_75)), l_1192)) <= l_1192) , (*g_982)), 0x17L)), l_1230)) == p_64)) , &g_420[1][6][0]) != &g_1120) , 4294967295UL) < l_1231[0][3][2]) != l_1192)) && g_136) & (-2L))) != 0x6F8DFC37L), l_1192)) != g_103[6][0]) && (*g_982)) , 255UL) , l_1192)), p_66)), 6L))) & l_1233) ^ p_64) | 0x0386L), 0x1DL)) , p_66)), g_396))) || p_64)), p_66));
            for (g_78 = 0; (g_78 <= 5); g_78 += 1)
            { /* block id: 586 */
                for (l_1230 = 0; (l_1230 <= 5); l_1230 += 1)
                { /* block id: 589 */
                    int i;
                    return g_808[g_78];
                }
            }
        }
        for (g_662 = 1; (g_662 <= 5); g_662 += 1)
        { /* block id: 596 */
            return (*l_75);
        }
        --l_1235[0][7][0];
    }
    return g_488;
}


/* ------------------------------------------ */
/* 
 * reads : g_76 g_2 g_103 g_7.f4 g_7.f6 g_7.f7 g_132 g_136 g_96 g_7.f2 g_7.f3 g_78 g_7.f1 g_116 g_7.f8 g_7.f0 g_7 g_134 g_149 g_72 g_155 g_371 g_396 g_74 g_421 g_296 g_447 g_982 g_983
 * writes: g_7.f0 g_76 g_96 g_116 g_7.f7 g_132 g_134 g_136 g_149 g_155 g_74 g_7.f8 g_296 g_371 g_420 g_7.f5 g_427 g_983
 */
static int32_t ** func_84(uint16_t  p_85)
{ /* block id: 25 */
    int16_t *l_177 = &g_136;
    int32_t l_180 = 0xDA58D7D7L;
    int32_t l_187 = 0x10D2CAD3L;
    int32_t l_188 = (-1L);
    uint8_t l_193 = 0UL;
    int32_t ** const l_215 = &g_155[0];
    int8_t l_229 = 0x58L;
    const int64_t l_275 = 0x7BC0B29BC3DCDBF5LL;
    int32_t l_321 = 0x3D4998C5L;
    uint32_t l_322 = 18446744073709551613UL;
    int64_t **l_426 = &g_421;
    int64_t ***l_440[8][1];
    int32_t *l_461 = (void*)0;
    const int32_t l_469 = 0x9AE775A7L;
    int32_t l_487 = (-1L);
    int32_t l_491 = 0L;
    int32_t l_492[1][8][5] = {{{(-4L),0xF8CC0ADFL,0xAC1D1783L,0xBFFE14D0L,(-7L)},{0x98CA1395L,(-4L),(-1L),(-7L),(-6L)},{(-1L),0xAC1D1783L,(-7L),0xF8CC0ADFL,(-7L)},{(-7L),(-7L),(-1L),(-1L),0x354F4144L},{(-7L),0xBCF43DDEL,0x354F4144L,0x9FF6B908L,7L},{(-1L),0x07307C66L,(-6L),(-1L),0xBFFE14D0L},{0x98CA1395L,0xBCF43DDEL,0xBCF43DDEL,0x98CA1395L,0L},{(-4L),(-7L),0xBCF43DDEL,0x354F4144L,0x9FF6B908L}}};
    int32_t l_495 = (-10L);
    int8_t l_496 = 0x37L;
    int32_t **l_551 = &g_155[0];
    int16_t l_576 = 1L;
    int32_t *l_589 = &g_76;
    const int8_t l_658 = 0x7FL;
    const struct S0 *l_707[8];
    uint32_t *l_713 = &g_72;
    const uint32_t l_734 = 0xF8599C03L;
    int32_t **l_796 = (void*)0;
    int32_t l_867 = (-4L);
    uint32_t l_964 = 0UL;
    uint64_t l_995 = 1UL;
    struct S0 *l_1056[6][9][4] = {{{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}},{{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}},{{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{(void*)0,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}},{{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}},{{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}},{{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7},{&g_7,&g_7,&g_7,&g_7}}};
    struct S0 **l_1055 = &l_1056[5][1][0];
    struct S0 ***l_1054 = &l_1055;
    int64_t *l_1111 = &g_808[3];
    const uint64_t l_1155 = 0x739C1D6BA36D0FF7LL;
    const uint8_t l_1183[7] = {0x4AL,0x4AL,0UL,0x4AL,0x4AL,0UL,0x4AL};
    int64_t *l_1188 = &g_776[2];
    int i, j, k;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
            l_440[i][j] = &l_426;
    }
    for (i = 0; i < 8; i++)
        l_707[i] = &g_7;
lbl_403:
    for (g_7.f0 = 0; (g_7.f0 <= (-15)); g_7.f0--)
    { /* block id: 28 */
        int32_t *l_88 = &g_76;
        (*l_88) ^= 1L;
    }
    for (g_76 = 10; (g_76 <= 5); g_76 = safe_sub_func_int32_t_s_s(g_76, 7))
    { /* block id: 33 */
        uint8_t *l_95 = &g_96;
        int32_t l_99 = 0xF3046699L;
        const int32_t l_100 = 0x0FEFE548L;
        int16_t *l_178 = (void*)0;
        int16_t **l_179 = &l_178;
        int32_t l_183 = 0x0800E734L;
        int32_t l_184 = 0L;
        int32_t l_185 = 0x32E4EC8CL;
        int32_t l_186[10] = {(-3L),0x1ABEA055L,(-3L),0x1ABEA055L,(-3L),0x1ABEA055L,(-3L),0x1ABEA055L,(-3L),0x1ABEA055L};
        uint32_t l_190[5];
        int8_t l_297 = 3L;
        int32_t **l_325[4] = {&g_155[0],&g_155[0],&g_155[0],&g_155[0]};
        int64_t **l_418 = (void*)0;
        int i;
        for (i = 0; i < 5; i++)
            l_190[i] = 4294967291UL;
        if (((func_91(((*l_95) = g_2[3]), (safe_sub_func_int16_t_s_s(l_99, (l_100 , l_100))), &g_74) , 0xCC87L) , (((!(l_177 == ((*l_179) = l_178))) > p_85) , p_85)))
        { /* block id: 82 */
            if (l_180)
                break;
            return &g_155[0];
        }
        else
        { /* block id: 85 */
            int32_t *l_181 = &g_116;
            int32_t *l_182[6][4][6] = {{{&l_99,(void*)0,&g_2[1],&g_78,&g_2[2],&g_78},{&l_99,&g_2[0],&l_99,(void*)0,&g_2[1],&g_78},{&g_116,&g_2[1],&g_2[1],&l_180,&g_2[1],&g_2[1]},{&g_2[1],&g_2[0],(void*)0,&l_180,&g_2[2],(void*)0}},{{&g_116,(void*)0,(void*)0,(void*)0,&g_116,&g_2[1]},{&l_99,(void*)0,&g_2[1],&g_78,&g_2[2],&g_78},{&l_99,&g_2[0],&l_99,(void*)0,&g_2[1],&g_78},{&g_116,&g_2[1],&g_2[1],&l_180,&g_2[1],&g_2[1]}},{{&g_2[1],&g_2[0],(void*)0,&l_180,&g_2[2],(void*)0},{&g_116,(void*)0,&g_2[2],&g_78,&g_2[1],(void*)0},{(void*)0,&g_78,&l_99,&l_180,&g_116,&l_180},{(void*)0,&g_2[1],(void*)0,&g_78,&l_99,&l_180}},{{&g_2[1],(void*)0,&l_99,&g_2[0],&l_99,(void*)0},{&l_99,&g_2[1],&g_2[2],&g_2[0],&g_116,&g_78},{&g_2[1],&g_78,&g_2[2],&g_78,&g_2[1],(void*)0},{(void*)0,&g_78,&l_99,&l_180,&g_116,&l_180}},{{(void*)0,&g_2[1],(void*)0,&g_78,&l_99,&l_180},{&g_2[1],(void*)0,&l_99,&g_2[0],&l_99,(void*)0},{&l_99,&g_2[1],&g_2[2],&g_2[0],&g_116,&g_78},{&g_2[1],&g_78,&g_2[2],&g_78,&g_2[1],(void*)0}},{{(void*)0,&g_78,&l_99,&l_180,&g_116,&l_180},{(void*)0,&g_2[1],(void*)0,&g_78,&l_99,&l_180},{&g_2[1],(void*)0,&l_99,&g_2[0],&l_99,(void*)0},{&l_99,&g_2[1],&g_2[2],&g_2[0],&g_116,&g_78}}};
            int64_t l_189[7][9][4] = {{{(-10L),0x0AE0B852C9F556B5LL,(-7L),0x6DCD9579318AA528LL},{(-1L),(-1L),(-1L),0x7CED499C8587F00ELL},{0x0AE0B852C9F556B5LL,0L,0xDA225B38493D40E6LL,1L},{9L,1L,7L,0L},{0L,0L,7L,0L},{9L,7L,0xDA225B38493D40E6LL,0xC9A4B138E7317A30LL},{0x0AE0B852C9F556B5LL,0x5321BD9B3265BCB9LL,(-1L),0x2EB0A1AFBE9AB393LL},{(-1L),0x2EB0A1AFBE9AB393LL,(-7L),(-9L)},{(-10L),0x45E16A52C59D5B58LL,(-9L),1L}},{{0x78E963307FDC0412LL,0x62A19D67C5E2B0FDLL,(-9L),0L},{1L,(-1L),1L,0L},{0x6DCD9579318AA528LL,(-9L),0xBE05BE547552E8ABLL,0x31945D7D14D15A41LL},{0xFC20C0B13E44FBD2LL,8L,(-1L),(-2L)},{0L,0x081ED5E9691121BBLL,(-5L),(-9L)},{(-1L),0x0AE0B852C9F556B5LL,(-1L),0x62A19D67C5E2B0FDLL},{0x081ED5E9691121BBLL,0x91D4868CC2CD189FLL,0xD41F1B08C51EAA54LL,1L},{0x31945D7D14D15A41LL,0x6DCD9579318AA528LL,(-1L),0L},{(-1L),(-1L),7L,0L}},{{0x78B05629B9E970FDLL,5L,6L,0x54702231ABC1FECELL},{(-1L),0x5321BD9B3265BCB9LL,1L,0xBE05BE547552E8ABLL},{0x54702231ABC1FECELL,0xCBEB70EFD0CC2756LL,(-5L),0x2EB0A1AFBE9AB393LL},{(-10L),1L,1L,1L},{0L,0xAC39EC935B343962LL,0xCBEB70EFD0CC2756LL,0L},{(-9L),0x92BE114A65F372E6LL,0L,0L},{1L,1L,0xBE05BE547552E8ABLL,0x78B05629B9E970FDLL},{0x5772B18EEE2E2142LL,0x774F4B611BEE515CLL,1L,0x5321BD9B3265BCB9LL},{0xC2E1771478F816ECLL,0x78B05629B9E970FDLL,1L,1L}},{{8L,0x78B05629B9E970FDLL,0L,0x5321BD9B3265BCB9LL},{0x78B05629B9E970FDLL,(-10L),0xD199DA16F4BA2F37LL,0x7CED499C8587F00ELL},{0x62A19D67C5E2B0FDLL,6L,0L,7L},{(-9L),0L,0x50761ED409948576LL,1L},{(-1L),0x533681B2D838E576LL,(-7L),0L},{1L,0xD41F1B08C51EAA54LL,0x91D4868CC2CD189FLL,0x081ED5E9691121BBLL},{0xF527235E5902A964LL,0x37EA3EBE08290AFDLL,1L,(-1L)},{0x1EA6DD72962DCB01LL,0xBF4506700EAA7C0ELL,(-9L),8L},{1L,0x5321BD9B3265BCB9LL,5L,(-1L)}},{{(-9L),0xC2E1771478F816ECLL,0x92BE114A65F372E6LL,7L},{0xCBEB70EFD0CC2756LL,(-9L),(-9L),(-2L)},{1L,(-1L),1L,0x8D20A931D57787EELL},{0L,0x858AB291146D75CFLL,0xFC20C0B13E44FBD2LL,0xCBEB70EFD0CC2756LL},{0xF527235E5902A964LL,0x78B05629B9E970FDLL,1L,0x45E16A52C59D5B58LL},{0x858AB291146D75CFLL,0x92BE114A65F372E6LL,3L,7L},{0x62A19D67C5E2B0FDLL,0xCBEB70EFD0CC2756LL,0xC2E1771478F816ECLL,(-1L)},{0x6DCD9579318AA528LL,0xC2E1771478F816ECLL,0L,1L},{7L,0x8D20A931D57787EELL,0x6D5BFB38901DACA5LL,1L}},{{5L,1L,0x91D4868CC2CD189FLL,0xE82F0F248867B80CLL},{0L,0x081ED5E9691121BBLL,0xFC20C0B13E44FBD2LL,0x081ED5E9691121BBLL},{0xC2E1771478F816ECLL,0xBF4506700EAA7C0ELL,0xBE05BE547552E8ABLL,0x91D4868CC2CD189FLL},{(-1L),(-6L),0x37EA3EBE08290AFDLL,(-9L)},{(-9L),0x1EA6DD72962DCB01LL,(-1L),(-1L)},{(-9L),6L,0x37EA3EBE08290AFDLL,(-2L)},{(-1L),(-1L),0xBE05BE547552E8ABLL,8L},{0xC2E1771478F816ECLL,5L,0xFC20C0B13E44FBD2LL,(-9L)},{0L,0x858AB291146D75CFLL,0x91D4868CC2CD189FLL,0x5321BD9B3265BCB9LL}},{{5L,0x92BE114A65F372E6LL,0x6D5BFB38901DACA5LL,0xAC39EC935B343962LL},{7L,(-9L),0L,(-10L)},{0x6DCD9579318AA528LL,0x1EA6DD72962DCB01LL,0xC2E1771478F816ECLL,(-1L)},{0x62A19D67C5E2B0FDLL,0x533681B2D838E576LL,3L,1L},{0x858AB291146D75CFLL,0xDA225B38493D40E6LL,1L,(-9L)},{0xF527235E5902A964LL,5L,0xFC20C0B13E44FBD2LL,0xE82F0F248867B80CLL},{0L,0xD41F1B08C51EAA54LL,1L,8L},{1L,(-6L),(-9L),1L},{0xCBEB70EFD0CC2756LL,0L,0x92BE114A65F372E6LL,(-10L)}}};
            uint16_t *l_214 = (void*)0;
            uint16_t *l_228[7][6][6] = {{{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149},{&g_149,(void*)0,(void*)0,&g_149,&g_149,&g_149},{&g_149,(void*)0,&g_149,&g_149,(void*)0,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,(void*)0,&g_149,(void*)0,&g_149}},{{&g_149,&g_149,&g_149,&g_149,(void*)0,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,(void*)0,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,(void*)0,&g_149,&g_149,(void*)0},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149}},{{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,(void*)0,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,(void*)0,&g_149,&g_149,(void*)0}},{{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,(void*)0,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149}},{{(void*)0,&g_149,(void*)0,&g_149,&g_149,(void*)0},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,(void*)0,&g_149,&g_149}},{{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,(void*)0,&g_149,&g_149,(void*)0},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{&g_149,&g_149,(void*)0,&g_149,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149}},{{&g_149,&g_149,(void*)0,(void*)0,&g_149,&g_149},{&g_149,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,(void*)0,&g_149,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,&g_149,&g_149},{(void*)0,&g_149,&g_149,&g_149,(void*)0,(void*)0},{(void*)0,&g_149,&g_149,(void*)0,(void*)0,&g_149}}};
            uint8_t *l_242 = &g_96;
            uint8_t l_293 = 0x4DL;
            uint32_t l_353 = 0UL;
            uint8_t l_428[6][2][7] = {{{0xAFL,0xAFL,0xD0L,0xAFL,0xAFL,0xD0L,0xAFL},{0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L,0xAFL}},{{0x24L,0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L},{0xAFL,0xAFL,0xD0L,0xAFL,0xAFL,0xD0L,0xAFL}},{{0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L,0xAFL},{0x24L,0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L}},{{0xAFL,0xAFL,0xD0L,0xAFL,0xAFL,0xD0L,0xAFL},{0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L,0xAFL}},{{0x24L,0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L},{0xAFL,0xAFL,0xD0L,0xAFL,0xAFL,0xD0L,0xAFL}},{{0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L,0xAFL},{0x24L,0xAFL,0x24L,0x24L,0xAFL,0x24L,0x24L}}};
            int i, j, k;
            (*l_181) = 0xC18CA032L;
            --l_190[0];
            l_193++;
            if (((g_7 , ((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_uint8_t_u_s(((safe_mul_func_int16_t_s_s((l_180 < (((safe_div_func_int32_t_s_s((safe_add_func_int16_t_s_s(((safe_add_func_uint16_t_u_u(((-2L) == ((safe_div_func_int16_t_s_s(((safe_lshift_func_int16_t_s_s((l_188 |= (safe_lshift_func_uint16_t_u_u(((*l_181) = p_85), 5))), g_78)) && ((((void*)0 != l_215) <= (safe_lshift_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u((p_85 < ((((l_184 = (safe_mod_func_uint64_t_u_u((safe_add_func_int8_t_s_s(((safe_mul_func_int16_t_s_s((-1L), (*l_181))) & p_85), p_85)), l_186[0]))) && p_85) , g_7.f3) != (*l_181))), g_134)) > p_85), 5))) == l_229)), 0x9219L)) && 5UL)), 0x3408L)) || g_149), g_7.f3)), p_85)) ^ (-4L)) | g_7.f4)), g_7.f3)) && (-1L)), g_72)) >= l_190[0]), 1)) != 0x705C1F3BL)) | l_186[6]))
            { /* block id: 92 */
                uint32_t l_263 = 4UL;
                int32_t l_298 = 4L;
                int32_t l_319 = (-1L);
                int32_t l_320[1][4];
                int i, j;
                for (i = 0; i < 1; i++)
                {
                    for (j = 0; j < 4; j++)
                        l_320[i][j] = 0x5B31C882L;
                }
                for (g_136 = (-14); (g_136 >= (-22)); g_136 = safe_sub_func_uint16_t_u_u(g_136, 5))
                { /* block id: 95 */
                    int8_t l_243 = 0x11L;
                    int32_t **l_244[2][4] = {{&l_181,&l_181,&l_182[5][3][2],&l_181},{&l_181,&l_182[5][3][2],&l_182[5][3][2],&l_181}};
                    uint32_t * const l_247[2][10][4] = {{{(void*)0,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&l_190[0],&g_72},{&g_72,(void*)0,(void*)0,&g_72},{(void*)0,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&l_190[0],&g_72},{&g_72,(void*)0,(void*)0,&g_72},{(void*)0,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&l_190[0],&g_72},{&g_72,(void*)0,(void*)0,&g_72},{(void*)0,&g_72,(void*)0,(void*)0}},{{&g_72,&g_72,&l_190[0],&g_72},{&g_72,(void*)0,(void*)0,&g_72},{(void*)0,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&l_190[0],&g_72},{&g_72,(void*)0,(void*)0,&g_72},{(void*)0,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&l_190[0],&g_72},{&g_72,(void*)0,(void*)0,&g_72},{(void*)0,&g_72,(void*)0,(void*)0},{&g_72,&g_72,&l_190[0],(void*)0}}};
                    int8_t *l_264 = &l_229;
                    int64_t *l_265 = (void*)0;
                    int64_t *l_266 = &l_189[2][0][3];
                    int64_t *l_267 = (void*)0;
                    int64_t *l_268 = &g_74;
                    int16_t *l_301 = (void*)0;
                    int16_t *l_302 = &g_296;
                    uint64_t *l_310 = (void*)0;
                    uint64_t *l_311 = &g_132;
                    int i, j, k;
                    for (g_149 = 0; (g_149 == 56); g_149++)
                    { /* block id: 98 */
                        struct S0 l_234 = {0x2F10878EL,59,7,-48,0xC0L,88,1087,-98,7541};
                        uint32_t l_236 = 0x7B6BF432L;
                        l_234.f7 = (((l_234 , (!(l_236 < ((*l_181) = (l_234.f5 > (safe_div_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((0L >= (((!((0x9E75L <= p_85) ^ p_85)) >= 0x0F07E736L) <= ((((l_177 == l_177) <= g_7.f2) , &g_96) != l_242))), 255UL)), 0x79L))))))) >= p_85) | g_7.f2);
                        if (l_243)
                            continue;
                        if (p_85)
                            continue;
                        (*l_181) = p_85;
                    }
                    if ((((l_244[0][0] != (g_7.f8 , ((safe_lshift_func_uint8_t_u_u((&l_186[6] != (((void*)0 == l_247[0][8][1]) , &l_186[2])), (safe_add_func_int32_t_s_s((safe_div_func_int64_t_s_s(((*l_268) = (safe_div_func_int64_t_s_s(((*l_266) = (safe_rshift_func_int8_t_s_u((~(safe_add_func_uint32_t_u_u(((safe_mul_func_int8_t_s_s(g_2[2], ((-9L) < ((*l_264) |= (((safe_div_func_uint64_t_u_u(l_263, p_85)) >= g_136) && 6L))))) == p_85), 0L))), p_85))), 18446744073709551615UL))), g_7.f4)), p_85)))) , (void*)0))) & g_134) <= (-1L)))
                    { /* block id: 108 */
                        if (p_85)
                            break;
                    }
                    else
                    { /* block id: 110 */
                        uint8_t l_269 = 0x00L;
                        int8_t *l_294[8][2] = {{(void*)0,&l_243},{(void*)0,&l_243},{(void*)0,&l_243},{(void*)0,&l_243},{(void*)0,&l_243},{(void*)0,&l_243},{(void*)0,&l_243},{(void*)0,&l_243}};
                        int32_t l_295 = 0x767B892AL;
                        int i, j;
                        l_269--;
                        (*l_181) = ((safe_div_func_uint8_t_u_u(((safe_unary_minus_func_int64_t_s(l_275)) & (safe_mod_func_uint8_t_u_u((safe_div_func_uint32_t_u_u(g_103[7][1], 0x02231B27L)), ((*l_95) = (((l_298 = ((l_263 >= ((((safe_rshift_func_int8_t_s_s(p_85, (safe_rshift_func_int16_t_s_s(((safe_mul_func_uint8_t_u_u((safe_div_func_uint16_t_u_u(((g_296 = (l_295 = ((((*l_264) ^= (+((g_7.f8 = g_103[0][0]) & (((p_85 < (safe_rshift_func_int16_t_s_u((safe_mod_func_int64_t_s_s(((*l_266) = ((g_134 = (-5L)) ^ (l_185 |= 0xC25BL))), l_269)), 3))) || l_293) && 0x11993899L)))) >= l_269) , g_7.f8))) >= p_85), 4L)), p_85)) == g_2[1]), l_297)))) & 18446744073709551615UL) || g_72) & p_85)) , 6L)) , p_85) | p_85))))), g_7.f0)) == p_85);
                        return &g_155[0];
                    }
                    (*l_181) |= 0x189C1302L;
                    if ((((p_85 = ((safe_rshift_func_int16_t_s_s(((*l_302) = g_7.f4), 14)) <= (safe_lshift_func_uint16_t_u_s((!(safe_rshift_func_int16_t_s_s(g_7.f3, 14))), g_78)))) ^ (((safe_mod_func_int64_t_s_s((*l_181), ((*l_311)++))) , ((safe_sub_func_int8_t_s_s(((void*)0 != (*l_179)), (l_100 >= (g_134 , ((safe_rshift_func_int8_t_s_s(g_72, g_136)) , 0x6B0DL))))) == 0x61L)) > 9L)) != g_7.f4))
                    { /* block id: 128 */
                        (*l_181) ^= l_185;
                    }
                    else
                    { /* block id: 130 */
                        const int64_t l_318[4][5] = {{0L,0x274297AA096E82DCLL,0L,0L,0x274297AA096E82DCLL},{1L,0xF3F56BA5D0A36D6BLL,0xF3F56BA5D0A36D6BLL,1L,0xF3F56BA5D0A36D6BLL},{0x274297AA096E82DCLL,0x274297AA096E82DCLL,(-1L),0x274297AA096E82DCLL,0x274297AA096E82DCLL},{0xF3F56BA5D0A36D6BLL,1L,0xF3F56BA5D0A36D6BLL,0xF3F56BA5D0A36D6BLL,1L}};
                        int i, j;
                        if (l_318[0][1])
                            break;
                    }
                }
                --l_322;
                return l_325[1];
            }
            else
            { /* block id: 136 */
                uint16_t l_333 = 0xA1A5L;
                int32_t l_340[6] = {1L,1L,1L,1L,1L,1L};
                int16_t **l_399 = &l_177;
                int64_t ***l_419[2];
                uint32_t *l_424 = (void*)0;
                uint32_t *l_425 = &l_190[2];
                uint8_t l_429[4][1][7] = {{{0x42L,0x46L,0x98L,0x42L,0x98L,0x46L,0x42L}},{{0xADL,0x42L,0x46L,0x98L,0x42L,0x98L,0x46L}},{{0x42L,0x42L,0xAFL,7UL,0UL,0xAFL,0UL}},{{7UL,0x46L,0x46L,7UL,0x98L,0xADL,7UL}}};
                int32_t l_445 = 8L;
                int32_t **l_446 = (void*)0;
                int i, j, k;
                for (i = 0; i < 2; i++)
                    l_419[i] = &l_418;
                for (l_322 = 0; (l_322 < 10); l_322++)
                { /* block id: 139 */
                    int32_t *l_341 = &g_7.f0;
                    int32_t l_354 = 0L;
                    int16_t *l_372 = (void*)0;
                    int32_t l_401 = 0x8F92EB95L;
                    if ((safe_unary_minus_func_uint16_t_u(((l_188 |= (((safe_div_func_int64_t_s_s(g_78, ((func_91((safe_add_func_uint64_t_u_u(((l_333 &= (65531UL < g_7.f3)) > p_85), ((safe_rshift_func_int8_t_s_u((-10L), ((safe_div_func_int64_t_s_s((((*l_341) ^= ((safe_rshift_func_int8_t_s_s(l_340[4], (p_85 < ((*l_215) != (void*)0)))) == 4L)) , 0x4EC2A0033338E9F5LL), g_76)) <= 0xC1BBL))) != g_7.f4))), l_340[5], &g_74) , g_116) , g_103[2][1]))) && 0xA970B7FCL) && p_85)) && g_7.f2))))
                    { /* block id: 143 */
                        int16_t l_344 = 0x4E84L;
                        uint32_t *l_370[10][1] = {{&g_371},{&g_371},{&g_371},{&g_371},{&g_371},{&g_371},{&g_371},{&g_371},{&g_371},{&g_371}};
                        uint64_t *l_373 = &g_132;
                        int32_t l_374 = 0L;
                        int i, j;
                        l_340[4] = (safe_add_func_uint8_t_u_u(1UL, (l_344 <= (g_149 = ((g_7.f6 | (safe_div_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((safe_add_func_int32_t_s_s(((*l_181) = (safe_lshift_func_int8_t_s_s(1L, 0))), l_353)), 5)), 0x39F4L))) != l_354)))));
                        if (p_85)
                            break;
                        l_374 = ((*l_181) = (safe_sub_func_int64_t_s_s(0xB2803182638C486ALL, (((((*l_373) = (((((safe_rshift_func_int8_t_s_u(((void*)0 == (*l_215)), g_7.f2)) || (safe_sub_func_uint64_t_u_u(p_85, g_7.f3))) == (((safe_sub_func_uint64_t_u_u((&g_134 == ((g_371 &= (safe_sub_func_int8_t_s_s(((safe_mod_func_uint32_t_u_u((safe_add_func_int32_t_s_s((l_340[4] |= 6L), (+9L))), (-1L))) == 0xA10CC8CFL), l_344))) , l_372)), (-10L))) > 0L) <= g_134)) == 0xF1L) | p_85)) && g_7.f1) , 0x07B0L) > (*l_181)))));
                    }
                    else
                    { /* block id: 153 */
                        uint64_t *l_400 = &g_132;
                        int32_t l_402 = 1L;
                        (*l_181) = l_354;
                        (*l_181) = (safe_lshift_func_int8_t_s_u((((safe_mul_func_int8_t_s_s((safe_mul_func_uint8_t_u_u((l_340[4] = (l_372 == l_372)), (p_85 < g_7.f8))), ((+(safe_sub_func_int64_t_s_s((safe_rshift_func_int8_t_s_s(((safe_sub_func_uint8_t_u_u((((l_402 &= (safe_lshift_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u(((safe_sub_func_int32_t_s_s(0x8B24F25BL, (((*l_400) = (l_354 |= (((((safe_add_func_int8_t_s_s((((g_396 & (((safe_rshift_func_int8_t_s_u(p_85, 1)) | ((**l_399) = (((((void*)0 != l_399) , g_136) & g_7.f4) == (*l_181)))) , p_85)) , p_85) ^ g_116), g_7.f2)) <= 1UL) , g_2[1]) <= 0xCEB4E095L) & g_132))) >= g_7.f2))) >= l_401), g_103[7][1])) , (-4L)), 7))) ^ p_85) , l_401), 0x22L)) , (-5L)), 4)), g_7.f3))) , l_401))) , l_340[4]) <= 0L), p_85));
                        if (g_7.f1)
                            goto lbl_403;
                        (*l_215) = &g_2[2];
                    }
                }
                (*l_215) = (*l_215);
                l_429[0][0][1] = ((*l_181) = (safe_sub_func_uint16_t_u_u(((safe_add_func_uint8_t_u_u(((((*l_242) = (safe_lshift_func_uint8_t_u_u(0xE4L, (safe_div_func_uint32_t_u_u(((safe_mod_func_uint16_t_u_u((0x76EC4814L ^ (safe_mul_func_uint16_t_u_u((g_149 = ((safe_sub_func_uint8_t_u_u((((g_72 == ((g_420[1][0][6] = l_418) != (g_427 = (((safe_sub_func_uint32_t_u_u(0UL, (g_7.f5 = ((*l_425) ^= g_74)))) || ((void*)0 == &g_103[4][1])) , l_426)))) , &l_189[2][0][3]) == (*l_426)), l_333)) , g_96)), p_85))), l_428[2][1][6])) >= l_340[0]), p_85))))) , p_85) <= p_85), 0x8EL)) < g_7.f0), 0UL)));
                if ((safe_mul_func_int16_t_s_s((safe_lshift_func_int8_t_s_s(((((((p_85 || (((safe_lshift_func_uint16_t_u_s(0x11FCL, ((((l_445 |= (l_321 &= (((p_85 || (l_340[4] = ((*g_421) = 0L))) | (safe_mul_func_uint8_t_u_u(((248UL >= (((void*)0 != l_440[4][0]) >= (safe_mul_func_int8_t_s_s(((((safe_add_func_int32_t_s_s(((((*l_425) = (g_116 | 0xF9CC3CFD5BAA6193LL)) , p_85) && p_85), g_296)) <= 0x1F3FL) && 0L) , 0x14L), 0x08L)))) , p_85), p_85))) & 0x38B7L))) , &g_155[0]) == l_446) & 0xABL))) , 0xD4E5L) , g_116)) , g_96) != g_447) ^ g_134) ^ g_132) != (-9L)), g_7.f2)), p_85)))
                { /* block id: 179 */
                    int8_t *l_456[7][7] = {{&l_229,&l_297,&l_297,&l_229,&l_297,&l_297,&l_229},{&l_297,&l_297,&l_229,&l_297,&l_229,(void*)0,&l_297},{&l_229,&l_297,(void*)0,&l_297,&l_297,(void*)0,&l_297},{&l_229,(void*)0,(void*)0,(void*)0,&l_297,&l_297,&l_297},{&l_297,&l_229,&l_229,&l_297,(void*)0,&l_229,&l_297},{(void*)0,&l_297,&l_297,(void*)0,&l_229,&l_229,(void*)0},{(void*)0,&l_297,(void*)0,&l_297,&l_229,&l_229,&l_229}};
                    int i, j;
                    (*l_181) = (((safe_div_func_uint8_t_u_u((p_85 > (safe_rshift_func_int8_t_s_u(((((safe_unary_minus_func_int16_t_s(((**l_399) = p_85))) || ((p_85 > p_85) | g_7.f3)) , (((p_85 != (~(safe_mul_func_int8_t_s_s((l_188 &= (&g_420[1][0][6] != (((g_76 <= g_103[1][1]) & p_85) , (void*)0))), 0x9DL)))) == (*g_421)) , &l_189[6][7][3])) != (void*)0), g_72))), g_103[0][1])) && (-6L)) , 0x6F7F3574L);
                    if (p_85)
                        continue;
                    return &g_155[0];
                }
                else
                { /* block id: 185 */
                    struct S0 *l_457 = &g_7;
                    struct S0 **l_458 = &l_457;
                    (*l_458) = l_457;
                    return l_446;
                }
            }
        }
    }
    for (l_193 = 0; (l_193 <= 1); l_193 += 1)
    { /* block id: 194 */
        int32_t *l_466[3];
        uint16_t *l_467 = (void*)0;
        uint16_t *l_468 = &g_149;
        int16_t l_470 = 8L;
        uint8_t *l_480[7][1][3] = {{{(void*)0,(void*)0,(void*)0}},{{&g_96,&g_96,&g_96}},{{(void*)0,(void*)0,(void*)0}},{{&g_96,&g_96,&g_96}},{{(void*)0,(void*)0,(void*)0}},{{&g_96,&g_96,&g_96}},{{(void*)0,(void*)0,(void*)0}}};
        int32_t *l_501 = &g_78;
        int32_t l_614 = 0x956C7C77L;
        uint64_t l_624 = 18446744073709551609UL;
        int32_t *l_629 = &g_493;
        int64_t *l_659 = &g_74;
        uint8_t l_688 = 1UL;
        uint64_t l_753 = 0x3606427B08E8B46ALL;
        struct S0 l_775 = {-5L,170,8,-80,0xCFL,16,581,-165,5415};
        int8_t l_787 = 0x88L;
        uint32_t *l_839 = &l_322;
        uint32_t **l_838 = &l_839;
        uint32_t l_842 = 18446744073709551614UL;
        int16_t **l_846 = (void*)0;
        struct S0 * const l_929[2] = {(void*)0,(void*)0};
        struct S0 * const *l_928 = &l_929[0];
        struct S0 * const **l_927 = &l_928;
        const int32_t *l_941[9][2][1] = {{{&g_942[3][0]},{&g_942[3][0]}},{{(void*)0},{&g_942[3][0]}},{{&g_942[3][0]},{(void*)0}},{{&g_942[3][0]},{&g_942[3][0]}},{{(void*)0},{&g_942[3][0]}},{{&g_942[3][0]},{(void*)0}},{{&g_942[3][0]},{&g_942[3][0]}},{{(void*)0},{&g_942[3][0]}},{{&g_942[3][0]},{(void*)0}}};
        uint16_t l_1090 = 0x22BAL;
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_466[i] = &l_188;
        l_187 = ((((((l_188 ^= ((safe_sub_func_uint64_t_u_u(((void*)0 != l_461), (65526UL && (((p_85 , ((*l_468) = (safe_sub_func_int8_t_s_s((((l_180 = ((0L <= (0x5AE1L != (((void*)0 == &g_103[0][0]) > g_136))) | p_85)) , l_193) , g_7.f7), (-6L))))) , 0xBA0C1B1AL) == l_469)))) , p_85)) || 0x05681FB2L) > l_470) < 0xF0AE782E7FEB9FB2LL) == 0x1562L) || 5L);
    }
    l_180 = (safe_div_func_uint16_t_u_u((((*l_589) ^= 0x4A182CC1L) || (safe_lshift_func_int8_t_s_u((~(((l_1183[0] == ((+p_85) > (((*l_177) &= (*l_589)) != (p_85 != (~((((*l_426) != l_1188) && ((*g_982) = (*g_982))) <= p_85)))))) , p_85) , (*l_589))), g_7.f2))), p_85));
    return &g_155[0];
}


/* ------------------------------------------ */
/* 
 * reads : g_103 g_7.f4 g_7.f6 g_7.f7 g_132 g_136 g_96 g_76 g_7.f2 g_7.f3 g_78 g_7.f1 g_116 g_7.f8 g_2 g_7.f0 g_134 g_7
 * writes: g_116 g_7.f7 g_132 g_134 g_136 g_149 g_96 g_155
 */
static struct S0  func_91(uint8_t  p_92, uint32_t  p_93, int64_t * p_94)
{ /* block id: 35 */
    uint8_t l_119 = 253UL;
    const int64_t *l_120 = &g_74;
    uint8_t *l_163 = (void*)0;
    int32_t *l_164 = &g_116;
    int32_t l_165[10][10][2] = {{{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L}},{{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L}},{{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L}},{{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L}},{{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L}},{{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L}},{{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L}},{{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L}},{{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L}},{{0L,0xB4E33999L},{0L,0x4DF40108L},{0L,7L},{0x5E5F9656L,7L},{0L,0x4DF40108L},{0L,0xB4E33999L},{0L,0x486E5B77L},{0x5E5F9656L,0x486E5B77L},{0L,0xB4E33999L},{0L,0x4DF40108L}}};
    uint64_t l_173 = 0xDF4D21E74E04448CLL;
    int i, j, k;
    for (p_93 = 0; (p_93 <= 48); ++p_93)
    { /* block id: 38 */
        uint32_t *l_108 = &g_72;
        int32_t l_123 = 0x66612900L;
        uint8_t l_124[7];
        const int32_t *l_144 = &l_123;
        const int32_t **l_143 = &l_144;
        uint16_t l_160 = 1UL;
        int i;
        for (i = 0; i < 7; i++)
            l_124[i] = 0UL;
        for (p_92 = 0; (p_92 <= 1); p_92 += 1)
        { /* block id: 41 */
            int32_t *l_115[3];
            int64_t *l_122 = &g_74;
            int64_t **l_121 = &l_122;
            uint8_t *l_125[4][10][4] = {{{&l_124[1],&l_124[1],(void*)0,&g_96},{&l_124[1],&g_96,&l_124[1],&l_119},{&l_124[1],&l_119,&l_124[0],&g_7.f4},{(void*)0,&g_96,&g_96,&l_124[1]},{&l_124[1],&l_124[1],&g_96,&g_7.f4},{&g_7.f4,&g_7.f4,&l_124[6],&l_124[1]},{&l_119,(void*)0,&l_124[1],&l_124[1]},{(void*)0,&g_96,&l_119,&g_96},{&g_96,&g_7.f4,&g_96,&g_96},{&l_119,&l_124[1],&l_119,&l_124[1]}},{{&l_124[1],&l_119,(void*)0,&l_124[5]},{&l_124[1],&l_124[1],&l_124[1],&l_119},{&g_96,(void*)0,&l_124[1],&g_7.f4},{&l_124[1],(void*)0,(void*)0,&g_96},{&l_124[1],&l_124[0],&l_119,&l_124[1]},{&l_119,&l_124[1],&g_96,&l_124[1]},{&g_96,&l_124[0],&l_119,&g_7.f4},{(void*)0,&g_96,&l_124[1],&l_124[5]},{&l_119,&l_124[1],&l_124[6],&l_124[1]},{&g_7.f4,&g_96,&g_96,(void*)0}},{{&l_124[1],&l_124[0],&g_96,&l_119},{(void*)0,&g_96,&l_124[0],&g_7.f4},{&l_119,&l_124[1],&l_124[1],&g_7.f4},{(void*)0,&l_124[1],&l_119,&g_96},{(void*)0,&g_7.f4,&l_124[1],&l_124[1]},{&l_124[0],&l_124[0],&g_96,&l_119},{&g_96,&g_7.f4,&l_124[1],(void*)0},{&l_119,&l_119,&l_119,&l_124[1]},{&l_124[1],&l_119,&l_119,(void*)0},{&l_119,&g_7.f4,&l_124[1],&l_119}},{{&l_119,&l_124[0],&l_124[0],&l_124[1]},{&g_96,&g_7.f4,(void*)0,&g_96},{&l_124[1],&l_124[1],&l_124[6],&g_7.f4},{&g_96,&l_124[1],&l_124[1],&g_7.f4},{&l_124[0],&g_96,(void*)0,&l_119},{(void*)0,&l_124[0],&g_7.f4,(void*)0},{&l_119,&g_96,&l_119,&l_124[1]},{(void*)0,&l_124[1],&l_119,&l_124[5]},{&g_96,&g_96,&g_96,&g_7.f4},{&l_119,&l_124[0],&l_124[1],&l_124[1]}}};
            int16_t *l_133 = &g_134;
            int16_t *l_135 = &g_136;
            uint32_t l_145 = 18446744073709551615UL;
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_115[i] = &g_116;
            g_7.f7 ^= (((l_123 = ((((safe_lshift_func_uint16_t_u_s((safe_lshift_func_uint16_t_u_u(((l_108 != &g_72) != (safe_mul_func_int8_t_s_s((g_103[(p_92 + 1)][p_92] | (safe_sub_func_uint8_t_u_u(((safe_mod_func_int32_t_s_s((g_116 = g_103[(p_92 + 4)][p_92]), (safe_sub_func_uint8_t_u_u(l_119, (1UL || p_93))))) & (((l_120 != ((*l_121) = (void*)0)) <= g_103[2][0]) | l_123)), 1L))), l_123))), g_7.f4)), l_124[1])) != l_119) | p_93) && p_93)) , g_7.f6) != 0UL);
            if ((g_116 |= ((((safe_sub_func_uint16_t_u_u(((safe_mod_func_int32_t_s_s((((safe_rshift_func_int16_t_s_s((((g_132 ^= 0L) <= p_92) , (((*l_135) |= ((*l_133) = 9L)) <= (safe_lshift_func_int16_t_s_s(((safe_div_func_int64_t_s_s((((safe_sub_func_uint64_t_u_u((g_96 & 1UL), (l_143 == &l_144))) ^ (((((&l_122 == &p_94) && 0x2C573671L) , p_92) , 0xECD7L) != g_76)) & (*l_144)), (**l_143))) , 0x5839L), g_7.f2)))), 12)) != (-1L)) < g_7.f3), g_78)) || g_136), g_7.f7)) >= 0x11L) >= 7L) , g_7.f1)))
            { /* block id: 50 */
                g_116 &= p_92;
            }
            else
            { /* block id: 52 */
                uint16_t *l_148 = &g_149;
                int32_t l_152 = 0L;
                uint64_t *l_153 = &g_132;
                int32_t l_156 = 0xA92FA667L;
                g_7.f7 |= ((l_145 , (safe_div_func_uint16_t_u_u((((*l_148) = g_78) ^ (-1L)), (7L | ((safe_mod_func_uint64_t_u_u(((*l_153) &= (((((void*)0 == &g_2[2]) ^ (((g_7.f8 , ((g_96 = (g_7.f1 & (g_2[1] , g_2[1]))) , g_7.f2)) <= 0x48BC0DD118ECCF47LL) , g_116)) & l_119) == l_152)), g_103[5][1])) ^ (**l_143)))))) > 1UL);
                l_152 ^= (g_7.f0 < g_2[1]);
                for (l_123 = 0; (l_123 <= 1); l_123 += 1)
                { /* block id: 60 */
                    int32_t *l_154 = (void*)0;
                    int32_t l_157 = 0x2FF17D06L;
                    for (g_134 = 0; (g_134 <= 1); g_134 += 1)
                    { /* block id: 63 */
                        int32_t l_158 = 4L;
                        int32_t l_159 = 8L;
                        g_155[0] = l_154;
                        if (l_119)
                            continue;
                        ++l_160;
                    }
                }
            }
        }
    }
    l_165[7][5][0] &= ((*l_164) = ((l_163 = &p_92) == &l_119));
    for (l_119 = 15; (l_119 < 34); l_119++)
    { /* block id: 77 */
        int32_t *l_168 = &g_116;
        int32_t *l_169 = &g_116;
        int32_t *l_170 = &l_165[7][5][0];
        int32_t *l_171 = &g_116;
        int32_t *l_172[8] = {&g_78,&g_2[1],&g_78,&g_2[1],&g_78,&g_2[1],&g_78,&g_2[1]};
        int i;
        ++l_173;
    }
    return g_7;
}




/* ---------------------------------------- */
//testcase_id 1484153244
int case1484153244(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_2[i], "g_2[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_7.f0, "g_7.f0", print_hash_value);
    transparent_crc(g_7.f1, "g_7.f1", print_hash_value);
    transparent_crc(g_7.f2, "g_7.f2", print_hash_value);
    transparent_crc(g_7.f3, "g_7.f3", print_hash_value);
    transparent_crc(g_7.f4, "g_7.f4", print_hash_value);
    transparent_crc(g_7.f5, "g_7.f5", print_hash_value);
    transparent_crc(g_7.f6, "g_7.f6", print_hash_value);
    transparent_crc(g_7.f7, "g_7.f7", print_hash_value);
    transparent_crc(g_7.f8, "g_7.f8", print_hash_value);
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_72, "g_72", print_hash_value);
    transparent_crc(g_74, "g_74", print_hash_value);
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_78, "g_78", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_103[i][j], "g_103[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_132, "g_132", print_hash_value);
    transparent_crc(g_134, "g_134", print_hash_value);
    transparent_crc(g_136, "g_136", print_hash_value);
    transparent_crc(g_149, "g_149", print_hash_value);
    transparent_crc(g_296, "g_296", print_hash_value);
    transparent_crc(g_371, "g_371", print_hash_value);
    transparent_crc(g_396, "g_396", print_hash_value);
    transparent_crc(g_447, "g_447", print_hash_value);
    transparent_crc(g_488, "g_488", print_hash_value);
    transparent_crc(g_493, "g_493", print_hash_value);
    transparent_crc(g_526, "g_526", print_hash_value);
    transparent_crc(g_611, "g_611", print_hash_value);
    transparent_crc(g_662, "g_662", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_752, "g_752", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_776[i], "g_776[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_808[i], "g_808[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_876, "g_876", print_hash_value);
    transparent_crc(g_926, "g_926", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_942[i][j], "g_942[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_955, "g_955", print_hash_value);
    transparent_crc(g_983, "g_983", print_hash_value);
    transparent_crc(g_1107, "g_1107", print_hash_value);
    transparent_crc(g_1209, "g_1209", print_hash_value);
    transparent_crc(g_1277, "g_1277", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_1289[i], "g_1289[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1410, "g_1410", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 7; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_1473[i][j][k], "g_1473[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1567, "g_1567", print_hash_value);
    transparent_crc(g_1814, "g_1814", print_hash_value);
    transparent_crc(g_1817, "g_1817", print_hash_value);
    transparent_crc(g_1830, "g_1830", print_hash_value);
    transparent_crc(g_1849, "g_1849", print_hash_value);
    transparent_crc(g_1909, "g_1909", print_hash_value);
    transparent_crc(g_2029, "g_2029", print_hash_value);
    transparent_crc(g_2030, "g_2030", print_hash_value);
    transparent_crc(g_2106.f0, "g_2106.f0", print_hash_value);
    transparent_crc(g_2106.f1, "g_2106.f1", print_hash_value);
    transparent_crc(g_2106.f2, "g_2106.f2", print_hash_value);
    transparent_crc(g_2106.f3, "g_2106.f3", print_hash_value);
    transparent_crc(g_2106.f4, "g_2106.f4", print_hash_value);
    transparent_crc(g_2106.f5, "g_2106.f5", print_hash_value);
    transparent_crc(g_2106.f6, "g_2106.f6", print_hash_value);
    transparent_crc(g_2106.f7, "g_2106.f7", print_hash_value);
    transparent_crc(g_2106.f8, "g_2106.f8", print_hash_value);
    transparent_crc(g_2267, "g_2267", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_2308[i], "g_2308[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2357, "g_2357", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_2390[i].f0, "g_2390[i].f0", print_hash_value);
        transparent_crc(g_2390[i].f1, "g_2390[i].f1", print_hash_value);
        transparent_crc(g_2390[i].f2, "g_2390[i].f2", print_hash_value);
        transparent_crc(g_2390[i].f3, "g_2390[i].f3", print_hash_value);
        transparent_crc(g_2390[i].f4, "g_2390[i].f4", print_hash_value);
        transparent_crc(g_2390[i].f5, "g_2390[i].f5", print_hash_value);
        transparent_crc(g_2390[i].f6, "g_2390[i].f6", print_hash_value);
        transparent_crc(g_2390[i].f7, "g_2390[i].f7", print_hash_value);
        transparent_crc(g_2390[i].f8, "g_2390[i].f8", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2690, "g_2690", print_hash_value);
    transparent_crc(g_2905, "g_2905", print_hash_value);
    transparent_crc(g_2907, "g_2907", print_hash_value);
    transparent_crc(g_2919, "g_2919", print_hash_value);
    transparent_crc(g_2945, "g_2945", print_hash_value);
    transparent_crc(g_2963, "g_2963", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_3003[i][j][k], "g_3003[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 1
breakdown:
   depth: 0, occurrence: 751
   depth: 1, occurrence: 15
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 7
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 39
breakdown:
   indirect level: 0, occurrence: 15
   indirect level: 1, occurrence: 11
   indirect level: 2, occurrence: 3
   indirect level: 3, occurrence: 7
   indirect level: 4, occurrence: 3
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 61
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 42
XXX times a single bitfield on LHS: 25
XXX times a single bitfield on RHS: 315

XXX max expression depth: 51
breakdown:
   depth: 1, occurrence: 119
   depth: 2, occurrence: 31
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
   depth: 8, occurrence: 1
   depth: 11, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 16, occurrence: 1
   depth: 18, occurrence: 3
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 2
   depth: 25, occurrence: 2
   depth: 29, occurrence: 2
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 36, occurrence: 1
   depth: 37, occurrence: 1
   depth: 39, occurrence: 1
   depth: 41, occurrence: 1
   depth: 51, occurrence: 1

XXX total number of pointers: 516

XXX times a variable address is taken: 1794
XXX times a pointer is dereferenced on RHS: 314
breakdown:
   depth: 1, occurrence: 243
   depth: 2, occurrence: 63
   depth: 3, occurrence: 8
XXX times a pointer is dereferenced on LHS: 384
breakdown:
   depth: 1, occurrence: 337
   depth: 2, occurrence: 40
   depth: 3, occurrence: 4
   depth: 4, occurrence: 2
   depth: 5, occurrence: 1
XXX times a pointer is compared with null: 61
XXX times a pointer is compared with address of another variable: 19
XXX times a pointer is compared with another pointer: 18
XXX times a pointer is qualified to be dereferenced: 10637

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1482
   level: 2, occurrence: 391
   level: 3, occurrence: 49
   level: 4, occurrence: 11
   level: 5, occurrence: 4
XXX number of pointers point to pointers: 233
XXX number of pointers point to scalars: 268
XXX number of pointers point to structs: 15
XXX percent of pointers has null in alias set: 34.5
XXX average alias set size: 1.5

XXX times a non-volatile is read: 2506
XXX times a non-volatile is write: 1277
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 1
XXX backward jumps: 7

XXX stmts: 122
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 23
   depth: 2, occurrence: 20
   depth: 3, occurrence: 14
   depth: 4, occurrence: 16
   depth: 5, occurrence: 20

XXX percentage a fresh-made variable is used: 15.2
XXX percentage an existing variable is used: 84.8
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/

