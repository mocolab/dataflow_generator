/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      3755984650
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */


/* --- FORWARD DECLARATIONS --- */
static int32_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_1(void)
{ /* block id: 0 */
    int32_t l_2[7][5] = {{(-9L),0L,0L,(-9L),(-3L)},{(-9L),0x33EFC07EL,0x98A59C17L,0x98A59C17L,0x33EFC07EL},{(-3L),0L,0x98A59C17L,(-5L),(-5L)},{0L,(-3L),0L,0x98A59C17L,(-5L)},{0x33EFC07EL,(-9L),(-5L),(-9L),0x33EFC07EL},{0L,(-9L),(-3L),0x33EFC07EL,(-3L)},{(-3L),(-3L),(-5L),0x33EFC07EL,1L}};
    int i, j;
    return l_2[1][3];
}




/* ---------------------------------------- */
//testcase_id 1484153287
int case1484153287(void)
{
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 1
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 1
breakdown:
   depth: 1, occurrence: 1

XXX total number of pointers: 0

XXX times a non-volatile is read: 1
XXX times a non-volatile is write: 0
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 1
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 1

XXX percentage a fresh-made variable is used: 100
XXX percentage an existing variable is used: 0
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/

