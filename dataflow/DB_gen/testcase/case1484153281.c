/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      2208782740
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_3[4] = {9L,9L,9L,9L};
static int16_t g_28 = 0x1CB4L;
static int32_t g_31[2][6] = {{0xFA5B821BL,0xFA5B821BL,0xFA5B821BL,0xFA5B821BL,0xFA5B821BL,0xFA5B821BL},{0xFA5B821BL,0xFA5B821BL,0xFA5B821BL,0xFA5B821BL,0xFA5B821BL,0xFA5B821BL}};
static int32_t g_36 = (-10L);
static uint64_t g_45 = 0x86553ECF3843C9D4LL;
static int32_t g_53 = 0L;
static uint8_t g_76 = 1UL;
static int16_t g_90 = 0xC49EL;
static uint64_t g_97 = 0UL;
static int32_t g_104[6] = {0x39D48ECEL,0x39D48ECEL,0x39D48ECEL,0x39D48ECEL,0x39D48ECEL,0x39D48ECEL};
static uint32_t g_126 = 0xD7CE4956L;
static uint32_t *g_125 = &g_126;
static int32_t **g_132 = (void*)0;
static int32_t g_145 = 0x98D5FDB6L;
static int16_t g_184[6] = {(-6L),(-6L),0x9303L,(-6L),(-6L),0x9303L};
static int32_t **g_186 = (void*)0;
static uint32_t g_216[7] = {0xCB06A753L,0xCB06A753L,1UL,0xCB06A753L,0xCB06A753L,1UL,0xCB06A753L};
static int64_t g_219[8] = {(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L),(-7L)};
static uint64_t g_244 = 0xA8F979C1DC01494ELL;
static int64_t g_246[8] = {0x66237C08B2C5ECF4LL,0x66237C08B2C5ECF4LL,0L,4L,4L,0x66237C08B2C5ECF4LL,4L,4L};
static int32_t ***g_259 = (void*)0;
static uint8_t g_268 = 255UL;
static int32_t g_318[10] = {9L,9L,9L,9L,9L,9L,9L,9L,9L,9L};
static uint32_t g_319[7] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL};
static int32_t g_320 = 0L;
static const uint32_t g_365 = 0x220A4AAFL;
static int64_t g_393 = 0L;
static int32_t g_394 = (-1L);
static int32_t *g_413 = (void*)0;
static uint8_t *g_424 = &g_76;
static uint8_t **g_423[9] = {&g_424,&g_424,&g_424,&g_424,&g_424,&g_424,&g_424,&g_424,&g_424};
static uint32_t g_456[6] = {18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL,18446744073709551613UL};
static uint32_t g_490 = 1UL;
static uint16_t g_495 = 0x6300L;
static int32_t **g_555 = &g_413;
static int8_t g_578 = 4L;
static int32_t * const g_594 = &g_31[1][0];
static int32_t * const *g_593 = &g_594;
static int32_t g_702 = 2L;
static int16_t *g_769[5][10][2] = {{{(void*)0,&g_184[3]},{&g_184[4],&g_90},{&g_28,(void*)0},{(void*)0,&g_90},{&g_90,&g_184[3]},{&g_28,&g_184[2]},{&g_28,&g_184[2]},{&g_28,&g_184[3]},{&g_90,&g_90},{(void*)0,(void*)0}},{{&g_28,&g_90},{&g_184[4],&g_184[3]},{(void*)0,&g_90},{&g_90,&g_184[5]},{&g_184[1],&g_184[5]},{&g_90,&g_184[4]},{&g_184[3],&g_184[4]},{&g_28,&g_28},{&g_184[4],(void*)0},{&g_184[1],&g_184[1]}},{{(void*)0,&g_90},{&g_184[4],&g_184[5]},{(void*)0,&g_28},{&g_90,(void*)0},{(void*)0,&g_184[4]},{(void*)0,(void*)0},{&g_90,&g_28},{(void*)0,&g_184[5]},{&g_184[4],&g_90},{(void*)0,&g_184[1]}},{{&g_184[1],(void*)0},{&g_184[4],&g_28},{&g_28,&g_184[4]},{&g_184[3],&g_184[4]},{&g_90,&g_184[5]},{&g_184[1],&g_184[5]},{&g_90,&g_90},{(void*)0,&g_184[3]},{&g_184[4],&g_90},{&g_28,(void*)0}},{{(void*)0,&g_90},{&g_90,&g_184[3]},{&g_28,&g_184[2]},{&g_28,&g_184[2]},{&g_28,&g_184[3]},{&g_90,&g_90},{(void*)0,(void*)0},{&g_28,&g_90},{&g_184[4],&g_184[3]},{(void*)0,&g_90}}};
static int16_t **g_768 = &g_769[3][7][0];
static uint32_t g_846 = 0xB3047919L;
static int32_t ** const *g_934 = (void*)0;
static int32_t ** const **g_933 = &g_934;
static const uint8_t *g_1087 = &g_76;
static const uint8_t **g_1086 = &g_1087;
static int64_t g_1200 = 0xF3D86118C8522D14LL;
static uint16_t *g_1209 = &g_495;
static uint16_t **g_1208 = &g_1209;
static const int16_t g_1247 = 0L;
static const int8_t g_1265 = 0xACL;
static int8_t *g_1292 = (void*)0;
static int8_t **g_1291[9][7][4] = {{{&g_1292,&g_1292,&g_1292,&g_1292},{(void*)0,(void*)0,&g_1292,&g_1292},{(void*)0,&g_1292,(void*)0,&g_1292},{(void*)0,&g_1292,&g_1292,&g_1292},{(void*)0,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292}},{{&g_1292,&g_1292,(void*)0,(void*)0},{&g_1292,&g_1292,(void*)0,(void*)0},{&g_1292,&g_1292,&g_1292,(void*)0},{&g_1292,&g_1292,(void*)0,&g_1292},{(void*)0,&g_1292,&g_1292,&g_1292},{(void*)0,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,(void*)0,(void*)0}},{{&g_1292,&g_1292,(void*)0,&g_1292},{&g_1292,(void*)0,&g_1292,(void*)0},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,(void*)0,&g_1292},{(void*)0,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292}},{{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,(void*)0,&g_1292,(void*)0},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292}},{{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,(void*)0},{&g_1292,(void*)0,&g_1292,&g_1292},{&g_1292,(void*)0,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,(void*)0,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292}},{{&g_1292,&g_1292,&g_1292,&g_1292},{(void*)0,&g_1292,(void*)0,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,(void*)0,(void*)0},{&g_1292,&g_1292,(void*)0,(void*)0},{&g_1292,(void*)0,(void*)0,&g_1292}},{{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,(void*)0,&g_1292},{(void*)0,&g_1292,(void*)0,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292}},{{&g_1292,(void*)0,&g_1292,(void*)0},{(void*)0,&g_1292,(void*)0,(void*)0},{(void*)0,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292}},{{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,(void*)0,&g_1292,&g_1292},{&g_1292,&g_1292,&g_1292,&g_1292},{&g_1292,(void*)0,&g_1292,&g_1292},{(void*)0,(void*)0,(void*)0,(void*)0},{&g_1292,&g_1292,&g_1292,&g_1292},{(void*)0,&g_1292,(void*)0,&g_1292}}};
static uint8_t *g_1312 = &g_268;
static uint8_t ** const g_1311 = &g_1312;
static uint8_t ** const *g_1310[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static uint8_t g_1325 = 0UL;
static int64_t g_1327 = 0x2B949DE278B997C4LL;
static uint8_t g_1368[1] = {0x0FL};
static uint64_t *g_1394[7][3][10] = {{{&g_244,(void*)0,(void*)0,&g_97,&g_97,&g_97,&g_45,&g_97,&g_97,(void*)0},{&g_45,(void*)0,&g_97,(void*)0,&g_244,&g_244,&g_244,&g_45,&g_244,(void*)0},{&g_97,(void*)0,&g_45,&g_244,&g_244,&g_244,&g_244,(void*)0,(void*)0,&g_244}},{{&g_97,(void*)0,&g_45,(void*)0,(void*)0,&g_244,&g_45,&g_45,(void*)0,&g_45},{&g_45,(void*)0,&g_244,(void*)0,&g_244,&g_244,(void*)0,&g_244,(void*)0,&g_45},{&g_97,&g_45,(void*)0,&g_244,&g_45,&g_244,&g_97,&g_244,&g_244,&g_45}},{{&g_97,&g_97,&g_45,&g_45,&g_45,&g_244,&g_45,(void*)0,&g_45,&g_45},{&g_45,&g_97,(void*)0,(void*)0,&g_244,&g_97,&g_45,(void*)0,&g_45,&g_45},{&g_244,&g_97,&g_244,(void*)0,(void*)0,&g_244,&g_97,(void*)0,&g_45,&g_244}},{{&g_244,(void*)0,&g_45,&g_45,&g_244,&g_45,&g_244,&g_97,&g_244,(void*)0},{&g_244,(void*)0,&g_45,&g_244,&g_244,(void*)0,&g_45,(void*)0,(void*)0,(void*)0},{&g_244,&g_45,&g_244,(void*)0,&g_97,&g_244,(void*)0,(void*)0,(void*)0,&g_244}},{{&g_244,&g_45,(void*)0,(void*)0,&g_244,&g_244,&g_45,(void*)0,(void*)0,&g_244},{(void*)0,(void*)0,&g_45,&g_244,&g_244,&g_244,&g_244,&g_244,&g_244,&g_244},{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,&g_244,&g_97,&g_244,&g_97,&g_97}},{{&g_244,&g_97,&g_244,&g_97,&g_97,&g_244,&g_45,&g_45,&g_97,&g_97},{&g_244,&g_97,&g_45,&g_97,(void*)0,(void*)0,&g_45,(void*)0,(void*)0,&g_244},{&g_244,&g_97,&g_45,&g_97,&g_244,&g_45,&g_97,&g_244,&g_45,&g_97}},{{&g_244,&g_244,&g_45,&g_97,(void*)0,&g_244,&g_45,&g_244,&g_244,&g_97},{&g_97,&g_45,&g_97,&g_45,(void*)0,&g_45,&g_244,&g_244,&g_45,(void*)0},{&g_45,&g_97,&g_45,&g_244,&g_97,(void*)0,(void*)0,&g_244,&g_97,&g_97}}};
static uint64_t **g_1393[7] = {&g_1394[6][1][9],&g_1394[5][1][5],&g_1394[6][1][9],&g_1394[6][1][9],&g_1394[5][1][5],&g_1394[6][1][9],&g_1394[6][1][9]};
static uint8_t g_1435 = 255UL;
static int32_t g_1470 = (-2L);
static int64_t g_1476 = 0x536D6EAA7696E421LL;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static uint64_t  func_13(int32_t  p_14, int16_t  p_15, const int32_t  p_16);
static int32_t  func_17(int16_t  p_18, uint32_t  p_19, const int32_t * const  p_20);
static int64_t  func_24(int8_t  p_25, int8_t  p_26, int32_t * p_27);
static int32_t * const  func_46(int16_t  p_47, int32_t * p_48, uint16_t  p_49, int8_t  p_50, int8_t  p_51);
static int32_t  func_57(uint64_t * p_58);
static int32_t * func_61(uint8_t  p_62);
static int32_t * func_63(uint32_t  p_64, int64_t  p_65, uint32_t  p_66);
static const int16_t  func_71(uint16_t  p_72, int32_t * p_73);
static int32_t  func_79(const uint64_t  p_80, uint8_t  p_81);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_28 g_31 g_36 g_53 g_184 g_413 g_555 g_125 g_126 g_216 g_424 g_76 g_104 g_702 g_319 g_846 g_933 g_495 g_45 g_318 g_393 g_145 g_1087 g_244 g_219 g_97 g_456 g_365 g_1086 g_320 g_1208 g_1209 g_769 g_1265 g_246 g_1291 g_90 g_132 g_186 g_423 g_268 g_768 g_1312 g_1325 g_593 g_594 g_1368 g_1311 g_1393 g_1435
 * writes: g_3 g_36 g_45 g_413 g_393 g_320 g_90 g_53 g_184 g_104 g_76 g_219 g_495 g_394 g_933 g_145 g_126 g_244 g_1200 g_768 g_578 g_28 g_97 g_132 g_216 g_268 g_246 g_456 g_490 g_846 g_318 g_1310 g_1312 g_1393 g_1325 g_319 g_259 g_593
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int32_t *l_2 = &g_3[3];
    int32_t *l_4 = &g_3[3];
    int32_t *l_5 = &g_3[1];
    int32_t *l_6[9] = {&g_3[3],&g_3[3],&g_3[3],&g_3[3],&g_3[3],&g_3[3],&g_3[3],&g_3[3],&g_3[3]};
    uint16_t l_7 = 0x7B84L;
    uint32_t l_10[2][5] = {{0x1C5A3D7FL,4294967288UL,0x1C5A3D7FL,4294967288UL,0x1C5A3D7FL},{4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL}};
    const uint32_t l_23[4][6][9] = {{{0x5F075F97L,4294967295UL,1UL,4294967288UL,4UL,8UL,0xF0790389L,0x46FAC5ADL,4294967288UL},{4UL,0x8EB0BADCL,0x1F32AFCBL,0xF412F3A6L,0x5DD253D9L,4294967294UL,4294967295UL,0x19B5F814L,1UL},{0x9BD12F8BL,4294967295UL,0xC84132D3L,3UL,6UL,0xDFD99643L,0x9EDB9CC5L,0x64B91BF5L,8UL},{4294967289UL,0x061FA46BL,4294967295UL,0x1F32AFCBL,0x46153B72L,0xD93BA6AEL,0x98BE49C9L,0xFEED1B4BL,4UL},{4294967295UL,0UL,4294967295UL,4294967291UL,0x0F620A34L,4UL,0x43DA7D8EL,6UL,1UL},{0x051DD2E9L,0x1F32AFCBL,4294967294UL,4294967295UL,0x061FA46BL,4294967295UL,4294967294UL,0x1F32AFCBL,0x051DD2E9L}},{{6UL,0x8F0FC5A5L,4294967295UL,0x25D8284AL,0x597EEDF6L,4294967292UL,6UL,0x9EDB9CC5L,0xF0790389L},{4294967295UL,4294967295UL,0xF3B0639EL,5UL,0x051DD2E9L,0x2E963E9BL,0xA6CBABA2L,0xD93BA6AEL,0xFEED1B4BL},{6UL,0x597EEDF6L,6UL,1UL,0xB3915004L,8UL,0xED9E8BD6L,4294967288UL,0xCCC853D3L},{0x051DD2E9L,0xF3E63D4DL,0xF412F3A6L,0xA6CBABA2L,0xA8FAD749L,0x6F6D7797L,0UL,0xA3A6D7D0L,0x5DD253D9L},{4294967295UL,0xED9E8BD6L,0x5DC8FF05L,0x597EEDF6L,5UL,0xCCC853D3L,4UL,4294967295UL,0x4FA72D50L},{4294967289UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967295UL,4294967289UL,0x8266C095L}},{{0x9BD12F8BL,4UL,0xA4137D6CL,2UL,6UL,6UL,1UL,3UL,4294967288UL},{4UL,5UL,4294967295UL,4294967293UL,1UL,9UL,0xDC6606A8L,0xE352601CL,0x8266C095L},{0x5F075F97L,0xCCC853D3L,0xDFD99643L,4UL,0xA4137D6CL,0x8F0FC5A5L,0x25D8284AL,4294967288UL,0xDFD99643L},{4UL,1UL,0xA3A6D7D0L,4294967295UL,4294967295UL,0xFEED1B4BL,0xA8FAD749L,4UL,4294967290UL},{0xB3915004L,0xE60E4510L,1UL,0x9EDB9CC5L,0x4FA72D50L,6UL,0x549C4B05L,0x5F075F97L,0x5F075F97L},{9UL,0x1F32AFCBL,4UL,0x66193FE6L,4UL,0x1F32AFCBL,9UL,0x8A2DB637L,4294967295UL}},{{0x1E0F758FL,0x597EEDF6L,0xA4137D6CL,0x64B91BF5L,0xED9E8BD6L,0x9EDB9CC5L,4294967291UL,2UL,4294967292UL},{1UL,0xE352601CL,0x6F6D7797L,0x5DD253D9L,0x66193FE6L,0xD93BA6AEL,0xA3A6D7D0L,0x8A2DB637L,4UL},{4UL,4UL,4294967288UL,0x9BD12F8BL,0x43DA7D8EL,4294967288UL,0x8F0FC5A5L,0x5F075F97L,0xF0790389L},{0x8266C095L,0x98BE49C9L,0xFEF6F1ACL,0xD93BA6AEL,4294967295UL,0xA3A6D7D0L,0xF3B0639EL,4UL,4294967295UL},{0xD497C242L,3UL,4294967291UL,8UL,0xAFFF74B7L,4294967295UL,0x43DA7D8EL,4294967288UL,0x43DA7D8EL},{4294967289UL,0x8A2DB637L,4294967295UL,4294967295UL,0x8A2DB637L,4294967289UL,0xA6AE2024L,0x5DD253D9L,0xA3A6D7D0L}}};
    int16_t *l_35[8] = {&g_28,(void*)0,(void*)0,&g_28,(void*)0,(void*)0,&g_28,(void*)0};
    uint32_t l_43 = 2UL;
    uint64_t *l_44[4][2][7] = {{{&g_45,&g_45,&g_45,&g_45,&g_45,&g_45,&g_45},{(void*)0,&g_45,(void*)0,&g_45,&g_45,(void*)0,&g_45}},{{&g_45,&g_45,&g_45,&g_45,&g_45,&g_45,&g_45},{(void*)0,&g_45,&g_45,(void*)0,&g_45,(void*)0,&g_45}},{{&g_45,&g_45,&g_45,&g_45,&g_45,&g_45,&g_45},{&g_45,&g_45,&g_45,&g_45,&g_45,&g_45,&g_45}},{{(void*)0,&g_45,(void*)0,&g_45,&g_45,(void*)0,&g_45},{&g_45,&g_45,&g_45,&g_45,&g_45,&g_45,&g_45}}};
    int64_t *l_658 = &g_393;
    uint64_t l_659 = 0x7801C5B4AA8D8F12LL;
    const uint8_t l_877 = 0x1EL;
    uint64_t *l_1172 = &g_244;
    uint32_t l_1180 = 0xB5E632B7L;
    uint16_t l_1199 = 65534UL;
    uint16_t **l_1207 = (void*)0;
    int16_t **l_1215 = &l_35[4];
    int8_t *l_1266[1];
    uint8_t l_1269 = 0xB7L;
    uint32_t l_1302[6][8][3] = {{{1UL,0xFBB0EFEFL,0x62B6D49FL},{0xAD3AF460L,0x32BC2857L,0x13216C93L},{1UL,18446744073709551615UL,1UL},{0UL,0xAD3AF460L,0x13216C93L},{0x68658FA3L,0xCAA381C8L,0x62B6D49FL},{3UL,0xAD3AF460L,0xAD3AF460L},{0x62B6D49FL,18446744073709551615UL,18446744073709551608UL},{3UL,0x32BC2857L,3UL}},{{0x68658FA3L,0xFBB0EFEFL,18446744073709551608UL},{0UL,0UL,0xAD3AF460L},{1UL,0xFBB0EFEFL,0x62B6D49FL},{0xAD3AF460L,0x32BC2857L,0x13216C93L},{1UL,18446744073709551615UL,1UL},{0UL,0xAD3AF460L,0x13216C93L},{0x68658FA3L,0xFBB0EFEFL,1UL},{0x13216C93L,3UL,3UL}},{{1UL,0xCAA381C8L,18446744073709551606UL},{0x13216C93L,0UL,0x13216C93L},{0x62B6D49FL,18446744073709551606UL,18446744073709551606UL},{0xAD3AF460L,0xAD3AF460L,3UL},{18446744073709551608UL,18446744073709551606UL,1UL},{3UL,0UL,0x32BC2857L},{18446744073709551608UL,0xCAA381C8L,18446744073709551608UL},{0xAD3AF460L,3UL,0x32BC2857L}},{{0x62B6D49FL,0xFBB0EFEFL,1UL},{0x13216C93L,3UL,3UL},{1UL,0xCAA381C8L,18446744073709551606UL},{0x13216C93L,0UL,0x13216C93L},{0x62B6D49FL,18446744073709551606UL,18446744073709551606UL},{0xAD3AF460L,0xAD3AF460L,3UL},{18446744073709551608UL,18446744073709551606UL,1UL},{3UL,0UL,0x32BC2857L}},{{18446744073709551608UL,0xCAA381C8L,18446744073709551608UL},{0xAD3AF460L,3UL,0x32BC2857L},{0x62B6D49FL,0xFBB0EFEFL,1UL},{0x13216C93L,3UL,3UL},{1UL,0xCAA381C8L,18446744073709551606UL},{0x13216C93L,0UL,0x13216C93L},{0x62B6D49FL,18446744073709551606UL,18446744073709551606UL},{0xAD3AF460L,0xAD3AF460L,3UL}},{{18446744073709551608UL,18446744073709551606UL,1UL},{3UL,0UL,0x32BC2857L},{18446744073709551608UL,0xCAA381C8L,18446744073709551608UL},{0xAD3AF460L,3UL,0x32BC2857L},{0x62B6D49FL,0xFBB0EFEFL,1UL},{0x13216C93L,3UL,3UL},{1UL,0xCAA381C8L,18446744073709551606UL},{0x13216C93L,0UL,0x13216C93L}}};
    int32_t ***l_1409[6][1];
    uint16_t l_1460 = 0x84D5L;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1266[i] = (void*)0;
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 1; j++)
            l_1409[i][j] = &g_132;
    }
    l_7++;
lbl_1167:
    --l_10[1][1];
    if ((func_13(((*l_4) = (*l_4)), ((func_17((safe_div_func_uint8_t_u_u((l_23[2][2][8] , (((*l_658) = func_24(((g_28 >= (g_45 = (safe_sub_func_int16_t_s_s((g_36 ^= (g_31[0][4] | (safe_rshift_func_uint16_t_u_s(g_31[1][4], (!(0x2BEB855BL != (g_28 & g_28))))))), (safe_mod_func_uint16_t_u_u((safe_mod_func_uint8_t_u_u(((safe_mod_func_int8_t_s_s(g_31[1][2], g_28)) <= g_28), l_43)), 65530UL)))))) > g_31[0][3]), g_28, l_4)) , (*g_424))), l_659)), (*g_125), l_4) , (*g_413)) , (-3L)), l_877) && g_393))
    { /* block id: 541 */
        uint8_t ***l_1117 = &g_423[1];
        int32_t l_1122 = (-9L);
        int32_t l_1123 = 1L;
        uint32_t l_1134 = 0x047DF436L;
        int32_t l_1160 = 9L;
        if ((l_1134 = (l_1117 == (((safe_add_func_int64_t_s_s((((*g_125)--) | l_1122), (l_1123 &= (g_145 & ((void*)0 != &g_424))))) , ((safe_mod_func_int16_t_s_s(((safe_sub_func_int32_t_s_s((l_1123 , ((safe_mul_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u((l_1123 != (18446744073709551613UL | (((+(+(*l_5))) || 0x0753D77DFA6D467FLL) != l_1122))), (*g_1087))) , l_1123), g_702)) | l_1123)), 0L)) , 0L), l_1123)) && 18446744073709551606UL)) , (void*)0))))
        { /* block id: 545 */
            return (*g_125);
        }
        else
        { /* block id: 547 */
            const int8_t l_1152 = (-6L);
            for (l_1134 = (-26); (l_1134 > 37); l_1134++)
            { /* block id: 550 */
                int32_t l_1149 = 0x791CAAFCL;
                uint64_t *l_1153[1];
                uint32_t l_1158 = 2UL;
                int64_t **l_1163 = (void*)0;
                int i;
                for (i = 0; i < 1; i++)
                    l_1153[i] = &g_244;
                for (g_244 = 0; (g_244 <= 7); g_244 += 1)
                { /* block id: 553 */
                    int32_t l_1137 = 0xE5D3C878L;
                    int8_t *l_1161 = &g_578;
                    int32_t l_1162[6];
                    int i;
                    for (i = 0; i < 6; i++)
                        l_1162[i] = 0x73EE09A9L;
                    l_6[1] = (void*)0;
                    if ((l_1137 = (0x5D35L | g_219[g_244])))
                    { /* block id: 556 */
                        return (*g_125);
                    }
                    else
                    { /* block id: 558 */
                        uint16_t *l_1159 = &g_495;
                        int64_t ***l_1164 = &l_1163;
                        l_1162[0] ^= ((safe_sub_func_int16_t_s_s(0xDDC0L, (((l_1160 |= ((0x98E4L == (safe_add_func_int32_t_s_s(((*l_5) = ((g_31[0][4] && (((!((((safe_mul_func_uint16_t_u_u(0xD845L, (safe_rshift_func_uint16_t_u_u((((--(*g_125)) == l_1152) <= l_1137), 8)))) == (&g_45 == l_1153[0])) > ((((*l_1159) |= (safe_rshift_func_int8_t_s_u((safe_mod_func_int32_t_s_s(l_1158, (-9L))), l_1122))) > l_1123) <= g_3[0])) != g_97)) > l_1152) & 0xCBL)) > l_1137)), l_1122))) & g_456[0])) , l_1161) == &g_578))) ^ 0L);
                        if (l_1162[1])
                            continue;
                        (*l_1164) = l_1163;
                    }
                    if (l_1149)
                        break;
                }
            }
            return (*g_125);
        }
    }
    else
    { /* block id: 572 */
        int32_t l_1170 = 0x7D4FD5E2L;
        uint64_t *l_1171 = (void*)0;
        int32_t l_1185 = 0x0B93E452L;
        uint16_t *l_1188 = &g_495;
        int64_t *l_1264 = &g_219[0];
        uint8_t *l_1276[3];
        uint8_t ** const *l_1309 = &g_423[1];
        uint64_t l_1322 = 0x8A4DD9D6E4C6B78ELL;
        int32_t l_1329 = (-1L);
        int32_t l_1331 = 0L;
        int32_t l_1332 = (-8L);
        int32_t l_1334 = 0x68A09BA0L;
        int32_t l_1337 = 0x49E8BC88L;
        int32_t l_1338 = 8L;
        int32_t l_1342 = (-1L);
        int32_t l_1343 = 0xFC2D91FDL;
        uint8_t l_1379 = 251UL;
        uint64_t **l_1397 = &g_1394[2][2][1];
        int32_t *** const l_1410 = &g_186;
        int64_t **l_1412 = (void*)0;
        int64_t ***l_1411 = &l_1412;
        uint64_t l_1436 = 0x54727C0E8ABA7BA2LL;
        int32_t l_1437 = 0x81596779L;
        uint16_t l_1451 = 65529UL;
        int32_t l_1452 = 0L;
        int16_t l_1458 = 0x1739L;
        int32_t l_1459 = 0x7677AB83L;
        int32_t l_1474 = 3L;
        int16_t l_1480 = 3L;
        int i;
        for (i = 0; i < 3; i++)
            l_1276[i] = &g_268;
        for (g_45 = (-22); (g_45 > 51); g_45++)
        { /* block id: 575 */
            uint64_t **l_1173 = &l_1172;
            if (g_76)
                goto lbl_1167;
            l_1180 |= (safe_mod_func_uint8_t_u_u((((l_1170 & (l_1171 != ((*l_1173) = l_1172))) ^ (safe_div_func_uint32_t_u_u(((((safe_div_func_int8_t_s_s(l_1170, (safe_add_func_int32_t_s_s((254UL & ((*g_424) &= l_1170)), l_1170)))) , (*g_125)) || (l_1170 == g_219[0])) <= (*l_2)), (*l_5)))) < 4294967288UL), g_319[6]));
        }
        if (((safe_rshift_func_uint8_t_u_u((safe_mul_func_int8_t_s_s((g_104[0] < (l_1170 > (l_1185 = (((*l_5) <= l_1185) != (safe_lshift_func_uint16_t_u_s((++(*l_1188)), 4)))))), ((1UL && ((g_1200 = ((((l_1170 >= ((((((safe_add_func_uint16_t_u_u(((((safe_rshift_func_int8_t_s_u(((safe_rshift_func_int8_t_s_s(l_1170, 0)) , ((safe_rshift_func_uint8_t_u_s(((*l_4) >= 1UL), g_219[0])) && g_365)), (*g_424))) > 18446744073709551610UL) || 0xC9L) == l_1199), (*l_4))) == 0x52L) , l_1170) ^ l_1170) > 0xBD89L) < g_31[0][4])) , g_36) , (void*)0) != &g_934)) <= l_1170)) <= (*l_5)))), (**g_1086))) <= g_320))
        { /* block id: 584 */
            int16_t ***l_1212 = &g_768;
            int16_t **l_1214 = &l_35[0];
            int16_t ***l_1213[4][7][4] = {{{&l_1214,(void*)0,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,(void*)0},{&l_1214,&l_1214,&l_1214,(void*)0},{&l_1214,&l_1214,(void*)0,&l_1214}},{{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,(void*)0,&l_1214,(void*)0},{&l_1214,&l_1214,(void*)0,&l_1214},{&l_1214,&l_1214,(void*)0,&l_1214},{&l_1214,&l_1214,(void*)0,&l_1214}},{{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,(void*)0,&l_1214},{&l_1214,&l_1214,&l_1214,(void*)0},{&l_1214,&l_1214,&l_1214,&l_1214}},{{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214},{&l_1214,&l_1214,&l_1214,&l_1214}}};
            int8_t *l_1216 = (void*)0;
            int8_t *l_1217 = &g_578;
            int32_t l_1219 = 1L;
            int32_t *l_1273 = &g_53;
            uint8_t l_1295 = 0xA5L;
            int32_t ****l_1304 = &g_259;
            int16_t *l_1321 = (void*)0;
            int8_t l_1326[9][1];
            int32_t l_1333 = 0xF8B05B75L;
            int32_t l_1335 = 0xBA8E5677L;
            int32_t l_1336 = 1L;
            int32_t l_1339 = 0x1B0FDD01L;
            int32_t l_1340[7] = {(-2L),(-2L),(-2L),(-2L),(-2L),(-2L),(-2L)};
            uint64_t l_1344 = 0UL;
            int8_t l_1378 = 2L;
            uint32_t *l_1398 = &g_216[5];
            int32_t *l_1405 = (void*)0;
            uint16_t l_1413 = 0xAAA3L;
            uint8_t l_1442 = 251UL;
            int i, j, k;
            for (i = 0; i < 9; i++)
            {
                for (j = 0; j < 1; j++)
                    l_1326[i][j] = 0x16L;
            }
            if ((safe_div_func_int8_t_s_s(0xFFL, ((*l_1217) = (safe_lshift_func_int16_t_s_u((safe_mod_func_int8_t_s_s(((l_1207 != g_1208) , (safe_rshift_func_uint16_t_u_u((((*l_1212) = &g_769[3][7][0]) != (l_1215 = &g_769[4][7][0])), 7))), (*l_5))), 5))))))
            { /* block id: 588 */
                uint8_t ***l_1225 = &g_423[6];
                int32_t l_1242 = 0xCB1365F9L;
                int64_t *l_1262 = &g_393;
                int32_t l_1303 = 0xE1AFF4C4L;
                const uint32_t *l_1324 = &l_23[1][5][8];
                const uint32_t **l_1323 = &l_1324;
                if ((~8L))
                { /* block id: 589 */
                    uint8_t l_1220 = 0UL;
                    int32_t l_1226 = (-1L);
                    uint32_t l_1239 = 18446744073709551615UL;
                    --l_1220;
                    if ((((safe_lshift_func_int16_t_s_s((l_1226 = (((**g_1208) , ((void*)0 != l_1225)) ^ g_28)), 5)) <= (*g_1209)) || ((safe_lshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s((safe_add_func_int64_t_s_s(l_1170, ((*l_658) &= (safe_mod_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_s(248UL, ((safe_rshift_func_uint8_t_u_s(((*g_424) &= 0x71L), 7)) >= ((0x06B4971FL && (*g_125)) , l_1219)))), (*g_1209)))))), l_1226)), 1)) && l_1239)))
                    { /* block id: 594 */
                        (*g_555) = (*g_555);
                    }
                    else
                    { /* block id: 596 */
                        const int16_t *l_1246[10] = {&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247,&g_1247};
                        const int16_t **l_1245 = &l_1246[0];
                        int64_t **l_1263[6] = {&l_1262,&l_1262,&l_1262,&l_1262,&l_1262,&l_1262};
                        int i;
                        (*g_555) = ((((safe_mul_func_uint8_t_u_u(l_1242, ((safe_add_func_int8_t_s_s((((*g_1209) &= (l_1220 ^ (((g_216[5] & 0xBBB2EE41E5247BE2LL) && (((&g_578 == (((*g_424) = ((((*l_1215) == ((*l_1245) = l_1188)) == (safe_mul_func_uint8_t_u_u(((safe_add_func_int16_t_s_s(l_1219, ((safe_sub_func_uint64_t_u_u((((safe_sub_func_int64_t_s_s((safe_mul_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(((l_1264 = l_1262) != (void*)0), 0x9F9B18253B4C9545LL)), l_1219)), 0x8BACL)), 18446744073709551609UL)) < 0xD7A4B30E8BD4C89FLL) == 0x392018745145F2B7LL), 1L)) , 65535UL))) , (*g_424)), g_97))) > g_1265)) , l_1266[0])) || l_1185) | g_246[7])) > 0x8AL))) ^ l_1219), 5UL)) >= 1L))) > g_184[0]) || 0x23L) , (void*)0);
                        (*g_555) = l_2;
                    }
                    for (g_244 = 0; (g_244 < 3); g_244++)
                    { /* block id: 606 */
                        int32_t *l_1272[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_1272[i] = &l_1242;
                        ++l_1269;
                        l_1273 = l_1272[2];
                    }
                }
                else
                { /* block id: 610 */
                    uint8_t l_1279 = 255UL;
                    uint16_t l_1282 = 0xD7FAL;
                    if (((6UL | (safe_add_func_int64_t_s_s(((*l_1264) = (((g_184[1] | ((l_1276[1] == (*g_1086)) ^ (safe_sub_func_uint32_t_u_u((((l_1279 <= (safe_mul_func_uint16_t_u_u(l_1282, (safe_lshift_func_int16_t_s_u(0x74C5L, (l_1242 , ((7L == (-1L)) & (*l_5)))))))) == (*l_1273)) && 65535UL), (*g_125))))) & l_1185) || l_1279)), g_318[6]))) , l_1242))
                    { /* block id: 612 */
                        (*l_5) = (safe_div_func_int64_t_s_s(((l_1242 != (safe_sub_func_uint16_t_u_u(((safe_sub_func_int32_t_s_s((*l_4), ((**g_1086) == l_1279))) | 253UL), (((*g_424) & ((void*)0 == g_1291[6][0][3])) > (*l_1273))))) | (*l_1273)), 1UL));
                        return (*g_125);
                    }
                    else
                    { /* block id: 615 */
                        (*l_4) |= 0L;
                        (*g_555) = func_61((*l_1273));
                    }
                    for (g_846 = 0; g_846 < 10; g_846 += 1)
                    {
                        g_318[g_846] = 2L;
                    }
                }
                l_1219 |= (((safe_rshift_func_uint8_t_u_u(l_1295, (safe_mul_func_uint16_t_u_u(0xBD3CL, (*l_1273))))) , &g_259) == ((safe_mul_func_uint8_t_u_u((l_1303 = ((*g_424) = (((((*g_1209) || (*g_1209)) , (((*l_5) = (((((*l_1273) , 1UL) | (((safe_sub_func_int16_t_s_s(((void*)0 != &l_658), l_1185)) , l_1302[2][7][2]) <= (*l_1273))) | (*l_1273)) != (*g_424))) & (*g_125))) , l_1242) <= 1UL))), 0x8CL)) , l_1304));
                l_1219 ^= (((l_1170 < ((safe_mod_func_uint8_t_u_u(0UL, (safe_mul_func_uint16_t_u_u(((g_1310[0] = l_1309) != (void*)0), (*l_1273))))) , 0x7F5E143185A4F122LL)) < (safe_rshift_func_uint16_t_u_s(((safe_mul_func_uint16_t_u_u((((*l_1323) = func_63((*l_5), (l_1303 |= ((*l_1264) = (safe_add_func_uint8_t_u_u(((*g_1312) = ((safe_mul_func_int16_t_s_s(((((*g_768) == l_1321) ^ (-1L)) & 1L), (**g_1208))) , 0x98L)), l_1242)))), l_1322)) != (void*)0), 0x4DA0L)) != 0x47EAL), g_1325))) == 0x74940C374949832DLL);
            }
            else
            { /* block id: 631 */
                int64_t l_1328 = 1L;
                int32_t l_1330 = 0x86364DA6L;
                int32_t l_1341 = (-4L);
                int32_t *l_1354[6] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                uint8_t l_1360[5][9] = {{255UL,0xE9L,0xF0L,255UL,255UL,0xF0L,248UL,254UL,248UL},{0x60L,0x81L,0xF0L,0xF0L,0x81L,0x60L,255UL,255UL,0xCBL},{0x60L,0xE9L,248UL,0x60L,254UL,0xCBL,255UL,255UL,248UL},{255UL,254UL,255UL,0xCBL,2UL,0xCBL,255UL,254UL,255UL},{248UL,255UL,255UL,0xCBL,254UL,0x60L,248UL,0xE9L,0x60L}};
                int8_t **l_1373 = (void*)0;
                int16_t l_1377[5] = {0x7D3EL,0x7D3EL,0x7D3EL,0x7D3EL,0x7D3EL};
                int32_t *l_1392 = &g_394;
                uint64_t ***l_1395 = &g_1393[2];
                uint64_t ***l_1396 = (void*)0;
                int16_t l_1434 = 0x529CL;
                uint32_t l_1445 = 1UL;
                uint32_t **l_1457[5][10][2] = {{{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398}},{{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398}},{{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398}},{{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398}},{{&l_1398,&l_1398},{(void*)0,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,(void*)0},{(void*)0,&l_1398},{&l_1398,&l_1398},{&l_1398,&l_1398},{(void*)0,(void*)0},{&l_1398,(void*)0}}};
                int i, j, k;
                l_1344--;
                for (g_244 = 0; (g_244 < 22); g_244++)
                { /* block id: 635 */
                    int32_t l_1353 = 1L;
                    int16_t l_1355 = 0xE09FL;
                    int32_t l_1359 = 4L;
                    (*l_1273) ^= ((((**g_1208) = 0UL) & (safe_div_func_uint32_t_u_u((*l_5), (safe_add_func_int16_t_s_s(l_1353, ((*g_593) == l_1273)))))) && l_1353);
                    if ((*l_5))
                        continue;
                    if (l_1330)
                        continue;
                    if (((*l_4) ^= (&l_1353 != ((*g_555) = l_1354[5]))))
                    { /* block id: 642 */
                        uint64_t l_1356 = 0xB1D4C609336F7016LL;
                        --l_1356;
                    }
                    else
                    { /* block id: 644 */
                        int32_t **l_1371[5] = {&l_1354[2],&l_1354[2],&l_1354[2],&l_1354[2],&l_1354[2]};
                        int32_t * const *l_1372[5][1];
                        int i, j;
                        for (i = 0; i < 5; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_1372[i][j] = &l_5;
                        }
                        ++l_1360[0][3];
                        (*l_1273) = (safe_add_func_uint32_t_u_u((+((((safe_lshift_func_int8_t_s_s((((*l_658) = (g_1368[0] , (((safe_sub_func_int64_t_s_s(l_1331, ((l_1371[4] = (void*)0) == l_1372[4][0]))) || g_184[4]) ^ (l_1373 == (void*)0)))) , (safe_mul_func_int8_t_s_s(((*l_1217) = ((!((((((*g_125) &= ((void*)0 != &g_934)) , 0xA2651D19L) | l_1359) && (*l_4)) ^ 0xD8L)) , 0xB4L)), l_1355))), l_1334)) & (*g_1209)) & 0xEF3FCF38L) == l_1185)), l_1377[4]));
                        if (l_1170)
                            break;
                        (*l_5) = 0xE1911CC1L;
                    }
                }
                l_1379--;
                if (((((((*g_1311) = &l_1379) != (*g_1086)) < (1L < g_246[7])) >= (safe_add_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_mod_func_int64_t_s_s((((safe_rshift_func_int8_t_s_s((((l_1398 = ((safe_rshift_func_uint8_t_u_u(((*l_1273) > ((void*)0 == l_1392)), 3)) , func_63((((*l_1395) = g_1393[2]) == (l_1397 = &g_1394[4][0][0])), g_97, (*g_125)))) != (void*)0) || (*l_1273)), 0)) || 1L) > 0xA90AL), l_1379)), l_1334)), l_1379))) , 0L))
                { /* block id: 660 */
                    uint32_t l_1399 = 0xD4D6349AL;
                    uint16_t l_1406 = 0UL;
                    int32_t l_1415 = 0x79B36E83L;
                    int32_t l_1417 = 0x0A664A08L;
                    l_1333 ^= ((((void*)0 == (*g_768)) || (((**g_1311) = l_1399) | (*l_1273))) | (!(*l_4)));
                    for (g_1325 = 3; (g_1325 <= 9); g_1325 += 1)
                    { /* block id: 665 */
                        int32_t l_1414 = 0x70C606D0L;
                        int32_t l_1416 = (-5L);
                        int32_t l_1418 = 0x852675ABL;
                        uint16_t l_1419[8] = {3UL,3UL,3UL,3UL,3UL,3UL,3UL,3UL};
                        int i;
                        g_318[g_1325] = ((g_318[g_1325] >= (l_1342 < ((**g_1311) ^= (((((safe_mul_func_uint64_t_u_u(((++g_319[6]) , (l_1406 = ((g_318[g_1325] , l_1405) == (void*)0))), (((safe_sub_func_int64_t_s_s((((*l_1304) = (l_1170 , l_1409[5][0])) != l_1410), 0UL)) ^ (*l_1273)) > l_1185))) < 1L) , (void*)0) == l_1411) | 0x25L)))) > l_1413);
                        --l_1419[4];
                        return (*g_125);
                    }
                    (*l_2) = (l_1406 >= ((+((g_184[3] < (1UL || ((safe_div_func_int64_t_s_s(((((*g_1087) > (*l_1273)) , ((((*g_594) , (*l_1273)) , ((safe_add_func_int16_t_s_s((*l_4), ((**g_1208) |= ((safe_div_func_int8_t_s_s((safe_mod_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((~0x8F2EBC30L), l_1434)), l_1399)), 0xABL)) && g_1435)))) , 0x1E78F72CL)) | 0x5D008D90L)) & l_1436), g_36)) , (-3L)))) == l_1437)) , l_1379));
                }
                else
                { /* block id: 676 */
                    int16_t l_1450 = 0x6BE8L;
                    uint32_t l_1453[3];
                    int i;
                    for (i = 0; i < 3; i++)
                        l_1453[i] = 0x9FFBC51CL;
                    if (((*g_125) | (safe_sub_func_uint32_t_u_u((((safe_lshift_func_uint8_t_u_s(((l_1442 < g_104[0]) ^ 255UL), 0)) <= (safe_sub_func_uint64_t_u_u((((((*l_1172) ^= l_1445) , (g_593 = &g_594)) == &g_594) ^ (((*l_2) < ((*l_1217) = ((l_1451 ^= (safe_mul_func_uint16_t_u_u((*g_1209), l_1450))) != 18446744073709551606UL))) , l_1452)), 0xF3CAE54713064EA8LL))) && 0UL), 0xBBD9D076L))))
                    { /* block id: 681 */
                        return l_1453[2];
                    }
                    else
                    { /* block id: 683 */
                        uint32_t ***l_1454 = (void*)0;
                        uint32_t **l_1456 = &g_125;
                        uint32_t ***l_1455[7] = {&l_1456,&l_1456,&l_1456,&l_1456,&l_1456,&l_1456,&l_1456};
                        int i;
                        l_1457[2][3][1] = &g_125;
                    }
                }
            }
            --l_1460;
        }
        else
        { /* block id: 689 */
            uint32_t l_1463 = 0xB34FA270L;
            int32_t *l_1467 = &g_318[0];
            int32_t l_1468[10];
            int8_t l_1482 = 0x6BL;
            int i;
            for (i = 0; i < 10; i++)
                l_1468[i] = 0x2BE077FDL;
            for (g_145 = 7; (g_145 >= 0); g_145 -= 1)
            { /* block id: 692 */
                int64_t l_1466 = (-1L);
                int32_t l_1469[10] = {0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L,0x5AFE67A0L};
                uint64_t l_1485 = 1UL;
                uint16_t l_1488 = 0x1FD9L;
                int i;
                --l_1463;
                if (l_1463)
                { /* block id: 694 */
                    if (l_1466)
                        break;
                    (*g_555) = l_1467;
                    (*g_555) = (*g_555);
                }
                else
                { /* block id: 698 */
                    int16_t l_1471 = 0x6B56L;
                    int32_t l_1472 = 0x3559F82EL;
                    int32_t l_1473 = 3L;
                    int32_t l_1475 = 2L;
                    int32_t l_1477 = 0L;
                    int32_t l_1478 = (-1L);
                    int32_t l_1479 = 1L;
                    int32_t l_1481 = 0xC1736CAAL;
                    int32_t l_1483 = 0x5DAB67A5L;
                    int32_t l_1484 = 0x927DF006L;
                    l_1485++;
                }
                --l_1488;
            }
        }
        (*l_2) ^= l_1452;
    }
    return (*g_125);
}


/* ------------------------------------------ */
/* 
 * reads : g_76 g_702 g_424 g_36 g_126 g_394 g_319 g_413 g_53 g_555 g_45 g_393 g_846 g_933 g_495 g_318
 * writes: g_76 g_219 g_495 g_104 g_36 g_394 g_933 g_53 g_145 g_45 g_393 g_413
 */
static uint64_t  func_13(int32_t  p_14, int16_t  p_15, const int32_t  p_16)
{ /* block id: 415 */
    uint64_t *l_889 = &g_45;
    uint8_t **l_893 = &g_424;
    const int32_t l_910 = 0x10B3DED5L;
    uint16_t l_912 = 65529UL;
    int32_t l_916 = 0xB6276D17L;
    int32_t l_920 = 0x49D9D1D4L;
    int32_t l_922 = 0xB8116DC2L;
    uint16_t l_923[1];
    uint16_t l_928[4][5][4] = {{{0x32F1L,65535UL,0x53DEL,0xE504L},{0x20E3L,0UL,0x679EL,65535UL},{0UL,0x2DE1L,0UL,65535UL},{65535UL,0UL,1UL,0xE504L},{0xCEC8L,65535UL,0xCEC8L,0x53DEL}},{{0UL,6UL,0x7EFCL,0UL},{65535UL,0xCEC8L,0xE504L,6UL},{0xD91EL,0x32FCL,0xE504L,0x2DE1L},{65535UL,6UL,0x7EFCL,0x7EFCL},{0UL,0UL,0xCEC8L,0xD91EL}},{{0xCEC8L,0xD91EL,1UL,65535UL},{65535UL,0x20E3L,0UL,1UL},{0UL,0x20E3L,0x679EL,65535UL},{0x20E3L,0xD91EL,0x53DEL,0xD91EL},{0x32F1L,0UL,6UL,0x7EFCL}},{{0x53DEL,6UL,0x32FCL,0x2DE1L},{0x7EFCL,0x32FCL,0x20E3L,6UL},{0x7EFCL,0xCEC8L,0x32FCL,0UL},{0x53DEL,6UL,6UL,0x53DEL},{0x32F1L,65535UL,0x53DEL,0xE504L}}};
    uint16_t **l_929 = (void*)0;
    int32_t * const *l_937 = &g_413;
    int32_t * const **l_936 = &l_937;
    int32_t * const ***l_935 = &l_936;
    int32_t *l_968[10];
    int16_t **l_986 = &g_769[3][7][0];
    int32_t l_1041[6] = {0x590495F7L,0x590495F7L,0x590495F7L,0x590495F7L,0x590495F7L,0x590495F7L};
    uint32_t *l_1083 = &g_126;
    int32_t ***l_1112 = &g_555;
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_923[i] = 0x1EB2L;
    for (i = 0; i < 10; i++)
        l_968[i] = &g_53;
    for (g_76 = 1; (g_76 <= 5); g_76 += 1)
    { /* block id: 418 */
        int32_t l_885[9][2] = {{0x38C1FBA8L,(-4L)},{0x38C1FBA8L,0x38C1FBA8L},{0x38C1FBA8L,(-4L)},{0x38C1FBA8L,0x38C1FBA8L},{0x38C1FBA8L,(-4L)},{0x38C1FBA8L,0x38C1FBA8L},{0x38C1FBA8L,(-4L)},{0x38C1FBA8L,0x38C1FBA8L},{0x38C1FBA8L,(-4L)}};
        uint64_t *l_888[5][8][3] = {{{(void*)0,&g_97,&g_97},{&g_244,(void*)0,&g_45},{&g_244,&g_97,&g_244},{(void*)0,&g_97,&g_45},{&g_45,&g_45,&g_244},{(void*)0,&g_97,&g_97},{&g_97,&g_244,(void*)0},{&g_97,&g_244,&g_45}},{{(void*)0,(void*)0,&g_244},{&g_45,&g_244,&g_97},{(void*)0,&g_97,(void*)0},{&g_244,(void*)0,&g_244},{&g_244,(void*)0,(void*)0},{(void*)0,&g_97,&g_97},{&g_97,&g_45,&g_244},{&g_45,&g_45,&g_244}},{{&g_97,&g_97,&g_97},{&g_244,&g_97,&g_45},{(void*)0,&g_45,(void*)0},{&g_97,&g_97,&g_97},{&g_244,(void*)0,(void*)0},{(void*)0,(void*)0,&g_97},{&g_97,&g_97,&g_244},{(void*)0,&g_45,&g_97}},{{&g_244,&g_244,(void*)0},{&g_97,&g_97,&g_97},{(void*)0,&g_45,&g_97},{&g_244,(void*)0,&g_97},{&g_97,&g_97,&g_97},{&g_97,(void*)0,(void*)0},{&g_45,(void*)0,&g_97},{&g_244,&g_97,&g_244}},{{(void*)0,&g_97,&g_97},{&g_45,&g_97,(void*)0},{(void*)0,(void*)0,&g_97},{&g_244,(void*)0,(void*)0},{&g_97,&g_97,&g_45},{&g_97,(void*)0,&g_97},{&g_97,&g_45,&g_244},{&g_97,&g_97,(void*)0}}};
        uint8_t **l_894 = &g_424;
        int64_t *l_908 = &g_219[5];
        uint32_t l_909 = 4294967288UL;
        uint16_t *l_911 = &g_495;
        int16_t l_921 = 0xB3F3L;
        int i, j, k;
        g_104[g_76] = ((((safe_unary_minus_func_uint64_t_u((safe_mul_func_int8_t_s_s((safe_add_func_uint16_t_u_u((0x0C81L <= ((*l_911) = ((safe_mod_func_uint32_t_u_u((((l_885[7][1] > ((safe_div_func_uint64_t_u_u(((l_888[1][2][0] != l_889) != (~(safe_div_func_uint8_t_u_u((((l_885[7][1] , l_893) == l_894) != (safe_lshift_func_uint16_t_u_u((safe_rshift_func_uint8_t_u_s((((safe_sub_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_u((p_14 && (((*l_908) = (+((((+(!(safe_div_func_int16_t_s_s((g_702 != 0x77L), l_885[7][1])))) , 0xB4L) == p_14) , 4L))) != 1UL)), (*g_424))) ^ 1L), 0xA162L)) < l_885[8][0]) > 0L), 1)), l_909))), l_910)))), 0x9BB8477260CD92F1LL)) >= p_16)) != p_15) , 4294967295UL), p_16)) ^ l_910))), 0L)), p_16)))) <= l_912) , p_15) | l_912);
        for (g_36 = 1; (g_36 <= 5); g_36 += 1)
        { /* block id: 424 */
            int32_t *l_913 = &g_104[2];
            int32_t *l_914 = &l_885[7][1];
            int32_t *l_915 = (void*)0;
            int32_t *l_917 = &g_104[g_76];
            int32_t *l_918 = &g_104[g_76];
            int32_t *l_919[6][2];
            int i, j;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 2; j++)
                    l_919[i][j] = &g_104[4];
            }
            l_923[0]--;
            return g_126;
        }
        for (g_394 = (-17); (g_394 <= 9); ++g_394)
        { /* block id: 430 */
            return l_928[0][3][2];
        }
    }
    for (l_916 = 1; (l_916 <= 6); l_916 += 1)
    { /* block id: 436 */
        uint16_t ***l_930 = &l_929;
        int i;
        (*l_930) = l_929;
        if (g_319[l_916])
            break;
    }
    if ((l_922 &= 0xD8FDB42CL))
    { /* block id: 441 */
        int32_t ** const *l_932 = &g_186;
        int32_t ** const **l_931 = &l_932;
        const int8_t l_960 = 3L;
        uint64_t l_967 = 18446744073709551615UL;
        int32_t ***l_1100 = &g_132;
        int32_t l_1103 = (-4L);
        int32_t *l_1111 = (void*)0;
        int32_t **l_1110 = &l_1111;
        int32_t ****l_1113 = (void*)0;
        int32_t ****l_1114 = (void*)0;
        int32_t ****l_1115[10][3] = {{&l_1100,&g_259,&l_1112},{&g_259,&l_1100,&l_1112},{&l_1100,&g_259,&l_1112},{&g_259,&g_259,&l_1100},{&l_1100,&l_1100,&g_259},{&g_259,&l_1100,&l_1100},{&l_1100,&g_259,&g_259},{&l_1112,&g_259,&l_1100},{&l_1112,&l_1100,&g_259},{&l_1112,&g_259,&l_1100}};
        int i, j;
lbl_1107:
        if (((g_933 = l_931) == l_935))
        { /* block id: 443 */
            int16_t **l_940 = &g_769[3][7][0];
            int32_t l_944 = 0x77A57FD8L;
            uint8_t *l_1007 = &g_76;
            int32_t l_1012 = 1L;
            const uint8_t l_1040 = 1UL;
            uint32_t *l_1080 = &g_126;
            int32_t *l_1084 = &g_53;
            int32_t *l_1098 = (void*)0;
            int32_t ** const l_1097 = &l_1098;
            int32_t ** const *l_1096[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int i;
            for (l_920 = 0; (l_920 <= 5); l_920 += 1)
            { /* block id: 446 */
                const int8_t l_959 = 0xBFL;
                uint16_t l_961[5] = {1UL,1UL,1UL,1UL,1UL};
                int32_t *l_965 = (void*)0;
                int i;
                for (l_912 = 0; (l_912 <= 5); l_912 += 1)
                { /* block id: 449 */
                    int32_t *l_964 = &g_104[l_920];
                    int i;
                }
                (***l_936) &= p_15;
                p_14 = (-4L);
                l_967 ^= ((**l_937) , (**g_555));
                for (g_145 = 5; (g_145 >= 1); g_145 -= 1)
                { /* block id: 478 */
                    for (g_76 = 0; (g_76 <= 1); g_76 += 1)
                    { /* block id: 481 */
                        l_968[7] = &p_14;
                    }
                }
            }
            for (g_45 = 26; (g_45 < 8); g_45 = safe_sub_func_uint16_t_u_u(g_45, 1))
            { /* block id: 488 */
                int16_t **l_987 = &g_769[3][7][0];
                int32_t l_1058[6][8][5] = {{{1L,1L,0x79633168L,(-7L),(-9L)},{0xF362DFB8L,(-5L),5L,0x02A0ABFCL,0xCC2F2594L},{(-7L),0xAD2C7E5EL,(-7L),0x6A7AD290L,(-6L)},{5L,(-5L),0xF362DFB8L,(-7L),(-2L)},{0x79633168L,1L,1L,0x79633168L,(-7L)},{0L,0x003DB0C5L,0xF362DFB8L,(-5L),0xBA7D0106L},{0x95B8E07BL,0L,(-7L),0L,0x95B8E07BL},{0x5D627510L,(-7L),5L,(-5L),0x6030E60FL}},{{4L,1L,0x79633168L,0x79633168L,1L},{0xCC2F2594L,3L,0L,(-7L),0x6030E60FL},{0L,0x79633168L,0x95B8E07BL,0x6A7AD290L,0x95B8E07BL},{0x6030E60FL,0x4A51141AL,0x5D627510L,0x02A0ABFCL,0xBA7D0106L},{0L,(-9L),4L,(-7L),(-7L)},{0xCC2F2594L,0x5663690FL,0xCC2F2594L,4L,(-2L)},{4L,(-9L),0L,1L,(-9L)},{0x6030E60FL,4L,(-7L),0x4A51141AL,0L}},{{4L,1L,0xAD2C7E5EL,(-9L),0x95B8E07BL},{5L,0x5663690FL,0L,0x5663690FL,5L},{1L,0x79633168L,(-7L),(-9L),0x6A7AD290L},{0xCC2F2594L,0x003DB0C5L,0x6030E60FL,0x4A51141AL,0x5D627510L},{1L,0xAD2C7E5EL,4L,0x79633168L,0x6A7AD290L},{0x36500D0EL,0x4A51141AL,5L,3L,5L},{0x6A7AD290L,0x6A7AD290L,1L,1L,0x95B8E07BL},{0x36500D0EL,(-1L),0xCC2F2594L,(-7L),0L}},{{1L,(-6L),1L,0L,(-9L)},{0xCC2F2594L,(-1L),0x36500D0EL,0x003DB0C5L,0xBA7D0106L},{1L,0x6A7AD290L,0x6A7AD290L,1L,1L},{5L,0x4A51141AL,0x36500D0EL,(-5L),0L},{4L,0xAD2C7E5EL,1L,0xAD2C7E5EL,4L},{0x6030E60FL,0x003DB0C5L,0xCC2F2594L,(-5L),(-7L)},{(-7L),0x79633168L,1L,1L,0x79633168L},{0L,0x5663690FL,5L,0x003DB0C5L,(-7L)}},{{0xAD2C7E5EL,1L,4L,0L,4L},{(-7L),4L,0x6030E60FL,(-7L),0L},{0xAD2C7E5EL,0x95B8E07BL,(-7L),1L,1L},{0L,(-5L),0L,3L,0xBA7D0106L},{(-7L),0x95B8E07BL,0xAD2C7E5EL,0x79633168L,(-9L)},{0x6030E60FL,4L,(-7L),0x4A51141AL,0L},{4L,1L,0xAD2C7E5EL,(-9L),0x95B8E07BL},{5L,0x5663690FL,0L,0x5663690FL,5L}},{{1L,0x79633168L,(-7L),(-9L),0x6A7AD290L},{0xCC2F2594L,0x003DB0C5L,0x6030E60FL,0x4A51141AL,0x5D627510L},{1L,0xAD2C7E5EL,4L,0x79633168L,0x6A7AD290L},{0x36500D0EL,0x4A51141AL,5L,3L,5L},{0x6A7AD290L,0x6A7AD290L,1L,1L,0x95B8E07BL},{0x36500D0EL,(-1L),0xCC2F2594L,(-7L),0L},{1L,(-6L),1L,0L,(-9L)},{0xCC2F2594L,(-1L),0x36500D0EL,0x003DB0C5L,0xBA7D0106L}}};
                uint32_t *l_1081 = &g_126;
                const uint8_t **l_1092[3][4][5] = {{{&g_1087,&g_1087,&g_1087,&g_1087,(void*)0},{&g_1087,&g_1087,(void*)0,&g_1087,&g_1087},{(void*)0,&g_1087,&g_1087,&g_1087,&g_1087},{&g_1087,&g_1087,(void*)0,&g_1087,(void*)0}},{{&g_1087,&g_1087,&g_1087,(void*)0,&g_1087},{(void*)0,(void*)0,(void*)0,(void*)0,&g_1087},{(void*)0,&g_1087,&g_1087,&g_1087,&g_1087},{(void*)0,&g_1087,&g_1087,&g_1087,&g_1087}},{{(void*)0,(void*)0,&g_1087,&g_1087,&g_1087},{&g_1087,&g_1087,&g_1087,(void*)0,(void*)0},{&g_1087,&g_1087,&g_1087,&g_1087,&g_1087},{&g_1087,&g_1087,&g_1087,&g_1087,&g_1087}}};
                int i, j, k;
                for (g_393 = 0; (g_393 > 26); g_393++)
                { /* block id: 491 */
                    int32_t l_1008 = 0x59F6641AL;
                    int32_t l_1009 = 0x02A258D5L;
                    const int32_t l_1039 = 0xABF22724L;
                    int32_t ** const *l_1095[6] = {&g_132,&g_186,&g_186,&g_132,&g_186,&g_186};
                    int32_t ***l_1099[8][7];
                    int i, j;
                    for (i = 0; i < 8; i++)
                    {
                        for (j = 0; j < 7; j++)
                            l_1099[i][j] = (void*)0;
                    }
                }
                return g_846;
            }
            if (g_126)
                goto lbl_1107;
        }
        else
        { /* block id: 529 */
            uint16_t l_1104 = 0x14F8L;
            --l_1104;
        }
        (*g_555) = ((((safe_mul_func_uint16_t_u_u(0x7600L, g_319[0])) == ((*g_933) != (*l_931))) , (((((*l_889) |= (((((((void*)0 == l_1110) , (*l_931)) == (l_1112 = l_1112)) ^ (!((((****l_935) = 0xA283L) , g_495) | 1L))) || p_14) <= p_14)) == p_14) < (-1L)) , (-1L))) , (void*)0);
    }
    else
    { /* block id: 537 */
        return g_318[0];
    }
    return p_14;
}


/* ------------------------------------------ */
/* 
 * reads : g_320 g_53 g_104
 * writes: g_320 g_90 g_53 g_184 g_104
 */
static int32_t  func_17(int16_t  p_18, uint32_t  p_19, const int32_t * const  p_20)
{ /* block id: 283 */
    int32_t l_660 = 0xCBB69E8AL;
    int32_t *l_661 = &g_104[2];
    int32_t l_662 = 0x884B1D52L;
    int32_t *l_663 = &g_318[7];
    int32_t *l_664 = &g_53;
    int32_t l_665 = (-1L);
    int32_t *l_666[10][7] = {{(void*)0,&g_104[0],&g_53,&l_665,&l_662,&g_318[8],&g_104[0]},{&g_104[0],&g_318[6],&l_662,&g_318[1],&l_662,&g_318[6],&g_104[0]},{(void*)0,&l_665,&g_104[0],&l_662,&g_104[0],&g_53,&g_53},{(void*)0,&g_318[6],&g_53,&g_53,(void*)0,&g_53,&g_53},{&g_104[0],&g_104[0],&g_104[0],&g_53,(void*)0,&g_318[8],&l_665},{&l_662,&l_662,&l_662,&g_53,&g_104[0],&l_662,&g_104[0]},{&l_662,&g_53,&g_53,&l_662,(void*)0,&g_104[0],&g_53},{(void*)0,&g_318[1],(void*)0,&g_318[1],(void*)0,&l_662,(void*)0},{&g_318[8],&g_104[0],&l_665,&l_665,&g_104[0],&g_318[8],&g_53},{&g_104[0],&g_53,&l_662,&l_662,&l_662,&g_53,&g_104[0]}};
    int32_t l_667 = 0x496D63EDL;
    int32_t l_668 = 0x3F43DD08L;
    uint16_t l_669[4] = {0xFA1CL,0xFA1CL,0xFA1CL,0xFA1CL};
    int8_t *l_687 = (void*)0;
    int16_t l_697 = 1L;
    int32_t l_839 = 0xDC273E44L;
    uint32_t l_861 = 0UL;
    int16_t l_876 = 6L;
    int i, j;
    l_669[1]++;
    for (g_320 = 1; (g_320 <= 5); g_320 += 1)
    { /* block id: 287 */
        const int32_t l_676 = 0x09396579L;
        int8_t *l_689 = &g_578;
        int8_t *l_690 = &g_578;
        int32_t *l_716 = &l_665;
        int16_t *l_735 = &g_90;
        int8_t l_738[5][5] = {{0x54L,0x86L,0x54L,(-5L),(-5L)},{9L,0xFCL,9L,(-1L),(-1L)},{0x54L,0x86L,0x54L,(-5L),(-5L)},{9L,0xFCL,9L,(-1L),(-1L)},{0x54L,0x86L,0x54L,(-5L),(-5L)}};
        int32_t l_746 = 1L;
        int32_t l_751 = 0x9C900D4EL;
        int8_t l_815 = 0x02L;
        int32_t l_827 = (-9L);
        int32_t l_828 = 1L;
        int32_t l_831 = 0x7BAC4747L;
        int32_t l_832 = (-1L);
        int32_t l_835[6][9] = {{0L,(-1L),0L,(-9L),(-9L),0L,(-1L),0L,(-9L)},{0x7249DF1FL,(-9L),(-9L),0x7249DF1FL,0x5133775FL,0x7249DF1FL,(-9L),(-9L),0x7249DF1FL},{(-2L),(-9L),1L,(-9L),(-2L),(-2L),(-9L),1L,(-9L)},{(-9L),0x5133775FL,0xECE4ADABL,0xECE4ADABL,0x5133775FL,(-9L),0x5133775FL,0xECE4ADABL,0xECE4ADABL},{(-2L),(-2L),(-9L),1L,(-9L),(-2L),(-2L),(-9L),1L},{0x7249DF1FL,0x5133775FL,0x7249DF1FL,(-9L),(-9L),0x7249DF1FL,(-7L),0xECE4ADABL,0x7249DF1FL}};
        int i, j;
        for (g_90 = 0; (g_90 <= 5); g_90 += 1)
        { /* block id: 290 */
            int32_t l_675 = 0x7A9D9239L;
            int16_t *l_677 = &g_184[4];
            int8_t l_684 = 0xC4L;
            int i;
            for (l_660 = 3; (l_660 >= 0); l_660 -= 1)
            { /* block id: 293 */
                int i;
                if (l_669[l_660])
                    break;
            }
            (*l_661) = (+(safe_mul_func_uint8_t_u_u(l_675, (((*l_664) |= 8L) || (l_676 < (((*l_677) = 0L) || ((4L && (*l_664)) | (((p_19 , (safe_sub_func_int64_t_s_s(((((-1L) < (safe_mod_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_u((*l_661), (*l_664))) != p_19), l_684))) >= p_19) != p_18), l_676))) , p_19) , l_676))))))));
        }
    }
    return l_876;
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_53 g_36 g_184 g_413 g_555 g_125 g_126 g_216 g_424 g_76
 * writes: g_36 g_413
 */
static int64_t  func_24(int8_t  p_25, int8_t  p_26, int32_t * p_27)
{ /* block id: 6 */
    int32_t *l_52 = &g_53;
    const int16_t *l_56 = (void*)0;
    int32_t **l_657 = &g_413;
    (*l_657) = func_46(p_25, l_52, ((g_31[0][4] < ((safe_mod_func_int8_t_s_s((g_53 > ((void*)0 == l_56)), (*l_52))) < func_57(&g_45))) , p_26), g_184[4], p_26);
    return (**l_657);
}


/* ------------------------------------------ */
/* 
 * reads : g_413 g_555 g_53 g_125 g_126 g_216 g_424 g_76
 * writes: g_413
 */
static int32_t * const  func_46(int16_t  p_47, int32_t * p_48, uint16_t  p_49, int8_t  p_50, int8_t  p_51)
{ /* block id: 271 */
    int32_t * const l_638 = &g_53;
    int32_t **l_639 = (void*)0;
    int32_t **l_640 = &g_413;
    (*l_640) = l_638;
    (*g_555) = (p_48 = (p_50 , (*l_640)));
    if (((safe_lshift_func_uint16_t_u_u(((((safe_rshift_func_int8_t_s_u(0xD1L, 1)) || ((safe_sub_func_int64_t_s_s(((safe_mod_func_int64_t_s_s(((safe_sub_func_uint32_t_u_u(((&p_48 == ((safe_lshift_func_int16_t_s_u((safe_div_func_int64_t_s_s(0xC336CF835F7E6944LL, (*l_638))), 15)) , &l_638)) , ((**l_640) <= (safe_sub_func_int64_t_s_s(((((7UL < (*g_125)) >= (*l_638)) , (-8L)) & 0xEC4981B7L), g_216[2])))), 0x2B31A727L)) < (*g_125)), (**l_640))) && 0x97E21C5DA7CA697ALL), 0x9D988731D3AE74D4LL)) && (**g_555))) & p_47) != (*g_424)), 13)) >= (**g_555)))
    { /* block id: 275 */
        return (*l_640);
    }
    else
    { /* block id: 277 */
        return p_48;
    }
}


/* ------------------------------------------ */
/* 
 * reads : g_36
 * writes: g_36
 */
static int32_t  func_57(uint64_t * p_58)
{ /* block id: 7 */
    uint8_t l_513 = 0x6DL;
    const int64_t *l_522 = &g_246[4];
    int32_t l_579 = 0x56AEA527L;
    int8_t *l_592 = (void*)0;
    uint16_t l_613 = 0UL;
    int64_t * const l_624 = (void*)0;
    for (g_36 = 0; (g_36 >= 8); g_36 = safe_add_func_int16_t_s_s(g_36, 3))
    { /* block id: 10 */
        int32_t **l_507 = &g_413;
        int32_t l_532 = 0xED6ECC84L;
        const uint8_t l_556 = 246UL;
        uint32_t *l_582[8];
        int i;
        for (i = 0; i < 8; i++)
            l_582[i] = &g_216[5];
    }
    l_579 = (l_579 != l_579);
    return l_613;
}


/* ------------------------------------------ */
/* 
 * reads : g_53 g_28 g_36 g_45 g_76 g_90 g_97 g_104 g_31 g_125 g_126 g_132 g_145 g_184 g_186 g_365 g_246 g_216 g_319 g_423 g_456 g_268 g_320 g_424
 * writes: g_28 g_97 g_104 g_126 g_90 g_132 g_45 g_145 g_184 g_216 g_219 g_320 g_76 g_268 g_246 g_244 g_393 g_394 g_413 g_456 g_490 g_495
 */
static int32_t * func_61(uint8_t  p_62)
{ /* block id: 11 */
    int8_t l_67 = 0x59L;
    int16_t *l_70 = &g_28;
    uint8_t *l_74 = (void*)0;
    uint8_t *l_75[10] = {(void*)0,&g_76,&g_76,(void*)0,&g_76,(void*)0,&g_76,&g_76,(void*)0,&g_76};
    int32_t l_77 = (-9L);
    int32_t l_78 = 0x13618026L;
    uint64_t *l_88 = &g_45;
    int16_t *l_89 = &g_90;
    int32_t *l_333 = &l_77;
    int32_t l_465 = (-1L);
    int32_t l_468 = 0x0C1648F8L;
    int32_t l_469[8][6] = {{0x0D96C5ABL,3L,0xAD00917CL,0x30047A4CL,0xAD00917CL,3L},{(-5L),0x30047A4CL,0xBFF320E4L,0L,0L,0L},{0L,0L,0L,0xAD00917CL,0xF4E6CEABL,0x2453B155L},{0x6A7FC95DL,0L,0x9C6C62DBL,0x9C6C62DBL,0L,0x6A7FC95DL},{0L,0x30047A4CL,3L,0xF762AB1EL,0xAD00917CL,1L},{0x9C6C62DBL,3L,0xF4E6CEABL,0x0D96C5ABL,(-1L),0x883309D8L},{0x9C6C62DBL,0xF762AB1EL,0x0D96C5ABL,0xF762AB1EL,0x9C6C62DBL,0xAD00917CL},{0L,0xAD00917CL,(-1L),0x9C6C62DBL,0x883309D8L,(-5L)}};
    uint32_t l_470[6][6][7] = {{{0xC6696383L,18446744073709551615UL,0xD176E13BL,0xC6696383L,18446744073709551615UL,0x62BF6830L,0xE171CDCEL},{0UL,18446744073709551615UL,0x8C62AFACL,0UL,18446744073709551615UL,0x0090F957L,18446744073709551615UL},{0UL,0xD176E13BL,0xD176E13BL,0UL,0xE171CDCEL,0x62BF6830L,18446744073709551615UL},{0xC6696383L,18446744073709551615UL,0xD176E13BL,0xC6696383L,18446744073709551615UL,0x62BF6830L,0xE171CDCEL},{0UL,18446744073709551615UL,0x8C62AFACL,0UL,18446744073709551615UL,0x0090F957L,18446744073709551615UL},{0UL,0xD176E13BL,0xD176E13BL,0UL,0xE171CDCEL,0x62BF6830L,18446744073709551615UL}},{{0xC6696383L,18446744073709551615UL,0xD176E13BL,0xC6696383L,18446744073709551615UL,0x62BF6830L,0xE171CDCEL},{0UL,18446744073709551615UL,0x8C62AFACL,0UL,18446744073709551615UL,0x0090F957L,18446744073709551615UL},{0UL,0xD176E13BL,0xD176E13BL,0UL,0xE171CDCEL,0x62BF6830L,18446744073709551615UL},{0xC6696383L,18446744073709551615UL,0xD176E13BL,0xC6696383L,18446744073709551615UL,0x62BF6830L,0xE171CDCEL},{0UL,18446744073709551615UL,0x8C62AFACL,0UL,18446744073709551615UL,0x0090F957L,18446744073709551615UL},{0UL,0xD176E13BL,0xD176E13BL,0UL,0xE171CDCEL,0x62BF6830L,18446744073709551615UL}},{{0xC6696383L,18446744073709551615UL,0xD176E13BL,0xC6696383L,18446744073709551615UL,0x62BF6830L,0xE171CDCEL},{0UL,18446744073709551615UL,0x8C62AFACL,0UL,18446744073709551615UL,0x0090F957L,18446744073709551615UL},{0UL,0xD176E13BL,0xD176E13BL,0UL,0xE171CDCEL,0x62BF6830L,18446744073709551615UL},{0xC6696383L,18446744073709551615UL,0xD176E13BL,0xC6696383L,18446744073709551615UL,0x62BF6830L,0xE171CDCEL},{0UL,18446744073709551615UL,0x8C62AFACL,0UL,18446744073709551615UL,0x0090F957L,18446744073709551615UL},{0UL,0xD176E13BL,0xD176E13BL,0UL,0xE171CDCEL,0x62BF6830L,18446744073709551615UL}},{{0xC6696383L,18446744073709551615UL,0xD176E13BL,0x8C62AFACL,0xBB41A095L,1UL,0UL},{0xD176E13BL,0x62BF6830L,0xBEDAE11AL,0xD176E13BL,0xBB41A095L,18446744073709551610UL,0xBB41A095L},{0xD176E13BL,0x0090F957L,0x0090F957L,0xD176E13BL,0UL,1UL,0xBB41A095L},{0x8C62AFACL,0x62BF6830L,0x0090F957L,0x8C62AFACL,0xBB41A095L,1UL,0UL},{0xD176E13BL,0x62BF6830L,0xBEDAE11AL,0xD176E13BL,0xBB41A095L,18446744073709551610UL,0xBB41A095L},{0xD176E13BL,0x0090F957L,0x0090F957L,0xD176E13BL,0UL,1UL,0xBB41A095L}},{{0x8C62AFACL,0x62BF6830L,0x0090F957L,0x8C62AFACL,0xBB41A095L,1UL,0UL},{0xD176E13BL,0x62BF6830L,0xBEDAE11AL,0xD176E13BL,0xBB41A095L,18446744073709551610UL,0xBB41A095L},{0xD176E13BL,0x0090F957L,0x0090F957L,0xD176E13BL,0UL,1UL,0xBB41A095L},{0x8C62AFACL,0x62BF6830L,0x0090F957L,0x8C62AFACL,0xBB41A095L,1UL,0UL},{0xD176E13BL,0x62BF6830L,0xBEDAE11AL,0xD176E13BL,0xBB41A095L,18446744073709551610UL,0xBB41A095L},{0xD176E13BL,0x0090F957L,0x0090F957L,0xD176E13BL,0UL,1UL,0xBB41A095L}},{{0x8C62AFACL,0x62BF6830L,0x0090F957L,0x8C62AFACL,0xBB41A095L,1UL,0UL},{0xD176E13BL,0x62BF6830L,0xBEDAE11AL,0xD176E13BL,0xBB41A095L,18446744073709551610UL,0xBB41A095L},{0xD176E13BL,0x0090F957L,0x0090F957L,0xD176E13BL,0UL,1UL,0xBB41A095L},{0x8C62AFACL,0x62BF6830L,0x0090F957L,0x8C62AFACL,0xBB41A095L,1UL,0UL},{0xD176E13BL,0x62BF6830L,0xBEDAE11AL,0xD176E13BL,0xBB41A095L,18446744073709551610UL,0xBB41A095L},{0xD176E13BL,0x0090F957L,0x0090F957L,0xD176E13BL,0UL,1UL,0xBB41A095L}}};
    int8_t *l_489[6][8] = {{&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0},{&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0},{&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0},{&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0},{&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0},{&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0,&l_67,(void*)0}};
    uint16_t *l_493 = (void*)0;
    uint16_t *l_494 = &g_495;
    int i, j, k;
    l_333 = func_63(l_67, g_53, ((*g_125) = (((*l_89) = ((safe_mul_func_int16_t_s_s((((*l_70) ^= l_67) > func_71(p_62, (((l_78 = (l_77 = (1L ^ g_36))) , func_79(((l_78 = (((l_70 != (((safe_sub_func_uint32_t_u_u(((l_77 |= ((((safe_rshift_func_int8_t_s_s((-1L), (((safe_mod_func_int64_t_s_s(p_62, 18446744073709551615UL)) , g_53) != g_45))) , l_88) == l_88) >= g_45)) > l_67), g_36)) , l_78) , l_89)) || g_53) != p_62)) ^ p_62), p_62)) , &g_53))), 5UL)) & g_53)) | 1L)));
    for (g_28 = 14; (g_28 < (-28)); g_28--)
    { /* block id: 139 */
        int32_t l_336 = (-9L);
        uint64_t l_376 = 0UL;
        int32_t l_429[7][8][4] = {{{0x97852A20L,1L,0x518E5E05L,1L},{3L,1L,9L,0x0CD02DF0L},{0x606E7B0CL,0x03A8FED7L,0x30B2C847L,(-5L)},{0x03A8FED7L,1L,0xF876F79DL,1L},{0x7F7B86A9L,0x0CD02DF0L,(-1L),1L},{0x518E5E05L,0x1ED58768L,0x1ED58768L,0x518E5E05L},{(-1L),5L,9L,0x73D422ECL},{1L,0xF876F79DL,1L,0x03A8FED7L}},{{0L,0x7AEEEAB0L,0xECE39943L,0x03A8FED7L},{0x1ED58768L,0xF876F79DL,1L,0x73D422ECL},{0x30B2C847L,5L,1L,0x518E5E05L},{0xF36B97A0L,0x1ED58768L,(-1L),0L},{1L,0xF36B97A0L,0x7AEEEAB0L,(-1L)},{0x30B2C847L,0L,0xAB1A9E05L,5L},{0xB25E1885L,(-10L),0xECE39943L,0xF36B97A0L},{0x73D422ECL,1L,0x73D422ECL,0xE0412640L}},{{1L,0L,(-3L),1L},{1L,1L,0x1ED58768L,0L},{0x1D43FE87L,0xB25E1885L,0x1ED58768L,0x1D43FE87L},{1L,5L,(-3L),0L},{1L,0L,0x73D422ECL,0x03A8FED7L},{0x73D422ECL,0x03A8FED7L,0xECE39943L,0x7AEEEAB0L},{0xB25E1885L,0xF876F79DL,0xAB1A9E05L,0L},{0x30B2C847L,0xE0412640L,0x7AEEEAB0L,0x518E5E05L}},{{1L,0xB25E1885L,(-1L),(-1L)},{0xF36B97A0L,0xF36B97A0L,1L,1L},{0x30B2C847L,(-1L),1L,5L},{0x1ED58768L,1L,0xECE39943L,1L},{0L,1L,1L,5L},{1L,(-1L),9L,1L},{(-1L),0xF36B97A0L,0x1ED58768L,(-1L)},{0x518E5E05L,0xB25E1885L,5L,0x518E5E05L}},{{1L,0xE0412640L,9L,0L},{(-10L),0xF876F79DL,0x73D422ECL,0x7AEEEAB0L},{0L,0x03A8FED7L,0x15C84610L,0x03A8FED7L},{0xB25E1885L,0L,1L,0L},{0x0A2BBF38L,5L,0x7AEEEAB0L,0x1D43FE87L},{0xF36B97A0L,0xB25E1885L,3L,0L},{0xF36B97A0L,1L,0x7AEEEAB0L,1L},{0x0A2BBF38L,0L,1L,0xE0412640L}},{{0xB25E1885L,1L,0x15C84610L,0xF36B97A0L},{0L,(-10L),0x73D422ECL,5L},{(-10L),0L,9L,(-1L)},{1L,0xF36B97A0L,5L,0L},{0x518E5E05L,0x1ED58768L,0x1ED58768L,0x518E5E05L},{(-1L),5L,9L,0x73D422ECL},{1L,0xF876F79DL,1L,0x03A8FED7L},{0L,0x7AEEEAB0L,0xECE39943L,0x03A8FED7L}},{{0x1ED58768L,0xF876F79DL,1L,0x73D422ECL},{0x30B2C847L,5L,1L,0x518E5E05L},{0xF36B97A0L,0x1ED58768L,(-1L),0L},{1L,0xF36B97A0L,0x7AEEEAB0L,(-1L)},{0x30B2C847L,0L,0xAB1A9E05L,0L},{0x1D43FE87L,(-5L),0x606E7B0CL,0x7AEEEAB0L},{3L,(-1L),3L,0xA782E568L},{(-1L),0x1ED58768L,0x97852A20L,0xE0412640L}}};
        int32_t *l_440 = (void*)0;
        int i, j, k;
        if (l_336)
        { /* block id: 140 */
            uint8_t **l_337 = &l_75[5];
            (*l_333) = (((*l_337) = &p_62) == &p_62);
        }
        else
        { /* block id: 143 */
            uint32_t l_355 = 0UL;
            const uint64_t l_364 = 0x89AB5338570F91B8LL;
            const int32_t *l_395 = &g_145;
            int32_t l_398[2][1];
            int32_t ***l_428[1][6][2];
            int16_t l_473[3];
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                    l_398[i][j] = 0x3A1E3D3DL;
            }
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 6; j++)
                {
                    for (k = 0; k < 2; k++)
                        l_428[i][j][k] = &g_186;
                }
            }
            for (i = 0; i < 3; i++)
                l_473[i] = 0x9698L;
            for (g_320 = (-20); (g_320 > (-19)); g_320 = safe_add_func_uint64_t_u_u(g_320, 2))
            { /* block id: 146 */
                uint32_t l_348 = 4294967288UL;
                int8_t l_367[3][1];
                int32_t *l_396[9] = {&l_78,&l_78,&l_78,&l_78,&l_78,&l_78,&l_78,&l_78,&l_78};
                int32_t l_404 = 0L;
                int64_t *l_412 = &g_219[1];
                int i, j;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 1; j++)
                        l_367[i][j] = 0x88L;
                }
                (*l_333) |= l_336;
                for (g_76 = 0; (g_76 < 22); g_76 = safe_add_func_int32_t_s_s(g_76, 1))
                { /* block id: 150 */
                    uint8_t l_366 = 0UL;
                    int64_t *l_368 = &g_246[4];
                    (*l_333) = ((safe_lshift_func_uint8_t_u_s((((*l_368) = (safe_div_func_int32_t_s_s((safe_sub_func_int64_t_s_s((0x970F7062198D4FBALL | 0xA40E25A2CC3E8ECBLL), (((g_76 >= l_348) <= (!((~((safe_rshift_func_uint16_t_u_s(((safe_add_func_int16_t_s_s(l_355, (((+((((safe_add_func_uint8_t_u_u(((safe_lshift_func_int8_t_s_s((!l_355), (safe_mod_func_uint64_t_u_u(l_355, 0x81A27D616F2CD21ELL)))) <= (g_268 = g_184[4])), 0x23L)) == l_364) < g_365) , (*l_333))) <= l_348) & p_62))) != l_366), p_62)) <= 0x4996L)) >= p_62))) == 0UL))), l_367[1][0]))) == 0x747EB430A314201ELL), 4)) , l_336);
                    for (g_244 = 29; (g_244 == 21); g_244 = safe_sub_func_int16_t_s_s(g_244, 9))
                    { /* block id: 156 */
                        uint64_t l_381 = 0x7FAF82C61DAAAD41LL;
                        int32_t **l_397 = &l_396[0];
                        (*l_397) = ((safe_unary_minus_func_int32_t_s(((0x46L | (safe_mul_func_uint8_t_u_u(((((safe_mul_func_uint8_t_u_u(0x94L, l_376)) > (safe_div_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u(((((l_381 >= (--p_62)) == ((*l_333) , (safe_mod_func_int16_t_s_s((safe_sub_func_uint32_t_u_u(((((((safe_rshift_func_uint8_t_u_s((+((safe_mul_func_uint8_t_u_u((g_268 = ((((*l_368) ^= 0x345054F20D8B6239LL) >= ((l_366 != l_355) == ((g_394 = (g_393 = ((l_381 | l_366) < l_381))) >= l_381))) , l_367[1][0])), (*l_333))) != 0xE01D3740L)), l_348)) , &g_31[1][1]) == l_395) <= 0xF692AB73A84520F0LL) == l_336) >= (-1L)), (*g_125))), g_76)))) && 0x1EAB644128163C9ELL) && 0x92C838B9F0410DA0LL), l_348)), g_216[4]))) || g_145) != 0x4B35L), 0UL))) , 1L))) , l_396[0]);
                    }
                }
                l_398[0][0] = (g_216[6] , ((1UL || p_62) , p_62));
                g_413 = ((((--(*g_125)) , ((p_62 && (0xA91E2998L & (~(safe_mul_func_int8_t_s_s(((0x81F1L || l_404) == p_62), ((l_376 < 0xCFFBF41D30294901LL) <= (l_398[0][0] = 0UL))))))) == (safe_unary_minus_func_uint16_t_u(((((*l_412) = (safe_rshift_func_int16_t_s_s((safe_rshift_func_int16_t_s_s((safe_sub_func_uint64_t_u_u(18446744073709551611UL, 0x04C5769C4B64D91CLL)), g_319[1])), 8))) & p_62) <= p_62))))) , p_62) , &l_398[0][0]);
            }
            for (g_393 = 25; (g_393 != 9); g_393--)
            { /* block id: 173 */
                int64_t *l_425 = (void*)0;
                int64_t *l_426 = &g_246[7];
                int32_t ** const * const l_427 = &g_186;
                const int8_t l_431 = 7L;
                int16_t *l_433 = &g_184[2];
                int32_t l_459 = 0L;
                int32_t l_461 = (-8L);
                int32_t l_463 = 0x2A2C5227L;
                int32_t l_467[1][7][1] = {{{0x90572055L},{0xF2B3E526L},{0x90572055L},{0xF2B3E526L},{0x90572055L},{0xF2B3E526L},{0x90572055L}}};
                uint32_t l_477 = 5UL;
                uint8_t l_480[10][3] = {{5UL,0x87L,5UL},{5UL,255UL,0x99L},{5UL,0x95L,0x45L},{5UL,0x87L,5UL},{5UL,255UL,0x99L},{5UL,0x95L,0x45L},{5UL,0x87L,5UL},{5UL,255UL,0x99L},{5UL,0x95L,0x45L},{5UL,0x87L,5UL}};
                int i, j, k;
                if (((safe_mul_func_uint16_t_u_u(0x86F0L, (((*l_88) = g_36) , 0UL))) < ((safe_div_func_int8_t_s_s((l_429[2][4][0] = (safe_mod_func_int64_t_s_s(((*l_426) = (!((void*)0 == g_423[1]))), (((l_427 != l_428[0][1][1]) < p_62) , 1L)))), p_62)) <= g_31[0][4])))
                { /* block id: 177 */
                    int32_t *l_430 = (void*)0;
                    return l_430;
                }
                else
                { /* block id: 179 */
                    uint32_t l_455 = 0x8DB44618L;
                    int32_t l_458 = 0L;
                    int32_t l_462[3][1];
                    int i, j;
                    for (i = 0; i < 3; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_462[i][j] = 1L;
                    }
                    if (p_62)
                    { /* block id: 180 */
                        int16_t **l_432 = &l_70;
                        int8_t *l_454[3];
                        int32_t l_457 = (-6L);
                        int i;
                        for (i = 0; i < 3; i++)
                            l_454[i] = &l_67;
                        l_440 = (((g_31[0][4] < (*l_333)) | (l_431 <= ((((*l_432) = &g_184[4]) != l_433) , (safe_rshift_func_uint8_t_u_u(0x7EL, (p_62 == (safe_div_func_int32_t_s_s((safe_lshift_func_uint8_t_u_s(0xAEL, 7)), (*l_333))))))))) , &l_429[2][4][0]);
                        l_458 = ((safe_lshift_func_uint16_t_u_s((((((safe_rshift_func_int8_t_s_s(1L, (((((*l_432) = l_433) == (void*)0) , ((safe_div_func_uint16_t_u_u((safe_sub_func_int32_t_s_s(p_62, ((*g_125) = (~g_365)))), (safe_sub_func_int64_t_s_s((safe_add_func_int8_t_s_s((g_456[0] |= (l_455 = 4L)), (((0x5AB7L < (18446744073709551615UL || (p_62 , (*l_333)))) != 1L) , p_62))), 0x636A92AA2B3F103CLL)))) > (*l_333))) < l_457))) , p_62) , p_62) != l_458) , g_268), (*l_333))) | 4L);
                        l_458 ^= ((void*)0 == &p_62);
                    }
                    else
                    { /* block id: 189 */
                        int32_t l_460 = 0x1370444DL;
                        int32_t l_464 = 6L;
                        int32_t l_466 = 0x648F1DF5L;
                        int32_t l_474 = 1L;
                        int32_t l_475 = 0x2D91F824L;
                        int32_t l_476 = 0xF15A9F3EL;
                        l_470[1][1][0]--;
                        l_477++;
                    }
                    (*l_333) = g_320;
                }
                if (l_480[5][1])
                    break;
            }
            if (p_62)
                continue;
        }
        if ((*l_333))
            break;
        l_469[4][5] ^= (*l_333);
    }
    if ((safe_div_func_uint8_t_u_u((~(*l_333)), ((*g_424) = (~(safe_sub_func_uint16_t_u_u(((*l_494) = (safe_add_func_int8_t_s_s((g_490 = (g_53 ^ (-1L))), (safe_lshift_func_uint8_t_u_s(0xF0L, 5))))), (safe_mul_func_int8_t_s_s((*l_333), ((safe_add_func_uint64_t_u_u((!p_62), g_104[0])) > (((((safe_mul_func_uint16_t_u_u(((safe_add_func_int8_t_s_s(((p_62 , p_62) >= 0xADL), p_62)) == (*l_333)), 0x1B07L)) ^ (*l_333)) , 0x460EBFD3L) | p_62) >= 0x22DC5914L)))))))))))
    { /* block id: 205 */
        int32_t *l_505 = (void*)0;
        (*l_333) = (*l_333);
        (*l_333) |= (p_62 , p_62);
        return l_505;
    }
    else
    { /* block id: 209 */
        int32_t *l_506 = &l_78;
        return &g_318[6];
    }
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t * func_63(uint32_t  p_64, int64_t  p_65, uint32_t  p_66)
{ /* block id: 133 */
    int32_t *l_321 = (void*)0;
    int32_t *l_322 = &g_104[0];
    int32_t *l_323 = &g_104[2];
    int32_t *l_324 = &g_104[0];
    int32_t *l_325 = &g_318[6];
    int32_t *l_326 = &g_104[0];
    int32_t *l_327[2][10] = {{&g_318[0],&g_53,&g_318[0],&g_318[0],&g_53,&g_318[0],&g_318[0],&g_53,&g_318[0],&g_318[0]},{&g_53,&g_53,(void*)0,&g_53,&g_53,(void*)0,&g_53,&g_53,(void*)0,&g_53}};
    int16_t l_328 = 0x712AL;
    uint16_t l_329[7][7][1] = {{{5UL},{4UL},{5UL},{4UL},{5UL},{4UL},{5UL}},{{4UL},{5UL},{4UL},{5UL},{4UL},{5UL},{4UL}},{{5UL},{4UL},{5UL},{4UL},{5UL},{4UL},{5UL}},{{4UL},{5UL},{4UL},{5UL},{4UL},{5UL},{4UL}},{{5UL},{4UL},{5UL},{4UL},{5UL},{4UL},{5UL}},{{4UL},{5UL},{4UL},{5UL},{4UL},{5UL},{4UL}},{{5UL},{4UL},{5UL},{4UL},{5UL},{4UL},{5UL}}};
    int32_t *l_332 = &g_104[0];
    int i, j, k;
    l_329[4][6][0]--;
    return l_332;
}


/* ------------------------------------------ */
/* 
 * reads : g_31 g_125 g_90 g_104 g_53 g_126 g_132 g_97 g_36 g_45 g_76 g_145 g_184 g_186
 * writes: g_126 g_90 g_132 g_45 g_145 g_184 g_104 g_97 g_216 g_219
 */
static const int16_t  func_71(uint16_t  p_72, int32_t * p_73)
{ /* block id: 24 */
    uint16_t l_113[10][10][2];
    int32_t *l_152 = &g_104[3];
    int32_t **l_151 = &l_152;
    int32_t **l_164[8][1] = {{(void*)0},{&l_152},{(void*)0},{&l_152},{(void*)0},{&l_152},{(void*)0},{&l_152}};
    int32_t ***l_163 = &l_164[3][0];
    int32_t *l_188 = &g_53;
    uint8_t * const l_245 = &g_76;
    int64_t l_255 = 0x052E4B5579BDA634LL;
    uint32_t l_301 = 18446744073709551612UL;
    int8_t l_305 = 1L;
    int i, j, k;
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
                l_113[i][j][k] = 65535UL;
        }
    }
    if (((((safe_div_func_int8_t_s_s(((l_113[3][1][1] & (safe_rshift_func_uint16_t_u_s(0x57EAL, p_72))) , g_31[0][4]), (+l_113[3][1][1]))) ^ (l_113[4][7][0] > (((safe_lshift_func_int16_t_s_u(((safe_rshift_func_uint16_t_u_s((l_113[3][1][1] < (((safe_sub_func_int64_t_s_s(p_72, (safe_lshift_func_int16_t_s_s(((void*)0 == g_125), l_113[9][0][1])))) , p_72) | p_72)), 13)) , g_90), 1)) != 0L) , p_72))) & g_104[0]) , (*p_73)))
    { /* block id: 25 */
        int16_t l_143 = 0x62ECL;
lbl_187:
        for (p_72 = 22; (p_72 <= 36); p_72 = safe_add_func_int8_t_s_s(p_72, 3))
        { /* block id: 28 */
            uint16_t l_134 = 1UL;
            uint8_t l_142[2];
            int32_t l_144 = 0xF47D9D0DL;
            int i;
            for (i = 0; i < 2; i++)
                l_142[i] = 254UL;
            for (g_126 = 0; (g_126 >= 30); g_126 = safe_add_func_int32_t_s_s(g_126, 6))
            { /* block id: 31 */
                int32_t **l_131 = (void*)0;
                int32_t ***l_133[10][3] = {{&g_132,(void*)0,(void*)0},{&l_131,&g_132,&g_132},{&g_132,(void*)0,(void*)0},{&l_131,&g_132,&g_132},{&g_132,(void*)0,(void*)0},{&l_131,&g_132,&g_132},{&g_132,(void*)0,(void*)0},{&l_131,&g_132,&g_132},{&g_132,(void*)0,(void*)0},{&l_131,&g_132,&g_132}};
                uint64_t *l_141 = &g_45;
                int i, j;
                for (g_90 = 0; (g_90 <= 1); g_90 += 1)
                { /* block id: 34 */
                    int i, j, k;
                    return l_113[(g_90 + 8)][(g_90 + 3)][g_90];
                }
                g_145 |= (l_144 = ((p_72 & (((l_131 != (g_132 = g_132)) , (l_143 &= ((((l_134 || l_113[3][1][1]) < ((g_104[0] >= ((safe_div_func_uint64_t_u_u((l_113[1][1][0] | ((*l_141) &= ((((safe_mul_func_int8_t_s_s(0x24L, g_97)) | 0x4782D3B2L) <= g_104[5]) , g_36))), p_72)) != l_142[1])) , l_142[1])) != p_72) > p_72))) || g_45)) && g_76));
                return l_113[3][1][1];
            }
        }
        for (g_90 = 0; (g_90 <= 1); g_90 += 1)
        { /* block id: 47 */
            int32_t ***l_153 = &l_151;
            int32_t ***l_154 = &g_132;
            int16_t *l_183 = &g_184[4];
            int32_t *l_185 = &g_145;
            g_104[0] = ((((*l_185) = (p_72 , (safe_add_func_int8_t_s_s((((((safe_rshift_func_int16_t_s_s((safe_unary_minus_func_uint16_t_u(((l_143 & (((*l_153) = l_151) == ((*l_154) = (void*)0))) , p_72))), ((*l_183) = ((safe_mod_func_int64_t_s_s(((safe_mul_func_int8_t_s_s((safe_sub_func_int32_t_s_s((safe_rshift_func_int8_t_s_u((l_163 == (void*)0), (((safe_mul_func_int16_t_s_s(p_72, (safe_rshift_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(((safe_sub_func_uint16_t_u_u((safe_lshift_func_int16_t_s_s((safe_div_func_int64_t_s_s(((safe_lshift_func_uint8_t_u_u(((((safe_mul_func_int16_t_s_s((safe_mod_func_uint16_t_u_u(g_31[1][1], 0xCA1CL)), p_72)) & 0xD3L) && g_36) , g_76), 2)) , 0x7BAAA9AA2B5C891FLL), p_72)), g_45)), p_72)) > 6L), l_143)), 3)))) ^ p_72) || p_72))), (*g_125))), g_97)) > g_31[0][1]), 0x29F704EBC73C5DB1LL)) <= p_72)))) || 1UL) < g_97) , 0UL) , g_184[3]), p_72)))) , &p_73) == g_186);
            for (g_97 = 0; (g_97 <= 1); g_97 += 1)
            { /* block id: 55 */
                if (g_126)
                    goto lbl_187;
                return l_143;
            }
        }
    }
    else
    { /* block id: 60 */
        uint16_t *l_189 = (void*)0;
        uint16_t *l_190 = &l_113[3][1][1];
        int32_t l_191[10][10][2] = {{{0x58E0F9DAL,0x8204FB6BL},{0x58E0F9DAL,0x97BB59C8L},{0L,0x31F8A238L},{0x97BB59C8L,0L},{0x8DB84F2AL,0x58E0F9DAL},{0xC2623A6DL,0x31F8A238L},{0x31F8A238L,0xC2623A6DL},{0x58E0F9DAL,0x8DB84F2AL},{0L,0x97BB59C8L},{0x31F8A238L,0L}},{{0x97BB59C8L,0x58E0F9DAL},{0x8204FB6BL,0x58E0F9DAL},{0x97BB59C8L,7L},{(-1L),0x8204FB6BL},{0x59904FF0L,1L},{0xC2623A6DL,7L},{(-1L),(-1L)},{7L,0xC2623A6DL},{1L,0x59904FF0L},{0x8204FB6BL,(-1L)}},{{7L,0x8204FB6BL},{0xC2623A6DL,1L},{0xC2623A6DL,0x8204FB6BL},{7L,(-1L)},{0x8204FB6BL,0x59904FF0L},{1L,0xC2623A6DL},{7L,(-1L)},{(-1L),7L},{0xC2623A6DL,1L},{0x59904FF0L,0x8204FB6BL}},{{(-1L),7L},{0x8204FB6BL,0xC2623A6DL},{1L,0xC2623A6DL},{0x8204FB6BL,7L},{(-1L),0x8204FB6BL},{0x59904FF0L,1L},{0xC2623A6DL,7L},{(-1L),(-1L)},{7L,0xC2623A6DL},{1L,0x59904FF0L}},{{0x8204FB6BL,(-1L)},{7L,0x8204FB6BL},{0xC2623A6DL,1L},{0xC2623A6DL,0x8204FB6BL},{7L,(-1L)},{0x8204FB6BL,0x59904FF0L},{1L,0xC2623A6DL},{7L,(-1L)},{(-1L),7L},{0xC2623A6DL,1L}},{{0x59904FF0L,0x8204FB6BL},{(-1L),7L},{0x8204FB6BL,0xC2623A6DL},{1L,0xC2623A6DL},{0x8204FB6BL,7L},{(-1L),0x8204FB6BL},{0x59904FF0L,1L},{0xC2623A6DL,7L},{(-1L),(-1L)},{7L,0xC2623A6DL}},{{1L,0x59904FF0L},{0x8204FB6BL,(-1L)},{7L,0x8204FB6BL},{0xC2623A6DL,1L},{0xC2623A6DL,0x8204FB6BL},{7L,(-1L)},{0x8204FB6BL,0x59904FF0L},{1L,0xC2623A6DL},{7L,(-1L)},{(-1L),7L}},{{0xC2623A6DL,1L},{0x59904FF0L,0x8204FB6BL},{(-1L),7L},{0x8204FB6BL,0xC2623A6DL},{1L,0xC2623A6DL},{0x8204FB6BL,7L},{(-1L),0x8204FB6BL},{0x59904FF0L,1L},{0xC2623A6DL,7L},{(-1L),(-1L)}},{{7L,0xC2623A6DL},{1L,0x59904FF0L},{0x8204FB6BL,(-1L)},{7L,0x8204FB6BL},{0xC2623A6DL,1L},{0xC2623A6DL,0x8204FB6BL},{7L,(-1L)},{0x8204FB6BL,0x59904FF0L},{1L,0xC2623A6DL},{7L,(-1L)}},{{(-1L),7L},{0xC2623A6DL,1L},{0x59904FF0L,0x8204FB6BL},{(-1L),7L},{0x8204FB6BL,0xC2623A6DL},{1L,0xC2623A6DL},{0x8204FB6BL,7L},{(-1L),0x8204FB6BL},{0x59904FF0L,1L},{0xC2623A6DL,7L}}};
        int i, j, k;
lbl_222:
        p_73 = l_188;
        l_191[5][1][0] &= (g_53 && ((*l_190) = g_36));
        if ((*p_73))
        { /* block id: 64 */
            uint32_t *l_215 = &g_216[5];
            int32_t l_217[1];
            int64_t *l_218 = &g_219[0];
            int32_t *l_221[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
            int32_t **l_220 = &l_221[6];
            int i;
            for (i = 0; i < 1; i++)
                l_217[i] = 1L;
            l_217[0] = (safe_add_func_uint16_t_u_u((g_97 < p_72), (((*l_220) = (((g_36 || ((safe_sub_func_int64_t_s_s((safe_mul_func_int16_t_s_s((safe_div_func_int64_t_s_s(l_191[5][1][0], ((*l_218) = (safe_div_func_uint64_t_u_u((g_31[1][5] , (((0x50D9L <= ((safe_rshift_func_int8_t_s_u((safe_rshift_func_int16_t_s_u(0L, 11)), 0)) | (safe_sub_func_int16_t_s_s((!(safe_sub_func_int32_t_s_s(0L, (((safe_lshift_func_int8_t_s_u(((((*l_215) = ((*g_125) = ((((((safe_add_func_int32_t_s_s(0x3E6FF05AL, p_72)) & g_97) , g_97) != p_72) != g_53) == g_90))) == g_31[1][0]) , (**l_151)), p_72)) > 4L) | g_184[4])))), l_217[0])))) | p_72) <= g_97)), g_45))))), g_184[3])), p_72)) , p_72)) >= 0x471759D7677ECEE2LL) , &g_145)) == (void*)0)));
        }
        else
        { /* block id: 70 */
            if (p_72)
                goto lbl_222;
        }
    }
    for (g_97 = 0; (g_97 < 21); g_97 = safe_add_func_uint32_t_u_u(g_97, 1))
    { /* block id: 76 */
        const uint32_t l_242 = 0x0F86B609L;
        int32_t *l_270 = &g_104[2];
        int32_t l_278[6] = {0L,0L,0L,0L,0L,0L};
        uint8_t l_306 = 1UL;
        int32_t * const *l_317 = &l_188;
        int32_t * const **l_316[9][9][3] = {{{&l_317,&l_317,(void*)0},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,(void*)0},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317}},{{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,&l_317},{(void*)0,&l_317,&l_317},{&l_317,&l_317,&l_317},{(void*)0,&l_317,&l_317}},{{(void*)0,(void*)0,(void*)0},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,(void*)0},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{(void*)0,(void*)0,&l_317},{(void*)0,&l_317,&l_317},{&l_317,(void*)0,&l_317}},{{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,&l_317},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{(void*)0,(void*)0,&l_317}},{{&l_317,&l_317,(void*)0},{(void*)0,(void*)0,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,(void*)0},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,(void*)0},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317}},{{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,&l_317}},{{(void*)0,&l_317,&l_317},{&l_317,&l_317,&l_317},{(void*)0,&l_317,&l_317},{(void*)0,(void*)0,(void*)0},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,(void*)0},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317}},{{(void*)0,(void*)0,&l_317},{(void*)0,&l_317,&l_317},{&l_317,&l_317,&l_317},{(void*)0,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317}},{{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{&l_317,(void*)0,&l_317},{(void*)0,&l_317,(void*)0},{&l_317,(void*)0,&l_317},{&l_317,&l_317,&l_317},{(void*)0,&l_317,(void*)0},{&l_317,&l_317,&l_317},{&l_317,&l_317,&l_317}}};
        int i, j, k;
    }
    return p_72;
}


/* ------------------------------------------ */
/* 
 * reads : g_45 g_76 g_90 g_97 g_104
 * writes: g_97 g_104
 */
static int32_t  func_79(const uint64_t  p_80, uint8_t  p_81)
{ /* block id: 17 */
    int32_t l_93 = (-9L);
    uint64_t *l_96 = &g_97;
    uint32_t *l_100 = (void*)0;
    uint32_t *l_101[9] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_102 = 0x5D8015DFL;
    int32_t *l_103 = &g_104[0];
    int32_t ***l_105 = (void*)0;
    int32_t **l_107 = &l_103;
    int32_t ***l_106 = &l_107;
    int32_t **l_108 = &l_103;
    int i;
    (*l_103) ^= ((((g_45 <= (safe_sub_func_uint16_t_u_u(((l_93 > (((g_76 == (safe_mul_func_int16_t_s_s(g_90, (l_93 && (0x4ABBE4CD0C3B02FFLL || (--(*l_96))))))) | (l_102 = p_80)) | l_93)) | 0xA1D3BC2F6D6459F1LL), p_81))) || p_81) ^ 247UL) , 0x67FC1DF0L);
    (*l_106) = (void*)0;
    (*l_108) = (void*)0;
    return p_80;
}




/* ---------------------------------------- */
//testcase_id 1484153281
int case1484153281(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_3[i], "g_3[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_28, "g_28", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_31[i][j], "g_31[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_36, "g_36", print_hash_value);
    transparent_crc(g_45, "g_45", print_hash_value);
    transparent_crc(g_53, "g_53", print_hash_value);
    transparent_crc(g_76, "g_76", print_hash_value);
    transparent_crc(g_90, "g_90", print_hash_value);
    transparent_crc(g_97, "g_97", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_104[i], "g_104[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_126, "g_126", print_hash_value);
    transparent_crc(g_145, "g_145", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_184[i], "g_184[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_216[i], "g_216[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_219[i], "g_219[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_244, "g_244", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_246[i], "g_246[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_268, "g_268", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_318[i], "g_318[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_319[i], "g_319[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_320, "g_320", print_hash_value);
    transparent_crc(g_365, "g_365", print_hash_value);
    transparent_crc(g_393, "g_393", print_hash_value);
    transparent_crc(g_394, "g_394", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        transparent_crc(g_456[i], "g_456[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_490, "g_490", print_hash_value);
    transparent_crc(g_495, "g_495", print_hash_value);
    transparent_crc(g_578, "g_578", print_hash_value);
    transparent_crc(g_702, "g_702", print_hash_value);
    transparent_crc(g_846, "g_846", print_hash_value);
    transparent_crc(g_1200, "g_1200", print_hash_value);
    transparent_crc(g_1247, "g_1247", print_hash_value);
    transparent_crc(g_1265, "g_1265", print_hash_value);
    transparent_crc(g_1325, "g_1325", print_hash_value);
    transparent_crc(g_1327, "g_1327", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1368[i], "g_1368[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1435, "g_1435", print_hash_value);
    transparent_crc(g_1470, "g_1470", print_hash_value);
    transparent_crc(g_1476, "g_1476", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 421
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 165
   depth: 2, occurrence: 38
   depth: 3, occurrence: 5
   depth: 4, occurrence: 2
   depth: 6, occurrence: 1
   depth: 7, occurrence: 1
   depth: 11, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 14, occurrence: 1
   depth: 16, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 5
   depth: 21, occurrence: 3
   depth: 23, occurrence: 2
   depth: 24, occurrence: 3
   depth: 25, occurrence: 1
   depth: 26, occurrence: 3
   depth: 28, occurrence: 1
   depth: 31, occurrence: 1
   depth: 34, occurrence: 1
   depth: 35, occurrence: 1
   depth: 36, occurrence: 2
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 40, occurrence: 1

XXX total number of pointers: 274

XXX times a variable address is taken: 692
XXX times a pointer is dereferenced on RHS: 189
breakdown:
   depth: 1, occurrence: 164
   depth: 2, occurrence: 22
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
XXX times a pointer is dereferenced on LHS: 173
breakdown:
   depth: 1, occurrence: 158
   depth: 2, occurrence: 12
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 30
XXX times a pointer is compared with address of another variable: 6
XXX times a pointer is compared with another pointer: 9
XXX times a pointer is qualified to be dereferenced: 5932

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 817
   level: 2, occurrence: 166
   level: 3, occurrence: 67
   level: 4, occurrence: 33
XXX number of pointers point to pointers: 112
XXX number of pointers point to scalars: 162
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31
XXX average alias set size: 1.39

XXX times a non-volatile is read: 1238
XXX times a non-volatile is write: 571
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 1
XXX backward jumps: 5

XXX stmts: 171
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 31
   depth: 1, occurrence: 27
   depth: 2, occurrence: 26
   depth: 3, occurrence: 30
   depth: 4, occurrence: 27
   depth: 5, occurrence: 30

XXX percentage a fresh-made variable is used: 16.9
XXX percentage an existing variable is used: 83.1
********************* end of statistics **********************/

