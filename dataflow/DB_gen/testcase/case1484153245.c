/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      1829129265
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int32_t g_7 = 0xE95AAD85L;
static uint16_t g_12[4] = {1UL,1UL,1UL,1UL};
static uint8_t g_16 = 0xA7L;
static int32_t g_19 = 0xC06E3268L;


/* --- FORWARD DECLARATIONS --- */
static uint64_t  func_1(void);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_12 g_19
 * writes: g_16 g_19
 */
static uint64_t  func_1(void)
{ /* block id: 0 */
    int16_t l_4 = 0x7F0AL;
    uint16_t *l_10 = (void*)0;
    uint16_t *l_11[8] = {&g_12[0],&g_12[0],(void*)0,&g_12[0],&g_12[0],(void*)0,&g_12[0],&g_12[0]};
    int32_t l_13 = (-1L);
    int32_t l_14 = 0xB0A6D081L;
    uint8_t *l_15 = &g_16;
    int32_t l_17 = 0x2883E731L;
    int32_t *l_18 = &g_19;
    int i;
    (*l_18) = ((((l_4 == ((safe_sub_func_uint8_t_u_u(((*l_15) = (((g_7 > ((g_7 , (g_7 ^ (l_4 == (((((safe_lshift_func_uint8_t_u_s(((((l_13 = 0UL) & (l_14 ^ l_4)) < ((l_11[1] != (void*)0) > 4294967295UL)) | 65527UL), l_14)) , l_13) , g_12[3]) & g_7) != 0xF36AL)))) >= 0x0ADCL)) != l_4) , 0x34L)), l_17)) < 1UL)) < (-7L)) <= 0x26CDL) & 1UL);
    return (*l_18);
}




/* ---------------------------------------- */
//testcase_id 1484153245
int case1484153245(void)
{
    int i;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_7, "g_7", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_12[i], "g_12[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_16, "g_16", print_hash_value);
    transparent_crc(g_19, "g_19", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 5
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 27
breakdown:
   depth: 1, occurrence: 2
   depth: 27, occurrence: 1

XXX total number of pointers: 4

XXX times a variable address is taken: 3
XXX times a pointer is dereferenced on RHS: 1
breakdown:
   depth: 1, occurrence: 1
XXX times a pointer is dereferenced on LHS: 2
breakdown:
   depth: 1, occurrence: 2
XXX times a pointer is compared with null: 1
XXX times a pointer is compared with address of another variable: 0
XXX times a pointer is compared with another pointer: 0
XXX times a pointer is qualified to be dereferenced: 1

XXX max dereference level: 1
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 3
XXX number of pointers point to pointers: 0
XXX number of pointers point to scalars: 4
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 50
XXX average alias set size: 1.25

XXX times a non-volatile is read: 16
XXX times a non-volatile is write: 5
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 0

XXX stmts: 2
XXX max block depth: 0
breakdown:
   depth: 0, occurrence: 2

XXX percentage a fresh-made variable is used: 31.2
XXX percentage an existing variable is used: 68.8
********************* end of statistics **********************/

