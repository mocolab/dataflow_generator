/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      1857189619
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int64_t g_19 = 0xC92EA21E1D82C32BLL;
static int32_t g_46 = 1L;
static int32_t g_56[9] = {0xC1994476L,0xC1994476L,0xC1994476L,0xC1994476L,0xC1994476L,0xC1994476L,0xC1994476L,0xC1994476L,0xC1994476L};
static const int32_t *g_55 = &g_56[0];
static int32_t g_57 = 1L;
static int32_t g_58[5][7] = {{1L,0x34EF9DEAL,0x7D9C74E3L,0L,0x90B51ECDL,0L,0x7D9C74E3L},{9L,9L,0x6A8B5F6EL,2L,0x34EF9DEAL,(-1L),1L},{1L,0L,0x6A8B5F6EL,0x6A8B5F6EL,0L,1L,0x90B51ECDL},{0L,0x6A8B5F6EL,0x7D9C74E3L,0x90B51ECDL,0x34EF9DEAL,0x34EF9DEAL,0x90B51ECDL},{2L,(-1L),2L,(-1L),0x90B51ECDL,0L,1L}};
static uint16_t g_60 = 1UL;
static uint16_t g_87 = 1UL;
static uint64_t g_119 = 0xB0CF0C6D799D52CCLL;
static int16_t g_165 = 0x9A83L;
static int64_t g_184 = 0L;
static int16_t g_186 = (-1L);
static uint32_t g_267 = 0x2AA1A6C8L;
static int8_t g_277 = (-10L);
static uint32_t g_344 = 0xEB773CABL;
static int16_t g_375 = 0x6AFCL;
static uint16_t g_378 = 0xB321L;
static uint64_t *g_388 = &g_119;
static uint64_t * const * const g_387 = &g_388;
static int8_t g_396 = 0x7EL;
static int8_t g_399 = 0x80L;
static uint16_t *g_405 = &g_60;
static uint16_t **g_404 = &g_405;
static uint32_t g_431[3] = {0xE580FF98L,0xE580FF98L,0xE580FF98L};
static uint32_t g_441 = 0UL;
static int8_t g_453 = 0xF5L;
static int8_t g_463 = 0L;
static int16_t g_494[3][6] = {{0xC9D2L,0x5D5FL,0xC100L,0xC9D2L,0x6B64L,0x6B64L},{4L,0x5D5FL,0x5D5FL,4L,0x6B64L,0xC100L},{0xF02DL,0x5D5FL,0x6B64L,0xF02DL,0x6B64L,0x5D5FL}};
static uint8_t g_518 = 0x00L;
static const uint8_t * const g_517[7][8] = {{&g_518,&g_518,(void*)0,(void*)0,(void*)0,&g_518,&g_518,&g_518},{&g_518,(void*)0,&g_518,(void*)0,&g_518,(void*)0,&g_518,(void*)0},{(void*)0,(void*)0,(void*)0,&g_518,&g_518,&g_518,(void*)0,(void*)0},{&g_518,&g_518,(void*)0,(void*)0,(void*)0,&g_518,&g_518,&g_518},{&g_518,(void*)0,&g_518,(void*)0,&g_518,(void*)0,&g_518,(void*)0},{(void*)0,(void*)0,(void*)0,&g_518,&g_518,&g_518,(void*)0,(void*)0},{&g_518,&g_518,(void*)0,(void*)0,(void*)0,&g_518,&g_518,&g_518}};
static int16_t *g_565[1] = {&g_375};
static int16_t ** const g_564[5] = {&g_565[0],&g_565[0],&g_565[0],&g_565[0],&g_565[0]};
static const int32_t **g_640 = (void*)0;
static const int32_t ***g_639 = &g_640;
static int32_t ***g_754 = (void*)0;
static int16_t g_761 = 1L;
static uint64_t g_865 = 0x29D0FE60EF2F4EBELL;
static const int32_t g_867 = 0xD8A85BCCL;
static int32_t *g_868 = (void*)0;
static int32_t g_870[7][2] = {{0x07913576L,3L},{0x07913576L,3L},{0x07913576L,3L},{0x07913576L,3L},{0x07913576L,3L},{0x07913576L,3L},{0x07913576L,3L}};
static const int16_t g_883[5][1] = {{0L},{0L},{0L},{0L},{0L}};
static const int16_t *g_885 = &g_883[3][0];
static const int16_t **g_884[3] = {&g_885,&g_885,&g_885};
static uint8_t g_896 = 0x17L;
static uint32_t *g_911 = &g_431[2];
static uint32_t **g_910[7][2][9] = {{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911}},{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{&g_911,&g_911,&g_911,(void*)0,&g_911,&g_911,(void*)0,&g_911,&g_911}},{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{&g_911,(void*)0,(void*)0,(void*)0,(void*)0,&g_911,&g_911,&g_911,(void*)0}},{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{&g_911,&g_911,&g_911,(void*)0,&g_911,&g_911,&g_911,&g_911,&g_911}},{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{(void*)0,&g_911,&g_911,&g_911,(void*)0,(void*)0,(void*)0,(void*)0,&g_911}},{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,(void*)0}},{{&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911,&g_911},{&g_911,(void*)0,&g_911,&g_911,&g_911,(void*)0,(void*)0,&g_911,(void*)0}}};
static const uint32_t *g_984 = &g_431[2];
static const uint32_t **g_983 = &g_984;
static int32_t g_1082 = 5L;
static const int32_t g_1164[5][7] = {{(-1L),(-1L),0L,0L,0xC5A599F6L,0L,0x2BCB62BCL},{5L,0xB0C922D4L,0L,(-1L),0x2BCB62BCL,0x2C70243CL,0x2C70243CL},{0xC5A599F6L,0L,(-1L),0L,0xC5A599F6L,(-1L),5L},{0x3F8A9E7FL,0L,(-1L),0x2CFFFA96L,0L,0L,1L},{0x2CFFFA96L,(-1L),0L,0L,0L,0L,(-1L)}};
static int64_t **g_1175 = (void*)0;
static int16_t **g_1201 = &g_565[0];
static int16_t ***g_1200[6][3][6] = {{{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,&g_1201},{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,(void*)0},{(void*)0,&g_1201,&g_1201,(void*)0,&g_1201,&g_1201}},{{(void*)0,&g_1201,&g_1201,&g_1201,&g_1201,&g_1201},{&g_1201,&g_1201,&g_1201,&g_1201,&g_1201,(void*)0},{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,&g_1201}},{{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,(void*)0},{(void*)0,&g_1201,&g_1201,(void*)0,&g_1201,&g_1201},{(void*)0,&g_1201,&g_1201,&g_1201,&g_1201,&g_1201}},{{&g_1201,&g_1201,&g_1201,&g_1201,&g_1201,(void*)0},{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,&g_1201},{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,(void*)0}},{{(void*)0,&g_1201,&g_1201,(void*)0,&g_1201,&g_1201},{(void*)0,&g_1201,&g_1201,&g_1201,&g_1201,&g_1201},{&g_1201,&g_1201,&g_1201,&g_1201,&g_1201,(void*)0}},{{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,&g_1201},{&g_1201,&g_1201,(void*)0,&g_1201,&g_1201,(void*)0},{(void*)0,&g_1201,&g_1201,(void*)0,&g_1201,&g_1201}}};
static int16_t ****g_1199 = &g_1200[5][0][3];
static uint8_t g_1253 = 252UL;
static uint32_t * const g_1257 = &g_267;
static int16_t *** const g_1286 = (void*)0;
static int16_t *** const *g_1285 = &g_1286;
static uint32_t g_1515 = 4294967293UL;
static int16_t g_1517 = 0xD130L;
static int32_t ****g_1576 = &g_754;
static int32_t *****g_1575[9] = {&g_1576,&g_1576,&g_1576,&g_1576,&g_1576,&g_1576,&g_1576,&g_1576,&g_1576};
static uint16_t *g_1584 = &g_378;
static int16_t *****g_1603 = &g_1199;
static int32_t g_1612 = 2L;
static uint16_t ***g_1681 = &g_404;
static uint16_t ****g_1680 = &g_1681;
static int32_t g_1682 = 0x1DFDC077L;
static const uint8_t g_1703 = 255UL;
static uint32_t g_1810 = 0x81E15AB0L;
static uint64_t g_1817 = 5UL;
static int64_t g_1862[3][2][7] = {{{0xEA4DB32FD41068E5LL,(-1L),0xEA4DB32FD41068E5LL,(-1L),0xEA4DB32FD41068E5LL,(-1L),0xEA4DB32FD41068E5LL},{1L,1L,0x863F3A7EE3A96FA3LL,0x863F3A7EE3A96FA3LL,1L,1L,0x863F3A7EE3A96FA3LL}},{{0xF1EE3FDC1970B451LL,(-1L),0xF1EE3FDC1970B451LL,(-1L),0xF1EE3FDC1970B451LL,(-1L),0xF1EE3FDC1970B451LL},{1L,0x863F3A7EE3A96FA3LL,0x863F3A7EE3A96FA3LL,1L,1L,0x863F3A7EE3A96FA3LL,0x863F3A7EE3A96FA3LL}},{{0xEA4DB32FD41068E5LL,(-1L),0xEA4DB32FD41068E5LL,(-1L),0xEA4DB32FD41068E5LL,(-1L),0xEA4DB32FD41068E5LL},{1L,1L,0x863F3A7EE3A96FA3LL,0x863F3A7EE3A96FA3LL,1L,1L,0x863F3A7EE3A96FA3LL}}};
static int32_t **g_1933 = &g_868;
static int32_t g_1999[2] = {0xD2980BFFL,0xD2980BFFL};
static int64_t *g_2050 = &g_19;
static int64_t **g_2049 = &g_2050;
static int16_t g_2060 = 0x7B2DL;
static uint64_t g_2087 = 0x09751C00DF1A49AELL;
static int32_t g_2169 = 0x58902BA9L;
static uint32_t g_2179 = 0xE90EAAB5L;
static const int32_t g_2183[4][5] = {{(-1L),5L,0x1AA23A8BL,5L,(-1L)},{0xFD09BD23L,(-10L),(-1L),(-10L),0xFD09BD23L},{(-1L),5L,0x1AA23A8BL,5L,(-1L)},{0xFD09BD23L,(-10L),(-1L),(-10L),0xFD09BD23L}};
static const int32_t *g_2246[7][7][5] = {{{&g_1082,&g_1082,(void*)0,&g_1082,(void*)0},{(void*)0,&g_867,&g_867,&g_2169,&g_867},{&g_2169,&g_1082,&g_1082,(void*)0,&g_1082},{&g_867,&g_867,&g_1082,&g_2169,&g_867},{&g_2169,&g_1082,&g_867,&g_867,&g_1082},{(void*)0,&g_2169,&g_2169,&g_867,&g_1082},{&g_867,(void*)0,&g_2169,(void*)0,&g_2169}},{{&g_1082,&g_867,&g_867,&g_1082,&g_867},{&g_2169,&g_2169,&g_1082,&g_1082,&g_2169},{&g_867,&g_867,&g_1082,&g_1082,&g_867},{&g_2169,&g_1082,&g_867,&g_1082,&g_1082},{&g_867,(void*)0,(void*)0,&g_1082,&g_1082},{&g_2169,&g_1082,&g_867,(void*)0,&g_1082},{&g_1082,&g_867,(void*)0,&g_867,&g_1082}},{{(void*)0,&g_1082,&g_867,&g_867,&g_1082},{&g_867,&g_867,(void*)0,&g_2169,&g_1082},{&g_867,(void*)0,&g_867,(void*)0,&g_867},{(void*)0,&g_1082,(void*)0,&g_2169,&g_2169},{&g_867,&g_1082,&g_1082,&g_1082,&g_867},{&g_867,&g_1082,(void*)0,&g_867,&g_2169},{(void*)0,&g_867,(void*)0,&g_867,&g_1082}},{{&g_1082,&g_867,&g_1082,&g_1082,&g_1082},{&g_2169,&g_1082,&g_2169,&g_867,&g_867},{&g_867,&g_1082,&g_1082,(void*)0,&g_1082},{&g_2169,&g_1082,&g_2169,&g_2169,&g_867},{&g_867,(void*)0,&g_867,&g_2169,&g_2169},{&g_1082,&g_1082,&g_867,(void*)0,(void*)0},{(void*)0,&g_867,(void*)0,&g_1082,&g_1082}},{{&g_1082,&g_1082,&g_1082,&g_1082,&g_867},{&g_2169,(void*)0,&g_2169,(void*)0,&g_2169},{(void*)0,&g_2169,&g_867,&g_2169,(void*)0},{&g_867,&g_867,&g_2169,&g_867,&g_2169},{&g_1082,&g_867,&g_867,&g_2169,(void*)0},{&g_867,&g_867,&g_2169,&g_1082,&g_2169},{(void*)0,&g_1082,&g_1082,(void*)0,&g_867}},{{&g_2169,&g_2169,&g_867,&g_867,&g_1082},{&g_2169,&g_867,(void*)0,&g_867,(void*)0},{(void*)0,(void*)0,&g_2169,&g_2169,&g_2169},{&g_867,&g_867,&g_867,(void*)0,&g_1082},{&g_1082,(void*)0,&g_2169,&g_867,&g_2169},{&g_867,&g_867,(void*)0,(void*)0,&g_1082},{(void*)0,(void*)0,&g_1082,&g_1082,(void*)0}},{{&g_2169,&g_867,(void*)0,&g_867,&g_1082},{&g_1082,&g_2169,(void*)0,&g_867,&g_867},{(void*)0,&g_1082,&g_1082,(void*)0,&g_1082},{&g_1082,&g_867,(void*)0,&g_1082,&g_1082},{&g_867,&g_867,&g_2169,&g_867,&g_867},{(void*)0,&g_867,&g_867,&g_1082,(void*)0},{&g_1082,&g_2169,&g_2169,(void*)0,&g_867}}};
static const int32_t **g_2245 = &g_2246[1][1][3];
static int16_t ** const *g_2474 = &g_564[3];
static int16_t ** const **g_2473[3][10][3] = {{{(void*)0,&g_2474,&g_2474},{(void*)0,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,(void*)0,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,(void*)0},{&g_2474,(void*)0,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474}},{{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,(void*)0},{(void*)0,&g_2474,&g_2474},{(void*)0,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,(void*)0,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,(void*)0},{&g_2474,(void*)0,&g_2474}},{{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,(void*)0},{&g_2474,&g_2474,(void*)0},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474},{&g_2474,&g_2474,&g_2474}}};
static int16_t ** const ***g_2472 = &g_2473[0][5][0];
static int16_t ** const ****g_2471 = &g_2472;
static uint8_t g_2562 = 1UL;
static int16_t ******g_2575 = &g_1603;
static int16_t * const **g_2585[3] = {(void*)0,(void*)0,(void*)0};
static int16_t * const ***g_2584 = &g_2585[0];
static int16_t * const **** const g_2583 = &g_2584;
static int16_t * const **** const *g_2582 = &g_2583;
static uint32_t g_2693[8][5][4] = {{{18446744073709551615UL,0x6702D9FAL,18446744073709551615UL,0UL},{3UL,5UL,0xF1E8CC54L,0x8D5A50DAL},{0xBE43B578L,0xF8F1976CL,5UL,5UL},{1UL,1UL,5UL,18446744073709551615UL},{0xBE43B578L,0xA0879DE3L,0xF1E8CC54L,0xF8F1976CL}},{{3UL,0xF1E8CC54L,18446744073709551615UL,0xF1E8CC54L},{18446744073709551615UL,0xF1E8CC54L,3UL,0xF8F1976CL},{0xF1E8CC54L,0xA0879DE3L,0xBE43B578L,18446744073709551615UL},{5UL,1UL,1UL,5UL},{5UL,0xF8F1976CL,0xBE43B578L,0x8D5A50DAL}},{{0xF1E8CC54L,5UL,3UL,0UL},{18446744073709551615UL,0x6702D9FAL,18446744073709551615UL,0UL},{3UL,5UL,0xF1E8CC54L,0x8D5A50DAL},{0xBE43B578L,0xF8F1976CL,5UL,5UL},{1UL,1UL,5UL,18446744073709551615UL}},{{0xBE43B578L,0xA0879DE3L,0xF1E8CC54L,0xF8F1976CL},{3UL,0xF1E8CC54L,18446744073709551615UL,0xF1E8CC54L},{18446744073709551615UL,0xF1E8CC54L,3UL,0xF8F1976CL},{0xF1E8CC54L,0xA0879DE3L,0xBE43B578L,18446744073709551615UL},{5UL,1UL,1UL,5UL}},{{5UL,0xF8F1976CL,0xBE43B578L,0x8D5A50DAL},{0xF1E8CC54L,5UL,3UL,0UL},{18446744073709551615UL,0x6702D9FAL,18446744073709551615UL,0UL},{3UL,5UL,0xF1E8CC54L,0x8D5A50DAL},{0xBE43B578L,0xF8F1976CL,5UL,5UL}},{{1UL,1UL,5UL,18446744073709551615UL},{0xBE43B578L,0xA0879DE3L,0xF1E8CC54L,0xF8F1976CL},{3UL,0xF1E8CC54L,18446744073709551615UL,0xF1E8CC54L},{18446744073709551615UL,0xF1E8CC54L,3UL,0xF8F1976CL},{0xF1E8CC54L,0xA0879DE3L,0xBE43B578L,18446744073709551615UL}},{{5UL,1UL,1UL,5UL},{5UL,0xF8F1976CL,0xBE43B578L,0x8D5A50DAL},{0xF1E8CC54L,5UL,3UL,0UL},{18446744073709551615UL,0x6702D9FAL,18446744073709551615UL,0UL},{3UL,5UL,0xF1E8CC54L,0x8D5A50DAL}},{{0xBE43B578L,0xF8F1976CL,5UL,5UL},{1UL,1UL,5UL,18446744073709551615UL},{0xBE43B578L,0xA0879DE3L,0xF1E8CC54L,0xF8F1976CL},{3UL,0xF1E8CC54L,18446744073709551615UL,0xF1E8CC54L},{18446744073709551615UL,0xF1E8CC54L,3UL,0xF8F1976CL}}};
static int32_t g_2694 = 0xD7F79577L;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int8_t  func_4(uint32_t  p_5, int8_t  p_6, int8_t  p_7);
static int8_t  func_9(uint32_t  p_10, int16_t  p_11, const int64_t  p_12, int8_t  p_13, int32_t  p_14);
static int8_t  func_24(int32_t  p_25, int8_t  p_26);
static int32_t  func_27(uint64_t  p_28, int32_t  p_29);
static int8_t  func_35(const int32_t  p_36, uint8_t  p_37, int8_t  p_38);
static const int32_t  func_39(uint32_t  p_40, uint64_t  p_41, int8_t  p_42);
static uint16_t  func_43(uint32_t  p_44);
static uint16_t  func_47(int32_t * p_48, uint64_t  p_49, int32_t * p_50, int32_t * p_51);
static int32_t * func_52(const int32_t * p_53, int32_t * p_54);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_19 g_431 g_388 g_119 g_165 g_1933 g_868 g_1999 g_55 g_56 g_57 g_58 g_60 g_387 g_404 g_405 g_494 g_441 g_2087 g_1612 g_984 g_1680 g_1681 g_2049 g_2050 g_885 g_883 g_911 g_1810 g_1164 g_870 g_1285 g_1286 g_46 g_2472 g_2473 g_867 g_2245 g_1584 g_378 g_1257 g_375 g_396 g_1515 g_1253 g_896 g_2562 g_1201 g_565 g_2575 g_1682 g_2183 g_2169 g_1575 g_1576 g_1862 g_518 g_453
 * writes: g_277 g_184 g_868 g_57 g_58 g_60 g_56 g_87 g_55 g_119 g_2087 g_1612 g_1999 g_431 g_453 g_19 g_1253 g_870 g_46 g_2471 g_2246 g_2169 g_267 g_375 g_396 g_1681 g_1682 g_2562 g_2582 g_378 g_1862
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    uint32_t l_8 = 0x61AB6EC7L;
    int32_t l_2251 = (-4L);
    uint16_t l_2252 = 0x8BBAL;
    int8_t *l_2253 = (void*)0;
    int8_t *l_2254 = &g_277;
    int64_t *l_2255 = &g_184;
    uint8_t l_2734 = 0xC6L;
    int32_t l_2735 = 0x5D8E2CF3L;
    int32_t *l_2736 = &g_870[4][1];
    (*l_2736) = (l_2735 = (((safe_sub_func_int32_t_s_s(((((((*l_2254) = func_4(l_8, func_9(((safe_lshift_func_uint8_t_u_u(0x88L, 6)) , (0x9614L && (safe_sub_func_uint64_t_u_u((l_8 && ((g_19 >= (g_19 || ((((*l_2255) = (((0x74L & ((*l_2254) = (safe_mod_func_int64_t_s_s(((safe_lshift_func_uint16_t_u_s(((l_2251 = (func_24(func_27(l_8, l_8), g_431[2]) == 1UL)) == l_8), l_8)) || 2UL), l_2252)))) ^ l_8) > 1UL)) <= l_8) > 0x8FL))) || (*g_388))), l_2252)))), l_2252, l_2252, l_8, l_2252), g_165)) == l_8) , g_494[0][4]) , l_2251) ^ 0xC01726B80C234191LL), 1L)) , l_2734) , l_2252));
    return (*l_2736);
}


/* ------------------------------------------ */
/* 
 * reads : g_1933 g_868 g_1999 g_55 g_56 g_57 g_58 g_60 g_119 g_387 g_388 g_404 g_405 g_494 g_441 g_2087 g_1612 g_984 g_431 g_1680 g_1681 g_2049 g_2050 g_19 g_885 g_883 g_911 g_1810 g_1164 g_870 g_1285 g_1286 g_46 g_2472 g_2473 g_867 g_2245 g_1584 g_378 g_1257 g_1515 g_1253 g_896 g_2562 g_1201 g_565 g_2575 g_1682 g_2183 g_2169 g_1575 g_1576 g_1862 g_518 g_453 g_375 g_396
 * writes: g_868 g_57 g_58 g_60 g_56 g_87 g_55 g_119 g_2087 g_1612 g_1999 g_431 g_453 g_19 g_1253 g_870 g_46 g_2471 g_2246 g_2169 g_267 g_375 g_396 g_184 g_1681 g_1682 g_2562 g_2582 g_378 g_1862
 */
static int8_t  func_4(uint32_t  p_5, int8_t  p_6, int8_t  p_7)
{ /* block id: 1057 */
    int8_t l_2257 = 0x04L;
    int8_t *l_2258 = (void*)0;
    int8_t *l_2259 = &g_453;
    const int32_t *l_2260 = (void*)0;
    int32_t *l_2261 = &g_1999[1];
    int32_t l_2271 = 0x0A98CEEFL;
    int32_t l_2272 = (-2L);
    int32_t l_2276 = (-10L);
    int32_t l_2277 = 1L;
    int32_t l_2278 = (-1L);
    int32_t l_2279 = 5L;
    int16_t ** const *l_2288 = (void*)0;
    int16_t ** const **l_2287 = &l_2288;
    int16_t ** const ***l_2286 = &l_2287;
    uint8_t * const l_2330 = &g_1253;
    const int64_t *l_2368 = (void*)0;
    const int64_t **l_2367 = &l_2368;
    int32_t l_2410 = 0x620B5F74L;
    int32_t l_2411[3];
    int32_t l_2412 = 0x049431B3L;
    int32_t ******l_2422 = &g_1575[7];
    const uint32_t l_2459 = 18446744073709551607UL;
    int64_t l_2463 = 0x1E5B4BAF5F1EEABFLL;
    int16_t *****l_2484 = (void*)0;
    const uint32_t l_2490 = 1UL;
    const int16_t l_2543 = 0xFB60L;
    int64_t l_2549 = 1L;
    int32_t l_2573 = (-1L);
    int16_t * const *l_2580 = (void*)0;
    int16_t * const **l_2579 = &l_2580;
    int16_t * const ***l_2578 = &l_2579;
    int16_t * const ****l_2577[4] = {&l_2578,&l_2578,&l_2578,&l_2578};
    int16_t * const **** const *l_2576 = &l_2577[3];
    const int16_t l_2608[9][7][2] = {{{9L,1L},{1L,0x49FCL},{(-1L),(-7L)},{9L,0x49FCL},{8L,0x839CL},{8L,0x49FCL},{0x2439L,8L}},{{0x49FCL,0x839CL},{9L,9L},{0x2439L,9L},{9L,0x839CL},{0x49FCL,8L},{0x2439L,0x49FCL},{8L,0x839CL}},{{8L,0x49FCL},{0x2439L,8L},{0x49FCL,0x839CL},{9L,9L},{0x2439L,9L},{9L,0x839CL},{0x49FCL,8L}},{{0x2439L,0x49FCL},{8L,0x839CL},{8L,0x49FCL},{0x2439L,8L},{0x49FCL,0x839CL},{9L,9L},{0x2439L,9L}},{{9L,0x839CL},{0x49FCL,8L},{0x2439L,0x49FCL},{8L,0x839CL},{8L,0x49FCL},{0x2439L,8L},{0x49FCL,0x839CL}},{{9L,9L},{0x2439L,9L},{9L,0x839CL},{0x49FCL,8L},{0x2439L,0x49FCL},{8L,0x839CL},{8L,0x49FCL}},{{0x2439L,8L},{0x49FCL,0x839CL},{9L,9L},{0x2439L,9L},{9L,0x839CL},{0x49FCL,8L},{0x2439L,0x49FCL}},{{8L,0x839CL},{8L,0x49FCL},{0x2439L,8L},{0x49FCL,0x839CL},{9L,9L},{0x2439L,9L},{9L,0x839CL}},{{0x49FCL,8L},{0x2439L,0x49FCL},{8L,0x839CL},{8L,0x49FCL},{0x2439L,8L},{0x49FCL,0x839CL},{9L,9L}}};
    uint32_t l_2689 = 18446744073709551615UL;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_2411[i] = (-9L);
    (*g_1933) = l_2261;
    if ((*g_868))
    { /* block id: 1061 */
        const int32_t *l_2264 = &g_1999[1];
        int32_t *l_2268 = &g_2169;
        int32_t l_2273 = 1L;
        int32_t l_2274 = 0L;
        int32_t l_2275 = 0x32987C83L;
        int32_t l_2280 = 0xC489718BL;
        if ((*g_55))
        { /* block id: 1062 */
            uint16_t l_2265 = 2UL;
            int32_t *l_2267 = (void*)0;
            int32_t **l_2266 = &l_2267;
            int32_t *l_2269 = &g_1612;
            (*l_2269) &= (0x47C9D5FAL > (safe_rshift_func_int16_t_s_u((1L >= (g_2087 &= (8UL < (((*l_2266) = func_52(l_2264, (l_2265 , (void*)0))) != l_2268)))), (*l_2264))));
        }
        else
        { /* block id: 1066 */
            int32_t *l_2270[4][9] = {{&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682},{&g_57,&g_870[2][0],&g_57,&g_57,&g_870[2][0],&g_57,&g_57,&g_870[2][0],&g_57},{&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682,&g_1682},{&g_57,&g_870[2][0],&g_57,&g_57,&g_870[2][0],&g_57,&g_57,&g_870[2][0],&g_57}};
            uint8_t l_2281 = 0x1FL;
            int i, j;
            ++l_2281;
            l_2280 = (safe_add_func_int32_t_s_s((&g_1199 == l_2286), (l_2271 = (((*l_2261) |= 1L) | (((safe_sub_func_int64_t_s_s((p_7 , p_7), ((safe_lshift_func_uint16_t_u_s((+1UL), 9)) & (((**g_387)--) && (safe_rshift_func_uint16_t_u_u(((((*g_984) > (safe_add_func_uint8_t_u_u((safe_div_func_uint64_t_u_u(((safe_mod_func_uint16_t_u_u(((((((safe_rshift_func_uint16_t_u_s(((safe_mul_func_int16_t_s_s(p_5, (safe_rshift_func_int8_t_s_u((safe_mod_func_uint32_t_u_u((((*g_55) , p_7) < (****g_1680)), p_6)), p_5)))) < p_5), p_7)) >= 0x093F7740323AA363LL) >= (**g_2049)) < p_5) >= (-1L)) < (*g_2050)), (*g_885))) , p_6), 0xEBD5D228C8C4E1B8LL)), p_6))) < (**g_2049)) > 0UL), 1)))))) && 0x9C4BL) , p_7)))));
            (*l_2261) = (*l_2264);
        }
        (**g_1933) = p_5;
    }
    else
    { /* block id: 1075 */
        int16_t *l_2314 = (void*)0;
        int32_t l_2318 = 5L;
        int32_t l_2369 = 0L;
        int32_t l_2370[6][1] = {{9L},{0x18F14153L},{0x18F14153L},{9L},{0x18F14153L},{0x18F14153L}};
        int32_t l_2371 = 0L;
        int32_t l_2372[3];
        int64_t l_2373 = (-5L);
        uint16_t l_2374 = 65530UL;
        int32_t *l_2375 = &l_2279;
        uint8_t *l_2391[7];
        int32_t *l_2392 = (void*)0;
        int32_t *l_2393 = &g_870[6][1];
        int32_t l_2439 = 0xD572D656L;
        int32_t ******l_2462 = &g_1575[4];
        int16_t *****l_2483 = &g_1199;
        const int16_t ***l_2487 = &g_884[0];
        const int16_t ****l_2486[7][6] = {{(void*)0,&l_2487,(void*)0,&l_2487,&l_2487,&l_2487},{(void*)0,&l_2487,(void*)0,&l_2487,&l_2487,&l_2487},{(void*)0,&l_2487,(void*)0,&l_2487,&l_2487,&l_2487},{(void*)0,&l_2487,(void*)0,&l_2487,&l_2487,&l_2487},{(void*)0,&l_2487,(void*)0,&l_2487,&l_2487,&l_2487},{&l_2487,&l_2487,&l_2487,&l_2487,(void*)0,&l_2487},{&l_2487,&l_2487,&l_2487,&l_2487,(void*)0,&l_2487}};
        const int16_t ***** const l_2485 = &l_2486[0][1];
        int16_t **l_2589 = &g_565[0];
        const uint64_t *l_2642 = (void*)0;
        const uint64_t **l_2641[9][8][3] = {{{&l_2642,(void*)0,&l_2642},{&l_2642,(void*)0,&l_2642},{(void*)0,&l_2642,&l_2642},{(void*)0,(void*)0,(void*)0},{(void*)0,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642}},{{&l_2642,&l_2642,&l_2642},{&l_2642,(void*)0,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,(void*)0,(void*)0},{&l_2642,&l_2642,(void*)0},{&l_2642,&l_2642,&l_2642},{(void*)0,&l_2642,&l_2642},{(void*)0,&l_2642,&l_2642}},{{&l_2642,&l_2642,&l_2642},{&l_2642,(void*)0,&l_2642},{(void*)0,&l_2642,&l_2642},{&l_2642,(void*)0,(void*)0},{&l_2642,(void*)0,&l_2642},{(void*)0,&l_2642,(void*)0},{&l_2642,&l_2642,(void*)0},{&l_2642,&l_2642,&l_2642}},{{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,(void*)0,(void*)0},{(void*)0,(void*)0,&l_2642},{&l_2642,(void*)0,(void*)0},{&l_2642,&l_2642,&l_2642},{(void*)0,&l_2642,&l_2642},{&l_2642,&l_2642,(void*)0}},{{&l_2642,&l_2642,&l_2642},{(void*)0,&l_2642,&l_2642},{(void*)0,(void*)0,&l_2642},{&l_2642,&l_2642,(void*)0},{&l_2642,(void*)0,&l_2642},{&l_2642,(void*)0,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,(void*)0}},{{&l_2642,&l_2642,&l_2642},{&l_2642,(void*)0,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,(void*)0},{(void*)0,(void*)0,&l_2642},{(void*)0,&l_2642,&l_2642},{(void*)0,&l_2642,(void*)0},{&l_2642,&l_2642,&l_2642}},{{&l_2642,&l_2642,(void*)0},{(void*)0,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,(void*)0},{&l_2642,&l_2642,(void*)0},{&l_2642,&l_2642,&l_2642},{(void*)0,&l_2642,(void*)0}},{{&l_2642,&l_2642,&l_2642},{(void*)0,&l_2642,&l_2642},{&l_2642,(void*)0,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{&l_2642,&l_2642,&l_2642},{(void*)0,&l_2642,&l_2642}},{{&l_2642,(void*)0,&l_2642},{&l_2642,&l_2642,(void*)0},{&l_2642,&l_2642,&l_2642},{(void*)0,(void*)0,(void*)0},{&l_2642,&l_2642,&l_2642},{&l_2642,(void*)0,(void*)0},{&l_2642,(void*)0,&l_2642},{&l_2642,&l_2642,(void*)0}}};
        int64_t l_2671[9][3][4] = {{{8L,0L,0xA9B57CE6A74B907FLL,0L},{1L,(-10L),0x1F52762B044E901CLL,(-1L)},{1L,(-6L),0xA9B57CE6A74B907FLL,0x0B7E346111636982LL}},{{8L,(-1L),0xB21E9B14FCF5FBF2LL,0x8F9B7C2FF9ED1A06LL},{1L,1L,1L,1L},{0x8F9B7C2FF9ED1A06LL,0xB21E9B14FCF5FBF2LL,(-1L),8L}},{{0x0B7E346111636982LL,0xA9B57CE6A74B907FLL,(-6L),1L},{(-1L),0x1F52762B044E901CLL,(-10L),1L},{0L,0xA9B57CE6A74B907FLL,0L,8L}},{{0x040A6EF0F44265AALL,0xB21E9B14FCF5FBF2LL,3L,1L},{(-1L),1L,(-1L),0x8F9B7C2FF9ED1A06LL},{(-1L),(-1L),0x0472C23863A165C8LL,0x0B7E346111636982LL}},{{0xB21E9B14FCF5FBF2LL,(-6L),0x0B7E346111636982LL,(-1L)},{0L,(-10L),0x0B7E346111636982LL,0L},{0xB21E9B14FCF5FBF2LL,0L,0x0472C23863A165C8LL,0x040A6EF0F44265AALL}},{{(-1L),3L,(-1L),(-1L)},{(-1L),(-1L),3L,(-1L)},{0x040A6EF0F44265AALL,0x0472C23863A165C8LL,0L,0xB21E9B14FCF5FBF2LL}},{{0L,0x0B7E346111636982LL,(-10L),0L},{(-1L),0x0B7E346111636982LL,(-6L),0xB21E9B14FCF5FBF2LL},{0x0B7E346111636982LL,0x0472C23863A165C8LL,(-1L),(-1L)}},{{0x8F9B7C2FF9ED1A06LL,(-1L),1L,(-1L)},{1L,3L,0xB21E9B14FCF5FBF2LL,0x040A6EF0F44265AALL},{8L,0L,0xA9B57CE6A74B907FLL,0L}},{{1L,(-10L),0x1F52762B044E901CLL,(-1L)},{1L,(-6L),0xA9B57CE6A74B907FLL,0x0B7E346111636982LL},{8L,(-1L),0xB21E9B14FCF5FBF2LL,0x8F9B7C2FF9ED1A06LL}}};
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_2372[i] = (-1L);
        for (i = 0; i < 7; i++)
            l_2391[i] = &g_896;
lbl_2609:
        (*l_2375) = (safe_sub_func_uint32_t_u_u(((void*)0 == l_2314), (~(safe_rshift_func_int16_t_s_u(((l_2318 & (safe_add_func_uint8_t_u_u(((((*g_911)--) , (safe_unary_minus_func_uint16_t_u(l_2318))) > (safe_mul_func_int16_t_s_s(((safe_lshift_func_uint8_t_u_s(((safe_lshift_func_uint16_t_u_s((((void*)0 != l_2330) < (safe_div_func_int64_t_s_s(((safe_div_func_int8_t_s_s(((l_2277 ^= (~(safe_sub_func_int64_t_s_s((((safe_mod_func_int8_t_s_s(((safe_mod_func_int16_t_s_s(((((*l_2259) = g_1810) == ((*l_2330) = ((((safe_mul_func_uint16_t_u_u((+(((safe_add_func_uint64_t_u_u((safe_mul_func_int16_t_s_s(((((*g_405) = (safe_mod_func_uint8_t_u_u((((safe_div_func_uint8_t_u_u((!((*g_2050) = (l_2372[1] = ((safe_rshift_func_uint16_t_u_s((safe_div_func_int16_t_s_s((safe_unary_minus_func_int32_t_s((safe_lshift_func_uint8_t_u_s(((((safe_mul_func_int8_t_s_s(((safe_lshift_func_int8_t_s_u(((l_2371 = (((((*l_2261) = (l_2369 &= (safe_sub_func_uint32_t_u_u((((void*)0 != l_2367) == (*l_2261)), p_5)))) && 2L) ^ 0x55L) ^ l_2370[4][0])) , 7L), l_2370[5][0])) >= 0x1C70F4A0L), p_7)) == 1UL) , (*l_2261)) <= 1UL), g_19)))), 0x3707L)), 2)) && 0x84B38A77L)))), 1L)) != l_2373) < 0xA6L), p_7))) <= p_5) , p_5), p_5)), p_5)) , p_5) < l_2370[1][0])), l_2370[4][0])) >= 0xB15C0646L) | 1UL) | p_6))) , p_7), (*g_885))) && p_7), 5L)) || (*l_2261)) <= p_5), (**g_387))))) , 1L), (-4L))) , p_5), (**g_387)))), l_2318)) , p_7), 5)) > l_2318), l_2374))), l_2370[4][0]))) == (**g_387)), 2)))));
        (*l_2393) &= ((!(safe_lshift_func_uint8_t_u_s(((safe_mul_func_uint16_t_u_u(((0x7FA0514BL | (safe_rshift_func_int16_t_s_u((((*l_2330) = ((void*)0 != &l_2258)) != (-1L)), 5))) > (safe_mod_func_uint32_t_u_u(((*g_911) = (p_5 , (((safe_rshift_func_uint8_t_u_u(((*l_2375) = (((-5L) | ((*l_2261) = (safe_lshift_func_int8_t_s_u((-3L), ((safe_mod_func_int64_t_s_s((**g_2049), p_6)) , p_5))))) < 0x1FL)), 7)) | g_1164[1][1]) , 4UL))), p_5))), p_6)) < p_7), p_6))) < p_5);
        if (p_5)
        { /* block id: 1092 */
            int32_t *l_2394 = &l_2276;
            int32_t *l_2395 = &g_58[1][6];
            int32_t *l_2396 = (void*)0;
            int32_t *l_2397 = &g_1682;
            int32_t *l_2398 = &g_46;
            int32_t *l_2399 = &g_1682;
            int32_t *l_2400 = &g_56[7];
            int32_t *l_2401 = &g_56[0];
            int32_t *l_2402 = &l_2272;
            int32_t *l_2403 = &l_2277;
            int32_t *l_2404 = &l_2271;
            int32_t *l_2405 = &l_2272;
            int32_t *l_2406 = &g_870[2][1];
            int32_t *l_2407 = &g_56[0];
            int32_t *l_2408 = (void*)0;
            int32_t *l_2409[7][3] = {{(void*)0,(void*)0,(void*)0},{&g_58[4][0],&g_58[4][0],&g_58[4][0]},{(void*)0,(void*)0,(void*)0},{&g_58[4][0],&g_58[4][0],&g_58[4][0]},{(void*)0,(void*)0,(void*)0},{&g_58[4][0],&g_58[4][0],&g_58[4][0]},{(void*)0,(void*)0,(void*)0}};
            uint64_t l_2413 = 0x673122300B1250D9LL;
            int32_t *******l_2423 = &l_2422;
            int64_t l_2432 = 9L;
            uint8_t l_2433 = 0x6EL;
            int16_t ** const ****l_2470 = &l_2286;
            int16_t ******l_2482[8] = {&g_1603,&g_1603,&g_1603,&g_1603,&g_1603,&g_1603,&g_1603,&g_1603};
            int32_t l_2499 = 0x3C6CE5F6L;
            int16_t l_2590 = 0x5BD8L;
            int32_t l_2619[5];
            int i, j;
            for (i = 0; i < 5; i++)
                l_2619[i] = 0L;
            l_2413++;
            if (((safe_div_func_uint64_t_u_u(((**g_387) = (safe_mod_func_uint16_t_u_u((((*l_2375) >= p_7) , ((((*l_2423) = l_2422) != (void*)0) <= ((safe_mul_func_uint8_t_u_u((safe_sub_func_uint16_t_u_u((safe_mod_func_int32_t_s_s((((((*l_2375) != ((safe_div_func_int8_t_s_s((((0x4B8FFC6A774E767BLL || p_5) , p_5) | ((((*l_2261) && (*l_2393)) | (*l_2402)) > 0x632C6594L)), p_6)) ^ (*l_2261))) , 0x1819L) , (void*)0) == (*g_1285)), (*g_55))), l_2432)), 0x8BL)) ^ 0x30L))), p_7))), p_5)) || 0UL))
            { /* block id: 1096 */
                uint64_t l_2436 = 0x1FBD920C650B9E8DLL;
                int32_t l_2437 = 0xFDB1AD61L;
                int32_t l_2438 = 0L;
                int32_t l_2440 = 0xE3880B99L;
                int32_t l_2441 = 0xC351D044L;
                uint16_t l_2442 = 0x3B41L;
                --l_2433;
                (*l_2404) = l_2436;
                ++l_2442;
                (*l_2407) = 0xAEB2E235L;
            }
            else
            { /* block id: 1101 */
                int32_t *l_2445 = (void*)0;
                l_2445 = l_2445;
                (*l_2398) |= (((safe_sub_func_int64_t_s_s((safe_add_func_uint32_t_u_u((!(p_5 , 0x89EE913CB2BF3D79LL)), (*g_868))), ((*g_2050) = ((((*g_405) = (0L ^ (safe_rshift_func_int16_t_s_s(((safe_rshift_func_uint8_t_u_u(((safe_mul_func_int16_t_s_s((safe_sub_func_int64_t_s_s((((*g_388) |= (p_7 | l_2459)) | (*g_2050)), (*l_2393))), ((safe_add_func_int16_t_s_s((((void*)0 == l_2462) >= g_58[1][2]), 0UL)) < p_7))) , 0x0FL), (*l_2261))) && p_7), (*l_2407))))) && (*l_2375)) >= (*l_2400))))) , p_7) | l_2463);
            }
            if (((!(safe_unary_minus_func_uint16_t_u(((safe_lshift_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((((g_2471 = l_2470) == ((safe_mul_func_int8_t_s_s(((*g_2472) == (*g_2472)), (safe_div_func_int64_t_s_s(((!(safe_mod_func_int64_t_s_s(p_6, (*g_388)))) > ((l_2484 = (l_2483 = &g_1199)) != l_2485)), (safe_mod_func_uint64_t_u_u(l_2490, ((**g_2049) = (*g_2050)))))))) , &l_2286)) && (*l_2375)), p_6)) < g_867), (*l_2402))) , (***g_1681))))) , p_5))
            { /* block id: 1112 */
                int32_t *l_2492 = &g_2169;
                int8_t l_2497 = 1L;
                int32_t l_2498[2];
                uint32_t l_2503[5][8] = {{0x37A7E7F2L,0x2F15F6EFL,0xB798C47FL,1UL,0x77623B56L,0x77623B56L,1UL,0xB798C47FL},{0x37A7E7F2L,0x37A7E7F2L,4294967288UL,0x27664DF7L,1UL,0x79340A2AL,0x37A7E7F2L,1UL},{0xDE6FF466L,4294967288UL,0x661E0266L,0xDE6FF466L,0x77623B56L,0xDE6FF466L,0x661E0266L,4294967288UL},{4294967288UL,0x79340A2AL,0xA3EB68EFL,0UL,0x79340A2AL,0x661E0266L,0x77623B56L,0xA3EB68EFL},{0xB798C47FL,0x77623B56L,0x27664DF7L,4294967288UL,4294967288UL,0x27664DF7L,0x77623B56L,0xB798C47FL}};
                uint16_t ***l_2523 = &g_404;
                int64_t l_2528 = 1L;
                uint32_t l_2552[3][2];
                uint32_t *l_2553 = &g_431[2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_2498[i] = 1L;
                for (i = 0; i < 3; i++)
                {
                    for (j = 0; j < 2; j++)
                        l_2552[i][j] = 0UL;
                }
                if ((((*g_1257) = (~((0x7166874D8D94FF48LL || ((l_2492 == ((*g_2245) = func_52(l_2492, l_2394))) == ((safe_div_func_uint16_t_u_u(0x6430L, (safe_add_func_uint32_t_u_u((l_2498[1] |= ((g_2169 = (*l_2261)) , (l_2497 = ((&l_2367 != &g_1175) , 0UL)))), (*g_868))))) | (*g_1584)))) != p_7))) , 0xD754E12CL))
                { /* block id: 1118 */
                    uint32_t l_2500 = 0xC5688E5AL;
                    l_2500++;
                }
                else
                { /* block id: 1120 */
                    return l_2503[1][7];
                }
                for (g_375 = 11; (g_375 == (-3)); g_375--)
                { /* block id: 1125 */
                    (*l_2402) = (safe_lshift_func_int8_t_s_s(p_7, 2));
                }
                for (g_396 = 0; (g_396 != 9); g_396 = safe_add_func_int16_t_s_s(g_396, 6))
                { /* block id: 1130 */
                    uint16_t ***l_2541 = &g_404;
                    int32_t *l_2544 = &g_56[0];
                    uint16_t ** const **l_2554 = (void*)0;
                    uint16_t ** const **l_2555 = (void*)0;
                    uint16_t *l_2559 = (void*)0;
                    uint16_t ** const l_2558 = &l_2559;
                    uint16_t ** const *l_2557 = &l_2558;
                    uint16_t ** const **l_2556 = &l_2557;
                    uint16_t ****l_2560 = &l_2523;
                    int32_t l_2561[4][8][8] = {{{(-7L),8L,0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L,8L},{0x523CCB4FL,0x89A33DC6L,(-7L),0x523CCB4FL,0x719E3CEDL,0x719E3CEDL,0x523CCB4FL,(-7L)},{8L,8L,0x1DC3B8FEL,0L,0x523CCB4FL,0x1DC3B8FEL,0x523CCB4FL,0L},{(-7L),0L,(-7L),0x719E3CEDL,0L,0x89A33DC6L,0x89A33DC6L,0L},{0L,0x89A33DC6L,0x89A33DC6L,0L,0x719E3CEDL,(-7L),0L,(-7L)},{0L,0x523CCB4FL,0x1DC3B8FEL,0x523CCB4FL,0L,0x1DC3B8FEL,8L,8L},{(-7L),0x523CCB4FL,0x719E3CEDL,0x719E3CEDL,0x523CCB4FL,(-7L),0x89A33DC6L,0x523CCB4FL},{8L,0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L,8L,(-7L)}},{{0x523CCB4FL,0L,0x1DC3B8FEL,8L,8L,0x1DC3B8FEL,0L,0x523CCB4FL},{(-7L),8L,0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L,8L},{0x523CCB4FL,0x89A33DC6L,(-7L),0x523CCB4FL,0x719E3CEDL,0x719E3CEDL,0x523CCB4FL,(-7L)},{8L,8L,0x1DC3B8FEL,0L,0x523CCB4FL,0x1DC3B8FEL,0x523CCB4FL,0L},{(-7L),0L,(-7L),0x719E3CEDL,0L,0x89A33DC6L,0x89A33DC6L,0L},{0L,0x89A33DC6L,0x89A33DC6L,0L,0x719E3CEDL,(-7L),0L,(-7L)},{0L,0x523CCB4FL,0x1DC3B8FEL,0x523CCB4FL,0L,0x1DC3B8FEL,8L,8L},{(-7L),0x523CCB4FL,0x719E3CEDL,0x719E3CEDL,0x523CCB4FL,(-7L),0x89A33DC6L,0x523CCB4FL}},{{8L,0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L,8L,(-7L)},{0x523CCB4FL,0L,0x1DC3B8FEL,8L,8L,0x1DC3B8FEL,0L,0x523CCB4FL},{(-7L),8L,0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L,8L},{0x523CCB4FL,0x89A33DC6L,(-7L),0x523CCB4FL,0x719E3CEDL,0x719E3CEDL,0x523CCB4FL,(-7L)},{8L,8L,0x1DC3B8FEL,0L,0x523CCB4FL,0x1DC3B8FEL,0x719E3CEDL,0x89A33DC6L},{0xC0F86EE0L,0x89A33DC6L,0xC0F86EE0L,1L,0x89A33DC6L,0x1DC3B8FEL,0x1DC3B8FEL,0x89A33DC6L},{0x89A33DC6L,0x1DC3B8FEL,0x1DC3B8FEL,0x89A33DC6L,1L,0xC0F86EE0L,0x89A33DC6L,0xC0F86EE0L},{0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L,8L,(-7L),(-7L)}},{{0xC0F86EE0L,0x719E3CEDL,1L,1L,0x719E3CEDL,0xC0F86EE0L,0x1DC3B8FEL,0x719E3CEDL},{(-7L),0x1DC3B8FEL,1L,(-7L),1L,0x1DC3B8FEL,(-7L),0xC0F86EE0L},{0x719E3CEDL,0x89A33DC6L,8L,(-7L),(-7L),8L,0x89A33DC6L,0x719E3CEDL},{0xC0F86EE0L,(-7L),0x1DC3B8FEL,1L,(-7L),1L,0x1DC3B8FEL,(-7L)},{0x719E3CEDL,0x1DC3B8FEL,0xC0F86EE0L,0x719E3CEDL,1L,1L,0x719E3CEDL,0xC0F86EE0L},{(-7L),(-7L),8L,0x89A33DC6L,0x719E3CEDL,8L,0x719E3CEDL,0x89A33DC6L},{0xC0F86EE0L,0x89A33DC6L,0xC0F86EE0L,1L,0x89A33DC6L,0x1DC3B8FEL,0x1DC3B8FEL,0x89A33DC6L},{0x89A33DC6L,0x1DC3B8FEL,0x1DC3B8FEL,0x89A33DC6L,1L,0xC0F86EE0L,0x89A33DC6L,0xC0F86EE0L}}};
                    int i, j, k;
                    for (l_2499 = 7; (l_2499 >= 1); l_2499 -= 1)
                    { /* block id: 1133 */
                        int64_t *l_2514 = (void*)0;
                        int64_t *l_2515 = &g_184;
                        uint16_t ****l_2542 = &l_2523;
                        int i;
                        g_56[(l_2499 + 1)] = ((*l_2261) = (((safe_mul_func_uint16_t_u_u((g_56[(l_2499 + 1)] > g_56[l_2499]), 0x7FDAL)) != (safe_div_func_int32_t_s_s(((**g_2049) ^ (((*l_2515) = 1L) || (safe_sub_func_int16_t_s_s((+(safe_lshift_func_int16_t_s_u((((safe_lshift_func_int8_t_s_s((g_56[(l_2499 + 1)] & ((void*)0 != l_2523)), g_1515)) , p_6) <= 0xE6L), 1))), l_2503[1][2])))), (*l_2398)))) || p_5));
                        (*l_2405) = ((*l_2397) = ((*l_2404) = (((safe_rshift_func_int8_t_s_s((safe_rshift_func_int8_t_s_u(p_6, l_2528)), 2)) < ((safe_div_func_uint64_t_u_u(((*g_388) = 9UL), (safe_sub_func_int8_t_s_s((safe_mul_func_uint16_t_u_u(((((safe_div_func_int16_t_s_s((&g_2246[6][5][4] != &g_2246[2][5][1]), (safe_mul_func_uint8_t_u_u(((*l_2330)--), (((*g_1680) = (*g_1680)) != ((*l_2542) = l_2541)))))) , (l_2498[0] &= (*g_885))) > 65529UL) , l_2543), (*g_1584))), g_896)))) >= p_6)) | (-10L))));
                        l_2544 = (*g_1933);
                        if (l_2498[1])
                            break;
                    }
                    (*l_2375) |= (safe_mul_func_uint8_t_u_u((safe_lshift_func_uint16_t_u_s(l_2549, 4)), (safe_div_func_uint8_t_u_u((p_5 , ((l_2552[1][0] || (l_2553 == l_2375)) | (((*l_2556) = (*g_1680)) != ((*l_2560) = (*g_1680))))), (*l_2393)))));
                    (*g_1933) = &l_2498[0];
                    ++g_2562;
                }
            }
            else
            { /* block id: 1154 */
                int16_t ** const l_2574 = (void*)0;
                int16_t * const **** const **l_2581[3][9] = {{&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576},{&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576},{&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576,&l_2576}};
                int32_t l_2588[5][5][7] = {{{0x8FDD9DB4L,8L,0x4EC57748L,0x9A887296L,0xE8748BCDL,0xAD7B6858L,0x2E339245L},{(-5L),0x83856E52L,0xCAD4268EL,1L,0L,0xCAD4268EL,0x9A1FC523L},{0L,8L,1L,0x90735403L,0xE8748BCDL,0x1685A0F5L,0x5E15D290L},{(-5L),0x9A1FC523L,1L,1L,1L,1L,0x9A1FC523L},{0x8FDD9DB4L,0xABC96BD9L,1L,0x9A887296L,0L,0x1685A0F5L,0x2E339245L}},{{0L,0x9A1FC523L,0xCAD4268EL,0L,1L,0xCAD4268EL,0x83856E52L},{0x8FDD9DB4L,8L,0x4EC57748L,0x9A887296L,0xE8748BCDL,0xAD7B6858L,0x2E339245L},{(-5L),0x83856E52L,0xCAD4268EL,1L,0L,0xCAD4268EL,0x9A1FC523L},{0L,8L,1L,0x90735403L,0xE8748BCDL,0x1685A0F5L,0x5E15D290L},{(-5L),0x9A1FC523L,1L,1L,1L,1L,0x9A1FC523L}},{{0x8FDD9DB4L,0xABC96BD9L,1L,0x9A887296L,0x69292E41L,0x96D1D10DL,0x825D9836L},{0xFB8773EDL,0xDAA7BD7EL,0x8BB68943L,0L,0L,0x8BB68943L,0x22E18F58L},{1L,1L,0xE8748BCDL,0xD0FB91B1L,0L,0x9A887296L,0x825D9836L},{3L,0x22E18F58L,0x8BB68943L,0L,0L,0x8BB68943L,0xDAA7BD7EL},{0x9B3D9607L,1L,5L,0x8FDA9C6AL,0L,0x96D1D10DL,0L}},{{3L,0xDAA7BD7EL,1L,0L,0L,1L,0xDAA7BD7EL},{1L,0xC3E2CE9BL,5L,0xD0FB91B1L,0x69292E41L,0x96D1D10DL,0x825D9836L},{0xFB8773EDL,0xDAA7BD7EL,0x8BB68943L,0L,0L,0x8BB68943L,0x22E18F58L},{1L,1L,0xE8748BCDL,0xD0FB91B1L,0L,0x9A887296L,0x825D9836L},{3L,0x22E18F58L,0x8BB68943L,0L,0L,0x8BB68943L,0xDAA7BD7EL}},{{0x9B3D9607L,1L,5L,0x8FDA9C6AL,0L,0x96D1D10DL,0L},{3L,0xDAA7BD7EL,1L,0L,0L,1L,0xDAA7BD7EL},{1L,0xC3E2CE9BL,5L,0xD0FB91B1L,0x69292E41L,0x96D1D10DL,0x825D9836L},{0xFB8773EDL,0xDAA7BD7EL,0x8BB68943L,0L,0L,0x8BB68943L,0x22E18F58L},{1L,1L,0xE8748BCDL,0xD0FB91B1L,0L,0x9A887296L,0x825D9836L}}};
                const uint32_t *** const l_2628 = &g_983;
                int i, j, k;
                for (p_6 = (-3); (p_6 > 15); p_6 = safe_add_func_uint32_t_u_u(p_6, 2))
                { /* block id: 1157 */
                    (*l_2261) ^= p_5;
                }
                (*l_2405) |= ((safe_lshift_func_int16_t_s_s(((((((safe_div_func_uint16_t_u_u((safe_sub_func_int16_t_s_s(((**g_1201) = (l_2573 = (*l_2375))), (l_2574 == ((g_2575 != &l_2484) , ((((((g_2582 = l_2576) != &l_2484) == ((safe_mul_func_int16_t_s_s(((*l_2397) ^= (p_5 || 0x701E8A4B41EB152DLL)), ((((1UL ^ 0x5E8067396CAC7BB5LL) , (void*)0) != (void*)0) , l_2588[3][2][3]))) < 0xE0469241ECF1D65ALL)) >= (*g_868)) > p_5) , l_2589))))), l_2590)) | 0x19B3D731L) == l_2588[1][2][3]) != (*l_2261)) , &g_399) == &g_463), 0)) , p_7);
                if (((safe_rshift_func_uint16_t_u_u(((*g_1584) &= (p_5 && ((*g_868) , (p_7 && (5UL > (((1UL | ((0x490FE1303015F5AFLL == ((safe_rshift_func_int16_t_s_u(l_2588[0][3][5], 6)) ^ ((((safe_div_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u(((safe_lshift_func_uint8_t_u_s((+(((((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_uint16_t_u_u(((safe_add_func_int64_t_s_s(l_2588[3][2][3], ((0xA9A7245E74854B9DLL && (*g_388)) != (*g_405)))) > (*g_2050)), 7)) & 1UL), 3)) > l_2608[8][1][1]) , 4UL) < p_5) <= 0x8BD182D7L)), 0)) & 0L), 0xBFBEL)), p_6)) || 0x78BDE0D468A62B92LL) , 0x867A1ABEL) & p_5))) >= 1L)) >= (**g_387)) & p_7)))))), 11)) , 0x88EE34ADL))
                { /* block id: 1166 */
                    uint64_t l_2629[8][6][5] = {{{18446744073709551615UL,0xFE386287025BAF71LL,0x04759D44FE45244ALL,18446744073709551615UL,7UL},{18446744073709551615UL,0x7FAB5EF2E33E00F8LL,0x4E75E4C8BF5B44C7LL,0x7FAB5EF2E33E00F8LL,18446744073709551615UL},{0xFE386287025BAF71LL,0xC163344D988A49D5LL,0UL,18446744073709551611UL,0x37B19FC1093485B0LL},{0x691861CFDD80B240LL,0xDC2FBB3DB7B4E785LL,0xFE386287025BAF71LL,0x10EE9FE0C1EAA6C8LL,0xDC2FBB3DB7B4E785LL},{0xC17C14EB8A39D3BCLL,0x7FAB5EF2E33E00F8LL,1UL,0xC163344D988A49D5LL,0x37B19FC1093485B0LL},{0x04759D44FE45244ALL,0x10EE9FE0C1EAA6C8LL,18446744073709551611UL,0UL,18446744073709551615UL}},{{0x37B19FC1093485B0LL,0UL,0xFE386287025BAF71LL,0xC17C14EB8A39D3BCLL,7UL},{0x7FAB5EF2E33E00F8LL,18446744073709551609UL,0xE19483CD63475BD0LL,0xC17C14EB8A39D3BCLL,0xC17C14EB8A39D3BCLL},{2UL,18446744073709551611UL,2UL,0UL,18446744073709551615UL},{0x691861CFDD80B240LL,7UL,0x04759D44FE45244ALL,0xC163344D988A49D5LL,18446744073709551611UL},{0x10EE9FE0C1EAA6C8LL,0x37B19FC1093485B0LL,0x600EC429C542693FLL,0x10EE9FE0C1EAA6C8LL,0xC17C14EB8A39D3BCLL},{18446744073709551611UL,0x10EE9FE0C1EAA6C8LL,0x04759D44FE45244ALL,18446744073709551611UL,18446744073709551609UL}},{{18446744073709551615UL,0x04759D44FE45244ALL,2UL,0x7FAB5EF2E33E00F8LL,18446744073709551611UL},{0x10EE9FE0C1EAA6C8LL,0xC163344D988A49D5LL,0xE19483CD63475BD0LL,18446744073709551615UL,0x37B19FC1093485B0LL},{0UL,0xC163344D988A49D5LL,0xFE386287025BAF71LL,0xFE386287025BAF71LL,0xC163344D988A49D5LL},{0xC17C14EB8A39D3BCLL,0x04759D44FE45244ALL,18446744073709551611UL,0xC163344D988A49D5LL,0UL},{0x7FAB5EF2E33E00F8LL,0x10EE9FE0C1EAA6C8LL,1UL,0x1DA92B66B907F171LL,18446744073709551615UL},{0UL,0x37B19FC1093485B0LL,0xFE386287025BAF71LL,2UL,18446744073709551609UL}},{{0xA3B0C5488CFB3292LL,1UL,0x4E75E4C8BF5B44C7LL,0xDC2FBB3DB7B4E785LL,2UL},{0xDC2FBB3DB7B4E785LL,18446744073709551611UL,0xC163344D988A49D5LL,0x04759D44FE45244ALL,7UL},{0x4E75E4C8BF5B44C7LL,18446744073709551611UL,0UL,0x9901C6DF20487E2ALL,7UL},{0UL,4UL,4UL,0UL,2UL},{0UL,0UL,18446744073709551611UL,7UL,18446744073709551611UL},{0xE19483CD63475BD0LL,0xA3B0C5488CFB3292LL,2UL,0UL,0UL}},{{0UL,0x9901C6DF20487E2ALL,0x4E75E4C8BF5B44C7LL,7UL,4UL},{2UL,0xFE386287025BAF71LL,0x37B19FC1093485B0LL,0UL,0xFE386287025BAF71LL},{2UL,0xA3B0C5488CFB3292LL,0xE19483CD63475BD0LL,0x9901C6DF20487E2ALL,0UL},{0xA3B0C5488CFB3292LL,0x600EC429C542693FLL,0xE19483CD63475BD0LL,0x04759D44FE45244ALL,0xE19483CD63475BD0LL},{0UL,0UL,0x37B19FC1093485B0LL,0xDC2FBB3DB7B4E785LL,18446744073709551611UL},{0UL,18446744073709551611UL,0x4E75E4C8BF5B44C7LL,2UL,0xDC2FBB3DB7B4E785LL}},{{0xDC2FBB3DB7B4E785LL,18446744073709551614UL,2UL,0x04759D44FE45244ALL,0UL},{2UL,18446744073709551611UL,18446744073709551611UL,0xFE386287025BAF71LL,7UL},{0x600EC429C542693FLL,0UL,4UL,0x600EC429C542693FLL,0xDC2FBB3DB7B4E785LL},{0UL,0x600EC429C542693FLL,0UL,7UL,1UL},{0UL,0xA3B0C5488CFB3292LL,0xC163344D988A49D5LL,0xA3B0C5488CFB3292LL,0UL},{0x600EC429C542693FLL,0xFE386287025BAF71LL,0x4E75E4C8BF5B44C7LL,0UL,0UL}},{{2UL,0x9901C6DF20487E2ALL,0x600EC429C542693FLL,0UL,0x9901C6DF20487E2ALL},{0xDC2FBB3DB7B4E785LL,0xA3B0C5488CFB3292LL,0x691861CFDD80B240LL,0xFE386287025BAF71LL,0UL},{0UL,0UL,0xE19483CD63475BD0LL,0xD473797FC9739A8DLL,0UL},{0UL,4UL,0x600EC429C542693FLL,0xDC2FBB3DB7B4E785LL,1UL},{0xA3B0C5488CFB3292LL,18446744073709551611UL,0xC17C14EB8A39D3BCLL,0xDC2FBB3DB7B4E785LL,0xDC2FBB3DB7B4E785LL},{2UL,18446744073709551611UL,2UL,0xD473797FC9739A8DLL,7UL}},{{2UL,1UL,0UL,0xFE386287025BAF71LL,0UL},{0UL,0UL,0x1DA92B66B907F171LL,0UL,0xDC2FBB3DB7B4E785LL},{0xE19483CD63475BD0LL,0UL,0UL,0UL,18446744073709551611UL},{0UL,0UL,2UL,0xA3B0C5488CFB3292LL,0xE19483CD63475BD0LL},{0UL,0xFE386287025BAF71LL,0xC17C14EB8A39D3BCLL,7UL,0UL},{0x4E75E4C8BF5B44C7LL,0xFE386287025BAF71LL,0x600EC429C542693FLL,0x600EC429C542693FLL,0xFE386287025BAF71LL}}};
                    int32_t *l_2631[5];
                    int i, j, k;
                    for (i = 0; i < 5; i++)
                        l_2631[i] = &l_2372[1];
                    if (g_60)
                        goto lbl_2609;
                    if (((*l_2399) = (&p_6 != &p_6)))
                    { /* block id: 1169 */
                        int32_t *l_2610 = &g_46;
                        int64_t *l_2630 = &g_1862[0][0][1];
                        l_2610 = ((*g_1933) = (void*)0);
                        l_2631[3] = (((safe_add_func_int64_t_s_s((-8L), (**g_2049))) == (l_2588[1][0][2] && ((((safe_lshift_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u((((safe_add_func_uint32_t_u_u((((*l_2630) = (((g_2169 = l_2619[4]) , (safe_sub_func_uint32_t_u_u((safe_rshift_func_int8_t_s_u((0x002BL <= (safe_mod_func_int16_t_s_s((((((*l_2330) = 0x6DL) ^ (safe_lshift_func_uint8_t_u_u(((((p_7 , ((void*)0 == l_2628)) , &p_6) != (void*)0) && p_5), 2))) < p_7) <= l_2588[3][2][3]), 5UL))), 7)), 8UL))) == l_2629[6][2][4])) > p_6), p_5)) == (*g_911)) <= p_6), 0x52DBB7D8D51A2A05LL)) != p_5), 6)) != (**g_387)) <= 255UL) > (-10L)))) , &l_2588[0][4][3]);
                    }
                    else
                    { /* block id: 1176 */
                        int32_t l_2632 = 1L;
                        (*l_2406) = (l_2632 = p_6);
                        (*l_2399) ^= (safe_add_func_int32_t_s_s((p_7 , 0xFD04F174L), (((&g_2583 == (((safe_lshift_func_int8_t_s_s(((((0xC4L != ((**g_1681) != (void*)0)) >= (((((*l_2261) < (safe_lshift_func_int8_t_s_u((0L | ((*g_1584) &= (++(*g_405)))), 3))) , (((((*g_911) = (*g_984)) == 0x4874BAACL) , l_2641[5][3][2]) == (void*)0)) , 65535UL) || 4UL)) , 0UL) >= g_2087), p_6)) != l_2588[3][2][3]) , (void*)0)) >= 249UL) , (*g_911))));
                    }
                }
                else
                { /* block id: 1184 */
                    uint64_t l_2643 = 4UL;
                    if (p_6)
                    { /* block id: 1185 */
                        return g_2183[0][2];
                    }
                    else
                    { /* block id: 1187 */
                        ++l_2643;
                    }
                    return (*l_2407);
                }
            }
        }
        else
        { /* block id: 1193 */
            uint64_t l_2672 = 18446744073709551609UL;
            uint16_t * const *l_2688 = (void*)0;
            int32_t l_2690 = 0x6E300DB4L;
            for (g_2169 = 0; (g_2169 <= 2); g_2169 += 1)
            { /* block id: 1196 */
                uint16_t *** const *l_2652[5][3] = {{&g_1681,&g_1681,&g_1681},{&g_1681,&g_1681,&g_1681},{&g_1681,&g_1681,&g_1681},{&g_1681,&g_1681,&g_1681},{&g_1681,&g_1681,&g_1681}};
                uint16_t *** const **l_2653 = (void*)0;
                uint16_t *** const **l_2654 = (void*)0;
                uint16_t *** const **l_2655 = &l_2652[0][1];
                int64_t *l_2657 = &l_2549;
                int64_t **l_2658 = (void*)0;
                int64_t **l_2659 = &l_2657;
                int32_t ****l_2662 = &g_754;
                int i, j;
                if ((g_431[g_2169] < (safe_rshift_func_int16_t_s_u((safe_sub_func_uint8_t_u_u(p_6, ((safe_add_func_int64_t_s_s((((*l_2655) = l_2652[0][2]) != (void*)0), ((((((!((*g_2049) != ((*l_2659) = l_2657))) != (0xCE24L & ((*l_2375) = (p_7 <= (((((safe_add_func_int32_t_s_s((g_56[(g_2169 + 1)] = (((l_2662 == (**l_2462)) && g_1253) ^ p_7)), p_6)) ^ p_5) | 0xBC83L) , g_1862[0][1][6]) & g_518))))) <= (-9L)) , g_119) && 0x16L) < (****g_1680)))) < g_494[0][5]))), p_6))))
                { /* block id: 1201 */
                    uint16_t *l_2691 = &g_378;
                    int32_t l_2692 = (-5L);
                    for (l_2279 = 1; (l_2279 >= 0); l_2279 -= 1)
                    { /* block id: 1204 */
                        int32_t *l_2663[5];
                        int i;
                        for (i = 0; i < 5; i++)
                            l_2663[i] = &l_2372[0];
                        l_2663[1] = &g_1999[l_2279];
                    }
                }
                else
                { /* block id: 1212 */
                    uint64_t l_2722 = 0xAA5F1951A56C73FFLL;
                    for (g_2087 = 0; (g_2087 <= 2); g_2087 += 1)
                    { /* block id: 1215 */
                        int32_t *l_2723 = &l_2278;
                        int i;
                        l_2372[g_2087] = (safe_rshift_func_int8_t_s_s((safe_add_func_uint64_t_u_u((safe_add_func_uint32_t_u_u(((safe_add_func_uint32_t_u_u(((safe_sub_func_int32_t_s_s((safe_rshift_func_uint8_t_u_u(l_2372[g_2169], 0)), l_2372[g_2169])) == (p_6 >= ((((p_6 || ((safe_mul_func_uint16_t_u_u(l_2372[g_2169], (((((**g_2049) = 0xBE4FE29F6B7678D8LL) & (safe_unary_minus_func_uint8_t_u((((**g_387) = (safe_div_func_int16_t_s_s(((safe_add_func_uint16_t_u_u((safe_div_func_int64_t_s_s((l_2372[g_2169] , p_6), (((((safe_lshift_func_uint8_t_u_s(l_2672, g_453)) < 0x0E7A6A168FF8E282LL) , (*g_387)) != (*g_387)) , l_2690))), 0x102EL)) < l_2722), (****g_1680)))) , 249UL)))) <= l_2690) > 0xF556C2B7030C7EEDLL))) & (*g_55))) != (*g_405)) , (void*)0) == (void*)0))), (**g_1933))) , p_6), p_6)), (-1L))), 3));
                        l_2723 = ((*g_1933) = &l_2372[g_2169]);
                    }
                }
                (*g_1933) = &l_2271;
            }
            for (g_46 = 0; (g_46 != 5); g_46++)
            { /* block id: 1227 */
                uint64_t l_2728 = 0UL;
                for (g_267 = 0; (g_267 > 47); g_267 = safe_add_func_uint16_t_u_u(g_267, 7))
                { /* block id: 1230 */
                    uint32_t l_2731 = 0x22598531L;
                    if (p_6)
                    { /* block id: 1231 */
                        (*g_1933) = (*g_1933);
                        ++l_2728;
                        (*g_1933) = &l_2690;
                    }
                    else
                    { /* block id: 1235 */
                        if (l_2731)
                            break;
                    }
                }
            }
        }
        for (g_2169 = 0; (g_2169 < 9); g_2169 = safe_add_func_int64_t_s_s(g_2169, 1))
        { /* block id: 1243 */
            (*l_2261) = ((*g_55) != p_5);
        }
    }
    return p_5;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_9(uint32_t  p_10, int16_t  p_11, const int64_t  p_12, int8_t  p_13, int32_t  p_14)
{ /* block id: 1054 */
    int32_t *l_2256 = &g_1999[1];
    l_2256 = l_2256;
    return p_14;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int8_t  func_24(int32_t  p_25, int8_t  p_26)
{ /* block id: 1048 */
    int32_t *l_2249 = &g_1612;
    int32_t *l_2250 = (void*)0;
    l_2250 = l_2249;
    return p_25;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_27(uint64_t  p_28, int32_t  p_29)
{ /* block id: 1 */
    int32_t l_30[9];
    int32_t *l_869[3];
    uint32_t ** const *l_1808[9] = {&g_910[5][0][1],&g_910[6][1][1],&g_910[6][1][1],&g_910[5][0][1],&g_910[6][1][1],&g_910[6][1][1],&g_910[5][0][1],&g_910[6][1][1],&g_910[6][1][1]};
    const int64_t l_1814 = (-9L);
    uint16_t *l_1852[1];
    int32_t l_1891 = 0xD81193ECL;
    uint32_t l_1904[1][2][1];
    int8_t *l_1939 = &g_453;
    int8_t l_1949 = 1L;
    int32_t **l_2076 = &g_868;
    uint64_t l_2098 = 0x8FD1CFDDB02322ABLL;
    int64_t ** const l_2139 = &g_2050;
    int32_t l_2164[6];
    uint64_t * const **l_2205 = (void*)0;
    int i, j, k;
    for (i = 0; i < 9; i++)
        l_30[i] = 0x2B622B60L;
    for (i = 0; i < 3; i++)
        l_869[i] = &g_870[6][1];
    for (i = 0; i < 1; i++)
        l_1852[i] = (void*)0;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 1; k++)
                l_1904[i][j][k] = 18446744073709551608UL;
        }
    }
    for (i = 0; i < 6; i++)
        l_2164[i] = (-9L);
    for (p_28 = 1; (p_28 <= 8); p_28 += 1)
    { /* block id: 4 */
        int32_t *l_45[7][4][9] = {{{&g_46,&g_46,(void*)0,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,(void*)0,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{(void*)0,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,(void*)0,&g_46}},{{&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,(void*)0},{&g_46,&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,(void*)0}},{{(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,(void*)0,(void*)0},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,(void*)0,(void*)0,(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,(void*)0,(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46}},{{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,(void*)0}},{{&g_46,&g_46,&g_46,&g_46,(void*)0,(void*)0,&g_46,&g_46,&g_46},{(void*)0,&g_46,&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46},{(void*)0,(void*)0,&g_46,(void*)0,&g_46,&g_46,&g_46,&g_46,(void*)0},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46}},{{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,(void*)0,&g_46,(void*)0,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46,(void*)0},{(void*)0,&g_46,&g_46,&g_46,&g_46,&g_46,(void*)0,&g_46,&g_46}},{{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,(void*)0},{&g_46,(void*)0,&g_46,&g_46,&g_46,&g_46,(void*)0,&g_46,(void*)0},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46},{&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,&g_46,(void*)0}}};
        uint32_t *l_1235 = &g_344;
        uint8_t l_1783 = 0x86L;
        const int16_t *****l_1829 = (void*)0;
        int8_t l_1861 = 0L;
        int64_t l_1903 = (-8L);
        int64_t ***l_1954 = &g_1175;
        uint16_t **l_1974 = (void*)0;
        int8_t l_2009 = 0x79L;
        uint16_t l_2027 = 0UL;
        const int16_t l_2063[3][4] = {{0x002AL,0x002AL,0x002AL,0x002AL},{0x002AL,0x002AL,0x002AL,0x002AL},{0x002AL,0x002AL,0x002AL,0x002AL}};
        int8_t l_2129 = 0xA7L;
        int32_t ****l_2172[9][2] = {{&g_754,&g_754},{(void*)0,(void*)0},{(void*)0,(void*)0},{&g_754,&g_754},{&g_754,(void*)0},{(void*)0,(void*)0},{(void*)0,&g_754},{&g_754,&g_754},{(void*)0,(void*)0}};
        int i, j, k;
    }
    return p_28;
}


/* ------------------------------------------ */
/* 
 * reads : g_1517 g_186 g_865 g_1680 g_1681 g_431 g_405 g_60 g_404 g_1612
 * writes: g_1517 g_186 g_865 g_184 g_60
 */
static int8_t  func_35(const int32_t  p_36, uint8_t  p_37, int8_t  p_38)
{ /* block id: 803 */
    const int32_t *l_1744 = &g_1164[1][1];
    int32_t *l_1745[5];
    int32_t l_1765 = 0x4E1ED704L;
    int i;
    for (i = 0; i < 5; i++)
        l_1745[i] = &g_46;
    l_1744 = (l_1745[4] = l_1745[3]);
    l_1744 = &p_36;
    for (g_1517 = 0; (g_1517 > (-13)); g_1517--)
    { /* block id: 809 */
        uint32_t l_1760 = 0UL;
        uint64_t l_1775 = 4UL;
        int32_t l_1777 = 0L;
        int32_t l_1778 = 0L;
        int32_t l_1779 = 0xE7BDD156L;
        int32_t l_1782 = 0x22BD6966L;
        for (g_186 = 0; (g_186 > (-6)); g_186--)
        { /* block id: 812 */
            const uint8_t *l_1766 = (void*)0;
            int32_t l_1771[5][2][1];
            int32_t l_1772 = 0xDA132A0AL;
            int32_t l_1773 = 0x66FBFE2FL;
            uint64_t **l_1781 = &g_388;
            uint64_t ***l_1780 = &l_1781;
            int i, j, k;
            for (i = 0; i < 5; i++)
            {
                for (j = 0; j < 2; j++)
                {
                    for (k = 0; k < 1; k++)
                        l_1771[i][j][k] = (-10L);
                }
            }
            for (g_865 = 18; (g_865 == 30); ++g_865)
            { /* block id: 815 */
                int64_t *l_1774 = &g_184;
                int32_t l_1776 = 1L;
                l_1782 ^= (((((safe_lshift_func_uint16_t_u_u((safe_sub_func_uint32_t_u_u((safe_lshift_func_uint16_t_u_u(((****g_1680) = (((((safe_mul_func_int8_t_s_s(l_1760, (safe_lshift_func_int8_t_s_s(((((safe_mod_func_uint16_t_u_u(((void*)0 != (*g_1680)), l_1765)) || l_1760) , (((l_1766 == (void*)0) >= ((safe_lshift_func_int16_t_s_u(0L, 9)) < (l_1778 = (l_1777 = (l_1776 = ((((((*l_1774) = (safe_mod_func_uint8_t_u_u(((l_1772 = ((((l_1771[3][1][0] = p_37) | l_1772) ^ (-1L)) == 0L)) ^ p_36), l_1773))) , 0x2BFFF78DAE3A0F87LL) < p_36) , p_37) >= l_1775)))))) >= l_1775)) < g_431[2]), 4)))) >= 0x677E3C651296DB00LL) , p_36) == l_1779) , (*g_405))), 6)), 4294967286UL)), 12)) , p_38) , l_1780) != (void*)0) ^ 0xBBBBF873L);
            }
        }
        l_1782 |= (*l_1744);
    }
    return g_1612;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static const int32_t  func_39(uint32_t  p_40, uint64_t  p_41, int8_t  p_42)
{ /* block id: 800 */
    const int32_t *l_1740 = (void*)0;
    int32_t *l_1741 = &g_57;
    int32_t **l_1742[8];
    int32_t *l_1743[1];
    int i;
    for (i = 0; i < 8; i++)
        l_1742[i] = (void*)0;
    for (i = 0; i < 1; i++)
        l_1743[i] = &g_58[3][6];
    l_1743[0] = l_1741;
    return p_41;
}


/* ------------------------------------------ */
/* 
 * reads : g_870 g_267 g_983 g_984 g_431 g_404 g_405 g_184 g_1253 g_1082 g_1257 g_1201 g_565 g_388 g_119 g_60 g_55 g_56 g_761 g_387 g_911 g_378 g_399 g_639 g_640 g_396 g_518 g_1515 g_1517 g_868 g_277 g_896 g_57 g_58 g_494 g_441 g_1175 g_1680 g_1164 g_1682 g_1584 g_1703 g_1681 g_865
 * writes: g_870 g_60 g_184 g_1253 g_1082 g_896 g_375 g_1285 g_518 g_405 g_119 g_431 g_344 g_865 g_396 g_277 g_868 g_56 g_57 g_58 g_87 g_55 g_378 g_399
 */
static uint16_t  func_43(uint32_t  p_44)
{ /* block id: 600 */
    int32_t *l_1236 = &g_870[6][1];
    int32_t l_1248 = 3L;
    const int64_t *l_1288 = &g_184;
    int32_t l_1298 = 0x25918A2BL;
    int32_t l_1309 = 0x68DB7976L;
    int32_t l_1314 = 7L;
    int32_t l_1321[3];
    uint64_t l_1323 = 0x5A0680ECB2546B71LL;
    uint16_t *l_1330 = &g_60;
    uint16_t l_1431 = 0xE210L;
    int8_t *l_1485 = (void*)0;
    uint16_t *l_1611 = &l_1431;
    int16_t ******l_1667 = &g_1603;
    const int32_t *l_1713 = &g_1164[3][6];
    int32_t *l_1716 = &l_1309;
    int32_t l_1730 = 0xCEF938F1L;
    int32_t *l_1731 = (void*)0;
    int32_t *l_1732 = &g_56[6];
    int32_t *l_1733 = &l_1314;
    int32_t *l_1734 = &l_1321[1];
    int32_t *l_1735 = &g_1612;
    int32_t *l_1736[10] = {&l_1309,&l_1730,&g_870[6][0],&g_870[6][0],&l_1730,&l_1309,&l_1730,&g_870[6][0],&g_870[6][0],&l_1730};
    uint64_t l_1737 = 18446744073709551613UL;
    int i;
    for (i = 0; i < 3; i++)
        l_1321[i] = 0x6985ECD6L;
lbl_1502:
    (*l_1236) |= 0x4E9A56FEL;
    if ((*l_1236))
    { /* block id: 602 */
        uint16_t l_1243 = 0x301BL;
        uint16_t l_1249 = 0xCAD7L;
        int64_t *l_1250 = &g_184;
        int64_t *l_1251 = (void*)0;
        int32_t l_1252 = (-1L);
        int16_t l_1312 = 0x884CL;
        int32_t l_1337 = (-6L);
        int16_t ****l_1354 = (void*)0;
        int8_t *l_1484 = (void*)0;
        int8_t l_1501 = 4L;
        int32_t l_1516[4][8][6] = {{{(-2L),0x8F430659L,0L,0xCF83AE28L,4L,0xA46544ACL},{1L,(-2L),(-1L),0xA46544ACL,0xE4B5BDF1L,1L},{0x8191FA1BL,4L,6L,0x80C815CBL,4L,0x8A74DA93L},{0x409DD60EL,2L,0x2DD4B59EL,(-8L),(-8L),0x2DD4B59EL},{0x5B6ABBCFL,0x5B6ABBCFL,(-8L),0x0A787918L,0L,0xE4B5BDF1L},{0x0156577AL,4L,0x90E0ED56L,(-1L),0x143A9814L,(-8L)},{1L,0x0156577AL,0x90E0ED56L,4L,0x5B6ABBCFL,0xE4B5BDF1L},{0x80C815CBL,4L,(-8L),0x8A74DA93L,4L,0x2DD4B59EL}},{{0x8A74DA93L,4L,0x2DD4B59EL,0xA46544ACL,(-2L),0x8A74DA93L},{0xFAB38F94L,1L,6L,0x0A787918L,6L,1L},{6L,2L,(-1L),0L,0x143A9814L,0xA46544ACL},{0x5B6ABBCFL,6L,0L,(-2L),0x59135CCFL,0xE4B5BDF1L},{0x8191FA1BL,6L,0x0156577AL,0x2DD4B59EL,0x143A9814L,0x787BF8BDL},{0x8A74DA93L,2L,0x8191FA1BL,4L,6L,0x80C815CBL},{0xE4B5BDF1L,1L,(-8L),0xCF83AE28L,(-2L),1L},{4L,4L,(-1L),(-1L),4L,4L}},{{0xFAB38F94L,4L,0xA644976DL,(-2L),0x5B6ABBCFL,1L},{0x409DD60EL,0x0156577AL,0xA46544ACL,0x787BF8BDL,0x5B6ABBCFL,0L},{6L,0x59135CCFL,0xA644976DL,0x2DD4B59EL,0x8A74DA93L,(-1L)},{(-2L),0L,0x80C815CBL,0x143A9814L,0x409DD60EL,6L},{0x8191FA1BL,0xE4B5BDF1L,4L,2L,0x59135CCFL,0x2DD4B59EL},{0xA46544ACL,0x8191FA1BL,4L,0x8191FA1BL,0xA46544ACL,(-8L)},{0x90E0ED56L,0x2DD4B59EL,0x787BF8BDL,0L,1L,0x90E0ED56L},{0x0A787918L,0xFAB38F94L,6L,0x2DD4B59EL,0x3A987C0EL,0x90E0ED56L}},{{0L,(-2L),0x787BF8BDL,4L,0x409DD60EL,(-8L)},{0x3A987C0EL,0x59135CCFL,4L,8L,1L,0x2DD4B59EL},{(-2L),0x3A987C0EL,4L,0x787BF8BDL,6L,6L},{0x90E0ED56L,0x80C815CBL,0x80C815CBL,0x90E0ED56L,0x59135CCFL,(-1L)},{(-1L),0xFAB38F94L,0xA644976DL,0x8191FA1BL,0x2DD4B59EL,0L},{0x0156577AL,0xA46544ACL,0x787BF8BDL,0x143A9814L,0x2DD4B59EL,0x0156577AL},{0x80C815CBL,0xFAB38F94L,(-1L),8L,0x59135CCFL,0x8191FA1BL},{0L,0x80C815CBL,0x143A9814L,0x409DD60EL,6L,(-8L)}}};
        int32_t *l_1519[7][3];
        int32_t **l_1565[1];
        int32_t ***l_1564 = &l_1565[0];
        int32_t ******l_1640 = &g_1575[4];
        int i, j, k;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 3; j++)
                l_1519[i][j] = &g_56[8];
        }
        for (i = 0; i < 1; i++)
            l_1565[i] = &l_1519[5][2];
        if ((((g_1253 |= (l_1252 = ((*l_1250) |= ((safe_div_func_int8_t_s_s(((p_44 && (((**g_404) = (safe_mod_func_int32_t_s_s(((*l_1236) = ((((safe_add_func_uint8_t_u_u((g_267 , l_1243), (l_1243 > ((safe_rshift_func_int8_t_s_u(((((((((p_44 | ((safe_lshift_func_int8_t_s_s(l_1243, (*l_1236))) > ((-1L) <= (p_44 >= 0x7E4C18C3L)))) == (*l_1236)) , 0xC389L) && l_1248) > (**g_983)) || 9L) != 0xF5L) ^ 0L), 7)) <= p_44)))) != 0xBAL) > 0xBF31D1A3L) <= 0x06A0L)), l_1249))) , (-1L))) & p_44), p_44)) | p_44)))) >= l_1249) , l_1243))
        { /* block id: 608 */
            uint16_t *l_1263[2];
            uint16_t ***l_1270 = &g_404;
            int32_t l_1271 = 0xB70AA280L;
            int64_t * const l_1287 = &g_19;
            int32_t l_1313 = 0x3FC0774EL;
            int32_t l_1316 = 4L;
            int32_t l_1317 = (-7L);
            int32_t l_1320 = 0L;
            uint8_t l_1450 = 0UL;
            int64_t l_1483 = 0xE6753C12C769C261LL;
            int16_t ** const *l_1512 = (void*)0;
            uint32_t *l_1514 = &g_267;
            int16_t **l_1518 = &g_565[0];
            int16_t *****l_1600 = (void*)0;
            const uint64_t l_1629 = 0x6C4BEA02F589AF62LL;
            int32_t *l_1633 = &g_870[6][1];
            int i;
            for (i = 0; i < 2; i++)
                l_1263[i] = &g_87;
lbl_1655:
            for (p_44 = 0; (p_44 <= 1); p_44 += 1)
            { /* block id: 611 */
                int32_t l_1254 = 0x51F9549FL;
                int32_t l_1305 = 0xE0BD4D66L;
                int32_t l_1315 = 0L;
                for (g_1082 = 2; (g_1082 >= 0); g_1082 -= 1)
                { /* block id: 614 */
                    int i, j;
                    g_870[(g_1082 + 4)][p_44] &= l_1254;
                    for (g_896 = 0; (g_896 <= 2); g_896 += 1)
                    { /* block id: 618 */
                        if (p_44)
                            break;
                    }
                }
                l_1271 |= (((safe_lshift_func_uint16_t_u_s((g_1257 == ((((safe_sub_func_uint8_t_u_u((((safe_unary_minus_func_uint32_t_u((safe_lshift_func_uint8_t_u_u(((l_1263[0] != (void*)0) , (((*l_1250) = (safe_mod_func_uint16_t_u_u(l_1254, ((**g_1201) = (*l_1236))))) >= ((0x10132D90L & l_1254) != (((safe_add_func_uint8_t_u_u(((18446744073709551615UL <= (*g_388)) != 0x38C5L), 0UL)) , (void*)0) == l_1270)))), 1)))) | 65535UL) >= 1UL), p_44)) < (**g_404)) , 65529UL) , &g_267)), p_44)) ^ 0UL) ^ p_44);
                (*l_1236) = p_44;
                for (g_896 = 0; (g_896 <= 2); g_896 += 1)
                { /* block id: 628 */
                    int16_t **l_1284 = &g_565[0];
                    int16_t *** const l_1283[5] = {&l_1284,&l_1284,&l_1284,&l_1284,&l_1284};
                    int16_t *** const *l_1282[7];
                    int16_t *** const **l_1281 = &l_1282[2];
                    int32_t *l_1289 = &l_1254;
                    int32_t l_1318 = 6L;
                    int32_t l_1319 = 0x3FEB5BA2L;
                    int32_t l_1322 = 1L;
                    int i, j;
                    for (i = 0; i < 7; i++)
                        l_1282[i] = &l_1283[1];
                    for (l_1252 = 0; (l_1252 <= 2); l_1252 += 1)
                    { /* block id: 631 */
                        return p_44;
                    }
                    (*l_1289) |= (~(p_44 != ((safe_mod_func_int32_t_s_s((*g_55), (safe_lshift_func_uint16_t_u_u(((((l_1252 = p_44) && (safe_add_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u((p_44 != ((((*l_1281) = &g_1200[5][0][3]) != (((void*)0 == &g_896) , (g_1285 = &g_1200[5][0][3]))) < p_44)), p_44)), p_44))) , l_1287) != l_1288), (*l_1236))))) || p_44)));
                    if (l_1271)
                        break;
                    for (g_518 = 0; (g_518 <= 2); g_518 += 1)
                    { /* block id: 641 */
                        int16_t l_1290[8][1][9] = {{{0xA3F2L,(-5L),0x748AL,1L,7L,0x7F7FL,0xA3F2L,0x7F7FL,7L}},{{1L,1L,1L,1L,(-6L),4L,1L,4L,(-6L)}},{{0xA3F2L,(-5L),0x748AL,1L,7L,0x7F7FL,0xA3F2L,0x7F7FL,7L}},{{1L,1L,1L,1L,(-6L),4L,1L,4L,(-6L)}},{{0xA3F2L,(-5L),0x748AL,1L,7L,0x7F7FL,0xA3F2L,0x7F7FL,7L}},{{1L,1L,1L,1L,(-6L),4L,1L,4L,(-6L)}},{{0xA3F2L,(-5L),0x748AL,1L,7L,0x7F7FL,0xA3F2L,0x7F7FL,7L}},{{1L,1L,1L,1L,(-6L),4L,1L,4L,(-6L)}}};
                        int32_t *l_1291 = &g_870[0][0];
                        int32_t *l_1292 = (void*)0;
                        int32_t *l_1293 = &g_870[(g_896 + 1)][p_44];
                        int32_t *l_1294 = &g_56[6];
                        int32_t *l_1295 = &l_1271;
                        int32_t *l_1296 = (void*)0;
                        int32_t *l_1297 = &g_870[(g_896 + 1)][p_44];
                        int32_t *l_1299 = (void*)0;
                        int32_t *l_1300 = &g_57;
                        int32_t *l_1301 = (void*)0;
                        int32_t *l_1302 = (void*)0;
                        int32_t *l_1303 = &l_1298;
                        int32_t *l_1304 = &g_56[0];
                        int32_t *l_1306 = &g_56[0];
                        int32_t *l_1307 = &l_1254;
                        int32_t *l_1308 = &l_1254;
                        int32_t *l_1310 = (void*)0;
                        int32_t *l_1311[8][5] = {{&g_56[0],&g_870[(g_896 + 1)][p_44],&g_870[(g_896 + 1)][p_44],&g_56[0],&l_1254},{&g_870[6][1],&l_1271,&g_57,&l_1252,&l_1252},{&g_870[6][1],&g_56[0],&g_870[6][1],&l_1254,&g_56[0]},{&l_1252,(void*)0,&l_1271,&l_1252,&l_1271},{&g_870[6][1],&g_870[6][1],(void*)0,&g_56[0],(void*)0},{&l_1309,&g_870[6][1],&l_1271,&l_1252,&l_1271},{&l_1254,(void*)0,&g_56[4],&l_1254,(void*)0},{(void*)0,&l_1271,&l_1252,&l_1271,(void*)0}};
                        int i, j, k;
                        l_1290[0][0][6] = 2L;
                        if ((*l_1289))
                            continue;
                        l_1323++;
                    }
                }
            }
            if ((safe_add_func_int32_t_s_s((safe_div_func_int64_t_s_s(0xE45F581A1FF8DCB9LL, l_1312)), (((*g_404) = (*g_404)) == l_1330))))
            { /* block id: 649 */
                uint8_t l_1338 = 0x10L;
                uint32_t *l_1343 = &g_344;
                uint16_t * const l_1346 = &g_60;
                int32_t l_1391 = 0x3FFBF423L;
                uint64_t **l_1442[9][4][7] = {{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388}},{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388}},{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388}},{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388}},{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388}},{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388}},{{&g_388,&g_388,&g_388,&g_388,&g_388,&g_388,&g_388},{&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388,(void*)0},{&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388,(void*)0},{&g_388,(void*)0,(void*)0,&g_388,(void*)0,&g_388,(void*)0}},{{&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388,(void*)0},{&g_388,(void*)0,(void*)0,&g_388,(void*)0,&g_388,(void*)0},{&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388,(void*)0},{&g_388,(void*)0,(void*)0,&g_388,(void*)0,&g_388,(void*)0}},{{&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388,(void*)0},{&g_388,(void*)0,(void*)0,&g_388,(void*)0,&g_388,(void*)0},{&g_388,&g_388,&g_388,&g_388,(void*)0,&g_388,(void*)0},{&g_388,(void*)0,(void*)0,&g_388,(void*)0,&g_388,(void*)0}}};
                int32_t **l_1451 = &g_868;
                const int32_t ****l_1461 = &g_639;
                const int32_t *****l_1460 = &l_1461;
                uint16_t l_1486 = 0x0968L;
                int32_t l_1654 = (-7L);
                int i, j, k;
                if (((safe_mul_func_int8_t_s_s(g_761, (l_1316 , ((((safe_sub_func_uint16_t_u_u(((((*l_1343) = ((*g_911) &= ((0x9CL <= (safe_mul_func_uint16_t_u_u((((((**g_387) = (*g_388)) < (*l_1236)) | l_1337) | (*g_405)), (l_1252 | (++l_1338))))) || (safe_mul_func_int16_t_s_s(0x9E65L, l_1338))))) == l_1312) < l_1252), 0UL)) | 4294967295UL) | p_44) <= p_44)))) == (*g_405)))
                { /* block id: 654 */
                    for (g_865 = (-7); (g_865 != 28); ++g_865)
                    { /* block id: 657 */
                        if ((*g_55))
                            break;
                        return p_44;
                    }
                    (*l_1236) &= l_1338;
                }
                else
                { /* block id: 662 */
                    int32_t l_1360 = (-1L);
                    int32_t l_1393[8][10][3] = {{{0xB1C588A1L,0xB0EA253CL,0xB0EA253CL},{5L,(-1L),0xD6D971E9L},{0x55EC4FB9L,6L,0L},{(-1L),0xD080F7C0L,0xACB36679L},{6L,0x9CAEA85FL,0x8DDE1B76L},{0xC476E27EL,0xD080F7C0L,0x0C7901A8L},{(-1L),6L,(-1L)},{0x63C1E6FBL,(-1L),(-1L)},{(-5L),0xB0EA253CL,0L},{0xD7DEC109L,0xB1C588A1L,0L}},{{(-10L),0xD0423F56L,(-4L)},{0xB1C588A1L,0x454A7F3EL,6L},{(-1L),(-10L),0L},{0x0C7901A8L,6L,(-10L)},{(-7L),0x33A43400L,0xC476E27EL},{5L,0xAD0E43FFL,0x8DDE1B76L},{9L,(-5L),0x0A7878BDL},{(-10L),0L,0x0A7878BDL},{0x63C1E6FBL,0x1F1C7B54L,0x8DDE1B76L},{0xB0EA253CL,(-5L),0xC476E27EL}},{{0L,0xB1C588A1L,(-10L)},{(-1L),0xC476E27EL,0L},{0xD0423F56L,0xB0EA253CL,6L},{0xEF2B850EL,(-1L),(-4L)},{(-1L),0L,0L},{(-7L),0x835A2A5FL,0L},{(-1L),0L,(-1L)},{0xC476E27EL,(-5L),(-1L)},{(-1L),0xC50D033EL,0x0C7901A8L},{(-8L),(-1L),0x8DDE1B76L}},{{0x454A7F3EL,0xE20D2CD0L,0xACB36679L},{(-8L),0xD0423F56L,0L},{(-1L),9L,0xD6D971E9L},{0xC476E27EL,(-5L),0xB0EA253CL},{(-1L),(-1L),(-8L)},{(-7L),0xC50D033EL,(-1L)},{(-1L),0x33A43400L,0L},{0xEF2B850EL,0x3D68CCFBL,0xBC32C524L},{0xD0423F56L,0xD080F7C0L,0x0A7878BDL},{0xAD0E43FFL,0xD6D971E9L,0xBC32C524L}},{{0x55EC4FB9L,0x661734B5L,0x835A2A5FL},{0x1E73D664L,0x017388A3L,5L},{0x0C7901A8L,0x454A7F3EL,0x9CAEA85FL},{0x661734B5L,0x454A7F3EL,0x0A7878BDL},{6L,0x017388A3L,1L},{(-1L),0x661734B5L,(-1L)},{(-1L),0xD6D971E9L,(-1L)},{0xEF2B850EL,0x16216B77L,0x454A7F3EL},{0x835A2A5FL,6L,(-1L)},{0xB0EA253CL,9L,0xEF2B850EL}},{{0x661734B5L,0xD7DEC109L,0xBC32C524L},{(-5L),0xAD0E43FFL,0xD080F7C0L},{0x8BE6AE31L,0x8BE6AE31L,(-5L)},{0x0C7901A8L,6L,(-1L)},{0x9CAEA85FL,5L,0x55EC4FB9L},{0x454A7F3EL,0x017388A3L,0x8BE6AE31L},{0x33A43400L,0x9CAEA85FL,0x55EC4FB9L},{1L,0xD7DEC109L,(-1L)},{0x8DDE1B76L,0xC476E27EL,(-5L)},{(-1L),1L,0xD080F7C0L}},{{0xB0EA253CL,0L,0xBC32C524L},{0x9CAEA85FL,(-2L),0xEF2B850EL},{0x55EC4FB9L,0xAD0E43FFL,(-1L)},{0x017388A3L,0x1E73D664L,0x454A7F3EL},{1L,0x454A7F3EL,(-1L)},{1L,0xB0EA253CL,(-1L)},{5L,0x8BE6AE31L,1L},{0x33A43400L,1L,0x0A7878BDL},{0xEF2B850EL,(-2L),0x9CAEA85FL},{0xEF2B850EL,0xC476E27EL,5L}},{{0x33A43400L,0xF6D40ECEL,0x835A2A5FL},{5L,9L,0xBC32C524L},{1L,0L,5L},{1L,0x661734B5L,0xD080F7C0L},{0x017388A3L,0x7F86637BL,(-4L)},{0x55EC4FB9L,5L,0x9CAEA85FL},{0x9CAEA85FL,0xB0EA253CL,1L},{0xB0EA253CL,0x1E73D664L,0x1E73D664L},{(-1L),0x9CAEA85FL,0x0A7878BDL},{0x8DDE1B76L,0L,0L}}};
                    int32_t l_1394 = 0x2A0DF48EL;
                    uint8_t l_1449[7] = {0xB5L,255UL,0xB5L,0xB5L,255UL,0xB5L,0xB5L};
                    int32_t *****l_1459 = (void*)0;
                    int64_t l_1487 = (-5L);
                    int16_t ** const *l_1510 = (void*)0;
                    uint32_t *l_1513 = (void*)0;
                    uint32_t l_1540 = 0xF2F6D269L;
                    int i, j, k;
                    if ((*l_1236))
                    { /* block id: 663 */
                        int32_t *l_1347 = (void*)0;
                        l_1316 ^= ((*l_1236) = (l_1346 == l_1330));
                    }
                    else
                    { /* block id: 666 */
                        uint32_t l_1369[8][8] = {{0UL,0xC68B1D4EL,0x026A4968L,6UL,4294967289UL,0xCDC5267BL,1UL,4294967288UL},{4294967288UL,4294967290UL,0UL,0x92CABAE6L,4294967289UL,4294967289UL,0x92CABAE6L,0UL},{0UL,0UL,5UL,4294967288UL,0xBC96FFC4L,0xBC268358L,0UL,0x330A5AD2L},{0xBC268358L,0xDA29A6F1L,0UL,0xBC96FFC4L,0x92CABAE6L,0x330A5AD2L,0xC68B1D4EL,0x330A5AD2L},{0xDA29A6F1L,4294967288UL,4294967289UL,4294967288UL,0xDA29A6F1L,0UL,5UL,0UL},{4294967289UL,1UL,4294967290UL,0x92CABAE6L,0UL,0xBC96FFC4L,0x026A4968L,4294967288UL},{0x026A4968L,5UL,4294967290UL,6UL,6UL,4294967290UL,5UL,0x026A4968L},{0UL,0xBC268358L,4294967289UL,0UL,0UL,0x92CABAE6L,0UL,0UL}};
                        int32_t *l_1370[9][7][4] = {{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}},{{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271},{&l_1271,&l_1271,&l_1271,&l_1271}}};
                        int32_t l_1384[5][10][2] = {{{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL}},{{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL}},{{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL}},{{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL}},{{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL},{0x90D35D9BL,0x90D35D9BL}}};
                        int8_t *l_1392 = &g_396;
                        int i, j, k;
                        l_1316 &= (((safe_rshift_func_int8_t_s_u((safe_mul_func_uint8_t_u_u((safe_div_func_uint8_t_u_u(l_1338, ((((*l_1236) |= ((p_44 , (((((void*)0 == l_1354) , (p_44 < (((**g_387) ^= ((safe_mul_func_int16_t_s_s((safe_rshift_func_int16_t_s_u((+l_1360), l_1317)), (l_1313 = (((p_44 == (safe_sub_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u((safe_mod_func_uint64_t_u_u(p_44, p_44)), l_1360)), p_44))) <= 0xD988133F710A859CLL) | 0L)))) & 65529UL)) , p_44))) == p_44) > 18446744073709551615UL)) ^ 0xB9F9E7ACFFA9BF76LL)) , p_44) || l_1338))), g_378)), l_1369[7][4])) || p_44) , l_1252);
                        (*l_1236) = ((l_1338 , p_44) >= (**g_387));
                        (*l_1236) = (safe_mul_func_uint8_t_u_u((safe_div_func_uint16_t_u_u((safe_sub_func_int64_t_s_s((((((&g_1286 == &g_1200[5][0][3]) , (+(~(safe_add_func_uint8_t_u_u(((safe_unary_minus_func_int64_t_s((l_1393[4][7][2] &= ((safe_rshift_func_int8_t_s_u(((*l_1392) = (((0xFF1BL <= l_1384[0][8][1]) , (*g_55)) | (l_1391 ^= ((!(+(safe_lshift_func_uint16_t_u_s((((p_44 | ((safe_sub_func_uint64_t_u_u(l_1249, ((l_1360 != (((l_1354 != (void*)0) , g_431[1]) & 0xFAL)) , p_44))) < l_1338)) < (-1L)) | l_1337), 14)))) , 0L)))), p_44)) , 0x77BEF598928CD6AELL)))) , 0x1AL), p_44))))) , 0x9BL) | 0x82L) && l_1360), l_1394)), 0x3585L)), p_44));
                        (*l_1236) &= p_44;
                    }
                    if (((safe_mod_func_uint8_t_u_u(0x24L, 1L)) , p_44))
                    { /* block id: 678 */
                        int8_t *l_1407 = &g_277;
                        int32_t *l_1416 = &l_1320;
                        (*l_1416) ^= (safe_sub_func_uint32_t_u_u(((safe_sub_func_uint64_t_u_u((--(*g_388)), 0x0866C27ED7D4AB93LL)) & g_399), (((safe_add_func_uint64_t_u_u((safe_mul_func_int8_t_s_s((-1L), ((*l_1407) = (-10L)))), ((safe_add_func_int8_t_s_s((safe_div_func_int32_t_s_s(((**g_404) ^ 0UL), (((safe_sub_func_int32_t_s_s(((*g_911) , l_1393[0][1][2]), (safe_mul_func_int16_t_s_s((1L > 0L), p_44)))) == (*l_1236)) & 6L))), g_431[2])) || 0x45AD66BEL))) != p_44) || p_44)));
                        l_1416 = &l_1393[4][7][2];
                        return l_1337;
                    }
                    else
                    { /* block id: 684 */
                        uint32_t l_1441 = 0x5B5F5B23L;
                        uint8_t *l_1452 = (void*)0;
                        uint8_t *l_1453[4] = {&g_1253,&g_1253,&g_1253,&g_1253};
                        uint64_t * const l_1464 = &g_865;
                        int i;
                        (*l_1236) = (((*g_388) = ((safe_lshift_func_int16_t_s_s((+(**g_387)), (((~((safe_rshift_func_uint8_t_u_s(p_44, 4)) == (((safe_lshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_u(0xB0L, (safe_lshift_func_uint8_t_u_s((l_1298 &= (((((safe_sub_func_uint64_t_u_u(l_1431, (((**g_1201) = (safe_rshift_func_int16_t_s_s((safe_sub_func_uint16_t_u_u((safe_rshift_func_uint16_t_u_u(((((+(safe_add_func_int16_t_s_s(p_44, l_1441))) != (l_1442[0][3][1] != (void*)0)) >= (safe_mod_func_uint16_t_u_u(((((safe_sub_func_uint8_t_u_u(((((void*)0 == &g_387) < (**g_387)) > 0UL), l_1249)) <= l_1338) , (*l_1236)) ^ 0x29L), l_1449[6]))) >= (-4L)), p_44)), (*g_405))), 15))) , l_1249))) , l_1450) , (*g_639)) != l_1451) || l_1441)), g_267)))), p_44)) , 0x774DL) , l_1450))) && (**g_404)) != 0xEF30BACBL))) && l_1243)) == p_44);
                        l_1391 |= p_44;
                        (*l_1236) = (((safe_mul_func_uint8_t_u_u(p_44, (safe_mul_func_int16_t_s_s(p_44, (safe_unary_minus_func_int32_t_s((((((l_1459 == (((*g_388) = 0x505B54E3E7DE39DDLL) , l_1460)) > (safe_mul_func_int8_t_s_s(((*g_387) != l_1464), (safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u(((safe_lshift_func_uint16_t_u_s((safe_sub_func_int64_t_s_s((((*l_1250) ^= ((-1L) && ((((((safe_div_func_uint16_t_u_u(((((((safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(((safe_add_func_uint16_t_u_u(l_1320, l_1483)) <= 0x49A0FD7CL), (*g_405))), (*g_405))) == p_44) , 0x4DECL) || (*g_405)) & p_44) ^ g_396), (**g_404))) , l_1484) != l_1485) != l_1486) == p_44) <= 0xAFC9E41EL))) || 18446744073709551611UL), 1L)), 12)) >= g_518), 0xE4L)), 4))))) > p_44) , l_1487) ^ p_44))))))) || (**g_983)) , p_44);
                    }
                    if (((safe_unary_minus_func_int64_t_s(3L)) || (safe_rshift_func_int8_t_s_u((((safe_mul_func_int16_t_s_s(p_44, p_44)) || (p_44 || (safe_div_func_uint16_t_u_u(0xBE60L, (0xBF69CA52L && (p_44 < (((safe_mul_func_int16_t_s_s(((**g_1201) = ((((((*g_388) , (safe_lshift_func_uint16_t_u_u((safe_div_func_int64_t_s_s(g_56[0], l_1337)), 12))) , (**g_404)) || p_44) > 0x3D35D89EBF09384DLL) | (*l_1236))), p_44)) && l_1501) || 18446744073709551608UL))))))) , 2L), 3))))
                    { /* block id: 695 */
                        if (l_1298)
                            goto lbl_1502;
                        return (*g_405);
                    }
                    else
                    { /* block id: 698 */
                        int16_t ** const **l_1511[10][10][2] = {{{&l_1510,&l_1510},{&l_1510,&l_1510},{(void*)0,&l_1510},{&l_1510,&l_1510},{(void*)0,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,(void*)0},{(void*)0,&l_1510}},{{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,&l_1510},{&l_1510,(void*)0},{&l_1510,(void*)0}},{{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,&l_1510},{&l_1510,(void*)0}},{{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,&l_1510}},{{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0},{(void*)0,&l_1510}},{{(void*)0,&l_1510},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0}},{{(void*)0,&l_1510},{(void*)0,&l_1510},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510},{&l_1510,&l_1510}},{{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,&l_1510},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0},{&l_1510,&l_1510}},{{&l_1510,&l_1510},{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,&l_1510},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510},{(void*)0,(void*)0}},{{&l_1510,&l_1510},{&l_1510,&l_1510},{&l_1510,(void*)0},{(void*)0,&l_1510},{(void*)0,&l_1510},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,(void*)0},{&l_1510,&l_1510},{(void*)0,&l_1510}}};
                        int32_t l_1525 = (-1L);
                        int8_t *l_1539 = &g_277;
                        uint8_t *l_1550[7];
                        int i, j, k;
                        for (i = 0; i < 7; i++)
                            l_1550[i] = &l_1449[6];
                        (*l_1451) = ((safe_rshift_func_uint8_t_u_u(p_44, (+((((safe_mul_func_uint8_t_u_u(((0xEA0AL != (safe_sub_func_int16_t_s_s(((l_1512 = l_1510) != (((((65535UL ^ ((*l_1330) = p_44)) >= (l_1513 != l_1514)) , ((l_1516[2][6][4] = (0UL || g_1515)) <= l_1252)) , p_44) , (void*)0)), g_1517))) <= p_44), (*l_1236))) , l_1518) != l_1518) , p_44)))) , &l_1252);
                        l_1519[5][0] = &l_1317;
                        g_56[0] |= ((safe_div_func_uint64_t_u_u((+g_761), (safe_lshift_func_int8_t_s_s((((((*l_1539) |= (((l_1525 != ((safe_add_func_int16_t_s_s((((safe_mod_func_int16_t_s_s((safe_sub_func_int16_t_s_s((l_1525 == ((*l_1236) = (((safe_sub_func_int32_t_s_s(((*g_868) = (-1L)), ((p_44 & ((void*)0 == (***l_1460))) , (((((l_1525 >= (safe_mod_func_uint8_t_u_u((~(safe_lshift_func_uint16_t_u_u(0x5172L, p_44))), p_44))) && (**g_404)) & 253UL) || p_44) >= p_44)))) | (*g_911)) <= p_44))), p_44)), 0x3CC4L)) && 0xA1L) , (-1L)), 0xB538L)) >= p_44)) ^ 0x764026B80AC71980LL) ^ p_44)) == p_44) <= (-1L)) & p_44), g_267)))) <= 0L);
                        (*g_868) |= (((l_1525 = l_1540) , p_44) != ((safe_div_func_int64_t_s_s((*l_1236), (*g_388))) >= ((**g_387) < (((safe_sub_func_uint8_t_u_u(249UL, (!((((safe_add_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u(((++g_896) & (safe_div_func_uint32_t_u_u(4294967295UL, (*g_984)))), 0x54B6B0C85E48A51BLL)) != 0x47L), p_44)) == 1L) , 0xA3L) ^ p_44)))) > p_44) , 18446744073709551609UL))));
                    }
                    l_1317 ^= (safe_sub_func_int8_t_s_s(p_44, 0xFAL));
                }
                if (l_1450)
                { /* block id: 714 */
                    int32_t l_1557 = (-4L);
                    l_1557 = p_44;
                }
                else
                { /* block id: 716 */
                    int8_t l_1578[4];
                    int16_t ***l_1604[9][1][6] = {{{&g_1201,&g_1201,&g_1201,(void*)0,(void*)0,&g_1201}},{{&g_1201,&l_1518,&l_1518,&l_1518,&l_1518,&l_1518}},{{&g_1201,&l_1518,&l_1518,&g_1201,&l_1518,&g_1201}},{{&l_1518,&l_1518,&g_1201,&l_1518,&l_1518,(void*)0}},{{(void*)0,&g_1201,&l_1518,(void*)0,&l_1518,&g_1201}},{{&g_1201,(void*)0,&g_1201,&g_1201,&g_1201,&g_1201}},{{&l_1518,&l_1518,&l_1518,&l_1518,(void*)0,(void*)0}},{{&g_1201,&g_1201,&g_1201,&g_1201,&l_1518,&g_1201}},{{&l_1518,&l_1518,&l_1518,&l_1518,&g_1201,&l_1518}}};
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_1578[i] = 0x70L;
                    for (l_1271 = 0; (l_1271 <= 2); l_1271 += 1)
                    { /* block id: 719 */
                        uint64_t l_1570 = 7UL;
                        int32_t ******l_1577 = &g_1575[4];
                        int32_t l_1595 = 0xEEDEAEB2L;
                        int16_t ******l_1601 = (void*)0;
                        int16_t ******l_1602 = &l_1600;
                        int8_t *l_1630 = &g_396;
                        int i;
                    }
                    for (g_119 = 14; (g_119 < 41); g_119 = safe_add_func_int8_t_s_s(g_119, 4))
                    { /* block id: 736 */
                        (*l_1451) = (l_1633 = l_1633);
                        if (p_44)
                            continue;
                    }
                    for (g_1082 = 0; (g_1082 == (-2)); g_1082 = safe_sub_func_uint8_t_u_u(g_1082, 8))
                    { /* block id: 743 */
                        int32_t l_1651 = 0x9D29B9F1L;
                        (***l_1564) &= ((safe_mod_func_uint16_t_u_u((safe_rshift_func_int16_t_s_u((*l_1236), ((**g_387) & (((l_1640 == &g_1575[6]) || (safe_add_func_int32_t_s_s(((((*g_911) = (((safe_lshift_func_int16_t_s_s(1L, 11)) >= (safe_rshift_func_int8_t_s_s((safe_mod_func_int64_t_s_s((safe_add_func_uint32_t_u_u(((l_1651 ^ ((*l_1633) , (safe_mul_func_int8_t_s_s(g_119, 0x03L)))) & (-1L)), 0x192DDA58L)), p_44)), 4))) != l_1654)) , 0x124AF350L) & 4294967292UL), l_1651))) | p_44)))), (*l_1633))) ^ p_44);
                        (*l_1633) = l_1651;
                        if (g_119)
                            goto lbl_1655;
                    }
                    (**l_1564) = &l_1320;
                }
                for (g_375 = 27; (g_375 < 26); g_375 = safe_sub_func_int16_t_s_s(g_375, 2))
                { /* block id: 753 */
                    uint32_t l_1658 = 0xFD02BFB9L;
                    int16_t *** const **l_1669[3][9][3] = {{{&g_1285,&g_1285,(void*)0},{&g_1285,(void*)0,&g_1285},{&g_1285,(void*)0,(void*)0},{&g_1285,(void*)0,&g_1285},{&g_1285,&g_1285,&g_1285},{&g_1285,&g_1285,&g_1285},{&g_1285,(void*)0,&g_1285},{&g_1285,&g_1285,&g_1285},{&g_1285,&g_1285,&g_1285}},{{(void*)0,(void*)0,&g_1285},{&g_1285,(void*)0,&g_1285},{&g_1285,(void*)0,&g_1285},{&g_1285,&g_1285,&g_1285},{(void*)0,&g_1285,&g_1285},{(void*)0,&g_1285,&g_1285},{&g_1285,&g_1285,&g_1285},{(void*)0,&g_1285,&g_1285},{(void*)0,(void*)0,&g_1285}},{{&g_1285,&g_1285,(void*)0},{&g_1285,&g_1285,&g_1285},{&g_1285,&g_1285,(void*)0},{(void*)0,(void*)0,&g_1285},{&g_1285,&g_1285,&g_1285},{&g_1285,&g_1285,(void*)0},{&g_1285,&g_1285,&g_1285},{&g_1285,&g_1285,&g_1285},{&g_1285,&g_1285,(void*)0}}};
                    int16_t *** const ***l_1668 = &l_1669[1][7][2];
                    int8_t *l_1675[4];
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_1675[i] = (void*)0;
                    l_1658--;
                    for (l_1252 = (-4); (l_1252 <= (-23)); l_1252 = safe_sub_func_uint16_t_u_u(l_1252, 9))
                    { /* block id: 757 */
                        (**l_1564) = (void*)0;
                        (*l_1451) = func_52(&l_1320, &l_1320);
                    }
                    if (p_44)
                        break;
                    l_1317 ^= ((safe_sub_func_uint32_t_u_u((safe_sub_func_uint8_t_u_u((((((l_1667 = &l_1600) != l_1668) && ((**g_983) , (((((((~(safe_rshift_func_uint16_t_u_u(((&l_1270 == ((((*l_1236) &= (safe_mul_func_int8_t_s_s(l_1658, (g_1175 != &l_1288)))) && (((safe_mod_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u(((*l_1633) | p_44), p_44)), p_44)) , (*g_387)) != (*g_387))) , g_1680)) , 65535UL), p_44))) & p_44) | 0xFBL) != p_44) , l_1658) | g_1253) & 1UL))) == p_44) & p_44), g_1164[3][4])), g_1682)) > 254UL);
                }
            }
            else
            { /* block id: 766 */
                uint64_t l_1695 = 0UL;
                int32_t l_1696[3];
                int i;
                for (i = 0; i < 3; i++)
                    l_1696[i] = 0L;
                l_1696[1] = (safe_add_func_uint64_t_u_u((safe_mod_func_uint32_t_u_u(((*g_1584) , 0xF652E776L), (*l_1633))), (safe_mod_func_uint16_t_u_u(((safe_mod_func_int8_t_s_s(((*g_911) , (g_396 = (g_1082 > p_44))), ((18446744073709551615UL <= (((l_1695 ^= ((***l_1564) = (((18446744073709551611UL >= ((safe_mul_func_int16_t_s_s((safe_add_func_int8_t_s_s(p_44, (*l_1236))), 0xC566L)) != p_44)) || 0x6278C287L) && 65526UL))) < (-5L)) >= 65535UL)) ^ p_44))) > g_57), p_44))));
            }
            return p_44;
        }
        else
        { /* block id: 773 */
            int64_t *l_1706 = &g_19;
            int64_t **l_1707 = &l_1250;
            uint64_t *l_1710 = &g_865;
            int32_t l_1711 = 2L;
            int32_t l_1712[9] = {(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L),(-9L)};
            int i;
            for (g_378 = 0; (g_378 <= 49); ++g_378)
            { /* block id: 776 */
                return (*l_1236);
            }
            (*l_1236) = ((l_1712[8] ^= (safe_lshift_func_int16_t_s_u((((safe_mul_func_int16_t_s_s(g_1703, ((void*)0 != (**l_1564)))) && (((((((p_44 , (safe_sub_func_uint32_t_u_u((**g_983), ((((*l_1707) = l_1706) == (p_44 , &g_19)) || ((*l_1710) = ((**g_387) = (~((((!g_60) || p_44) != (*g_405)) , (*l_1236))))))))) , (*l_1236)) | (**g_404)) , (-10L)) < (-1L)) ^ 0L) | l_1711)) | p_44), 11))) , (-8L));
            (*l_1236) = (***l_1564);
            l_1711 ^= (*l_1236);
        }
    }
    else
    { /* block id: 787 */
        int16_t *l_1714 = &g_1517;
        int32_t **l_1715[1][5][8] = {{{&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868},{&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868},{&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868},{&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868},{&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868,&g_868}}};
        int8_t *l_1727 = &g_399;
        int8_t *l_1728 = (void*)0;
        int8_t *l_1729[4][4][4] = {{{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0},{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0}},{{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0},{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0}},{{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0},{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0}},{{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0},{&g_396,&g_396,&g_453,(void*)0},{&g_463,&g_396,(void*)0,(void*)0}}};
        int i, j, k;
        l_1236 = func_52(l_1713, ((l_1714 != (void*)0) , (void*)0));
        l_1716 = &l_1298;
        l_1321[1] = ((((safe_mod_func_int8_t_s_s((l_1730 = (safe_mod_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u((p_44 || (safe_lshift_func_int8_t_s_u(1L, 1))), p_44)), (((((**g_404) = (safe_rshift_func_int8_t_s_s(0x25L, ((*l_1716) = ((*l_1727) = ((((g_761 , p_44) & (*l_1716)) , ((*g_984) > ((*l_1716) , p_44))) | g_1517)))))) && 65535UL) == (*g_1584)) , 0x05BCL)))), 0xF3L)) && 4294967289UL) < (*g_1584)) & (*l_1713));
    }
    l_1731 = &l_1298;
    l_1737++;
    return (***g_1681);
}


/* ------------------------------------------ */
/* 
 * reads : g_870 g_388 g_119 g_431 g_60 g_983 g_984 g_405 g_1082 g_56 g_463 g_399 g_387 g_55 g_404 g_565 g_911 g_1175 g_396 g_87 g_1164 g_1199 g_375 g_165 g_1201 g_57 g_1200
 * writes: g_884 g_870 g_431 g_60 g_1082 g_868 g_463 g_399 g_119 g_375 g_896 g_518 g_56 g_87 g_910 g_55
 */
static uint16_t  func_47(int32_t * p_48, uint64_t  p_49, int32_t * p_50, int32_t * p_51)
{ /* block id: 420 */
    int64_t l_871 = 0x65446C630ECB4B86LL;
    int32_t l_872[4][3] = {{(-1L),0xA5AB6D91L,0x5BD67C8CL},{(-1L),(-1L),0xA5AB6D91L},{0x8B0C5EB0L,0xA5AB6D91L,0xA5AB6D91L},{0xA5AB6D91L,0x4E3BFB03L,0x5BD67C8CL}};
    int32_t *l_873[9][1] = {{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0},{(void*)0}};
    uint64_t l_874 = 0xC8CFB93EC130C377LL;
    uint16_t *l_879[2];
    const int16_t *l_882 = &g_883[3][0];
    const int16_t **l_881 = &l_882;
    const int16_t ***l_880[3];
    uint32_t *l_888 = &g_431[2];
    int16_t **l_923[5] = {&g_565[0],&g_565[0],&g_565[0],&g_565[0],&g_565[0]};
    int16_t ***l_922 = &l_923[1];
    int64_t l_952 = 0xF9F3F0728C6CCC9ELL;
    uint64_t l_953 = 7UL;
    int32_t l_1008 = 0x234108A8L;
    uint32_t l_1019[4] = {18446744073709551611UL,18446744073709551611UL,18446744073709551611UL,18446744073709551611UL};
    int16_t l_1048 = (-1L);
    uint64_t l_1074 = 18446744073709551609UL;
    const int32_t *l_1163 = &g_1164[1][1];
    int32_t **l_1187 = &l_873[2][0];
    int32_t l_1196 = 0x61745982L;
    int i, j;
    for (i = 0; i < 2; i++)
        l_879[i] = &g_60;
    for (i = 0; i < 3; i++)
        l_880[i] = &l_881;
    l_874++;
lbl_891:
    (*p_51) = (((((((safe_add_func_uint16_t_u_u(((l_879[0] != (void*)0) == (*p_51)), 5L)) < p_49) , p_49) == ((g_884[2] = (void*)0) != ((((void*)0 != (*l_881)) , (*g_388)) , &g_565[0]))) >= p_49) <= 0x7AL) > 0UL);
    if (((safe_div_func_int32_t_s_s((*p_51), (--(*l_888)))) , 0x53DC2717L))
    { /* block id: 425 */
        int16_t l_906 = 3L;
        int32_t l_909[10][9] = {{(-1L),0x4E5D9499L,0x21930E62L,(-4L),0x21930E62L,0x4E5D9499L,(-1L),0x95571FA3L,0xC5516617L},{(-1L),0x4C59A701L,5L,5L,0x49E87CCBL,0x63B69267L,1L,1L,1L},{(-3L),0x8DD575E1L,(-1L),(-1L),0x8DD575E1L,(-3L),0xC5516617L,0x95571FA3L,(-1L)},{1L,0xED8E2DEDL,0x16D071FAL,0x63B69267L,(-1L),0x90B73D35L,0x49E87CCBL,0x49E87CCBL,0x90B73D35L},{(-1L),0xB8034B8AL,(-1L),0xB8034B8AL,(-1L),0x21930E62L,0xC5516617L,(-4L),0xFFD446D4L},{0x77BDFA05L,0x49E87CCBL,1L,(-6L),0xED8E2DEDL,0x4C59A701L,1L,0x4C59A701L,0xED8E2DEDL},{0xC5516617L,0xC5AA3CEAL,0xC5AA3CEAL,0xC5516617L,0L,0x21930E62L,(-1L),(-3L),(-8L)},{0x90538787L,1L,3L,1L,0x7EE70A9DL,0x90B73D35L,0x90B73D35L,0x7EE70A9DL,1L},{0x4E5D9499L,0x65E491ABL,0x4E5D9499L,(-7L),0L,(-3L),0xFFD446D4L,0x003B3C96L,0x21930E62L},{0x16D071FAL,0x77BDFA05L,0x7EE70A9DL,5L,0xED8E2DEDL,0x63B69267L,0xED8E2DEDL,5L,0x7EE70A9DL}};
        int32_t l_925 = 1L;
        int32_t l_944 = 0x9D70573EL;
        int32_t l_945 = 0x6755497EL;
        int32_t l_946 = (-10L);
        int32_t l_948 = 0xB7D27B0BL;
        int32_t l_949 = 1L;
        int32_t l_951[3];
        int16_t ** const *l_973 = (void*)0;
        uint64_t * const *l_1039 = &g_388;
        int i, j;
        for (i = 0; i < 3; i++)
            l_951[i] = 0xC2C524ADL;
        if (p_49)
            goto lbl_891;
        for (g_60 = 0; (g_60 <= 4); g_60 += 1)
        { /* block id: 429 */
            uint64_t l_892[2][8][3] = {{{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL},{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL},{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL},{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL}},{{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL},{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL},{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL},{2UL,2UL,2UL},{0xB980A650C022FF22LL,6UL,0xB980A650C022FF22LL}}};
            uint8_t *l_905 = &g_896;
            int32_t *l_908 = &g_57;
            int32_t * const l_912 = &l_872[1][2];
            int16_t **l_920 = &g_565[0];
            int16_t ***l_919 = &l_920;
            int32_t l_939 = 0x05085743L;
            int32_t l_941 = 0xDCCF36AAL;
            int32_t l_942[9][8][3] = {{{(-1L),0x3D28DFF4L,0x1303B823L},{0x7681C3BBL,0x7FADCA1FL,0x0A528985L},{0x5DD5734AL,0x7561DEDBL,0x9CA1203FL},{0xF9EAAD97L,0x7681C3BBL,0L},{(-4L),0xA196D9E0L,0xFC03B36DL},{(-10L),0xA196D9E0L,0x2C897560L},{0xFD1049F2L,0x7681C3BBL,(-1L)},{0xC9596F18L,0x7561DEDBL,(-2L)}},{{0xFDEAC7FEL,0x7FADCA1FL,(-6L)},{0x404CC479L,0x3D28DFF4L,0x5DD5734AL},{7L,0L,0x7561DEDBL},{0L,0xF4442FBCL,0xB09C935FL},{0x383E9D90L,0x5DD5734AL,0x2C897560L},{0xBD72C15DL,0x6EDEE8D7L,0x1303B823L},{(-1L),0L,0x8F69AAAEL},{0L,0xFD1049F2L,0xC20270FBL}},{{0xF9EAAD97L,(-1L),0xF9EAAD97L},{7L,0xE83B51F0L,0x866CDB3CL},{0xB34C9F32L,0xA196D9E0L,0xF4442FBCL},{0x7561DEDBL,1L,0L},{(-1L),0xFDEAC7FEL,(-2L)},{0x7561DEDBL,0L,1L},{0xB34C9F32L,(-6L),(-6L)},{7L,0x383E9D90L,0x6421C552L}},{{0xF9EAAD97L,0x771DF8A8L,0xEC85431CL},{0L,0x5DD5734AL,0xF4442FBCL},{(-1L),0x9CA1203FL,0x1DA5BAA0L},{0xBD72C15DL,0x008F3867L,0x8F69AAAEL},{0x383E9D90L,0x7561DEDBL,0x450877BBL},{0L,0xBD72C15DL,0x0A528985L},{7L,0x6C5EEFB1L,0xFC03B36DL},{0x404CC479L,0xE83B51F0L,(-7L)}},{{0xFDEAC7FEL,1L,0xB09C935FL},{0xC9596F18L,0x6421C552L,(-4L)},{0xFD1049F2L,0x008F3867L,1L},{(-10L),0x3D28DFF4L,1L},{(-4L),0x68E976C0L,(-4L)},{0xF9EAAD97L,(-7L),0xB09C935FL},{0x5DD5734AL,0L,(-7L)},{0x7681C3BBL,0x9CA1203FL,0xFC03B36DL}},{{(-1L),(-5L),0x0A528985L},{0x68E976C0L,0xFDEAC7FEL,0x450877BBL},{0L,(-1L),0x8F69AAAEL},{(-4L),0xD712A2B6L,0x1DA5BAA0L},{0x404CC479L,0x6C5EEFB1L,0xF4442FBCL},{0x6421C552L,0x7681C3BBL,0xEC85431CL},{1L,0x6421C552L,0x6421C552L},{0x7561DEDBL,(-5L),(-6L)}},{{8L,0x6EDEE8D7L,1L},{1L,0x383E9D90L,(-2L)},{0L,0x23D55EA3L,0L},{0x5DD5734AL,0x383E9D90L,0xF4442FBCL},{1L,0x6EDEE8D7L,0x866CDB3CL},{0x7681C3BBL,(-5L),0xF9EAAD97L},{0x383E9D90L,0x6421C552L,0xC20270FBL},{2L,0x7681C3BBL,0x8F69AAAEL}},{{1L,9L,0xEC85431CL},{0xF9EAAD97L,6L,0x4AF81FE6L},{(-1L),(-2L),8L},{0x68E976C0L,(-10L),0xA1638799L},{(-10L),3L,0x23D55EA3L},{1L,0xFD1049F2L,0x5C958FD6L},{0L,0xF4442FBCL,0xC9596F18L},{(-2L),0x5EF92951L,(-10L)}},{{0xF4442FBCL,0x2C897560L,0x4AF81FE6L},{0xE2887258L,0L,0x1604E008L},{0xE2887258L,0xFC03B36DL,(-2L)},{0xF4442FBCL,(-1L),0xFD1049F2L},{(-2L),0xE2887258L,0x450877BBL},{0L,(-5L),0xEC85431CL},{1L,9L,(-1L)},{(-10L),0x100C7F77L,0xD06880E7L}}};
            const uint16_t *l_971 = &g_60;
            const uint16_t **l_970 = &l_971;
            const uint16_t ***l_969 = &l_970;
            int32_t l_1015 = 0xDDF31A0FL;
            int16_t l_1018 = 1L;
            int8_t l_1044[6][7] = {{(-1L),0L,(-1L),(-1L),0L,(-1L),0L},{1L,0xF5L,0xF5L,1L,0xDCL,1L,0xF5L},{0x89L,0x89L,(-1L),(-1L),(-1L),0x89L,0x89L},{1L,0xF5L,(-9L),0xF5L,1L,1L,0xF5L},{8L,0L,8L,(-1L),(-1L),8L,0L},{0xF5L,0xDCL,(-9L),(-9L),0xDCL,0xF5L,0xDCL}};
            uint64_t l_1050 = 18446744073709551615UL;
            uint16_t l_1053 = 0x840EL;
            int i, j, k;
            (*p_51) = l_892[0][4][2];
        }
    }
    else
    { /* block id: 522 */
        int8_t l_1079 = 0x0FL;
        uint16_t **l_1080 = &g_405;
        int32_t *l_1081 = &g_1082;
        uint8_t l_1083[10][3] = {{254UL,248UL,0x6AL},{0x91L,248UL,248UL},{247UL,248UL,255UL},{254UL,248UL,0x6AL},{0x91L,248UL,248UL},{247UL,248UL,255UL},{254UL,248UL,0x6AL},{0x91L,248UL,248UL},{247UL,248UL,255UL},{254UL,248UL,0x6AL}};
        int8_t *l_1084 = &l_1079;
        int64_t l_1097 = 3L;
        int32_t l_1143 = 7L;
        int32_t l_1144 = 0x0BB2DC94L;
        int32_t ****l_1151[6][10] = {{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754}};
        uint16_t l_1169[3];
        uint32_t **l_1190 = &l_888;
        int16_t ****l_1202 = (void*)0;
        int64_t * const l_1233 = (void*)0;
        int64_t * const *l_1232 = &l_1233;
        int64_t * const **l_1231 = &l_1232;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1169[i] = 65535UL;
        (*p_51) = l_1074;
        if ((safe_add_func_uint32_t_u_u((**g_983), (((*p_51) = (*p_51)) && ((*g_405) < (((((*l_1084) = (((safe_sub_func_uint16_t_u_u(l_1079, ((void*)0 != l_1080))) && 1L) ^ (((((*l_1081) &= ((void*)0 != &g_640)) , p_49) & l_1083[0][0]) != g_56[5]))) & (-7L)) ^ (*g_388)) <= p_49))))))
        { /* block id: 527 */
            int64_t l_1104 = (-2L);
            int32_t *l_1105[3];
            int i;
            for (i = 0; i < 3; i++)
                l_1105[i] = &l_872[1][2];
            p_48 = l_1105[0];
            (*p_51) = (*p_51);
            return p_49;
        }
        else
        { /* block id: 532 */
            int32_t **l_1106 = &g_868;
            uint64_t **l_1156 = &g_388;
            const int32_t **l_1162[1][9][5] = {{{&g_55,&g_55,&g_55,&g_55,&g_55},{&g_55,&g_55,&g_55,&g_55,&g_55},{&g_55,&g_55,&g_55,(void*)0,&g_55},{&g_55,(void*)0,&g_55,&g_55,(void*)0},{&g_55,&g_55,&g_55,&g_55,&g_55},{&g_55,&g_55,(void*)0,&g_55,&g_55},{&g_55,(void*)0,(void*)0,&g_55,&g_55},{&g_55,&g_55,&g_55,&g_55,&g_55},{&g_55,&g_55,&g_55,&g_55,&g_55}}};
            int64_t *l_1177[8][9][3] = {{{&l_952,&g_184,&l_871},{(void*)0,&l_1097,&l_871},{&l_871,&g_184,&l_871},{&l_1097,&l_952,&l_1097},{&l_871,&g_184,&l_871},{&l_952,&l_1097,&g_184},{&g_184,&g_184,&l_871},{(void*)0,&l_952,&l_1097},{&l_952,&l_952,&g_19}},{{(void*)0,(void*)0,&l_871},{&g_184,&l_1097,&l_952},{&l_952,&g_184,&l_952},{&l_871,&l_952,&l_952},{&l_1097,&l_952,&l_952},{&l_871,&g_184,&l_952},{(void*)0,&l_1097,&l_871},{&l_952,&g_184,&g_19},{&l_1097,(void*)0,&l_1097}},{{&l_871,&g_184,&l_871},{&l_952,&l_1097,&g_184},{&l_871,&g_184,&l_871},{(void*)0,&l_952,&l_1097},{&l_952,&l_952,&l_871},{(void*)0,&g_184,&l_871},{&l_871,&l_1097,&l_871},{&l_952,(void*)0,&l_952},{&l_871,&l_952,&l_871}},{{&l_1097,&l_952,&l_952},{&l_952,&g_184,&l_871},{(void*)0,&l_1097,&l_871},{&l_871,&g_184,&l_871},{&l_1097,&l_952,&l_1097},{&l_871,&g_184,&l_871},{&l_952,&l_1097,&g_184},{&g_184,&g_184,&l_871},{(void*)0,&l_952,&l_1097}},{{&l_952,&l_952,&g_19},{(void*)0,(void*)0,&l_871},{&g_184,&l_1097,&l_952},{&l_952,&g_184,&l_952},{&l_871,&l_952,&l_952},{&l_1097,&l_952,&l_952},{&l_871,&g_184,&l_952},{(void*)0,&l_1097,&l_871},{&l_952,&g_184,&g_19}},{{&l_1097,(void*)0,&l_1097},{&l_871,&g_184,&l_871},{&l_952,&l_1097,&g_184},{&l_871,&g_184,&l_871},{(void*)0,&l_952,&l_1097},{&l_952,&l_952,&l_871},{(void*)0,&g_184,&l_871},{&l_871,&g_19,&g_19},{(void*)0,&l_1097,&l_952}},{{&l_952,&g_184,&l_871},{&l_952,&l_952,&l_952},{(void*)0,&l_1097,&g_19},{&l_871,&l_952,&g_184},{&l_871,(void*)0,&l_871},{&l_952,&l_1097,&l_871},{&l_952,(void*)0,&l_952},{(void*)0,&l_952,(void*)0},{&l_871,&l_1097,&l_952}},{{&l_1097,&l_952,&l_871},{&l_871,&g_184,&l_952},{&l_1097,&l_1097,&g_184},{&l_871,&g_19,&l_871},{(void*)0,(void*)0,&l_952},{&l_952,&g_184,(void*)0},{&l_952,(void*)0,&l_952},{&l_871,&l_1097,&l_871},{&l_871,&l_952,&g_184}}};
            int64_t * const *l_1176 = &l_1177[7][8][1];
            int64_t * const **l_1178 = &l_1176;
            int32_t l_1179 = 0x4D82BB8FL;
            int i, j, k;
            (*l_1106) = p_48;
            for (g_463 = 0; (g_463 < 17); g_463++)
            { /* block id: 536 */
                int32_t l_1110[4] = {6L,6L,6L,6L};
                uint16_t l_1117 = 0xCDD4L;
                const int32_t l_1118[9] = {6L,(-10L),6L,6L,(-10L),6L,6L,(-10L),6L};
                int32_t l_1122 = 1L;
                int32_t ****l_1148 = &g_754;
                uint64_t **l_1158 = &g_388;
                int i;
                for (g_399 = 3; (g_399 >= 0); g_399 -= 1)
                { /* block id: 539 */
                    int8_t l_1142 = 0x34L;
                    int32_t l_1145 = (-1L);
                    uint8_t *l_1146 = &l_1083[8][1];
                    l_1122 = (!(((*g_911) = ((l_1110[0] > (((*l_1146) = (safe_mul_func_uint8_t_u_u((safe_lshift_func_int16_t_s_u((safe_mul_func_int16_t_s_s((l_1145 = (((((*l_1084) = (l_1117 || (l_1118[4] , ((((+(safe_div_func_int32_t_s_s((((((**g_387)++) | ((!(~((safe_div_func_int8_t_s_s((safe_rshift_func_uint16_t_u_s(((1UL | (l_1144 = (l_1143 = ((***l_922) = (safe_mul_func_int8_t_s_s(((safe_add_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((((*g_55) , ((~(safe_add_func_int64_t_s_s((safe_lshift_func_uint8_t_u_s(((0x4EL ^ ((((((void*)0 != (*g_404)) , (1UL && p_49)) <= l_1142) , 0x943CL) == p_49)) , p_49), l_1142)), l_1142))) , p_49)) , 252UL) < l_1097), (-1L))), 8UL)) , (-2L)), p_49)))))) <= l_1083[0][0]), l_1117)), l_1097)) & p_49))) >= 0x71C32258L)) || l_1118[4]) <= p_49), l_1097))) >= l_1097) == l_1122) || l_1144)))) | 6L) < l_1142) <= (*g_405))), p_49)), 9)), g_870[4][0]))) < p_49)) || 1UL)) == (*p_51)));
                    (*p_51) = (safe_unary_minus_func_uint32_t_u((l_1083[5][1] == (*p_51))));
                    for (l_1122 = 0; (l_1122 <= 3); l_1122 += 1)
                    { /* block id: 552 */
                        int32_t *****l_1149 = (void*)0;
                        int32_t *****l_1150 = &l_1148;
                        int64_t * const l_1152 = &l_871;
                        uint64_t **l_1157 = &g_388;
                        uint8_t *l_1159 = &g_896;
                        uint8_t *l_1160 = (void*)0;
                        uint8_t *l_1161 = &g_518;
                        g_870[6][0] ^= (((*l_1150) = l_1148) != l_1151[5][0]);
                        (*p_51) ^= (((l_1152 != (void*)0) || (~p_49)) , (((--(*l_1146)) != ((*l_1161) = ((*l_1159) = (l_1156 != (l_1158 = ((0xC6BDCEF04B2927EDLL != 0x02480EC8E781B9A1LL) , l_1157)))))) <= p_49));
                        if ((*g_55))
                            continue;
                    }
                }
            }
            (*l_1106) = p_48;
            (*p_51) = ((1L < (safe_rshift_func_int8_t_s_u(((p_49 ^ (safe_sub_func_uint64_t_u_u((**g_387), (((l_1169[0] || 0xCCL) , p_49) == (safe_mod_func_int16_t_s_s((0x3C9E6B39L == (safe_add_func_int8_t_s_s((+(((g_1175 == ((*l_1178) = l_1176)) < g_396) , 0xBEEDBF6FFE316B94LL)), p_49))), l_1179)))))) | (**g_404)), 2))) == p_49);
        }
        g_56[2] ^= (*p_51);
        for (g_87 = 0; (g_87 > 57); g_87 = safe_add_func_uint64_t_u_u(g_87, 4))
        { /* block id: 572 */
            int32_t **l_1186 = (void*)0;
            uint32_t ***l_1191[6][7];
            int16_t ****l_1203 = &g_1200[2][2][3];
            int64_t *l_1217 = &l_952;
            const int32_t l_1218 = 0x233BE26FL;
            const int32_t * const ***l_1226 = (void*)0;
            int i, j;
            for (i = 0; i < 6; i++)
            {
                for (j = 0; j < 7; j++)
                    l_1191[i][j] = &g_910[5][0][1];
            }
            (*p_51) = ((safe_add_func_uint64_t_u_u((((p_49 && (*g_984)) < (safe_mul_func_uint16_t_u_u(((l_1187 = l_1186) == &p_48), (*l_1163)))) & ((void*)0 == &g_404)), ((safe_lshift_func_uint8_t_u_u(p_49, p_49)) , p_49))) , (*g_55));
            g_910[2][0][2] = l_1190;
            (*p_51) |= ((safe_mod_func_int64_t_s_s(((*l_1217) = ((safe_lshift_func_int16_t_s_s(l_1196, 15)) || (((p_49 = ((*g_388) = (p_49 & (safe_add_func_int8_t_s_s(((((l_1202 = g_1199) == l_1203) || p_49) , (+((((safe_add_func_int64_t_s_s(g_375, ((-5L) != ((((p_49 && (safe_mod_func_int16_t_s_s((safe_mul_func_int16_t_s_s(((((safe_sub_func_uint32_t_u_u(((safe_mul_func_uint16_t_u_u(((**g_404)--), (g_165 , p_49))) ^ (*l_1163)), 0x4A9C2996L)) == (*l_1163)) , p_49) && 0xB7L), (**g_1201))), 0x7E4AL))) | p_49) > p_49) , p_49)))) >= 1L) == g_165) >= p_49))), 8UL))))) , (void*)0) != (void*)0))), l_1218)) && 0x492A43BEL);
            for (l_874 = 0; (l_874 != 38); l_874 = safe_add_func_uint8_t_u_u(l_874, 2))
            { /* block id: 584 */
                int32_t *l_1223 = &g_870[6][1];
                if ((safe_add_func_uint32_t_u_u(((*g_911) = (p_49 <= ((void*)0 != l_1223))), (safe_add_func_uint16_t_u_u((**g_404), 65527UL)))))
                { /* block id: 586 */
                    (*p_51) = (0x3E7F593841CFACF4LL == ((*l_1217) = (((*l_1084) ^= g_57) ^ (l_1226 == (void*)0))));
                }
                else
                { /* block id: 590 */
                    int8_t l_1234 = 0L;
                    (*l_1223) = (safe_add_func_uint64_t_u_u(((safe_mul_func_int64_t_s_s((*l_1163), (l_1231 != (((*g_1199) != (void*)0) , &g_1175)))) != 65529UL), (0x41L > l_1234)));
                    return (**g_404);
                }
                g_55 = p_51;
            }
        }
    }
    return (*l_1163);
}


/* ------------------------------------------ */
/* 
 * reads : g_57 g_58 g_60 g_56 g_55 g_119 g_387 g_388 g_404 g_405 g_494 g_441 g_870 g_1082
 * writes: g_57 g_58 g_60 g_56 g_87 g_55 g_119
 */
static int32_t * func_52(const int32_t * p_53, int32_t * p_54)
{ /* block id: 6 */
    int32_t l_65 = (-2L);
    int32_t l_68 = (-10L);
    uint8_t l_70 = 1UL;
    uint16_t *l_75 = &g_60;
    int32_t l_92[6];
    uint64_t *l_169[7] = {(void*)0,&g_119,(void*)0,(void*)0,&g_119,(void*)0,(void*)0};
    int32_t l_200 = 0L;
    uint32_t l_216 = 0x2D187CE4L;
    uint16_t l_292 = 0x8B83L;
    int32_t *l_304[5][3] = {{&l_200,&l_200,&l_200},{&l_92[2],&l_92[2],&l_92[2]},{&l_200,&l_200,&l_200},{&l_92[2],&l_92[2],&l_92[2]},{&l_200,&l_200,&l_200}};
    int32_t l_362 = 5L;
    int16_t l_397 = (-8L);
    int64_t *l_451 = &g_184;
    int64_t **l_450 = &l_451;
    int32_t l_462 = 0xE1B09006L;
    uint32_t l_580[7][8] = {{18446744073709551606UL,18446744073709551606UL,18446744073709551611UL,18446744073709551606UL,18446744073709551606UL,0UL,0x9AADA7B1L,0x792EDA67L},{0x1524A877L,18446744073709551608UL,18446744073709551606UL,18446744073709551611UL,18446744073709551606UL,18446744073709551606UL,18446744073709551611UL,18446744073709551606UL},{0x792EDA67L,0x792EDA67L,18446744073709551606UL,7UL,18446744073709551614UL,1UL,0x9AADA7B1L,0x1524A877L},{18446744073709551606UL,0x9AADA7B1L,18446744073709551611UL,0x792EDA67L,18446744073709551611UL,0x9AADA7B1L,18446744073709551606UL,0x1524A877L},{0x9AADA7B1L,1UL,18446744073709551614UL,7UL,18446744073709551606UL,0x792EDA67L,0x792EDA67L,18446744073709551606UL},{18446744073709551611UL,18446744073709551606UL,18446744073709551606UL,18446744073709551611UL,18446744073709551606UL,18446744073709551608UL,0x1524A877L,0x792EDA67L},{0x9AADA7B1L,0UL,18446744073709551606UL,18446744073709551606UL,18446744073709551611UL,18446744073709551606UL,18446744073709551606UL,0UL}};
    uint16_t **l_626 = &g_405;
    uint16_t ***l_625[10] = {(void*)0,(void*)0,(void*)0,&g_404,(void*)0,(void*)0,(void*)0,(void*)0,&g_404,(void*)0};
    uint32_t l_693 = 0xACB67141L;
    int8_t l_728 = 0xB1L;
    int32_t **l_757 = &l_304[3][2];
    int32_t ***l_756 = &l_757;
    uint32_t l_818 = 0xA8C0E979L;
    uint64_t * const l_864[1][9] = {{&g_865,&g_865,&g_865,&g_865,&g_865,&g_865,&g_865,&g_865,&g_865}};
    uint64_t * const *l_863[4][3] = {{&l_864[0][1],&l_864[0][1],&l_864[0][1]},{(void*)0,(void*)0,(void*)0},{&l_864[0][1],&l_864[0][1],&l_864[0][1]},{(void*)0,(void*)0,(void*)0}};
    int32_t l_866 = 1L;
    int i, j;
    for (i = 0; i < 6; i++)
        l_92[i] = 0L;
lbl_103:
    for (g_57 = 8; (g_57 >= 0); g_57 -= 1)
    { /* block id: 9 */
        int32_t l_67 = 1L;
        int32_t l_69 = (-1L);
        int i;
        for (g_58[3][6] = 8; (g_58[3][6] >= 0); g_58[3][6] -= 1)
        { /* block id: 12 */
            uint16_t *l_59 = &g_60;
            int32_t *l_62 = (void*)0;
            int32_t **l_61 = &l_62;
            int32_t *l_64 = (void*)0;
            int32_t **l_63 = &l_64;
            int32_t *l_66[9] = {&g_56[0],&g_56[0],(void*)0,&g_56[0],&g_56[0],(void*)0,&g_56[0],&g_56[0],(void*)0};
            int i;
            (*l_61) = (((*l_59) = (0x72L < 0x0FL)) , &g_56[g_58[3][6]]);
            (*l_63) = ((*l_61) = p_54);
            for (g_60 = 3; (g_60 <= 8); g_60 += 1)
            { /* block id: 19 */
                int i;
                g_56[g_57] |= 0x7FA8275BL;
            }
            l_70--;
        }
        g_58[1][1] ^= g_56[g_57];
    }
    if ((safe_add_func_uint32_t_u_u((65535UL != (++(*l_75))), 0xC022AD61L)))
    { /* block id: 27 */
        int32_t *l_79[2][7] = {{&l_68,&g_58[2][0],&g_58[2][0],&l_68,&g_58[3][6],&g_57,&g_58[3][6]},{&l_68,&g_58[2][0],&g_58[2][0],&l_68,&g_58[3][6],&g_57,&g_58[3][6]}};
        int32_t **l_78 = &l_79[1][1];
        uint8_t l_89 = 0x3DL;
        uint64_t l_93 = 18446744073709551611UL;
        int i, j;
        (*l_78) = &g_57;
        for (g_60 = (-19); (g_60 >= 24); g_60 = safe_add_func_uint8_t_u_u(g_60, 2))
        { /* block id: 31 */
            uint16_t *l_86 = &g_87;
            int32_t l_88 = 0x87CC540EL;
            g_58[3][6] = (safe_sub_func_uint32_t_u_u((safe_rshift_func_uint16_t_u_u(((*l_86) = g_58[1][5]), 9)), 0xAFB09233L));
            l_88 = (*g_55);
            (**l_78) &= l_88;
            for (l_65 = 1; (l_65 >= 0); l_65 -= 1)
            { /* block id: 38 */
                int i, j;
                if ((*g_55))
                    break;
            }
        }
        l_89++;
        l_93++;
    }
    else
    { /* block id: 44 */
        uint8_t l_108 = 0x04L;
        uint64_t *l_125 = &g_119;
        int32_t *l_143 = &g_58[3][6];
        int32_t l_202 = (-1L);
        int32_t l_212 = 0xE19D36E7L;
        int32_t l_215 = 0L;
        int64_t *l_248 = &g_184;
        uint32_t l_280 = 0UL;
        int32_t l_287 = 0x5F71A27BL;
        int32_t l_288 = 0x03F426ACL;
        int32_t l_300 = 7L;
        int32_t l_357 = 0x22BAB605L;
        int32_t l_358 = 0x4A32D140L;
        int32_t l_359 = 0x34576F95L;
        int32_t l_360 = 0x7EBC8C07L;
        int32_t l_361[3];
        uint8_t l_369 = 0UL;
        uint32_t *l_385 = &g_344;
        const uint16_t *l_403 = &l_292;
        const uint16_t **l_402 = &l_403;
        int64_t ***l_461 = &l_450;
        int16_t *l_477 = (void*)0;
        int16_t **l_476 = &l_477;
        uint8_t l_500 = 0xFCL;
        const int32_t **l_558 = &g_55;
        int64_t l_654[3];
        uint16_t l_669[4][2] = {{65526UL,65535UL},{65535UL,65526UL},{65535UL,65535UL},{65526UL,65535UL}};
        uint64_t l_732 = 0xBE273387F84CEAF8LL;
        uint32_t *l_774 = &g_431[0];
        const int16_t *l_798 = &g_494[2][2];
        const int16_t **l_797[3][5][2] = {{{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798}},{{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798}},{{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798},{&l_798,&l_798}}};
        const int16_t *** const l_796 = &l_797[2][4][0];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_361[i] = 0L;
        for (i = 0; i < 3; i++)
            l_654[i] = 0x35ECE51340FECEE4LL;
        for (l_65 = 0; (l_65 > 22); l_65 = safe_add_func_uint64_t_u_u(l_65, 3))
        { /* block id: 47 */
            uint32_t l_131[6];
            uint64_t *l_157 = &g_119;
            uint64_t *l_168 = &g_119;
            int32_t l_201 = 1L;
            int32_t l_203 = 1L;
            int32_t l_204 = 1L;
            int32_t l_208 = 0xB1CADDAFL;
            int32_t l_211 = 0x1C3AE444L;
            int i;
            for (i = 0; i < 6; i++)
                l_131[i] = 18446744073709551613UL;
            for (g_60 = 4; (g_60 > 59); ++g_60)
            { /* block id: 50 */
                int64_t l_109 = 0x19BB5C08239865A2LL;
                int64_t l_142[7][8][4] = {{{(-6L),0L,0xBD8262CBF24FAA97LL,1L},{0x15D791A12690506ALL,(-8L),0xA41DF58876CA199CLL,(-9L)},{0L,0xA41DF58876CA199CLL,1L,1L},{1L,1L,(-10L),0L},{0x46B8AB05D55F32A4LL,0x8744AF555DEF731FLL,1L,(-1L)},{(-10L),0L,0xF5DD3F592D988608LL,1L},{0x702682AE03F78152LL,0L,(-1L),(-1L)},{0L,0x8744AF555DEF731FLL,(-9L),0L}},{{(-8L),1L,0x46B8AB05D55F32A4LL,1L},{(-1L),0xA41DF58876CA199CLL,0L,(-9L)},{0x8744AF555DEF731FLL,(-8L),0x8744AF555DEF731FLL,1L},{(-9L),0L,0L,(-6L)},{0xF5DD3F592D988608LL,0x702682AE03F78152LL,(-6L),0L},{0x0DF9A620E0AF8641LL,0L,(-6L),0x46B8AB05D55F32A4LL},{0xF5DD3F592D988608LL,0L,0L,(-1L)},{(-9L),0L,0x8744AF555DEF731FLL,0xF5DD3F592D988608LL}},{{0x8744AF555DEF731FLL,0xF5DD3F592D988608LL,0L,(-8L)},{(-1L),0x46B8AB05D55F32A4LL,0x46B8AB05D55F32A4LL,(-1L)},{(-8L),(-1L),(-9L),0xFC415474206E0452LL},{0L,0L,(-1L),0L},{0x702682AE03F78152LL,0xBD8262CBF24FAA97LL,0xF5DD3F592D988608LL,0L},{(-10L),0L,1L,0xFC415474206E0452LL},{0x46B8AB05D55F32A4LL,(-1L),(-10L),(-1L)},{1L,0x46B8AB05D55F32A4LL,1L,(-8L)}},{{0L,0xF5DD3F592D988608LL,0xA41DF58876CA199CLL,0xF5DD3F592D988608LL},{0x15D791A12690506ALL,0L,0xBD8262CBF24FAA97LL,(-1L)},{(-6L),0L,(-1L),0x46B8AB05D55F32A4LL},{(-8L),0L,0L,0L},{(-8L),0x702682AE03F78152LL,(-1L),(-6L)},{(-6L),0L,0xBD8262CBF24FAA97LL,1L},{0x15D791A12690506ALL,(-8L),0xA41DF58876CA199CLL,(-9L)},{0L,0xA41DF58876CA199CLL,1L,1L}},{{1L,1L,(-10L),0L},{0x46B8AB05D55F32A4LL,0x8744AF555DEF731FLL,1L,(-1L)},{(-10L),0L,0xF5DD3F592D988608LL,(-1L)},{0xA41DF58876CA199CLL,(-10L),0L,1L},{(-10L),0L,(-6L),0x702682AE03F78152LL},{(-6L),0xFC415474206E0452LL,(-8L),(-8L)},{0L,(-1L),0xBD8262CBF24FAA97LL,(-6L)},{0L,1L,0L,0xFC415474206E0452LL}},{{(-6L),0L,(-9L),0x46B8AB05D55F32A4LL},{0x0DF9A620E0AF8641LL,0xA41DF58876CA199CLL,(-6L),0L},{0x15D791A12690506ALL,(-9L),(-6L),(-8L)},{0x0DF9A620E0AF8641LL,0x702682AE03F78152LL,(-9L),0L},{(-6L),0x8744AF555DEF731FLL,0L,0x0DF9A620E0AF8641LL},{0L,0x0DF9A620E0AF8641LL,0xBD8262CBF24FAA97LL,0L},{0L,(-8L),(-8L),0L},{(-6L),1L,(-6L),(-1L)}},{{(-10L),0xBD8262CBF24FAA97LL,0L,0x8744AF555DEF731FLL},{0xA41DF58876CA199CLL,1L,0x0DF9A620E0AF8641LL,0x8744AF555DEF731FLL},{0L,0xBD8262CBF24FAA97LL,(-1L),(-1L)},{(-8L),1L,0L,0L},{0xFC415474206E0452LL,(-8L),(-8L),0L},{(-9L),0x0DF9A620E0AF8641LL,(-1L),0x0DF9A620E0AF8641LL},{(-8L),0x8744AF555DEF731FLL,1L,0L},{0x46B8AB05D55F32A4LL,0x702682AE03F78152LL,0L,(-8L)}}};
                int i, j, k;
                for (g_57 = 4; (g_57 >= 0); g_57 -= 1)
                { /* block id: 53 */
                    int32_t l_102 = 0L;
                    if ((safe_sub_func_uint64_t_u_u(((void*)0 != &g_58[3][6]), l_102)))
                    { /* block id: 54 */
                        int i;
                        l_92[g_57] = 0xF5949B8DL;
                        if (g_57)
                            goto lbl_103;
                    }
                    else
                    { /* block id: 57 */
                        int32_t l_112 = 0x2575ED6FL;
                        int32_t *l_113 = &l_112;
                        int32_t **l_114 = &l_113;
                        const int32_t **l_115 = &g_55;
                        (*l_113) = (g_56[0] && (safe_lshift_func_int16_t_s_u((safe_add_func_int8_t_s_s((l_108 ^ (*g_55)), l_109)), (safe_mul_func_uint8_t_u_u(l_112, l_109)))));
                        if ((*g_55))
                            continue;
                        (*l_114) = &l_112;
                        (*l_115) = p_53;
                    }
                    for (g_87 = 24; (g_87 < 48); g_87 = safe_add_func_uint16_t_u_u(g_87, 6))
                    { /* block id: 65 */
                        uint64_t *l_118 = &g_119;
                        int32_t *l_122 = &g_58[3][6];
                        int64_t *l_130[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_130[i] = (void*)0;
                        (*l_122) ^= ((l_102 , l_70) || ((*l_118)++));
                        (*l_122) = 0xE28BE021L;
                        g_56[2] = ((*l_122) |= (-7L));
                        g_56[0] = (safe_mod_func_int64_t_s_s((((l_125 == &g_119) ^ (safe_mod_func_int16_t_s_s((safe_mul_func_int8_t_s_s(((l_131[1] = (g_56[5] ^ ((*l_122) , (0xB94E1185L | 0UL)))) > (safe_rshift_func_uint16_t_u_u(((0x85L ^ (safe_add_func_uint64_t_u_u(((*l_118)--), (((safe_rshift_func_int8_t_s_s((safe_sub_func_uint32_t_u_u(l_102, l_108)), g_60)) , l_142[5][5][2]) != 18446744073709551607UL)))) >= l_102), 11))), 0x2FL)), l_142[0][1][0]))) | l_102), l_108));
                    }
                    return l_143;
                }
            }
            for (l_68 = 0; (l_68 == (-26)); --l_68)
            { /* block id: 80 */
                uint32_t l_163 = 0x5DA96190L;
                int32_t l_167 = 0x08E7B0B9L;
                int32_t l_213 = (-8L);
                int32_t l_214 = 0xDCD9A750L;
                g_57 &= ((*l_143) = (*g_55));
                for (g_87 = 21; (g_87 <= 52); g_87 = safe_add_func_int32_t_s_s(g_87, 9))
                { /* block id: 85 */
                    int32_t l_158 = 0x910C71A7L;
                    int16_t *l_164 = &g_165;
                    int32_t *l_166[4][1];
                    int32_t l_196 = 0x7F2CF57DL;
                    int i, j;
                    for (i = 0; i < 4; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_166[i][j] = &g_57;
                    }
                    for (g_119 = (-8); (g_119 <= 40); g_119 = safe_add_func_uint32_t_u_u(g_119, 2))
                    { /* block id: 88 */
                        int32_t *l_152[1][6] = {{(void*)0,(void*)0,&g_58[2][2],(void*)0,(void*)0,&g_58[2][2]}};
                        int i, j;
                        (*l_143) ^= (safe_add_func_int64_t_s_s((-1L), 0xBF0741CDB462DF2ALL));
                        return l_152[0][3];
                    }
                }
                if ((*l_143))
                    break;
            }
            if ((*g_55))
                break;
        }
    }
    (**l_757) = (safe_rshift_func_uint8_t_u_u((safe_mul_func_uint16_t_u_u((((*g_388) = (((*g_55) & (((**g_387) == (safe_sub_func_uint16_t_u_u(((*l_75) = ((**g_404) , (safe_rshift_func_int16_t_s_u((safe_sub_func_uint32_t_u_u((safe_unary_minus_func_uint8_t_u(((((safe_lshift_func_uint16_t_u_u((*g_405), 11)) , g_494[2][5]) && 0L) > ((safe_mod_func_uint64_t_u_u(((*g_55) , (((l_863[3][1] = &l_169[1]) == (void*)0) , (**l_757))), 0x393124B2D034E443LL)) | l_866)))), g_441)), (**l_757))))), 65535UL))) == (*g_55))) == (**l_757))) , (**l_757)), 1L)), 5));
    return p_54;
}




/* ---------------------------------------- */
//testcase_id 1484153503
int case1484153503(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_19, "g_19", print_hash_value);
    transparent_crc(g_46, "g_46", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_56[i], "g_56[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_57, "g_57", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_58[i][j], "g_58[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_87, "g_87", print_hash_value);
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_165, "g_165", print_hash_value);
    transparent_crc(g_184, "g_184", print_hash_value);
    transparent_crc(g_186, "g_186", print_hash_value);
    transparent_crc(g_267, "g_267", print_hash_value);
    transparent_crc(g_277, "g_277", print_hash_value);
    transparent_crc(g_344, "g_344", print_hash_value);
    transparent_crc(g_375, "g_375", print_hash_value);
    transparent_crc(g_378, "g_378", print_hash_value);
    transparent_crc(g_396, "g_396", print_hash_value);
    transparent_crc(g_399, "g_399", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_431[i], "g_431[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_441, "g_441", print_hash_value);
    transparent_crc(g_453, "g_453", print_hash_value);
    transparent_crc(g_463, "g_463", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
        {
            transparent_crc(g_494[i][j], "g_494[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_518, "g_518", print_hash_value);
    transparent_crc(g_761, "g_761", print_hash_value);
    transparent_crc(g_865, "g_865", print_hash_value);
    transparent_crc(g_867, "g_867", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_870[i][j], "g_870[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 1; j++)
        {
            transparent_crc(g_883[i][j], "g_883[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_896, "g_896", print_hash_value);
    transparent_crc(g_1082, "g_1082", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 7; j++)
        {
            transparent_crc(g_1164[i][j], "g_1164[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1253, "g_1253", print_hash_value);
    transparent_crc(g_1515, "g_1515", print_hash_value);
    transparent_crc(g_1517, "g_1517", print_hash_value);
    transparent_crc(g_1612, "g_1612", print_hash_value);
    transparent_crc(g_1682, "g_1682", print_hash_value);
    transparent_crc(g_1703, "g_1703", print_hash_value);
    transparent_crc(g_1810, "g_1810", print_hash_value);
    transparent_crc(g_1817, "g_1817", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 7; k++)
            {
                transparent_crc(g_1862[i][j][k], "g_1862[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1999[i], "g_1999[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2060, "g_2060", print_hash_value);
    transparent_crc(g_2087, "g_2087", print_hash_value);
    transparent_crc(g_2169, "g_2169", print_hash_value);
    transparent_crc(g_2179, "g_2179", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_2183[i][j], "g_2183[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_2562, "g_2562", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_2693[i][j][k], "g_2693[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_2694, "g_2694", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 677
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 69
breakdown:
   depth: 1, occurrence: 246
   depth: 2, occurrence: 58
   depth: 3, occurrence: 7
   depth: 4, occurrence: 5
   depth: 5, occurrence: 2
   depth: 6, occurrence: 3
   depth: 8, occurrence: 1
   depth: 11, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 1
   depth: 15, occurrence: 1
   depth: 17, occurrence: 2
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 2
   depth: 22, occurrence: 4
   depth: 23, occurrence: 2
   depth: 24, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 2
   depth: 27, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 2
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 33, occurrence: 3
   depth: 34, occurrence: 2
   depth: 35, occurrence: 1
   depth: 37, occurrence: 1
   depth: 38, occurrence: 1
   depth: 40, occurrence: 1
   depth: 43, occurrence: 1
   depth: 52, occurrence: 1
   depth: 69, occurrence: 1

XXX total number of pointers: 517

XXX times a variable address is taken: 1282
XXX times a pointer is dereferenced on RHS: 422
breakdown:
   depth: 1, occurrence: 292
   depth: 2, occurrence: 114
   depth: 3, occurrence: 9
   depth: 4, occurrence: 7
XXX times a pointer is dereferenced on LHS: 350
breakdown:
   depth: 1, occurrence: 296
   depth: 2, occurrence: 42
   depth: 3, occurrence: 9
   depth: 4, occurrence: 3
XXX times a pointer is compared with null: 67
XXX times a pointer is compared with address of another variable: 24
XXX times a pointer is compared with another pointer: 20
XXX times a pointer is qualified to be dereferenced: 8520

XXX max dereference level: 7
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 2018
   level: 2, occurrence: 560
   level: 3, occurrence: 116
   level: 4, occurrence: 126
   level: 5, occurrence: 42
   level: 6, occurrence: 31
   level: 7, occurrence: 1
XXX number of pointers point to pointers: 232
XXX number of pointers point to scalars: 285
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31.1
XXX average alias set size: 1.5

XXX times a non-volatile is read: 2489
XXX times a non-volatile is write: 1165
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 7

XXX stmts: 239
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 25
   depth: 2, occurrence: 41
   depth: 3, occurrence: 34
   depth: 4, occurrence: 46
   depth: 5, occurrence: 63

XXX percentage a fresh-made variable is used: 15.9
XXX percentage an existing variable is used: 84.1
********************* end of statistics **********************/

