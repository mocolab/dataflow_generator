/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      2033708622
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint32_t g_3 = 0x45CDBE8EL;
static int16_t g_7[3] = {0xD581L,0xD581L,0xD581L};
static int32_t g_9 = 0x023ED15DL;
static uint16_t g_55 = 65530UL;
static uint16_t g_105 = 0x3A0CL;
static uint64_t g_114 = 0xAEFBDD559FE4D3D8LL;
static const uint8_t g_116[4] = {0x23L,0x23L,0x23L,0x23L};
static uint8_t g_119 = 0xD6L;
static int32_t g_125 = 0xA06A03D0L;
static int16_t g_127 = (-1L);
static uint8_t g_130 = 0x49L;
static int16_t *g_132[5][10] = {{&g_7[2],&g_7[1],&g_7[1],&g_7[2],&g_7[2],&g_7[0],&g_7[2],&g_7[2],&g_7[1],&g_7[1]},{&g_7[0],&g_7[1],&g_7[0],&g_7[1],&g_7[2],&g_7[2],&g_7[2],&g_7[2],&g_7[1],&g_7[0]},{&g_7[2],&g_7[2],&g_7[2],&g_7[0],&g_7[2],&g_7[2],&g_7[1],&g_7[2],&g_7[2],&g_7[0]},{&g_7[0],&g_7[2],&g_7[0],&g_7[2],&g_7[2],&g_7[0],&g_7[1],&g_7[1],&g_7[0],&g_7[2]},{&g_7[2],&g_7[2],&g_7[2],&g_7[2],&g_7[1],&g_7[0],&g_7[1],&g_7[0],&g_7[1],&g_7[0]}};
static int16_t **g_131[1][3][4] = {{{&g_132[2][3],&g_132[2][3],&g_132[2][3],&g_132[2][3]},{&g_132[2][3],&g_132[2][3],&g_132[2][3],&g_132[2][3]},{&g_132[2][3],&g_132[2][3],&g_132[2][3],&g_132[2][3]}}};
static uint16_t g_136 = 65535UL;
static int8_t g_178[5] = {(-10L),(-10L),(-10L),(-10L),(-10L)};
static uint16_t g_179 = 65526UL;
static uint32_t g_209 = 4294967294UL;
static const uint8_t *g_212 = (void*)0;
static uint32_t g_223 = 4294967292UL;
static int32_t g_234 = 9L;
static int32_t g_235 = 0x6540C610L;
static int64_t g_236[5] = {0xDFD0E00C26BE9DCCLL,0xDFD0E00C26BE9DCCLL,0xDFD0E00C26BE9DCCLL,0xDFD0E00C26BE9DCCLL,0xDFD0E00C26BE9DCCLL};
static int64_t g_239 = 0xB60E2ED7CCDFA98FLL;
static int8_t g_255[7] = {0x8BL,0x8BL,0x8BL,0x8BL,0x8BL,0x8BL,0x8BL};
static int32_t g_259 = 0xBF9C2CA9L;
static int32_t g_264 = 0x9F7B5380L;
static int64_t g_265 = 8L;
static int16_t g_266[1] = {(-1L)};
static int32_t g_267 = 0L;
static int16_t g_268 = 0x627BL;
static uint32_t g_269 = 9UL;
static uint32_t g_278[9][10][2] = {{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}},{{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL},{0xF6503A9DL,0xF6503A9DL}}};
static int32_t *g_290 = &g_125;
static int32_t **g_289 = &g_290;
static uint16_t **g_408 = (void*)0;
static uint16_t ***g_407 = &g_408;
static int32_t *g_429 = &g_125;
static int32_t g_440 = 0L;
static uint8_t g_441 = 0x03L;
static int16_t g_453 = 0x01A7L;
static int32_t g_454 = 0xCE1C01A1L;
static uint16_t g_461 = 0x6D82L;
static int8_t g_469 = 0x22L;
static uint8_t g_470 = 8UL;
static const int32_t g_475[7][2][6] = {{{1L,5L,1L,0xC2EEBBADL,0xC9205413L,0xC9205413L},{0xD18B912BL,1L,1L,0xD18B912BL,5L,0x9A32693AL}},{{0x9A32693AL,0xD18B912BL,0xC9205413L,0xD18B912BL,0x9A32693AL,0xC2EEBBADL},{0xD18B912BL,0x9A32693AL,0xC2EEBBADL,0xC2EEBBADL,0x9A32693AL,0xD18B912BL}},{{1L,0xD18B912BL,5L,0x9A32693AL,5L,0xD18B912BL},{5L,1L,0xC2EEBBADL,0xC9205413L,0xC9205413L,0xC2EEBBADL}},{{5L,5L,0xC9205413L,0x9A32693AL,0x13C404A4L,0x9A32693AL},{1L,5L,1L,0xC2EEBBADL,0xC9205413L,0xC9205413L}},{{0xD18B912BL,1L,1L,0xD18B912BL,5L,0x9A32693AL},{0x9A32693AL,0xD18B912BL,0xC9205413L,0xD18B912BL,0x9A32693AL,0xC2EEBBADL}},{{0xD18B912BL,0x9A32693AL,0xC2EEBBADL,0xC2EEBBADL,0x9A32693AL,0xD18B912BL},{1L,0xD18B912BL,5L,0x9A32693AL,5L,0xD18B912BL}},{{5L,1L,0xC2EEBBADL,0xC9205413L,0xC9205413L,0xC2EEBBADL},{5L,5L,0xC9205413L,0x9A32693AL,0x13C404A4L,0x9A32693AL}}};
static uint8_t g_533 = 251UL;
static int32_t g_544 = 0x3B7070A0L;
static uint16_t g_545[1][4][8] = {{{1UL,65535UL,0x7508L,65535UL,1UL,65535UL,0x7508L,65535UL},{1UL,65535UL,0x7508L,65535UL,1UL,65535UL,0x7508L,65535UL},{1UL,65535UL,0x7508L,65535UL,1UL,65535UL,0x7508L,65535UL},{1UL,65535UL,0x7508L,65535UL,1UL,65535UL,0x7508L,65535UL}}};
static int16_t *g_594 = &g_127;
static int16_t **g_593 = &g_594;
static int8_t g_644 = 2L;
static int32_t g_648 = (-4L);
static int16_t g_649 = 0x51A7L;
static uint64_t g_650 = 0x7A2216A7398B1486LL;
static int32_t g_673 = 0xECF6A51FL;
static int16_t g_674 = (-1L);
static int16_t g_675 = 7L;
static int32_t g_676 = 1L;
static uint8_t g_677 = 247UL;
static int8_t *g_701 = (void*)0;
static int8_t **g_700 = &g_701;
static int8_t ***g_699 = &g_700;
static const int32_t *g_748 = &g_125;
static int16_t **g_761 = &g_594;
static uint64_t g_789[8] = {18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL,1UL,18446744073709551615UL,1UL};
static uint32_t *g_823 = &g_209;
static uint32_t **g_822 = &g_823;
static const int16_t *g_903 = &g_675;
static const int16_t **g_902 = &g_903;
static uint16_t g_910[10][8][3] = {{{0x9208L,0x6449L,65530UL},{0x1071L,0x1071L,1UL},{0xADC2L,65527UL,3UL},{0xBDB9L,6UL,65527UL},{0x3D84L,65528UL,1UL},{0xB2B7L,0xBDB9L,65527UL},{0UL,0UL,3UL},{0xDFF6L,0x328FL,1UL}},{{1UL,0UL,65530UL},{0UL,0x00F9L,65535UL},{65535UL,0x3D84L,0UL},{0x1360L,0x00F9L,0x9231L},{65528UL,65526UL,0UL},{0xADCEL,0x9011L,65535UL},{0x0C10L,1UL,0UL},{6UL,65526UL,65535UL}},{{65528UL,65534UL,0xADC2L},{6UL,0x133DL,0x328FL},{0x0C10L,0x0C10L,65528UL},{0xADCEL,0x2A2EL,6UL},{65528UL,0x74EAL,65535UL},{3UL,0xD559L,1UL},{0x919EL,65528UL,65535UL},{0x93ECL,1UL,6UL}},{{65535UL,65528UL,65528UL},{0xEB6EL,0x0DD3L,0x328FL},{0x438CL,0xD3BEL,0xADC2L},{65528UL,3UL,65535UL},{0xCDCCL,0xD3BEL,0UL},{65526UL,0x0DD3L,65535UL},{0xD3BEL,65528UL,0UL},{0x2A2EL,1UL,0xBDB9L}},{{0xD379L,65528UL,0x2DB8L},{0x9011L,0xD559L,0x00F9L},{0xD379L,0x74EAL,0UL},{0x2A2EL,0x2A2EL,65535UL},{0xD3BEL,0x0C10L,0x029BL},{65526UL,0x133DL,0xB2B7L},{0xCDCCL,65534UL,0x9208L},{65528UL,65526UL,0xB2B7L}},{{0x438CL,1UL,0x029BL},{0xEB6EL,0x9011L,65535UL},{65535UL,65526UL,0UL},{0x93ECL,0xADCEL,0x00F9L},{0x919EL,0xCDCCL,0x2DB8L},{3UL,0xADCEL,0xBDB9L},{65528UL,65526UL,0UL},{0xADCEL,0x9011L,65535UL}},{{0x0C10L,1UL,0UL},{6UL,65526UL,65535UL},{65528UL,65534UL,0xADC2L},{6UL,0x133DL,0x328FL},{0x0C10L,0x0C10L,65528UL},{0xADCEL,0x2A2EL,6UL},{65528UL,0x74EAL,65535UL},{3UL,0xD559L,1UL}},{{0x919EL,65528UL,65535UL},{0x93ECL,1UL,6UL},{65535UL,65528UL,65528UL},{0xEB6EL,0x0DD3L,0x328FL},{0x438CL,0xD3BEL,0xADC2L},{65528UL,3UL,65535UL},{0xCDCCL,0xD3BEL,0UL},{65526UL,0x0DD3L,65535UL}},{{0xD3BEL,65528UL,0UL},{0x2A2EL,1UL,0xBDB9L},{0xD379L,65528UL,0x2DB8L},{0x9011L,0xD559L,0x00F9L},{0xD379L,0x74EAL,0UL},{0x2A2EL,0x2A2EL,65535UL},{0xD3BEL,0x0C10L,0x029BL},{65526UL,0x133DL,0xB2B7L}},{{0xCDCCL,65534UL,0x9208L},{65528UL,65526UL,0xB2B7L},{0x438CL,1UL,0x029BL},{0xEB6EL,0x9011L,0xD559L},{0x3B34L,0x7994L,65528UL},{65535UL,1UL,0xADCEL},{0xC70AL,65533UL,0x7F15L},{65527UL,1UL,65526UL}}};
static uint8_t g_999 = 0x25L;
static int32_t g_1015 = (-3L);
static int32_t g_1058 = 0L;
static uint8_t g_1059 = 0UL;
static int32_t *g_1114 = &g_125;
static uint8_t g_1143 = 0UL;
static uint32_t g_1242 = 0x61691DC2L;
static uint32_t g_1292 = 0xC20E0FCDL;
static const int16_t g_1349 = (-4L);
static const int16_t g_1350 = 1L;
static const int16_t g_1351 = (-1L);
static const int16_t g_1352 = (-8L);
static const int16_t g_1353 = 8L;
static const int16_t g_1354 = 6L;
static const int16_t g_1355 = (-1L);
static const int16_t g_1356 = 0xCDD8L;
static const int16_t g_1357[2] = {(-1L),(-1L)};
static const int16_t g_1358[6][10] = {{(-9L),0x9E74L,0xD8EAL,0xDB26L,(-5L),0x638EL,0xE96AL,6L,0L,0L},{0x9E74L,0xF4D9L,0x638EL,0x47CCL,0x47CCL,0x638EL,0xF4D9L,0x9E74L,0xDB26L,0xE96AL},{(-9L),0xD8EAL,0xDCBAL,0x9E74L,0xED7FL,0x47CCL,0x4B5BL,0xE96AL,0x4B5BL,0x47CCL},{0xA109L,0xED7FL,0xDCBAL,0xED7FL,0xA109L,0xE96AL,0xDB26L,0x9E74L,0xF4D9L,0x638EL},{0xDB26L,(-5L),0x638EL,0xE96AL,6L,0L,0L,6L,0xE96AL,0x638EL},{0xE96AL,0xE96AL,0xD8EAL,0x638EL,0xA109L,0x2187L,6L,0xDCBAL,0xF7C5L,0x47CCL}};
static const int16_t g_1359 = 1L;
static const int16_t g_1362 = 0x99F7L;
static int32_t g_1367[1] = {(-1L)};
static int64_t *g_1408[4] = {&g_236[1],&g_236[1],&g_236[1],&g_236[1]};
static int64_t * const *g_1407 = &g_1408[0];
static int32_t g_1421 = (-8L);
static uint32_t g_1470 = 0x2E30CA0FL;
static int8_t g_1528 = (-3L);
static int16_t g_1555[2] = {0x4F8BL,0x4F8BL};
static uint64_t g_1557 = 0x5AA0AAC4D4A25D6ELL;
static uint16_t g_1628 = 0x49A2L;


/* --- FORWARD DECLARATIONS --- */
static const uint64_t  func_1(void);
static uint16_t  func_16(uint8_t  p_17, uint32_t  p_18, int16_t * p_19, const int64_t  p_20, int64_t  p_21);
static const int16_t ** func_25(int16_t * p_26, int16_t * p_27, int16_t * p_28, const int16_t ** p_29, int8_t  p_30);
static int16_t * func_31(int32_t  p_32, const int16_t * p_33, int16_t * p_34, uint8_t  p_35);
static int16_t * func_38(uint8_t  p_39, uint64_t  p_40);
static int32_t  func_41(int16_t  p_42, int32_t  p_43, int16_t * p_44, int32_t * p_45, int32_t  p_46);
static int16_t * func_58(const uint16_t  p_59, uint8_t  p_60);
static uint8_t  func_62(uint16_t * p_63, int32_t  p_64);
static int16_t  func_66(const uint16_t * p_67, int16_t ** p_68, int32_t * p_69);
static int16_t  func_72(int16_t * p_73, uint16_t * const  p_74, uint8_t  p_75);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_3 g_9 g_7 g_761 g_594 g_470 g_823 g_429 g_125 g_475 g_105 g_235 g_266 g_278 g_127 g_290 g_289 g_236 g_673 g_259 g_903 g_675 g_999 g_789 g_209 g_677 g_1015 g_544 g_822 g_469 g_1059 g_902 g_593 g_264 g_268 g_676 g_130 g_269 g_119 g_1114 g_1421
 * writes: g_7 g_9 g_470 g_209 g_127 g_105 g_235 g_236 g_125 g_3 g_259 g_675 g_290 g_999 g_699 g_544 g_469 g_822 g_1059 g_429 g_650 g_255 g_676 g_130 g_748
 */
static const uint64_t  func_1(void)
{ /* block id: 0 */
    int16_t *l_4 = (void*)0;
    int16_t *l_5 = (void*)0;
    int16_t *l_6 = &g_7[2];
    int32_t *l_8 = &g_9;
    const int16_t l_22 = 0xA329L;
    int32_t l_1096 = (-1L);
    int32_t *l_1097 = &g_676;
    int16_t *l_1103 = &g_7[2];
    int8_t * const *l_1213 = &g_701;
    int8_t * const **l_1212 = &l_1213;
    int8_t * const l_1275 = &g_178[0];
    int32_t l_1374 = (-6L);
    uint8_t *l_1482[1];
    uint8_t **l_1481[2];
    uint8_t **l_1483 = &l_1482[0];
    uint16_t l_1531 = 0xAACAL;
    const uint16_t l_1533 = 65535UL;
    int64_t **l_1584 = &g_1408[3];
    int64_t ***l_1583[6][9] = {{&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584},{(void*)0,&l_1584,&l_1584,(void*)0,&l_1584,&l_1584,&l_1584,&l_1584,(void*)0},{&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584},{&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,(void*)0,&l_1584,&l_1584,&l_1584},{&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,(void*)0,&l_1584,(void*)0,&l_1584},{&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,&l_1584,(void*)0}};
    uint64_t l_1591[3][3][7] = {{{0x422F1293558B36FCLL,0x8875EA54F0045359LL,0xACB504B33BFF396FLL,0xACB504B33BFF396FLL,0x8875EA54F0045359LL,0x422F1293558B36FCLL,0UL},{0UL,3UL,4UL,0x73ADE1FAB89F16AELL,18446744073709551609UL,1UL,0xFC31C72C834E3261LL},{0x422F1293558B36FCLL,0x8875EA54F0045359LL,0xACB504B33BFF396FLL,0xACB504B33BFF396FLL,0x8875EA54F0045359LL,0x422F1293558B36FCLL,0UL}},{{0UL,3UL,4UL,0x73ADE1FAB89F16AELL,18446744073709551609UL,1UL,0xFC31C72C834E3261LL},{0x422F1293558B36FCLL,0x8875EA54F0045359LL,0xACB504B33BFF396FLL,0xACB504B33BFF396FLL,0x8875EA54F0045359LL,0x422F1293558B36FCLL,0UL},{0UL,3UL,4UL,0x73ADE1FAB89F16AELL,18446744073709551609UL,1UL,0xFC31C72C834E3261LL}},{{0x422F1293558B36FCLL,0x8875EA54F0045359LL,0xACB504B33BFF396FLL,0xACB504B33BFF396FLL,0x8875EA54F0045359LL,0x422F1293558B36FCLL,0UL},{0UL,3UL,4UL,0x73ADE1FAB89F16AELL,18446744073709551609UL,1UL,18446744073709551614UL},{0xC14D503B68372882LL,0xACB504B33BFF396FLL,0UL,0UL,0xACB504B33BFF396FLL,0xC14D503B68372882LL,0x3DA0B6217ECF17FELL}}};
    uint32_t l_1607[3][10] = {{0x469A40C1L,0xFCC5BA26L,0xBF5048BEL,4294967295UL,0xCD20F398L,4294967295UL,0xBF5048BEL,0xFCC5BA26L,0x469A40C1L,0x8CE15C31L},{0x469A40C1L,1UL,0x0A812E6BL,9UL,4294967295UL,0x8CE15C31L,0x8CE15C31L,4294967295UL,9UL,0x0A812E6BL},{0x8CE15C31L,0x8CE15C31L,4294967295UL,9UL,0x0A812E6BL,1UL,0x469A40C1L,0x6AD09415L,0x469A40C1L,1UL}};
    uint8_t * const l_1621[5] = {&g_999,&g_999,&g_999,&g_999,&g_999};
    int i, j, k;
    for (i = 0; i < 1; i++)
        l_1482[i] = &g_470;
    for (i = 0; i < 2; i++)
        l_1481[i] = &l_1482[0];
    (*l_8) = (!((*l_6) = g_3));
    (*l_1097) &= (((g_3 , (g_3 > g_3)) & (g_3 <= (*l_8))) && ((safe_add_func_int8_t_s_s((l_4 == (void*)0), ((safe_sub_func_int64_t_s_s((safe_mul_func_uint16_t_u_u(func_16(g_3, g_7[2], l_5, l_22, g_3), l_1096)), g_268)) <= (*l_8)))) == (*l_8)));
    for (g_130 = (-4); (g_130 > 5); g_130 = safe_add_func_uint16_t_u_u(g_130, 3))
    { /* block id: 489 */
        uint32_t l_1100 = 0x71272DCDL;
        int16_t *l_1104 = &g_127;
        int32_t *l_1109 = &g_9;
        int16_t ***l_1134[3];
        int8_t *l_1135 = (void*)0;
        int8_t *l_1136[3];
        int64_t *l_1137[3][6][2] = {{{&g_239,&g_239},{&g_265,&g_236[0]},{&g_236[3],&g_239},{&g_265,&g_265},{&g_239,&g_265},{&g_265,&g_236[1]}},{{&g_265,&g_265},{&g_239,&g_265},{&g_265,&g_239},{&g_236[3],&g_236[0]},{&g_265,&g_239},{&g_239,&g_239}},{{&g_265,&g_236[0]},{&g_236[3],&g_239},{&g_265,&g_265},{&g_239,&g_265},{&g_265,&g_236[1]},{&g_265,&g_265}}};
        uint64_t l_1180 = 0xD61EF6BF545F645ALL;
        uint8_t *l_1343 = &g_1143;
        uint8_t * const *l_1342[7][3][1] = {{{(void*)0},{&l_1343},{&l_1343}},{{(void*)0},{&l_1343},{&l_1343}},{{(void*)0},{&l_1343},{&l_1343}},{{(void*)0},{&l_1343},{&l_1343}},{{(void*)0},{&l_1343},{&l_1343}},{{(void*)0},{&l_1343},{&l_1343}},{{(void*)0},{&l_1343},{&l_1343}}};
        int16_t *l_1344[5][10][5] = {{{&g_7[2],&g_127,(void*)0,&g_649,(void*)0},{&g_7[2],&g_7[2],&g_268,&g_266[0],&g_268},{(void*)0,&g_675,&g_453,&g_127,&g_7[2]},{(void*)0,&g_7[1],&g_649,&g_649,&g_7[1]},{&g_7[1],&g_7[1],(void*)0,&g_7[2],&g_7[2]},{(void*)0,&g_266[0],&g_675,&g_7[0],&g_7[1]},{&g_127,(void*)0,(void*)0,&g_7[2],&g_453},{(void*)0,(void*)0,&g_674,&g_453,&g_266[0]},{&g_7[1],(void*)0,&g_675,&g_266[0],&g_674},{(void*)0,&g_127,&g_649,&g_649,&g_266[0]}},{{(void*)0,&g_268,&g_266[0],&g_674,&g_674},{&g_7[2],&g_453,&g_675,&g_649,&g_7[2]},{&g_7[2],&g_674,&g_7[2],&g_268,&g_7[2]},{(void*)0,(void*)0,(void*)0,&g_675,&g_649},{(void*)0,&g_675,&g_675,&g_674,&g_127},{(void*)0,&g_675,(void*)0,&g_453,&g_7[2]},{&g_127,(void*)0,(void*)0,(void*)0,&g_674},{&g_7[1],&g_266[0],&g_453,&g_7[2],&g_649},{(void*)0,&g_674,&g_649,(void*)0,&g_675},{(void*)0,&g_649,(void*)0,(void*)0,(void*)0}},{{&g_7[2],(void*)0,&g_674,&g_268,(void*)0},{&g_268,&g_649,&g_453,&g_266[0],&g_7[1]},{&g_266[0],&g_674,(void*)0,&g_453,&g_266[0]},{&g_674,&g_453,&g_266[0],&g_268,&g_268},{&g_649,(void*)0,&g_266[0],(void*)0,&g_7[2]},{&g_268,&g_266[0],&g_675,(void*)0,&g_266[0]},{&g_453,&g_453,&g_674,(void*)0,(void*)0},{&g_649,&g_453,&g_649,&g_649,&g_453},{&g_266[0],(void*)0,&g_266[0],&g_453,&g_266[0]},{&g_649,&g_127,(void*)0,(void*)0,&g_7[2]}},{{&g_268,&g_7[2],&g_266[0],&g_266[0],&g_266[0]},{&g_649,&g_268,&g_649,(void*)0,&g_453},{(void*)0,&g_7[2],&g_674,&g_268,&g_7[2]},{&g_268,(void*)0,&g_675,&g_127,(void*)0},{&g_268,&g_266[0],&g_266[0],&g_453,&g_453},{&g_266[0],&g_453,&g_266[0],&g_649,&g_7[1]},{&g_268,&g_7[1],(void*)0,&g_266[0],&g_268},{&g_7[2],&g_268,&g_453,(void*)0,&g_266[0]},{&g_649,&g_453,&g_674,(void*)0,&g_266[0]},{&g_675,&g_266[0],(void*)0,(void*)0,&g_674}},{{&g_649,&g_266[0],&g_649,&g_453,&g_266[0]},{&g_674,&g_674,&g_453,&g_453,(void*)0},{&g_7[2],&g_127,(void*)0,(void*)0,&g_127},{&g_268,&g_268,&g_674,(void*)0,&g_266[0]},{&g_649,&g_674,&g_675,(void*)0,(void*)0},{(void*)0,&g_268,&g_268,&g_266[0],&g_7[2]},{&g_7[2],(void*)0,&g_266[0],&g_649,(void*)0},{&g_127,&g_7[2],(void*)0,&g_453,&g_7[1]},{&g_7[2],&g_266[0],(void*)0,&g_127,&g_266[0]},{&g_268,&g_266[0],(void*)0,&g_268,(void*)0}}};
        int32_t l_1373 = 0x0FA8EDAEL;
        int32_t l_1375 = 8L;
        int32_t l_1376 = 0xED12CD32L;
        int8_t ** const * const l_1404 = &g_700;
        int32_t l_1405 = 1L;
        int64_t **l_1416 = &g_1408[0];
        int64_t ***l_1415 = &l_1416;
        uint32_t ***l_1455 = &g_822;
        uint32_t l_1495 = 1UL;
        int64_t l_1550 = (-1L);
        int16_t l_1590[8] = {0xAE5EL,0xD91FL,0xAE5EL,0xD91FL,0xAE5EL,0xD91FL,0xAE5EL,0xD91FL};
        int64_t **l_1594 = &g_1408[2];
        int i, j, k;
        for (i = 0; i < 3; i++)
            l_1134[i] = &g_761;
        for (i = 0; i < 3; i++)
            l_1136[i] = &g_469;
        (*l_1097) ^= (l_1100 == (safe_div_func_int8_t_s_s((((g_266[0] && (l_1103 != l_1104)) & (((safe_add_func_int8_t_s_s((((((((*g_823) = 0x4A240D44L) ^ (((((safe_rshift_func_uint16_t_u_u(g_269, 5)) , l_1097) != l_1109) >= (safe_mul_func_uint8_t_u_u(((safe_mul_func_uint8_t_u_u(((*g_429) , (*l_8)), 0x34L)) < g_119), 1L))) != 0x86C011BBL)) , 0L) , 0xC3BC3874L) >= 0x74A39B03L) , 0L), (*l_1109))) ^ (*l_8)) != (*g_594))) && (*l_1109)), (*l_1109))));
        (*g_289) = g_1114;
    }
    for (g_676 = (-10); (g_676 > 19); ++g_676)
    { /* block id: 687 */
        const int32_t **l_1650[7][9] = {{&g_748,(void*)0,&g_748,&g_748,&g_748,&g_748,&g_748,(void*)0,&g_748},{&g_748,&g_748,&g_748,(void*)0,&g_748,&g_748,(void*)0,&g_748,(void*)0},{&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748},{&g_748,(void*)0,&g_748,(void*)0,&g_748,&g_748,&g_748,&g_748,(void*)0},{&g_748,(void*)0,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748},{&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748},{&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748,&g_748}};
        int i, j;
        for (g_544 = 6; (g_544 < (-1)); g_544--)
        { /* block id: 690 */
            l_8 = &g_9;
        }
        g_748 = &g_475[3][0][1];
        return g_1421;
    }
    return (*l_8);
}


/* ------------------------------------------ */
/* 
 * reads : g_7 g_761 g_594 g_470 g_823 g_429 g_125 g_475 g_105 g_235 g_266 g_278 g_127 g_290 g_289 g_236 g_3 g_673 g_259 g_903 g_675 g_999 g_789 g_209 g_677 g_1015 g_544 g_822 g_469 g_1059 g_902 g_593 g_264
 * writes: g_470 g_209 g_127 g_105 g_235 g_236 g_125 g_3 g_259 g_675 g_290 g_999 g_699 g_544 g_469 g_822 g_1059 g_429 g_650 g_255
 */
static uint16_t  func_16(uint8_t  p_17, uint32_t  p_18, int16_t * p_19, const int64_t  p_20, int64_t  p_21)
{ /* block id: 3 */
    const int16_t *l_37 = &g_7[2];
    int32_t l_65 = 0x6422F8DCL;
    int16_t *l_901 = &g_649;
    uint8_t *l_920 = &g_470;
    int32_t * const * const l_943 = &g_429;
    int32_t * const * const *l_942 = &l_943;
    uint16_t l_972 = 65529UL;
    int32_t l_996[2];
    uint16_t ** const l_1082 = (void*)0;
    int32_t *l_1092 = &l_996[0];
    uint64_t *l_1093 = &g_650;
    int8_t *l_1094[10] = {&g_178[4],(void*)0,&g_178[4],&g_178[4],(void*)0,&g_178[4],&g_178[4],(void*)0,&g_178[4],&g_178[4]};
    const int32_t l_1095 = 0L;
    int i;
    for (i = 0; i < 2; i++)
        l_996[i] = 0xA9F4C0A4L;
    for (p_21 = 0; (p_21 == 12); p_21 = safe_add_func_uint32_t_u_u(p_21, 3))
    { /* block id: 6 */
        uint32_t l_36 = 4294967295UL;
        uint16_t *l_53 = (void*)0;
        uint16_t *l_54[4] = {&g_55,&g_55,&g_55,&g_55};
        const int8_t l_61 = 0xD6L;
        int16_t **l_275 = &g_132[3][6];
        const int16_t ***l_919 = &g_902;
        int i;
        if (g_7[0])
            break;
    }
    if ((((*l_920) ^= (l_37 == (*g_761))) | (l_65 = l_65)))
    { /* block id: 396 */
        int8_t l_933[7][2][10] = {{{1L,0L,0x78L,0L,1L,1L,0x52L,0xDDL,0x78L,(-1L)},{0xCDL,0xD8L,0x78L,1L,0x6CL,0xDDL,0x10L,(-5L),0xDDL,0x0CL}},{{(-1L),0xF8L,0xD1L,(-1L),0xD9L,0x57L,1L,0x20L,0x60L,0x20L},{0xF8L,1L,0x1FL,0x20L,0x1FL,1L,0xF8L,0x85L,0x0CL,0xD8L}},{{0x85L,0L,(-8L),(-1L),0xF8L,0xCDL,(-1L),0xD9L,0xDDL,0x85L},{(-5L),0L,0x60L,0x52L,0x20L,0x3FL,0xF8L,0xD8L,0x81L,0x52L}},{{0x52L,1L,1L,0L,0x78L,0L,1L,0x1FL,0x1FL,1L},{0x78L,0xF8L,0L,0L,0xF8L,0x78L,0x10L,0xDCL,(-1L),1L}},{{0x37L,0xD8L,0x05L,0xD9L,0xDCL,0x3AL,0x52L,0xD8L,(-1L),0x20L},{0x37L,0L,0x0CL,(-5L),(-8L),0x78L,0x20L,(-8L),0x10L,0x6CL}},{{0x78L,0x20L,(-8L),0x10L,0x6CL,0L,1L,1L,1L,0L},{0x52L,0xD9L,0xA0L,0xD9L,0x52L,0x3FL,0x37L,0x20L,0L,(-1L)}},{{(-5L),0x78L,0xCDL,(-1L),0L,0xCDL,0x20L,0x10L,0x1FL,(-1L)},{0x85L,(-1L),0x78L,0xDDL,0x52L,1L,1L,1L,0x3AL,0xD1L}}};
        int32_t l_941 = (-1L);
        int32_t l_944 = 0xA21B14CCL;
        int16_t ***l_966 = &g_131[0][0][2];
        uint64_t *l_1009 = &g_789[1];
        int8_t ***l_1012 = &g_700;
        uint8_t *l_1019 = &g_470;
        int32_t **l_1056[6];
        int32_t **l_1057 = &g_290;
        int32_t **l_1062 = &g_429;
        int i, j, k;
        for (i = 0; i < 6; i++)
            l_1056[i] = &g_290;
        if ((l_944 ^= ((safe_mod_func_uint64_t_u_u((((*g_823) = p_18) <= (safe_lshift_func_uint8_t_u_u((safe_lshift_func_uint8_t_u_s(((*l_920) = (safe_add_func_int16_t_s_s((((safe_mul_func_int16_t_s_s(((safe_lshift_func_uint16_t_u_u(65535UL, (18446744073709551606UL <= l_933[3][1][3]))) , (p_17 != (((p_21 , p_21) , (((0xB6C96CFF190E7F86LL ^ (((l_941 = (safe_add_func_int32_t_s_s((safe_unary_minus_func_uint16_t_u((((safe_lshift_func_int16_t_s_u(((((*g_594) = (-1L)) != l_933[5][0][8]) && 0x7DB40ABEL), l_933[3][1][3])) , p_20) && p_20))), p_17))) , (void*)0) == l_942)) != (**l_943)) > (-2L))) >= p_20))), p_21)) > l_933[3][0][0]) != p_18), 0xA6B0L))), 3)), l_933[1][0][0]))), g_475[3][0][1])) | 0x57D9L)))
        { /* block id: 402 */
            int16_t ***l_967 = &g_131[0][2][3];
            int32_t l_968[6] = {0x83AEAE30L,0x83AEAE30L,(-10L),0x83AEAE30L,0x83AEAE30L,(-10L)};
            int8_t * const *l_990 = &g_701;
            int8_t * const **l_989 = &l_990;
            uint32_t l_1002 = 0x0602B967L;
            int i;
            for (g_105 = 0; (g_105 <= 0); g_105 += 1)
            { /* block id: 405 */
                int8_t * const **l_971[2][3];
                int8_t * const ***l_970 = &l_971[1][2];
                int32_t l_973 = (-1L);
                int i, j;
                for (i = 0; i < 2; i++)
                {
                    for (j = 0; j < 3; j++)
                        l_971[i][j] = (void*)0;
                }
                for (g_235 = 0; (g_235 >= 0); g_235 -= 1)
                { /* block id: 408 */
                    int64_t *l_969 = &g_236[1];
                    int i;
                    (**l_943) = ((((g_266[g_235] != ((*g_594) = ((safe_mod_func_uint16_t_u_u((safe_mul_func_uint8_t_u_u((~(g_266[0] , ((*g_823) = (((((safe_add_func_int8_t_s_s((((safe_sub_func_int32_t_s_s((((safe_div_func_int64_t_s_s(((*l_969) &= ((((safe_sub_func_int8_t_s_s(((((*g_429) > ((~(safe_lshift_func_uint8_t_u_s(((void*)0 == &g_132[2][3]), (~((18446744073709551609UL == g_278[0][6][0]) == ((((((safe_mul_func_int8_t_s_s(((((safe_sub_func_uint32_t_u_u((((*g_594) , l_966) == l_967), (**l_943))) > (*g_290)) , (*l_943)) != (*g_289)), (**l_943))) < (-1L)) , 1L) , l_968[5]) != 4UL) >= p_17)))))) > p_21)) >= g_266[g_235]) || l_941), p_20)) & (-6L)) , p_17) , 0x444B7D2E5C363280LL)), g_266[g_235])) , (void*)0) == l_970), p_18)) & 1L) == l_972), l_973)) == g_266[0]) & 0x494832DE92669753LL) , 1UL) | p_21)))), p_20)), p_17)) , (-1L)))) , g_3) ^ p_21) & g_673);
                }
            }
            for (g_3 = 0; (g_3 > 34); ++g_3)
            { /* block id: 417 */
                int32_t *l_976 = &g_259;
                int32_t l_997 = 0x7D80D376L;
                (*l_976) ^= ((**g_289) = 0xCA1605AFL);
                for (g_675 = (-15); (g_675 <= (-1)); g_675 = safe_add_func_uint32_t_u_u(g_675, 7))
                { /* block id: 422 */
                    int64_t l_991 = 0x1A91247BA2A5B908LL;
                    int32_t l_994 = (-1L);
                    int32_t l_995 = 0xFC3B21B8L;
                    int32_t l_998 = 4L;
                    int32_t *l_1004 = &l_941;
                    (*g_289) = &l_944;
                    if (((l_968[1] == ((safe_div_func_uint16_t_u_u((safe_add_func_int8_t_s_s((((-1L) ^ ((p_21 <= (safe_rshift_func_uint8_t_u_u((p_21 & l_968[5]), 0))) & ((safe_mod_func_uint64_t_u_u((((*g_903) , &g_131[0][1][3]) == ((l_989 == (((0x25A62ED38C4780A7LL != p_21) <= p_18) , &g_700)) , &g_131[0][2][3])), (-5L))) & p_21))) != (***l_942)), 0xFBL)), l_991)) >= p_21)) , 0xF71D27F0L))
                    { /* block id: 424 */
                        int32_t *l_992 = &g_9;
                        int32_t *l_993[10][10][2];
                        int i, j, k;
                        for (i = 0; i < 10; i++)
                        {
                            for (j = 0; j < 10; j++)
                            {
                                for (k = 0; k < 2; k++)
                                    l_993[i][j][k] = (void*)0;
                            }
                        }
                        g_999--;
                    }
                    else
                    { /* block id: 426 */
                        (*g_289) = (*g_289);
                        return l_1002;
                    }
                    for (l_65 = 6; (l_65 >= 2); l_65 -= 1)
                    { /* block id: 432 */
                        int32_t **l_1003[4][6][4] = {{{&l_976,(void*)0,&l_976,&g_429},{&l_976,&l_976,&l_976,(void*)0},{(void*)0,&g_429,(void*)0,(void*)0},{&l_976,&l_976,&l_976,&g_429},{&g_429,(void*)0,&l_976,(void*)0},{&l_976,&l_976,(void*)0,&l_976}},{{(void*)0,&l_976,&l_976,(void*)0},{&l_976,(void*)0,&l_976,&g_429},{&l_976,&l_976,&l_976,(void*)0},{(void*)0,&g_429,(void*)0,(void*)0},{&l_976,&l_976,&l_976,&g_429},{&g_429,(void*)0,&l_976,(void*)0}},{{&l_976,&l_976,(void*)0,&l_976},{(void*)0,&l_976,&l_976,(void*)0},{&l_976,(void*)0,&l_976,&g_429},{&l_976,&l_976,&l_976,(void*)0},{(void*)0,&g_429,(void*)0,(void*)0},{&l_976,&l_976,&l_976,&g_429}},{{&g_429,(void*)0,&l_976,(void*)0},{&l_976,&l_976,(void*)0,&l_976},{(void*)0,&l_976,&l_976,(void*)0},{&l_976,(void*)0,&l_976,&g_429},{&l_976,&l_976,&l_976,(void*)0},{(void*)0,&g_429,(void*)0,(void*)0}}};
                        int i, j, k;
                        (**g_289) &= 0x2A4EA115L;
                        if (g_789[(l_65 + 1)])
                            continue;
                        l_1004 = (**l_942);
                        if (l_933[6][1][8])
                            break;
                    }
                    if ((**l_943))
                        continue;
                }
            }
        }
        else
        { /* block id: 441 */
            uint64_t *l_1008 = (void*)0;
            int64_t *l_1011 = &g_236[3];
            int8_t ****l_1013 = (void*)0;
            int8_t ****l_1014 = &g_699;
            const int32_t l_1042 = (-3L);
            if (((((*g_823) , ((g_673 == ((safe_unary_minus_func_int64_t_s(((*l_1011) ^= (safe_add_func_int8_t_s_s((((&g_789[5] != (l_1008 = (l_1009 = l_1008))) | l_941) , (**l_943)), (safe_unary_minus_func_uint64_t_u(g_677))))))) , (((*l_1014) = l_1012) != (p_20 , (void*)0)))) , p_17)) , (*g_290)) != g_1015))
            { /* block id: 446 */
                uint8_t l_1025 = 0xE3L;
                int32_t * const l_1044 = (void*)0;
                int32_t **l_1045[10];
                int32_t **l_1046 = &g_290;
                int i;
                for (i = 0; i < 10; i++)
                    l_1045[i] = &g_290;
                for (g_544 = 0; (g_544 >= (-26)); g_544--)
                { /* block id: 449 */
                    uint8_t **l_1020 = &l_920;
                    uint16_t *l_1043 = &l_972;
                    g_259 |= (~(((**g_761) = ((&p_17 == ((*l_1020) = l_1019)) == ((safe_lshift_func_uint16_t_u_s((safe_mul_func_uint16_t_u_u((***l_942), l_944)), l_1025)) != (**g_822)))) , ((safe_rshift_func_uint16_t_u_s(((*l_1043) &= (safe_sub_func_uint8_t_u_u((l_1025 >= (safe_add_func_int64_t_s_s((safe_mul_func_uint16_t_u_u((safe_lshift_func_int8_t_s_s((safe_lshift_func_int8_t_s_s((safe_rshift_func_int16_t_s_s((((safe_mod_func_uint64_t_u_u(0x421756519CE6A302LL, 18446744073709551615UL)) >= (**g_761)) != l_1025), 9)), p_17)), 4)), l_1025)), l_1042))), 0xA6L))), l_1042)) || p_17)));
                }
                (*l_1046) = l_1044;
            }
            else
            { /* block id: 456 */
                for (g_469 = 0; (g_469 >= 22); g_469++)
                { /* block id: 459 */
                    uint32_t ***l_1049 = &g_822;
                    int8_t *l_1052 = &l_933[3][1][3];
                    (*l_1049) = &g_823;
                    if (((((((*l_920) = ((safe_lshift_func_uint16_t_u_s((((void*)0 == p_19) , ((((*l_1052) = 0x36L) < (((safe_add_func_uint64_t_u_u(((void*)0 == &g_236[2]), 0x96EF66E467B212E6LL)) > 0x012958BAL) > ((p_17 && l_944) >= (**l_943)))) || (***l_942))), l_944)) || 0xD756L)) || l_1042) , 0xB6L) != p_18) & 1L))
                    { /* block id: 463 */
                        (**g_289) &= 1L;
                        if (p_21)
                            continue;
                    }
                    else
                    { /* block id: 466 */
                        int16_t l_1055 = 0L;
                        (*g_429) &= l_1055;
                        return l_941;
                    }
                }
            }
        }
        (*l_1057) = (*l_943);
        ++g_1059;
        (*l_1062) = (**l_942);
    }
    else
    { /* block id: 476 */
        (**l_943) &= 0xD21913ADL;
    }
    (**l_943) = (((safe_rshift_func_uint16_t_u_s(0xDA2BL, (safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u((~((safe_lshift_func_int8_t_s_u(((safe_add_func_uint8_t_u_u(((***l_942) >= (l_65 = ((safe_mul_func_uint16_t_u_u((((safe_rshift_func_int8_t_s_s((&l_920 == &g_212), (g_255[3] = (((safe_mod_func_int64_t_s_s((**l_943), (safe_sub_func_uint32_t_u_u(((void*)0 != l_1082), (safe_rshift_func_int16_t_s_s((safe_add_func_uint32_t_u_u((((+((*l_1093) = (safe_add_func_int32_t_s_s(p_20, ((*l_1092) = (safe_mul_func_uint8_t_u_u(((*l_920) = (**l_943)), p_17))))))) & (**l_943)) >= (**g_902)), (*g_823))), 15)))))) , 0xFD9FL) > (**g_593))))) & 7UL) >= g_264), (**g_593))) ^ p_18))), p_18)) < 0xFDL), 0)) < 0xBAD96203B46D7359LL)), l_1095)), (**g_761))))) , 1UL) , (**l_943));
    return (**l_943);
}


/* ------------------------------------------ */
/* 
 * reads : g_676 g_910 g_7
 * writes: g_676 g_910 g_125
 */
static const int16_t ** func_25(int16_t * p_26, int16_t * p_27, int16_t * p_28, const int16_t ** p_29, int8_t  p_30)
{ /* block id: 384 */
    int64_t l_908 = 0xB0F5677A845B2C69LL;
    int32_t l_909[8];
    const int16_t **l_918 = &g_903;
    int i;
    for (i = 0; i < 8; i++)
        l_909[i] = 0xB6D43730L;
    for (g_676 = 0; (g_676 <= 2); g_676 += 1)
    { /* block id: 387 */
        int32_t *l_904 = &g_125;
        int32_t l_905 = 0xD6AEEA10L;
        int32_t *l_906 = &g_259;
        int32_t *l_907[7];
        int i;
        for (i = 0; i < 7; i++)
            l_907[i] = &l_905;
        g_910[7][3][2]++;
        (*l_904) = (g_7[g_676] >= (safe_mul_func_int16_t_s_s((+(safe_sub_func_int8_t_s_s(p_30, p_30))), (-5L))));
    }
    return l_918;
}


/* ------------------------------------------ */
/* 
 * reads : g_429 g_761 g_594
 * writes: g_125
 */
static int16_t * func_31(int32_t  p_32, const int16_t * p_33, int16_t * p_34, uint8_t  p_35)
{ /* block id: 380 */
    int32_t **l_899 = (void*)0;
    int32_t *l_900[10];
    int i;
    for (i = 0; i < 10; i++)
        l_900[i] = &g_9;
    (*g_429) = 0xB76EADF5L;
    l_900[2] = &g_9;
    return (*g_761);
}


/* ------------------------------------------ */
/* 
 * reads : g_429 g_125 g_178 g_116 g_475 g_265 g_209 g_594 g_127 g_239 g_119 g_650 g_114 g_179 g_454 g_3 g_677 g_278 g_545 g_470 g_699 g_55 g_461 g_675 g_700 g_701 g_544 g_761 g_136
 * writes: g_125 g_469 g_55 g_650 g_441 g_114 g_179 g_454 g_3 g_677 g_545 g_239 g_470 g_461 g_290 g_748 g_544 g_674 g_136
 */
static int16_t * func_38(uint8_t  p_39, uint64_t  p_40)
{ /* block id: 244 */
    int8_t *l_635 = (void*)0;
    const int32_t *l_637 = &g_475[0][1][0];
    const int32_t **l_636 = &l_637;
    int32_t l_642 = 0x604A0046L;
    int32_t l_645 = 0x71A107FCL;
    int32_t l_646 = 0x2D9CB721L;
    int32_t l_647 = 0x60EBB5FDL;
    int16_t l_656 = 0L;
    int32_t l_721 = 0xBACDF83FL;
    uint16_t *l_726 = &g_461;
    int8_t *** const *l_741 = &g_699;
    int64_t *l_744 = &g_239;
    uint16_t l_745 = 0x4CB9L;
    int16_t **l_763[4];
    uint8_t l_779[7][5] = {{0x67L,0x67L,7UL,0x67L,0x67L},{6UL,255UL,6UL,6UL,255UL},{0x67L,255UL,255UL,0x67L,255UL},{255UL,255UL,255UL,255UL,255UL},{255UL,0x67L,255UL,255UL,0x67L},{255UL,6UL,6UL,255UL,6UL},{0x67L,0x67L,7UL,0x67L,0x67L}};
    int16_t l_805 = 0x4CD6L;
    int32_t *l_898 = (void*)0;
    int i, j;
    for (i = 0; i < 4; i++)
        l_763[i] = &g_594;
lbl_659:
    (*g_429) = 2L;
    for (g_125 = 1; (g_125 >= 0); g_125 -= 1)
    { /* block id: 248 */
        const int32_t ***l_638 = &l_636;
        int8_t *l_639 = &g_469;
        uint16_t *l_640 = &g_55;
        int32_t l_643 = 6L;
        uint8_t *l_655 = &g_441;
        uint64_t *l_657 = &g_650;
        int32_t *l_658 = (void*)0;
        int8_t l_668 = 0xC0L;
        int32_t l_672 = 0x972BED4DL;
        int i;
        if ((safe_mul_func_uint16_t_u_u((g_178[(g_125 + 1)] , (safe_rshift_func_int8_t_s_s((safe_sub_func_uint8_t_u_u(g_178[(g_125 + 1)], (0x6D27BCE3AB0F523ELL >= (safe_lshift_func_uint8_t_u_u((safe_add_func_uint16_t_u_u((249UL || (((((safe_lshift_func_int16_t_s_s((g_178[(g_125 + 1)] && ((safe_rshift_func_uint16_t_u_u(g_178[(g_125 + 1)], ((*l_640) = (safe_lshift_func_int16_t_s_s((l_635 == (void*)0), ((((*l_639) = ((((((*l_638) = l_636) != &l_637) > g_116[2]) == (*l_637)) , g_265)) | g_209) , (*g_594))))))) <= p_39)), p_39)) && p_39) != 0x66L) , (-9L)) <= g_239)), p_40)), 0))))), g_119))), 0x9F5BL)))
        { /* block id: 252 */
            int32_t *l_641[3][5][6] = {{{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0}},{{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,(void*)0,&g_259,(void*)0},{(void*)0,&g_259,(void*)0,&g_125,(void*)0,&g_125},{&g_125,(void*)0,&g_125,&g_125,(void*)0,&g_125}},{{&g_125,(void*)0,&g_125,&g_125,(void*)0,&g_125},{&g_125,(void*)0,&g_125,&g_125,(void*)0,&g_125},{&g_125,(void*)0,&g_125,&g_125,(void*)0,&g_125},{&g_125,(void*)0,&g_125,&g_125,(void*)0,&g_125},{&g_125,(void*)0,&g_125,&g_125,(void*)0,&g_125}}};
            int i, j, k;
            (**l_638) = (**l_638);
            g_650++;
            l_643 = p_40;
        }
        else
        { /* block id: 256 */
            int32_t *l_653 = &l_645;
            (**l_638) = l_653;
        }
        l_643 = (((*l_637) != (~((((*l_655) = 0x72L) != (*l_637)) || ((l_656 , (void*)0) == l_657)))) & (-1L));
        for (g_114 = 0; (g_114 <= 1); g_114 += 1)
        { /* block id: 263 */
            int32_t l_669 = 1L;
            int32_t l_671 = 0x20E6DFE3L;
            if (g_265)
                goto lbl_659;
            for (g_179 = 0; (g_179 <= 1); g_179 += 1)
            { /* block id: 267 */
                int16_t *l_660 = &g_127;
                int32_t l_670[9] = {(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L),(-6L)};
                int i;
                for (g_454 = 0; (g_454 <= 1); g_454 += 1)
                { /* block id: 270 */
                    return l_660;
                }
                for (g_3 = 0; (g_3 <= 1); g_3 += 1)
                { /* block id: 275 */
                    int32_t *l_661 = &g_9;
                    int32_t *l_662 = (void*)0;
                    int32_t *l_663 = &g_259;
                    int32_t *l_664 = &l_642;
                    int32_t *l_665 = &l_646;
                    int32_t *l_666 = &l_643;
                    int32_t *l_667[9] = {&l_647,&l_647,&l_647,&l_647,&l_647,&l_647,&l_647,&l_647,&l_647};
                    int i;
                    g_677++;
                    for (g_650 = 0; (g_650 <= 0); g_650 += 1)
                    { /* block id: 279 */
                        int i, j, k;
                        (*l_666) ^= (((g_278[(g_650 + 2)][(g_650 + 6)][g_114] == ((safe_mod_func_uint64_t_u_u(((((*l_665) , (safe_rshift_func_int8_t_s_s(((safe_div_func_int8_t_s_s((safe_lshift_func_uint16_t_u_u((***l_638), 12)), g_545[0][1][4])) , ((***l_638) < (g_545[g_650][(g_125 + 2)][(g_3 + 4)] ^= ((~g_278[(g_650 + 2)][(g_650 + 6)][g_114]) ^ 0x2B25L)))), 1))) | (1UL > p_39)) != 5L), 0x1A257AFDE1265948LL)) , 0L)) || 0L) && p_40);
                    }
                }
            }
            (**l_638) = (**l_638);
            for (g_3 = 0; (g_3 <= 1); g_3 += 1)
            { /* block id: 288 */
                uint32_t l_696 = 4294967295UL;
                int32_t l_702[3][7] = {{0L,0L,0x2C77157CL,0x4D8FCD97L,0x2C77157CL,0L,0L},{0xCC881572L,0L,(-3L),0L,0xCC881572L,0xCC881572L,0L},{2L,0L,2L,0x2C77157CL,0x2C77157CL,2L,0L}};
                int i, j;
                for (g_239 = 1; (g_239 >= 0); g_239 -= 1)
                { /* block id: 291 */
                    int16_t l_691 = 0x57AEL;
                    int32_t *l_692 = &l_646;
                    int32_t *l_693 = &g_259;
                    int32_t *l_694[4];
                    int64_t l_695 = 0xCD302F186E46DA1BLL;
                    int i, j, k;
                    for (i = 0; i < 4; i++)
                        l_694[i] = &l_672;
                    ++l_696;
                    if (g_278[(g_114 + 6)][g_125][g_114])
                        break;
                    if ((*g_429))
                        continue;
                }
                for (g_470 = 0; (g_470 <= 1); g_470 += 1)
                { /* block id: 298 */
                    int i, j, k;
                    l_702[1][0] ^= (g_278[(g_3 + 2)][(g_125 + 2)][g_114] && ((void*)0 == g_699));
                    l_702[1][0] = (safe_rshift_func_int8_t_s_u(0L, (safe_lshift_func_uint8_t_u_u(0x13L, (safe_div_func_int32_t_s_s((**l_636), (g_55 & ((p_39 ^ (safe_mod_func_int32_t_s_s(((safe_add_func_uint64_t_u_u((p_39 & (0x51D74B83CF42F238LL || (safe_rshift_func_int16_t_s_u((safe_div_func_uint32_t_u_u((safe_add_func_uint64_t_u_u((safe_add_func_uint32_t_u_u(((void*)0 == &l_671), (*g_429))), p_40)), g_114)), l_721)))), 3L)) || l_671), 0xD283F39FL))) < 0x0527E5F1A6FC8AB3LL))))))));
                    l_671 = 3L;
                }
            }
        }
    }
    (*g_429) = (safe_lshift_func_int8_t_s_u((((**l_636) <= (safe_add_func_uint16_t_u_u(((*l_726)++), (**l_636)))) >= (((safe_div_func_int16_t_s_s(((((safe_div_func_int16_t_s_s((&g_648 == ((safe_mod_func_uint32_t_u_u(0x78740965L, 0xF82BD582L)) , &g_264)), (safe_div_func_uint8_t_u_u((safe_lshift_func_int8_t_s_s((safe_add_func_int64_t_s_s((&g_699 == (l_741 = l_741)), ((*l_744) = ((safe_mod_func_int16_t_s_s(1L, p_40)) , g_675)))), 2)), p_40)))) ^ l_745) == (**l_636)) || (-1L)), p_40)) , (void*)0) == (**g_699))), p_39));
    if ((*l_637))
    { /* block id: 310 */
        int32_t * const l_746 = &g_125;
        int32_t **l_747 = &g_290;
        (*l_747) = ((*g_429) , l_746);
    }
    else
    { /* block id: 312 */
        int16_t **l_762[4];
        int32_t l_776 = 9L;
        uint32_t *l_819 = (void*)0;
        uint32_t **l_818 = &l_819;
        int i;
        for (i = 0; i < 4; i++)
            l_762[i] = (void*)0;
        g_748 = (*l_636);
        for (g_544 = 24; (g_544 == (-18)); --g_544)
        { /* block id: 316 */
            uint64_t l_753 = 1UL;
            int32_t l_777 = 5L;
            int32_t l_778 = 1L;
            for (g_674 = 1; (g_674 < 3); g_674++)
            { /* block id: 319 */
                (*g_429) = p_39;
            }
            (*g_429) = (l_753 |= (**l_636));
            l_776 = (((((*l_726) &= (l_777 = (!(((safe_mod_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((g_761 == (l_763[3] = l_762[1])), ((void*)0 != &l_636))), (0x44834A74A583837ALL && (safe_rshift_func_int16_t_s_u(7L, 10))))), (safe_lshift_func_uint8_t_u_s(((safe_add_func_uint32_t_u_u((safe_div_func_uint16_t_u_u(((((safe_add_func_uint16_t_u_u((p_39 >= (safe_sub_func_int16_t_s_s((*l_637), (**l_636)))), 0UL)) || p_40) , p_39) != 0xAE886F88119AB558LL), 1L)), l_776)) != l_753), p_39)))) || 0xC804EEE1L) , (**g_761))))) == l_778) != (**l_636)) | l_779[6][3]);
        }
        for (g_461 = (-2); (g_461 <= 37); g_461 = safe_add_func_uint16_t_u_u(g_461, 3))
        { /* block id: 331 */
            int64_t l_788 = 0xFAA09F318F139465LL;
            int8_t *l_799 = &g_255[3];
            int32_t l_804 = 1L;
            int16_t *l_851[9][4] = {{&g_266[0],&g_674,&g_268,&g_268},{&g_266[0],&g_266[0],&g_266[0],&g_268},{&g_268,&g_674,&g_268,&g_266[0]},{&g_268,&g_266[0],&g_266[0],&g_268},{&g_266[0],&g_266[0],&g_268,&g_266[0]},{&g_266[0],&g_674,&g_268,&g_268},{&g_266[0],&g_266[0],&g_266[0],&g_268},{&g_268,&g_674,&g_268,&g_266[0]},{&g_268,&g_266[0],&g_266[0],&g_268}};
            int16_t *l_852 = &g_266[0];
            const uint8_t l_872 = 0x68L;
            uint16_t **l_877 = &l_726;
            uint16_t **l_881 = &l_726;
            int32_t *l_897 = &g_676;
            int i, j;
            for (g_136 = 0; (g_136 <= 39); g_136 = safe_add_func_uint32_t_u_u(g_136, 2))
            { /* block id: 334 */
                int8_t l_784 = 0xF5L;
                int16_t l_847 = (-2L);
            }
        }
        (*l_636) = (l_898 = &l_646);
    }
    return (*g_761);
}


/* ------------------------------------------ */
/* 
 * reads : g_278 g_116 g_268 g_289 g_119 g_209 g_290 g_125 g_259 g_9 g_266 g_236 g_179 g_178 g_267 g_130 g_3 g_7 g_265 g_239 g_407 g_235 g_441 g_429 g_461 g_470 g_55 g_408 g_533 g_132 g_545 g_269 g_440 g_234 g_127 g_105 g_255 g_223 g_131 g_594 g_453
 * writes: g_278 g_266 g_7 g_9 g_125 g_290 g_209 g_259 g_179 g_267 g_289 g_130 g_3 g_255 g_265 g_119 g_131 g_429 g_441 g_470 g_469 g_239 g_408 g_55 g_114 g_268 g_545 g_533 g_593 g_178
 */
static int32_t  func_41(int16_t  p_42, int32_t  p_43, int16_t * p_44, int32_t * p_45, int32_t  p_46)
{ /* block id: 65 */
    int32_t *l_276 = (void*)0;
    int32_t *l_277[8] = {&g_259,&g_259,&g_259,&g_259,&g_259,&g_259,&g_259,&g_259};
    int16_t *l_296 = &g_266[0];
    uint8_t l_297 = 0UL;
    int16_t *l_298 = &g_7[2];
    int16_t ***l_403 = &g_131[0][0][0];
    uint64_t l_425 = 18446744073709551615UL;
    int8_t l_468 = 3L;
    int32_t l_484 = (-1L);
    uint32_t l_572 = 0x26312D90L;
    int64_t l_599 = 2L;
    int32_t l_616[2][4][8] = {{{0xC8EF2555L,0xB42619A3L,0x48C225DEL,0x1ED51C45L,(-2L),(-2L),0x1ED51C45L,0x48C225DEL},{0xB42619A3L,0xB42619A3L,0x2A4D2811L,0x1ED51C45L,0xF75FBCCBL,(-2L),1L,0x48C225DEL},{0xC8EF2555L,0xB42619A3L,0x48C225DEL,0x1ED51C45L,(-2L),(-2L),0x1ED51C45L,0x48C225DEL},{0xB42619A3L,0xB42619A3L,0x2A4D2811L,0x1ED51C45L,0xF75FBCCBL,(-2L),1L,0x48C225DEL}},{{0xC8EF2555L,0xB42619A3L,0x48C225DEL,0x1ED51C45L,(-2L),(-2L),0x1ED51C45L,0x48C225DEL},{0xB42619A3L,0xB42619A3L,0x2A4D2811L,0x1ED51C45L,0xF75FBCCBL,(-2L),1L,0x48C225DEL},{0xC8EF2555L,0xB42619A3L,0x48C225DEL,0x1ED51C45L,(-2L),(-2L),0x1ED51C45L,0x48C225DEL},{0xB42619A3L,0xB42619A3L,0x2A4D2811L,0x1ED51C45L,0xF75FBCCBL,(-2L),1L,0x48C225DEL}}};
    int i, j, k;
    g_278[0][6][0]--;
    g_9 &= (((g_116[1] , ((safe_lshift_func_uint16_t_u_u((safe_mod_func_int16_t_s_s((*p_44), p_46)), (safe_sub_func_int32_t_s_s((((0UL | (safe_add_func_int8_t_s_s(((void*)0 == g_289), (safe_lshift_func_uint16_t_u_s(g_119, ((*l_298) = (((p_42 , (safe_rshift_func_int16_t_s_s(((*l_296) = (~(*p_44))), (*p_44)))) <= l_297) >= p_43))))))) , p_46) <= g_209), (**g_289))))) < (*p_45))) || (*p_44)) , (-10L));
    if ((*p_45))
    { /* block id: 70 */
        uint32_t l_343[2];
        int32_t l_347 = 1L;
        int i;
        for (i = 0; i < 2; i++)
            l_343[i] = 18446744073709551615UL;
        (*g_290) |= 9L;
        (**g_289) |= (g_259 <= (*p_44));
        for (l_297 = 0; (l_297 <= 0); l_297 += 1)
        { /* block id: 75 */
            uint16_t l_317 = 5UL;
            int32_t l_319[4] = {1L,1L,1L,1L};
            int32_t **l_340[5];
            int i;
            for (i = 0; i < 5; i++)
                l_340[i] = &l_277[7];
            (*g_289) = &p_43;
            for (g_209 = 1; (g_209 <= 4); g_209 += 1)
            { /* block id: 79 */
                uint16_t *l_318[8][1] = {{&g_105},{&g_179},{&g_105},{&g_179},{&g_105},{&g_179},{&g_105},{&g_179}};
                uint32_t l_321 = 0x10920946L;
                int32_t *l_330 = &g_267;
                int32_t **l_339[9] = {&l_276,&l_276,&l_276,&l_276,&l_276,&l_276,&l_276,&l_276,&l_276};
                int32_t ***l_338[4][7][5] = {{{&l_339[1],&l_339[4],(void*)0,&l_339[4],&l_339[4]},{&l_339[4],&l_339[6],&l_339[4],&l_339[4],&l_339[7]},{&l_339[1],&l_339[4],&l_339[4],&l_339[0],&l_339[7]},{&l_339[6],&l_339[1],&l_339[4],&l_339[4],&l_339[4]},{&l_339[4],&l_339[4],&l_339[7],&l_339[0],&l_339[4]},{&l_339[4],(void*)0,&l_339[7],&l_339[4],&l_339[4]},{(void*)0,&l_339[4],&l_339[4],&l_339[4],(void*)0}},{{&l_339[4],(void*)0,&l_339[4],&l_339[0],&l_339[4]},{&l_339[4],&l_339[4],&l_339[4],&l_339[4],(void*)0},{(void*)0,&l_339[1],(void*)0,&l_339[3],&l_339[4]},{&l_339[4],&l_339[4],&l_339[4],&l_339[3],(void*)0},{&l_339[4],&l_339[6],(void*)0,&l_339[4],&l_339[4]},{&l_339[6],&l_339[4],&l_339[4],&l_339[0],&l_339[4]},{&l_339[1],&l_339[4],(void*)0,&l_339[4],&l_339[4]}},{{&l_339[4],&l_339[6],&l_339[4],&l_339[4],&l_339[7]},{&l_339[1],&l_339[4],&l_339[4],&l_339[0],&l_339[7]},{&l_339[6],&l_339[1],&l_339[4],&l_339[4],&l_339[4]},{&l_339[4],&l_339[4],&l_339[7],&l_339[0],&l_339[4]},{&l_339[4],(void*)0,&l_339[7],&l_339[4],&l_339[4]},{(void*)0,&l_339[4],&l_339[4],&l_339[4],(void*)0},{&l_339[4],(void*)0,&l_339[4],&l_339[0],&l_339[4]}},{{&l_339[4],&l_339[4],&l_339[4],&l_339[4],(void*)0},{(void*)0,&l_339[1],(void*)0,&l_339[3],&l_339[4]},{&l_339[4],&l_339[4],&l_339[4],&l_339[3],(void*)0},{&l_339[4],&l_339[6],(void*)0,&l_339[4],&l_339[4]},{&l_339[6],&l_339[4],&l_339[4],&l_339[0],&l_339[4]},{&l_339[1],&l_339[4],(void*)0,&l_339[4],&l_339[4]},{&l_339[4],&l_339[6],&l_339[4],&l_339[4],&l_339[7]}}};
                const int16_t *l_346 = &g_266[0];
                const int16_t **l_345 = &l_346;
                const int16_t ***l_344 = &l_345;
                uint8_t *l_348 = &g_130;
                int i, j, k;
                (**g_289) = g_266[l_297];
                (**g_289) = ((((safe_mod_func_int8_t_s_s((safe_sub_func_uint16_t_u_u(g_236[g_209], (((safe_mod_func_int32_t_s_s((safe_lshift_func_uint8_t_u_u((l_321 &= (safe_rshift_func_uint8_t_u_u((((-1L) == (safe_rshift_func_uint16_t_u_u(((g_236[(l_297 + 1)] , (*p_44)) > (safe_sub_func_uint32_t_u_u(g_236[1], (safe_rshift_func_uint16_t_u_u((l_319[3] = (safe_mod_func_int32_t_s_s(0x2BA8E144L, (l_317 | l_317)))), (g_179 &= (!(l_318[1][0] == (void*)0)))))))), g_178[0]))) <= g_236[g_209]), p_42))), p_42)), 0x3957207AL)) <= p_46) <= g_278[1][0][0]))), p_46)) , g_267) < p_42) != g_178[4]);
                (*g_290) = (safe_mul_func_uint8_t_u_u((0x1068L <= (((safe_add_func_uint64_t_u_u((safe_sub_func_int8_t_s_s(0L, ((*l_348) |= (((*l_330) = ((safe_mul_func_uint16_t_u_u(0xBD3BL, 0L)) < p_42)) , ((*p_45) | (+(safe_div_func_uint32_t_u_u(p_43, (safe_mul_func_int16_t_s_s(((l_347 = (safe_lshift_func_int8_t_s_u(((((l_340[2] = (g_289 = &p_45)) != (((((((safe_add_func_int8_t_s_s(l_319[0], 0xB6L)) || l_343[1]) , l_344) != &g_131[0][2][3]) || l_317) , 1L) , (void*)0)) == g_236[1]) || p_42), p_43))) || (-4L)), (*p_44))))))))))), 0xE64486FD62860D3FLL)) | 5UL) & (-3L))), 0xD7L));
            }
        }
    }
    else
    { /* block id: 93 */
        uint32_t l_365 = 0UL;
        uint8_t l_396 = 0xA8L;
        int16_t ***l_404[6][4][5] = {{{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]},{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]}},{{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]},{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]}},{{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]},{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]}},{{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]},{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]}},{{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]},{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]}},{{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]},{&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3],&g_131[0][2][3]},{&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0],&g_131[0][2][0]}}};
        uint16_t ***l_409[1][3][6] = {{{&g_408,&g_408,&g_408,&g_408,&g_408,&g_408},{&g_408,&g_408,&g_408,&g_408,&g_408,&g_408},{&g_408,&g_408,&g_408,&g_408,&g_408,&g_408}}};
        int32_t l_438 = 0x7934608FL;
        int32_t l_439 = (-5L);
        int32_t l_450 = (-7L);
        int32_t l_451[5][4] = {{0x8029F27BL,1L,1L,0x8029F27BL},{1L,0x8029F27BL,1L,1L},{0x8029F27BL,0x8029F27BL,0xA4364D82L,0x8029F27BL},{0x8029F27BL,1L,1L,0x8029F27BL},{1L,0x8029F27BL,1L,1L}};
        int16_t l_503 = 2L;
        uint8_t *l_506 = &g_119;
        int16_t l_590 = 0x2EDAL;
        int64_t l_596 = 0xDEA5E97BEEA317F9LL;
        int i, j, k;
        for (g_3 = 0; (g_3 != 2); ++g_3)
        { /* block id: 96 */
            uint8_t l_367 = 0x66L;
            uint32_t l_370 = 0x30F51270L;
            uint8_t l_371 = 1UL;
            int32_t l_378[2][1][7];
            uint64_t l_406[10][5] = {{0xFB8FE764DF80EDAALL,0x488AD1CA65634AEDLL,8UL,0x71D8F2B562EA4142LL,0x8808A8C2A39CCFACLL},{18446744073709551609UL,0xFB8FE764DF80EDAALL,0x8808A8C2A39CCFACLL,0xFB8FE764DF80EDAALL,18446744073709551609UL},{18446744073709551608UL,0xF1394130D6F0E65ELL,0x8808A8C2A39CCFACLL,8UL,2UL},{0UL,18446744073709551610UL,8UL,18446744073709551615UL,0x5E713FEC8D1C3F76LL},{0x5E713FEC8D1C3F76LL,18446744073709551609UL,0xFA73403350E35CE9LL,0xF1394130D6F0E65ELL,2UL},{0x8808A8C2A39CCFACLL,18446744073709551615UL,18446744073709551615UL,0x8808A8C2A39CCFACLL,18446744073709551609UL},{2UL,18446744073709551615UL,0x6B1574CE7931884ALL,0x83595F58C2DD40FCLL,0x8808A8C2A39CCFACLL},{0xF1394130D6F0E65ELL,18446744073709551609UL,2UL,18446744073709551614UL,0x71D8F2B562EA4142LL},{0x6B1574CE7931884ALL,18446744073709551610UL,0x488AD1CA65634AEDLL,0x83595F58C2DD40FCLL,0x83595F58C2DD40FCLL},{18446744073709551610UL,0xF1394130D6F0E65ELL,18446744073709551610UL,0x8808A8C2A39CCFACLL,0xF3FFA15C97F00F2ELL}};
            int32_t *l_430 = (void*)0;
            int8_t ** const *l_542 = (void*)0;
            int i, j, k;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 7; k++)
                        l_378[i][j][k] = 0x564142C6L;
                }
            }
            if ((**g_289))
                break;
            for (g_130 = 0; (g_130 < 33); ++g_130)
            { /* block id: 100 */
                int8_t *l_361 = &g_255[3];
                int64_t *l_366 = &g_265;
                int32_t l_368 = 0xDA34F26FL;
                uint8_t *l_369 = &l_297;
                int16_t ***l_405 = (void*)0;
                if ((safe_div_func_uint64_t_u_u(((((safe_lshift_func_int8_t_s_u((((255UL > ((void*)0 != &p_44)) ^ ((*l_369) = ((safe_div_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u(((*l_361) = (-1L)), ((((+((safe_div_func_uint16_t_u_u((((*l_366) = l_365) >= ((l_367 , (l_368 & ((((p_43 , (((0x9505C1A4L >= 0xD8D7B752L) > l_367) >= 1L)) || (*p_44)) , 8UL) >= p_43))) ^ g_236[1])), (*p_44))) < g_178[4])) >= g_268) ^ l_365) >= 0L))), 0x9E0E923B78758525LL)) <= (**g_289)))) , l_370), g_266[0])) > 0x94C0L) != (*p_44)) < l_371), g_7[2])))
                { /* block id: 104 */
                    const uint64_t l_394[3][3] = {{18446744073709551612UL,0x2D893DD203B5EF5ELL,18446744073709551612UL},{18446744073709551612UL,0x2D893DD203B5EF5ELL,18446744073709551612UL},{18446744073709551612UL,0x2D893DD203B5EF5ELL,18446744073709551612UL}};
                    uint16_t l_395 = 0xEDBBL;
                    int i, j;
                    l_396 &= (safe_add_func_uint64_t_u_u((safe_div_func_uint64_t_u_u((247UL || (-10L)), (((safe_rshift_func_int16_t_s_u(((++g_119) , (*p_44)), 13)) && (((safe_unary_minus_func_uint8_t_u(((safe_lshift_func_uint16_t_u_u((safe_div_func_uint8_t_u_u((safe_mul_func_int16_t_s_s((safe_mul_func_uint8_t_u_u((safe_mul_func_int16_t_s_s(0xD780L, (p_42 != (**g_289)))), (safe_sub_func_uint32_t_u_u(0x40461B76L, (((*l_369) ^= ((g_9 | (((*p_45) & p_42) != (-3L))) < 0x2254A7E6BF6D669ELL)) , l_394[1][1]))))), 1L)), p_46)), g_278[0][6][0])) , l_368))) & 65535UL) <= 0x7E2560BBL)) & l_395))), p_42));
                    if ((p_46 ^ ((safe_mul_func_int8_t_s_s(((safe_lshift_func_int16_t_s_s((((safe_mod_func_int16_t_s_s(0x31C4L, ((((l_404[1][2][3] = l_403) == l_405) != ((g_265 || ((*g_289) != (void*)0)) , g_278[8][1][1])) , (p_46 & l_378[0][0][5])))) || 1UL) , (*p_44)), l_406[0][0])) , 5L), g_239)) && l_367)))
                    { /* block id: 109 */
                        if ((*p_45))
                            break;
                        (*g_290) |= 1L;
                        return (*g_290);
                    }
                    else
                    { /* block id: 113 */
                        (*g_290) = ((l_368 <= (g_407 == l_409[0][1][2])) | 0UL);
                        if ((*p_45))
                            continue;
                    }
                    (*l_403) = &p_44;
                }
                else
                { /* block id: 118 */
                    int16_t l_426[3][4] = {{(-3L),(-3L),(-3L),(-3L)},{(-3L),(-3L),(-3L),(-3L)},{(-3L),(-3L),(-3L),(-3L)}};
                    int i, j;
                    if ((safe_mul_func_uint16_t_u_u(65530UL, (l_378[1][0][0] &= (safe_div_func_int8_t_s_s(((safe_add_func_int64_t_s_s((p_43 < (((g_116[1] < ((((g_236[1] >= 0x267AD000L) != 0L) ^ (safe_mod_func_int64_t_s_s((((safe_sub_func_int32_t_s_s(((**g_289) ^= (2UL | (((+(safe_sub_func_int64_t_s_s(g_178[4], ((void*)0 == p_45)))) < l_368) && l_425))), g_235)) != 0L) >= l_368), g_119))) < l_426[2][3])) && (*p_45)) , 0L)), 0xFE8A7A934EFD52D6LL)) != p_46), l_406[2][0]))))))
                    { /* block id: 121 */
                        int64_t l_427 = 7L;
                        int32_t **l_428[9];
                        int i;
                        for (i = 0; i < 9; i++)
                            l_428[i] = &l_277[6];
                        if ((*g_290))
                            break;
                        (*p_45) = (&l_378[1][0][3] != (g_429 = ((*g_289) = (l_427 , (*g_289)))));
                    }
                    else
                    { /* block id: 126 */
                        (*g_289) = &p_43;
                        return (*p_45);
                    }
                    l_430 = (*g_289);
                }
            }
            if ((**g_289))
            { /* block id: 133 */
                uint8_t l_444 = 0UL;
                int32_t l_447 = 0L;
                int32_t l_448 = (-5L);
                int32_t l_449 = 0xFCBFBDB2L;
                int32_t l_452 = 0xE0BB2F9CL;
                uint32_t l_455 = 0x87D9670AL;
                for (g_9 = 0; (g_9 == 28); g_9 = safe_add_func_int64_t_s_s(g_9, 4))
                { /* block id: 136 */
                    uint32_t *l_434 = &g_269;
                    uint32_t **l_433 = &l_434;
                    uint32_t *l_436 = &g_278[2][0][1];
                    uint32_t **l_435 = &l_436;
                    int32_t l_437 = (-1L);
                    if (((((*l_433) = &l_370) == ((*l_435) = &l_370)) , 0xCCE88BC9L))
                    { /* block id: 139 */
                        --g_441;
                        (*g_289) = (*g_289);
                        --l_444;
                    }
                    else
                    { /* block id: 143 */
                        return (*p_45);
                    }
                }
                l_455--;
                (*g_429) |= (l_452 = (~255UL));
            }
            else
            { /* block id: 150 */
                for (g_179 = 14; (g_179 < 28); g_179++)
                { /* block id: 153 */
                    int32_t *l_462 = (void*)0;
                    int32_t l_466 = (-1L);
                    int32_t l_467[3];
                    int8_t *l_479 = &g_255[3];
                    int8_t **l_478[5] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
                    int i;
                    for (i = 0; i < 3; i++)
                        l_467[i] = 0x4E359100L;
                    if ((g_461 > 1L))
                    { /* block id: 154 */
                        int32_t **l_463 = (void*)0;
                        g_429 = ((*g_289) = (l_462 = (*g_289)));
                    }
                    else
                    { /* block id: 158 */
                        return (*p_45);
                    }
                    for (g_119 = (-19); (g_119 > 23); g_119 = safe_add_func_int8_t_s_s(g_119, 1))
                    { /* block id: 163 */
                        const int32_t *l_474 = &g_475[3][0][1];
                        const int32_t **l_473 = &l_474;
                        const int32_t *l_477 = &l_451[4][1];
                        const int32_t **l_476 = &l_477;
                        int8_t ***l_480 = (void*)0;
                        int8_t ***l_481 = &l_478[2];
                        ++g_470;
                        (*l_476) = ((*l_473) = &p_43);
                        (*l_481) = l_478[4];
                        return (*g_290);
                    }
                    (*l_462) |= (*p_45);
                    for (g_469 = 0; (g_469 > 10); ++g_469)
                    { /* block id: 173 */
                        l_484 |= (*g_290);
                        if (l_451[3][1])
                            break;
                        (**g_289) |= ((void*)0 != &l_468);
                    }
                }
                return (**g_289);
            }
            for (g_265 = 1; (g_265 > 11); ++g_265)
            { /* block id: 183 */
                uint8_t *l_491 = (void*)0;
                uint8_t *l_492 = &g_470;
                int32_t l_496 = 0L;
                int64_t *l_500 = &g_239;
                uint16_t *l_502 = &g_55;
                uint16_t **l_501[6] = {&l_502,&l_502,&l_502,&l_502,&l_502,&l_502};
                int16_t * const *l_520 = &g_132[1][7];
                int16_t * const **l_519 = &l_520;
                uint32_t l_538 = 0xCEFBD8A1L;
                int8_t ** const *l_541 = (void*)0;
                int8_t ** const **l_543 = &l_542;
                int i;
                (*g_290) |= (safe_lshift_func_int16_t_s_s((((*l_298) = 5L) , 0x3595L), (((l_503 = (safe_lshift_func_uint8_t_u_u(((((*l_492)--) && l_439) || (((((*l_502) = (p_43 != (((((!(++(*l_492))) > ((~(((*l_500) = ((g_55 & p_43) >= l_396)) >= (l_438 ^ (((*g_407) = l_501[3]) != (void*)0)))) & l_496)) <= 1UL) , l_496) ^ g_116[1]))) && (**g_408)) || (***g_407)) , p_46)), 7))) , p_46) , (*p_44))));
                if ((safe_add_func_int32_t_s_s(((l_506 = (void*)0) != &g_441), 0xB85B302CL)))
                { /* block id: 193 */
                    if ((*g_290))
                        break;
                }
                else
                { /* block id: 195 */
                    uint32_t l_539 = 4294967295UL;
                    for (l_297 = 0; (l_297 <= 0); l_297 += 1)
                    { /* block id: 198 */
                        uint64_t *l_534 = &g_114;
                        uint64_t *l_535 = &l_406[5][0];
                        int32_t l_536 = 0x66832A09L;
                        int8_t *l_537 = &g_255[1];
                        uint16_t *l_540[9][4] = {{&g_136,&g_55,&g_136,&g_461},{&g_179,&g_55,&g_55,&g_179},{&g_55,&g_179,&g_136,(void*)0},{&g_55,&g_136,&g_55,&g_136},{&g_179,(void*)0,&g_136,&g_136},{&g_136,&g_136,&g_461,(void*)0},{(void*)0,&g_179,&g_461,&g_179},{&g_136,&g_55,&g_136,&g_461},{&g_179,&g_55,&g_55,&g_179}};
                        int i, j;
                        if (l_406[(l_297 + 7)][(l_297 + 4)])
                            break;
                        if (l_496)
                            continue;
                        (*p_45) = ((**g_289) = (((safe_mod_func_int16_t_s_s(0x8A97L, 1L)) && ((((safe_mod_func_int64_t_s_s(((safe_lshift_func_uint8_t_u_u((((l_536 = (p_42 = ((+(safe_mod_func_int32_t_s_s(((safe_div_func_uint32_t_u_u(((!(&g_131[0][1][1] == (p_43 , l_519))) & (safe_sub_func_uint32_t_u_u((safe_rshift_func_uint8_t_u_u((safe_rshift_func_int16_t_s_s((((*l_296) = ((((((*l_537) = ((safe_mod_func_int64_t_s_s(((safe_div_func_int16_t_s_s(((***l_519) = (((*l_535) = ((*p_44) == (((((*l_534) = (safe_mul_func_int16_t_s_s((g_533 , 1L), (*p_44)))) > p_46) , p_43) || 0xC2L))) > 1L)), l_370)) != g_125), p_46)) <= l_536)) != p_46) > l_538) && p_42) | g_116[1])) && 0x65F8L), 3)), 7)), 0x83CFDE08L))), g_278[0][6][0])) <= l_536), (*g_429)))) <= 3L))) ^ p_46) & p_46), 5)) > l_539), g_267)) || (*p_44)) , p_44) != l_540[8][0])) , (**g_289)));
                        (*g_289) = (*g_289);
                    }
                    (*g_429) = 0x23CBA5BBL;
                }
                (*p_45) |= (((*l_502) = ((*p_44) == (-1L))) || (l_541 != ((*l_543) = l_542)));
                g_545[0][1][4]--;
            }
        }
        for (g_265 = 23; (g_265 < (-5)); --g_265)
        { /* block id: 222 */
            uint8_t l_552 = 4UL;
            uint8_t *l_573 = &g_533;
            uint32_t l_574 = 0xC19B7028L;
            int32_t ***l_582 = (void*)0;
            int16_t **l_592[8][1][4] = {{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}},{{&g_132[1][6],&g_132[1][6],&g_132[1][6],&g_132[1][6]}}};
            int16_t **l_595 = &g_132[2][9];
            int i, j, k;
            if ((l_503 & ((safe_add_func_uint32_t_u_u(((l_552 >= ((safe_mul_func_uint8_t_u_u((((((safe_div_func_int64_t_s_s((safe_add_func_uint32_t_u_u(g_269, ((safe_rshift_func_int8_t_s_u(g_440, ((safe_div_func_uint16_t_u_u(p_46, (safe_mul_func_int8_t_s_s(((safe_sub_func_uint32_t_u_u((((g_266[0] | (safe_mod_func_int32_t_s_s((l_450 &= ((**g_289) = (l_552 == (!((*l_573) = (safe_mul_func_int8_t_s_s((((l_572 != (*p_45)) < (-2L)) || p_42), 0x36L))))))), (*p_45)))) | g_234) , p_42), l_365)) >= p_43), g_127)))) < (-1L)))) & l_503))), g_105)) > p_46) & l_574) != l_503) < g_255[6]), l_574)) > l_574)) , g_223), 2L)) , (*g_290))))
            { /* block id: 226 */
                int64_t l_577 = 6L;
                uint64_t *l_583 = (void*)0;
                uint64_t *l_584 = &l_425;
                int16_t **l_591 = (void*)0;
                int8_t *l_597 = &g_178[4];
                int8_t *l_598 = &g_469;
                (*p_45) = ((((safe_add_func_uint64_t_u_u(l_577, (l_438 = (g_235 > (safe_sub_func_int64_t_s_s(((((*l_598) = (((((safe_mul_func_uint8_t_u_u(((*l_573) = ((((void*)0 == l_582) & (**g_289)) , ((((*l_584) = p_42) , (**l_403)) != &p_42))), (g_255[0] = ((*l_597) |= ((((safe_lshift_func_int16_t_s_u(((*l_298) &= (((g_593 = (l_592[1][0][1] = ((((safe_mod_func_int16_t_s_s((safe_unary_minus_func_uint8_t_u((p_46 , l_590))), p_42)) ^ 0L) || (*p_44)) , l_591))) != l_595) & p_42)), l_596)) != g_267) == 0x4F481F72L) == p_43))))) , (void*)0) != &p_46) , 0x17A9D417L) , l_577)) != p_46) & l_599), l_577)))))) < p_46) != g_461) == p_42);
            }
            else
            { /* block id: 237 */
                (**g_289) = 0x2966DCFDL;
            }
        }
        (*g_429) = ((*p_44) && ((safe_add_func_uint16_t_u_u(((((safe_lshift_func_uint8_t_u_s((safe_add_func_int8_t_s_s((1UL != (safe_add_func_int32_t_s_s((*g_429), ((*p_44) , 8UL)))), (safe_rshift_func_int16_t_s_u((*p_44), 2)))), ((*p_44) <= (safe_mul_func_uint16_t_u_u((safe_sub_func_uint8_t_u_u(((((safe_mod_func_uint16_t_u_u((((p_43 , p_43) , &p_42) != (**l_403)), 0x9952L)) || 0xD9DED700L) , (*g_594)) , l_616[0][3][0]), g_267)), (*p_44)))))) & (*p_44)) < g_453) != 5L), (*p_44))) < (*p_44)));
    }
    return (*g_429);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int16_t * func_58(const uint16_t  p_59, uint8_t  p_60)
{ /* block id: 62 */
    int16_t *l_274 = &g_268;
    return l_274;
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_7 g_3 g_55 g_114 g_116 g_119 g_125 g_131 g_136 g_132 g_105 g_130 g_178 g_179 g_209 g_127 g_223 g_212 g_269
 * writes: g_9 g_114 g_119 g_125 g_127 g_130 g_136 g_179 g_209 g_212 g_131 g_223 g_255 g_269
 */
static uint8_t  func_62(uint16_t * p_63, int32_t  p_64)
{ /* block id: 9 */
    uint16_t *l_77 = &g_55;
    uint16_t **l_76[8][8][4] = {{{&l_77,&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77,(void*)0},{&l_77,&l_77,(void*)0,&l_77},{(void*)0,&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0,&l_77}},{{&l_77,&l_77,&l_77,(void*)0},{&l_77,(void*)0,&l_77,&l_77},{(void*)0,&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77,(void*)0}},{{(void*)0,(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77,&l_77},{&l_77,(void*)0,&l_77,&l_77},{(void*)0,&l_77,&l_77,&l_77},{(void*)0,(void*)0,&l_77,&l_77},{(void*)0,&l_77,&l_77,(void*)0},{&l_77,(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77,&l_77}},{{&l_77,&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77,(void*)0},{(void*)0,(void*)0,&l_77,(void*)0},{(void*)0,&l_77,&l_77,&l_77},{&l_77,(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77}},{{&l_77,&l_77,&l_77,&l_77},{(void*)0,(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0,&l_77},{&l_77,&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77,&l_77},{&l_77,(void*)0,&l_77,&l_77},{(void*)0,&l_77,(void*)0,&l_77}},{{&l_77,&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0,&l_77},{&l_77,(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77,(void*)0},{&l_77,(void*)0,&l_77,(void*)0},{&l_77,(void*)0,&l_77,&l_77},{&l_77,&l_77,(void*)0,&l_77},{(void*)0,&l_77,&l_77,&l_77}},{{&l_77,(void*)0,(void*)0,&l_77},{(void*)0,&l_77,&l_77,(void*)0},{&l_77,&l_77,&l_77,&l_77},{&l_77,(void*)0,(void*)0,&l_77},{&l_77,&l_77,(void*)0,&l_77},{&l_77,(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77,&l_77}},{{&l_77,(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77},{&l_77,(void*)0,&l_77,(void*)0},{(void*)0,(void*)0,&l_77,(void*)0},{(void*)0,&l_77,&l_77,&l_77},{&l_77,(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77,&l_77}}};
    uint16_t **l_78[3];
    uint16_t **l_79[6] = {&l_77,&l_77,&l_77,&l_77,&l_77,&l_77};
    uint16_t **l_80[8][10][3] = {{{&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0},{&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0},{&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0},{&l_77,&l_77,&l_77},{&l_77,&l_77,(void*)0}},{{&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77},{(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77}},{{&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77},{(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77}},{{&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77},{(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77}},{{&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77},{(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77}},{{&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77},{(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77}},{{&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,&l_77},{(void*)0,(void*)0,(void*)0},{&l_77,&l_77,&l_77},{(void*)0,&l_77,(void*)0},{&l_77,&l_77,&l_77}},{{&l_77,&l_77,(void*)0},{(void*)0,&l_77,&l_77},{&l_77,&l_77,&l_77},{(void*)0,&l_77,&l_77},{&l_77,(void*)0,&l_77},{&l_77,&l_77,(void*)0},{&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77},{&l_77,&l_77,&l_77},{&l_77,(void*)0,&l_77}}};
    uint16_t **l_81 = (void*)0;
    uint16_t **l_82 = &l_77;
    int16_t *l_126 = &g_127;
    int32_t l_128[2][5] = {{0x7A264E93L,(-1L),(-1L),0x7A264E93L,(-1L)},{0x7A264E93L,0x7A264E93L,0xD2503B3CL,0x7A264E93L,0x7A264E93L}};
    uint8_t *l_129 = &g_130;
    int32_t *l_257 = &g_125;
    int32_t *l_258 = &g_125;
    int32_t *l_260 = &g_259;
    int32_t *l_261 = (void*)0;
    int32_t *l_262 = (void*)0;
    int32_t *l_263[6];
    int32_t **l_272 = (void*)0;
    int32_t **l_273 = &l_263[2];
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_78[i] = &l_77;
    for (i = 0; i < 6; i++)
        l_263[i] = (void*)0;
    (*l_257) = (&g_9 == (func_66((((g_9 || ((((*l_126) = func_72(p_63, ((*l_82) = &g_55), g_9)) & (247UL > ((*l_129) = (((p_64 , l_128[0][2]) > p_64) <= l_128[0][2])))) ^ (-1L))) >= (*p_63)) , &g_55), g_131[0][2][3], &l_128[0][2]) , &l_128[0][2]));
    (*l_257) &= g_114;
    --g_269;
    (*l_273) = &l_128[1][3];
    return (*l_258);
}


/* ------------------------------------------ */
/* 
 * reads : g_136 g_3 g_132 g_7 g_114 g_125 g_119 g_105 g_9 g_130 g_178 g_179 g_55 g_209 g_127 g_223 g_212 g_116
 * writes: g_136 g_125 g_130 g_179 g_209 g_9 g_212 g_131 g_114 g_223 g_255 g_119
 */
static int16_t  func_66(const uint16_t * p_67, int16_t ** p_68, int32_t * p_69)
{ /* block id: 22 */
    int32_t *l_133 = &g_125;
    int32_t *l_134 = (void*)0;
    int32_t *l_135[6][10] = {{&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9},{&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9},{&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9},{&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9},{&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9},{&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9,&g_9}};
    int16_t l_174 = 0xD822L;
    uint16_t l_175 = 65530UL;
    int16_t l_188 = 0xE08CL;
    uint16_t *l_202 = &l_175;
    uint16_t **l_201[9];
    uint16_t ***l_200 = &l_201[8];
    int16_t l_207 = 7L;
    int8_t *l_254 = &g_255[3];
    uint8_t *l_256 = &g_119;
    int i, j;
    for (i = 0; i < 9; i++)
        l_201[i] = &l_202;
lbl_243:
    --g_136;
    for (g_125 = 0; (g_125 == (-11)); g_125 = safe_sub_func_uint32_t_u_u(g_125, 4))
    { /* block id: 26 */
        int64_t l_148[9][4][7] = {{{0xEDB36A152264B8EALL,0x61E54E034D058532LL,0x1541C76BB48C356ELL,0xF12A740B190672B4LL,0x0765CA490DA0945ALL,0x6AFD11974AA1647FLL,8L},{0xAF5C048D29CDFBC4LL,(-3L),0x1541C76BB48C356ELL,(-1L),5L,0xDCF4DCFDAD02808DLL,0x39CC89D979A8B53ELL},{0x0765CA490DA0945ALL,0x4857036B52242021LL,1L,0x0E0A5FE3C0FC352BLL,(-6L),0x6AFD11974AA1647FLL,(-1L)},{0xF833B27F04C0B57BLL,0x4857036B52242021LL,(-5L),(-1L),0x954279E96FC21787LL,(-1L),0x9FE8AC17F9EEEF0CLL}},{{0L,(-3L),(-1L),1L,(-6L),2L,(-3L)},{0L,0x61E54E034D058532LL,1L,0x4857036B52242021LL,5L,(-1L),(-1L)},{0xF833B27F04C0B57BLL,0xF12A740B190672B4LL,0x846699B53D0E5225LL,1L,0x0765CA490DA0945ALL,0x7C869DD9846AC962LL,(-1L)},{0x0765CA490DA0945ALL,3L,0x6ACD93D51DFD2E61LL,(-1L),0L,0x9FE8AC17F9EEEF0CLL,(-3L)}},{{0xAF5C048D29CDFBC4LL,(-1L),0x846699B53D0E5225LL,0x0E0A5FE3C0FC352BLL,0x490890113F68E754LL,0x9FE8AC17F9EEEF0CLL,0x9FE8AC17F9EEEF0CLL},{0xEDB36A152264B8EALL,(-1L),1L,(-1L),0xEDB36A152264B8EALL,0x7C869DD9846AC962LL,(-1L)},{0x0C4AEE0EB6C92F55LL,(-1L),(-1L),0xF12A740B190672B4LL,0xEDB36A152264B8EALL,(-1L),0x39CC89D979A8B53ELL},{2L,3L,(-5L),0x09A04A83BE9465D9LL,0x490890113F68E754LL,2L,8L}},{{0x0C4AEE0EB6C92F55LL,0xF12A740B190672B4LL,1L,0x09A04A83BE9465D9LL,0L,(-1L),1L},{0xEDB36A152264B8EALL,0x61E54E034D058532LL,0x1541C76BB48C356ELL,0xF12A740B190672B4LL,0x0765CA490DA0945ALL,0x6AFD11974AA1647FLL,8L},{0xAF5C048D29CDFBC4LL,(-3L),0x1541C76BB48C356ELL,(-1L),5L,0xDCF4DCFDAD02808DLL,0x39CC89D979A8B53ELL},{0x0765CA490DA0945ALL,0x4857036B52242021LL,1L,(-1L),(-4L),(-5L),0x6AA09A3B3DE1EFC3LL}},{{2L,0x92F5982E8761BCA9LL,0x93B096275E0746DCLL,0L,(-1L),0x64128449A3136B17LL,1L},{1L,1L,(-1L),7L,(-4L),0x39F97B59C6DFE233LL,(-4L)},{1L,2L,(-1L),0x92F5982E8761BCA9LL,8L,0x6AA09A3B3DE1EFC3LL,0x64128449A3136B17LL},{2L,0xB2917DE5E0314B3ELL,0x1CB1BBB9F26E7CECLL,7L,0xDCF4DCFDAD02808DLL,0x846699B53D0E5225LL,0x64128449A3136B17LL}},{{0xDCF4DCFDAD02808DLL,0x8654D647035F2876LL,1L,0L,1L,1L,(-4L)},{(-3L),0L,0x1CB1BBB9F26E7CECLL,(-1L),(-1L),1L,1L},{0x6AFD11974AA1647FLL,(-1L),(-1L),(-1L),0x6AFD11974AA1647FLL,0x846699B53D0E5225LL,0x6AA09A3B3DE1EFC3LL},{0x39CC89D979A8B53ELL,0L,(-1L),0xB2917DE5E0314B3ELL,0x6AFD11974AA1647FLL,0x6AA09A3B3DE1EFC3LL,0x6ACD93D51DFD2E61LL}},{{0x9FE8AC17F9EEEF0CLL,0x8654D647035F2876LL,0x93B096275E0746DCLL,0L,(-1L),0x39F97B59C6DFE233LL,0x1541C76BB48C356ELL},{0x39CC89D979A8B53ELL,0xB2917DE5E0314B3ELL,0x5EB517B2DE282C3DLL,0L,1L,0x64128449A3136B17LL,(-1L)},{0x6AFD11974AA1647FLL,2L,0xD04623539A05EB5DLL,0xB2917DE5E0314B3ELL,0xDCF4DCFDAD02808DLL,(-5L),0x1541C76BB48C356ELL},{(-3L),1L,0xD04623539A05EB5DLL,(-1L),8L,0L,0x6ACD93D51DFD2E61LL}},{{0xDCF4DCFDAD02808DLL,0x92F5982E8761BCA9LL,0x5EB517B2DE282C3DLL,(-1L),(-4L),(-5L),0x6AA09A3B3DE1EFC3LL},{2L,0x92F5982E8761BCA9LL,0x93B096275E0746DCLL,0L,(-1L),0x64128449A3136B17LL,1L},{1L,1L,(-1L),7L,(-4L),0x39F97B59C6DFE233LL,(-4L)},{1L,2L,(-1L),0x92F5982E8761BCA9LL,8L,0x6AA09A3B3DE1EFC3LL,0x64128449A3136B17LL}},{{2L,0xB2917DE5E0314B3ELL,0x1CB1BBB9F26E7CECLL,7L,0xDCF4DCFDAD02808DLL,0x846699B53D0E5225LL,0x64128449A3136B17LL},{0xDCF4DCFDAD02808DLL,0x8654D647035F2876LL,1L,0L,1L,1L,(-4L)},{(-3L),0L,0x1CB1BBB9F26E7CECLL,(-1L),(-1L),1L,1L},{0x6AFD11974AA1647FLL,(-1L),(-1L),(-1L),0x6AFD11974AA1647FLL,0x846699B53D0E5225LL,0x6AA09A3B3DE1EFC3LL}}};
        uint8_t *l_176 = (void*)0;
        uint8_t *l_177 = &g_130;
        uint32_t *l_208 = &g_209;
        const uint8_t l_210[1] = {0UL};
        uint32_t l_211 = 18446744073709551615UL;
        int16_t **l_219 = &g_132[2][3];
        int32_t l_222 = 1L;
        uint8_t l_240 = 0xEDL;
        int i, j, k;
        g_179 ^= ((*p_69) |= (~((safe_div_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u((safe_sub_func_int8_t_s_s(l_148[5][1][6], (g_3 , (safe_rshift_func_int16_t_s_u((-1L), (safe_mod_func_int16_t_s_s((safe_mul_func_int8_t_s_s((((*l_177) = (safe_add_func_uint32_t_u_u((((l_148[5][1][6] ^ (((**p_68) <= ((safe_mul_func_int8_t_s_s(l_148[7][2][1], (+(((safe_add_func_int16_t_s_s((((safe_mod_func_int64_t_s_s(l_148[1][2][5], (g_114 , (safe_mod_func_uint16_t_u_u(((safe_sub_func_int64_t_s_s(((safe_sub_func_uint64_t_u_u((safe_add_func_int16_t_s_s(((safe_sub_func_uint8_t_u_u((*l_133), 0x6BL)) ^ l_174), (**p_68))), l_148[2][2][4])) , l_175), (*l_133))) ^ g_119), g_105))))) ^ (**p_68)) & l_148[5][1][6]), 0x80DDL)) < 0xC907L) < (**p_68))))) >= g_9)) | 0x08A32CF9L)) , 5UL) >= 0x05C497654C189411LL), g_130))) & l_148[6][0][4]), l_148[5][1][6])), g_178[4]))))))), (*l_133))) >= 0x4DC6A2FAL), l_148[5][1][6])) , 4L)));
        if ((safe_mod_func_uint16_t_u_u(0xEA29L, (safe_sub_func_uint64_t_u_u((safe_lshift_func_int8_t_s_s((((safe_mul_func_int8_t_s_s((((((l_188 >= (((safe_sub_func_uint64_t_u_u(((((*l_208) |= (0L != (0x8153L && (l_207 = ((*p_67) > (safe_lshift_func_uint8_t_u_s(((*l_177)++), (((!((void*)0 != l_200)) != (safe_rshift_func_int8_t_s_s(l_148[5][1][6], 4))) < ((safe_sub_func_int32_t_s_s(9L, (-1L))) > (-6L)))))))))) < (*p_69)) > g_55), l_210[0])) >= (*l_133)) , (**p_68))) , 0L) != 0x9C605374L) , (void*)0) == &l_201[7]), g_178[4])) | g_178[4]) | l_211), g_9)), g_127)))))
        { /* block id: 33 */
            uint8_t *l_213[3];
            const int32_t l_217 = (-1L);
            int16_t ***l_218[1][6] = {{(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0}};
            int32_t l_237[6] = {0x4EEE4FC8L,0x4EEE4FC8L,0x4EEE4FC8L,0x4EEE4FC8L,0x4EEE4FC8L,0x4EEE4FC8L};
            int i, j;
            for (i = 0; i < 3; i++)
                l_213[i] = &g_130;
            (*p_69) = (g_9 = (g_127 , g_179));
            g_9 &= (&g_132[2][3] != (((((g_212 = &g_119) == (g_130 , l_213[0])) , ((safe_sub_func_int8_t_s_s((safe_unary_minus_func_int16_t_s(0x3125L)), l_210[0])) , l_217)) | (*p_69)) , (g_131[0][2][3] = (l_219 = p_68))));
            for (g_114 = 0; (g_114 != 10); g_114 = safe_add_func_uint16_t_u_u(g_114, 3))
            { /* block id: 42 */
                const int32_t l_230 = 0xE5E4B7D5L;
                int32_t l_233 = 3L;
                int32_t l_238 = 0x55D07911L;
                ++g_223;
                l_222 ^= ((((((l_148[5][1][6] , &g_130) != (void*)0) > (safe_rshift_func_int8_t_s_u((safe_rshift_func_uint8_t_u_s((((*p_69) <= (l_217 == l_230)) | (l_217 | (safe_mod_func_uint16_t_u_u(0x5E7FL, (*l_133))))), 6)), (*g_212)))) >= l_217) <= l_148[5][1][6]) && 255UL);
                l_240--;
                return (**p_68);
            }
        }
        else
        { /* block id: 48 */
            if (l_211)
                goto lbl_243;
        }
    }
    (*p_69) = (*p_69);
    (*p_69) ^= (safe_rshift_func_uint8_t_u_u(((*l_256) |= ((0xC40AL && ((((g_116[1] , (safe_lshift_func_uint16_t_u_s((safe_mod_func_uint16_t_u_u((*p_67), (**p_68))), (**p_68)))) , ((safe_div_func_uint16_t_u_u(0x703BL, 0x6BBEL)) < (g_178[4] | (((*l_254) = ((safe_mod_func_uint32_t_u_u(0xFCA5432BL, (*l_133))) != g_7[2])) ^ 0xACL)))) , (*l_133)) == 4294967295UL)) && g_209)), (*l_133)));
    return (**p_68);
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_7 g_3 g_55 g_114 g_116 g_119 g_125
 * writes: g_9 g_114 g_119 g_125
 */
static int16_t  func_72(int16_t * p_73, uint16_t * const  p_74, uint8_t  p_75)
{ /* block id: 11 */
    uint8_t l_93 = 0x43L;
    uint16_t * const l_104[5] = {&g_105,&g_105,&g_105,&g_105,&g_105};
    uint16_t * const *l_103[3];
    uint16_t * const **l_102 = &l_103[0];
    int32_t *l_110 = (void*)0;
    int32_t *l_111 = (void*)0;
    int32_t *l_112 = &g_9;
    uint8_t *l_113 = &l_93;
    int32_t l_115 = 1L;
    uint32_t l_117 = 0xA093EA4BL;
    uint8_t *l_118 = &g_119;
    int8_t l_120[9][8][3] = {{{0xCDL,0x38L,0xCCL},{0xA3L,(-1L),(-1L)},{0L,0x85L,0xCCL},{(-7L),0x0AL,0xCDL},{(-7L),1L,0xA3L},{0L,0xD9L,0L},{0xA3L,1L,(-7L)},{0xCDL,0x0AL,(-7L)}},{{0xCCL,0x85L,0L},{(-1L),3L,0xD2L},{0x17L,(-1L),3L},{3L,(-1L),0x17L},{0xD2L,3L,(-10L)},{0xB6L,0L,0x17L},{0x4CL,0xCCL,3L},{0x4CL,0xA3L,0xD2L}},{{0xB6L,0L,0xB6L},{0xD2L,0xA3L,0x4CL},{3L,0xCCL,0x4CL},{0x17L,0L,0xB6L},{(-10L),3L,0xD2L},{0x17L,(-1L),3L},{3L,(-1L),0x17L},{0xD2L,3L,(-10L)}},{{0xB6L,0L,0x17L},{0x4CL,0xCCL,3L},{0x4CL,0xA3L,0xD2L},{0xB6L,0L,0xB6L},{0xD2L,0xA3L,0x4CL},{3L,0xCCL,0x4CL},{0x17L,0L,0xB6L},{(-10L),3L,0xD2L}},{{0x17L,(-1L),3L},{3L,(-1L),0x17L},{0xD2L,3L,(-10L)},{0xB6L,0L,0x17L},{0x4CL,0xCCL,3L},{0x4CL,0xA3L,0xD2L},{0xB6L,0L,0xB6L},{0xD2L,0xA3L,0x4CL}},{{3L,0xCCL,0x4CL},{0x17L,0L,0xB6L},{(-10L),3L,0xD2L},{0x17L,(-1L),3L},{3L,(-1L),0x17L},{0xD2L,3L,(-10L)},{0xB6L,0L,0x17L},{0x4CL,0xCCL,3L}},{{0x4CL,0xA3L,0xD2L},{0xB6L,0L,0xB6L},{0xD2L,0xA3L,0x4CL},{3L,0xCCL,0x4CL},{0x17L,0L,0xB6L},{(-10L),3L,0xD2L},{0x17L,(-1L),3L},{3L,(-1L),0x17L}},{{0xD2L,3L,(-10L)},{0xB6L,0L,0x17L},{0x4CL,0xCCL,3L},{0x4CL,0xA3L,0xD2L},{0xB6L,0L,0xB6L},{0xD2L,0xA3L,0x4CL},{3L,0xCCL,0x4CL},{0x17L,0L,0xB6L}},{{(-10L),3L,0xD2L},{0x17L,(-1L),3L},{3L,(-1L),0x17L},{0xD2L,3L,(-10L)},{0xB6L,0L,0x17L},{0x4CL,0xCCL,3L},{0x4CL,0xA3L,0xD2L},{0xB6L,0L,0xB6L}}};
    int16_t l_121 = (-1L);
    int32_t *l_122 = (void*)0;
    int32_t *l_123 = (void*)0;
    int32_t *l_124 = &g_125;
    int i, j, k;
    for (i = 0; i < 3; i++)
        l_103[i] = &l_104[3];
    (*l_124) |= (((g_9 || ((&g_9 == ((safe_div_func_int64_t_s_s(((((p_75 , ((*l_118) |= ((((~(safe_add_func_uint8_t_u_u((g_114 |= ((*l_113) = (p_75 = (safe_add_func_uint16_t_u_u((!(safe_add_func_int64_t_s_s(((l_93 || ((!(safe_unary_minus_func_uint64_t_u(((safe_mul_func_uint16_t_u_u((g_7[1] != (g_3 , ((safe_add_func_uint32_t_u_u((((*l_102) = &p_74) != (void*)0), (((safe_mul_func_int8_t_s_s((((*l_112) = (safe_add_func_uint8_t_u_u((((g_3 | l_93) , p_75) || l_93), 0x37L))) >= g_7[2]), (-5L))) || g_7[2]) & (*p_74)))) != p_75))), g_7[2])) ^ g_7[2])))) ^ 0x6CBFC796L)) > (-1L)), g_3))), (*p_73)))))), l_115))) == (*p_73)) ^ g_116[1]) == l_117))) >= l_120[7][6][1]) && (*p_73)) , g_119), 0x48DF11712D94B84BLL)) , &l_115)) < l_120[7][6][1])) != g_7[2]) || l_121);
    return g_7[2];
}




/* ---------------------------------------- */
//testcase_id 1484147159
int case1484147159(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_3, "g_3", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_7[i], "g_7[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_55, "g_55", print_hash_value);
    transparent_crc(g_105, "g_105", print_hash_value);
    transparent_crc(g_114, "g_114", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_116[i], "g_116[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_119, "g_119", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_127, "g_127", print_hash_value);
    transparent_crc(g_130, "g_130", print_hash_value);
    transparent_crc(g_136, "g_136", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_178[i], "g_178[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_179, "g_179", print_hash_value);
    transparent_crc(g_209, "g_209", print_hash_value);
    transparent_crc(g_223, "g_223", print_hash_value);
    transparent_crc(g_234, "g_234", print_hash_value);
    transparent_crc(g_235, "g_235", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_236[i], "g_236[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_239, "g_239", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_255[i], "g_255[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_259, "g_259", print_hash_value);
    transparent_crc(g_264, "g_264", print_hash_value);
    transparent_crc(g_265, "g_265", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_266[i], "g_266[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_267, "g_267", print_hash_value);
    transparent_crc(g_268, "g_268", print_hash_value);
    transparent_crc(g_269, "g_269", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        for (j = 0; j < 10; j++)
        {
            for (k = 0; k < 2; k++)
            {
                transparent_crc(g_278[i][j][k], "g_278[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_440, "g_440", print_hash_value);
    transparent_crc(g_441, "g_441", print_hash_value);
    transparent_crc(g_453, "g_453", print_hash_value);
    transparent_crc(g_454, "g_454", print_hash_value);
    transparent_crc(g_461, "g_461", print_hash_value);
    transparent_crc(g_469, "g_469", print_hash_value);
    transparent_crc(g_470, "g_470", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 2; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_475[i][j][k], "g_475[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_533, "g_533", print_hash_value);
    transparent_crc(g_544, "g_544", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 4; j++)
        {
            for (k = 0; k < 8; k++)
            {
                transparent_crc(g_545[i][j][k], "g_545[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_644, "g_644", print_hash_value);
    transparent_crc(g_648, "g_648", print_hash_value);
    transparent_crc(g_649, "g_649", print_hash_value);
    transparent_crc(g_650, "g_650", print_hash_value);
    transparent_crc(g_673, "g_673", print_hash_value);
    transparent_crc(g_674, "g_674", print_hash_value);
    transparent_crc(g_675, "g_675", print_hash_value);
    transparent_crc(g_676, "g_676", print_hash_value);
    transparent_crc(g_677, "g_677", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        transparent_crc(g_789[i], "g_789[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 3; k++)
            {
                transparent_crc(g_910[i][j][k], "g_910[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_999, "g_999", print_hash_value);
    transparent_crc(g_1015, "g_1015", print_hash_value);
    transparent_crc(g_1058, "g_1058", print_hash_value);
    transparent_crc(g_1059, "g_1059", print_hash_value);
    transparent_crc(g_1143, "g_1143", print_hash_value);
    transparent_crc(g_1242, "g_1242", print_hash_value);
    transparent_crc(g_1292, "g_1292", print_hash_value);
    transparent_crc(g_1349, "g_1349", print_hash_value);
    transparent_crc(g_1350, "g_1350", print_hash_value);
    transparent_crc(g_1351, "g_1351", print_hash_value);
    transparent_crc(g_1352, "g_1352", print_hash_value);
    transparent_crc(g_1353, "g_1353", print_hash_value);
    transparent_crc(g_1354, "g_1354", print_hash_value);
    transparent_crc(g_1355, "g_1355", print_hash_value);
    transparent_crc(g_1356, "g_1356", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1357[i], "g_1357[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_1358[i][j], "g_1358[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1359, "g_1359", print_hash_value);
    transparent_crc(g_1362, "g_1362", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_1367[i], "g_1367[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1421, "g_1421", print_hash_value);
    transparent_crc(g_1470, "g_1470", print_hash_value);
    transparent_crc(g_1528, "g_1528", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_1555[i], "g_1555[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_1557, "g_1557", print_hash_value);
    transparent_crc(g_1628, "g_1628", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 339
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 47
breakdown:
   depth: 1, occurrence: 195
   depth: 2, occurrence: 48
   depth: 3, occurrence: 3
   depth: 4, occurrence: 3
   depth: 5, occurrence: 3
   depth: 6, occurrence: 1
   depth: 8, occurrence: 1
   depth: 12, occurrence: 1
   depth: 14, occurrence: 1
   depth: 17, occurrence: 5
   depth: 18, occurrence: 1
   depth: 19, occurrence: 1
   depth: 20, occurrence: 1
   depth: 21, occurrence: 2
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 3
   depth: 25, occurrence: 2
   depth: 26, occurrence: 1
   depth: 28, occurrence: 1
   depth: 29, occurrence: 1
   depth: 30, occurrence: 2
   depth: 32, occurrence: 1
   depth: 33, occurrence: 3
   depth: 36, occurrence: 1
   depth: 39, occurrence: 1
   depth: 42, occurrence: 1
   depth: 46, occurrence: 1
   depth: 47, occurrence: 1

XXX total number of pointers: 324

XXX times a variable address is taken: 736
XXX times a pointer is dereferenced on RHS: 333
breakdown:
   depth: 1, occurrence: 251
   depth: 2, occurrence: 75
   depth: 3, occurrence: 7
XXX times a pointer is dereferenced on LHS: 231
breakdown:
   depth: 1, occurrence: 196
   depth: 2, occurrence: 31
   depth: 3, occurrence: 4
XXX times a pointer is compared with null: 30
XXX times a pointer is compared with address of another variable: 5
XXX times a pointer is compared with another pointer: 9
XXX times a pointer is qualified to be dereferenced: 2934

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1031
   level: 2, occurrence: 260
   level: 3, occurrence: 111
   level: 4, occurrence: 1
XXX number of pointers point to pointers: 126
XXX number of pointers point to scalars: 198
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 26.5
XXX average alias set size: 1.38

XXX times a non-volatile is read: 1541
XXX times a non-volatile is write: 671
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 5

XXX stmts: 190
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 36
   depth: 1, occurrence: 29
   depth: 2, occurrence: 27
   depth: 3, occurrence: 30
   depth: 4, occurrence: 30
   depth: 5, occurrence: 38

XXX percentage a fresh-made variable is used: 17.9
XXX percentage an existing variable is used: 82.1
********************* end of statistics **********************/

