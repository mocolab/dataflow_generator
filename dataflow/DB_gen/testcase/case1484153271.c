/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      3418274326
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint16_t g_6 = 1UL;
static int32_t g_18 = 0x4F98CAFAL;
static int32_t *g_17 = &g_18;
static int32_t g_34 = 1L;
static int32_t *g_33 = &g_34;
static int32_t g_60 = 0x5FC0266DL;
static uint32_t g_62 = 4UL;
static uint16_t g_84 = 65527UL;
static int64_t g_115 = (-1L);
static int32_t g_122 = 1L;
static int16_t g_124 = 3L;
static uint8_t g_125 = 1UL;
static int8_t g_139 = 0x1EL;
static int8_t g_144 = 0L;
static uint64_t g_147 = 0x0A132677FA2621F7LL;
static int16_t g_167 = 1L;
static int16_t g_171 = (-2L);
static uint32_t g_205 = 4294967294UL;
static uint16_t g_251 = 65535UL;
static int32_t *g_290[3] = {&g_18,&g_18,&g_18};
static int32_t **g_289[7] = {&g_290[1],&g_290[1],&g_290[1],&g_290[1],&g_290[1],&g_290[1],&g_290[1]};
static int32_t g_342 = 0xD4B91A1CL;
static int32_t g_346 = 8L;
static uint32_t g_377 = 0x065113E6L;
static uint8_t g_407 = 0x53L;
static const int64_t g_435 = 0L;
static uint8_t *g_461[7][5][3] = {{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}},{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}},{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}},{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}},{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}},{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}},{{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407},{&g_407,&g_407,&g_407}}};
static uint8_t **g_460 = &g_461[0][1][1];
static int64_t g_645[4][1][9] = {{{1L,0x505A46B1A80D32DFLL,0x7EE68A2DCF4EA918LL,0x505A46B1A80D32DFLL,1L,0x7EE68A2DCF4EA918LL,(-1L),(-1L),0x7EE68A2DCF4EA918LL}},{{0L,8L,0xF87233032BE8C534LL,8L,0L,0xF87233032BE8C534LL,0L,0L,0xF87233032BE8C534LL}},{{1L,0x505A46B1A80D32DFLL,0x7EE68A2DCF4EA918LL,0x505A46B1A80D32DFLL,1L,0x7EE68A2DCF4EA918LL,(-1L),(-1L),0x7EE68A2DCF4EA918LL}},{{0L,8L,0xF87233032BE8C534LL,8L,0L,0xF87233032BE8C534LL,0L,0L,0xF87233032BE8C534LL}}};
static int64_t g_660 = 0x7D152EA28EFC4CA0LL;
static uint32_t * const g_715 = &g_62;
static uint32_t * const *g_714[5] = {&g_715,&g_715,&g_715,&g_715,&g_715};
static int32_t *g_746 = &g_60;
static int32_t **g_745 = &g_746;
static uint8_t **g_754 = (void*)0;
static int32_t *g_802 = (void*)0;
static int8_t g_815 = 0L;
static int32_t g_826 = (-3L);
static int32_t g_872 = (-1L);
static int16_t g_922 = 0xB0AEL;
static const uint8_t *g_1012 = &g_407;
static const uint8_t * const *g_1011[6] = {&g_1012,&g_1012,&g_1012,&g_1012,&g_1012,&g_1012};
static const uint8_t * const **g_1010 = &g_1011[2];
static const uint8_t * const ***g_1009 = &g_1010;
static uint8_t ***g_1014 = &g_754;
static uint8_t ****g_1013 = &g_1014;
static int16_t *g_1022 = (void*)0;
static int16_t **g_1021 = &g_1022;
static int16_t *** const g_1020 = &g_1021;
static int16_t ***g_1025 = &g_1021;
static int32_t *g_1068 = &g_34;
static int64_t **g_1071 = (void*)0;
static int64_t *g_1074 = &g_645[3][0][3];
static int64_t **g_1073[6][8] = {{&g_1074,&g_1074,(void*)0,&g_1074,&g_1074,&g_1074,&g_1074,(void*)0},{&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074},{&g_1074,(void*)0,&g_1074,(void*)0,&g_1074,&g_1074,&g_1074,&g_1074},{(void*)0,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,(void*)0},{&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074},{&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074,&g_1074}};
static int32_t ***g_1077[2] = {&g_745,&g_745};
static int32_t ****g_1076 = &g_1077[1];
static const int32_t g_1133 = 9L;
static const int32_t *g_1132 = &g_1133;
static const int32_t **g_1131 = &g_1132;
static const int32_t ***g_1130[6][10][4] = {{{&g_1131,&g_1131,&g_1131,(void*)0},{&g_1131,&g_1131,&g_1131,(void*)0},{(void*)0,&g_1131,(void*)0,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{(void*)0,&g_1131,&g_1131,(void*)0},{&g_1131,&g_1131,(void*)0,(void*)0},{&g_1131,(void*)0,&g_1131,&g_1131},{(void*)0,(void*)0,&g_1131,(void*)0},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131}},{{(void*)0,&g_1131,&g_1131,&g_1131},{(void*)0,&g_1131,&g_1131,&g_1131},{&g_1131,(void*)0,(void*)0,&g_1131},{&g_1131,(void*)0,&g_1131,&g_1131},{&g_1131,(void*)0,&g_1131,(void*)0},{&g_1131,(void*)0,&g_1131,(void*)0},{&g_1131,(void*)0,&g_1131,&g_1131},{(void*)0,(void*)0,&g_1131,&g_1131},{&g_1131,(void*)0,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131}},{{(void*)0,&g_1131,(void*)0,&g_1131},{(void*)0,&g_1131,(void*)0,&g_1131},{&g_1131,&g_1131,(void*)0,(void*)0},{(void*)0,(void*)0,(void*)0,&g_1131},{&g_1131,(void*)0,(void*)0,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,(void*)0,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131}},{{&g_1131,(void*)0,&g_1131,&g_1131},{&g_1131,&g_1131,(void*)0,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{(void*)0,(void*)0,&g_1131,(void*)0},{(void*)0,(void*)0,&g_1131,(void*)0},{(void*)0,&g_1131,&g_1131,(void*)0},{(void*)0,&g_1131,&g_1131,&g_1131},{(void*)0,(void*)0,&g_1131,&g_1131},{(void*)0,&g_1131,&g_1131,&g_1131},{&g_1131,(void*)0,(void*)0,&g_1131}},{{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,(void*)0},{&g_1131,(void*)0,&g_1131,(void*)0},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,(void*)0,&g_1131,(void*)0},{&g_1131,&g_1131,(void*)0,&g_1131},{&g_1131,(void*)0,(void*)0,&g_1131},{(void*)0,&g_1131,(void*)0,(void*)0},{&g_1131,(void*)0,(void*)0,(void*)0}},{{(void*)0,(void*)0,(void*)0,&g_1131},{(void*)0,(void*)0,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,(void*)0},{&g_1131,(void*)0,&g_1131,(void*)0},{&g_1131,(void*)0,&g_1131,&g_1131},{&g_1131,(void*)0,(void*)0,(void*)0},{(void*)0,(void*)0,&g_1131,(void*)0},{&g_1131,&g_1131,&g_1131,&g_1131},{&g_1131,&g_1131,&g_1131,&g_1131}}};
static int8_t *g_1147 = &g_144;
static int8_t **g_1146 = &g_1147;
static int32_t g_1230 = 0x2526D13BL;
static uint64_t g_1231 = 1UL;
static const uint8_t g_1375 = 0x7AL;
static uint32_t g_1420 = 0x37F6C1F2L;
static const uint32_t g_1472[9] = {0x5F4983DDL,0x5F4983DDL,0x5F4983DDL,0x5F4983DDL,0x5F4983DDL,0x5F4983DDL,0x5F4983DDL,0x5F4983DDL,0x5F4983DDL};
static const uint8_t *** const *g_1577 = (void*)0;
static const uint8_t *** const **g_1576 = &g_1577;
static const uint8_t *** const ***g_1575[7] = {&g_1576,&g_1576,&g_1576,&g_1576,&g_1576,&g_1576,&g_1576};
static int32_t g_1592[10][2] = {{0x5A86F8F6L,(-6L)},{(-6L),0x5A86F8F6L},{(-6L),(-6L)},{0x5A86F8F6L,(-6L)},{(-6L),0x5A86F8F6L},{(-6L),(-6L)},{0x5A86F8F6L,(-6L)},{(-6L),0x5A86F8F6L},{(-6L),(-6L)},{0x5A86F8F6L,(-6L)}};
static uint8_t ******g_1629 = (void*)0;
static uint8_t ****** const *g_1628[10][8][3] = {{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}},{{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629},{&g_1629,&g_1629,&g_1629}}};
static int8_t *g_1647 = &g_815;
static int8_t *g_1649 = (void*)0;
static uint8_t *** const g_1679[7][10] = {{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754},{&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754,&g_754}};
static uint32_t *g_1722 = (void*)0;
static uint32_t **g_1721 = &g_1722;
static int16_t *g_1747 = &g_167;
static int8_t g_1829 = 1L;
static uint32_t g_1886[5] = {0x5BD1423BL,0x5BD1423BL,0x5BD1423BL,0x5BD1423BL,0x5BD1423BL};
static int32_t g_2040 = (-1L);
static const int16_t g_2240 = 1L;
static uint32_t g_2290 = 0x4FD39F50L;
static int32_t g_2329 = 0xB774A080L;
static int32_t g_2380 = 0xC9516B27L;
static int8_t g_2391 = 6L;
static int16_t g_2397 = 0L;
static int16_t g_2419[2] = {0x4DBDL,0x4DBDL};
static const uint8_t *g_2440 = &g_1375;
static uint32_t g_2524 = 0UL;
static uint8_t *** const *g_2558 = &g_1014;
static uint8_t *** const **g_2557 = &g_2558;
static uint8_t g_2665 = 0x01L;
static int32_t g_2695 = 0x070C08C9L;
static int32_t g_2696 = (-10L);
static uint32_t g_2775 = 0x0B909EE2L;
static int64_t **g_2835 = (void*)0;
static uint64_t g_2884 = 18446744073709551615UL;
static uint32_t g_2921 = 0xA09179CCL;
static int16_t g_2922 = 0x07FAL;
static int32_t g_2929 = (-1L);


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static uint16_t  func_11(int32_t * p_12, int32_t  p_13, int32_t  p_14, int32_t * p_15, int32_t * p_16);
static int16_t  func_24(const uint8_t  p_25, const uint8_t  p_26, int32_t * p_27, int32_t * p_28, int32_t * p_29);
static int32_t * func_35(int8_t  p_36, const int32_t * p_37, uint32_t  p_38, int64_t  p_39, int32_t * const * p_40);
static const int32_t * func_41(uint32_t  p_42, uint8_t  p_43, uint16_t  p_44);
static int16_t  func_45(int8_t  p_46, uint16_t  p_47, int64_t  p_48, uint8_t  p_49, int32_t * p_50);
static uint8_t  func_51(int64_t  p_52);
static int32_t * func_53(int32_t * p_54, int32_t ** p_55, int8_t  p_56);
static int32_t * func_57(int32_t  p_58, int16_t  p_59);
static int32_t  func_69(int8_t  p_70);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_6 g_2929
 * writes: g_6
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int8_t l_2 = (-1L);
    int32_t l_3 = 0xF2C29A3DL;
    int32_t *l_4 = &l_3;
    int32_t *l_5[2];
    uint16_t l_23 = 65532UL;
    const uint8_t l_1584 = 1UL;
    uint64_t l_1802[7] = {0x7FD842D701C77FD0LL,0x7FD842D701C77FD0LL,0x7FD842D701C77FD0LL,0x7FD842D701C77FD0LL,0x7FD842D701C77FD0LL,0x7FD842D701C77FD0LL,0x7FD842D701C77FD0LL};
    int32_t ** const *l_2229[6];
    const uint8_t **l_2243 = &g_1012;
    uint32_t l_2262 = 0xA0FF0DF9L;
    uint64_t l_2314 = 18446744073709551614UL;
    uint8_t *****l_2414 = &g_1013;
    int32_t l_2432[4];
    int16_t ***l_2435 = &g_1021;
    uint8_t l_2473 = 0xBFL;
    uint16_t l_2480 = 7UL;
    uint32_t **l_2493 = &g_1722;
    const uint32_t **l_2494 = (void*)0;
    int64_t *l_2519[2];
    uint8_t l_2528[9];
    const uint32_t l_2535 = 2UL;
    int64_t l_2551[6][4][8] = {{{(-8L),0x7D1503D4E83F1017LL,0x8C39ED3BE867F314LL,(-6L),0xA27483209A790D78LL,(-6L),0x8C39ED3BE867F314LL,0x7D1503D4E83F1017LL},{0L,0x0D45F277EB8E4492LL,1L,0x0D0B8E6FE173B43ELL,(-6L),(-1L),(-4L),0x8C39ED3BE867F314LL},{0x7D1503D4E83F1017LL,(-6L),1L,(-4L),0L,0L,(-4L),1L},{(-4L),(-4L),1L,(-1L),0x4331347A973355EALL,1L,0x8C39ED3BE867F314LL,(-8L)}},{{0x4331347A973355EALL,1L,0x8C39ED3BE867F314LL,(-8L),9L,1L,9L,(-8L)},{1L,1L,1L,(-1L),1L,0x7D1503D4E83F1017LL,(-6L),1L},{0x0D0B8E6FE173B43ELL,(-6L),(-1L),(-4L),0x8C39ED3BE867F314LL,1L,1L,0x8C39ED3BE867F314LL},{0x0D0B8E6FE173B43ELL,9L,9L,0x0D0B8E6FE173B43ELL,1L,(-4L),(-8L),0x7D1503D4E83F1017LL}},{{1L,0xA27483209A790D78LL,(-4L),(-6L),9L,1L,0x7D1503D4E83F1017LL,1L},{0x4331347A973355EALL,0xA27483209A790D78LL,(-8L),0xA27483209A790D78LL,0x4331347A973355EALL,(-4L),0x0D0B8E6FE173B43ELL,0x0D45F277EB8E4492LL},{(-4L),9L,0x4331347A973355EALL,1L,0L,1L,0xA27483209A790D78LL,0xA27483209A790D78LL},{0x7D1503D4E83F1017LL,(-6L),0x4331347A973355EALL,0x4331347A973355EALL,(-6L),0x7D1503D4E83F1017LL,0x0D0B8E6FE173B43ELL,0L}},{{0L,1L,(-8L),0x0D45F277EB8E4492LL,0xA27483209A790D78LL,1L,0x7D1503D4E83F1017LL,(-4L)},{(-8L),1L,(-4L),0x0D45F277EB8E4492LL,(-4L),1L,(-8L),0L},{(-6L),(-4L),9L,0x4331347A973355EALL,1L,0L,1L,0xA27483209A790D78LL},{0x0D45F277EB8E4492LL,(-6L),(-1L),1L,1L,(-1L),(-6L),0x0D45F277EB8E4492LL}},{{(-6L),0x0D45F277EB8E4492LL,1L,0xA27483209A790D78LL,(-4L),(-6L),9L,1L},{(-8L),0x7D1503D4E83F1017LL,0x8C39ED3BE867F314LL,(-6L),0xA27483209A790D78LL,(-6L),0x8C39ED3BE867F314LL,0x7D1503D4E83F1017LL},{0L,0x0D45F277EB8E4492LL,1L,0x0D0B8E6FE173B43ELL,(-6L),(-1L),(-4L),0x8C39ED3BE867F314LL},{0x7D1503D4E83F1017LL,(-6L),1L,(-4L),0L,0L,(-4L),1L}},{{(-4L),(-4L),1L,(-1L),0x4331347A973355EALL,1L,0x8C39ED3BE867F314LL,(-8L)},{0x4331347A973355EALL,1L,0x8C39ED3BE867F314LL,(-8L),9L,1L,9L,(-8L)},{1L,1L,1L,(-1L),1L,0x7D1503D4E83F1017LL,(-6L),1L},{0x0D0B8E6FE173B43ELL,(-6L),(-1L),(-4L),1L,1L,1L,1L}}};
    uint64_t l_2567 = 18446744073709551615UL;
    const uint64_t l_2570[9] = {18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL,18446744073709551615UL};
    uint16_t l_2617 = 0x3D82L;
    uint16_t l_2618 = 1UL;
    uint16_t l_2706 = 65527UL;
    int32_t l_2851 = 0x2030D99BL;
    int8_t l_2883 = 1L;
    int32_t l_2891 = 0xB7FF08CFL;
    const int16_t l_2899 = 0x43B3L;
    int64_t l_2919 = 8L;
    uint32_t *l_2926 = (void*)0;
    int i, j, k;
    for (i = 0; i < 2; i++)
        l_5[i] = &l_3;
    for (i = 0; i < 6; i++)
        l_2229[i] = (void*)0;
    for (i = 0; i < 4; i++)
        l_2432[i] = (-10L);
    for (i = 0; i < 2; i++)
        l_2519[i] = (void*)0;
    for (i = 0; i < 9; i++)
        l_2528[i] = 1UL;
    l_3 = l_2;
    g_6--;
    return g_2929;
}


/* ------------------------------------------ */
/* 
 * reads : g_115 g_1131 g_147 g_124 g_1472 g_1647 g_122 g_33 g_18 g_1068 g_660 g_346 g_167
 * writes: g_115 g_1132 g_147 g_144 g_815 g_1829 g_18 g_62 g_251 g_34
 */
static uint16_t  func_11(int32_t * p_12, int32_t  p_13, int32_t  p_14, int32_t * p_15, int32_t * p_16)
{ /* block id: 839 */
    uint32_t l_1813 = 0xA62C5EF1L;
    uint8_t ***l_1815[9][9][3] = {{{&g_460,(void*)0,&g_460},{&g_460,&g_460,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_460,&g_754},{&g_754,&g_460,&g_460},{&g_754,&g_460,&g_460},{&g_754,&g_460,&g_460},{&g_460,&g_754,&g_460},{&g_460,&g_754,&g_754}},{{&g_754,&g_460,&g_460},{&g_754,(void*)0,&g_754},{&g_754,&g_460,&g_460},{&g_754,&g_754,&g_754},{&g_754,&g_460,&g_460},{&g_460,&g_460,&g_460},{&g_754,&g_460,&g_460},{&g_754,&g_754,&g_460},{&g_460,&g_754,&g_754}},{{&g_754,&g_754,&g_460},{&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460},{&g_754,&g_460,(void*)0},{&g_754,&g_754,&g_460},{&g_754,&g_460,&g_460},{&g_754,(void*)0,&g_460},{&g_754,&g_460,&g_460},{&g_754,&g_754,&g_460}},{{&g_754,&g_754,&g_754},{&g_460,&g_460,&g_754},{&g_460,&g_460,(void*)0},{&g_754,&g_460,&g_754},{&g_460,&g_460,(void*)0},{&g_754,&g_754,&g_754},{&g_754,&g_460,&g_754},{&g_460,(void*)0,&g_460},{&g_754,&g_460,&g_460}},{{&g_754,&g_460,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_460,&g_460},{&g_754,&g_460,(void*)0},{&g_460,(void*)0,&g_460},{&g_460,&g_460,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_460,&g_754},{&g_754,&g_460,&g_460}},{{&g_754,&g_460,&g_460},{&g_754,&g_460,&g_460},{&g_460,&g_754,&g_460},{&g_460,&g_754,&g_754},{&g_754,&g_460,&g_460},{&g_754,(void*)0,&g_754},{&g_754,&g_460,&g_460},{&g_754,&g_754,&g_754},{&g_754,&g_460,&g_460}},{{&g_460,&g_460,&g_460},{&g_754,&g_460,&g_460},{&g_754,&g_754,&g_460},{&g_460,&g_754,&g_754},{&g_754,&g_754,&g_460},{&g_754,&g_754,&g_460},{&g_460,&g_754,&g_460},{&g_460,&g_460,&g_460},{&g_460,&g_460,&g_754}},{{&g_460,&g_754,&g_460},{&g_460,&g_754,&g_460},{&g_460,&g_754,&g_460},{&g_460,&g_460,&g_460},{&g_460,&g_460,&g_460},{&g_460,&g_754,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_754,&g_460}},{{&g_754,&g_754,&g_460},{&g_460,&g_460,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_754,&g_460},{&g_754,&g_754,&g_460},{&g_460,&g_460,&g_460},{&g_754,&g_754,&g_754},{&g_754,&g_754,&g_460},{&g_460,&g_754,&g_460}}};
    int32_t l_1822 = (-5L);
    uint8_t ****l_1836 = &l_1815[2][4][1];
    int32_t l_1883 = 0xC02481E7L;
    int32_t l_1885 = 0L;
    int32_t l_1903 = 0xBBDD7A56L;
    int32_t l_1908 = 0xB743BEC4L;
    uint8_t *****l_1994 = &l_1836;
    uint8_t ******l_1993 = &l_1994;
    uint64_t l_2048[8][4][1] = {{{1UL},{18446744073709551606UL},{1UL},{4UL}},{{0x06376FB945D4688BLL},{18446744073709551606UL},{0x06376FB945D4688BLL},{4UL}},{{1UL},{18446744073709551606UL},{1UL},{4UL}},{{0x06376FB945D4688BLL},{18446744073709551606UL},{0x06376FB945D4688BLL},{4UL}},{{1UL},{18446744073709551606UL},{1UL},{4UL}},{{0x06376FB945D4688BLL},{18446744073709551606UL},{0x06376FB945D4688BLL},{4UL}},{{1UL},{18446744073709551606UL},{1UL},{4UL}},{{0x06376FB945D4688BLL},{18446744073709551606UL},{0x06376FB945D4688BLL},{4UL}}};
    uint64_t l_2050 = 0x12F8E7D6093C9EF8LL;
    uint32_t l_2051 = 0x5D2E492DL;
    int32_t ****l_2052 = &g_1077[1];
    uint16_t *l_2090 = (void*)0;
    uint8_t **l_2145 = &g_461[0][1][1];
    int32_t l_2175 = 4L;
    uint32_t l_2176 = 0xADBA0D80L;
    int32_t *l_2186 = (void*)0;
    int64_t l_2216 = (-5L);
    int i, j, k;
    for (g_115 = 0; (g_115 == (-10)); g_115 = safe_sub_func_uint64_t_u_u(g_115, 5))
    { /* block id: 842 */
        int32_t *l_1805 = &g_34;
        (*g_1131) = l_1805;
        return p_13;
    }
    for (p_13 = (-1); (p_13 <= 12); ++p_13)
    { /* block id: 848 */
        uint64_t *l_1810 = &g_147;
        uint8_t ***l_1814 = &g_460;
        int32_t l_1818 = 1L;
        int8_t *l_1828 = (void*)0;
        int32_t l_1878 = 0xC9E6D67EL;
        int32_t l_1879 = 5L;
        int32_t l_1880 = 0L;
        int32_t l_1896 = 8L;
        int32_t l_1897 = (-8L);
        int32_t l_1900 = 0xEE10BBEAL;
        int32_t l_1901 = (-5L);
        int32_t l_1904 = 0xA0F66863L;
        int32_t l_1905 = 0xFC34B61DL;
        uint8_t *** const *l_2164 = &g_1679[2][8];
        uint8_t *** const ** const l_2163 = &l_2164;
        int32_t l_2168 = 0x4BFE413BL;
        int32_t ***l_2183 = &g_745;
        (*g_33) |= ((safe_div_func_int32_t_s_s((((*l_1810)++) <= ((l_1813 &= 0xA890L) & ((l_1814 == l_1815[2][4][1]) , (g_1829 = ((safe_add_func_uint8_t_u_u(l_1818, 0xEEL)) , (+(safe_add_func_int64_t_s_s((l_1822 >= (safe_div_func_uint32_t_u_u(((safe_unary_minus_func_uint64_t_u(g_124)) < (p_13 & ((*g_1647) = ((safe_rshift_func_uint8_t_u_s(((g_1472[4] , l_1828) == l_1828), 6)) , p_14)))), 1UL))), g_122)))))))), 0xAAF52458L)) , l_1813);
        for (g_62 = (-6); (g_62 != 41); g_62++)
        { /* block id: 856 */
            int64_t l_1834 = 0x1ABE96D381A6B0CFLL;
            int32_t l_1835 = (-1L);
            int32_t l_1843 = (-1L);
            uint32_t * const **l_1874[10][4] = {{&g_714[1],&g_714[1],&g_714[4],&g_714[1]},{&g_714[1],(void*)0,(void*)0,&g_714[1]},{(void*)0,&g_714[1],(void*)0,(void*)0},{&g_714[1],&g_714[1],&g_714[4],&g_714[1]},{&g_714[1],(void*)0,(void*)0,&g_714[1]},{(void*)0,&g_714[1],(void*)0,(void*)0},{&g_714[1],&g_714[1],&g_714[4],&g_714[1]},{&g_714[1],(void*)0,(void*)0,&g_714[1]},{(void*)0,&g_714[1],(void*)0,(void*)0},{&g_714[1],&g_714[1],&g_714[4],&g_714[1]}};
            int32_t l_1882 = 0x3FAF0F4AL;
            int32_t l_1902 = 0x1FFF3724L;
            int32_t l_1907 = 0xB6AD0988L;
            int32_t l_1909[7];
            uint16_t l_1912 = 0x8793L;
            uint8_t ******l_1995 = &l_1994;
            int16_t *l_2020[8][5] = {{&g_167,&g_171,&g_922,&g_171,(void*)0},{(void*)0,&g_124,&g_167,&g_922,(void*)0},{&g_922,&g_171,&g_171,&g_171,&g_171},{&g_171,(void*)0,&g_171,(void*)0,&g_167},{&g_171,&g_171,&g_922,(void*)0,(void*)0},{&g_922,&g_171,&g_124,&g_171,(void*)0},{(void*)0,&g_171,&g_922,(void*)0,&g_922},{&g_167,&g_167,&g_171,&g_171,&g_922}};
            int32_t l_2092 = 0xE13BC404L;
            uint8_t l_2131[5];
            uint16_t *l_2140 = &l_1912;
            uint32_t l_2146 = 0xE488D862L;
            uint64_t l_2147[9][2] = {{0UL,0UL},{0UL,0UL},{0UL,0UL},{0UL,0UL},{0UL,0UL},{0UL,0UL},{0UL,0UL},{0UL,0UL},{0UL,0UL}};
            int16_t ***l_2162[6];
            int i, j;
            for (i = 0; i < 7; i++)
                l_1909[i] = 0xCAFFB40EL;
            for (i = 0; i < 5; i++)
                l_2131[i] = 0UL;
            for (i = 0; i < 6; i++)
                l_2162[i] = &g_1021;
        }
        for (g_251 = 0; (g_251 <= 1); g_251 += 1)
        { /* block id: 1008 */
            uint64_t *l_2196 = &g_147;
            uint16_t l_2197 = 7UL;
            l_2186 = (void*)0;
            for (g_34 = 2; (g_34 >= 0); g_34 -= 1)
            { /* block id: 1012 */
                int16_t l_2187 = 0x4CB5L;
                (*g_1131) = (l_2187 , ((safe_rshift_func_uint8_t_u_u((safe_mod_func_int64_t_s_s((p_14 > (safe_sub_func_uint16_t_u_u((safe_mod_func_int8_t_s_s(((-7L) || p_14), 0xB2L)), (((*p_12) = 0L) & ((((l_1810 != (l_2196 = (void*)0)) != l_2197) == l_2187) , l_1905))))), l_1905)), 0)) , &l_1879));
            }
            for (p_14 = 0; (p_14 <= 1); p_14 += 1)
            { /* block id: 1019 */
                uint16_t l_2198 = 65528UL;
                (*g_1068) = (*p_16);
                return l_2198;
            }
        }
    }
    (*g_1131) = &l_1885;
    (*p_12) = ((safe_lshift_func_int16_t_s_s((safe_add_func_int64_t_s_s((safe_div_func_uint32_t_u_u((p_14 ^ (((((4294967289UL & (safe_mod_func_int32_t_s_s((((g_660 >= (((*p_15) , (0x2BL >= 0x65L)) < ((safe_sub_func_uint64_t_u_u(g_346, (((safe_unary_minus_func_uint8_t_u((safe_rshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_u(((l_2216 >= (safe_div_func_int8_t_s_s(p_13, 0x3BL))) , 0L), 0)), 5)))) >= (-10L)) == p_13))) < p_13))) && p_13) , (*p_16)), g_167))) && 65529UL) < 65527UL) , &l_1994) != (void*)0)), l_2048[1][1][0])), 18446744073709551610UL)), 8)) ^ p_13);
    return p_14;
}


/* ------------------------------------------ */
/* 
 * reads : g_1010 g_1011 g_645 g_6 g_1068 g_122
 * writes: g_84 g_6 g_377 g_34 g_122
 */
static int16_t  func_24(const uint8_t  p_25, const uint8_t  p_26, int32_t * p_27, int32_t * p_28, int32_t * p_29)
{ /* block id: 830 */
    uint16_t *l_1783 = &g_84;
    const int32_t l_1794 = 1L;
    uint64_t l_1795[3];
    uint16_t *l_1796 = &g_6;
    uint32_t *l_1797 = &g_377;
    uint32_t l_1798 = 0x00ED3A7FL;
    int32_t *l_1799 = &g_122;
    int i;
    for (i = 0; i < 3; i++)
        l_1795[i] = 0UL;
    (*l_1799) |= ((*g_1068) = (safe_div_func_int32_t_s_s((((*l_1797) = (((safe_unary_minus_func_int8_t_s((safe_mul_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(((((safe_add_func_uint64_t_u_u((safe_rshift_func_int8_t_s_u((safe_mod_func_int32_t_s_s(((safe_add_func_int64_t_s_s((((void*)0 == (*g_1010)) & (-1L)), (((*l_1783) = (&g_1575[0] != (void*)0)) | ((*l_1796) ^= ((((4UL != (((safe_rshift_func_uint8_t_u_s((safe_mul_func_uint8_t_u_u((g_645[3][0][3] && ((((safe_mul_func_uint16_t_u_u((safe_div_func_int32_t_s_s((safe_lshift_func_uint16_t_u_u(((0x7272L | p_25) | 247UL), 5)), 1UL)), (-1L))) || l_1794) ^ l_1794) & p_26)), l_1795[1])), 4)) , 0x9E276CC9969E3C9ALL) >= p_26)) , l_1795[1]) , 0x0C126489L) != 3L))))) , (*p_28)), 0x727A7237L)), 2)), 18446744073709551608UL)) == 255UL) , (void*)0) != &l_1794), p_26)), l_1795[1])))) && 0x93BDE429L) , l_1794)) >= (*p_28)), l_1798)));
    (*g_1068) = ((&l_1795[1] != &l_1795[0]) || (safe_sub_func_uint16_t_u_u(1UL, ((*l_1799) <= p_26))));
    return (*l_1799);
}


/* ------------------------------------------ */
/* 
 * reads : g_342 g_1068 g_1131 g_1132 g_1020 g_1021 g_1022 g_1230 g_1628 g_1012 g_407 g_1147 g_144 g_6 g_872 g_922 g_1146 g_147 g_1074 g_645 g_815 g_34 g_18 g_1076 g_1077 g_60 g_205 g_17 g_84 g_1649 g_802 g_122 g_1592 g_1025 g_377 g_1747 g_167 g_460 g_461 g_745 g_746
 * writes: g_342 g_34 g_1230 g_171 g_139 g_377 g_1132 g_922 g_1147 g_1647 g_1649 g_115 g_289 g_346 g_660 g_205 g_60 g_84 g_62 g_802 g_1021 g_1721 g_407 g_746 g_122 g_1592
 */
static int32_t * func_35(int8_t  p_36, const int32_t * p_37, uint32_t  p_38, int64_t  p_39, int32_t * const * p_40)
{ /* block id: 752 */
    uint8_t **** const *l_1627 = &g_1013;
    uint8_t **** const **l_1626 = &l_1627;
    uint8_t **** const ***l_1625 = &l_1626;
    int32_t l_1641 = 0x4E622CCEL;
    int16_t *l_1669 = &g_922;
    uint8_t *** const *l_1674[9] = {&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014,&g_1014};
    uint8_t *** const **l_1673 = &l_1674[2];
    uint8_t *** const ***l_1672 = &l_1673;
    int32_t *l_1680 = &g_34;
    uint16_t l_1766 = 1UL;
    int32_t *l_1767 = &g_1592[8][0];
    int i;
    for (g_342 = 0; (g_342 <= 1); ++g_342)
    { /* block id: 755 */
        uint64_t l_1616[5];
        int16_t *l_1643 = &g_124;
        int32_t l_1661[9] = {(-1L),0x2048E95EL,(-1L),(-1L),0x2048E95EL,(-1L),(-1L),0x2048E95EL,(-1L)};
        uint8_t *l_1695 = &g_407;
        uint32_t **l_1720 = (void*)0;
        int32_t l_1724 = 0xB4B25406L;
        int16_t *l_1743 = &g_171;
        int64_t l_1746[8];
        int32_t **** const l_1748 = &g_1077[1];
        int8_t l_1763 = 1L;
        int32_t *l_1764 = &g_122;
        int32_t *l_1765 = &l_1641;
        int i;
        for (i = 0; i < 5; i++)
            l_1616[i] = 7UL;
        for (i = 0; i < 8; i++)
            l_1746[i] = 0x33614662F7BD294CLL;
        (**p_40) = 0x160B37C7L;
        for (g_1230 = 0; (g_1230 >= 3); g_1230++)
        { /* block id: 759 */
            int64_t l_1639 = 0xF617A2D542BF12FCLL;
            int8_t *l_1648 = &g_815;
            uint8_t *****l_1655 = &g_1013;
            int32_t l_1660 = (-1L);
            uint64_t *l_1696 = &l_1616[3];
            uint32_t *l_1719 = (void*)0;
            uint32_t **l_1718[1];
            int32_t *l_1723[5][1] = {{&g_122},{&g_1592[9][1]},{&g_122},{&g_1592[9][1]},{&g_122}};
            int i, j;
            for (i = 0; i < 1; i++)
                l_1718[i] = &l_1719;
            for (g_171 = (-22); (g_171 > (-13)); g_171++)
            { /* block id: 762 */
                int16_t l_1607 = (-2L);
                int16_t *l_1642 = &l_1607;
                int8_t *l_1646 = &g_139;
                int8_t **l_1645[10] = {(void*)0,&l_1646,(void*)0,&l_1646,(void*)0,&l_1646,(void*)0,&l_1646,(void*)0,&l_1646};
                uint8_t *****l_1654 = &g_1013;
                int32_t *l_1662 = &l_1661[4];
                int i;
                for (g_139 = 0; (g_139 != (-17)); g_139 = safe_sub_func_uint8_t_u_u(g_139, 8))
                { /* block id: 765 */
                    int32_t *l_1608 = &g_1592[4][0];
                    int32_t *l_1609 = &g_34;
                    int32_t *l_1610 = (void*)0;
                    int32_t *l_1611 = &g_1592[1][0];
                    int32_t *l_1612 = &g_1592[2][0];
                    int32_t *l_1613 = &g_122;
                    int32_t *l_1614 = &g_34;
                    int32_t *l_1615[10] = {&g_1592[6][1],(void*)0,&g_1592[6][1],(void*)0,&g_1592[6][1],(void*)0,&g_1592[6][1],(void*)0,&g_1592[6][1],(void*)0};
                    uint8_t ****** const *l_1630 = (void*)0;
                    int i;
                    for (g_377 = (-23); (g_377 < 14); g_377 = safe_add_func_int16_t_s_s(g_377, 8))
                    { /* block id: 768 */
                        (*g_1131) = (*g_1131);
                    }
                    --l_1616[3];
                    for (g_377 = 1; (g_377 <= 28); g_377++)
                    { /* block id: 774 */
                        int64_t l_1638 = 0x631B658F438105A5LL;
                        int16_t *l_1640 = &g_922;
                        int32_t l_1644 = 5L;
                        l_1644 |= (safe_add_func_int16_t_s_s(((**g_1020) == (l_1643 = (((((*l_1640) &= ((safe_mul_func_uint8_t_u_u(1UL, ((g_1230 < (((((((l_1625 == (l_1630 = g_1628[8][5][0])) , ((((safe_sub_func_uint16_t_u_u((!(safe_mod_func_uint8_t_u_u((*g_1012), (safe_lshift_func_int16_t_s_u((((l_1638 || p_38) || l_1639) == 7L), l_1639))))), 65534UL)) < 0xBAF0L) != (*g_1147)) , g_6)) <= 5L) & p_39) , p_36) | p_39) , g_872)) ^ l_1638))) == p_36)) , l_1641) , l_1639) , l_1642))), 65533UL));
                    }
                    if (l_1616[3])
                        break;
                }
                (**p_40) = (((((*g_1146) = &p_36) == (g_1649 = (l_1648 = (g_1647 = &g_144)))) | (safe_sub_func_uint16_t_u_u(65535UL, (((0x88AA45610CC11DF6LL < g_147) || (safe_lshift_func_int16_t_s_s((l_1654 == l_1655), (safe_add_func_int8_t_s_s(p_38, (l_1660 &= (safe_lshift_func_int8_t_s_u(((((p_38 , 0x3AFF76B7FD4741B8LL) >= (*g_1074)) || g_815) != (*g_1068)), 2)))))))) , l_1616[3])))) ^ g_1230);
                l_1661[4] = l_1607;
                for (g_139 = 9; (g_139 >= 0); g_139 -= 1)
                { /* block id: 791 */
                    uint16_t l_1678 = 65526UL;
                    (**p_40) = 0L;
                    l_1641 = (**p_40);
                    for (g_115 = 9; (g_115 >= 0); g_115 -= 1)
                    { /* block id: 796 */
                        int32_t ***l_1663 = (void*)0;
                        int32_t ***l_1664 = &g_289[2];
                        p_37 = func_53(l_1662, ((*l_1664) = &l_1662), (l_1639 >= g_18));
                    }
                    if (l_1616[3])
                    { /* block id: 800 */
                        int32_t *l_1665[9][1][2] = {{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}},{{&g_18,(void*)0}}};
                        int i, j, k;
                        (*g_1131) = func_53(l_1665[4][0][1], &g_802, (*g_1649));
                    }
                    else
                    { /* block id: 802 */
                        int32_t *l_1666 = &g_1592[8][0];
                        uint8_t *** const ****l_1675 = &l_1672;
                        l_1680 = l_1666;
                    }
                }
            }
            (*g_1068) = ((safe_mod_func_uint8_t_u_u(((g_872 , g_122) & (safe_mul_func_int8_t_s_s((0xBD2D7063F6A20FC3LL && (safe_add_func_int64_t_s_s(l_1639, (safe_mod_func_int32_t_s_s((*l_1680), (((-1L) == ((safe_div_func_uint32_t_u_u(((safe_rshift_func_int8_t_s_u((safe_lshift_func_uint8_t_u_u((((((((*g_1025) = (*g_1020)) != (void*)0) == ((*l_1696) = ((void*)0 != l_1695))) > 1L) != g_122) & (*l_1680)), 5)), p_36)) && 0xCA4DL), l_1639)) | (*l_1680))) | 0xA8C8L)))))), 0xF0L))), l_1660)) , l_1660);
            l_1724 &= ((l_1639 > (0x0B3133C4ED2A8974LL ^ (safe_lshift_func_int8_t_s_u((safe_rshift_func_int8_t_s_u((safe_mul_func_int8_t_s_s((+(safe_mod_func_int64_t_s_s((safe_add_func_uint64_t_u_u((safe_add_func_int64_t_s_s((p_36 == (((p_39 , (((((safe_add_func_int8_t_s_s((safe_rshift_func_uint16_t_u_u((((((249UL > (l_1639 < (-4L))) , l_1718[0]) == (g_1721 = l_1720)) == 1UL) & g_377), p_36)), (*l_1680))) ^ 0L) ^ 0xC3188996L) , l_1680) != (void*)0)) != (**p_40)) ^ (*l_1680))), p_36)), g_645[3][0][7])), p_36))), p_36)), 2)), l_1661[8])))) || l_1660);
            l_1724 |= (safe_rshift_func_int16_t_s_s((safe_add_func_uint64_t_u_u(18446744073709551615UL, (p_36 < (safe_mul_func_uint16_t_u_u((l_1661[3] = (safe_lshift_func_int16_t_s_u(0xCD88L, (safe_mul_func_int16_t_s_s((safe_add_func_uint32_t_u_u(g_407, ((((safe_rshift_func_int8_t_s_s((l_1661[8] <= (*l_1680)), 7)) & (safe_add_func_uint16_t_u_u((safe_add_func_uint64_t_u_u(((l_1743 = (void*)0) != ((((safe_mod_func_uint8_t_u_u(((void*)0 == (**l_1672)), l_1746[0])) , (*l_1680)) < 0xFAC382F3L) , g_1747)), (-4L))), (-7L)))) , l_1748) != l_1748))), (*l_1680)))))), 65535UL))))), p_38));
        }
        (*g_1131) = (g_407 , func_57((p_38 , p_38), (*g_1747)));
        (*l_1765) ^= (((*l_1680) , 0xAEFCL) > ((((**g_460) = ((*l_1672) == (**l_1625))) == (safe_div_func_uint8_t_u_u(((((*l_1764) = (safe_mul_func_int8_t_s_s(((*l_1680) && ((safe_mod_func_int16_t_s_s((((((safe_mul_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((safe_sub_func_uint64_t_u_u(((-8L) > (safe_mod_func_uint32_t_u_u((((((*g_745) = (***l_1748)) != l_1680) , p_36) , p_39), 1UL))), (*l_1680))), l_1616[3])), (-10L))) && l_1616[3]) , p_39) == l_1661[3]) & l_1763), l_1724)) != l_1746[0])), (-2L)))) != (*l_1680)) , (*l_1680)), 246UL))) & l_1616[1]));
    }
    (*l_1680) ^= l_1766;
    return l_1767;
}


/* ------------------------------------------ */
/* 
 * reads : g_1068 g_34 g_1131 g_1592 g_460 g_461 g_407
 * writes: g_122 g_34 g_1592 g_1132 g_407
 */
static const int32_t * func_41(uint32_t  p_42, uint8_t  p_43, uint16_t  p_44)
{ /* block id: 744 */
    int32_t **l_1587 = &g_1068;
    int32_t *l_1590 = &g_122;
    int32_t *l_1591 = &g_1592[8][0];
    const int32_t *l_1595[5][6] = {{&g_1592[1][0],&g_34,&g_34,&g_1592[1][0],&g_34,&g_1592[1][0]},{&g_1592[1][0],&g_34,&g_1592[1][0],&g_34,&g_34,&g_1592[1][0]},{(void*)0,(void*)0,&g_34,&g_18,&g_34,(void*)0},{&g_34,&g_34,&g_18,&g_18,&g_34,&g_34},{(void*)0,&g_34,&g_18,&g_34,(void*)0,(void*)0}};
    int i, j;
    (*l_1591) = ((**l_1587) = ((p_43 && (safe_mul_func_int16_t_s_s((((void*)0 != l_1587) , p_43), ((((safe_lshift_func_uint16_t_u_s(p_42, 0)) || ((**l_1587) & p_43)) | (((*l_1590) = (**l_1587)) , p_42)) ^ p_44)))) >= 0xBAL));
    (*g_1131) = (*l_1587);
    (**l_1587) = ((*l_1591) != (++(**g_460)));
    return l_1595[1][3];
}


/* ------------------------------------------ */
/* 
 * reads : g_289 g_290 g_18
 * writes:
 */
static int16_t  func_45(int8_t  p_46, uint16_t  p_47, int64_t  p_48, uint8_t  p_49, int32_t * p_50)
{ /* block id: 631 */
    uint8_t *l_1382 = &g_407;
    uint8_t *l_1384[4][4] = {{&g_125,&g_125,&g_125,&g_125},{&g_125,&g_125,&g_125,&g_125},{&g_125,&g_125,&g_125,&g_125},{&g_125,&g_125,&g_125,&g_125}};
    uint8_t **l_1383 = &l_1384[3][2];
    int32_t l_1385 = 0xE2E3DA88L;
    uint32_t *l_1388 = &g_205;
    int32_t ***l_1395 = &g_289[2];
    int32_t l_1441 = (-1L);
    int32_t l_1460[8][7][2] = {{{1L,1L},{0L,0x0DDFA558L},{1L,0x0DDFA558L},{0L,1L},{1L,0L},{0x0DDFA558L,1L},{0x0DDFA558L,0L}},{{1L,1L},{0L,0x0DDFA558L},{1L,0x0DDFA558L},{0L,1L},{1L,0L},{0x0DDFA558L,1L},{0x0DDFA558L,0L}},{{1L,1L},{0L,0x0DDFA558L},{1L,0x0DDFA558L},{0L,1L},{1L,0L},{0x0DDFA558L,1L},{0x0DDFA558L,0L}},{{1L,1L},{0L,0x0DDFA558L},{1L,0x0DDFA558L},{0L,1L},{1L,0L},{0x0DDFA558L,1L},{0x0DDFA558L,0L}},{{1L,1L},{0L,0x0DDFA558L},{1L,0x0DDFA558L},{0L,1L},{1L,0L},{0x0DDFA558L,1L},{0x0DDFA558L,0L}},{{1L,1L},{0L,0x0DDFA558L},{1L,0x0DDFA558L},{0L,1L},{1L,1L},{0L,2L},{0L,1L}},{{(-4L),(-4L)},{1L,0L},{2L,0L},{1L,(-4L)},{(-4L),1L},{0L,2L},{0L,1L}},{{(-4L),(-4L)},{1L,0L},{2L,0L},{1L,(-4L)},{(-4L),1L},{0L,2L},{0L,1L}}};
    int64_t l_1465[8] = {0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL,0xC05AF51B47A64D06LL};
    int32_t **l_1473[4];
    uint32_t l_1485 = 0xEAC2296FL;
    uint8_t *****l_1524 = &g_1013;
    uint8_t **** const *l_1579 = &g_1013;
    uint8_t **** const * const *l_1578 = &l_1579;
    uint8_t **** const * const **l_1580 = &l_1578;
    uint32_t l_1583 = 0x2DCE93D2L;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_1473[i] = (void*)0;
    return (***l_1395);
}


/* ------------------------------------------ */
/* 
 * reads : g_62 g_60 g_84 g_18 g_17 g_1068 g_34 g_1147 g_144 g_1076 g_1077 g_205 g_802 g_1146 g_122 g_251 g_115 g_1025 g_1021 g_1022 g_147 g_1230 g_1012 g_407 g_1231 g_745 g_746 g_1020 g_1010 g_1011 g_1074 g_645 g_125 g_826 g_460 g_461 g_1009 g_139 g_1133 g_346 g_1131 g_1132 g_872 g_167
 * writes: g_62 g_60 g_84 g_34 g_346 g_660 g_205 g_802 g_922 g_115 g_251 g_147 g_645 g_407 g_1230 g_1132 g_167 g_1011
 */
static uint8_t  func_51(int64_t  p_52)
{ /* block id: 5 */
    uint32_t *l_61 = &g_62;
    int32_t *l_65 = (void*)0;
    int32_t *l_66 = &g_60;
    int32_t l_67 = 8L;
    int32_t **l_1156 = &g_802;
    int16_t ***l_1199 = (void*)0;
    int16_t l_1202[8] = {(-9L),0xACF5L,(-9L),0xACF5L,(-9L),0xACF5L,(-9L),0xACF5L};
    uint64_t *l_1203[6] = {&g_147,&g_147,&g_147,&g_147,&g_147,&g_147};
    int16_t *l_1204 = (void*)0;
    int16_t *l_1205 = &g_922;
    int8_t l_1206 = 0x82L;
    int32_t l_1252 = 0L;
    int8_t **l_1270 = &g_1147;
    int64_t ***l_1326[7] = {&g_1071,&g_1071,&g_1073[0][6],&g_1071,&g_1071,&g_1073[0][6],&g_1071};
    const uint8_t * const l_1374 = &g_1375;
    const uint8_t * const *l_1373 = &l_1374;
    int i;
lbl_1211:
    (*l_1156) = func_53(func_57(((*l_66) = (((*l_61)--) , p_52)), l_67), l_1156, (*g_1147));
    if (((safe_rshift_func_uint16_t_u_u((l_67 != (((((safe_mul_func_int8_t_s_s(((~(**g_1146)) > ((safe_rshift_func_uint8_t_u_s((((*l_1205) = (((&g_147 == (((l_67 , (safe_div_func_uint32_t_u_u((safe_mod_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u((l_1199 != (void*)0), (safe_mod_func_uint16_t_u_u(((&g_1071 == (void*)0) , (((*l_61) = (((l_67 > l_1202[7]) ^ p_52) ^ 0x5B6BL)) , 0UL)), g_122)))), (**g_1146))), p_52))) && (-4L)) , l_1203[1])) || p_52) , 0xFD3FL)) | p_52), p_52)) , g_251)), (*g_1147))) , (*g_1147)) | l_1206) , 0x59F737B6L) || p_52)), 15)) != p_52))
    { /* block id: 555 */
        uint32_t l_1208 = 0x4526EA1CL;
        int32_t *****l_1267 = &g_1076;
        int32_t l_1272 = 1L;
        uint8_t ***l_1288[5] = {&g_460,&g_460,&g_460,&g_460,&g_460};
        int32_t l_1302[10] = {0x0FB57030L,(-2L),0x0FB57030L,(-2L),0x0FB57030L,(-2L),0x0FB57030L,(-2L),0x0FB57030L,(-2L)};
        int8_t **l_1361 = &g_1147;
        int32_t l_1372 = (-2L);
        int i;
        l_1208 = ((+0xE3D57A3A4F2FFA4DLL) , (-1L));
        for (g_115 = (-25); (g_115 >= 12); g_115 = safe_add_func_uint8_t_u_u(g_115, 3))
        { /* block id: 559 */
            int16_t *l_1218[4] = {&g_167,&g_167,&g_167,&g_167};
            int i;
            (*g_1068) = (*g_1068);
            if (l_1206)
                goto lbl_1211;
            (*g_1068) = (safe_sub_func_uint8_t_u_u(((safe_sub_func_uint64_t_u_u((g_147 &= (safe_sub_func_uint64_t_u_u(((-1L) & ((**g_1025) == l_1218[0])), (safe_rshift_func_int8_t_s_s(0x27L, (p_52 || (!(safe_rshift_func_int8_t_s_s(((g_251 ^= 0x64A4L) || ((safe_rshift_func_uint16_t_u_u(((l_1208 , 0UL) & (safe_sub_func_int32_t_s_s(1L, (*g_17)))), 12)) <= p_52)), p_52))))))))), g_1230)) , (*g_1012)), g_1231));
        }
        if ((((~(safe_add_func_int16_t_s_s((((*g_1021) == ((9L >= ((**g_745) , l_1208)) , (**g_1020))) , ((safe_lshift_func_int16_t_s_u(p_52, 2)) && (safe_sub_func_int16_t_s_s((safe_add_func_uint32_t_u_u((safe_rshift_func_int16_t_s_s(((*l_1205) = ((!(safe_mod_func_int64_t_s_s((p_52 = (safe_mul_func_uint8_t_u_u((safe_mod_func_int64_t_s_s(((*g_1074) = ((safe_rshift_func_uint8_t_u_u(p_52, (***g_1010))) | ((-5L) == (*g_1147)))), l_1252)), l_1208))), 0x01E6A87AC70641F9LL))) != (*g_1147))), l_1208)), l_1208)), l_1208)))), l_1208))) < l_1208) < l_1208))
        { /* block id: 569 */
            int8_t **l_1271 = &g_1147;
            uint32_t *l_1273 = &l_1208;
            int16_t ***l_1278 = (void*)0;
            int16_t l_1279 = 8L;
            uint16_t *l_1301 = &g_251;
            (*g_1068) |= (safe_rshift_func_uint16_t_u_u(((safe_unary_minus_func_uint64_t_u((safe_add_func_int32_t_s_s((((((g_147 &= p_52) < (((safe_lshift_func_int8_t_s_s(((~((safe_sub_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u((((((void*)0 != l_1267) || ((safe_add_func_int16_t_s_s(((l_1270 != l_1271) == p_52), p_52)) && g_18)) > ((*l_1273)++)) && ((safe_rshift_func_int16_t_s_u(l_1272, 12)) , p_52)), g_645[1][0][8])) == 1L), 0xA6L)) ^ l_1272)) > p_52), 7)) , (void*)0) == l_1278)) == g_125) | 3UL) & 0L), g_826)))) , 1UL), l_1279));
            (*g_1068) = ((l_1302[6] = (safe_sub_func_uint8_t_u_u((--(**g_460)), (safe_mod_func_uint32_t_u_u((safe_mod_func_uint16_t_u_u(((*l_1301) = ((l_1288[1] == (*g_1009)) <= (((0x549B46F9L > ((safe_lshift_func_int8_t_s_s((safe_unary_minus_func_uint64_t_u((0x202C066CL ^ ((safe_add_func_uint8_t_u_u(l_1279, ((((((*g_1074) |= (safe_add_func_int8_t_s_s(0xACL, (safe_mul_func_uint16_t_u_u(g_139, ((l_1272 = ((safe_rshift_func_uint8_t_u_u((safe_unary_minus_func_int32_t_s(p_52)), 7)) > (-10L))) , l_1208)))))) >= p_52) < g_1133) <= 0xCF804A1DL) != (**g_1146)))) ^ (-1L))))), 3)) | l_1279)) <= 0x4D1C8F94L) , 0x2C4FL))), p_52)), 4L))))) >= 5UL);
        }
        else
        { /* block id: 579 */
            int32_t l_1324 = 0L;
            uint64_t l_1325 = 18446744073709551614UL;
            const int64_t ***l_1356 = (void*)0;
            for (g_346 = 0; (g_346 >= 24); g_346++)
            { /* block id: 582 */
                int32_t l_1307 = 0L;
                for (g_1230 = 5; (g_1230 >= 25); g_1230++)
                { /* block id: 585 */
                    uint32_t l_1314 = 0x4A8A12E6L;
                    int32_t l_1315 = 0x8EF772C9L;
                    int32_t **l_1322 = &g_746;
                    for (l_1206 = 0; (l_1206 <= 2); l_1206 += 1)
                    { /* block id: 588 */
                        int32_t **l_1323 = &l_66;
                        int i, j;
                        if (l_1307)
                            break;
                        (*g_1068) = (((safe_div_func_int64_t_s_s((p_52 | l_1272), ((safe_lshift_func_uint8_t_u_u((safe_div_func_int32_t_s_s(l_1314, g_62)), ((l_1315 = ((void*)0 == &g_1077[1])) < l_1314))) , (((safe_lshift_func_int16_t_s_s((safe_sub_func_uint64_t_u_u(((safe_lshift_func_int8_t_s_s(((-1L) == (l_1322 != l_1323)), 7)) , g_115), l_1315)), 10)) , p_52) && 3L)))) , (void*)0) != (void*)0);
                    }
                }
                l_1324 ^= l_1302[2];
            }
            (*g_1068) ^= l_1325;
            if ((((*g_1147) , l_1326[1]) != l_1326[1]))
            { /* block id: 597 */
                uint16_t l_1341 = 0x05A5L;
                int32_t l_1347 = 1L;
                int32_t l_1348[8];
                int i;
                for (i = 0; i < 8; i++)
                    l_1348[i] = 0x5DEBA6BEL;
                for (l_1325 = 0; (l_1325 >= 57); l_1325 = safe_add_func_int32_t_s_s(l_1325, 4))
                { /* block id: 600 */
                    const int8_t l_1342 = 1L;
                    if ((safe_mul_func_uint16_t_u_u(((((((void*)0 == &l_61) != ((((((0x0EBE288AL && 6UL) && (safe_lshift_func_uint8_t_u_s(0x25L, l_1302[6]))) < ((*g_1074) = p_52)) & (safe_lshift_func_uint8_t_u_s(p_52, 4))) < (safe_mul_func_uint8_t_u_u(((safe_add_func_int16_t_s_s(((l_1341 ^ 0x0B54L) , p_52), l_1342)) || 0x58AE1C25L), p_52))) == 0xA2063200E52FDBF5LL)) >= 1L) , l_1325) ^ l_1341), 0x22ADL)))
                    { /* block id: 602 */
                        uint8_t l_1345 = 0x95L;
                        l_1345 &= (l_1342 >= (safe_lshift_func_int8_t_s_s(p_52, 3)));
                        return p_52;
                    }
                    else
                    { /* block id: 605 */
                        return p_52;
                    }
                }
                (*g_1131) = (*g_1131);
                (*g_1131) = func_57(((((l_1348[3] = (~(l_1347 = g_872))) == (0x7321L & (~(((safe_add_func_int32_t_s_s((-1L), ((g_251 , (safe_rshift_func_int8_t_s_u((2UL | 0x2096L), (*g_1012)))) , (((safe_add_func_uint16_t_u_u(((l_1356 == (void*)0) > 0x090BBAD0L), l_1302[0])) | (***g_1010)) & g_18)))) < l_1341) , p_52)))) && l_1347) < p_52), p_52);
            }
            else
            { /* block id: 613 */
                uint16_t l_1357 = 0x3160L;
                l_1357 = 1L;
            }
        }
        for (g_167 = 0; (g_167 < 26); g_167 = safe_add_func_uint8_t_u_u(g_167, 4))
        { /* block id: 619 */
            int8_t **l_1360[2][4][9] = {{{(void*)0,(void*)0,(void*)0,&g_1147,&g_1147,&g_1147,(void*)0,(void*)0,(void*)0},{&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147},{(void*)0,(void*)0,(void*)0,&g_1147,&g_1147,&g_1147,(void*)0,(void*)0,(void*)0},{&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147}},{{(void*)0,(void*)0,(void*)0,&g_1147,&g_1147,&g_1147,(void*)0,(void*)0,(void*)0},{&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147},{(void*)0,(void*)0,(void*)0,&g_1147,&g_1147,&g_1147,(void*)0,(void*)0,(void*)0},{&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147,&g_1147}}};
            int32_t l_1362 = 0x75D96ED5L;
            int i, j, k;
            l_1361 = l_1360[0][2][6];
            (*l_1156) = (*l_1156);
            l_1252 = ((l_1362 = 0xB3F2L) , ((((safe_add_func_int64_t_s_s((p_52 & ((g_115 , ((p_52 >= ((!(safe_add_func_int8_t_s_s(l_1302[6], 0x4DL))) > ((safe_add_func_int16_t_s_s(((*l_1205) = ((safe_mod_func_int8_t_s_s((*g_1147), (5UL ^ (g_872 && l_1202[7])))) > p_52)), l_1372)) != p_52))) > 0xC5L)) , l_1372)), l_1362)) || (-7L)) >= g_84) , p_52));
        }
    }
    else
    { /* block id: 626 */
        (*g_1010) = (l_1373 = (void*)0);
    }
    return p_52;
}


/* ------------------------------------------ */
/* 
 * reads : g_346 g_1076 g_1077 g_60 g_34 g_205 g_17 g_18 g_84 g_1068 g_802 g_290
 * writes: g_346 g_660 g_205 g_60 g_84 g_62 g_34 g_802 g_290
 */
static int32_t * func_53(int32_t * p_54, int32_t ** p_55, int8_t  p_56)
{ /* block id: 538 */
    int64_t l_1165 = 8L;
    int32_t l_1167 = 0x661ABA3AL;
    int32_t l_1168 = 0L;
    for (g_346 = 0; (g_346 >= 25); g_346 = safe_add_func_int8_t_s_s(g_346, 5))
    { /* block id: 541 */
        int32_t l_1166 = 0x560176F4L;
        uint32_t l_1169 = 1UL;
        int32_t *l_1175 = &g_346;
        int32_t ** const l_1174 = &l_1175;
        int32_t ** const *l_1173[6];
        int32_t ** const **l_1172 = &l_1173[1];
        uint32_t *l_1183 = &g_205;
        int i;
        for (i = 0; i < 6; i++)
            l_1173[i] = &l_1174;
        for (g_660 = (-2); (g_660 <= 25); g_660++)
        { /* block id: 544 */
            int64_t l_1161 = 8L;
            int32_t *l_1162 = &g_122;
            int32_t *l_1163 = (void*)0;
            int32_t *l_1164[7][1][1] = {{{&g_18}},{{&g_34}},{{&g_18}},{{&g_34}},{{&g_18}},{{&g_34}},{{&g_18}}};
            int i, j, k;
            l_1169--;
        }
        (*p_55) = func_57(((l_1165 , ((*g_1076) == ((*l_1172) = (*g_1076)))) , p_56), (safe_sub_func_int64_t_s_s(((g_60 > (((+(safe_add_func_int16_t_s_s(p_56, g_34))) == ((*l_1183)++)) != ((((l_1168 != l_1169) ^ (*g_17)) ^ 247UL) , p_56))) > 5L), l_1167)));
    }
    return (*p_55);
}


/* ------------------------------------------ */
/* 
 * reads : g_60 g_84 g_18 g_17 g_1068 g_34
 * writes: g_60 g_84 g_62 g_34
 */
static int32_t * func_57(int32_t  p_58, int16_t  p_59)
{ /* block id: 8 */
    uint16_t l_68[8];
    int32_t *l_82[9][7][4] = {{{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,(void*)0},{(void*)0,(void*)0,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,(void*)0,&g_60}},{{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60}},{{&g_60,&g_60,(void*)0,&g_60},{(void*)0,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,(void*)0,&g_60},{&g_60,&g_60,&g_60,&g_60}},{{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60}},{{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,(void*)0,&g_60},{(void*)0,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,(void*)0,&g_60}},{{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,&g_60}},{{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,(void*)0,&g_60},{(void*)0,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60}},{{(void*)0,&g_60,(void*)0,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{&g_60,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_60},{(void*)0,&g_60,&g_60,&g_60}},{{&g_60,(void*)0,(void*)0,&g_60},{&g_60,&g_60,&g_60,(void*)0},{&g_60,&g_60,&g_60,(void*)0},{(void*)0,&g_60,&g_60,&g_60},{&g_60,(void*)0,&g_60,&g_60},{&g_60,&g_60,(void*)0,&g_60},{&g_60,&g_60,(void*)0,(void*)0}}};
    uint16_t *l_83 = &g_84;
    int16_t l_89 = (-4L);
    int i, j, k;
    for (i = 0; i < 8; i++)
        l_68[i] = 65532UL;
    for (g_60 = 0; g_60 < 8; g_60 += 1)
    {
        l_68[g_60] = 0xEE49L;
    }
    (*g_1068) &= func_69(((safe_mul_func_uint8_t_u_u((&p_58 == &g_60), ((safe_unary_minus_func_int8_t_s((((safe_add_func_int8_t_s_s(((((safe_mul_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(l_68[3], (safe_rshift_func_uint16_t_u_u((l_82[6][2][1] != &g_60), ((*l_83) &= g_60))))), (safe_lshift_func_uint8_t_u_s((((l_68[5] , (0xF3L ^ ((safe_lshift_func_int8_t_s_s(g_18, 5)) , g_18))) || l_68[0]) & (*g_17)), p_58)))) , g_84) && 0xB1BDA2DDL) ^ p_59), p_58)) < p_58) , 0x14L))) , l_89))) , 0x60L));
    return l_82[6][2][1];
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes: g_84 g_60 g_62
 */
static int32_t  func_69(int8_t  p_70)
{ /* block id: 11 */
    uint32_t l_90[9] = {0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL,0UL};
    int32_t l_106[8][1];
    uint8_t l_150 = 0x82L;
    int32_t **l_155 = &g_17;
    int32_t *l_184[10] = {&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0],&l_106[4][0]};
    int32_t *l_185 = &g_122;
    int32_t l_186 = 0L;
    int8_t l_206 = 0xFBL;
    uint32_t l_213 = 1UL;
    int32_t *l_231 = &g_34;
    int16_t *l_272 = &g_167;
    uint8_t *l_277[1];
    int32_t ***l_288[4][7] = {{&l_155,(void*)0,&l_155,&l_155,(void*)0,&l_155,&l_155},{&l_155,&l_155,&l_155,&l_155,&l_155,&l_155,&l_155},{(void*)0,(void*)0,(void*)0,&l_155,&l_155,&l_155,&l_155},{&l_155,&l_155,&l_155,&l_155,&l_155,&l_155,&l_155}};
    uint64_t *l_301 = &g_147;
    uint16_t *l_302 = &g_84;
    uint16_t l_303 = 0x7F06L;
    int16_t *l_304 = &g_124;
    uint8_t l_343 = 0x33L;
    int8_t l_369[10][7][1] = {{{0xA2L},{(-1L)},{(-1L)},{(-1L)},{1L},{(-1L)},{0x4BL}},{{(-1L)},{1L},{(-1L)},{(-1L)},{(-1L)},{0xA2L},{(-1L)}},{{(-1L)},{(-1L)},{1L},{(-1L)},{0x4BL},{(-1L)},{1L}},{{(-1L)},{(-1L)},{(-1L)},{0xA2L},{(-1L)},{(-1L)},{(-1L)}},{{1L},{(-1L)},{0x4BL},{(-1L)},{1L},{(-1L)},{(-1L)}},{{(-1L)},{0xA2L},{(-1L)},{(-1L)},{(-1L)},{1L},{(-1L)}},{{0x4BL},{(-1L)},{1L},{(-1L)},{(-1L)},{(-1L)},{0xA2L}},{{(-1L)},{(-1L)},{(-1L)},{1L},{(-1L)},{0x4BL},{(-1L)}},{{1L},{(-1L)},{(-1L)},{(-1L)},{0xA2L},{(-1L)},{(-1L)}},{{(-1L)},{1L},{(-1L)},{0x4BL},{(-1L)},{1L},{(-1L)}}};
    int8_t *l_420 = &g_139;
    int32_t *l_540 = &l_186;
    int64_t *l_670 = (void*)0;
    int64_t **l_669 = &l_670;
    uint16_t l_709 = 0xBE66L;
    int32_t l_923[8][2] = {{5L,2L},{2L,0x3A5436E7L},{2L,0x3A5436E7L},{2L,2L},{5L,5L},{5L,2L},{2L,0x3A5436E7L},{2L,0x3A5436E7L}};
    uint8_t l_926 = 0UL;
    int32_t l_1003[6][10] = {{0L,0x25462338L,0x0CC97866L,0L,0x137B664BL,0x137B664BL,0L,0x0CC97866L,0x25462338L,0L},{0x0CC97866L,(-1L),0x25462338L,0x137B664BL,(-1L),0x137B664BL,0x25462338L,(-1L),0x0CC97866L,0x0CC97866L},{0L,0xA22B8BD4L,0x96AE9928L,(-1L),(-1L),0x96AE9928L,0xA22B8BD4L,0x137B664BL,(-1L),0x137B664BL},{0x0CC97866L,0x96AE9928L,1L,0x0CC97866L,1L,0x96AE9928L,0x0CC97866L,5L,5L,0x0CC97866L},{5L,0x137B664BL,1L,1L,0x137B664BL,5L,0x96AE9928L,0x137B664BL,0x96AE9928L,5L},{0x25462338L,0x137B664BL,(-1L),0x137B664BL,0x25462338L,(-1L),0x0CC97866L,0x0CC97866L,(-1L),0x25462338L}};
    int64_t l_1004 = 0x5B1EC5E60AD1DC33LL;
    uint64_t l_1006 = 7UL;
    int32_t *** const * const l_1075 = (void*)0;
    int i, j, k;
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 1; j++)
            l_106[i][j] = (-10L);
    }
    for (i = 0; i < 1; i++)
        l_277[i] = &l_150;
    for (g_84 = 0; (g_84 <= 8); g_84 += 1)
    { /* block id: 14 */
        int32_t *l_104 = (void*)0;
        const int32_t *l_113 = &g_34;
        const int32_t **l_112 = &l_113;
        int32_t l_116 = (-1L);
        int32_t l_117[7];
        uint32_t l_141 = 0x379A3785L;
        uint64_t l_202 = 0UL;
        int i;
        for (i = 0; i < 7; i++)
            l_117[i] = 0x61D8FC8FL;
        for (g_60 = 0; g_60 < 9; g_60 += 1)
        {
            l_90[g_60] = 1UL;
        }
        if (l_90[0])
            continue;
        for (g_62 = 1; (g_62 <= 8); g_62 += 1)
        { /* block id: 19 */
            uint64_t l_109[9] = {1UL,0xBE831782E4D72E8FLL,0xBE831782E4D72E8FLL,1UL,0xBE831782E4D72E8FLL,0xBE831782E4D72E8FLL,1UL,0xBE831782E4D72E8FLL,0xBE831782E4D72E8FLL};
            int64_t *l_114 = &g_115;
            int32_t l_121 = 0x904BE6A8L;
            int32_t l_123[8][3] = {{4L,4L,4L},{(-8L),(-8L),(-8L)},{4L,4L,4L},{(-8L),(-8L),(-8L)},{4L,4L,4L},{(-8L),(-8L),(-8L)},{4L,4L,4L},{(-8L),(-8L),(-8L)}};
            int16_t *l_195[6];
            uint32_t *l_203 = &l_90[8];
            uint32_t *l_204 = &g_205;
            int32_t *l_250 = &l_116;
            int i, j;
            for (i = 0; i < 6; i++)
                l_195[i] = &g_171;
        }
    }
    return p_70;
}




/* ---------------------------------------- */
//testcase_id 1484153271
int case1484153271(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_6, "g_6", print_hash_value);
    transparent_crc(g_18, "g_18", print_hash_value);
    transparent_crc(g_34, "g_34", print_hash_value);
    transparent_crc(g_60, "g_60", print_hash_value);
    transparent_crc(g_62, "g_62", print_hash_value);
    transparent_crc(g_84, "g_84", print_hash_value);
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc(g_124, "g_124", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_139, "g_139", print_hash_value);
    transparent_crc(g_144, "g_144", print_hash_value);
    transparent_crc(g_147, "g_147", print_hash_value);
    transparent_crc(g_167, "g_167", print_hash_value);
    transparent_crc(g_171, "g_171", print_hash_value);
    transparent_crc(g_205, "g_205", print_hash_value);
    transparent_crc(g_251, "g_251", print_hash_value);
    transparent_crc(g_342, "g_342", print_hash_value);
    transparent_crc(g_346, "g_346", print_hash_value);
    transparent_crc(g_377, "g_377", print_hash_value);
    transparent_crc(g_407, "g_407", print_hash_value);
    transparent_crc(g_435, "g_435", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 9; k++)
            {
                transparent_crc(g_645[i][j][k], "g_645[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_660, "g_660", print_hash_value);
    transparent_crc(g_815, "g_815", print_hash_value);
    transparent_crc(g_826, "g_826", print_hash_value);
    transparent_crc(g_872, "g_872", print_hash_value);
    transparent_crc(g_922, "g_922", print_hash_value);
    transparent_crc(g_1133, "g_1133", print_hash_value);
    transparent_crc(g_1230, "g_1230", print_hash_value);
    transparent_crc(g_1231, "g_1231", print_hash_value);
    transparent_crc(g_1375, "g_1375", print_hash_value);
    transparent_crc(g_1420, "g_1420", print_hash_value);
    for (i = 0; i < 9; i++)
    {
        transparent_crc(g_1472[i], "g_1472[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 10; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_1592[i][j], "g_1592[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1829, "g_1829", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_1886[i], "g_1886[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2040, "g_2040", print_hash_value);
    transparent_crc(g_2240, "g_2240", print_hash_value);
    transparent_crc(g_2290, "g_2290", print_hash_value);
    transparent_crc(g_2329, "g_2329", print_hash_value);
    transparent_crc(g_2380, "g_2380", print_hash_value);
    transparent_crc(g_2391, "g_2391", print_hash_value);
    transparent_crc(g_2397, "g_2397", print_hash_value);
    for (i = 0; i < 2; i++)
    {
        transparent_crc(g_2419[i], "g_2419[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_2524, "g_2524", print_hash_value);
    transparent_crc(g_2665, "g_2665", print_hash_value);
    transparent_crc(g_2695, "g_2695", print_hash_value);
    transparent_crc(g_2696, "g_2696", print_hash_value);
    transparent_crc(g_2775, "g_2775", print_hash_value);
    transparent_crc(g_2884, "g_2884", print_hash_value);
    transparent_crc(g_2921, "g_2921", print_hash_value);
    transparent_crc(g_2922, "g_2922", print_hash_value);
    transparent_crc(g_2929, "g_2929", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 726
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 40
breakdown:
   depth: 1, occurrence: 95
   depth: 2, occurrence: 26
   depth: 3, occurrence: 3
   depth: 4, occurrence: 1
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 9, occurrence: 1
   depth: 14, occurrence: 1
   depth: 16, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 1
   depth: 21, occurrence: 4
   depth: 22, occurrence: 1
   depth: 23, occurrence: 1
   depth: 24, occurrence: 3
   depth: 27, occurrence: 2
   depth: 28, occurrence: 2
   depth: 29, occurrence: 2
   depth: 32, occurrence: 1
   depth: 40, occurrence: 1

XXX total number of pointers: 506

XXX times a variable address is taken: 1277
XXX times a pointer is dereferenced on RHS: 350
breakdown:
   depth: 1, occurrence: 276
   depth: 2, occurrence: 54
   depth: 3, occurrence: 17
   depth: 4, occurrence: 3
XXX times a pointer is dereferenced on LHS: 438
breakdown:
   depth: 1, occurrence: 393
   depth: 2, occurrence: 38
   depth: 3, occurrence: 7
XXX times a pointer is compared with null: 65
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 30
XXX times a pointer is qualified to be dereferenced: 8718

XXX max dereference level: 7
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1835
   level: 2, occurrence: 329
   level: 3, occurrence: 174
   level: 4, occurrence: 50
   level: 5, occurrence: 34
   level: 6, occurrence: 6
   level: 7, occurrence: 6
XXX number of pointers point to pointers: 226
XXX number of pointers point to scalars: 280
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31
XXX average alias set size: 1.53

XXX times a non-volatile is read: 2452
XXX times a non-volatile is write: 1287
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 3
XXX backward jumps: 11

XXX stmts: 100
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 19
   depth: 2, occurrence: 19
   depth: 3, occurrence: 13
   depth: 4, occurrence: 10
   depth: 5, occurrence: 10

XXX percentage a fresh-made variable is used: 16.5
XXX percentage an existing variable is used: 83.5
********************* end of statistics **********************/

