/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.3.0
 * Git version: 
 * Options:   --no_main_use
 * Seed:      3940921563
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static int8_t g_7 = 9L;
static int32_t g_9 = (-1L);
static int32_t g_13 = 0L;
static uint32_t g_16[6][8][4] = {{{7UL,0xCB1743E6L,1UL,9UL},{0x8278B721L,0x5B3659EAL,0x3245E346L,0x8278B721L},{4294967295UL,1UL,4294967295UL,0x8278B721L},{4294967295UL,0x5B3659EAL,1UL,9UL},{2UL,0xCB1743E6L,0xEC1A2C9DL,0x8278B721L},{0x8278B721L,0x32F82966L,0x32F82966L,0x8278B721L},{0UL,0xCB1743E6L,4294967295UL,9UL},{4294967295UL,0x5B3659EAL,0x7D413053L,0x8278B721L}},{{2UL,1UL,1UL,0x8278B721L},{9UL,0x5B3659EAL,0x32F82966L,9UL},{4294967295UL,0xCB1743E6L,1UL,0x8278B721L},{4294967295UL,0x32F82966L,0x7D413053L,9UL},{0x185F1871L,1UL,0xEC1A2C9DL,0x73637828L},{9UL,0x32F82966L,0x5B3659EAL,9UL},{0UL,0x7D413053L,1UL,9UL},{0UL,0x32F82966L,0x7D413053L,0x73637828L}},{{7UL,1UL,1UL,9UL},{9UL,0x3245E346L,0x3245E346L,9UL},{4294967295UL,1UL,1UL,0x73637828L},{4294967295UL,0x32F82966L,0xCB1743E6L,9UL},{7UL,0x7D413053L,0xEC1A2C9DL,9UL},{0x73637828L,0x32F82966L,0x3245E346L,0x73637828L},{0UL,1UL,4294967291UL,9UL},{4294967295UL,0x3245E346L,0x7D413053L,9UL}},{{0x185F1871L,1UL,0xEC1A2C9DL,0x73637828L},{9UL,0x32F82966L,0x5B3659EAL,9UL},{0UL,0x7D413053L,1UL,9UL},{0UL,0x32F82966L,0x7D413053L,0x73637828L},{7UL,1UL,1UL,9UL},{9UL,0x3245E346L,0x3245E346L,9UL},{4294967295UL,1UL,1UL,0x73637828L},{4294967295UL,0x32F82966L,0xCB1743E6L,9UL}},{{7UL,0x7D413053L,0xEC1A2C9DL,9UL},{0x73637828L,0x32F82966L,0x3245E346L,0x73637828L},{0UL,1UL,4294967291UL,9UL},{4294967295UL,0x3245E346L,0x7D413053L,9UL},{0x185F1871L,1UL,0xEC1A2C9DL,0x73637828L},{9UL,0x32F82966L,0x5B3659EAL,9UL},{0UL,0x7D413053L,1UL,9UL},{0UL,0x32F82966L,0x7D413053L,0x73637828L}},{{7UL,1UL,1UL,9UL},{9UL,0x3245E346L,0x3245E346L,9UL},{4294967295UL,1UL,1UL,0x73637828L},{4294967295UL,0x32F82966L,0xCB1743E6L,9UL},{7UL,0x7D413053L,0xEC1A2C9DL,9UL},{0x73637828L,0x32F82966L,0x3245E346L,0x73637828L},{0UL,1UL,4294967291UL,9UL},{4294967295UL,0x3245E346L,0x7D413053L,9UL}}};
static const int32_t *g_52 = &g_13;
static const int32_t **g_51 = &g_52;
static const int32_t ***g_50[9] = {&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51,&g_51};
static uint64_t g_95 = 18446744073709551614UL;
static uint32_t g_115 = 0x034421CFL;
static int32_t g_118 = 2L;
static int64_t g_130[1] = {0xCCA4C161D2AD6A2FLL};
static uint8_t g_135 = 0xADL;
static int16_t g_167[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint8_t g_169 = 255UL;
static uint8_t *g_168 = &g_169;
static uint32_t *g_191 = &g_115;
static uint16_t g_195 = 65535UL;
static uint8_t g_198 = 0xBBL;
static uint64_t g_201 = 18446744073709551615UL;
static const uint64_t *g_229 = &g_95;
static const uint64_t **g_228 = &g_229;
static const uint16_t **g_236 = (void*)0;
static int16_t g_240[10] = {0x2E7FL,0x4FD3L,0x4FD3L,0x2E7FL,0x6031L,0x2E7FL,0x4FD3L,0x4FD3L,0x2E7FL,0x6031L};
static int8_t g_241 = 5L;
static int8_t g_242 = 0xA7L;
static uint64_t g_243 = 0xA4616B117CF7601CLL;
static uint16_t g_268[3][4] = {{65533UL,1UL,65533UL,1UL},{65533UL,1UL,65533UL,1UL},{65533UL,1UL,65533UL,1UL}};
static uint16_t g_272 = 65533UL;
static uint16_t *g_289 = &g_195;
static uint16_t **g_288 = &g_289;
static uint16_t ***g_287 = &g_288;
static uint32_t g_297 = 0x7259765AL;
static uint64_t g_319 = 2UL;
static int32_t g_320[5] = {0x7E97E9C7L,0x7E97E9C7L,0x7E97E9C7L,0x7E97E9C7L,0x7E97E9C7L};
static int8_t g_321 = 0xF4L;
static uint64_t g_322 = 0xDE79FB1DFDDE1CF8LL;
static uint16_t g_435 = 0x5309L;
static uint64_t g_442[5][2] = {{18446744073709551611UL,18446744073709551611UL},{18446744073709551611UL,18446744073709551611UL},{18446744073709551611UL,18446744073709551611UL},{18446744073709551611UL,18446744073709551611UL},{18446744073709551611UL,18446744073709551611UL}};
static int8_t g_447 = (-1L);
static uint32_t g_450 = 5UL;
static uint8_t g_477 = 255UL;
static int32_t g_505 = 0L;
static int8_t g_518 = 0x30L;
static uint8_t g_519[8][5] = {{0x64L,248UL,0x68L,248UL,0x64L},{9UL,0xBCL,252UL,0x11L,0UL},{252UL,0xBCL,9UL,9UL,0xBCL},{0x68L,248UL,0x64L,0xBCL,0UL},{248UL,9UL,0x64L,0xEAL,0x64L},{0UL,0UL,9UL,0x68L,6UL},{248UL,6UL,252UL,0x68L,0x68L},{0x68L,0x2FL,0x68L,0xEAL,0x11L}};
static uint32_t g_523 = 18446744073709551610UL;
static const int64_t *g_531[4] = {&g_130[0],&g_130[0],&g_130[0],&g_130[0]};
static const int64_t **g_530 = &g_531[1];
static uint16_t g_541 = 0xB15FL;
static const uint32_t g_582[3][1][1] = {{{8UL}},{{8UL}},{{8UL}}};
static const uint32_t g_584 = 0x004EAEB8L;
static uint32_t g_590 = 0x67D6B026L;
static int32_t g_595 = 1L;
static int8_t g_597 = 0L;
static int32_t g_598 = 0x6E6E472AL;
static int16_t g_599 = 0xAE26L;
static uint32_t g_601 = 18446744073709551614UL;
static int64_t g_616 = 0x2332A4DB88C4D0D5LL;
static int16_t g_784[10] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint8_t g_785[3][2] = {{0xA6L,0xA6L},{255UL,0xA6L},{0xA6L,255UL}};
static uint16_t * const *g_805[4] = {&g_289,&g_289,&g_289,&g_289};
static uint16_t * const **g_804 = &g_805[3];
static uint8_t g_881 = 246UL;
static uint16_t ****g_914 = &g_287;
static uint16_t *****g_913 = &g_914;
static int32_t g_923 = 0xB0FE9B1EL;
static int32_t g_924 = 0xAC3C8AF9L;
static int32_t g_925[5] = {(-1L),(-1L),(-1L),(-1L),(-1L)};
static uint32_t g_926 = 4294967295UL;
static uint64_t g_962 = 0xC1E0DBE341575B65LL;
static int32_t g_1025[3][9] = {{0x7A5EB06CL,0xBCA1F082L,(-1L),0xD4F92C03L,0xB04ED563L,0xB04ED563L,0xD4F92C03L,(-1L),0xBCA1F082L},{0x363F3E3DL,(-1L),0x7A5EB06CL,0x577CC752L,0x70F7AA96L,0L,0L,0x70F7AA96L,0x577CC752L},{0x363F3E3DL,0x01E7AD95L,0x363F3E3DL,0L,0xD4F92C03L,0x7A5EB06CL,0xF2FC1E55L,0xF2FC1E55L,0x7A5EB06CL}};
static int32_t *g_1136 = (void*)0;
static int32_t * const *g_1135 = &g_1136;
static int32_t * const **g_1134[3][8][9] = {{{(void*)0,&g_1135,(void*)0,&g_1135,&g_1135,(void*)0,&g_1135,(void*)0,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0,&g_1135},{&g_1135,(void*)0,&g_1135,&g_1135,(void*)0,&g_1135,(void*)0,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0},{&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0,&g_1135,&g_1135},{(void*)0,&g_1135,&g_1135,(void*)0,&g_1135,(void*)0,&g_1135,&g_1135,(void*)0},{&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135}},{{&g_1135,&g_1135,(void*)0,(void*)0,&g_1135,&g_1135,&g_1135,(void*)0,(void*)0},{&g_1135,&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0},{(void*)0,&g_1135,(void*)0,(void*)0,(void*)0,(void*)0,&g_1135,(void*)0,(void*)0},{&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,(void*)0,&g_1135,&g_1135,(void*)0,&g_1135,(void*)0},{&g_1135,&g_1135,(void*)0,(void*)0,&g_1135,&g_1135,&g_1135,(void*)0,(void*)0},{&g_1135,&g_1135,(void*)0,&g_1135,(void*)0,&g_1135,&g_1135,(void*)0,&g_1135},{&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135}},{{(void*)0,(void*)0,(void*)0,(void*)0,&g_1135,(void*)0,(void*)0,(void*)0,(void*)0},{&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0,&g_1135},{(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135},{&g_1135,&g_1135,&g_1135,(void*)0,&g_1135,&g_1135,&g_1135,&g_1135,(void*)0},{(void*)0,&g_1135,(void*)0,(void*)0,(void*)0,(void*)0,&g_1135,(void*)0,(void*)0},{&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135,&g_1135},{&g_1135,(void*)0,&g_1135,(void*)0,&g_1135,&g_1135,(void*)0,&g_1135,(void*)0},{&g_1135,&g_1135,(void*)0,(void*)0,&g_1135,&g_1135,&g_1135,(void*)0,(void*)0}}};
static uint8_t g_1182 = 0xBCL;
static uint32_t ***g_1300 = (void*)0;
static int32_t **g_1356 = &g_1136;
static int32_t ***g_1355 = &g_1356;
static const uint8_t *g_1381 = (void*)0;
static const uint8_t **g_1380 = &g_1381;
static const uint8_t ***g_1379 = &g_1380;
static int32_t g_1391[3][6][6] = {{{1L,0x55A22220L,0x181251F1L,0x4F3DB47FL,0x4F3DB47FL,0x181251F1L},{0x181251F1L,0x181251F1L,0xF9B3272EL,0x672F974EL,0xC09B16ACL,7L},{(-6L),0x4A44F4E5L,0xF435C3C1L,0x45761D47L,0L,0xF9B3272EL},{(-1L),(-6L),0xF435C3C1L,0L,0x181251F1L,7L},{0xF0E819A0L,0L,0xF9B3272EL,(-1L),0xE1CE468FL,0x181251F1L},{(-1L),0xE1CE468FL,0x181251F1L,0x81E6C21CL,0x18F63EFAL,0L}},{{1L,0xF9B3272EL,0xAFF8921CL,0x13E1AC5DL,0xCAA8DD4CL,0x13E1AC5DL},{0xCAA8DD4CL,0x93C900BEL,0xCAA8DD4CL,2L,0xF0E819A0L,1L},{0x45761D47L,(-1L),0L,0xAFF8921CL,1L,0x18F63EFAL},{0x4A44F4E5L,0x672F974EL,0L,0xAFF8921CL,(-1L),0xE1CE468FL},{0x81E6C21CL,1L,(-1L),0xE1CE468FL,0xAFF8921CL,0xBCA479DDL},{1L,0x13E1AC5DL,0L,1L,0xF0E819A0L,0x4A44F4E5L}},{{(-1L),0L,1L,0x672F974EL,0x55A22220L,0x55A22220L},{0xF9B3272EL,0x181251F1L,0x181251F1L,0xF9B3272EL,0x672F974EL,0xC09B16ACL},{0L,7L,(-6L),0xAFF8921CL,(-8L),0L},{0x93C900BEL,0xF9B3272EL,(-1L),0x81E6C21CL,(-8L),0L},{0x55A22220L,7L,0xF0E819A0L,0x18F63EFAL,0x672F974EL,0xCAA8DD4CL},{7L,0x181251F1L,(-1L),0x45761D47L,0x55A22220L,0xAFF8921CL}}};
static int32_t * const g_1390 = &g_1391[1][2][2];
static int32_t * const *g_1389 = &g_1390;
static int32_t g_1394 = 0x36CB3A4FL;
static int32_t * const g_1393 = &g_1394;
static int32_t * const *g_1392 = &g_1393;
static int8_t *g_1439 = &g_447;
static int8_t **g_1438[4][2][10] = {{{&g_1439,&g_1439,(void*)0,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439},{&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439}},{{&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439},{&g_1439,&g_1439,(void*)0,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439}},{{&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439},{&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439}},{{&g_1439,&g_1439,(void*)0,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439},{&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439}}};
static int64_t *g_1489 = (void*)0;
static uint16_t *** const ***g_1510 = (void*)0;
static int32_t *****g_1573 = (void*)0;
static int8_t g_1622 = 4L;
static uint32_t g_1673 = 0xA7641A9EL;
static int32_t g_1789 = 0xF8554C61L;
static int32_t g_1842 = 0xB0A8DBB2L;
static int32_t g_1896[7] = {8L,8L,8L,8L,8L,8L,8L};
static uint16_t g_1913[5][3][4] = {{{0UL,0UL,8UL,65530UL},{0xDB4FL,0xC642L,0x4031L,0UL},{0x3EA4L,0UL,0x4031L,65531UL}},{{0xDB4FL,65529UL,8UL,0x32BAL},{0UL,0UL,0UL,0UL},{0UL,0UL,0xC642L,0x3EA4L}},{{65529UL,0xDB4FL,65535UL,65530UL},{0UL,0x3EA4L,0x72ECL,65530UL},{0xC642L,0xDB4FL,0x4031L,0x3EA4L}},{{0UL,0UL,0x9D62L,0UL},{0xDB4FL,0UL,0x72ECL,0x32BAL},{65531UL,65529UL,0UL,65531UL}},{{65529UL,0UL,0UL,0UL},{65529UL,0xC642L,0UL,0x4031L},{65535UL,0x72ECL,0x9D62L,0x9D62L}}};
static const uint16_t *****g_1940 = (void*)0;
static const uint16_t ******g_1939 = &g_1940;
static uint16_t ******g_1941 = &g_913;
static const int8_t g_1960 = 9L;
static uint64_t *g_2062 = &g_201;
static uint64_t **g_2061 = &g_2062;
static const int32_t *g_2135 = &g_925[1];
static uint8_t **g_2150 = &g_168;
static uint8_t ***g_2149 = &g_2150;
static int32_t *g_2192[9] = {&g_1896[4],&g_1896[4],&g_1896[4],&g_1896[4],&g_1896[4],&g_1896[4],&g_1896[4],&g_1896[4],&g_1896[4]};
static int32_t ** const g_2191 = &g_2192[4];
static int32_t ** const *g_2190 = &g_2191;
static uint8_t ****g_2249 = &g_2149;
static int32_t ****g_2324 = &g_1355;
static int32_t *****g_2323 = &g_2324;
static int32_t **g_2342[7] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
static int32_t *** const g_2341[8] = {&g_2342[1],&g_2342[1],&g_2342[1],&g_2342[1],&g_2342[1],&g_2342[1],&g_2342[1],&g_2342[1]};
static int32_t *** const *g_2340 = &g_2341[0];
static int64_t **g_2466 = &g_1489;


/* --- FORWARD DECLARATIONS --- */
static uint32_t  func_1(void);
static int16_t  func_5(uint8_t  p_6);
static int32_t ** func_23(int32_t  p_24, int32_t * const * p_25, int8_t  p_26, int32_t * const  p_27, uint32_t  p_28);
static int32_t * const * func_29(const int16_t  p_30, int32_t *** p_31, int32_t ** const * p_32, int32_t * p_33);
static const int32_t  func_34(int32_t  p_35, uint16_t  p_36, int32_t *** p_37, int32_t *** p_38, const uint8_t  p_39);
static int64_t  func_47(const int32_t *** p_48, int32_t *** p_49);
static int32_t * const ** func_69(uint64_t  p_70, int32_t * p_71);
static uint16_t  func_74(int32_t * p_75, const uint16_t  p_76);
static int32_t * func_77(uint32_t  p_78, int64_t  p_79);
static uint32_t  func_80(uint32_t * p_81, uint64_t  p_82, int32_t  p_83, uint32_t  p_84, uint8_t  p_85);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_288 g_289 g_195 g_13 g_229 g_95 g_191 g_115 g_804 g_805 g_168 g_228 g_595 g_1393 g_1394 g_1789 g_130 g_913 g_914 g_287 g_1439 g_447 g_321 g_2149 g_2150 g_169 g_51 g_52 g_241 g_2062 g_1356 g_530 g_531 g_2135 g_925 g_201 g_243 g_1135 g_1136 g_601 g_2061 g_1913 g_167 g_1355 g_1941 g_2249 g_962 g_1573 g_2323 g_616 g_16 g_268
 * writes: g_1939 g_1941 g_13 g_240 g_169 g_195 g_595 g_321 g_2149 g_616 g_130 g_241 g_2190 g_201 g_1136 g_243 g_601 g_1789 g_2249 g_52 g_1573 g_2323 g_2340 g_447 g_115 g_2466
 */
static uint32_t  func_1(void)
{ /* block id: 0 */
    int64_t l_2 = 0xD33A4A9BDBA76AABLL;
    int32_t ***l_2163 = &g_1356;
    int64_t l_2202 = 0xAACBC86E03CDF2D7LL;
    int32_t l_2209 = 0x1208FD94L;
    uint16_t ******l_2228[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t l_2262 = 0x35598E66L;
    int32_t l_2263[2][9][6] = {{{0xF5D7FEC8L,0x7E0312D6L,0x70696F3AL,0x9A49B1EEL,0x02A200A2L,0xB65FDC62L},{0x9A49B1EEL,0x02A200A2L,0xB65FDC62L,0x4BC769D1L,0xFA54B11AL,(-1L)},{0x1593FD48L,0x70696F3AL,0xAC960770L,0x0619B836L,0xB0EBF55AL,0x0619B836L},{0xB0EBF55AL,(-6L),0xB0EBF55AL,0x626927F9L,0xF5D7FEC8L,0L},{9L,0xB031EBCAL,0xB4FB05DBL,0xAC960770L,0L,0xFA54B11AL},{0x6BE00A2DL,0x0A9883A0L,0x7E0312D6L,0xAC960770L,(-7L),0x626927F9L},{9L,0xB0EBF55AL,0x52707A77L,0x626927F9L,0x7E0312D6L,0x55C38462L},{0xB0EBF55AL,(-1L),0xD1D515CCL,0x0619B836L,0x72BBC3EDL,1L},{0x1593FD48L,0xF5D7FEC8L,0x6BE00A2DL,0x4BC769D1L,1L,1L}},{{0x9A49B1EEL,0x3D5E7916L,0x3D5E7916L,0x9A49B1EEL,0x4BC769D1L,0xAC960770L},{0xF5D7FEC8L,2L,0x02A200A2L,0x7E0312D6L,(-6L),0xD1D515CCL},{(-7L),0x9A49B1EEL,0x68565755L,9L,(-6L),1L},{1L,2L,0x72BBC3EDL,0x0A9883A0L,0x4BC769D1L,1L},{0xB65FDC62L,0x3D5E7916L,0x1593FD48L,0L,1L,0x7E0312D6L},{1L,0xF5D7FEC8L,0x0619B836L,1L,0x72BBC3EDL,0x3D5E7916L},{0x7E0312D6L,(-1L),1L,(-1L),0x7E0312D6L,0x9A49B1EEL},{(-2L),0xB0EBF55AL,0L,2L,(-7L),0xB4FB05DBL},{0xAC960770L,0x0A9883A0L,1L,0xB0EBF55AL,0L,0xB4FB05DBL}}};
    const int8_t l_2291 = 0xC6L;
    uint16_t l_2317 = 0x9C1BL;
    uint8_t l_2330 = 255UL;
    int64_t l_2374 = (-4L);
    uint32_t ****l_2440 = &g_1300;
    uint32_t l_2454 = 0xB01B5B14L;
    uint16_t *l_2473 = &g_1913[1][2][2];
    int32_t l_2475 = 0x3580E5CDL;
    int i, j, k;
    if (l_2)
    { /* block id: 1 */
        uint16_t l_2138[5] = {1UL,1UL,1UL,1UL,1UL};
        int32_t *l_2139 = &g_595;
        uint8_t l_2145 = 255UL;
        int32_t l_2146 = 9L;
        int32_t ***l_2164 = &g_1356;
        int32_t *l_2188 = &g_923;
        int32_t ** const l_2187 = &l_2188;
        int32_t ** const *l_2186[7] = {(void*)0,(void*)0,&l_2187,(void*)0,(void*)0,&l_2187,(void*)0};
        const uint32_t *l_2218[4];
        const uint32_t **l_2217[3][2] = {{&l_2218[1],&l_2218[1]},{&l_2218[1],&l_2218[1]},{&l_2218[1],&l_2218[1]}};
        uint32_t **l_2219 = &g_191;
        int32_t l_2259[7][1] = {{1L},{0x096D64F2L},{1L},{1L},{0x096D64F2L},{1L},{1L}};
        int16_t l_2260 = 0L;
        int64_t l_2261 = 0x33B7AFC70F3DCDA6LL;
        uint32_t l_2267 = 2UL;
        uint16_t ** const *l_2302 = (void*)0;
        uint32_t l_2358[2];
        uint64_t l_2441 = 0x832AB4624F099C83LL;
        int32_t l_2447 = 0x40F6EF4EL;
        int32_t *l_2448 = (void*)0;
        int32_t *l_2449 = (void*)0;
        int32_t *l_2450 = (void*)0;
        int32_t *l_2451 = &g_925[1];
        int32_t *l_2452 = &l_2263[1][7][4];
        int32_t *l_2453[3];
        int i, j;
        for (i = 0; i < 4; i++)
            l_2218[i] = &g_584;
        for (i = 0; i < 2; i++)
            l_2358[i] = 0x4609296FL;
        for (i = 0; i < 3; i++)
            l_2453[i] = &g_118;
        (*l_2139) |= (safe_rshift_func_uint16_t_u_s((func_5(g_7) & l_2138[0]), 0));
        if (((safe_sub_func_int32_t_s_s(((*l_2139) = (0x453829C4L ^ ((*l_2139) >= (safe_unary_minus_func_uint32_t_u(((*l_2139) == 1UL)))))), l_2)) > ((l_2 , l_2) >= ((((safe_mul_func_uint16_t_u_u((((((((((l_2 > ((void*)0 == &g_1380)) , (*g_1393)) , l_2145) >= l_2146) && 0x82E7L) | l_2138[2]) && 0x49E5L) || g_1789) && g_130[0]), (*****g_913))) <= l_2) ^ (*g_1439)) == (*g_191)))))
        { /* block id: 975 */
            uint16_t l_2158 = 65533UL;
            int32_t l_2176[10] = {(-1L),0L,(-1L),(-1L),0L,(-1L),(-1L),0L,(-1L),(-1L)};
            uint32_t *l_2179 = &g_926;
            uint32_t *l_2214 = &g_601;
            const uint16_t l_2223[10] = {65527UL,65527UL,65527UL,65527UL,65527UL,65527UL,65527UL,65527UL,65527UL,65527UL};
            int32_t **l_2227 = &l_2188;
            uint16_t *l_2242 = &l_2138[2];
            int8_t l_2264 = 1L;
            int32_t **l_2275 = (void*)0;
            uint16_t l_2276[1][4][4] = {{{0x792DL,65535UL,65535UL,0x792DL},{65535UL,65535UL,4UL,4UL},{65535UL,65535UL,0x792DL,65535UL},{65535UL,4UL,4UL,65535UL}}};
            int i, j, k;
            for (g_321 = 0; (g_321 < 6); ++g_321)
            { /* block id: 978 */
                uint8_t ****l_2151[6] = {&g_2149,(void*)0,&g_2149,&g_2149,(void*)0,&g_2149};
                int32_t l_2174[5][5][10] = {{{1L,(-5L),(-1L),(-9L),0xBDDCAD4FL,(-9L),(-1L),(-5L),1L,0x6A5CB506L},{(-1L),(-5L),0xD7469306L,0x4808115FL,0x7ABFFC58L,9L,0x41300EF8L,(-5L),(-4L),(-2L)},{0x3D2E5428L,0L,(-1L),0x4808115FL,0x79005EFFL,(-9L),0xACAC753EL,0L,1L,(-2L)},{0x79ABCFF4L,(-5L),(-1L),(-9L),0x7ABFFC58L,(-9L),(-1L),(-5L),0x79ABCFF4L,0x6A5CB506L},{0x3D2E5428L,(-5L),2L,0x4808115FL,0xBDDCAD4FL,9L,0xACAC753EL,(-5L),0x94CCD903L,(-2L)}},{{(-1L),0L,(-1L),0x4808115FL,3L,(-9L),0x41300EF8L,0L,0x79ABCFF4L,(-2L)},{1L,(-5L),(-1L),(-9L),0xBDDCAD4FL,(-9L),(-1L),(-5L),1L,0x6A5CB506L},{(-1L),(-5L),0xD7469306L,0x4808115FL,0x7ABFFC58L,9L,0x41300EF8L,(-5L),(-4L),(-2L)},{0x3D2E5428L,0L,(-1L),0x4808115FL,0x79005EFFL,(-9L),0xACAC753EL,0L,1L,(-2L)},{0x79ABCFF4L,(-5L),(-1L),(-9L),0x7ABFFC58L,(-9L),(-1L),(-5L),0x79ABCFF4L,0x6A5CB506L}},{{0x3D2E5428L,(-5L),2L,0x4808115FL,0xBDDCAD4FL,9L,0xACAC753EL,(-5L),0x94CCD903L,(-2L)},{(-1L),0L,(-1L),0x4808115FL,3L,(-9L),0x41300EF8L,0L,0x79ABCFF4L,(-2L)},{1L,(-5L),(-1L),(-9L),0xBDDCAD4FL,(-9L),(-1L),(-5L),1L,0x6A5CB506L},{(-1L),(-5L),0xD7469306L,0x4808115FL,0x7ABFFC58L,9L,0x41300EF8L,(-5L),(-4L),(-2L)},{0x3D2E5428L,0L,(-1L),0x4808115FL,0x79005EFFL,(-9L),0xACAC753EL,0L,1L,(-2L)}},{{0x79ABCFF4L,(-5L),(-1L),(-9L),0x7ABFFC58L,(-9L),(-1L),(-5L),0x79ABCFF4L,0x6A5CB506L},{0x3D2E5428L,(-5L),2L,0x4808115FL,0xBDDCAD4FL,9L,0xACAC753EL,(-5L),0x94CCD903L,(-2L)},{(-1L),0L,(-1L),0x3FF19168L,(-4L),2L,0x79ABCFF4L,9L,0x22B6C464L,(-4L)},{0x8B6326F8L,(-9L),0x94CCD903L,2L,(-9L),2L,0x94CCD903L,(-9L),0x8B6326F8L,0xB1F0DCD6L},{0x936379EBL,(-9L),(-1L),0x3FF19168L,0x34CE7013L,(-6L),0x79ABCFF4L,(-9L),1L,(-4L)}},{{0xC43BEB87L,9L,0x94CCD903L,0x3FF19168L,0x50EA63EBL,2L,1L,9L,0x8B6326F8L,(-4L)},{0x22B6C464L,(-9L),(-4L),2L,0x34CE7013L,2L,(-4L),(-9L),0x22B6C464L,0xB1F0DCD6L},{0xC43BEB87L,(-9L),0x3D2E5428L,0x3FF19168L,(-9L),(-6L),1L,(-9L),1L,(-4L)},{0x936379EBL,9L,(-4L),0x3FF19168L,(-4L),2L,0x79ABCFF4L,9L,0x22B6C464L,(-4L)},{0x8B6326F8L,(-9L),0x94CCD903L,2L,(-9L),2L,0x94CCD903L,(-9L),0x8B6326F8L,0xB1F0DCD6L}}};
                int32_t ****l_2208 = &g_1355;
                int32_t *****l_2207 = &l_2208;
                int i, j, k;
                if ((&g_1380 == (g_2149 = g_2149)))
                { /* block id: 980 */
                    uint64_t l_2171 = 18446744073709551615UL;
                    int64_t *l_2172 = &g_616;
                    int64_t *l_2173 = &g_130[0];
                    int8_t *l_2175 = &g_241;
                    int32_t ** const **l_2189[7];
                    int32_t l_2196 = 0L;
                    int i;
                    for (i = 0; i < 7; i++)
                        l_2189[i] = &l_2186[3];
                    l_2176[2] |= (((safe_add_func_uint16_t_u_u(((*l_2139) <= ((*l_2175) |= (((safe_add_func_uint32_t_u_u((*g_191), (safe_div_func_uint32_t_u_u((--l_2158), ((*l_2139) , ((((safe_lshift_func_uint8_t_u_u(((l_2163 == l_2164) ^ ((*l_2139) ^ (safe_rshift_func_uint16_t_u_u(((safe_rshift_func_int8_t_s_u(((safe_div_func_int64_t_s_s((((*l_2173) = ((*l_2172) = ((***g_2149) < l_2171))) <= (((-1L) ^ (**g_51)) <= l_2174[2][1][4])), 0x1473DE7D51BD22B5LL)) , (*g_1439)), 4)) ^ (-8L)), (***g_804))))), (*g_168))) > 0xFDF88EA1L) >= (*g_168)) || (*l_2139))))))) != (*l_2139)) , (*g_1439)))), 0xEFDDL)) > (*g_168)) && l_2171);
                    l_2196 = (safe_sub_func_int8_t_s_s((l_2179 != &g_582[0][0][0]), ((l_2174[3][1][4] ^= (safe_mul_func_int16_t_s_s(g_115, ((0UL && ((safe_sub_func_int16_t_s_s((safe_sub_func_int8_t_s_s(((g_2190 = l_2186[3]) == (void*)0), (*g_1439))), (safe_mul_func_uint16_t_u_u(0xB8F1L, (safe_unary_minus_func_uint16_t_u((((*g_2062) = 18446744073709551611UL) != 0x4963FD6D518C965ELL))))))) , (*g_1439))) || (-9L))))) , 0x15L)));
                }
                else
                { /* block id: 990 */
                    uint16_t l_2197 = 5UL;
                    (*g_1356) = &l_2176[1];
                    if (l_2197)
                        break;
                    return l_2197;
                }
                (*l_2139) &= ((((safe_sub_func_int64_t_s_s((0x19L | ((safe_mul_func_uint16_t_u_u(0UL, (((**g_530) ^ (g_1789 | l_2202)) | (safe_add_func_uint64_t_u_u(((*g_2062) &= (((safe_sub_func_int16_t_s_s((l_2207 == (void*)0), (l_2209 & (((*g_168) , 18446744073709551608UL) , (*g_2135))))) >= l_2158) ^ 0xF8L)), l_2176[2]))))) | 7UL)), 0xB0DC11C5EE67ECF1LL)) && 0x2AL) || 0xB9446ABA92D6CCE9LL) & (*g_229));
                for (g_243 = 15; (g_243 <= 28); ++g_243)
                { /* block id: 999 */
                    int32_t **l_2212 = (void*)0;
                    int32_t **l_2213 = &g_1136;
                    (*l_2213) = (*g_1135);
                    if ((**g_51))
                        continue;
                    if ((*g_2135))
                        continue;
                }
            }
            if (((((*l_2214)--) , l_2217[1][1]) != l_2219))
            { /* block id: 1006 */
                int32_t l_2229 = 0xAC0346FFL;
                for (g_1789 = 0; (g_1789 <= (-16)); g_1789 = safe_sub_func_uint16_t_u_u(g_1789, 6))
                { /* block id: 1009 */
                    int64_t l_2222 = 0x72824863798E421ALL;
                    int64_t l_2224 = (-1L);
                    int32_t *l_2257 = &l_2176[2];
                    if ((((l_2222 && (0xFE794303FB2FFA1DLL && ((l_2223[8] || l_2224) > ((l_2224 <= ((++(**g_2061)) || ((l_2227 != ((7L > (l_2228[3] == &g_913)) , (void*)0)) >= l_2229))) & l_2222)))) || l_2229) && l_2223[8]))
                    { /* block id: 1011 */
                        uint8_t ****l_2247[9] = {&g_2149,&g_2149,&g_2149,&g_2149,&g_2149,&g_2149,&g_2149,&g_2149,&g_2149};
                        uint8_t *****l_2248 = &l_2247[6];
                        int32_t l_2254 = 0x1C7805DAL;
                        uint32_t l_2255 = 0x5BE81512L;
                        uint32_t ****l_2256 = &g_1300;
                        int i;
                        (*l_2139) = (safe_lshift_func_int16_t_s_s((((l_2176[2] = ((*g_168) &= (safe_mul_func_int16_t_s_s((safe_add_func_uint16_t_u_u((safe_add_func_int8_t_s_s((safe_rshift_func_int8_t_s_u((((safe_sub_func_int32_t_s_s((l_2242 == (void*)0), (g_1913[2][2][0] , 1L))) <= ((safe_rshift_func_int16_t_s_u((((safe_div_func_uint16_t_u_u(((*l_2242) = ((**g_288) &= ((&g_1379 == (g_2249 = ((*l_2248) = l_2247[0]))) ^ ((*l_2164) == (void*)0)))), (safe_add_func_int32_t_s_s(((safe_sub_func_int64_t_s_s((g_616 = (*l_2139)), (**g_228))) >= l_2222), l_2254)))) == l_2176[1]) == 1L), 5)) < 0L)) <= 0UL), l_2209)), l_2255)), g_167[3])), (*l_2139))))) , l_2256) == (void*)0), 6));
                        (*g_1356) = l_2257;
                    }
                    else
                    { /* block id: 1021 */
                        (*g_1356) = &l_2176[9];
                        (*g_51) = &l_2176[3];
                        if (l_2229)
                            break;
                    }
                    (*l_2139) = 8L;
                    (**g_1355) = &l_2176[2];
                }
                (*g_51) = (*g_1356);
            }
            else
            { /* block id: 1030 */
                int32_t *l_2258[4][4] = {{&g_13,&l_2176[2],&l_2176[2],&g_13},{&g_13,&l_2176[2],&l_2176[2],&g_13},{&g_13,&l_2176[2],&l_2176[2],&g_13},{&g_13,&l_2176[2],&l_2176[2],&g_13}};
                int32_t l_2265 = 0x82CAA735L;
                int16_t l_2266 = 0xA7D6L;
                int8_t ***l_2303 = &g_1438[2][1][6];
                uint32_t l_2305 = 7UL;
                const int32_t *l_2307[2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_2307[i] = &g_925[1];
                ++l_2267;
                if ((*l_2139))
                { /* block id: 1032 */
                    uint64_t l_2273[9];
                    int32_t ***l_2274 = &l_2227;
                    int32_t l_2279[8];
                    int64_t *l_2282[5][6] = {{(void*)0,(void*)0,&l_2,&g_130[0],(void*)0,&g_130[0]},{&l_2,&l_2,(void*)0,&g_130[0],(void*)0,&l_2},{&g_130[0],&l_2,(void*)0,&g_130[0],(void*)0,&g_130[0]},{&l_2261,&g_130[0],&l_2,&l_2,&g_130[0],&l_2261},{&l_2,&g_130[0],&l_2261,&l_2261,(void*)0,(void*)0}};
                    int i, j;
                    for (i = 0; i < 9; i++)
                        l_2273[i] = 0x4BBC1846E972F33BLL;
                    for (i = 0; i < 8; i++)
                        l_2279[i] = 0x1F9F8041L;
                    if ((0UL & ((safe_add_func_int8_t_s_s(((*g_1439) , (*l_2139)), ((safe_unary_minus_func_int64_t_s((l_2176[2] >= ((*g_2135) || ((l_2273[5] < (**g_530)) ^ ((((*l_2274) = &g_2192[4]) == l_2275) & 65535UL)))))) <= (*g_2062)))) & 0x30732BAB2BD68E48LL)))
                    { /* block id: 1034 */
                        l_2263[1][7][4] &= ((*l_2139) = l_2276[0][2][1]);
                    }
                    else
                    { /* block id: 1037 */
                        int64_t **l_2283 = &l_2282[1][2];
                        int32_t l_2290 = (-5L);
                        l_2146 &= (((((safe_lshift_func_int16_t_s_u((((5L >= (((l_2279[3] = (*g_52)) ^ l_2176[2]) != ((((safe_sub_func_uint32_t_u_u((((((((*l_2283) = l_2282[4][5]) == (void*)0) , ((******g_1941) ^ (((((*g_191) || (l_2223[8] , ((*l_2139) = ((safe_mul_func_uint16_t_u_u(((*l_2242) ^= (((((safe_div_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((((**g_51) | l_2290) == l_2276[0][0][0]), (*g_191))), 1L)) != 4294967295UL) && (*g_191)) || (*l_2139)) , (****g_914))), l_2276[0][0][2])) || (*g_2062))))) != l_2290) != 0UL) , (***g_287)))) == l_2291) , l_2273[5]) >= 0x96DE133AL), 0xD598C55CL)) , &g_297) != (void*)0) | (*g_1439)))) , 18446744073709551615UL) && (-2L)), l_2273[5])) > (***g_287)) ^ l_2273[5]) , 0x283BD055L) < 0L);
                        if (g_195)
                            goto lbl_2292;
lbl_2292:
                        (*l_2139) = 1L;
                        (*l_2139) = ((void*)0 == &l_2290);
                    }
                }
                else
                { /* block id: 1047 */
                    uint64_t l_2301[2];
                    int8_t ****l_2304 = &l_2303;
                    const int32_t **l_2306[4][5][3] = {{{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135}},{{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,(void*)0},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,(void*)0},{&g_2135,&g_2135,&g_2135}},{{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,&g_2135}},{{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,(void*)0},{&g_2135,&g_2135,&g_2135},{&g_2135,&g_2135,(void*)0},{&g_2135,&g_2135,(void*)0}}};
                    int i, j, k;
                    for (i = 0; i < 2; i++)
                        l_2301[i] = 0xF697105BA9186717LL;
                    l_2263[1][7][4] |= ((safe_add_func_int8_t_s_s(((*l_2139) | (*l_2139)), (safe_lshift_func_int16_t_s_u((safe_add_func_uint64_t_u_u(((safe_mod_func_uint16_t_u_u((l_2301[0] < ((((((void*)0 != l_2302) >= ((((*l_2304) = l_2303) != &g_1438[2][0][9]) || l_2176[9])) >= ((*****g_913) >= l_2305)) | 255UL) & (****g_2249))), (***g_287))) | 0x4E8CL), (**g_530))), (**g_288))))) , 0xD01B0F43L);
                    l_2307[1] = ((*g_51) = (void*)0);
                }
                for (l_2145 = 0; (l_2145 >= 10); l_2145 = safe_add_func_int32_t_s_s(l_2145, 1))
                { /* block id: 1055 */
                    int32_t *l_2310[6][1][10] = {{{&l_2176[4],&l_2176[4],&l_2263[1][8][3],&l_2262,(void*)0,(void*)0,&l_2262,&l_2263[1][8][3],&l_2176[4],&l_2176[4]}},{{(void*)0,&l_2262,&l_2263[1][8][3],&l_2176[4],&l_2176[4],&l_2263[1][8][3],&l_2262,(void*)0,(void*)0,&l_2262}},{{(void*)0,&l_2176[4],&l_2262,&l_2262,&l_2176[4],(void*)0,&l_2263[1][8][3],&l_2263[1][8][3],(void*)0,&l_2176[4]}},{{&l_2176[4],&l_2262,&l_2262,&l_2176[4],(void*)0,&l_2263[1][8][3],&l_2263[1][8][3],(void*)0,&l_2176[4],&l_2262}},{{&l_2176[4],&l_2176[4],&l_2263[1][8][3],&l_2262,(void*)0,(void*)0,&l_2262,&l_2263[1][8][3],&l_2176[4],&l_2176[4]}},{{(void*)0,&l_2262,&l_2263[1][8][3],&l_2176[4],&l_2176[4],&l_2263[1][8][3],&l_2262,(void*)0,(void*)0,&l_2262}}};
                    int i, j, k;
                    l_2310[4][0][7] = (*g_1135);
                    if (l_2276[0][2][1])
                        break;
                }
            }
        }
        else
        { /* block id: 1060 */
            const uint32_t l_2313 = 0x77C446C1L;
            int32_t ******l_2322 = &g_1573;
            int32_t ******l_2325[1][6] = {{&g_2323,&g_2323,&g_2323,&g_2323,&g_2323,&g_2323}};
            int32_t **l_2339 = &l_2139;
            int32_t *** const l_2338 = &l_2339;
            int32_t *** const *l_2337 = &l_2338;
            int32_t *** const **l_2336[7] = {&l_2337,&l_2337,&l_2337,&l_2337,&l_2337,&l_2337,&l_2337};
            uint32_t *l_2355 = &g_450;
            uint32_t l_2356 = 9UL;
            int64_t *l_2357 = &g_616;
            uint8_t l_2376 = 1UL;
            uint32_t l_2379 = 18446744073709551615UL;
            int8_t *l_2387 = &g_242;
            int i, j;
            (*l_2139) = ((safe_div_func_uint8_t_u_u((l_2313 , (l_2313 , (safe_div_func_uint16_t_u_u((***g_287), (~(**g_51)))))), l_2317)) , ((7UL < 0x62BBE412C4755369LL) > g_962));
            (***l_2338) = (safe_add_func_int16_t_s_s((((((((((*l_2322) = g_1573) == (g_2323 = g_2323)) >= (*g_1439)) , ((safe_rshift_func_uint16_t_u_u((&l_2164 != ((safe_add_func_int64_t_s_s(l_2330, (safe_lshift_func_int8_t_s_s(((!((*l_2357) &= (safe_mod_func_uint8_t_u_u(((g_2340 = &l_2164) != ((((0x677AL > (~(~(~(safe_mul_func_uint16_t_u_u(((*g_289) = ((((safe_sub_func_uint16_t_u_u((((((safe_rshift_func_uint8_t_u_s(((((((*g_1439) = (+(safe_add_func_uint64_t_u_u(l_2291, ((((***g_804) , l_2355) == (void*)0) & 65528UL))))) , (*g_191)) & l_2356) , (*l_2139)) && (**g_2061)), l_2291)) > (-2L)) >= (****g_2249)) && 1L) , (****l_2337)), 0x033AL)) < (*l_2139)) < (*l_2139)) | (*g_289))), 0x0057L)))))) < (*l_2139)) | 0x4F644B9F4E2AC27ALL) , &l_2164)), (**g_2150))))) ^ (*l_2139)), 2)))) , &l_2163)), l_2358[1])) && (*g_191))) || 0x8875L) ^ 0UL) < 1L) , (-5L)), (*l_2139)));
            for (g_115 = 0; (g_115 >= 27); g_115++)
            { /* block id: 1071 */
                const int8_t l_2361 = 0x0BL;
                uint32_t l_2364 = 0x482B3DB3L;
                int64_t l_2375 = 0x079C38A45CE233F4LL;
                int8_t *l_2390 = &g_447;
                int32_t ** const l_2397 = &l_2139;
                uint64_t l_2417 = 18446744073709551615UL;
                int8_t l_2423 = 0x5AL;
                uint32_t ****l_2435 = &g_1300;
                if (l_2361)
                    break;
            }
        }
        l_2454--;
    }
    else
    { /* block id: 1129 */
        uint16_t l_2459 = 0xFEEDL;
        int64_t **l_2465[10][3] = {{(void*)0,&g_1489,(void*)0},{&g_1489,&g_1489,&g_1489},{(void*)0,&g_1489,(void*)0},{&g_1489,&g_1489,&g_1489},{(void*)0,&g_1489,(void*)0},{&g_1489,&g_1489,&g_1489},{(void*)0,&g_1489,(void*)0},{&g_1489,&g_1489,&g_1489},{(void*)0,&g_1489,(void*)0},{&g_1489,&g_1489,&g_1489}};
        uint32_t l_2474 = 4294967295UL;
        int32_t l_2476[7][10];
        int i, j;
        for (i = 0; i < 7; i++)
        {
            for (j = 0; j < 10; j++)
                l_2476[i][j] = 0x17DD6611L;
        }
        l_2476[5][9] = (((**g_288) ^= ((safe_lshift_func_uint16_t_u_u((((l_2459 || ((safe_mod_func_uint8_t_u_u(2UL, (safe_unary_minus_func_uint64_t_u((safe_mod_func_int8_t_s_s((g_16[3][0][3] || ((g_2466 = l_2465[3][1]) != (void*)0)), l_2459)))))) != ((safe_div_func_int32_t_s_s(l_2459, 4294967295UL)) | ((safe_mul_func_int8_t_s_s(((*g_1439) = ((((safe_sub_func_uint8_t_u_u((l_2473 == (void*)0), 255UL)) && l_2474) , 0L) | 0UL)), 0x7FL)) | l_2459)))) && 0xBCFC9B87DF7D0461LL) != 0x3B1732CCL), l_2475)) < l_2474)) < g_616);
    }
    return g_268[2][1];
}


/* ------------------------------------------ */
/* 
 * reads : g_288 g_289 g_195 g_13 g_229 g_95 g_191 g_115 g_804 g_805 g_168 g_228
 * writes: g_1939 g_1941 g_13 g_240 g_169 g_195
 */
static int16_t  func_5(uint8_t  p_6)
{ /* block id: 2 */
    int32_t l_8[9][6] = {{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L},{0xFA20BD03L,0L,0xFA20BD03L,0xFA20BD03L,0L,0xFA20BD03L}};
    int32_t l_14[5][3];
    int32_t *l_20 = &g_13;
    int32_t ** const l_19 = &l_20;
    int32_t l_1420 = 0x61C30273L;
    int8_t * const l_1436 = (void*)0;
    int8_t * const *l_1435[10] = {(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0,(void*)0};
    int32_t *l_1484 = &l_1420;
    int32_t **l_1483 = &l_1484;
    int64_t *l_1490 = (void*)0;
    int32_t l_1505 = 1L;
    const int32_t l_1563 = 0x0312D43CL;
    int8_t **l_1626[6][8] = {{&g_1439,&g_1439,&g_1439,&g_1439,(void*)0,&g_1439,&g_1439,&g_1439},{&g_1439,(void*)0,&g_1439,&g_1439,&g_1439,&g_1439,(void*)0,&g_1439},{(void*)0,&g_1439,&g_1439,&g_1439,&g_1439,&g_1439,(void*)0,&g_1439},{(void*)0,&g_1439,&g_1439,&g_1439,&g_1439,(void*)0,&g_1439,(void*)0},{&g_1439,(void*)0,&g_1439,(void*)0,&g_1439,&g_1439,&g_1439,&g_1439},{(void*)0,&g_1439,&g_1439,(void*)0,(void*)0,&g_1439,(void*)0,(void*)0}};
    int32_t *****l_1653 = (void*)0;
    uint64_t l_1668 = 0x6649F91DD62603B1LL;
    uint16_t l_1669 = 9UL;
    uint32_t * const *l_1762[1][9][2];
    const uint32_t l_1814 = 1UL;
    int8_t l_1835 = 5L;
    uint16_t l_1843 = 65534UL;
    uint16_t l_1854 = 0x7E78L;
    const int16_t *l_1868 = &g_167[0];
    int32_t ***l_1879 = (void*)0;
    int32_t l_1881[5];
    int64_t l_1889 = (-4L);
    uint16_t l_1930[1][10] = {{0UL,1UL,0UL,1UL,0UL,1UL,0UL,1UL,0UL,1UL}};
    uint8_t l_1934 = 5UL;
    const uint16_t *****l_1936 = (void*)0;
    const uint16_t ******l_1935[3][6] = {{&l_1936,&l_1936,&l_1936,&l_1936,&l_1936,&l_1936},{&l_1936,&l_1936,&l_1936,&l_1936,&l_1936,&l_1936},{&l_1936,&l_1936,&l_1936,&l_1936,&l_1936,&l_1936}};
    const uint16_t *******l_1937 = (void*)0;
    const uint16_t *******l_1938[10][10] = {{&l_1935[0][3],&l_1935[2][5],&l_1935[2][4],&l_1935[1][5],&l_1935[1][5],(void*)0,&l_1935[0][3],(void*)0,(void*)0,&l_1935[0][3]},{(void*)0,&l_1935[2][3],&l_1935[1][5],&l_1935[1][5],&l_1935[2][3],(void*)0,(void*)0,&l_1935[1][5],&l_1935[2][5],(void*)0},{&l_1935[1][5],&l_1935[1][5],(void*)0,&l_1935[1][2],(void*)0,(void*)0,&l_1935[2][4],&l_1935[0][3],(void*)0,&l_1935[2][4]},{&l_1935[1][5],&l_1935[2][5],(void*)0,&l_1935[1][5],&l_1935[1][5],(void*)0,&l_1935[2][4],(void*)0,&l_1935[1][5],&l_1935[1][5]},{(void*)0,&l_1935[2][4],(void*)0,&l_1935[1][5],&l_1935[1][5],(void*)0,&l_1935[2][5],&l_1935[1][5],&l_1935[1][5],(void*)0},{&l_1935[0][3],&l_1935[2][4],(void*)0,(void*)0,&l_1935[1][2],(void*)0,&l_1935[1][5],&l_1935[1][5],(void*)0,&l_1935[1][5]},{&l_1935[1][5],(void*)0,(void*)0,&l_1935[2][3],&l_1935[1][5],&l_1935[1][5],&l_1935[2][3],(void*)0,(void*)0,&l_1935[1][5]},{(void*)0,&l_1935[0][3],(void*)0,&l_1935[1][5],&l_1935[1][5],&l_1935[2][4],&l_1935[2][5],&l_1935[0][3],(void*)0,(void*)0},{&l_1935[2][3],&l_1935[1][5],(void*)0,&l_1935[0][3],&l_1935[1][5],(void*)0,(void*)0,&l_1935[1][5],&l_1935[1][1],&l_1935[1][5]},{&l_1935[1][5],&l_1935[2][5],&l_1935[1][5],&l_1935[2][4],&l_1935[1][5],&l_1935[2][5],&l_1935[1][5],(void*)0,(void*)0,&l_1935[1][5]}};
    uint64_t l_1946 = 0x5E739D6947B7E390LL;
    uint8_t **l_1947 = &g_168;
    uint16_t l_1969 = 65535UL;
    uint32_t **l_1982 = &g_191;
    uint32_t ***l_1981 = &l_1982;
    int64_t l_2083 = 0xE1A866D039F95351LL;
    int32_t l_2090[5][2] = {{0xE3E6AFBAL,0xEA19590BL},{0xE3E6AFBAL,0xEA19590BL},{0xE3E6AFBAL,0xEA19590BL},{0xE3E6AFBAL,0xEA19590BL},{0xE3E6AFBAL,0xEA19590BL}};
    int16_t l_2128 = 0x396FL;
    const int32_t *l_2136 = (void*)0;
    int i, j, k;
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
            l_14[i][j] = (-4L);
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 9; j++)
        {
            for (k = 0; k < 2; k++)
                l_1762[i][j][k] = &g_191;
        }
    }
    for (i = 0; i < 5; i++)
        l_1881[i] = 0x359F06FCL;
    for (p_6 = 0; (p_6 <= 5); p_6 += 1)
    { /* block id: 5 */
        int16_t l_43 = 0xD946L;
        int32_t l_46 = 4L;
        int32_t * const l_1361 = (void*)0;
        int32_t ****l_1434 = (void*)0;
        uint64_t *l_1457 = (void*)0;
        int32_t l_1524 = (-5L);
        int8_t l_1532 = (-5L);
        int32_t l_1536 = 2L;
        int32_t l_1538 = (-2L);
        int32_t l_1539 = 0xD768B2EFL;
        const uint16_t *l_1565 = &g_272;
        const uint16_t **l_1564 = &l_1565;
        uint32_t * const *l_1638 = (void*)0;
        uint32_t * const **l_1637[3];
        int16_t *l_1657[2];
        uint32_t l_1683 = 2UL;
        uint16_t ** const *l_1684 = &g_288;
        uint16_t *******l_1691 = (void*)0;
        int32_t l_1836[3];
        uint32_t l_1859 = 0x1FCD7337L;
        int32_t l_1888 = 0xA656699DL;
        int64_t l_1890[10];
        int i;
        for (i = 0; i < 3; i++)
            l_1637[i] = &l_1638;
        for (i = 0; i < 2; i++)
            l_1657[i] = (void*)0;
        for (i = 0; i < 3; i++)
            l_1836[i] = 0xAC424F26L;
        for (i = 0; i < 10; i++)
            l_1890[i] = 0x2506114104747E7ALL;
    }
lbl_2137:
    if ((safe_div_func_uint32_t_u_u(p_6, (l_1934 , (((((g_1939 = l_1935[1][5]) == (g_1941 = &g_913)) == (((safe_mod_func_int16_t_s_s((safe_lshift_func_uint8_t_u_u((((((((void*)0 != l_1868) >= (((((**l_19) = 0xB9D87972L) != l_1946) , l_1947) != (void*)0)) | (**g_288)) ^ p_6) <= p_6) < p_6), p_6)), (*g_289))) ^ p_6) || 0x16EBL)) , (**l_19)) , 0xB53DB319L)))))
    { /* block id: 893 */
        const int8_t *l_1959[2][2] = {{&g_1960,&g_1960},{&g_1960,&g_1960}};
        const int8_t **l_1958[4];
        const int8_t ***l_1957 = &l_1958[2];
        const int8_t ****l_1956 = &l_1957;
        int32_t l_1964 = 0x4CEF8875L;
        int16_t *l_1966 = &g_240[7];
        int i, j;
        for (i = 0; i < 4; i++)
            l_1958[i] = &l_1959[1][1];
        l_1969 &= ((safe_mod_func_uint64_t_u_u((safe_mod_func_int16_t_s_s((safe_div_func_int32_t_s_s((p_6 >= (safe_add_func_uint16_t_u_u((((*l_1956) = (void*)0) == (void*)0), ((*g_289) = ((((safe_add_func_uint8_t_u_u(((*g_168) = ((((!((l_1964 == (*g_229)) != ((!(-4L)) , ((*l_1966) = p_6)))) > (0x67L && ((safe_mul_func_int16_t_s_s((((&g_1439 == (void*)0) , (*g_191)) , l_1964), 0x5CD9L)) >= l_1964))) ^ p_6) && (***g_804))), 7L)) >= (*l_20)) | l_1964) <= (**g_228)))))), p_6)), 0x6964L)), (*l_20))) != 0x36FF85CE26319AA8LL);
        if (l_1946)
            goto lbl_2137;
    }
    else
    { /* block id: 899 */
        uint32_t ****l_1977 = &g_1300;
        uint32_t **l_1980 = &g_191;
        uint32_t ***l_1979 = &l_1980;
        uint32_t ****l_1978 = &l_1979;
        const uint8_t ***l_1993[9] = {&g_1380,&g_1380,&g_1380,&g_1380,&g_1380,&g_1380,&g_1380,&g_1380,&g_1380};
        int32_t l_1994 = 8L;
        int32_t l_2032 = 7L;
        int32_t l_2034 = 0xB52B7351L;
        int32_t l_2050 = 1L;
        int32_t l_2051 = 0x29B0D909L;
        uint64_t **l_2064 = &g_2062;
        uint16_t ** const *l_2095[4] = {(void*)0,(void*)0,(void*)0,(void*)0};
        uint16_t ** const ** const l_2094[3] = {&l_2095[3],&l_2095[3],&l_2095[3]};
        int32_t *l_2100 = (void*)0;
        uint8_t l_2134[8] = {0UL,0x6BL,0UL,0UL,0x6BL,0UL,0UL,0UL};
        int i;
    }
    return p_6;
}


/* ------------------------------------------ */
/* 
 * reads : g_272 g_914 g_287 g_288 g_289 g_195 g_505 g_1379 g_1136 g_442 g_320 g_240 g_530 g_531 g_130 g_1355 g_1356
 * writes: g_272 g_505 g_1389 g_1392 g_195 g_442
 */
static int32_t ** func_23(int32_t  p_24, int32_t * const * p_25, int8_t  p_26, int32_t * const  p_27, uint32_t  p_28)
{ /* block id: 611 */
    int32_t l_1362 = 0x6DB84C44L;
    int32_t *l_1363 = &g_505;
    int32_t *l_1364[7][6][3] = {{{&g_118,&g_9,&g_9},{&g_118,&g_118,&g_9},{&g_925[4],&g_1025[1][1],(void*)0},{&l_1362,&g_118,&g_13},{&l_1362,&g_9,&g_118},{&g_925[4],&g_13,&g_13}},{{&g_118,&g_13,(void*)0},{&g_118,&g_9,&g_9},{&g_118,&g_118,&g_9},{&g_925[4],&g_1025[1][1],(void*)0},{&l_1362,&g_118,&g_13},{&l_1362,&g_9,&g_118}},{{&g_925[4],&g_13,&g_13},{&g_118,&g_13,(void*)0},{&g_118,&g_9,&g_9},{&g_118,&g_118,&g_9},{&g_925[4],&g_1025[1][1],(void*)0},{&l_1362,&g_118,&g_13}},{{&l_1362,&g_9,&g_118},{&g_925[4],&g_13,&g_13},{&g_118,&g_13,(void*)0},{&g_118,&g_9,&g_9},{&g_118,&g_118,&g_9},{&g_925[4],&g_1025[1][1],(void*)0}},{{&l_1362,&g_118,&g_13},{&l_1362,&g_9,&g_118},{&g_925[4],&g_13,&g_13},{&g_118,&g_13,(void*)0},{&g_118,&g_9,&g_9},{&g_118,&g_118,&g_9}},{{&g_925[4],&g_1025[1][1],(void*)0},{&l_1362,&g_118,&g_13},{&l_1362,&g_9,&g_118},{&g_925[4],&g_13,&g_13},{&g_118,&g_13,(void*)0},{&g_118,&g_9,&g_9}},{{&g_118,&g_118,&g_9},{&g_925[4],&g_1025[1][1],(void*)0},{&l_1362,&g_118,&g_13},{&l_1362,&g_9,&g_118},{&g_925[4],&g_13,&g_13},{&g_118,&g_13,(void*)0}}};
    uint32_t l_1365[8][2] = {{1UL,0x2D677577L},{1UL,1UL},{0x2D677577L,1UL},{1UL,0x2D677577L},{1UL,1UL},{0x2D677577L,1UL},{1UL,0x2D677577L},{1UL,1UL}};
    int16_t l_1374 = (-4L);
    uint16_t l_1384 = 0xF2EAL;
    int32_t *l_1386 = (void*)0;
    int32_t * const *l_1385 = &l_1386;
    int64_t *l_1401[5][2] = {{&g_616,&g_616},{&g_616,&g_616},{&g_616,&g_616},{&g_616,&g_616},{&g_616,&g_616}};
    int8_t l_1405 = 0L;
    int i, j, k;
    --l_1365[0][0];
    for (g_272 = 0; (g_272 > 22); ++g_272)
    { /* block id: 615 */
        uint16_t l_1370 = 0xEB16L;
        uint32_t **l_1373 = (void*)0;
        int32_t * const **l_1387 = (void*)0;
        int32_t * const **l_1388[7][3][1] = {{{&l_1385},{&l_1385},{(void*)0}},{{(void*)0},{&l_1385},{&l_1385}},{{&l_1385},{(void*)0},{(void*)0}},{{&l_1385},{&l_1385},{&l_1385}},{{(void*)0},{(void*)0},{&l_1385}},{{&l_1385},{&l_1385},{(void*)0}},{{(void*)0},{&l_1385},{&l_1385}}};
        uint64_t *l_1402 = &g_442[2][1];
        uint16_t *l_1403[9][3] = {{&l_1370,&g_268[1][0],&g_268[1][0]},{(void*)0,&g_268[0][0],(void*)0},{&l_1370,&l_1370,&g_268[1][0]},{&g_435,&g_268[0][0],&g_435},{&l_1370,&g_268[1][0],&g_268[1][0]},{(void*)0,&g_268[0][0],(void*)0},{&l_1370,&l_1370,&g_268[1][0]},{&g_435,&g_268[0][0],&g_435},{&l_1370,&g_268[1][0],&g_268[1][0]}};
        int32_t l_1404 = 1L;
        int32_t l_1406 = 1L;
        int32_t l_1407 = 0x48AAB7CFL;
        int i, j, k;
        (*l_1363) |= ((((****g_914) < ((1UL >= (***g_287)) >= ((l_1370 , (safe_mod_func_int32_t_s_s((l_1374 = ((((void*)0 == l_1373) > p_26) > 0UL)), 0xE6B2805CL))) ^ (-1L)))) | g_272) ^ 0xD2545437320B426CLL);
        l_1407 = ((*l_1363) = ((safe_mod_func_uint64_t_u_u((g_1379 == ((l_1406 &= (l_1370 || ((safe_mul_func_uint8_t_u_u((p_26 <= ((((((l_1384 &= (*l_1363)) | (((((g_1392 = (g_1389 = l_1385)) != (((4L <= ((p_28 < (safe_lshift_func_uint8_t_u_s((safe_mod_func_int8_t_s_s(((l_1404 = (((((*l_1402) ^= (safe_rshift_func_uint16_t_u_s(((**g_288) |= (((p_26 || ((void*)0 == l_1401[1][0])) , (void*)0) != (*p_25))), 15))) , p_26) & g_320[2]) == l_1370)) , p_28), p_26)), 7))) , g_240[5])) != l_1405) , &g_1393)) , &g_584) != &g_16[1][3][0]) && l_1370)) , 0x543F19C3L) != l_1370) | 1L) | 0x190B819EE7DDF973LL)), 5L)) == 0xD0D7L))) , (void*)0)), (**g_530))) & 0L));
    }
    return (*g_1355);
}


/* ------------------------------------------ */
/* 
 * reads : g_1135 g_1136 g_595
 * writes: g_1136 g_595
 */
static int32_t * const * func_29(const int16_t  p_30, int32_t *** p_31, int32_t ** const * p_32, int32_t * p_33)
{ /* block id: 607 */
    int32_t **l_1357 = &g_1136;
    int32_t *l_1358 = &g_595;
    int32_t * const *l_1359 = &l_1358;
    int32_t * const *l_1360 = &g_1136;
    (*l_1357) = (*g_1135);
    (*l_1358) &= 0L;
    return l_1360;
}


/* ------------------------------------------ */
/* 
 * reads : g_51 g_52 g_962 g_595 g_1182 g_168 g_169 g_229 g_95 g_287 g_288 g_289 g_195 g_228 g_913 g_914 g_1135 g_1136 g_1025 g_590 g_16 g_240 g_804 g_805 g_530 g_531 g_241 g_130 g_784 g_1300 g_442 g_118 g_505 g_924 g_167 g_115 g_519 g_616 g_582 g_242
 * writes: g_118 g_242 g_52 g_595 g_1182 g_590 g_923 g_95 g_240 g_926 g_241 g_321 g_195 g_442 g_297 g_272 g_523 g_201 g_169 g_167 g_1025 g_115 g_322 g_616
 */
static const int32_t  func_34(int32_t  p_35, uint16_t  p_36, int32_t *** p_37, int32_t *** p_38, const uint8_t  p_39)
{ /* block id: 513 */
    const int32_t *l_1145 = (void*)0;
    uint16_t **l_1155 = (void*)0;
    int32_t l_1157 = 0xE78919CFL;
    const int64_t l_1170[8] = {1L,1L,1L,1L,1L,1L,1L,1L};
    uint8_t *l_1173 = &g_519[4][2];
    int32_t l_1179 = 0x08A56591L;
    int32_t l_1180[9] = {0xE6E11F0DL,1L,0xE6E11F0DL,0xE6E11F0DL,1L,0xE6E11F0DL,0xE6E11F0DL,1L,0xE6E11F0DL};
    const int8_t l_1196 = 0x24L;
    int32_t **l_1203 = (void*)0;
    int32_t l_1229 = 9L;
    int64_t l_1262 = (-1L);
    int32_t *l_1275[8][1][7] = {{{&g_1025[2][4],&g_925[2],(void*)0,&g_925[2],&g_1025[2][4],&g_925[1],&g_925[1]}},{{&g_925[1],(void*)0,&g_925[1],(void*)0,&g_925[1],&g_9,&g_9}},{{&g_1025[2][4],&g_925[2],(void*)0,&g_925[2],&g_1025[2][4],&g_925[1],&g_925[1]}},{{&g_925[1],(void*)0,&g_925[1],(void*)0,&g_925[1],&g_9,&g_9}},{{&g_1025[2][4],&g_925[2],(void*)0,&g_925[2],&g_1025[2][4],&g_925[1],&g_925[1]}},{{&g_925[1],(void*)0,&g_925[1],(void*)0,&g_925[1],&g_9,&g_9}},{{&g_1025[2][4],&g_925[2],(void*)0,&g_925[2],&g_1025[2][4],&g_925[1],&g_925[1]}},{{&g_925[1],(void*)0,&g_925[1],(void*)0,&g_925[1],&g_9,&g_9}}};
    uint32_t l_1314 = 0UL;
    const uint64_t l_1354[8] = {18446744073709551615UL,0xB7805E993942776CLL,0xB7805E993942776CLL,18446744073709551615UL,0xB7805E993942776CLL,0xB7805E993942776CLL,18446744073709551615UL,0xB7805E993942776CLL};
    int i, j, k;
lbl_1248:
    l_1145 = l_1145;
lbl_1185:
    for (g_118 = 0; (g_118 == 15); g_118 = safe_add_func_uint16_t_u_u(g_118, 8))
    { /* block id: 517 */
        uint8_t *l_1160 = &g_135;
        int32_t l_1161 = (-1L);
        int32_t *l_1162 = &g_924;
        int32_t l_1181[9] = {(-1L),(-1L),0xC44B7E08L,(-1L),(-1L),0xC44B7E08L,(-1L),(-1L),0xC44B7E08L};
        int i;
        for (g_242 = 0; (g_242 < 29); g_242++)
        { /* block id: 520 */
            int16_t l_1152[6][10][3] = {{{0xF696L,0x8D0FL,0x3B0CL},{0x202AL,5L,0xE2EFL},{0xE2EFL,(-4L),0xC485L},{0x5204L,5L,0x7FE7L},{8L,0x8D0FL,0x0BC3L},{0x5420L,0x9F9AL,0L},{0x782DL,1L,8L},{0x9F9AL,1L,0xDD13L},{(-1L),0x202AL,(-8L)},{1L,1L,1L}},{{0x4ECEL,0x319EL,1L},{0xDD13L,0xC485L,(-8L)},{0x8296L,(-7L),0xDD13L},{(-1L),(-8L),8L},{0L,0xE2EFL,0L},{5L,0x0D2FL,0x0BC3L},{0x3752L,0x0BC3L,0x7FE7L},{9L,8L,0xC485L},{0L,0x3B0CL,0xE2EFL},{9L,(-1L),0x3B0CL}},{{0x3752L,0x5D33L,0x2E69L},{5L,0x782DL,1L},{0L,0xF696L,0L},{(-1L),0x8296L,0x5204L},{0x8296L,0x7370L,(-4L)},{0xDD13L,0x4DEEL,9L},{0x4ECEL,0x4DEEL,0x49D8L},{1L,0x7370L,1L},{(-1L),0x8296L,5L},{0x9F9AL,0xF696L,0L}},{{0x782DL,0L,0x3752L},{0x3B0CL,(-7L),0x0D2FL},{5L,0x8296L,1L},{9L,0x782DL,0xF696L},{0x7370L,5L,1L},{(-4L),0x5204L,0x0D2FL},{(-1L),0xF674L,0x3752L},{1L,0x7370L,0L},{0x319EL,5L,1L},{1L,0x319EL,0xDD13L}},{{(-1L),0L,0x3DCFL},{(-1L),0x0D2FL,0x2E69L},{(-1L),0xDD13L,1L},{(-1L),(-4L),9L},{1L,0x202AL,0x8D0FL},{0x319EL,(-8L),(-8L)},{1L,0x7FE7L,(-1L)},{(-1L),0x49D8L,0x782DL},{(-4L),0x4DEEL,0x7370L},{0x7370L,1L,0L}},{{9L,0x4DEEL,0xDD13L},{5L,0x49D8L,0x5204L},{0x3B0CL,0x7FE7L,0x9F9AL},{0L,(-8L),5L},{0x7FE7L,0x202AL,0x5D33L},{0x4ECEL,(-4L),5L},{(-8L),0xDD13L,0x202AL},{0x5420L,0x0D2FL,0x202AL},{0x5D33L,0L,5L},{0L,0x319EL,0x5D33L}}};
            uint16_t ***** const l_1174 = &g_914;
            int32_t *l_1175 = &g_595;
            int32_t *l_1176 = &g_595;
            int32_t *l_1177 = &g_925[1];
            int32_t *l_1178[2];
            int i, j, k;
            for (i = 0; i < 2; i++)
                l_1178[i] = &g_1025[2][4];
            (*g_51) = &l_1161;
            (*l_1175) &= ((((l_1152[3][1][1] == (safe_unary_minus_func_uint32_t_u((&g_914 != (((safe_mod_func_int8_t_s_s(1L, ((safe_sub_func_int32_t_s_s((**g_51), 3L)) | (((safe_rshift_func_int8_t_s_u(0L, 4)) || (((0xBE6FL & l_1170[6]) != (safe_sub_func_uint64_t_u_u((((((g_962 ^ 0x1DC5L) , (-1L)) , l_1161) , &g_785[0][0]) != l_1173), l_1152[3][9][0]))) > 0x2CL)) , l_1152[5][2][1])))) > p_35) , l_1174))))) <= 248UL) & l_1152[0][3][2]) >= 7UL);
            if (g_595)
                goto lbl_1185;
            g_1182++;
        }
    }
    if ((safe_rshift_func_uint8_t_u_u(((safe_rshift_func_int16_t_s_u((safe_lshift_func_int8_t_s_u(((safe_mul_func_uint16_t_u_u(((safe_div_func_int8_t_s_s(9L, l_1196)) , p_35), (!((safe_add_func_int32_t_s_s(((((*g_168) , p_39) && ((((p_35 >= (((+(*g_229)) , (void*)0) != (((safe_div_func_uint8_t_u_u((p_39 != (***g_287)), p_35)) , 0x4DL) , l_1203))) && p_35) < 0x6EL) , 1L)) || 0x60A000D716487BF2LL), p_39)) == p_39)))) & (**g_228)), 0)), p_39)) & 4294967294UL), 4)))
    { /* block id: 529 */
        int8_t l_1214 = 0xE5L;
        int32_t l_1221 = 0x085BCEEAL;
        const int16_t l_1260 = 0xEDCCL;
        int32_t l_1277 = (-1L);
        int32_t l_1282 = 0L;
        int32_t l_1283 = 0x0BE9EFCCL;
        uint32_t **l_1302 = (void*)0;
        uint32_t ***l_1301 = &l_1302;
        uint16_t l_1303 = 6UL;
        int32_t l_1312[3];
        int64_t l_1326[5];
        int i;
        for (i = 0; i < 3; i++)
            l_1312[i] = 0x70AF1023L;
        for (i = 0; i < 5; i++)
            l_1326[i] = 0x67DB36A0E70D2AF7LL;
        for (g_590 = 0; (g_590 <= 2); g_590 += 1)
        { /* block id: 532 */
            int16_t l_1206 = 0x8D56L;
            int32_t *l_1207 = (void*)0;
            int32_t *l_1208 = (void*)0;
            int32_t l_1209 = 9L;
            int8_t *l_1269 = &g_241;
            int32_t l_1279 = 1L;
            int32_t l_1280 = (-1L);
            int32_t l_1281 = 0xC375567CL;
            int32_t l_1294 = 0x264F0D09L;
            int32_t l_1296 = 0xFAE8F376L;
            uint16_t l_1316[1][6][9] = {{{65527UL,65527UL,0xFA6FL,4UL,0xB37CL,0xFA6FL,0xB37CL,4UL,0xFA6FL},{65527UL,65527UL,0xFA6FL,4UL,0xB37CL,0xFA6FL,0xB37CL,4UL,0xFA6FL},{65527UL,65527UL,0xFA6FL,4UL,0xB37CL,0xFA6FL,0xB37CL,4UL,0xFA6FL},{65527UL,65527UL,0xFA6FL,4UL,0xB37CL,0xFA6FL,0xB37CL,4UL,0xFA6FL},{65527UL,65527UL,0xFA6FL,4UL,0xB37CL,0xFA6FL,0xB37CL,4UL,0xFA6FL},{65527UL,65527UL,0xFA6FL,4UL,0xB37CL,0xFA6FL,0xB37CL,4UL,0xFA6FL}}};
            uint64_t *l_1341 = (void*)0;
            uint64_t **l_1340 = &l_1341;
            int i, j, k;
            l_1209 = ((l_1180[7] = (*****g_913)) >= (safe_lshift_func_int16_t_s_u(l_1206, 11)));
            if (g_95)
                goto lbl_1185;
            l_1221 = (((*g_168) != (0L || (safe_mod_func_uint8_t_u_u((safe_sub_func_int8_t_s_s(l_1214, p_36)), p_35)))) != (safe_div_func_int8_t_s_s(((safe_sub_func_uint32_t_u_u((((g_169 >= p_39) != (safe_lshift_func_uint8_t_u_s((*g_168), 6))) > (((*g_1135) == (void*)0) , (***g_287))), p_39)) & 0x1493C8CFL), l_1214)));
            if (l_1214)
                break;
            for (l_1209 = 0; (l_1209 <= 2); l_1209 += 1)
            { /* block id: 540 */
                const int64_t l_1247 = 0xFB7BFFF6D0C5EE3CLL;
                int32_t l_1285 = 0L;
                int32_t l_1290 = 0xBF6CBA6DL;
                int32_t l_1291 = 0x26458388L;
                int16_t l_1292 = 0x81A7L;
                int32_t l_1293[2][1][3] = {{{0x9A6DE4A4L,0x9A6DE4A4L,0x5CE477C3L}},{{0x9A6DE4A4L,0x9A6DE4A4L,0x5CE477C3L}}};
                uint64_t l_1313 = 18446744073709551613UL;
                int i, j, k;
                (*g_51) = &g_1025[g_590][(l_1209 + 6)];
                if (g_1025[l_1209][(l_1209 + 6)])
                    continue;
                for (g_118 = 2; (g_118 >= 0); g_118 -= 1)
                { /* block id: 545 */
                    uint64_t *l_1235[3];
                    int16_t *l_1236 = &g_240[7];
                    int32_t l_1244[10] = {0x160741A8L,0x1F9D7D02L,0x160741A8L,0x6CA3D620L,0x6CA3D620L,0x160741A8L,0x1F9D7D02L,0x160741A8L,0x6CA3D620L,0x6CA3D620L};
                    uint32_t *l_1245 = (void*)0;
                    uint32_t *l_1246 = &g_926;
                    int i, j, k;
                    for (i = 0; i < 3; i++)
                        l_1235[i] = &g_442[4][1];
                    if (g_1025[g_590][(l_1209 + 6)])
                        break;
                    for (g_923 = 2; (g_923 >= 0); g_923 -= 1)
                    { /* block id: 549 */
                        return g_16[3][6][1];
                    }
                    if ((!((safe_div_func_int16_t_s_s((l_1157 = (safe_add_func_uint64_t_u_u((p_35 || ((safe_lshift_func_uint8_t_u_u((g_1025[l_1209][(l_1209 + 6)] != p_36), (l_1229 != ((safe_lshift_func_int16_t_s_s((!(safe_mul_func_uint8_t_u_u(p_39, (((((*l_1236) &= ((g_95 = l_1221) | (0xF0L && 0x6EL))) < (safe_add_func_uint32_t_u_u(((*l_1246) = (safe_rshift_func_int16_t_s_s((((safe_lshift_func_uint16_t_u_u(((+p_36) ^ (-8L)), g_1025[g_590][(l_1209 + 6)])) , 0x8F217C68L) == l_1244[3]), l_1214))), (**g_51)))) || 0L) , l_1247)))), 14)) > 5L)))) < (***g_804))), p_35))), l_1229)) && p_39)))
                    { /* block id: 556 */
                        int8_t *l_1254[3][8] = {{&g_447,&l_1214,&g_447,(void*)0,(void*)0,&g_447,&l_1214,&g_447},{&g_447,(void*)0,&l_1214,(void*)0,&g_447,&g_447,(void*)0,&l_1214},{&g_447,&g_447,(void*)0,&l_1214,(void*)0,&g_447,&g_447,(void*)0}};
                        int32_t l_1255 = 0xC8E7F14FL;
                        int64_t **l_1258 = (void*)0;
                        int32_t *l_1259[1][8] = {{&l_1221,&l_1221,&l_1221,&l_1221,&l_1221,&l_1221,&l_1221,&l_1221}};
                        int i, j;
                        if (l_1247)
                            goto lbl_1248;
                        l_1221 |= (((p_35 , (p_36 < (safe_add_func_int16_t_s_s(((*l_1236) = (~((p_39 , (**g_228)) & (safe_mod_func_int8_t_s_s((g_241 ^= ((*g_530) != &l_1170[7])), (g_321 = l_1255)))))), (safe_add_func_int64_t_s_s(0x7248F21013469786LL, (&g_531[1] != l_1258))))))) <= 0xE92DL) && g_1025[g_590][(l_1209 + 6)]);
                        l_1221 = (l_1260 < ((((void*)0 != &p_35) ^ p_35) && (safe_unary_minus_func_uint32_t_u(l_1262))));
                    }
                    else
                    { /* block id: 563 */
                        return p_35;
                    }
                }
                for (l_1206 = 0; (l_1206 <= 0); l_1206 += 1)
                { /* block id: 569 */
                    int8_t *l_1274[6][9][4] = {{{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518}},{{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518}},{{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518}},{{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518}},{{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518}},{{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518},{(void*)0,&g_518,(void*)0,&g_518}}};
                    int8_t **l_1273 = &l_1274[5][7][3];
                    int32_t l_1286 = 0L;
                    int32_t l_1288 = 0xCB148F55L;
                    int32_t l_1289[7][10] = {{0xD4520D58L,1L,0L,1L,0xD4520D58L,1L,0L,1L,0xD4520D58L,1L},{0x4281F407L,1L,0x4281F407L,0x54C292B5L,0x4281F407L,1L,0x4281F407L,0x54C292B5L,0x4281F407L,1L},{0xD4520D58L,0x54C292B5L,0L,0x54C292B5L,0xD4520D58L,0x54C292B5L,0L,0x54C292B5L,0xD4520D58L,0x54C292B5L},{0x4281F407L,0x54C292B5L,0x4281F407L,1L,0x4281F407L,0x54C292B5L,0x4281F407L,1L,0x4281F407L,0x54C292B5L},{0xD4520D58L,1L,0L,1L,0xD4520D58L,1L,0L,1L,0xD4520D58L,1L},{0x4281F407L,1L,0x4281F407L,0x54C292B5L,0x4281F407L,1L,0x4281F407L,0x54C292B5L,0x4281F407L,1L},{0xD4520D58L,0x54C292B5L,0L,0x54C292B5L,0xD4520D58L,0x54C292B5L,0L,0x54C292B5L,0xD4520D58L,0x54C292B5L}};
                    int8_t l_1295 = 0x19L;
                    int i, j, k;
                    if ((((g_130[l_1206] == (safe_rshift_func_uint16_t_u_u(6UL, (((((safe_mul_func_int8_t_s_s(p_36, 0x72L)) , ((safe_mod_func_uint8_t_u_u((*g_168), (((l_1269 != (void*)0) | (+(((safe_rshift_func_uint16_t_u_u((((*l_1273) = l_1269) != (void*)0), 12)) == 0xC6A34A8046B43E82LL) ^ l_1247))) | 0L))) || 0xFA0DA45AL)) , g_1025[g_590][(l_1209 + 6)]) && g_1182) <= 0x4233E366324A8333LL)))) && (*g_229)) & g_784[7]))
                    { /* block id: 571 */
                        int32_t **l_1276 = &l_1208;
                        int32_t l_1278 = 0xC7DC5329L;
                        int32_t l_1284 = 0x975CA86DL;
                        int32_t l_1287[4][10][3] = {{{0x3220294FL,1L,1L},{0L,(-1L),0x788E8148L},{0xF1E428F5L,1L,(-1L)},{0xF1E428F5L,1L,0x3220294FL},{0L,0xE4A82CDAL,0L},{0x3220294FL,1L,0xF1E428F5L},{(-1L),1L,0xF1E428F5L},{0x788E8148L,(-1L),0L},{1L,1L,0x3220294FL},{0x788E8148L,0x3220294FL,(-1L)}},{{(-1L),0x3220294FL,0x788E8148L},{0x3220294FL,1L,1L},{0L,(-1L),0x788E8148L},{0xF1E428F5L,1L,(-1L)},{0xF1E428F5L,1L,0x3220294FL},{0L,0xE4A82CDAL,0L},{0x3220294FL,1L,0xF1E428F5L},{(-1L),1L,0xF1E428F5L},{0x788E8148L,(-1L),0L},{1L,1L,0x3220294FL}},{{0x788E8148L,0x3220294FL,(-1L)},{(-1L),0x3220294FL,0x788E8148L},{0x3220294FL,1L,1L},{0L,(-1L),0x788E8148L},{0xF1E428F5L,1L,(-1L)},{0xF1E428F5L,1L,0x3220294FL},{0L,0xE4A82CDAL,0L},{0x3220294FL,1L,0xF1E428F5L},{(-1L),1L,0xF1E428F5L},{0x788E8148L,(-1L),0L}},{{1L,1L,0x3220294FL},{0x788E8148L,0x3220294FL,0xE4A82CDAL},{0xE4A82CDAL,1L,0xF1E428F5L},{1L,0x3208D405L,0x3208D405L},{1L,0xE4A82CDAL,0xF1E428F5L},{0L,0x788E8148L,0xE4A82CDAL},{0L,0x3220294FL,1L},{1L,0x3F74ED99L,1L},{1L,0x3220294FL,0L},{0xE4A82CDAL,0x788E8148L,0L}}};
                        uint16_t l_1297 = 0x822DL;
                        int i, j, k;
                        (*g_51) = ((*l_1276) = (l_1275[7][0][6] = &g_505));
                        ++l_1297;
                    }
                    else
                    { /* block id: 576 */
                        (*g_51) = &l_1180[5];
                    }
                    if (((p_36 , g_1300) == l_1301))
                    { /* block id: 579 */
                        const int16_t l_1315 = 0xDDB3L;
                        l_1289[0][5] |= ((0xFC81395DL >= ((l_1303 , l_1291) & (!p_39))) >= ((((*g_289) = 4UL) > (((!(safe_lshift_func_int16_t_s_u((safe_mul_func_int8_t_s_s(((((((g_442[2][0] |= (*g_229)) > ((g_297 = ((safe_rshift_func_uint16_t_u_u((g_118 || 0x93L), (p_39 | p_35))) , 0x43EF46C1L)) <= l_1312[0])) && (**g_51)) & 0x468C420C2BC5FDE5LL) && p_39) <= l_1313), p_39)), p_39))) , p_39) && l_1314)) || l_1315));
                        l_1316[0][3][8]++;
                    }
                    else
                    { /* block id: 585 */
                        uint8_t l_1327 = 0xBFL;
                        const uint32_t *l_1329 = &g_582[0][0][0];
                        const uint32_t **l_1328 = &l_1329;
                        int16_t *l_1330 = &g_167[4];
                        uint32_t *l_1333[6][5][8] = {{{&g_115,&g_926,&g_115,&g_297,&g_115,&g_926,&g_115,&g_16[4][0][1]},{&g_115,&g_297,&g_926,&g_297,&l_1314,&g_926,&g_115,&g_926},{&l_1314,&g_926,&g_115,&g_926,&l_1314,&g_297,&g_926,&g_297},{&g_115,&g_16[4][0][1],&g_115,&g_926,&g_115,&g_297,&g_115,&g_926},{&g_115,&g_16[0][7][1],&g_115,&g_297,&g_115,&g_926,&g_926,&g_16[4][0][1]}},{{&g_115,&g_16[0][7][1],&g_115,&g_297,(void*)0,&g_297,&g_115,&g_16[0][7][1]},{&g_115,&g_16[4][0][1],&g_926,&g_926,&g_115,&g_297,&g_115,&g_16[0][7][1]},{&g_115,&g_926,&g_115,&g_297,&g_115,&g_926,&g_115,&g_16[4][0][1]},{&g_115,&g_297,&g_926,&g_297,&l_1314,&g_926,&g_115,&g_926},{&l_1314,&g_926,&g_115,&g_926,&l_1314,&g_297,&g_926,&g_297}},{{&g_115,&g_16[4][0][1],&g_115,&g_926,&g_115,&g_297,&g_115,&g_926},{&g_115,&g_16[0][7][1],&g_115,&g_297,&g_115,&g_926,&g_926,&g_16[4][0][1]},{&g_115,&g_16[0][7][1],&g_115,&g_297,(void*)0,&g_297,&g_115,&g_16[0][7][1]},{&g_115,&g_16[4][0][1],&g_926,&g_926,&g_115,&g_297,&g_115,&g_16[0][7][1]},{&g_115,&g_926,&g_115,&g_297,&g_115,&g_926,&g_115,&g_16[4][0][1]}},{{&g_115,&g_297,&g_926,&g_297,&l_1314,&g_926,&g_115,&g_926},{&l_1314,&g_926,&g_115,&g_926,&l_1314,&g_297,&g_926,&g_297},{&g_115,&g_16[4][0][1],&g_115,&g_926,&g_115,&g_297,&g_115,&g_926},{&g_115,&g_16[0][7][1],&g_115,&g_297,&g_115,&g_926,&g_926,&g_16[4][0][1]},{&g_115,&g_16[0][7][1],&g_115,&g_297,(void*)0,&g_297,&g_115,&g_16[0][7][1]}},{{&g_115,&g_16[4][0][1],&g_926,&g_926,&g_115,&g_297,&l_1314,&l_1314},{&l_1314,&g_16[4][0][1],(void*)0,&g_297,(void*)0,&g_16[4][0][1],&l_1314,&g_297},{&g_115,&g_297,&g_115,&g_926,&g_926,&g_16[4][0][1],&g_115,&g_16[4][0][1]},{&g_926,&g_16[4][0][1],&g_115,&g_16[4][0][1],&g_926,&g_926,&g_115,&g_297},{&g_115,&g_297,&l_1314,&g_16[4][0][1],(void*)0,&g_297,(void*)0,&g_16[4][0][1]}},{{&l_1314,&l_1314,&l_1314,&g_926,&g_115,&g_16[0][7][1],&g_115,&g_297},{(void*)0,&l_1314,&g_115,&g_297,&g_115,&g_297,&g_115,&l_1314},{(void*)0,&g_297,&g_115,&g_16[0][7][1],&g_115,&g_926,&l_1314,&l_1314},{&l_1314,&g_16[4][0][1],(void*)0,&g_297,(void*)0,&g_16[4][0][1],&l_1314,&g_297},{&g_115,&g_297,&g_115,&g_926,&g_926,&g_16[4][0][1],&g_115,&g_16[4][0][1]}}};
                        uint64_t *l_1342 = &g_322;
                        int i, j, k;
                        g_1025[g_590][(l_1209 + 6)] = (((*l_1330) |= ((p_39 ^ (&l_1314 == ((*l_1328) = func_77(g_924, (safe_add_func_uint32_t_u_u(((0xC70BAECB05FB115CLL && (-5L)) && ((safe_div_func_int32_t_s_s((~(&g_881 != (void*)0)), (safe_sub_func_int32_t_s_s(g_130[l_1206], 0x10DD0D7CL)))) > l_1326[3])), l_1327)))))) <= p_36)) || (***g_287));
                        l_1293[0][0][1] |= (((*g_52) == (safe_mod_func_uint32_t_u_u(((g_115--) < (safe_rshift_func_int16_t_s_u(g_195, 0))), (**g_51)))) | ((*l_1269) = g_519[0][4]));
                        l_1279 = ((((((safe_add_func_uint32_t_u_u((((p_35 != (*g_168)) == p_39) || (*g_168)), 0xB8A1B2E4L)) , (void*)0) != l_1340) , ((*l_1342) = (p_39 , (*g_229)))) , 0x38BB4410L) <= (**g_51));
                    }
                }
            }
        }
        l_1277 &= 0xA6255593L;
    }
    else
    { /* block id: 599 */
        int64_t *l_1343 = &g_616;
        int32_t l_1348 = 3L;
        int32_t l_1353 = 0L;
        l_1353 |= ((((((void*)0 == &g_784[0]) > ((*l_1343) &= p_36)) , (safe_mul_func_int8_t_s_s((safe_add_func_uint64_t_u_u(l_1348, (l_1348 | (((****g_914) = (safe_rshift_func_uint8_t_u_u((p_36 == (l_1348 | 7L)), (((safe_rshift_func_int8_t_s_u(9L, 1)) || p_39) <= p_39)))) == g_582[0][0][0])))), 0xADL))) != 0x5FB9L) | 0x95E3L);
        if (g_242)
            goto lbl_1185;
    }
    l_1275[0][0][6] = &l_1180[5];
    return l_1354[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_16 g_13 g_52 g_9 g_95 g_7 g_322 g_321 g_201 g_268 g_297 g_191 g_115 g_289 g_130 g_272 g_198 g_287 g_288 g_51 g_228 g_229 g_195 g_240 g_168 g_135 g_435 g_442 g_450 g_320 g_319 g_169 g_118 g_477 g_505 g_519 g_523 g_530 g_243 g_541 g_518 g_241 g_167 g_590 g_601 g_616 g_584 g_785 g_784 g_595 g_914 g_804 g_805 g_1134 g_597 g_598 g_531 g_447
 * writes: g_115 g_322 g_201 g_95 g_297 g_118 g_195 g_272 g_320 g_52 g_169 g_167 g_435 g_442 g_450 g_240 g_477 g_198 g_505 g_519 g_518 g_243 g_541 g_590 g_601 g_616 g_785 g_523 g_595 g_1134 g_191
 */
static int64_t  func_47(const int32_t *** p_48, int32_t *** p_49)
{ /* block id: 25 */
    uint32_t * const l_57[5][7] = {{&g_16[0][0][0],&g_16[4][2][2],&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1]},{(void*)0,&g_16[4][0][1],(void*)0,&g_16[4][2][2],&g_16[4][0][1],(void*)0,&g_16[4][0][1]},{&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1],&g_16[2][2][0],&g_16[4][0][1],(void*)0,&g_16[4][0][1]},{&g_16[4][0][1],&g_16[4][2][2],&g_16[4][2][2],&g_16[4][0][1],&g_16[4][0][1],(void*)0,&g_16[4][0][1]},{&g_16[4][0][1],&g_16[0][0][0],&g_16[4][2][2],&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1]}};
    int32_t l_58 = (-4L);
    uint32_t *l_86 = (void*)0;
    uint64_t *l_94[4][5][10] = {{{&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,(void*)0},{&g_95,&g_95,(void*)0,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,(void*)0,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,(void*)0},{&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95},{&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,(void*)0}},{{&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,(void*)0},{&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95},{&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,(void*)0,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95}},{{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,(void*)0},{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95}},{{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95},{&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,(void*)0},{&g_95,&g_95,&g_95,&g_95,&g_95,&g_95,(void*)0,&g_95,&g_95,&g_95}}};
    int32_t l_96 = (-1L);
    int32_t * const ***l_1137 = (void*)0;
    int32_t * const ***l_1138 = &g_1134[1][2][7];
    int32_t **l_1140 = &g_1136;
    int32_t ***l_1139 = &l_1140;
    uint32_t l_1141 = 0x944D035CL;
    uint16_t *l_1142 = &g_435;
    int32_t l_1143 = 0x7176C0F0L;
    int i, j, k;
    l_1143 &= ((safe_sub_func_int8_t_s_s((l_57[2][1] == (g_191 = (((*l_1142) = ((g_16[5][3][0] , (l_58 , (safe_rshift_func_int8_t_s_s((safe_add_func_uint64_t_u_u(((((safe_rshift_func_int8_t_s_u((((safe_lshift_func_uint16_t_u_u(((***g_804) = (safe_mod_func_uint16_t_u_u((g_13 > (((*l_1138) = func_69((l_96 = ((((safe_lshift_func_uint16_t_u_s(func_74(func_77(func_80(l_86, (l_58 = ((safe_mul_func_uint16_t_u_u((safe_add_func_uint32_t_u_u((~l_58), l_58)), (safe_mod_func_uint32_t_u_u(0UL, (g_16[4][0][1] , (*g_52)))))) , 0xE2D3E462BFC98A5CLL)), (*g_52), l_96, l_96), l_96), l_96), 5)) & 0xDBEAL) , (void*)0) != (void*)0)), l_86)) != l_1139)), 0xDBEBL))), 15)) && (*g_168)) < g_597), 2)) , &g_51) == p_48) | g_598), (**g_530))), g_447)))) <= l_1141)) , (void*)0))), g_7)) | (-1L));
    return (**g_530);
}


/* ------------------------------------------ */
/* 
 * reads : g_51 g_1134
 * writes: g_52
 */
static int32_t * const ** func_69(uint64_t  p_70, int32_t * p_71)
{ /* block id: 503 */
    int64_t *l_1129 = (void*)0;
    int32_t l_1130 = 0x60A87C6BL;
    int32_t *l_1133 = &g_595;
    int32_t * const *l_1132 = &l_1133;
    int32_t * const **l_1131 = &l_1132;
    (*g_51) = &l_1130;
    return g_1134[1][2][7];
}


/* ------------------------------------------ */
/* 
 * reads : g_16 g_914 g_287 g_288 g_289 g_195 g_804 g_805 g_51 g_52 g_595 g_201 g_9 g_13 g_169
 * writes: g_195 g_272 g_523 g_52 g_595 g_201 g_169
 */
static uint16_t  func_74(int32_t * p_75, const uint16_t  p_76)
{ /* block id: 496 */
    uint16_t **l_1121 = (void*)0;
    int32_t l_1126 = 0xAA1BAFA5L;
    int32_t l_1127 = 0x9C23E8ADL;
    uint32_t l_1128[2];
    int i;
    for (i = 0; i < 2; i++)
        l_1128[i] = 0x0D22A124L;
    (*g_51) = func_77(g_16[3][1][3], (l_1127 ^= (safe_lshift_func_uint16_t_u_u((1UL < ((***g_804) = (****g_914))), (safe_add_func_int32_t_s_s((safe_rshift_func_uint16_t_u_s(((255UL <= ((((l_1121 = (**g_914)) == (void*)0) < (safe_add_func_int8_t_s_s(((safe_mul_func_uint16_t_u_u(p_76, (0xAC0A04B69E99F8BDLL != 0xEAEEE86B5ED01C4ALL))) < 0x6BE2L), l_1126))) > l_1126)) >= l_1126), 12)), 1UL))))));
    return l_1128[1];
}


/* ------------------------------------------ */
/* 
 * reads : g_272 g_523 g_51 g_52 g_595 g_201 g_9 g_13 g_169 g_505
 * writes: g_272 g_523 g_52 g_595 g_201 g_169
 */
static int32_t * func_77(uint32_t  p_78, int64_t  p_79)
{ /* block id: 343 */
    int32_t *l_790 = &g_595;
    uint8_t **l_798 = &g_168;
    uint8_t ***l_797 = &l_798;
    uint16_t ***l_807 = &g_288;
    int32_t l_880[2];
    const uint16_t l_910 = 1UL;
    uint8_t l_973[3][5];
    int16_t l_1000[5] = {0x062CL,0x062CL,0x062CL,0x062CL,0x062CL};
    int32_t l_1039[9] = {(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L),(-1L)};
    uint16_t ****l_1108 = &g_287;
    int64_t **l_1109 = (void*)0;
    int32_t *l_1114 = (void*)0;
    int i, j;
    for (i = 0; i < 2; i++)
        l_880[i] = 1L;
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 5; j++)
            l_973[i][j] = 246UL;
    }
    for (g_272 = 1; (g_272 <= 9); g_272 += 1)
    { /* block id: 346 */
        return &g_9;
    }
    for (g_523 = 20; (g_523 < 19); g_523 = safe_sub_func_int64_t_s_s(g_523, 3))
    { /* block id: 351 */
        uint16_t ****l_817 = (void*)0;
        int32_t l_854 = 0xF7D790D5L;
        int32_t l_855[8][6][5] = {{{(-1L),6L,0xAC7F24C4L,0x29F0DB78L,1L},{2L,0x7D810235L,(-1L),(-1L),0xFACF10C5L},{0x05D9683DL,3L,0x630D33BEL,0x630D33BEL,3L},{5L,(-3L),0x943EEF67L,0x9AE1BAECL,0L},{3L,0L,0x34B35848L,1L,0x29F0DB78L},{0x04CCE7BEL,0x661D17D3L,0xCD3AE9FAL,0L,0L}},{{3L,0x923A2A2DL,0L,0xAB585503L,0x294D6D40L},{5L,0L,0xD25BADF6L,0xFACF10C5L,2L},{0x05D9683DL,0x9762DD4BL,0xC5258B64L,0x2E26A32EL,0xEEBDCF4EL},{2L,0x9D73B669L,0x67D1BFE0L,0L,(-3L)},{(-1L),0xCE2E3F04L,0x1FB76973L,(-1L),3L},{(-3L),0x4719D6ABL,0x1F98BE5CL,(-1L),2L}},{{0xAC7F24C4L,9L,0xA6CDEE51L,0x294D6D40L,0x34B35848L},{0x0DC3AE14L,(-1L),(-1L),(-7L),(-7L)},{0x445DF674L,0xAC7F24C4L,0x445DF674L,0L,0L},{(-1L),(-4L),0x78CF08E1L,1L,1L},{0L,0x05D9683DL,0xAB585503L,1L,0x309E7856L},{1L,(-7L),0x78CF08E1L,1L,0x9D73B669L}},{{0L,0xA6CDEE51L,0x445DF674L,0x2E26A32EL,0x8BBDA18BL},{2L,0L,(-1L),0x67D1BFE0L,0L},{(-1L),0x737AAA71L,0xA6CDEE51L,0xCE802528L,0x630D33BEL},{0xCD3AE9FAL,5L,0xD25BADF6L,0x0DC3AE14L,5L},{0x309E7856L,0L,(-1L),0L,0xD2C50379L},{0x7BF11548L,(-7L),1L,1L,0x67D1BFE0L}},{{0x2E26A32EL,0x4567D093L,0L,0x737AAA71L,0L},{0x04CCE7BEL,0x943EEF67L,(-4L),0x943EEF67L,0x04CCE7BEL},{(-5L),0xAC7F24C4L,0L,0L,0L},{0xCD3AE9FAL,(-4L),1L,(-3L),0xFFC7AB24L},{1L,0x923A2A2DL,0x9718B71DL,0xAC7F24C4L,0L},{7L,(-3L),(-1L),7L,0x04CCE7BEL}},{{0L,0x603639FBL,0x58C653DAL,0xD2C50379L,0L},{1L,0x04CCE7BEL,2L,0x9D73B669L,0x67D1BFE0L},{0x58C653DAL,1L,0x4567D093L,0x5E872C04L,0xD2C50379L},{(-1L),1L,(-4L),2L,5L},{1L,0x2E26A32EL,3L,0xAC7F24C4L,0x630D33BEL},{0xD25BADF6L,0x87DA9C87L,0x7BF11548L,(-1L),0L}},{{0xAC7F24C4L,0x5E872C04L,0x8BBDA18BL,0x630D33BEL,0x8BBDA18BL},{0x8D4F5152L,0x8D4F5152L,5L,0x943EEF67L,0x9D73B669L},{0L,0L,0x4567D093L,0x34B35848L,0x309E7856L},{0x225FF147L,0x0938C81EL,(-2L),0xCD3AE9FAL,1L},{0xC5258B64L,0L,(-1L),0L,0L},{0x009CE972L,0x8D4F5152L,(-1L),0xD25BADF6L,(-7L)}},{{0x923A2A2DL,0x5E872C04L,0x9C12B4C8L,0xC5258B64L,0x34B35848L},{0x943EEF67L,0x87DA9C87L,0x9AE1BAECL,0x67D1BFE0L,2L},{9L,0x2E26A32EL,0L,0x1FB76973L,0x923A2A2DL},{0L,1L,7L,0x1F98BE5CL,(-2L)},{0xC5258B64L,1L,0x4F12025FL,1L,1L},{0xFFC7AB24L,0x04CCE7BEL,1L,1L,0x8D4F5152L}}};
        uint8_t l_956[1];
        int8_t l_971[2][8][1];
        int8_t l_1024 = 0xA5L;
        int32_t l_1026 = 0x0424D114L;
        int i, j, k;
        for (i = 0; i < 1; i++)
            l_956[i] = 0x89L;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 8; j++)
            {
                for (k = 0; k < 1; k++)
                    l_971[i][j][k] = 0xDBL;
            }
        }
        (*g_51) = l_790;
        (*l_790) = (**g_51);
        for (g_201 = (-22); (g_201 >= 58); g_201 = safe_add_func_int16_t_s_s(g_201, 1))
        { /* block id: 356 */
            int16_t *l_800 = &g_240[7];
            uint64_t *l_801 = &g_95;
            uint16_t * const ***l_806 = &g_804;
            int32_t l_808 = (-1L);
            int32_t l_818[1][9];
            const int64_t **l_850 = &g_531[3];
            uint16_t *****l_911 = (void*)0;
            int32_t l_972[2];
            uint16_t * const **l_991[3][6][9] = {{{&g_805[3],(void*)0,&g_805[3],&g_805[3],(void*)0,(void*)0,&g_805[1],(void*)0,(void*)0},{&g_805[3],(void*)0,&g_805[3],(void*)0,&g_805[3],(void*)0,&g_805[1],&g_805[3],&g_805[3]},{&g_805[3],(void*)0,(void*)0,&g_805[3],&g_805[3],&g_805[3],&g_805[3],(void*)0,&g_805[3]},{&g_805[3],&g_805[2],&g_805[3],(void*)0,&g_805[3],&g_805[3],&g_805[2],&g_805[3],&g_805[3]},{&g_805[3],&g_805[3],&g_805[3],&g_805[3],(void*)0,(void*)0,&g_805[3],&g_805[3],&g_805[3]},{&g_805[3],&g_805[3],&g_805[1],&g_805[1],&g_805[2],(void*)0,(void*)0,&g_805[3],&g_805[1]}},{{&g_805[0],&g_805[3],&g_805[3],&g_805[3],&g_805[3],&g_805[3],&g_805[2],&g_805[3],&g_805[1]},{&g_805[3],&g_805[3],(void*)0,&g_805[3],&g_805[3],&g_805[3],&g_805[2],&g_805[3],(void*)0},{&g_805[3],&g_805[3],&g_805[3],&g_805[3],(void*)0,(void*)0,&g_805[3],&g_805[3],&g_805[3]},{&g_805[2],&g_805[2],&g_805[3],&g_805[3],(void*)0,&g_805[3],&g_805[3],&g_805[1],&g_805[3]},{&g_805[3],(void*)0,&g_805[3],(void*)0,&g_805[2],&g_805[3],&g_805[2],(void*)0,&g_805[3]},{(void*)0,(void*)0,&g_805[3],&g_805[3],&g_805[2],&g_805[1],&g_805[2],(void*)0,&g_805[1]}},{{(void*)0,&g_805[2],(void*)0,&g_805[3],&g_805[3],&g_805[3],(void*)0,&g_805[3],(void*)0},{&g_805[3],&g_805[3],&g_805[3],(void*)0,(void*)0,&g_805[3],&g_805[3],&g_805[3],&g_805[3]},{&g_805[3],&g_805[2],&g_805[3],&g_805[3],&g_805[0],&g_805[2],&g_805[2],&g_805[3],&g_805[3]},{&g_805[3],(void*)0,&g_805[3],(void*)0,&g_805[3],&g_805[2],&g_805[3],&g_805[3],(void*)0},{&g_805[0],&g_805[3],&g_805[3],&g_805[1],&g_805[3],&g_805[1],&g_805[1],&g_805[3],&g_805[1]},{(void*)0,&g_805[3],(void*)0,(void*)0,&g_805[3],&g_805[3],&g_805[3],&g_805[3],&g_805[3]}}};
            int64_t *l_1008 = &g_130[0];
            int64_t *l_1015 = &g_616;
            int32_t *l_1016 = &l_808;
            int32_t *l_1017 = (void*)0;
            int32_t *l_1018 = &g_595;
            int32_t l_1019[7][7][4] = {{{1L,0L,0x36A9119BL,0L},{0x0646B43FL,(-1L),0xCB69C781L,0xEF6DA0C3L},{2L,0x0BD5068EL,1L,0xEA259A5EL},{(-6L),1L,0x5121F210L,1L},{0xC3ADEEE2L,(-6L),0x0646B43FL,0x992E7D41L},{1L,0x8698E4D0L,0xFA03047FL,(-6L)},{0xA09223F5L,0xBAC507FFL,1L,0x2AAFC509L}},{{0xA09223F5L,0x13CEC1B8L,0xFA03047FL,0x5121F210L},{1L,0x2AAFC509L,0x0646B43FL,0x36A9119BL},{0xC3ADEEE2L,0xC3ADEEE2L,0x5C390D9CL,(-6L)},{0xFA03047FL,8L,0x2AAFC509L,0L},{0x5121F210L,(-7L),0L,0xCBBFF888L},{0x04C0682CL,0x509960D5L,0x0646B43FL,0x47E848E9L},{0x2AAFC509L,3L,0L,0xBAC507FFL}},{{0xEA259A5EL,0x5121F210L,(-1L),0L},{0x5E54E06FL,0x3813302EL,0x3813302EL,0x5E54E06FL},{(-1L),0x6CDC9631L,0xA09223F5L,8L},{0x13CEC1B8L,2L,(-7L),(-6L)},{0xC3ADEEE2L,0L,3L,(-6L)},{0x0646B43FL,2L,0x388863F0L,8L},{0xEF6DA0C3L,0x6CDC9631L,1L,0x5E54E06FL}},{{1L,0x3813302EL,0x13CEC1B8L,0L},{2L,0x5121F210L,0x36A9119BL,0xBAC507FFL},{0x9728FBA4L,3L,0xB9B10037L,0x47E848E9L},{0x66A5C07CL,0x509960D5L,8L,0xCBBFF888L},{0x3813302EL,(-7L),1L,0L},{0L,8L,7L,(-6L)},{(-6L),0xC3ADEEE2L,(-6L),0x0646B43FL}},{{0xBAC507FFL,0x0BD5068EL,0x509960D5L,0x5C390D9CL},{0xCB69C781L,0L,0xC3ADEEE2L,0x0BD5068EL},{0x5C390D9CL,0x388863F0L,0xC3ADEEE2L,1L},{0xCB69C781L,0xA09223F5L,0x509960D5L,0x3813302EL},{0xBAC507FFL,0xFA03047FL,(-6L),0xEF6DA0C3L},{(-6L),0xEF6DA0C3L,7L,0x66A5C07CL},{0L,0x5E54E06FL,1L,0xEA259A5EL}},{{0x3813302EL,1L,8L,(-1L)},{0x66A5C07CL,(-1L),0xB9B10037L,0xA09223F5L},{0x9728FBA4L,0xCB69C781L,0x36A9119BL,(-1L)},{2L,0L,0x13CEC1B8L,0x13CEC1B8L},{1L,1L,1L,1L},{0xEF6DA0C3L,5L,0x388863F0L,0x6CDC9631L},{0x0646B43FL,0x13CEC1B8L,3L,0x388863F0L}},{{0xC3ADEEE2L,0x13CEC1B8L,(-7L),0x6CDC9631L},{0x13CEC1B8L,5L,0xA09223F5L,1L},{(-1L),1L,0x3813302EL,0x13CEC1B8L},{0x5E54E06FL,0L,(-1L),(-1L)},{0xEA259A5EL,0xCB69C781L,0L,0xA09223F5L},{0x2AAFC509L,(-1L),0x0646B43FL,(-1L)},{0x04C0682CL,1L,0L,0xEA259A5EL}}};
            int32_t *l_1020 = &l_855[5][5][1];
            int32_t *l_1021 = (void*)0;
            int32_t *l_1022 = &g_595;
            int32_t *l_1023[9][4] = {{(void*)0,&g_925[1],(void*)0,&g_925[1]},{&l_880[1],(void*)0,&g_925[2],(void*)0},{&g_925[1],(void*)0,(void*)0,&g_925[1]},{(void*)0,&g_925[1],&l_880[1],(void*)0},{(void*)0,&l_880[1],(void*)0,&g_925[2]},{&g_925[1],(void*)0,&g_925[2],&g_925[2]},{&l_880[1],&l_880[1],(void*)0,(void*)0},{(void*)0,&g_925[1],(void*)0,&g_925[1]},{&l_854,(void*)0,(void*)0,&l_880[1]}};
            uint8_t l_1027 = 0x7EL;
            int i, j, k;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 9; j++)
                    l_818[i][j] = (-1L);
            }
            for (i = 0; i < 2; i++)
                l_972[i] = 3L;
        }
    }
    for (g_201 = (-29); (g_201 == 3); g_201 = safe_add_func_uint16_t_u_u(g_201, 6))
    { /* block id: 457 */
        int32_t l_1032 = 0xA6F60AF1L;
        int32_t l_1035 = (-9L);
        int32_t l_1036 = (-5L);
        int32_t l_1037 = 0x579681F6L;
        int32_t l_1038 = 0L;
        if ((**g_51))
        { /* block id: 458 */
            int32_t *l_1033 = &l_880[0];
            int32_t *l_1034[6][10][4] = {{{(void*)0,&g_925[3],&g_13,&g_9},{&g_925[1],&g_1025[2][4],&g_1025[0][3],&g_1025[2][4]},{&g_925[1],(void*)0,&g_13,(void*)0},{(void*)0,&g_1025[2][4],&g_595,&g_925[0]},{(void*)0,&g_1025[2][4],&g_9,&g_505},{&g_505,(void*)0,(void*)0,&g_9},{&g_1025[0][3],&l_880[0],(void*)0,(void*)0},{&g_925[0],&g_13,&g_13,&g_925[0]},{&g_925[3],&g_925[1],(void*)0,&g_1025[2][4]},{&g_13,&g_1025[2][4],&g_13,&g_595}},{{(void*)0,&g_505,&g_925[0],&g_595},{&g_1025[0][3],&g_1025[2][4],&l_880[0],&g_1025[2][4]},{(void*)0,&g_925[1],(void*)0,&g_925[0]},{&g_925[1],&g_13,&g_595,(void*)0},{&g_925[1],&l_880[0],&g_13,&g_9},{&g_925[3],(void*)0,(void*)0,&g_505},{&g_925[1],&g_1025[2][4],&g_925[1],&g_925[0]},{&g_925[0],&g_1025[2][4],&g_925[0],(void*)0},{&g_925[1],(void*)0,&g_9,&g_1025[2][4]},{&g_1025[2][4],&g_1025[2][4],&g_9,&g_9}},{{&g_925[1],&g_925[3],&g_925[0],&g_9},{&g_925[0],&l_880[0],&g_925[1],&g_925[1]},{&g_925[1],&g_925[1],(void*)0,(void*)0},{&g_925[3],(void*)0,&g_13,&g_925[0]},{&g_925[1],&g_925[3],&g_595,&g_595},{&g_925[1],&g_925[1],(void*)0,&g_505},{(void*)0,&g_1025[2][4],&l_880[0],&g_925[1]},{&g_1025[0][3],&g_13,&g_925[0],&l_880[0]},{(void*)0,&g_13,&g_13,&g_925[1]},{&g_13,&g_1025[2][4],(void*)0,&g_505}},{{&g_925[3],&g_925[1],&g_13,&g_595},{&g_925[0],&g_925[3],(void*)0,&g_925[0]},{&g_1025[0][3],(void*)0,(void*)0,(void*)0},{&g_505,&g_925[1],&g_9,&g_925[1]},{(void*)0,&l_880[0],&g_595,&g_9},{(void*)0,&g_925[3],&g_13,&g_9},{&g_925[1],&g_1025[2][4],&g_1025[0][3],&g_1025[2][4]},{&g_925[1],(void*)0,&g_13,(void*)0},{(void*)0,&g_1025[2][4],&g_595,&g_925[0]},{(void*)0,&g_1025[2][4],&g_9,&g_505}},{{&g_505,(void*)0,(void*)0,&g_9},{&g_1025[0][3],&l_880[0],(void*)0,(void*)0},{&g_925[0],&g_13,&g_13,&g_925[0]},{&g_925[3],&g_925[1],(void*)0,&g_1025[2][4]},{&g_13,&g_1025[2][4],&g_13,&g_595},{(void*)0,&g_505,&g_925[0],&g_595},{&g_1025[0][3],&g_1025[2][4],&l_880[0],(void*)0},{&g_595,&g_925[1],&g_1025[0][3],&g_13},{&g_13,(void*)0,&g_1025[1][1],&g_1025[0][3]},{&g_925[1],&g_505,&g_925[3],&l_880[0]}},{{&g_1025[2][4],&g_13,&l_880[0],&l_880[0]},{&g_505,&l_1032,&g_505,&l_880[0]},{&g_13,(void*)0,&l_880[0],&g_9},{&g_13,&l_880[0],&g_1025[2][4],(void*)0},{(void*)0,&g_925[3],&g_1025[2][4],&l_880[0]},{&g_13,&g_1025[2][4],&l_880[0],&g_1025[2][4]},{&g_13,&g_505,&g_505,&g_925[1]},{&g_505,&g_925[1],&l_880[0],&g_595},{&g_1025[2][4],&l_880[0],&g_925[3],&l_880[0]},{&g_925[1],&g_925[3],&g_1025[1][1],&g_1025[1][1]}}};
            uint64_t l_1040[5][7] = {{18446744073709551615UL,0xD288026AE1E56485LL,18446744073709551615UL,0xF04CDA7271E0EBDCLL,0xB3B0A424A7C3F542LL,0UL,0xB3B0A424A7C3F542LL},{1UL,0x397DBF10835CAE41LL,0x397DBF10835CAE41LL,1UL,0x23A9E0638D45E51ELL,0xF04CDA7271E0EBDCLL,18446744073709551615UL},{18446744073709551615UL,0xF04CDA7271E0EBDCLL,0x23A9E0638D45E51ELL,1UL,0x397DBF10835CAE41LL,0x397DBF10835CAE41LL,1UL},{0xB3B0A424A7C3F542LL,0UL,0xB3B0A424A7C3F542LL,0xF04CDA7271E0EBDCLL,18446744073709551615UL,0xD288026AE1E56485LL,18446744073709551615UL},{0UL,0xDBB62017B20A1AA6LL,0xB3B0A424A7C3F542LL,0x23A9E0638D45E51ELL,0x252E8C3D03609493LL,0x23A9E0638D45E51ELL,0xB3B0A424A7C3F542LL}};
            int i, j, k;
            --l_1040[3][5];
        }
        else
        { /* block id: 460 */
            uint32_t l_1094[2][1][7] = {{{0xE936AA8EL,18446744073709551613UL,18446744073709551613UL,0xE936AA8EL,18446744073709551613UL,18446744073709551613UL,0xE936AA8EL}},{{0xE673B996L,1UL,0xE673B996L,0xE673B996L,1UL,0xE673B996L,0xE673B996L}}};
            int i, j, k;
            (*l_790) = (-1L);
            if (p_78)
                continue;
            for (g_169 = 19; (g_169 != 38); g_169 = safe_add_func_int8_t_s_s(g_169, 7))
            { /* block id: 465 */
                uint32_t l_1071 = 0UL;
                const int32_t l_1072 = 0L;
                int32_t l_1073 = 5L;
                int32_t l_1081 = 0L;
            }
            (*g_51) = (*g_51);
        }
    }
    return l_1114;
}


/* ------------------------------------------ */
/* 
 * reads : g_9 g_95 g_13 g_16 g_7 g_322 g_321 g_268 g_191 g_115 g_289 g_130 g_272 g_198 g_287 g_288 g_51 g_201 g_228 g_229 g_52 g_195 g_240 g_168 g_135 g_435 g_297 g_442 g_450 g_320 g_319 g_169 g_118 g_477 g_505 g_519 g_523 g_530 g_243 g_541 g_518 g_241 g_167 g_590 g_601 g_616 g_584 g_785 g_784
 * writes: g_115 g_322 g_201 g_95 g_297 g_118 g_195 g_272 g_320 g_52 g_169 g_167 g_435 g_442 g_450 g_240 g_477 g_198 g_505 g_519 g_518 g_243 g_541 g_590 g_601 g_616 g_785
 */
static uint32_t  func_80(uint32_t * p_81, uint64_t  p_82, int32_t  p_83, uint32_t  p_84, uint8_t  p_85)
{ /* block id: 27 */
    uint8_t l_97 = 0xF6L;
    uint32_t *l_113 = (void*)0;
    uint32_t *l_114[10][5][5] = {{{&g_16[3][6][3],&g_16[0][6][1],&g_16[4][0][1],&g_115,&g_16[3][1][1]},{(void*)0,&g_16[1][7][3],&g_16[4][0][1],&g_115,&g_115},{&g_16[4][0][1],&g_16[1][5][2],&g_16[5][1][1],&g_16[0][6][1],&g_16[3][1][1]},{&g_16[4][0][1],&g_115,(void*)0,(void*)0,(void*)0},{&g_16[3][1][1],&g_115,&g_16[3][1][1],&g_16[1][3][3],(void*)0}},{{&g_16[0][3][2],&g_16[4][0][1],&g_115,(void*)0,&g_16[4][0][1]},{&g_16[4][0][1],&g_16[4][0][1],&g_16[4][7][1],&g_16[4][0][1],(void*)0},{&g_16[1][5][0],&g_16[2][2][1],&g_115,&g_16[4][0][1],&g_16[1][7][3]},{&g_115,&g_115,&g_16[3][1][1],&g_115,&g_115},{&g_115,&g_16[4][0][1],(void*)0,&g_16[0][3][2],&g_16[4][0][1]}},{{&g_115,&g_16[2][1][1],&g_16[5][1][1],&g_115,&g_16[4][0][1]},{&g_115,&g_16[4][0][1],&g_16[4][0][1],&g_16[1][5][0],(void*)0},{&g_115,&g_16[4][6][2],&g_16[4][0][1],&g_16[4][4][0],&g_115},{&g_115,&g_115,&g_115,&g_115,(void*)0},{&g_115,&g_16[4][0][1],&g_16[4][0][1],&g_16[1][5][2],(void*)0}},{{&g_16[1][5][0],&g_16[4][0][1],&g_16[4][0][1],&g_115,&g_16[5][0][3]},{&g_16[4][0][1],(void*)0,&g_115,&g_16[1][5][2],&g_16[3][1][0]},{&g_16[0][3][2],(void*)0,&g_16[4][0][1],&g_16[4][0][1],&g_115},{&g_16[3][1][0],&g_16[4][0][1],&g_16[4][7][1],&g_115,&g_115},{&g_115,&g_16[4][0][1],&g_16[4][0][1],(void*)0,&g_16[4][0][1]}},{{&g_16[3][6][3],&g_115,&g_115,&g_16[4][0][1],&g_16[4][0][1]},{&g_115,&g_16[4][0][1],&g_16[4][0][1],&g_16[2][2][1],&g_16[4][0][1]},{(void*)0,&g_16[4][0][1],&g_115,&g_16[4][0][1],(void*)0},{&g_115,&g_115,(void*)0,&g_115,&g_16[4][0][1]},{&g_16[4][0][1],&g_16[4][4][0],&g_115,&g_16[1][3][3],&g_115}},{{(void*)0,&g_16[4][0][1],&g_16[4][0][1],&g_115,&g_16[4][0][1]},{&g_16[4][0][1],&g_16[1][3][3],&g_16[4][0][1],&g_16[4][0][1],(void*)0},{&g_16[4][0][1],&g_16[1][4][3],&g_16[4][0][1],&g_115,&g_16[4][0][1]},{&g_115,&g_16[4][0][1],&g_16[4][0][1],&g_115,&g_16[4][0][1]},{&g_16[1][4][3],&g_16[4][0][1],&g_16[1][5][0],(void*)0,&g_16[4][0][1]}},{{(void*)0,&g_16[1][5][2],&g_16[4][0][1],&g_16[4][0][1],&g_115},{&g_16[4][0][1],&g_115,&g_16[4][0][1],&g_16[4][0][1],&g_115},{&g_16[3][4][3],&g_16[4][0][1],&g_16[4][0][1],&g_16[2][1][1],&g_16[4][0][1]},{&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1],&g_16[1][4][3],(void*)0},{&g_16[4][0][1],&g_115,&g_115,&g_16[2][4][0],(void*)0}},{{&g_16[4][0][1],&g_16[4][0][1],(void*)0,&g_16[4][0][1],&g_115},{&g_16[3][4][3],&g_16[4][0][1],&g_115,&g_115,&g_16[4][0][1]},{&g_16[4][0][1],(void*)0,&g_16[4][0][1],&g_16[4][0][1],&g_115},{(void*)0,&g_115,&g_115,&g_16[4][6][2],&g_115},{&g_16[1][4][3],&g_16[4][0][1],&g_16[4][0][1],&g_16[4][0][1],&g_16[5][0][3]}},{{&g_115,&g_115,&g_16[4][7][1],&g_115,&g_16[4][7][1]},{&g_16[4][0][1],&g_16[4][0][1],&g_115,&g_16[4][0][1],&g_16[4][0][1]},{&g_16[4][0][1],&g_16[4][6][2],&g_115,&g_16[2][4][0],&g_16[3][4][3]},{(void*)0,&g_16[1][5][0],&g_16[4][0][1],&g_16[1][4][3],&g_115},{&g_16[4][0][1],&g_16[4][6][2],(void*)0,&g_16[2][1][1],&g_115}},{{&g_115,&g_16[4][0][1],&g_16[1][4][3],&g_16[4][0][1],&g_115},{(void*)0,&g_115,&g_16[3][6][3],&g_16[4][0][1],&g_16[3][1][0]},{&g_115,&g_16[4][0][1],&g_16[4][0][1],(void*)0,&g_16[1][4][3]},{&g_16[3][6][3],&g_115,&g_115,&g_115,&g_16[3][1][0]},{&g_115,(void*)0,&g_115,&g_115,&g_115}}};
    int32_t l_116 = 0xEF491993L;
    int32_t l_119 = 3L;
    int32_t l_193 = 5L;
    int32_t l_267[6] = {(-3L),(-3L),(-3L),(-3L),(-3L),(-3L)};
    int64_t * const l_286 = &g_130[0];
    int64_t l_294 = 0xA98145AF6B685A5ALL;
    int64_t l_318 = 0x167734CD83DB8F6ALL;
    int32_t *l_344 = (void*)0;
    int32_t **l_343 = &l_344;
    int32_t ***l_342 = &l_343;
    int16_t *l_386 = (void*)0;
    uint64_t l_474 = 0x8F86351D8BECA8AFLL;
    int32_t l_546 = 0x5CF3C468L;
    uint32_t l_617 = 0x45A170B9L;
    int32_t l_652 = 0x3B8F442AL;
    int64_t l_662 = 0x6885919ACF585464LL;
    int8_t l_675 = (-8L);
    uint64_t l_703 = 18446744073709551615UL;
    const uint16_t *l_725[8][1][3] = {{{&g_268[1][0],&g_435,&g_435}},{{&g_435,&g_435,(void*)0}},{{&g_541,&g_435,(void*)0}},{{&g_268[1][0],&g_435,&g_435}},{{&g_435,&g_435,(void*)0}},{{&g_541,&g_435,(void*)0}},{{&g_268[1][0],&g_435,&g_435}},{{&g_435,&g_435,&g_272}}};
    uint64_t *l_763 = &g_322;
    int8_t *l_781 = &l_675;
    int32_t l_782[2][9][8] = {{{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L}},{{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L},{0L,0x5B8F0228L,(-1L),1L,(-1L),0x5B8F0228L,0L,1L},{0L,0L,(-1L),3L,(-1L),0L,0L,3L}}};
    int i, j, k;
lbl_438:
    if ((l_97 < (safe_mod_func_int64_t_s_s(l_97, (+((safe_add_func_int16_t_s_s(l_97, (g_9 | p_83))) || (safe_lshift_func_int16_t_s_u((safe_div_func_int8_t_s_s(((((safe_lshift_func_uint8_t_u_s(((((safe_mod_func_uint16_t_u_u(((safe_sub_func_int64_t_s_s(((((g_115 = (g_95 >= (g_13 , l_97))) > (l_97 > p_83)) == 0x17CE365DL) & 0x6598L), g_16[4][0][1])) <= 65534UL), l_116)) >= g_7) == 0x7837L) == (-1L)), 4)) , g_95) , 0xFF75L) & g_95), g_16[2][2][2])), 3))))))))
    { /* block id: 29 */
        int32_t *l_117[8] = {&g_9,(void*)0,&g_9,(void*)0,&g_9,(void*)0,&g_9,(void*)0};
        uint32_t l_164 = 0xA2725A88L;
        const uint16_t **l_238 = (void*)0;
        uint8_t *l_252 = &g_198;
        int64_t * const l_285 = (void*)0;
        int i;
        l_119 = 0xBE4CFF9CL;
        for (p_83 = 14; (p_83 < 11); p_83--)
        { /* block id: 33 */
            return g_16[3][5][3];
        }
        for (p_85 = 0; (p_85 == 44); p_85 = safe_add_func_uint64_t_u_u(p_85, 2))
        { /* block id: 38 */
            int64_t *l_129[7][7] = {{&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0]},{&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0]},{(void*)0,&g_130[0],(void*)0,&g_130[0],(void*)0,&g_130[0],(void*)0},{&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0]},{&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0]},{&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0],&g_130[0]},{(void*)0,&g_130[0],(void*)0,&g_130[0],(void*)0,&g_130[0],(void*)0}};
            int32_t l_131[5] = {0xEEB4ECF4L,0xEEB4ECF4L,0xEEB4ECF4L,0xEEB4ECF4L,0xEEB4ECF4L};
            uint8_t *l_134 = &g_135;
            int32_t l_138 = 0x7A66F30BL;
            int32_t l_165 = 0x7CF008C4L;
            int16_t *l_166 = &g_167[3];
            uint8_t l_170 = 1UL;
            uint16_t *l_259 = &g_195;
            uint16_t **l_258 = &l_259;
            uint16_t ***l_257 = &l_258;
            int32_t l_271 = 0x181E6766L;
            int i, j;
        }
        g_322--;
    }
    else
    { /* block id: 127 */
        const int16_t *l_333[10][10][1] = {{{&g_240[6]},{&g_240[8]},{&g_240[6]},{&g_167[4]},{&g_240[8]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_167[3]}},{{&g_167[3]},{&g_240[7]},{&g_240[8]},{&g_167[4]},{&g_240[6]},{&g_240[8]},{&g_240[6]},{&g_167[4]},{&g_240[8]},{&g_240[7]}},{{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_240[8]},{&g_167[4]},{&g_240[6]},{&g_240[8]}},{{&g_240[6]},{&g_167[4]},{&g_240[8]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]}},{{&g_240[8]},{&g_167[4]},{&g_240[6]},{&g_240[8]},{&g_240[6]},{&g_167[4]},{&g_240[8]},{&g_240[7]},{&g_167[3]},{&g_167[3]}},{{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_240[8]},{&g_167[4]},{&g_240[6]},{&g_240[8]},{&g_240[6]},{&g_167[4]}},{{&g_240[8]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_240[8]},{&g_167[4]}},{{&g_240[6]},{&g_240[8]},{&g_240[6]},{&g_167[4]},{&g_240[8]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_167[3]}},{{&g_167[3]},{&g_240[7]},{&g_240[8]},{&g_167[4]},{&g_240[6]},{&g_240[8]},{&g_240[6]},{&g_167[4]},{&g_240[8]},{&g_240[7]}},{{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_167[3]},{&g_167[3]},{&g_240[7]},{&g_240[8]},{&g_167[4]},{&g_240[6]},{&g_240[8]}}};
        int32_t l_334 = 0x745105DDL;
        uint8_t * const *l_339 = (void*)0;
        uint8_t * const **l_340 = &l_339;
        int32_t *l_341 = &l_116;
        int i, j, k;
        l_267[3] &= (((safe_lshift_func_int8_t_s_s(((((*l_341) = ((safe_sub_func_int8_t_s_s((safe_rshift_func_int8_t_s_s(p_83, 5)), ((((l_333[5][0][0] != (void*)0) & (l_97 , (&g_130[0] == &g_130[0]))) > (l_334 < 0x3AL)) == (safe_lshift_func_int8_t_s_u(((safe_add_func_uint32_t_u_u((((*l_340) = l_339) == (void*)0), 9L)) , g_321), 0))))) , p_83)) , (void*)0) != l_342), 4)) , p_84) , 0L);
    }
    for (g_201 = (-20); (g_201 == 27); g_201 = safe_add_func_uint16_t_u_u(g_201, 6))
    { /* block id: 134 */
        int16_t *l_385 = &g_167[3];
        uint32_t l_388 = 0xBF29AA3EL;
        int32_t *l_391 = &g_9;
        int64_t * const l_398 = &g_130[0];
        uint16_t ****l_413 = &g_287;
        int32_t l_441 = (-4L);
        int16_t l_470 = 7L;
        uint16_t * const *l_503 = &g_289;
        uint16_t * const **l_502 = &l_503;
        uint8_t l_577[6] = {2UL,246UL,246UL,2UL,246UL,246UL};
        int32_t l_596 = 1L;
        int32_t l_600 = 0xB60961CDL;
        const int8_t *l_756 = &g_597;
        int i;
        for (g_95 = 0; (g_95 < 35); g_95 = safe_add_func_uint8_t_u_u(g_95, 3))
        { /* block id: 137 */
            uint64_t l_349 = 18446744073709551611UL;
            int32_t l_389 = 0L;
            int32_t l_390[2];
            int8_t *l_532 = &g_518;
            uint64_t l_576 = 18446744073709551606UL;
            const uint32_t *l_583 = &g_584;
            int16_t *l_647 = (void*)0;
            uint16_t **l_655 = (void*)0;
            int32_t ** const l_736[8] = {&l_391,&l_391,&l_391,&l_391,&l_391,&l_391,&l_391,&l_391};
            int32_t l_746 = 0x850789F4L;
            int i;
            for (i = 0; i < 2; i++)
                l_390[i] = 0x5B4049BEL;
            if (l_349)
            { /* block id: 138 */
                int32_t l_382[4];
                int i;
                for (i = 0; i < 4; i++)
                    l_382[i] = (-5L);
                for (p_85 = 0; (p_85 > 15); ++p_85)
                { /* block id: 141 */
                    for (l_97 = 24; (l_97 >= 13); l_97 = safe_sub_func_int32_t_s_s(l_97, 4))
                    { /* block id: 144 */
                        (**l_342) = &p_83;
                    }
                    return g_268[1][1];
                }
                for (g_297 = 11; (g_297 == 5); g_297 = safe_sub_func_uint16_t_u_u(g_297, 6))
                { /* block id: 151 */
                    int16_t l_377 = (-6L);
                    uint16_t *l_387 = &g_272;
                    g_118 = p_85;
                    if (((((safe_sub_func_int64_t_s_s((l_390[0] &= (l_389 |= (safe_sub_func_int16_t_s_s(((safe_mod_func_int16_t_s_s((((safe_div_func_uint16_t_u_u(((3L && ((safe_rshift_func_uint8_t_u_s((safe_rshift_func_int16_t_s_u(p_85, 4)), 5)) , ((safe_unary_minus_func_int8_t_s(((((safe_rshift_func_uint8_t_u_s((safe_mul_func_int8_t_s_s((((safe_add_func_uint32_t_u_u(((safe_sub_func_uint16_t_u_u(((*g_191) & l_377), ((*g_289) = 0UL))) != (safe_rshift_func_int16_t_s_s(((((p_83 & 0x78L) ^ ((((g_320[1] = (safe_add_func_int16_t_s_s(l_382[1], ((*l_387) ^= ((safe_mod_func_int16_t_s_s((((l_386 = l_385) == (void*)0) > 0x4EE3L), g_130[0])) | 0xB9L))))) , p_83) >= 0L) >= p_85)) || 0x9C80L) , (-6L)), 0))), p_85)) , p_84) != g_198), p_83)), 0)) && l_382[0]) | p_85) != l_388))) ^ l_388))) , 0xEFADL), p_85)) , (**g_287)) != l_385), l_377)) || g_16[1][0][0]), 0x9C9CL)))), p_85)) == l_388) != 0xF2E2D8AAL) || p_85))
                    { /* block id: 159 */
                        (*g_51) = l_391;
                        return p_82;
                    }
                    else
                    { /* block id: 162 */
                        return g_201;
                    }
                }
            }
            else
            { /* block id: 166 */
                int32_t l_394 = 0L;
                uint8_t l_395[4][7] = {{4UL,4UL,0UL,4UL,4UL,0UL,4UL},{4UL,0xB0L,0xB0L,4UL,0xB0L,0xB0L,4UL},{0xB0L,4UL,0xB0L,0xB0L,4UL,0xB0L,0xB0L},{4UL,4UL,0UL,4UL,4UL,0UL,4UL}};
                int64_t *l_399[8] = {&l_318,&l_318,&l_318,&l_318,&l_318,&l_318,&l_318,&l_318};
                int i, j;
                l_395[2][5] = ((safe_div_func_int64_t_s_s((0x48E5D8197AA4DF51LL ^ (**g_228)), 1L)) | (p_82 < (l_394 != (-9L))));
                if ((**g_51))
                    continue;
                for (l_394 = 0; (l_394 <= 2); l_394 += 1)
                { /* block id: 171 */
                    int64_t **l_400 = &l_399[5];
                    uint16_t *****l_414 = &l_413;
                    int64_t l_415 = (-1L);
                    int i, j;
                    if ((((safe_mod_func_uint8_t_u_u(((0UL | (g_268[l_394][l_394] > ((p_82 , (l_398 != ((*l_400) = l_399[6]))) , ((safe_mul_func_uint16_t_u_u((safe_sub_func_int16_t_s_s(((safe_add_func_uint64_t_u_u(18446744073709551615UL, g_195)) > ((safe_div_func_uint16_t_u_u((safe_mul_func_int16_t_s_s((*l_391), (safe_div_func_int64_t_s_s((((*l_414) = l_413) != &g_287), (-1L))))), l_395[2][5])) > (***g_287))), l_415)), 0UL)) && p_83)))) & (*l_391)), (-1L))) , p_85) >= 0xBDA4172FA32C99ABLL))
                    { /* block id: 174 */
                        p_83 = (g_240[7] != ((*g_168) = 1UL));
                        (*g_51) = (*g_51);
                    }
                    else
                    { /* block id: 178 */
                        int32_t *l_434[1];
                        int i;
                        for (i = 0; i < 1; i++)
                            l_434[i] = &l_389;
                        (*l_343) = &p_83;
                        (***l_342) = (safe_rshift_func_int8_t_s_u((((((0x1F3DL & (safe_lshift_func_int8_t_s_u((safe_lshift_func_int8_t_s_s(((safe_sub_func_int32_t_s_s(((**g_51) , (safe_add_func_uint32_t_u_u(((*g_191) = (((((safe_lshift_func_int8_t_s_u(((-1L) && 0x2860FF60L), 7)) & (*g_191)) | ((0x4FCDL >= g_115) , ((safe_div_func_int16_t_s_s(((*l_385) = (safe_sub_func_int64_t_s_s(g_272, (safe_mul_func_int8_t_s_s((***l_342), p_84))))), g_95)) | 253UL))) < (***g_287)) | 5UL)), 4294967295UL))), (**l_343))) , l_389), 0)), p_82))) < g_135) && l_395[0][6]) == p_84) , 0xD5L), 6));
                        ++g_435;
                    }
                }
            }
            for (g_272 = 0; (g_272 <= 4); g_272 += 1)
            { /* block id: 189 */
                int32_t l_440 = 0x1BECB8D5L;
                int32_t l_445 = 0x14B6D7EAL;
                int32_t l_446 = 0xE381F511L;
                uint8_t l_515 = 255UL;
                if (g_272)
                    goto lbl_438;
                for (g_297 = 0; (g_297 <= 1); g_297 += 1)
                { /* block id: 193 */
                    int32_t *l_439[9][10][2] = {{{&l_267[(g_297 + 1)],&l_267[(g_297 + 1)]},{&l_267[3],&l_267[(g_297 + 1)]},{&l_267[(g_297 + 1)],&l_267[(g_297 + 1)]},{&l_267[(g_297 + 1)],&l_390[g_297]},{&l_267[3],&l_267[(g_297 + 1)]},{&l_390[g_297],&l_267[(g_297 + 1)]},{&l_390[g_297],&l_267[(g_297 + 1)]},{&l_267[3],&l_390[g_297]},{&l_267[(g_297 + 1)],&l_267[(g_297 + 1)]},{&l_267[(g_297 + 1)],&l_267[(g_297 + 1)]}},{{&l_267[3],&l_267[(g_297 + 1)]},{&l_267[(g_297 + 1)],&l_267[(g_297 + 1)]},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]}},{{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0}},{{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9}},{{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]}},{{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0}},{{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]}},{{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9}},{{&l_267[(g_297 + 1)],(void*)0},{&l_267[3],&l_267[3]},{(void*)0,&l_267[3]},{&l_267[3],(void*)0},{&l_267[(g_297 + 1)],&g_9},{(void*)0,&l_267[(g_297 + 1)]},{&g_9,(void*)0},{&g_9,&l_267[(g_297 + 1)]},{(void*)0,&g_9},{&l_267[(g_297 + 1)],(void*)0}}};
                    int i, j, k;
                    (*l_343) = &l_267[(g_297 + 1)];
                    for (l_389 = 0; (l_389 <= 1); l_389 += 1)
                    { /* block id: 197 */
                        int i;
                        l_267[l_389] &= 0xDEAEA008L;
                    }
                    g_442[2][0]--;
                    for (l_349 = 0; (l_349 <= 9); l_349 += 1)
                    { /* block id: 203 */
                        int32_t l_448[9][8] = {{0x5791FB3BL,0L,1L,1L,(-7L),(-7L),1L,1L},{0xE7615848L,0xE7615848L,0L,0x2BFE5CF9L,(-7L),0xDD15CBB3L,6L,0xDD15CBB3L},{0x5791FB3BL,1L,0x2BFE5CF9L,1L,0x5791FB3BL,0xFEFA4915L,0xE7615848L,0xDD15CBB3L},{1L,(-7L),6L,0x2BFE5CF9L,0x2BFE5CF9L,6L,(-7L),1L},{0L,0xFEFA4915L,6L,(-7L),0xDD15CBB3L,6L,0xDD15CBB3L,(-7L)},{0xFEFA4915L,1L,0xFEFA4915L,1L,(-7L),6L,0x2BFE5CF9L,0x2BFE5CF9L},{0x2BFE5CF9L,0L,0xE7615848L,0xE7615848L,0L,0x2BFE5CF9L,(-7L),0xDD15CBB3L},{0x2BFE5CF9L,0x5791FB3BL,1L,0L,(-7L),0L,1L,0x5791FB3BL},{0xFEFA4915L,1L,6L,0L,0xDD15CBB3L,1L,1L,0xDD15CBB3L}};
                        int32_t l_449 = 0xBA3D9054L;
                        int i, j;
                        g_450++;
                        if (g_320[g_272])
                            break;
                        (**l_343) = (safe_div_func_int16_t_s_s((l_267[(g_272 + 1)] <= (safe_unary_minus_func_int64_t_s((-1L)))), (safe_lshift_func_int16_t_s_s((safe_sub_func_uint16_t_u_u(l_390[g_297], ((safe_add_func_int8_t_s_s(4L, p_85)) != (0xF243L & ((***g_287)--))))), (g_240[(g_272 + 4)] = (((*g_168) |= ((((safe_div_func_uint16_t_u_u((~(safe_lshift_func_int16_t_s_u((+(g_115 == l_470)), (!(safe_sub_func_int8_t_s_s(l_390[g_297], 0x33L)))))), g_240[7])) , g_319) > l_390[g_297]) != l_474)) & g_272))))));
                        g_118 = ((safe_rshift_func_uint16_t_u_s((*l_344), ((*l_391) == (*g_168)))) , ((***l_342) = (-1L)));
                    }
                }
                for (g_118 = 6; (g_118 >= 0); g_118 -= 1)
                { /* block id: 216 */
                    int32_t l_504 = 1L;
                    int32_t *l_506 = &l_389;
                    int32_t *l_507 = &l_504;
                    int32_t *l_508 = &l_504;
                    int32_t *l_509 = (void*)0;
                    int32_t *l_510 = &l_390[0];
                    int32_t *l_511 = (void*)0;
                    int32_t *l_512 = &l_390[1];
                    int32_t *l_513 = &l_390[0];
                    int32_t *l_514[5] = {&l_389,&l_389,&l_389,&l_389,&l_389};
                    int i;
                    if ((g_477 ^= g_320[g_272]))
                    { /* block id: 218 */
                        uint16_t ** const *l_498 = &g_288;
                        uint8_t *l_501 = &g_198;
                        int i;
                        g_505 ^= (safe_rshift_func_int8_t_s_u((!((safe_sub_func_int32_t_s_s((**g_51), ((l_504 ^= (safe_add_func_uint16_t_u_u(p_82, (g_240[(g_272 + 2)] = (((((safe_lshift_func_int16_t_s_s((safe_rshift_func_uint8_t_u_u(((*g_168) = (*g_168)), ((*l_501) ^= (safe_add_func_int16_t_s_s((((+l_390[0]) , (safe_sub_func_uint16_t_u_u((safe_sub_func_int8_t_s_s((((void*)0 == l_498) <= (p_83 >= (safe_rshift_func_int8_t_s_s(((*g_51) == ((*l_343) = &p_83)), g_435)))), p_84)), p_83))) , g_268[1][2]), p_83))))), 2)) , &g_288) == l_502) >= (*g_191)) | g_195))))) >= p_82))) == 8L)), p_84));
                    }
                    else
                    { /* block id: 225 */
                        if (p_84)
                            break;
                    }
                    ++l_515;
                    --g_519[4][2];
                }
            }
            if (((*l_391) < ((~g_523) | (((*l_532) = (safe_sub_func_uint64_t_u_u(0x06D3B504CB59A954LL, (((((p_84 == (*l_391)) ^ (safe_add_func_uint64_t_u_u((((g_272 > ((safe_lshift_func_int8_t_s_s(l_390[0], 2)) | 0x0AA02A9C730C4FFELL)) , &l_398) != g_530), 0xD328D7F765F64B87LL))) <= (*g_52)) , 0xA254CA6F1A25314ALL) , p_82)))) >= p_83))))
            { /* block id: 233 */
                uint16_t l_569 = 0x8289L;
                int64_t *l_572 = &l_318;
                uint32_t l_575[4][7][4] = {{{1UL,0x68C2A1F5L,4294967287UL,0x8C1710C6L},{0x0F0716BDL,1UL,0x68C2A1F5L,0x68C2A1F5L},{0x86002D92L,0x86002D92L,0x68C2A1F5L,4294967287UL},{0x0F0716BDL,0x43EA68DFL,4294967287UL,1UL},{1UL,4294967287UL,4294967287UL,4294967287UL},{4294967287UL,4294967287UL,1UL,1UL},{4294967287UL,0x43EA68DFL,0x0F0716BDL,4294967287UL}},{{0x68C2A1F5L,0x86002D92L,0x86002D92L,0x68C2A1F5L},{0x68C2A1F5L,1UL,0x0F0716BDL,0x8C1710C6L},{4294967287UL,0x68C2A1F5L,1UL,0xFEC0DF3DL},{4294967287UL,0x6A4F596CL,4294967287UL,0xFEC0DF3DL},{1UL,0x68C2A1F5L,4294967287UL,0x8C1710C6L},{0x0F0716BDL,1UL,0x68C2A1F5L,0x68C2A1F5L},{0x86002D92L,0x86002D92L,0x68C2A1F5L,4294967287UL}},{{0x0F0716BDL,0x43EA68DFL,4294967287UL,1UL},{1UL,4294967287UL,4294967287UL,4294967287UL},{4294967287UL,4294967287UL,1UL,1UL},{4294967287UL,0x43EA68DFL,0x0F0716BDL,4294967287UL},{0x68C2A1F5L,0x86002D92L,0x86002D92L,0x68C2A1F5L},{0x68C2A1F5L,1UL,0x0F0716BDL,0x8C1710C6L},{4294967287UL,0x68C2A1F5L,1UL,0xFEC0DF3DL}},{{4294967287UL,0x6A4F596CL,4294967287UL,0xFEC0DF3DL},{1UL,0x68C2A1F5L,4294967287UL,0x8C1710C6L},{0x0F0716BDL,1UL,0x68C2A1F5L,0x68C2A1F5L},{0x86002D92L,0x86002D92L,0x68C2A1F5L,4294967287UL},{0x0F0716BDL,0x43EA68DFL,4294967287UL,1UL},{1UL,4294967287UL,4294967287UL,4294967287UL},{4294967287UL,4294967287UL,1UL,1UL}}};
                int32_t l_593 = 0x3CA297EFL;
                int32_t l_594 = 0xA982184AL;
                int i, j, k;
                for (g_243 = 0; (g_243 < 52); g_243++)
                { /* block id: 236 */
                    int32_t *l_535 = &l_390[0];
                    int32_t *l_536 = &g_118;
                    int32_t *l_537 = &l_390[0];
                    int32_t *l_538 = &l_193;
                    int32_t *l_539 = (void*)0;
                    int32_t *l_540[10][1];
                    int64_t *l_571 = &l_318;
                    int64_t **l_570 = &l_571;
                    int16_t l_573 = 0xAF16L;
                    int16_t *l_574 = &g_240[5];
                    int i, j;
                    for (i = 0; i < 10; i++)
                    {
                        for (j = 0; j < 1; j++)
                            l_540[i][j] = (void*)0;
                    }
                    for (g_198 = 0; (g_198 <= 2); g_198 += 1)
                    { /* block id: 239 */
                        int i, j;
                        return g_268[g_198][(g_198 + 1)];
                    }
                    g_541--;
                    if ((((safe_add_func_int16_t_s_s(((l_546 ^ (g_130[0] , (((l_441 = (safe_lshift_func_int8_t_s_s((safe_add_func_int8_t_s_s((safe_rshift_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((safe_mod_func_int16_t_s_s(((*l_574) = ((safe_mod_func_uint64_t_u_u(((g_167[3] |= (safe_lshift_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u((((safe_rshift_func_uint16_t_u_u((*l_391), 13)) < (((((safe_mul_func_int8_t_s_s(((*l_532) ^= g_13), (safe_lshift_func_uint16_t_u_s(l_569, 0)))) ^ (p_83 & (0x43L > g_241))) , ((*l_570) = &g_130[0])) != l_572) && (*g_168))) , 0x5D5F804BL), l_573)), (*l_391)))) & p_83), 0x9F87D9EFAEA21824LL)) < (*l_391))), (*g_289))), 15)) <= l_575[2][2][3]), 1)), g_268[1][0])), 7))) || (*l_391)) == (-2L)))) == l_576), l_577[5])) || (-1L)) || (*l_538)))
                    { /* block id: 248 */
                        const uint32_t *l_581 = &g_582[0][0][0];
                        const uint32_t **l_580[4];
                        int32_t l_589 = 1L;
                        int i;
                        for (i = 0; i < 4; i++)
                            l_580[i] = &l_581;
                        g_505 = (((*g_168) = (safe_div_func_int8_t_s_s(((4294967295UL <= ((g_167[3] |= ((&g_16[4][0][1] != (l_583 = &g_115)) , (&g_531[1] != &g_531[1]))) < g_450)) <= (safe_lshift_func_int8_t_s_u(((*g_191) > (*g_191)), (safe_mul_func_uint16_t_u_u(l_389, (-1L)))))), g_319))) && (*g_168));
                        ++g_590;
                        (*l_535) |= (*g_52);
                        if (l_390[0])
                            continue;
                    }
                    else
                    { /* block id: 256 */
                        return p_83;
                    }
                    g_601--;
                }
            }
            else
            { /* block id: 261 */
                int32_t *l_606 = &l_596;
                int32_t *l_607 = &g_595;
                int32_t *l_608 = &g_595;
                int32_t *l_609 = &l_441;
                int32_t *l_610 = &l_267[2];
                int32_t *l_611 = &l_267[3];
                int32_t *l_612 = &l_600;
                int32_t *l_613 = &l_441;
                int32_t *l_614 = (void*)0;
                int32_t *l_615[8] = {&l_390[1],&l_390[1],&l_267[1],&l_390[1],&l_390[1],&l_267[1],&l_390[1],&l_390[1]};
                int i;
                for (g_195 = 24; (g_195 >= 46); g_195++)
                { /* block id: 264 */
                    for (g_118 = 0; (g_118 >= 0); g_118 -= 1)
                    { /* block id: 267 */
                        l_390[0] = ((p_83 <= (***g_287)) || p_84);
                        return g_519[4][2];
                    }
                }
                if (g_505)
                    goto lbl_438;
                l_617++;
            }
            for (g_616 = 18; (g_616 != (-29)); g_616--)
            { /* block id: 277 */
                int64_t **l_636 = (void*)0;
                int32_t *l_650 = &l_116;
                int32_t l_667 = 0L;
                uint64_t *l_704 = &g_442[4][0];
                uint64_t *l_705 = &l_474;
                uint16_t *l_706 = &g_541;
                int32_t l_707 = 0L;
            }
        }
    }
    g_118 ^= (((safe_unary_minus_func_uint64_t_u((*g_229))) | ((*l_763) |= p_82)) == ((g_541 ^ g_268[1][2]) == (p_85 > (safe_div_func_int32_t_s_s((((safe_lshift_func_uint8_t_u_s((*g_168), (p_82 != (safe_unary_minus_func_int8_t_s(((*l_781) = (((((*g_168) || (safe_mul_func_int16_t_s_s((safe_mul_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u((safe_mul_func_int16_t_s_s(((safe_add_func_uint8_t_u_u((safe_rshift_func_uint16_t_u_u((**g_288), 14)), 0xE3L)) || 65535UL), (-1L))), (-1L))), (**g_288))), 0xA7BCL))) > p_84) < 0x8D1FL) > p_84))))))) < l_782[1][4][1]) && p_85), 3L)))));
    if (p_82)
    { /* block id: 337 */
        return g_584;
    }
    else
    { /* block id: 339 */
        int32_t *l_783[10][1] = {{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]},{&l_267[3]}};
        int i, j;
        --g_785[0][0];
        return g_784[3];
    }
}




/* ---------------------------------------- */
//testcase_id 1484153248
int case1484153248(void)
{
    int i, j, k;
	struct timeval start, end, result, temp;
	FILE *fp;
	FILE *ff;
    int print_hash_value = 0,option=0;;
 	long double temp2=1;
	while(1){
 	    temp2 *= 1.0007;
		if(temp2 > 1000000){
 		option =9;
			break;}
	}
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_9, "g_9", print_hash_value);
    transparent_crc(g_13, "g_13", print_hash_value);
    for (i = 0; i < 6; i++)
    {
        for (j = 0; j < 8; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_16[i][j][k], "g_16[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_95, "g_95", print_hash_value);
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_118, "g_118", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_130[i], "g_130[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_135, "g_135", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_167[i], "g_167[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_169, "g_169", print_hash_value);
    transparent_crc(g_195, "g_195", print_hash_value);
    transparent_crc(g_198, "g_198", print_hash_value);
    transparent_crc(g_201, "g_201", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_240[i], "g_240[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_241, "g_241", print_hash_value);
    transparent_crc(g_242, "g_242", print_hash_value);
    transparent_crc(g_243, "g_243", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 4; j++)
        {
            transparent_crc(g_268[i][j], "g_268[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_272, "g_272", print_hash_value);
    transparent_crc(g_297, "g_297", print_hash_value);
    transparent_crc(g_319, "g_319", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_320[i], "g_320[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_321, "g_321", print_hash_value);
    transparent_crc(g_322, "g_322", print_hash_value);
    transparent_crc(g_435, "g_435", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_442[i][j], "g_442[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_447, "g_447", print_hash_value);
    transparent_crc(g_450, "g_450", print_hash_value);
    transparent_crc(g_477, "g_477", print_hash_value);
    transparent_crc(g_505, "g_505", print_hash_value);
    transparent_crc(g_518, "g_518", print_hash_value);
    for (i = 0; i < 8; i++)
    {
        for (j = 0; j < 5; j++)
        {
            transparent_crc(g_519[i][j], "g_519[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_523, "g_523", print_hash_value);
    transparent_crc(g_541, "g_541", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 1; j++)
        {
            for (k = 0; k < 1; k++)
            {
                transparent_crc(g_582[i][j][k], "g_582[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_584, "g_584", print_hash_value);
    transparent_crc(g_590, "g_590", print_hash_value);
    transparent_crc(g_595, "g_595", print_hash_value);
    transparent_crc(g_597, "g_597", print_hash_value);
    transparent_crc(g_598, "g_598", print_hash_value);
    transparent_crc(g_599, "g_599", print_hash_value);
    transparent_crc(g_601, "g_601", print_hash_value);
    transparent_crc(g_616, "g_616", print_hash_value);
    for (i = 0; i < 10; i++)
    {
        transparent_crc(g_784[i], "g_784[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_785[i][j], "g_785[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_881, "g_881", print_hash_value);
    transparent_crc(g_923, "g_923", print_hash_value);
    transparent_crc(g_924, "g_924", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        transparent_crc(g_925[i], "g_925[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_926, "g_926", print_hash_value);
    transparent_crc(g_962, "g_962", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 9; j++)
        {
            transparent_crc(g_1025[i][j], "g_1025[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_1182, "g_1182", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 6; j++)
        {
            for (k = 0; k < 6; k++)
            {
                transparent_crc(g_1391[i][j][k], "g_1391[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1394, "g_1394", print_hash_value);
    transparent_crc(g_1622, "g_1622", print_hash_value);
    transparent_crc(g_1673, "g_1673", print_hash_value);
    transparent_crc(g_1789, "g_1789", print_hash_value);
    transparent_crc(g_1842, "g_1842", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        transparent_crc(g_1896[i], "g_1896[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 3; j++)
        {
            for (k = 0; k < 4; k++)
            {
                transparent_crc(g_1913[i][j][k], "g_1913[i][j][k]", print_hash_value);
                if (print_hash_value) printf("index = [%d][%d][%d]\n", i, j, k);

            }
        }
    }
    transparent_crc(g_1960, "g_1960", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
	gettimeofday(&end,NULL);
	timersub(&end,&start,&result);
	fp = fopen("etime.txt","a");
	ff = fopen("timet.txt","a");
	fprintf(fp,"---sec : %ld  us : %ld---",result.tv_sec,result.tv_usec);
	fprintf(ff,"%ld\n",result.tv_sec*1000000+result.tv_usec);
//	printf("case%d end / execution time = %ld\n",option,result.tv_sec*1000000+result.tv_usec);
	fclose(ff); 
	fclose(fp); 
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 628
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 47
breakdown:
   depth: 1, occurrence: 188
   depth: 2, occurrence: 41
   depth: 3, occurrence: 5
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
   depth: 6, occurrence: 1
   depth: 8, occurrence: 2
   depth: 12, occurrence: 1
   depth: 13, occurrence: 2
   depth: 15, occurrence: 3
   depth: 16, occurrence: 3
   depth: 17, occurrence: 1
   depth: 18, occurrence: 2
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 22, occurrence: 2
   depth: 23, occurrence: 3
   depth: 24, occurrence: 4
   depth: 25, occurrence: 1
   depth: 27, occurrence: 3
   depth: 28, occurrence: 1
   depth: 30, occurrence: 1
   depth: 31, occurrence: 1
   depth: 36, occurrence: 1
   depth: 40, occurrence: 1
   depth: 42, occurrence: 1
   depth: 46, occurrence: 2
   depth: 47, occurrence: 1

XXX total number of pointers: 546

XXX times a variable address is taken: 1354
XXX times a pointer is dereferenced on RHS: 417
breakdown:
   depth: 1, occurrence: 269
   depth: 2, occurrence: 99
   depth: 3, occurrence: 28
   depth: 4, occurrence: 10
   depth: 5, occurrence: 8
   depth: 6, occurrence: 3
XXX times a pointer is dereferenced on LHS: 310
breakdown:
   depth: 1, occurrence: 257
   depth: 2, occurrence: 34
   depth: 3, occurrence: 14
   depth: 4, occurrence: 3
   depth: 5, occurrence: 2
XXX times a pointer is compared with null: 51
XXX times a pointer is compared with address of another variable: 14
XXX times a pointer is compared with another pointer: 11
XXX times a pointer is qualified to be dereferenced: 8177

XXX max dereference level: 6
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 1068
   level: 2, occurrence: 443
   level: 3, occurrence: 275
   level: 4, occurrence: 42
   level: 5, occurrence: 32
   level: 6, occurrence: 16
XXX number of pointers point to pointers: 244
XXX number of pointers point to scalars: 302
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 31
XXX average alias set size: 1.4

XXX times a non-volatile is read: 2329
XXX times a non-volatile is write: 1016
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 3
XXX backward jumps: 11

XXX stmts: 191
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 30
   depth: 1, occurrence: 27
   depth: 2, occurrence: 24
   depth: 3, occurrence: 25
   depth: 4, occurrence: 38
   depth: 5, occurrence: 47

XXX percentage a fresh-made variable is used: 16.8
XXX percentage an existing variable is used: 83.2
********************* end of statistics **********************/

