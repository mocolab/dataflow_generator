#! /bin/bash

#echo -n "number of test_case : "
#read CASENUM 
#echo "number of test_case : $CASENUM"
CASENUM=$1
OPTION=$2
rm ../../csmith/src/db/*
rm -rf prepro_g etime.txt time.txt pid1.txt ipc.txt #./testcase/pre_result.txt
if [ $OPTION -eq 1 ];then
	rm -r ./testcase
fi
mkdir ./testcase
cd ../../csmith/src
gcc -w -o sys sys.c
./sys -0 $CASENUM --no_main_use
cp extract.c ./db/
cp db.sh ./db/
cd db
gcc -w -o extract extract.c
./db.sh $CASENUM
##rm ../../../dataflow/DB_gen/testcase/case*.c
cp ./case*.c ../../../dataflow/DB_gen/testcase
cat ./testcase_id.txt >> ../../../dataflow/DB_gen/testcase/testcase_id.txt
cd ../../../dataflow/DB_gen

#cp ./src/extract.c ./testcase/
#cp ./src/db.sh ./testcase/
#cd testcase
#gcc -w -o extract extract.c
#./db.sh $CASENUM
#rm db.sh extract* 
#rm t_case*.c
#cd ..
#gcc -w -o prepro_main ./prepro_main.c
gcc -w -o prepro_main ./src/prepro_main.c
./prepro_main $CASENUM
chmod 777 pre_sh.sh
./pre_sh.sh
./prepro_g
cp ./testcase/pre_result.txt ../pre_result_dir/pre_result0.txt #to do
rm ipc.txt etime.txt timet.txt prepro_g prepro_main pre_sh.sh prepro_g.c 
