import random
from gurobipy import *

def dal_final_header_gen(in_num,sdf_num,actor_num):
	test_case = []
#name = "SDF_"+str(sdf_num)+"_comb_a"+str(actor_num)
	name = "a"+str(actor_num)
#do actor name extract not a+num
	f=open("./dal_src_dir/df_"+str(sdf_num)+"/"+name+".h","w")
	
	f.write("#ifndef "+name+"_H\n")
	f.write("#define "+name+"_H\n")
	f.write("\n")
	f.write("#include <dal.h>\n")
	f.write("\n")
	for in_i in range(in_num):
		f.write("#define PORT_IN"+str(in_i)+" \"p"+str(in_i)+"\"\n")
	f.write("#define EVENT_DONE \"stop_fsm\"\n")
	f.write("\n")
	f.write("typedef struct _local_states{\n")
	f.write("\tint index;\n")
	f.write("\tint len;\n")
	f.write("}"+name+"_State;\n")
	f.write("\n")
	f.write("void "+name+"_init(DALProcess *);\n")
	f.write("int "+name+"_fire(DALProcess *);\n")
	f.write("void "+name+"_finish(DALProcess *);\n")
	f.write("\n")
	f.write("#endif\n")
	f.close()

def dal_final_src_gen(in_num,sdf_num,actor_num):
	local_len = 1
#name = "SDF_"+str(sdf_num)+"_comb_a"+str(actor_num)
	name = "a"+str(actor_num)
#do actor name extract not a+num
	f2=open("./dal_src_dir/df_"+str(sdf_num)+"/"+name+".c","w")

	f2.write("#include <stdio.h>\n")
	f2.write("#include <time.h>\n")
	f2.write("#include \""+name+".h\""+"\n")
	f2.write("\n")
	f2.write("void "+name+"_init"+"(DALProcess *p) {"+"\n")
	f2.write("\tp->local->index=0;\n")
	f2.write("\tp->local->len="+str(local_len)+";\n")
	f2.write("}\n")
	f2.write("\n")
	f2.write("int "+name+"_fire"+"(DALProcess *p) {"+"\n")
	f2.write("\ttimespec start2,end2;\n")
	f2.write("\tlong diff_time2,diff_ntime2;\n")
	f2.write("\tint x,n;\n")
	f2.write("\tif(p->local->index < p->local->len){\n")
	f2.write("\t\tprintf(\""+name+"_start\\n\");\n")
	for in_i in range(in_num):
		f2.write("\t\tDAL_read((void*)PORT_IN"+str(in_i)+", &x,sizeof(int),p);\n")
	f2.write("\t\tprintf(\""+name+"_loop\\n\");\n")
	f2.write("\t\tclock_gettime(CLOCK_MONOTONIC,&start2);\n")
	f2.write("\t\tprintf(\""+name+"_end\\n\");\n")
	f2.write("\t\tp->local->index++;\n")		
	f2.write("\t\tclock_gettime(CLOCK_MONOTONIC,&end2);\n")
	f2.write("\t\tdiff_time2 = (end2.tv_sec - start2.tv_sec);\n")
	f2.write("\t\tdiff_ntime2 = (end2.tv_nsec-start2.tv_nsec);\n")
	f2.write("\t\tprintf(\"%0.6lf second(s) - time of a"+str(actor_num)+" loop\\n\",diff_time2+(double)diff_ntime2/1000000000);")
	f2.write("\t}\n")
	f2.write("\tif(p->local->index >= p->local->len){\n")
	f2.write("\t\tDAL_send_event((void *)EVENT_DONE,p);\n")
	f2.write("\t\treturn(1);\n")
	f2.write("\t}\n")
	f2.write("\n")
	f2.write("\treturn 0;\n")
	f2.write("}\n")
	f2.write("\n")
	f2.write("void "+name+"_finish(DALProcess *p) {\n")
	f2.write("\tfflush(stdout);\n")
	f2.write("}\n")
#	f.close()
	f2.close()







