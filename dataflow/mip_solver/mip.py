import random
from gurobipy import *

def dal_header_gen(in_num,out_num,in_rate,out_rate,sdf_num,actor_num,case_num,actor_max,end_vertex_num):
	test_case = []
#name = "SDF_"+str(sdf_num)+"_comb_a"+str(actor_num)
	name = "a"+str(actor_num)
#do actor name extract not a+num
	f=open("./dal_src_dir/df_"+str(sdf_num)+"/"+name+".h","w")
	f2=open("../DB_gen/testcase/testcase_id.txt","r")
	lines = f2.readlines()
	for line in lines:
		test_case.append(line[14:24])

	f.write("#ifndef "+name+"_H\n")
	f.write("#define "+name+"_H\n")
	f.write("\n")
	f.write("#include <dal.h>\n")
#f.write("#include \""+name+".h\""+"\n")
	f.write("\n")
	for in_i in range(in_num):
		f.write("#define PORT_IN"+str(in_rate[in_i][0])+" \"p"+str(in_rate[in_i][0])+"\"\n")
	for out_i in range(out_num):
		f.write("#define PORT_OUT"+str(out_rate[out_i][0])+" \"p"+str(out_rate[out_i][0])+"\"\n")
	if (actor_num > 0) and (in_num > 0) and ( out_num == 0):
		f.write("#define EVENT_DONE_"+str(end_vertex_num-1)+" \"stop_fsm_"+str(end_vertex_num-1)+"\"\n")
	f.write("\n")
	f.write("typedef struct _local_states{\n")
	f.write("\tint index;\n")
	f.write("\tint len;\n")
	f.write("}"+name+"_State;\n")
	f.write("\n")
	f.write("void "+name+"_init(DALProcess *);\n")
	f.write("int "+name+"_fire(DALProcess *);\n")
	f.write("void "+name+"_finish(DALProcess *);\n")
	f.write("\n")
	for t in range(case_num):
		f.write("int case"+str(test_case[t])+"(void);\n")
	f.write("\n")
	f.write("\n")
	f.write("#endif\n")
	f.close()
	f2.close()

def dal_src_gen(loop_group,in_num,out_num,in_rate,out_rate,sdf_num,actor_num,case_num,actor_max,end_vertex_num):
	loop=[]
	test_case=[]
	local_len = 1
#name = "SDF_"+str(sdf_num)+"_comb_a"+str(actor_num)
	name = "a"+str(actor_num)
#do actor name extract not a+num
#	f=open("../mip_solver/loop_dir/"+name+".txt","r")
	f2=open("./dal_src_dir/df_"+str(sdf_num)+"/"+name+".c","w")
	f3=open("../DB_gen/testcase/testcase_id.txt","r")
#	lines = f.readlines()
#	for line in lines:
#		loop.append(int(line))
	lines3 = f3.readlines()
	for line3 in lines3:
		test_case.append(line3[14:24])

	f2.write("#include <stdio.h>\n")
	f2.write("#include <time.h>\n")
	f2.write("#include \""+name+".h\""+"\n")
	f2.write("\n")
	f2.write("void "+name+"_init"+"(DALProcess *p) {"+"\n")
	f2.write("\tp->local->index=0;\n")
	f2.write("\tp->local->len="+str(local_len)+";\n")
	f2.write("}\n")
	f2.write("\n")
	f2.write("int "+name+"_fire"+"(DALProcess *p) {"+"\n")
	f2.write("\ttimespec start2,end2;\n")
	f2.write("\tlong diff_time2,diff_ntime2;\n")
	f2.write("\tint x,n;\n")
	f2.write("\tif(p->local->index < p->local->len){\n")
	f2.write("\t\tprintf(\""+name+"_start\\n\");\n")
	for in_i in range(in_num):
		for in_rate_i in range(in_rate[in_i][1]):
			f2.write("\t\tDAL_read((void*)PORT_IN"+str(in_rate[in_i][0])+", &x,sizeof(int),p);\n")
	f2.write("\t\tclock_gettime(CLOCK_MONOTONIC,&start2);\n")
	f2.write("\t\tprintf(\""+name+"_loop\\n\");\n")
	for case_i in range(case_num):
		f2.write("\t\tfor(n=0;n<"+str(loop_group[case_i])+";n++){\n")
		f2.write("\t\t\tcase"+test_case[case_i]+"();\n")
		f2.write("\t\t}\n")
	f2.write("\t\tx=x+1;\n")
	f2.write("\t\tprintf(\""+name+"_loop_end\\n\");\n")
	for out_i in range(out_num):
		for out_rate_i in range(out_rate[out_i][1]):
			f2.write("\t\tDAL_write((void*)PORT_OUT"+str(out_rate[out_i][0])+",&x, sizeof(int), p);\n")
	f2.write("\t\tprintf(\""+name+"_end\\n\");\n")
	f2.write("\t\tp->local->index++;\n")		
	f2.write("\t\tclock_gettime(CLOCK_MONOTONIC,&end2);\n")
	f2.write("\t\tdiff_time2 = (end2.tv_sec - start2.tv_sec);\n")
	f2.write("\t\tdiff_ntime2 = (end2.tv_nsec-start2.tv_nsec);\n")
	f2.write("\t\tprintf(\"%0.6lf second(s) - time of a"+str(actor_num)+" loop\\n\",diff_time2+(double)diff_ntime2/1000000000);")
	f2.write("\t}\n")
	f2.write("\tif(p->local->index >= p->local->len){\n")
	if (actor_num > 0)and (in_num > 0) and( out_num == 0):
		f2.write("\tDAL_send_event((void *)EVENT_DONE_"+str(end_vertex_num)+",p);\n")
		end_vertex_num = end_vertex_num + 1
	f2.write("\t\treturn(1);\n")
	f2.write("\t}\n")
	f2.write("\n")
	f2.write("\treturn 0;\n")
	f2.write("}\n")
	f2.write("\n")
	f2.write("void "+name+"_finish(DALProcess *p) {\n")
	f2.write("\tfflush(stdout);\n")
	f2.write("}\n")
#	f.close()
	f2.close()
	f3.close()
	return end_vertex_num


def mip_solver(case_num,hw_num,actor_num,df_num,rate_list):
# read case time & required time of all HW
	case_time = [[0 for i in range(case_num)] for h in range(hw_num)]
	for h in range(hw_num):
		i = 0
		f = open('../pre_result_dir/pre_result' + str(h) + '.txt', 'r')
		lines = f.readlines()
		for line in lines:
				case_time[h][i] = line[7:15]
				print("hw" + str(h) + " " + str(h) + "," + str(i) + " : " + case_time[h][i])
				i += 1
#end_vertex_num!!
	end_vertex_num = 0
	for itr in range(actor_num):
		proc_list = []
		req_time = [0 for i in range(100)]
		e = 0.5
		inf = 9999999
		u_limit = 10
#	case_num = input('enter the number of case : ')
#	hw_num = input('enter the number of Test_HW : ')
#	case_time = [[0 for i in range(case_num)] for h in range(hw_num)]
#	req_time = []
		loop_group = []
		f = open('../mip_solver/req_time_dir/req_time_'+str(df_num)+'.txt','r')
		lines = f.readlines()
		print(lines)
		temp_str = lines[itr+1]
		line_split = temp_str.split(' ')
		print(line_split)
		for spl in range(len(line_split)-2):
			line_split_2 = line_split[spl+1].split('=')
			line_split_3 = line_split_2[0].split('_')
			proc_num= int(line_split_3[1])
			temp_req_time = float(line_split_2[1])
			req_time[proc_num]=0.1*temp_req_time
			proc_list.append(proc_num)
#req_time.insert(h,0.001*random.randint(1,100))

#just test
#req_time[0]=99*0.001
#req_time[1]=77*0.001
		print("--------- " + str(itr) +"_req_time --------")
		print(req_time)
		print("--------- " + str(itr) +"_proc_list --------")
		print(proc_list)
# create var list
		l = [] #loop
		Groundset = range(case_num)

		try:
				# create model
				m = Model("mip1")
				# add var
				e_rate = m.addVar(vtype=GRB.INTEGER, ub=100000, lb=0, name="Error_rate")
				for i in range(case_num):
						l.insert(i, m.addVar(vtype=GRB.INTEGER,ub=10000000,lb=0, name="Loop"+str(i)))

				u = m.addVars(Groundset, vtype=GRB.BINARY, name="Used")
				# add constr
				for i in range(case_num):
						m.addConstr(l[i] >= e + (u[i] - 1) * inf, "c_check_1_" + str(i))
						m.addConstr(l[i] <= e + (u[i] * inf), "c_check_2_" + str(i))
				for h in range(len(proc_list)):
						print("h"+str(h))
						req_unit = float(req_time[proc_list[h]]) * 0.00001
						print(proc_list[h])
						m.addConstr(quicksum(l[i]*float(case_time[int(proc_list[h])][i]) for i in range(case_num)) <= float(req_time[int(proc_list[h])])+req_unit*e_rate,"c_req_hw_ub_" + str(proc_list[h]))
						m.addConstr(quicksum(l[i]*float(case_time[int(proc_list[h])][i]) for i in range(case_num)) >= float(req_time[int(proc_list[h])])-req_unit*e_rate,"c_req_hw_lb_" + str(proc_list[h]))
			#      m.addConstr(quicksum(l[i] * float(case_time[h][i])-float(req_time[h])/float(case_num) for i in range(case_num))>=0,"c_req_hw_zb_" + str(h))
				m.addConstr(u.sum() <= u_limit, "c_u_limit")

				# m.setObjective(quicksum(l[i] for i in range(case_num)), GRB.MINIMIZE)
				m.setObjective(e_rate, GRB.MINIMIZE)

				# Optimize
				m.optimize()

				# print
				w = 0
				print "Vars"
				for v in m.getVars():
					w = w+1
					print(v.varName, v.x)
					if str(v.varName[0:4])== "Loop":
						loop_group.insert(w,int(v.x))
				print "Constrs"
				for c in m.getConstrs():
					print(c.constrName, c.sense, c.rhs)
				print('Obj => Error_rate: ', float(m.objVal)*0.00001)
				print(req_time)
				print("loop_group is shown below")
				print(loop_group)
				f2=open("../mip_solver/loop_dir/SDF_"+str(df_num)+"_comb_"+"a"+str(itr)+".txt","w")
				for k in range(len(loop_group)):
					f2.write(str(loop_group[k])+'\n')
				
				# --- src gen
				print("rate_list")
				print(rate_list)
				in_num =0
				out_num =0
				in_rate= []
				out_rate=[]
				for n in range(len(rate_list)): 
					if int(rate_list[n][0])==itr:
						if rate_list[n][2]=="in":
							temp_list=[0,0]
							in_num = in_num+1
							temp_list[0]=int(rate_list[n][1])
							temp_list[1]=int(rate_list[n][3])
							in_rate.append(temp_list)
						elif rate_list[n][2]=="out":
							temp_list=[0,0]
							out_num = out_num+1
							temp_list[0]=int(rate_list[n][1])
							temp_list[1]=int(rate_list[n][3])
							out_rate.append(temp_list)
				end_vertex_num = dal_src_gen(loop_group,in_num,out_num,in_rate,out_rate,df_num,itr,case_num,actor_num,end_vertex_num)
				dal_header_gen(in_num,out_num,in_rate,out_rate,df_num,itr,case_num,actor_num,end_vertex_num)
			  # --- src gen 



		except GurobiError:
				print('Error reported')
	print("end_vertex_num")
	print(end_vertex_num)
	return end_vertex_num

#	return loop_group

#case_num = input('enter the number of case : ')
#hw_num = input('enter the number of Test_HW : ')
#case_time = [[0 for i in range(case_num)] for h in range(hw_num)]
#req_time = []
#mip_solver(req_time,case_time)







